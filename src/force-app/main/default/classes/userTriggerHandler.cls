public class userTriggerHandler {
    
    public static void beforeUpdate(User[] oldUsers, User[] newUsers) {
        boolean checkDeactivation = false;
        for (integer i=0; i<newUsers.size(); i++) {
            if (oldUsers[i].isActive && !newUsers[i].isActive) {
                //when a user is deactivated, make sure they aren't a territory or default owner
                checkDeactivation = true;
            }
        }
        if (checkDeactivation) {
            userManagement.checkDeactivation(newUsers);
        } 
    }
    
    public static void afterInsert(User[] oldUsers, User[] newUsers) {
        boolean runRRManagement = false;
        for (integer i=0; i<newUsers.size(); i++) {
            //when a RR user is inserted, sent to RR management
            if (newUsers[i].EntLeads_RR__c && newUsers[i].suite_of_interest__c != 'Ergonomics') {
                runRRManagement = true;
            }
        }
        if (runRRManagement) {
            userManagement.enterpriseLeadRRManagement();
        }
    }
    
    public static void afterUpdate(User[] oldUsers, User[] newUsers) {
        boolean runRRManagement = false;
        for (integer i=0; i<newUsers.size(); i++) {
            //when a RR user is changed, send to RR management
            if (oldUsers[i].EntLeads_RR__c != newUsers[i].EntLeads_RR__c && newUsers[i].suite_of_interest__c != 'Ergonomics') {
                    runRRManagement = true;
                }
        }
        if (runRRManagement) {
            userManagement.enterpriseLeadRRManagement();
        }
    }
    
    
    
    
    
}