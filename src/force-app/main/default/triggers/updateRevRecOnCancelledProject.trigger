trigger updateRevRecOnCancelledProject on Case (after update) {
    set<Id> cancelledIdSet = new set<Id>();
    for(Case cse : System.Trigger.new){
        if(cse.Type == 'Compliance Services' && cse.Status == 'Closed' && cse.Project_Status__c=='Cancelled'){
            cancelledIdSet.add(cse.Id);
        }
    }
    //Runs when cancelling a case
    if(cancelledIdSet.size() > 0){
        Revenue_Recognition_Transaction__c[] revRecTransactions = [SELECT Id, Recognition_Amount__c, Revenue_Recognition__c,
                                                                   Revenue_Recognition__r.Case__c FROM Revenue_Recognition_Transaction__c WHERE
                                                                   Revenue_Recognition__r.Case__c IN : cancelledIdSet];
        for(Id cseId : cancelledIdSet){
            for(Revenue_Recognition_Transaction__c rrt : revRecTransactions){
                if(cseId == rrt.Revenue_Recognition__r.Case__c){
                    rrt.Recognition_Amount__c = 0;
                }
            }
        } 
        update revRecTransactions;
    }
}