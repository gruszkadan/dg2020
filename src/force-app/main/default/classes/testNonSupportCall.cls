@isTest(SeeAllData=true)
private class testNonSupportCall {
    
    static testMethod void myUnitTest() {
        
        User u = [SELECT Id FROM user WHERE Group__c = 'Customer Care' and IsActive = TRUE LIMIT 1];
        system.runAs(u){
        Account newAccount = new Account();
        newAccount.Name = 'Non Customer';
        newAccount.AdminID__c = '402365';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Non';
        newContact.LastName = 'Customer';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        
        nonSupportCall myController = new nonSupportCall();
            myController.ptype = 'Call';
        myController.createCase();
        myController.caseInfo(); 
        myController.nonCCRole = FALSE;
        }
    }
}