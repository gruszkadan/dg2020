@isTest()
private class testAmeConsoleToDoController {
    @isTest static void test1() {
        
        //Insert Account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.NAICS_Code__c = 'Test';
        newAccount.Customer_Status__c ='Prospect';
        insert newAccount;
        
        //Insert Contact
        Contact Con = new Contact();
        Con.LastName = 'Lahners';
        Con.AccountId = newAccount.Id;
        insert Con; 
        
        //Insert AME
        Account_Management_Event__c newAme = new Account_Management_Event__c();
        newAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        newAme.Status__c = 'Active';
        newAme.Account__c = newAccount.Id;
        newAme.Contact__c = Con.Id;
        insert newAme;
        
        ApexPages.currentPage().getParameters().put('id', newAME.id);
        ameConsoleToDoController controller = new ameConsoleToDoController();
        controller.getCurrentDateTime();
        controller.updateSort();
        
        //Assert that the query found our AME 
        system.assert(controller.ames.size() == 1);
        
        //Assert that the ame came back as selected
        system.assert(controller.selectedAMEId == newAME.id);
    }
    
}