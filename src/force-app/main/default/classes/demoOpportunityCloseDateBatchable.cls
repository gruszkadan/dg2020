global class demoOpportunityCloseDateBatchable implements Database.Batchable<SObject>, Database.Stateful{
    global integer numOfOpps = 0;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (!test.isRunningTest()) {
            return Database.getQueryLocator([SELECT id,CloseDate,StageName
                                             FROM Opportunity
                                             WHERE StageName = 'System Demo'
                                             AND Opportunity_Type__c = 'New'
                                             AND CloseDate >=: date.today()
                                             AND CloseDate <=: date.today().addDays(100)]);
        } else {
            return Database.getQueryLocator([SELECT id,CloseDate,StageName
                                             FROM Opportunity
                                             WHERE StageName = 'System Demo'
                                             AND Opportunity_Type__c = 'New'
                                             AND CloseDate >=: date.today()
                                             AND CloseDate <=: date.today().addDays(100)
                                             LIMIT 100]);
        }
    }
    
    global void execute(Database.BatchableContext bc, list<SObject> batch) {
        Opportunity[] oppsToUpdate = new list<Opportunity>();
        Integer numberOfDays = Date.daysInMonth(date.today().addDays(100).year(), date.today().addDays(100).month());
        Date lastDayOfMonth = Date.newInstance(date.today().addDays(100).year(), date.today().addDays(100).month(), numberOfDays);
        for(Opportunity o : (list<Opportunity>) batch){
            if(o.CloseDate != lastDayOfMonth){
                o.CloseDate = lastDayOfMonth;
                oppsToUpdate.add(o);
            }
        }
        if(oppsToUpdate.size() > 0){
            try {
                numOfOpps = oppsToUpdate.size();
                update oppsToUpdate;
            } catch(exception e) {
                salesforceLog.createLog('Opportunity', true, 'demoOpportunityCloseDateBatchable', 'execute', string.valueOf(e));
            }
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        //Compose Async Apex Job info
        AsyncApexJob oAsyncApexJob = [SELECT Id, ApexClassId,
                                      JobItemsProcessed,
                                      TotalJobItems,
                                      NumberOfErrors,
                                      ExtendedStatus,
                                      CreatedBy.Email
                                      FROM AsyncApexJob
                                      WHERE id =: bc.getJobId()];
        //Compose Single EmailMessage service
        string htmlBody ='<b>System Demo Opportunities Close Date Results</b></br></br><b># of Opportunities Updated:</b> '+numOfOpps+'</br></br><b># of Errors:</b> '+ oAsyncApexJob.NumberOfErrors;
        if(oAsyncApexJob.ExtendedStatus != NULL){
            htmlBody = htmlBody + '</br></br><b>Error Message:</b> '+ oAsyncApexJob.ExtendedStatus;
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] sToAddress = new String[]{'salesforceops@ehs.com'};
            mail.setToAddresses(sToAddress);
        mail.setReplyTo('dgruszka@ehs.com');                      
        mail.setSenderDisplayName('Daniel Gruszka');
        mail.setSubject('Batch Job Summary: System Demo Opportunities Close Date Update');
        mail.setHtmlBody(htmlBody);
        Messaging.sendEmail( new Messaging.SingleEmailMessage[]{mail});
    }
}