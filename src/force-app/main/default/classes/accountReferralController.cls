public class accountReferralController {
    public Account_Referral__c refAuth {get;set;}
    public Account_Referral__c refErgo {get;set;}
    public Account_Referral__c refEHS {get;set;}
    public Account_Referral__c refChem {get;set;}
    public Account_Referral__c refODT {get;set;}
    public boolean showAuth {get;set;}
    public boolean showErgo {get;set;}
    public boolean showEHS {get;set;}
    public boolean showChem {get;set;}
    public boolean showODT {get;set;}
    public id aId {get;set;}     // account id parameter
    public string anchor {get;set;}     // anchor parameter
    public string refType {get;set;}
    
    public accountReferralController() {
        aId = ApexPages.currentPage().getParameters().get('id');     // grab the account id passed through url
        anchor = ApexPages.currentPage().getParameters().get('anchor');     // grab the anchor location passed through url
        findReferral();     // call method to query the correct referral
    }
    
    public void findReferral() {
        string SObjectAPIName = 'Account_Referral__c';
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string extraFields = ', Contact_To_Speak_To_Consultants__r.FirstName, Contact_To_Speak_To_Consultants__r.LastName, Contact_To_Speak_To_Consultants__r.Email, Contact_To_Speak_To_Consultants__r.Phone,'+
            ' Referrer__r.FirstName, Referrer__r.LastName, Safety_Training_Contact__r.Name, Safety_Training_Contact__r.Phone, Safety_Training_Contact__r.Email ';
        string query = 'SELECT '+commaSepratedFields+extraFields+' FROM '+SObjectAPIName+' WHERE Account__c = \''+aid+'\' ORDER BY CreatedDate ASC';
        Account_Referral__c[] refs = Database.query(query);    
        for (Account_Referral__c ref : refs) {
            if (ref.Type__c == 'Authoring') {
                showAuth = true;
                refAuth = ref;
                if (ref.Date__c < date.parse('12/01/2015')) {
                    showAuth = false;
                }
            }
            if (ref.Type__c == 'Ergonomics') {
                showErgo = true;
                refErgo = ref;
                if (ref.Date__c < date.parse('12/01/2015')) {
                    showErgo = false;
                }
            }
            if (ref.Type__c == 'EHS') {
                showEHS = true;
                refEHS = ref;
                if (ref.Date__c < date.parse('12/01/2015')) {
                    showEHS = false;
                }
            }
            if (ref.Type__c == 'ODT') {
                showODT = true;
                refODT = ref;
                if (ref.Date__c < date.parse('12/01/2015')) {
                    showODT = false;
                }
            }
            if (ref.Type__c == 'Chemical Management') {
                showChem = true;
                refChem = ref;
                if (ref.Date__c < date.parse('12/01/2015')) {
                    showChem = false;
                }
            }
        }
    }
}