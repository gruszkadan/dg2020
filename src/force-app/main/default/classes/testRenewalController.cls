@isTest(SeeAllData=true)
private class testRenewalController {

    static testMethod void test1() {
        //Setup
               
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.AdminID__c = '123456';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
                
        Case newCase1 = new Case();
        newCase1.AccountID = newAccount.Id;
        newCase1.ContactID = newContact.Id;
        newCase1.Status = 'Active';
        newCase1.Type = 'Quarterly Contact';
        newCase1.Subject = 'Test';
        newCase1.Case_Resolution__c = 'ttttt';
        newCase1.OwnerID = '00580000003UChI';  
        insert newCase1;
                
        Renewal__c newRenew = new Renewal__c();
        newRenew.Account__c = newAccount.ID;
        newRenew.Contact__c = newContact.Id;
        newRenew.Status__c = 'Active';
        insert newRenew;
        
        Renewal_Note__c newNote = new Renewal_Note__c();
        newNote.Renewal__c = newRenew.Id;
        newNote.Comments__c = 'testtestetsets';
        newNote.Note_Type__c = 'Call - Outbound';
        newNote.Call_Duration__c = 1;
        insert newNote;      
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c=newAccount.Id;
        newContract.Contact__c=newContact.Id;
        newContract.Renewal__c = newRenew.Id;
        newContract.OwnerID='00580000003UChI';
    	insert newContract;
        
        
        ApexPages.currentPage().getParameters().put('Id', newRenew.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Renewal__c());
        renewalController myController = new renewalController(testController); 
        myController.newRenewalNote();       
    }
    }