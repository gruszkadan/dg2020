public class kbContentSubmissionController {
    private ApexPages.StandardController controller;
    public SF_Knowledge_Content_Request__c newSubmission {get;set;}
    public User theUser {get;set;}
    
    public kbContentSubmissionController(ApexPages.StandardController stdController) {
        Id kbContentId = ApexPages.currentPage().getParameters().get('id');
        String submissionType = ApexPages.currentPage().getParameters().get('submissionType');
        this.controller = stdController;
        theUser = [Select id, Name, LastName, Email, ManagerID, Department__c from User where id =: UserInfo.getUserId() LIMIT 1];
        if(kbContentId == NULL){
            newSubmission = new SF_Knowledge_Content_Request__c();
            newSubmission.Submitter_Name__c = theUser.Id;
            newSubmission.Submission_Date__c = System.today();
            newSubmission.Submission_Type__c = submissionType;
        }
    }
    
    public PageReference step2(){
        if(newSubmission.Submission_Type__c =='SF Knowledge Article'){
            PageReference kb = Page.kbContentSubmission_new_kb;
            kb.getParameters().put('submissionType', newSubmission.Submission_Type__c);	
			kb.setRedirect(true);
            return kb;
        } else {
            PageReference content = Page.kbContentSubmission_new_content;
            content.getParameters().put('submissionType', newSubmission.Submission_Type__c);
			content.setRedirect(true);
            return content;
        }
    }
    
    public PageReference saveNewSubmission() {
        insert newSubmission;
        PageReference pageRef = new PageReference('/' + newSubmission.Id);
        return pageRef;
    }
}