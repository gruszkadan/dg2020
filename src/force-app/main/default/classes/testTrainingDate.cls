@isTest(SeeAllData=true)
public with sharing class testTrainingDate {
	static testMethod void test1() { 
        datetime fDate = datetime.newInstance(2013, 06, 21);
        datetime lDate = datetime.newInstance(2013, 07, 21);
        
        Account newAccount = new Account();
    	newAccount.Name = 'Test Account';
    	newAccount.AdminID__c = '123456';
    	newAccount.Customer_Status__c ='Active';
    	insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'IMP: HQ';
        newCase.Subject = 'Test';
        newCase.OwnerID = '00580000003UChI';  
        insert newCase;
        
        Case_Notes__c newNote = new Case_Notes__c();
        newNote.Case__c = newCase.ID;
        newNote.Notes_Type__c = 'Training';
        newNote.Call_Duration__c = 10;
        newNote.Customer_Contacted__c = TRUE;
        newNote.Product__c = 'HQ Account';
        newNote.CreatedDate = fDate;
        insert newNote;
        
        Case newCase1 = new Case();
        newCase1.AccountID = newAccount.Id;
        newCase1.ContactID = newContact.Id;
        newCase1.Status = 'Active';
        newCase1.Type = 'IMP: HQ';
        newCase1.Subject = 'Test';
        newCase1.OwnerID = '00580000003UChI';  
        insert newCase1;
        
        Case_Notes__c newNote1 = new Case_Notes__c();
        newNote1.Case__c = newCase1.ID;
        newNote1.Notes_Type__c = 'Training';
        newNote1.Call_Duration__c = 10;
        newNote1.Customer_Contacted__c = TRUE;
        newNote1.Product__c = 'HQ Account';
        newNote1.CreatedDate = lDate;
        insert newNote1;
   
        Account acct = [select id, First_Training_Date__c, Last_Training_Date__c from Account where Id = :newAccount.Id]; 
        System.assertEquals(ldate,acct.Last_Training_Date__c); 
        System.assertEquals(fdate,acct.First_Training_Date__c); 
}
}