public with sharing class proposalPrint {
    private Apexpages.StandardController controller; 
    public Quote__c theQuote {get;set;}
    public final Account account {get; set;}
    public final Contact contact {get; set;}
    public Quote_Product__c[] hqProduct {get;set;}
    public Quote_Product__c[] hq {get;set;}
    public Quote_Product__c[] GM {get;set;}
    public Quote_Product__c[] GMPRO {get;set;}
    public Quote_Product__c[] authLabel {get;set;}
    public Quote_Product__c[] authMSDS {get;set;}
    public Quote_Product__c[] authReg {get;set;}
    public Quote_Product__c[] authTrans {get;set;}
    public Quote_Product__c[] authCas {get;set;}
    public Quote_Product__c[] authComplexFormula {get;set;}
    public Quote_Product__c[] authComplianceReview {get;set;}
    public Quote_Product__c[] authCustomTemplate {get;set;}
    public Quote_Product__c[] authGroupingAnalysis {get;set;}
    public Quote_Product__c[] authLabAnalysis {get;set;}
    public Quote_Product__c[] authCustomPhrase {get;set;}
    public Quote_Product__c[] authAliasSDS {get;set;}
    public Quote_Product__c[] regXR {get;set;}
    public Quote_Product__c[] chemItems {get;set;}
    public Quote_Product__c pricingFactor {get;set;}
    public Quote_Product__c[] servicesItems {get;set;}
    public Quote_Product__c[] specialIndexingItems {get;set;}
    public Quote_Product__c[] ongoingIndexingItems {get;set;}
    public Quote_Product__c[] adcItems {get;set;}
    public Quote_Product__c[] oItems {get;set;}
    public Quote_Product__c[] proposedItems {get;set;}
    public Quote_Product__c[] authoringItems {get;set;}
    public Quote_Product__c[] imItems {get;set;}
    public Quote_Product__c[] imMain {get;set;}
    public Quote_Product__c[] imOptions {get;set;}
    public Quote_Product__c[] imServices {get;set;}
    public Quote_Product__c[] shoppingCart {get;set;}
    public Quote_Product__c[] hqDetail {get;set;}
    public Quote_Product__c[] hqRegXRDetail {get;set;}
    public Quote_Product__c[] gmDetail {get;set;}
    public Quote_Product__c[] ODT {get;set;}
    public Quote_Product__c[] rdDetail {get;set;}
    public Quote_Product__c[] webplianceDetail {get;set;}
    public Quote_Product__c[] authoringDetail {get;set;}
    public Quote_Product__c[] faxbackDetail {get;set;}
    public Quote_Product__c[] libraryBuildDetail {get;set;}
    public Quote_Product__c[] imDetail {get;set;}
    public Quote_Product__c[] trainingDetail {get;set;}
    public Quote_Product__c[] trainingIndustryDetail {get;set;}
    public Quote_Product__c[] trainingIndustryPremiumDetail {get;set;}
    public Quote_Product__c[] STK {get;set;}
    public Quote_Product__c[] ETK {get;set;}
    public Quote_Product__c[] spillCenter {get;set;}
    public Quote__c po;
    public integer numNonODT {get;set;}
    public integer numAtts {get;set;}
    public integer numLogo {get;set;}
    public String logoMessage {get;set;}
    public Attachment logo {get;set;}
    private Product2[] products;
    SObject Y1Total;
    SObject Y2Total;
    SObject Y3Total;
    SObject Y4Total;
    SObject Y5Total;
    SObject siY1Total;
    SObject siY2Total;
    SObject siY3Total;
    SObject siY4Total;
    SObject siY5Total;
    SObject oiY1Total;
    SObject oiY2Total;
    SObject oiY3Total;
    SObject oiY4Total;
    SObject oiY5Total;
    SObject Y1AuthTotal;
    SObject Y2AuthTotal;
    SObject Y3AuthTotal;
    SObject Y4AuthTotal;
    SObject Y5AuthTotal;
    public Boolean showChem {get;set;}
    public Boolean showOptions {get;set;}
    public Boolean showServices {get;set;}
    public Boolean showIMMain {get;set;}
    public Boolean showIMOptions {get;set;}
    public Boolean showIMServices {get;set;}
    public Boolean showSpecialIndexing {get;set;}
    public Boolean showOngoingIndexing {get;set;}
    public Boolean showADC {get;set;}
    public Boolean showAuthoring {get;set;}
    public Boolean showProposed {get;set;}
    public Boolean showhqDetail {get;set;}
    public Boolean showhqRegXRDetail {get;set;}
    public Boolean showgmDetail {get;set;}
    public Boolean showrdDetail {get;set;}
    public Boolean showwebplianceDetail {get;set;}
    public Boolean showauthoringDetail {get;set;}
    public Boolean showfaxbackDetail {get;set;}
    public Boolean showlibraryBuildDetail {get;set;}
    public Boolean showimDetail {get;set;}
    public Boolean showtrainingDetail {get;set;}
    public Boolean showTrainingIndustryDetail {get;set;}
    public Boolean showTrainingPremiumDetail {get;set;}
    public Boolean showSTK {get;set;}
    public Boolean showETK {get;set;}
    public Boolean showSpillCenter {get;set;}
    public Boolean showLogo {get;set;}
    public Boolean showPricingFactor {get;set;}
    public Integer theYear {get;set;}
    public boolean isErgo {get;set;}
    public Attachment myfile;
    boolean isOption;
    id qopsId;
    
    public proposalPrint(ApexPages.StandardController stdController) {
        this.controller = stdController;
        // grab the quote id and query the quote
        id quoteId = ApexPages.currentPage().getParameters().get('id');     
        queryQuote(quoteId);
        queryShoppingCart();
        
        numAtts = [SELECT count() FROM Attachment WHERE ParentId = :theQuote.id AND Name LIKE '%.pdf'];
        numLogo = [SELECT count() FROM Attachment WHERE ParentId = :theQuote.id AND Name LIKE '%logo%'];
        
        if (theQuote.Account__c != null) {
            account = [SELECT id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode FROM Account WHERE id = :theQuote.Account__c LIMIT 1];
        } else {
            account = new Account();
        }
        
        if (theQuote.Contact__c != null) {
            contact = [SELECT id, Name FROM Contact WHERE id = :theQuote.Contact__c LIMIT 1];
        } else {
            contact = new Contact();
        }
        theYear = date.today().year();
        createLists();
        numNonOdt = shoppingCart.size() - odt.size();
        setPricingSections();
        
    }

    public void queryQuote(string qid) {
        string SObjectAPIName = 'Quote__c';
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string addFields = '';
        string query = 'SELECT '+commaSepratedFields+addFields+' FROM '+SObjectAPIName+' WHERE id = \''+qid+'\' LIMIT 1';
        theQuote = Database.query(query);
        
        if (theQuote.Proposal_Type__c == 'Ergonomics') {
            isErgo = true;
        } else {
            isErgo = false;
        }
        
        //check for options
        if (theQuote.Quote_Options__c != null) {
            isOption = true;
            qopsId = theQuote.Quote_Options__c;
        } else {
            isOption = false;
        }
    }
    
    public void queryShoppingCart() {
        string SObjectAPIName = 'Quote_Product__c';
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string addFields = ', Product__r.Product_Page__c, Product__r.ODT__c, Product__r.Incident_Management__c, Product__r.Proposal_Print_Group__c';
        string query = '';
        if (isOption) {
            query += 'SELECT '+commaSepratedFields+addFields+' FROM '+SObjectAPIName+' WHERE Quote__r.Quote_Options__c = \''+qopsId+'\' AND Quote__r.Include_Option_In_Proposal__c = true ORDER BY Product_Print_Name__c';
        } else {
            query += 'SELECT '+commaSepratedFields+addFields+' FROM '+SObjectAPIName+' WHERE Quote__c = \''+theQuote.id+'\' ORDER BY Product_Print_Name__c';
        }
        shoppingCart = Database.query(query);
    }
    
    public void createLists() {
        hqProduct = new List<Quote_Product__c>();
        hqDetail = new List<Quote_Product__c>();
        hqRegXRDetail = new List<Quote_Product__c>();
        gmDetail = new List<Quote_Product__c>();
        rdDetail = new List<Quote_Product__c>();
        webplianceDetail = new List<Quote_Product__c>();
        authoringDetail = new List<Quote_Product__c>();
        faxbackDetail = new List<Quote_Product__c>();
        libraryBuildDetail = new List<Quote_Product__c>();
        imDetail = new List<Quote_Product__c>();
        trainingIndustryDetail = new List<Quote_Product__c>();
        trainingDetail = new List<Quote_Product__c>();
        trainingIndustryPremiumDetail = new List<Quote_Product__c>();
        stk = new List<Quote_Product__c>();
        etk = new List<Quote_Product__c>();
        spillCenter = new List<Quote_Product__c>();
        hq = new List<Quote_Product__c>();
        regXr = new List<Quote_Product__c>();
        gm = new List<Quote_Product__c>();
        gmPro = new List<Quote_Product__c>();
        odt = new List<Quote_Product__c>();
        chemItems = new List<Quote_Product__c>();
        authLabel = new List<Quote_Product__c>();
        authAliasSds = new List<Quote_Product__c>();
        authMsds = new List<Quote_Product__c>();
        authReg = new List<Quote_Product__c>();
        authTrans = new List<Quote_Product__c>();
        authCas = new List<Quote_Product__c>();
        authComplexFormula = new List<Quote_Product__c>();
        authComplianceReview = new List<Quote_Product__c>();
        authCustomTemplate = new List<Quote_Product__c>();
        authGroupingAnalysis = new List<Quote_Product__c>();
        authLabAnalysis = new List<Quote_Product__c>();
        authCustomPhrase = new List<Quote_Product__c>();
        servicesItems = new List<Quote_Product__c>();
        specialIndexingItems = new List<Quote_Product__c>();
        ongoingIndexingItems = new List<Quote_Product__c>();
        adcItems = new List<Quote_Product__c>();
        oItems = new List<Quote_Product__c>();
        proposedItems = new List<Quote_Product__c>();
        authoringItems = new List<Quote_Product__c>();
        imItems = new List<Quote_Product__c>();
        imMain = new List<Quote_Product__c>();
        imOptions = new List<Quote_Product__c>();
        imServices = new List<Quote_Product__c>();
        
        for (Quote_Product__c qp : shoppingCart) {
            if (qp.Product_Print_Name__c.startsWithIgnoreCase('HQ')) {
                hqProduct.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalHQProductDetailV2') {
                hqDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalHQRegXRProductDetailV2_Front') {
                hqRegXRDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalGMProductDetailV2_front') {
                gmDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalRDProductDetailV2_front') {
                rdDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalWebplianceProductDetail') {
                webplianceDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalAuthoringProductDetailV2') {
                authoringDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalFaxBackProductDetailV2') {
                faxbackDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalServicesProductDetailV2') {
                libraryBuildDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalIMProductDetailV2_front') {
                imDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalWTIndustryPackages') {
                trainingIndustryDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalWTProductDetailV2_front') {
                trainingDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalWTPremiumDetail') {
                trainingIndustryPremiumDetail.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalSTKProductDetailV2') {
                stk.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalETKProductDetailV2') {
                etk.add(qp);
            }
            if (qp.Product__r.Product_Page__c == 'proposalChemicalSpillProductDetailV2') {
                spillCenter.add(qp);
            }
            if (qp.Product_Print_Name__c == 'HQ Account') {
                hq.add(qp);
            }
            if (qp.Product_Print_Name__c == 'HQ RegXR Account') {
                regXr.add(qp);
            }
            if (qp.Product_Print_Name__c == 'GM Account') {
                gm.add(qp);
            }
            if (qp.Product_Print_Name__c == 'GM Pro') {
                gmPro.add(qp);
            }
            if (qp.Product__r.ODT__c == true) {
                odt.add(qp);
            }
            if (qp.Print_Group__c == 'MSDS/ Chemical Management') {
                chemItems.add(qp);
            }
           /* if (qp.Name__c == 'Authoring - Label Authoring') {
                authLabel.add(qp);
            }
            if (qp.Name__c == 'Authoring - Alias SDS Creation') {
                authAliasSds.add(qp);
            }
            if (qp.Name__c == 'Authoring - MSDS Authoring') {
                authMsds.add(qp);
            }
            if (qp.Name__c == 'Authoring - Regulatory Consulting') {
                authReg.add(qp);
            }
            if (qp.Name__c == 'Authoring - Translation Service') {
                authTrans.add(qp);
            }
            if (qp.Name__c == 'Authoring - CAS # Retrieval Fee') {
                authCas.add(qp);
            }
            if (qp.Name__c == 'Authoring - Complex Formula Fee') {
                authComplexFormula.add(qp);
            }
            if (qp.Name__c == 'Authoring - Compliance Review') {
                authComplianceReview.add(qp);
            }
            if (qp.Name__c == 'Authoring - Custom Template') {
                authCustomTemplate.add(qp);
            }
            if (qp.Name__c == 'Authoring - Grouping Analysis Services') {
                authGroupingAnalysis.add(qp);
            }
            if (qp.Name__c == 'Authoring - Laboratory Analysis Results Review') {
                authLabAnalysis.add(qp);
            }
            if (qp.Name__c == 'Authoring - Custom Phrase Library') {
                authCustomPhrase.add(qp);
            }
*/
            if (qp.Print_Group__c == 'Services') {
                servicesItems.add(qp);
            }
            if (qp.Name__c.startsWithIgnoreCase('Indexing Field')) {
                specialIndexingItems.add(qp);
            }
            if (qp.Name__c.startsWithIgnoreCase('Ongoing Indexing Field')) {
                ongoingIndexingItems.add(qp);
            }
            if (qp.Print_Group__c == 'Additional Compliance Solutions') {
                adcItems.add(qp);
            }
            if (qp.Print_Group__c == 'Options') {
                oItems.add(qp);
            }
            if (qp.Print_Group__c != 'Authoring') {
                proposedItems.add(qp);
            }
            if (qp.Print_Group__c == 'Authoring') {
                authoringItems.add(qp);
            }
            if (qp.Product__r.Incident_Management__c == true) {
                imItems.add(qp);
            }
            if (qp.Product__r.Incident_Management__c == true && qp.Product__r.Proposal_Print_Group__c == 'IM Main') {
                imMain.add(qp);
            }
            if (qp.Product__r.Incident_Management__c == true && qp.Product__r.Proposal_Print_Group__c == 'IM Options') {
                imOptions.add(qp);
            }
            if (qp.Product__r.Incident_Management__c == true && qp.Product__r.Proposal_Print_Group__c == 'IM Services') {
                imServices.add(qp);
            }
        }
        SYSTEM.DEBUG('GM Account = '+gm.size());
    }
    
    public Attachment getmyfile() {
        myfile = new Attachment();
        return myfile;
    }
    
    public void setLogo() {
        if (numLogo > 0) {
            this.logo = [SELECT id FROM Attachment WHERE ParentId = :theQuote.id AND Name LIKE '%logo%'];
            showLogo = true;
        } else {
            this.logo = new Attachment();
            showLogo = false;
        }
    }
    
    public void addLogo() {
        try {   
            Attachment a = new Attachment(ParentId = theQuote.id, Name = myfile.Name, Body = myfile.Body);
            insert a;
            this.logoMessage = 'Your Logo Has Been Added!'; 
        } catch(Exception e) {
            this.logoMessage = e.getMessage();
        }
    }
    
    public void setLogoOnAdd() {
        if (theQuote.id != null) {
            this.logo = [SELECT id FROM Attachment WHERE ParentId = :theQuote.id AND Name LIKE '%logo%'];
            showLogo = true;
        } else {
            this.logo = new Attachment();
            showLogo = false;
        }
    }
    
    public PageReference saveLogo() {
        addLogo();
        setLogoOnAdd();
        return null;
    }
    
    public void queryPrintOptions() {
        try {
            id id = System.currentPageReference().getParameters().get('id');
            po = [SELECT Page_ROI_Figures__c, Page_ROI_Benefits__c, Page_Key_Compliance__c, Page_HazCom_2012_Revision__c, Page_GHS_Adoption_Timeline__c, Page_Why_Go_Electronic__c, Page_Managing_the_GHS_Transition__c 
                  FROM Quote__c WHERE id = :id];
        } catch(Exception e) {
            ApexPages.addMessages(e);
        }  
    }
    
    public User getUser() {
        User u = [SELECT Name, Email, Phone, Direct_Phone_Number__c, Title FROM User WHERE id = :UserInfo.getUserId()];
        return u;
    }
    
    public void getPricingFactor() {
        if (hqProduct.size() > 0) {
            pricingFactor = hqProduct[0];
        } else {
            pricingFactor = null;
        }
    }
    
    public string getFormatProposalDate() {
        return System.today().format();
    }
    
    public SObject getY1AuthTotal() {
        Y1Total = [select SUM(Y1_Total_Price__c)sum FROM Quote_Product__c where Print_Group__c ='Authoring' and Y1_Total_Price__c >0 AND Quote__c=:theQuote.Id ];
        return Y1Total;
    }
    
    public SObject getY2AuthTotal() {
        Y2Total = [select SUM(Y2_Total_Price__c)sum FROM Quote_Product__c where Print_Group__c ='Authoring' and Y2_Total_Price__c >0 AND Quote__c=:theQuote.Id ];
        return Y2Total;
    }
    
    public SObject getY3AuthTotal() {
        Y3Total = [select SUM(Y3_Total_Price__c)sum FROM Quote_Product__c where Print_Group__c ='Authoring' and Y3_Total_Price__c >0 AND Quote__c=:theQuote.Id ];
        return Y3Total;
    }
    
    public SObject getY4AuthTotal() {
        Y4Total = [select SUM(Y4_Total_Price__c)sum FROM Quote_Product__c where Print_Group__c ='Authoring' and Y4_Total_Price__c >0 AND Quote__c=:theQuote.Id ];
        return Y4Total;
    }
    
    public SObject getY5AuthTotal() {
        Y5Total = [select SUM(Y5_Total_Price__c)sum FROM Quote_Product__c where Print_Group__c ='Authoring' and Y5_Total_Price__c >0 AND Quote__c=:theQuote.Id ];
        return Y5Total;
    }
    
    public SObject getY1Total() {
        Y1Total = [select SUM(Y1_Total_Price__c)sum FROM Quote_Product__c where Print_Group__c !='Authoring' and Y1_Total_Price__c >0 AND Quote__c=:theQuote.Id ];
        return Y1Total;
    }
    
    public SObject getY2Total() {
        Y2Total = [select SUM(Y2_Total_Price__c)sum FROM Quote_Product__c where Print_Group__c !='Authoring' and Y2_Total_Price__c >0 AND Quote__c=:theQuote.Id ];
        return Y2Total;
    }
    
    public SObject getY3Total() {
        Y3Total = [select SUM(Y3_Total_Price__c)sum FROM Quote_Product__c where Print_Group__c !='Authoring' and Y3_Total_Price__c >0 AND Quote__c=:theQuote.Id ];
        return Y3Total;
    }
    
    public SObject getY4Total() {
        Y4Total = [select SUM(Y4_Total_Price__c)sum FROM Quote_Product__c where Print_Group__c !='Authoring' and Y4_Total_Price__c >0 AND Quote__c=:theQuote.Id ];
        return Y4Total;
    }
    
    public SObject getY5Total() {
        Y5Total = [select SUM(Y5_Total_Price__c)sum FROM Quote_Product__c where Print_Group__c !='Authoring' and Y5_Total_Price__c >0 AND Quote__c=:theQuote.Id ];
        return Y5Total;
    }
    
    public SObject getsiY1Total() {
        siY1Total = [select SUM(Y1_Total_Price__c)sum FROM Quote_Product__c where Name__c Like 'Indexing Field%' and Y1_Total_Price__c >0 AND Bundle__c = FALSE AND Quote__c=:theQuote.Id];
        return siY1Total;
    }
    
    public SObject getsiY2Total() {
        siY2Total = [select SUM(Y2_Total_Price__c)sum FROM Quote_Product__c where Name__c Like 'Indexing Field%' and Y2_Total_Price__c >0 AND Bundle__c = FALSE AND Quote__c=:theQuote.Id ];
        return siY2Total;
    }
    
    public SObject getsiY3Total() {
        siY3Total = [select SUM(Y3_Total_Price__c)sum FROM Quote_Product__c where Name__c Like 'Indexing Field%' and Y3_Total_Price__c >0 AND Bundle__c = FALSE AND Quote__c=:theQuote.Id ];
        return siY3Total;
    }
    
    public SObject getsiY4Total() {
        siY4Total = [select SUM(Y4_Total_Price__c)sum FROM Quote_Product__c where Name__c Like 'Indexing Field%' and Y4_Total_Price__c >0 AND Bundle__c = FALSE AND Quote__c=:theQuote.Id ];
        return siY4Total;
    }
    
    public SObject getsiY5Total() {
        siY5Total = [select SUM(Y5_Total_Price__c)sum FROM Quote_Product__c where Name__c Like 'Indexing Field%' and Y5_Total_Price__c >0 AND Bundle__c = FALSE AND Quote__c=:theQuote.Id ];
        return siY5Total;
    }
    
    public SObject getoiY1Total() {
        oiY1Total = [select SUM(Y1_Total_Price__c)sum FROM Quote_Product__c where Name__c Like 'Ongoing Indexing Field%' and Y1_Total_Price__c >0 AND Bundle__c = FALSE AND Quote__c=:theQuote.Id ];
        return oiY1Total;
    }
    
    public SObject getoiY2Total() {
        oiY2Total = [select SUM(Y2_Total_Price__c)sum FROM Quote_Product__c where Name__c Like 'Ongoing Indexing Field%' and Y2_Total_Price__c >0 AND Bundle__c = FALSE AND Quote__c=:theQuote.Id ];
        return oiY2Total;
    }
    
    public SObject getoiY3Total() {
        oiY3Total = [select SUM(Y3_Total_Price__c)sum FROM Quote_Product__c where Name__c Like 'Ongoing Indexing Field%' and Y3_Total_Price__c >0 AND Bundle__c = FALSE AND Quote__c=:theQuote.Id ];
        return oiY3Total;
    }
    
    public SObject getoiY4Total() {
        oiY4Total = [select SUM(Y4_Total_Price__c)sum FROM Quote_Product__c where Name__c Like 'Ongoing Indexing Field%' and Y4_Total_Price__c >0 AND Bundle__c = FALSE AND Quote__c=:theQuote.Id ];
        return oiY4Total;
    }
    
    public SObject getoiY5Total() {
        oiY5Total = [select SUM(Y5_Total_Price__c)sum FROM Quote_Product__c where Name__c Like 'Ongoing Indexing Field%' and Y5_Total_Price__c >0 AND Bundle__c = FALSE AND Quote__c=:theQuote.Id ];
        return oiY5Total;
    }
    
    public void setPricingSections() {
        getPricingFactor();
        setShowChem();
        setShowServices();
        setShowSpecialIndexing();
        setShowIMMain();        
        setShowIMServices();        
        setShowIMOptions();
        setShowOngoingIndexing();
        setShowADC();
        setShowOptions();
        setShowAuthoring();
        setShowProposed();
        setShowhqDetail();
        setShowhqRegXRDetail();
        setShowgmDetail();
        setShowrdDetail();
        setShowwebplianceDetail();
        setShowauthoringDetail();
        setShowfaxbackDetail();
        setShowlibraryBuildDetail();
        setShowimDetail();
        setShowtrainingDetail();
        setShowTrainingIndustryDetail();
        setShowTrainingPremiumDetail();
        setShowSTK();
        setShowETK();
        setShowSpillCenter();
        setLogo();
    }
    
    public void setShowhqDetail() {
        if (hqDetail.size() > 0) {
            showhqDetail = true;
        } else {
            showhqDetail = false;
        }
        SYSTEM.DEBUG('showhqDetail = '+showhqDetail);
    }
    
    public void setShowSpillCenter() {
        if(spillCenter.size()>0 ) {
            showSpillCenter = true;
        }
        else{
            showSpillCenter = false;
        }
    }
    
    public void setShowhqRegXRDetail() {
        if(hqRegXRDetail.size()>0 ) {
            showhqRegXRDetail = true;
        }
        else{
            showhqRegXRDetail = false;
        }
    }
    
    public void setShowgmDetail() {
        if(gmDetail.size()>0 ) {
            showgmDetail = true;
        }
        else{
            showgmDetail = false;
        }
    }
    
    public void setShowrdDetail() {
        if(rdDetail.size()>0 ) {
            showrdDetail = true;
        }
        else{
            showrdDetail = false;
        }
    }
    
    public void setShowwebplianceDetail() {
        if(webplianceDetail.size()>0 ) {
            showwebplianceDetail = true;
        }
        else{
            showwebplianceDetail = false;
        }
    }
    
    public void setShowauthoringDetail() {
        if(authoringDetail.size()>0 ) {
            showauthoringDetail = true;
        }
        else{
            showauthoringDetail = false;
        }
    }
    
    public void setShowfaxbackDetail() {
        if(faxbackDetail.size()>0 ) {
            showfaxbackDetail = true;
        }
        else{
            showfaxbackDetail = false;
        }
    }
    
    public void setShowlibraryBuildDetail() {
        if(libraryBuildDetail.size()>0 ) {
            showlibraryBuildDetail = true;
        }
        else{
            showlibraryBuildDetail = false;
        }
    }
    
    public void setShowimDetail() {
        if(imDetail.size()>0 ) {
            showimDetail = true;
        }
        else{
            showimDetail = false;
        }
    }
    
    public void setShowtrainingDetail() {
        if(trainingDetail.size()>0 ) {
            showtrainingDetail = true;
        }
        else{
            showtrainingDetail = false;
        }
    }
    
    public void setShowTrainingPremiumDetail() {
        if(trainingIndustryPremiumDetail.size()>0 ) {
            showtrainingPremiumDetail = true;
        }
        else{
            showtrainingPremiumDetail = false;
        }
    }
    
    public void setShowTrainingIndustryDetail() {
        if(trainingIndustryDetail.size()>0 ) {
            showTrainingIndustryDetail = true;
        }
        else{
            showTrainingIndustryDetail = false;
        }
    }
    
    public void setShowSTK() {
        if(STK.size()>0 ) {
            showSTK = true;
        }
        else{
            showSTK = false;
        }
    }
    
    public void setShowETK() {
        if(ETK.size()>0 ) {
            showETK = true;
        }
        else{
            showETK = false;
        }
    }
    
    public void setShowChem() {
        if(chemItems.size()>0 ){
            showChem = true;
        }
        else{
            showChem=false;
        }
    }
    
    public void setShowIMMain() {
        if(imMain.size()>0 ){
            showIMMain = true;
        }
        else{
            showIMMain=false;
        }
    }
    
    public void setShowIMOptions() {
        if(imOptions.size()>0 ){
            showIMOptions = true;
        }
        else{
            showIMOptions=false;
        }
    }
    
    public void setShowIMServices() {
        if(imServices.size()>0 ){
            showIMServices = true;
        }
        else{
            showIMServices=false;
        }
    }
    
    public void setShowSpecialIndexing() {
        if(SpecialIndexingItems.size()>0){
            showSpecialIndexing = true;
        }
        else{
            showSpecialIndexing=false;
        }
    }
    
    public void setShowOngoingIndexing() {
        if(OngoingIndexingItems.size()>0){
            showOngoingIndexing = true;
        }
        else{
            showOngoingIndexing=false;
        }
    }        
    
    public void setShowServices() {
        if(servicesItems.size()>0){
            showServices = true;
        }
        else{
            showServices=false;
        }
    }
    
    public void setShowADC() {
        if(adcItems.size()>0){
            showADC = true;
        }
        else{
            showADC=false;
        }
    }
    
    public void setShowOptions() {
        if(oItems.size()>0){
            showOptions = true;
        }
        else{
            showOptions=false;
        }
    }
    
    public void setShowAuthoring() {
        if(authoringItems.size()>0){
            showAuthoring = true;
        }
        else{
            showAuthoring=false;
        }
    }
    
    public void setShowProposed() {
        if(proposedItems.size()>0){
            showProposed = true;
        }
        else{
            showProposed =false;
        }
    }
    
    public void saveChanges() {
        try{   
            update theQuote;
        }
        
        catch(Exception e)
        {
            ApexPages.addMessages(e);
        }
    }
    
    public PageReference backToQuote() {
        PageReference quoteDetail = Page.quoteDetail;
        quoteDetail.setRedirect(true);
        quoteDetail.getParameters().put('id',theQuote.id); 
        return quoteDetail; 
    } 
    

    
    
    
}