public class proposalBuilderUtility {
    
    //takes jsonString, returns true if proposal needs page was selected
    public static boolean containsNeeds(string jsonField){
        if(jsonField != NULL){
            if(jsonField.Contains('Needs Requirements')){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public static decimal returnDisplayValue(decimal bundledTotal, decimal currencyRate){        
        if(bundledTotal != null){
            if(currencyRate != null && currencyRate != 0 ){              
                return (bundledTotal*currencyRate).setScale(2, RoundingMode.HALF_UP);
            }else{
                return bundledTotal;
            }
        }else{ 
            return 0;
        }
    }
    
}