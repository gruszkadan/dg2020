public class contractController {
    private ApexPages.StandardController controller;
    public id contractId {get;set;}
    public Contract__c theContract {get;set;}
    public User theUser {get;set;}
    public User theDirector {get;set;}
    public Account theAccount {get;set;}
    public Contact theContact {get;set;}
    public integer numAtts {get;set;}
    public Contract_Line_Item__c[] shoppingCart {get;set;}
    public Contract_Line_Item__c[] contractItemsThatHaveTerms {get;set;}
    public List<Contract_Line_Item__c> contractItemsWithTerms {get;set;}
    public List<Contract_Line_Item__c> authoringRushItems  {get;set;}
    public ID parentId {get;set;}
    public String pdfName {get;set;}
    public QueueSobject ordersQueue {get;set;}
    public QueueSobject newOrdersQueue {get;set;}
    public Boolean orderSubmitConfirm {get;set;}
    public string environment {get;set;}
    public User u {get;set;}
    date myDate = date.today();
    public List<Id> product2Ids {get;set;}
    public String sId {get;set;}
    public Integer cl {get;set;}
    public Integer COFprevgen {get;set;}
    public String realURL {get;set;}
    public Boolean viewApprovalPage {get;set;}
    public Boolean viewManualBuild {get;set;}
    public Boolean viewOrdersButtons {get;set;}
    public Boolean viewEditButton {get;set;}
    public Boolean viewSendToOrdersButton {get;set;}
    public String shoppingcartList {get;set;}
    public Id profile_director {get;set;}
    public Id profile_manager {get;set;}
    public String selDiscountCode {get;set;}
    private contractCustomSettings contract_helper = new contractCustomSettings();
    private msdsCustomSettings helper = new msdsCustomSettings();
    public enum Priority {Highest, High, Normal, Low, Lowest}
    public string initalApprover {get;set;}
    public string initalApproverSelected {get;set;}
    public contractUtility util {get;set;}
    public string firstUnsignedApprover {get;set;}
    public boolean appSubmitted {get;set;}
    public boolean isApprover {get;set;}
    //ehs additions
    public Attachment myfile;
    public string fileExt {get;set;}
    public integer numEHSAttachments {get;set;}
    public boolean showOrderConfirmation {get;set;}
    public string errorMsg {get;set;}
    public Group Queue {get;set;}
    public EmailTemplate Temp {get;set;}
    //bundling indexing additions
    public boolean hasBundledIndexing {get;set;}
    public decimal sIdIndex {get;set;}
    //wrappers for product types
    //public lineItem[] mWrappers {get;set;}
    // public lineItem[] sWrappers {get;set;}
    //public lineItem[] cWrappers {get;set;}
    //public lineItem[] ehsWrappers {get;set;}
    public event[] latestDemo {get;set;}
    public integer numManagementItems {get;set;}
    public integer numServiceItems {get;set;}
    public integer numComplianceItems {get;set;}
    //view notes record
    public Contract_Line_Item__c viewingItem {get;set;}
    public string viewItemNotesType {get;set;}
    public ProcessInstance[] viewingItemApprovals {get;set;}
    public map<id,ProcessInstance[]> viewingItemApprovalsMap {get;set;}
    public turnOverNoteWrappers[]  turnOverNotes {get;set;}
    public string allProductNames {get;set;}
    public string sendToOrdersError {get;set;}
    public boolean needsSalesTurnOverNotes {get;set;}
    public string productPlatform {get;set;}
    public boolean missingCoverage {get;set;}
    public string errorMessage {get;set;}
    public boolean showOrdersButton {get;set;}
    
    
    public contractController(ApexPages.StandardController stdController) {
        util = new contractUtility();
        needsSalesTurnOverNotes = false; 
        turnOverNotes = new List<turnOverNoteWrappers>();
        contractId = ApexPages.currentPage().getParameters().get('id');
        String submitOrder = ApexPages.currentPage().getParameters().get('o');
        if (ApexPages.currentPage().getParameters().get('order') == '1') {
            showOrderConfirmation = true;
        } else {
            showOrderConfirmation = false;
        }
        this.controller = stdController;
        viewApprovalPage = contract_helper.viewApprovalPage();
        viewManualBuild = contract_helper.viewManualBuild();
        viewOrdersButtons = contract_helper.viewOrdersButtons();
        if (ApexPages.currentPage().getParameters().get('submitted') == '1') {
            appSubmitted = true;
        }
        profile_director = [ SELECT id, profileId FROM User WHERE Role__c = 'Director' AND isActive = TRUE LIMIT 1 ].profileId;
        profile_manager = [ SELECT id, profileId FROM User WHERE Role__c = 'Sales Manager' AND isActive = TRUE LIMIT 1 ].profileId;
        if (contractId != null ) {
            queryContract();
        } else {
            theContract = (Contract__c)stdController.getRecord();
        }
        
        if(submitOrder == '1'){
            orderSubmitConfirm = true;
        } else {
            orderSubmitConfirm = false;
        }
        if(theContract.Owner.Name != 'Orders Queue' && theContract.Owner.Name != 'New Orders Queue' && theContract.Owner.Type != 'Queue'){   
            theUser= [Select ID, UserType, Email, Sales_Director__c, Department__c, Group__c, Manager.Email from User where ID=: theContract.OwnerId];
        }
        if(theContract.Owner.Name != 'New Orders Queue' && theContract.Owner.Name != 'Orders Queue'  && theUser != null && theUser.Department__c == 'Sales' && theContract.Owner.Type != 'Queue'){
            theDirector=[Select ID, UserType, Email from User where ID=:theUser.Sales_Director__c];
        }
        theAccount = [Select ID, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, Customer_Annual_Revenue__c from Account where ID =:theContract.Account__c];
        theContact = [Select ID, FirstName, LastName, Name from Contact where ID =:theContract.Contact__c];
        
        latestDemo = [Select EndDateTime FROM Event WHERE AccountId = :theAccount.id AND Subject = 'HQ - Demo' AND Event_Status__c = 'Completed' ORDER BY EndDateTime DESC Limit 1];
        
        //send to orders button render
        if(theContract.Status__c != 'Send to Orders'){
            showOrdersButton = true;
        }else{
            showOrdersButton= false;
        }
        // query the line items
        loadItems(false);
        //Determine the Product Platforms on the Contract
        productPlatform = productUtility.findProductSuites(product2Ids);
        missingCoverage = false;
        
        //check coverage   
        if((productPlatform.contains('MSDS Management') || productPlatform.contains('On-Demand Training')|| productPlatform.contains('MSDS Authoring'))
           && (theContract.Num_of_Locations__c == Null || theContract.Coverage__c == null)){
               missingCoverage = true;
           }
        if(productPlatform.contains('EHS Management') && (theContract.EHS_Coverage__c == Null || theContract.EHS_Coverage_Type__c == null)){
            missingCoverage = true;
        }
        if(productPlatform.contains('Ergonomics') && (theContract.Ergo_Coverage__c == Null || theContract.Ergo_Coverage_Type__c == null)){
            missingCoverage = true;
        }        
        
        numAtts = [select count() from attachment where parentid=:theContract.Id AND Name LIKE '%.pdf' ];
        numEHSAttachments = [select count() from attachment where parentid=:theContract.Id AND Name LIKE 'EHS%'];
        ordersQueue = [Select QueueId From QueueSobject where SobjectType='Contract__c' and Queue.Name ='Orders Queue'];
        newOrdersQueue = [Select QueueId From QueueSobject where SobjectType='Contract__c' and Queue.Name ='New Orders Queue'];
        u = [Select id, Name, LastName, UserType, Group__c, Department__c, Role__c, Email, ProfileId, Profile.Name from User where id =: UserInfo.getUserId() LIMIT 1];
        
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D300000001HEfEAM'){
            environment = 'Production';
        } else {
            environment ='Sandbox';
        }
        realURL = URL.getSalesforceBaseUrl().toExternalForm()+'/';
        
        // Edit button logic
        viewEditButton = TRUE;
        if( theContract.Status__c == 'Sent to Orders' || theContract.Status__c == 'Pending Orders' || theContract.Status__c == 'Order Processed' ) {
            viewEditButton = FALSE;
        }
        if( theContract.Contract_Type__c == 'New' && theContract.Status__c == 'Pending Approval' ) {
            if( viewApprovalPage == TRUE ) {
                viewEditButton = TRUE;
            }
            if( viewApprovalPage == FALSE ) {
                viewEditButton = FALSE;
            }
        }
        // Send to Orders Button Logic
        viewSendToOrdersButton = FALSE;
        if (theContract.Contract_Type__c=='New' && theContract.Status__c =='Approved') {
            viewSendToOrdersButton = TRUE;            
        } else if (theContract.Contract_Type__c=='New' && theContract.Status__c =='Out for Signature') {
            viewSendToOrdersButton = TRUE;            
        } else if (theContract.Contract_Type__c=='New' && theContract.Status__c =='Signed') {
            viewSendToOrdersButton = TRUE;            
        } else {
            viewSendToOrdersButton = FALSE;  
        }  
        
        if (theContract.Status__c == 'Pending Approval') {
            checkApprover();
        }
        viewingItem = new Contract_Line_Item__c();
        viewingItemApprovals = new list<ProcessInstance>();
        viewingItemApprovalsMap = new map<id,ProcessInstance[]>();
        checkForQuoteApprovalNotes();
    }
    
    public Approval__c getApp() {
        Approval__c[] app = [SELECT id, Name
                             FROM Approval__c
                             WHERE Contract__c = : contractId];
        if (app.size()>0) {
            return app[0];
        } else {
            return null;
        }
    }
    
    public void queryContract() {
        string addFields = 'Owner.Name, Owner.Type, Sales_Rep__r.Name, Account__r.Name, Account__r.Enterprise_Sales__c, Related_Contract_Account__r.Name, Promotion__r.Name, Promotion__r.Code__c, Account__r.Customer_Annual_Revenue__c, Account__r.AdminID__c ';
        theContract = (Contract__c)new dynamicQuery().query('Contract__c', addFields, 'id = \''+contractId+'\'');
    }
    
    // ------------------------------ REFRESH FROM OPPOTYUNITY METHODS ------------------------------ //
    // button on contract detail page to refresh line items
    public PageReference refreshFromOpportunity() {
        Opportunity refOpp = [SELECT id, Currency__c, Licensing_Covered_by_Another_Location__c, Multiple_Location_Indexing__c, Currency_Rate__c, Currency_Rate_Date__c, Implementation_Model__c, Subscription_Model__c
                              FROM Opportunity
                              WHERE id = :theContract.Opportunity__c LIMIT 1];
        
        
        // call the line item conversion class and pass the contract to it
        contractUtility util = new contractUtility();
        List<id> OppIds = new list<Id>();
        OppIds.add(theContract.Opportunity__c);
        util.lineItemConversion(theContract, OppIds); 
        
        queryContract();
        // once the line item conversion controller runs, refresh the items
        loadItems(true);                                                
        
        //update fields on the contract
        
        //if did not and now does have EHS, set teh milestones
        if(theContract.EHS_Implementation_Model__c != refOpp.Implementation_Model__c){
            if(refOpp.Implementation_Model__c != NULL){
                ApexPages.currentPage().getParameters().put('JSON', null);
                ApexPages.currentPage().getParameters().put('ImplementationModel', refOpp.Implementation_Model__c);
                util.createMilestoneTables();
                theContract.Billing_Milestones_JSON__c  = util.saveBillingMilestonesJSON(false, theContract.Billing_Milestones_JSON__c);  
            }else{
                theContract.Billing_Milestones_JSON__c  =  null;
            }
        }
        
        theContract.Subscription_Model__c = refOpp.Subscription_Model__c;
        theContract.EHS_Implementation_Model__c = refOpp.Implementation_Model__c;
        theContract.Billing_Currency__c = refOpp.Currency__c;
        theContract.Billing_Currency_Rate__c = refOpp.Currency_Rate__c;
        theContract.Billing_Currency_Rate_Date__c = refOpp.Currency_Rate_Date__c;
        theContract.Licensing_Covered_by_Another_Location__c = refOpp.Licensing_Covered_by_Another_Location__c;
        theContract.Multiple_Location_Indexing__c = refOpp.Multiple_Location_Indexing__c;
        theContract.Modified_Down_Payment_Percent__c = util.updateModifiedDownPaymentPercent(shoppingCart, theContract); 
        update theContract;
        
        //check approval and return to contract page
        util.approvalCheck(theContract, false, productPlatform);
        PageReference pr = new PageReference('/'+theContract.id);
        return pr.setRedirect(true);
    }
    
    
    
    public void loadItems(boolean refresh) {
        if (refresh == true) {
            shoppingCart.clear();
        }
        
        String SobjectApiName = 'Contract_Line_Item__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String additionalFields = ', Product__r.ID, Product__r.Name, Product__r.Contract_Print_Group__c, Product__r.ProductCode, Product__r.Contract_Product_Name__c, Product__r.Product_Print_Grouping__c, Product__r.Product_Platform__c, Product__r.Contract_Product_Type__c, Product__r.Sales_Product_Grouping__c, Product__r.Family, Product__r.Product_Suite__c, Product__r.Requires_Sales_Turnover_Notes__c, Product__r.Contract_End_Date_Type__c, Product__r.Contract_Start_Date_Type__c, (SELECT id FROM ProcessInstances) ';
        String OrderBy = ' Order By Group_Name__c ASC, Group_Parent_ID__c ASC, Group_ID__c ASC NULLS FIRST, Name__c ASC';
        String query = 'select ' + commaSepratedFields + additionalFields +' from ' + SobjectApiName + ' where Contract__c=\''+theContract.Id+'\' '+ OrderBy;
        // return the contract items
        shoppingCart = Database.query(query);
        authoringRushItems = new List<Contract_Line_Item__c>();
        product2Ids = new List<Id>();
        shoppingcartlist = '';
        for (Contract_Line_Item__c sc : shoppingcart) {
            shoppingcartlist = shoppingcartlist+sc.Name__c+'<br/>';
            if (sc.Auth_Rush_Quote__c == TRUE) {
                authoringRushItems.add(sc);
            }
        }
        contractItemsWithTerms = new List<Contract_Line_Item__c>();
        boolean updThis = false;
        for(Contract_Line_Item__c ct : shoppingcart) {
            product2Ids.add(ct.Product__r.ID);
            allProductNames += ct.Name__c + ';';
            
            if(ct.Product__r.Requires_Sales_Turnover_Notes__c){
                needsSalesTurnOverNotes = true;
            }
            
            if (ct.Contract_Terms__c != null) {
                if (ct.Contract_Terms_Char_Count__c == null || ct.Contract_Terms_Char_Count__c == 0) {
                    ct.Contract_Terms_Char_Count__c = ct.Contract_Terms__c.length();
                    ct.Original_Char_Count__c = ct.Contract_Terms__c.stripHtmlTags().length();
                    updThis = true;
                }
                contractItemsWithTerms.add(ct);
            }
        }
        if (refresh == true && updThis == true) {
            update contractItemsWithTerms;
        }
    }
    // ------------------------------ / REFRESH FROM OPPOTYUNITY METHODS ------------------------------ //
        
    public Attachment getmyfile() {
        myfile = new Attachment();
        return myfile;
    }
    
    public Attachment[] getAtts() {
        Attachment[] atts = new list<Attachment>();
        atts.addAll([SELECT id, Name, ContentType, LastModifiedDate, CreatedBy.Name, IsPrivate, Description
                     FROM Attachment
                     WHERE ParentId = : theContract.id ORDER BY CreatedDate DESC]);
        return atts;
    }    
    
    public void checkApprover() {
        ProcessInstanceWorkItem piwi = [SELECT id, Actorid, OriginalActorid
                                        FROM ProcessInstanceWorkItem 
                                        WHERE ProcessInstance.TargetObjectid = :theContract.id];
        User currentApprover = [SELECT id, Name, Manager.id, SmallPhotoURL FROM User WHERE id = :piwi.Actorid LIMIT 1];
        if (u.id == piwi.Actorid) {
            isApprover = true;
        } else {
            if (u.id == currentApprover.Manager.id) {
                isApprover = true;
            } else {
                isApprover = false;
            }
        }
    }
    
    public void acceptOrderChanges () {
        if (theContract.Id != null) {
            try {
                Contract__c theContract = [select Id, OwnerID, Status__c from Contract__c where Id=:theContract.Id];
                theContract.OwnerID = u.Id;
                theContract.Status__c = 'Pending Orders';
                update theContract;
            }
            catch(Exception e){
                ApexPages.addMessages(e);
            }
        } 
    }
    
    public void processOrderChanges () {
        if (theContract.Id != null) {
            try {
                Contract__c theContract = [select Id, OwnerID, Status__c from Contract__c where Id=:theContract.Id];
                theContract.Status__c = 'Order Processed';
                update theContract;
            }
            catch(Exception e){
                ApexPages.addMessages(e);
            }
        } 
    }
    
    public void submitCOFChanges () {
        if (theContract.Id != null ) {
            try {
                Contract__c theContract = [select Id, OwnerID, Order_Submitted_By__c, Order_Submitted_Date__c, Account__r.Customer_Annual_Revenue__c, Status__c from Contract__c where Id=:theContract.Id];
                theContract.OwnerID = ordersQueue.QueueId;
                theContract.Status__c = 'Sent to Orders';
                theContract.Order_Submitted_By__c = theUser.id;
                theContract.Order_Submitted_Date__c = myDate;
                update theContract;
            }
            catch(Exception e){
                ApexPages.addMessages(e);
            }
        } 
    }
    
    public PageReference acceptOrder() {
        acceptOrderChanges();
        PageReference contract_detail = Page.contract_detail;
        contract_detail.setRedirect(true);
        contract_detail.getParameters().put('id',theContract.Id); 
        return contract_detail;                 
    }
    
    public PageReference processOrder() {
        processOrderChanges();
        PageReference contract_detail = Page.contract_detail;
        contract_detail.setRedirect(true);
        contract_detail.getParameters().put('id',theContract.Id); 
        return contract_detail;                 
    }
    
    public PageReference submitCOFtoOrders() {
        this.controller.save();
        submitCOFChanges();
        PageReference contract_detail = Page.contract_detail;
        contract_detail.setRedirect(true);
        contract_detail.getParameters().put('id',theContract.Id);
        contract_detail.getParameters().put('o','1'); 
        return contract_detail;                 
    }   
    
    public PageReference backToContract(){
        PageReference contract_detail = Page.contract_detail;
        contract_detail.setRedirect(true);
        contract_detail.getParameters().put('id',theContract.id); 
        return contract_detail; 
    }
    
    public PageReference sendToOrders(){
        PageReference contract_send_to_orders = Page.contract_send_to_orders;
        contract_send_to_orders.setRedirect(true);
        contract_send_to_orders.getParameters().put('id',theContract.id); 
        return contract_send_to_orders; 
    }
    
    public PageReference sendBackToOrders(){
        PageReference contract_note_new = Page.contract_note_new;
        contract_note_new.setRedirect(true);
        contract_note_new.getParameters().put('CF00N800000057HgX',theContract.Name);
        contract_note_new.getParameters().put('CF00N800000057HgX_lkid',theContract.id);
        contract_note_new.getParameters().put('repUpdate','1');
        contract_note_new.getParameters().put('sfdc.override', '1');
        contract_note_new.getParameters().put('scontrolCaching', '1'); 
        return contract_note_new; 
    }
    
    public PageReference declineOrder(){
        PageReference contract_decline_order = Page.contract_decline_order;
        contract_decline_order.setRedirect(true);
        contract_decline_order.getParameters().put('CF00N800000057HgX',theContract.Name);
        contract_decline_order.getParameters().put('CF00N800000057HgX_lkid',theContract.id);    
        contract_decline_order.getParameters().put('sfdc.override', '1');
        contract_decline_order.getParameters().put('scontrolCaching', '1'); 
        return contract_decline_order.setRedirect(true); 
    }
    
    
    public void contractFee() {
        util.contractFee(theContract, theContract.Product_Group__c);
    }
    
    public void countChars() {
        for(Contract_Line_Item__c cI : contractItemsWithTerms) {
            cI.Contract_Terms_Char_Count__c = cI.Contract_Terms__c.length();
        }
        update contractItemsWithTerms;
        contractItemsThatHaveTerms = [Select ID, Product__r.Contract_Print_Group__c, Sort_Order__c, Contract_Sort_Order__c, Name__c, Name, Flow_Referance_Name__c, Contract_Terms_Char_Count__c,
                                      Contract_Terms__c, Product__r.Contract_End_Date_Type__c, Product__r.Contract_Start_Date_Type__c, Auth_Expected_End_Date__c
                                      FROM Contract_Line_Item__c where Contract__c=:theContract.Id and Contract_Terms_Char_Count__c>0 order By Contract_Sort_Order__c ASC];
    }
    
    public PageReference contractEdit(){
        PageReference contractEdit = Page.contract_edit;
        contractEdit.setRedirect(true);
        contractEdit.getParameters().put('id',theContract.id);
        return contractEdit; 
    }
    
    public PageReference contractEditHumantech(){
        PageReference contractEditHumantech = Page.contract_edit_humantech;
        contractEditHumantech.setRedirect(true);
        contractEditHumantech.getParameters().put('id',theContract.id);
        return contractEditHumantech; 
    }
    
    public void clearErrors(){
        errorMessage = null;
    }

    public PageReference sendToOrdersErgo(){
        errorMessage = null;       

        //setting the validation condition
        if(theContract.A2_Suggested_URL__c == null){
            errorMessage = 'Please Fill In The Required Field';
            return null;
        }else{
             //query for email template
            Temp = [SELECT Id from EmailTemplate WHERE DeveloperName = 'Ergonomics_Orders'];
        
             //Query for Ergonomics queue
            Queue = [SELECT Id, Type from Group where DeveloperName = 'Ergonomics_Orders_Queue'];

            theContract.Status__c = 'Sent to Orders';
            theContract.Order_Submitted_By__c = userInfo.getUserId();
            theContract.Order_Submitted_Date__c = date.today();
            update theContract;
            
            //Initializing new message
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            If(environment == 'Sandbox'){
                message.toaddresses = new string[] {'salesforceops@ehs.com'};
                    } else if(environment == 'Production'){
                        message.toaddresses = new string[] {'humantechhelp@ehs.com', 'agreements@ehs.com', 'humantechcs@ehs.com', 'klupo@ehs.com', theUser.Manager.Email};
                            if(theDirector != null){
                                message.toaddresses.add(theDirector.Email);
                            }
                    }
            message.setWhatId(theContract.Id);
            message.setTemplateId(Temp.Id);
            message.setTargetObjectId(theContract.OwnerId);
            message.setSaveAsActivity(false);
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            
            theContract.OwnerId = Queue.Id;
            update theContract;
            
        }
        PageReference saveSubmit = Page.contract_detail;
        saveSubmit.getParameters().put('id', theContract.id);
        saveSubmit.setRedirect(true);
        return saveSubmit;
    }
    
    //Method to update status to Approved
    public pageReference updateStatus(){
        pageReference pr = null;
        theContract.Status__c = 'Approved';
        try{
            update theContract;
            pr = page.contract_detail;
            pr.getParameters().put('Id', contractId);
            return pr.setredirect(true);
        }
        catch(System.DMLexception e) {
            apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR,'Please try again or contact Salesforce Ops'));
        }
        return pr;
    }
    
    
    //runs when user presses "Submit" on the choose first approver modal
    public PageReference saveSubmitApproval() {
        string cmts = '';
        if (theContract.Approval_Submission_Comments__c != '') {
            cmts = theContract.Approval_Submission_Comments__c;
        } else {
            theContract.Approval_Submission_Comments__c = 'No submission comments given';
            cmts = theContract.Approval_Submission_Comments__c;
        }
        update theContract;
        if (!test.isRunningTest()) {
            //call for approval
            util.approvalCheck(theContract, true, productPlatform);
        }
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments(cmts);
        req1.setObjectid(theContract.id);
        req1.setNextApproverIds(new Id[] {theContract.First_Approver__c});
        if (theContract.Contract_Type__c == 'New') {
            req1.setProcessDefinitionNameOrId('Contract_Approval_R9');
        } else {
            req1.setProcessDefinitionNameOrId('Retention_Contract_Approval_V2');
        }
        Approval.ProcessResult result = Approval.process(req1);
        PageReference saveSubmitApproval = Page.contract_detail;
        saveSubmitApproval.getParameters().put('id', theContract.id);
        saveSubmitApproval.getParameters().put('submitted', '1');
        saveSubmitApproval.setRedirect(true);
        return saveSubmitApproval;
    }
    
    public PageReference manualBuild(){
        PageReference contractEdit = Page.contract_manual_build;
        contractEdit.setRedirect(true);
        contractEdit.getParameters().put('id',theContract.id);
        return contractEdit; 
    } 
    
    public PageReference sendToOrder2() {
        PageReference contract_detail = Page.contract_detail;
        contract_detail.setRedirect(true);
        contract_detail.getParameters().put('id',theContract.Id);
        contract_detail.getParameters().put('o','1'); 
        
        //set contract fields
        theContract.Status__c = 'Sent to Orders';
        theContract.Order_Submitted_By__c = u.Id;
        theContract.Order_Submitted_Date__c = date.TODAY();
        theContract.Discount_Code__c = selDiscountCode;
        theContract.Sales_Turnover_Notes__c = saveTurnOverNotes();
        update theContract;
        
        string subject = '';
        boolean highPriority = false;
        boolean authoringRush = false;
        if (authoringRushItems.size() > 0) {
            subject = 'RUSH New Order - Contract #'+theContract.Name;
            highPriority = true;
        } else {
            subject = 'New Order - Contract #'+theContract.Name;
        }
        if (authoringRushItems.size() > 0) {
            authoringRush = true;
        }
        
        // send to orders email
        contractUtility.newOrderSubmissionEmail(environment, theContract, subject, highPriority, authoringRush, shoppingCart);
        
        // Assign to queue
        if (theContract.Contract_Type__c == 'Renewal') {
            theContract.OwnerId = OrdersQueue.QueueId;
        } else {
            theContract.OwnerId = newOrdersQueue.QueueId;
        }
        update theContract;
        
        return contract_detail; 
    }
    
    
    public PageReference gotoReview(){
        if( theContract.Unsigned_Contract__c == FALSE )
        {
            PageReference pr = Page.contractReview;
            pr.getParameters().put('id', theContract.Id);
            pr.getParameters().put('process', '1');
            pr.setRedirect(true);
            return pr;
        }
        else
        {
            PageReference pr = Page.contractReview;
            pr.getParameters().put('id', theContract.Id);
            pr.getParameters().put('process', '2');
            pr.setRedirect(true);
            return pr;
        }
    }
    
    public List<SelectOption> getDiscountCodes()  { 
        List<SelectOption> o = new List<SelectOption>();      
        Schema.DescribeFieldResult fieldResult = Contract__c.Discount_Code__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry entry : picklistEntries )
        {
            o.add( new SelectOption( entry.getValue() , entry.getLabel() ) ); 
        }
        return o;
    }
    
    public void unsignedContractCheck() {
        theContract.Unsigned_Contract__c = false;
        createNotes();
    }
    
    public SelectOption[] getInitalContractApprovers() {
        SelectOption[] ops = new list<SelectOption>();
        //set to hold user ids
        set<id> userIds = new set<id>();
        //set to hold profile ids
        set<id> profileIds = new set<id>();
        ops.add(new SelectOption('', '-- Select Approver --', false));
        //loop through contract custom settings and find either sales or retention approvers
        for (contractCustomSettings__c ccs : [SELECT id, Name, SetupOwnerId, Initial_Contract_Approver_Sales__c, Initial_Contract_Approver_Retention__c FROM contractCustomSettings__c]) {
            //approvers for sales contracts
            if (theContract.Contract_Type__c == 'New') {
                if (ccs.Initial_Contract_Approver_Sales__c) {
                    if (ccs.Name.contains('User')) {
                        userIds.add(ccs.SetupOwnerId);
                    } else if (ccs.Name.contains('Profile')) {
                        profileIds.add(ccs.SetupOwnerId);
                    }
                }
                
            } else {
                //approvers for renewal contracts
                if (ccs.Initial_Contract_Approver_Retention__c) {
                    if (ccs.Name.contains('User')) {
                        userIds.add(ccs.SetupOwnerId);
                    } else if (ccs.Name.contains('Profile')) {
                        profileIds.add(ccs.SetupOwnerId);
                    }
                }
            }
        }
        //query our approvers and add them to the select options list
        for (User app : [SELECT id, Name 
                         FROM User
                         WHERE isActive = true 
                         AND (id IN : userIds
                              OR ProfileId IN : profileIds)
                         ORDER BY Name ASC]) {
                             ops.add(new SelectOption(app.id, app.Name));
                         }
        
        return ops;
    }
    
    public PageReference markDead(){
        theContract.Status__c = 'Dead';
        update theContract;
        PageReference pr = Page.contract_detail;
        pr.getParameters().put('id', theContract.Id);
        pr.setRedirect(true);
        return pr;
    }
    
    public void checkForQuoteApprovalNotes(){
        string[] qpIds = new list<string>();
        if(shoppingCart.size() > 0){
            for(Contract_Line_Item__c item:shoppingCart){
                if(item.qpID__c != null){
                    qpIds.add(item.qpID__c);
                }
            }
            Quote_Product__c[] approvedQuoteProducts = [select id from Quote_Product__c where id in:qpIds and Approval_Status__c = 'Approved'];
            viewingItemApprovals = [SELECT CompletedDate, ElapsedTimeInDays, ElapsedTimeInHours, ElapsedTimeInMinutes, Id, ProcessDefinitionId, Status, SubmittedById, TargetObjectId,
                                    (SELECT Id, StepStatus, ActorId, Actor.Name, OriginalActorId, CreatedDate, OriginalActor.Name, Comments FROM StepsAndWorkitems WHERE Comments != null ORDER BY CreatedDate DESC, Id DESC)
                                    FROM ProcessInstance 
                                    WHERE TargetObjectId in:approvedQuoteProducts 
                                    ORDER BY CompletedDate DESC
                                    NULLS FIRST];
            for(String qpId:qpIds){
                ProcessInstance[] pis = new list<ProcessInstance>();
                for(ProcessInstance approval:viewingItemApprovals){
                    if(approval.TargetObjectId == qpId){
                        pis.add(approval);
                    }
                }
                viewingItemApprovalsMap.put(qpId,pis);
            }
        }
    }
    
    public void viewNotes(){
        string viewItemId = ApexPages.currentPage().getParameters().get('viewItemId');
        viewItemNotesType = ApexPages.currentPage().getParameters().get('viewItemNotesType');
        for(Contract_Line_Item__c item:shoppingCart){
            if(item.id == viewItemId){
                viewingItem = item;
            }
        }
    }
    
    public void createNotes(){
        if(needsSalesTurnOverNotes && theContract.Contract_Type__c == 'New'){            
            turnOverNotes = new List<turnOverNoteWrappers>();
            Sales_Turnover_Note__mdt[] salesMetadata = [Select Products__c, Question__c, Question_for_Other_Products__c, Required__c, Sort_Order__c, Required_Products__c from Sales_Turnover_Note__mdt Order By Sort_Order__c ASC];
            for(Sales_Turnover_Note__mdt notes : salesMetadata){
                boolean proceed = false;
                if(notes.Required_Products__c != Null){    
                    for(string prdt : notes.Required_Products__c.split(';')){
                        if(allProductNames.contains(prdt)){
                            proceed = true; 
                            break;
                        }
                    }
                }
                if(proceed){
                    if(notes.Products__c != null){
                        boolean containsSpecifiedProducts = false;
                        for(string pdtName: notes.Products__c.split(';')){
                            if(allProductNames.contains(pdtName)){
                                containsSpecifiedProducts = true;
                                break;
                            }                        
                        }
                        
                        if(containsSpecifiedProducts){
                            turnOverNotes.add(new turnOverNoteWrappers(notes, notes.Question__c));
                        }else if (notes.Question_for_Other_Products__c != Null){
                            turnOverNotes.add(new turnOverNoteWrappers(notes, notes.Question_for_Other_Products__c));
                        }
                    }
                } 
            }
        }
    }
    
    //concat questions and answers
    public string saveTurnOverNotes(){
        string returnS = '';
        for(turnOverNoteWrappers wrap: turnOverNotes){
            //returnS += '<b>' + wrap.STN.Question__c + '</b><br/> ' + wrap.input + '<br/> <br/>';    
            returnS += '<b>' + wrap.question + '</b><br/> ';
            if(wrap.input != null && wrap.input != ''){
                returnS +=wrap.input + '<br/> <br/>'; 
            }else{
                returnS +=' No response provided. <br/> <br/>'; 
            }
        }        
        return returnS;
    }
    
    //sales turnover validation
    public boolean hasValidTurnOverNotes(){
        sendToOrdersError = null;
        boolean isValid = true;
        for(turnOverNoteWrappers STON: turnOverNotes){
            if(STON.STN.required__c && (STON.input == Null || STON.input == '')){
                isValid = false;
                break;
            }
        }
        return isValid;
    }
    
    
    //sales turnover note wrappers
    public class turnOverNoteWrappers{
        public string question {get;set;} 
        public string input {get;set;} 
        public Sales_Turnover_Note__mdt STN {get;set;}
        
        public turnOverNoteWrappers(Sales_Turnover_Note__mdt xSTN, string xQuestion){
            STN = xSTN;  
            question = xQuestion;
        }        
    }
    
    
    public Pagereference MarkContractUnderReview(){
        pageReference pr = null;
        thecontract.status__c = 'Under Review';
        try{
            update theContract;
            pr = page.contract_detail;
            pr.getParameters().put('Id', contractId);
            return pr.setredirect(true);
        }
        catch(System.DMLexception e) {
            apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR,'Please try again or contact Salesforce Ops'));
        }
        //return null when catch
        return pr;
    }
}