@isTest
private class testCaseController {
    
    static testMethod void caseController_test1() {
        
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //test case
        Case newCase = new Case();
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;  
        newCase.AccountId = newAcct.id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Subject = 'Test';
        newCase.Type = 'CC Support';
        newCase.Num_Of_Case_Issues__c = 2;
        newCase.Origin = 'Email';
        newCase.Reason_for_Training__c = 'Refresher Training';
        insert newCase;
        
        Case_Issue__c[] newIssues = new List<Case_Issue__c>();
        for (integer i=0; i<2; i++) {
            Case_Issue__c newIssue = new Case_Issue__c();
            newIssue.Associated_Case__c = newCase.id;
            newIssue.Product_In_Use__c = 'GM';
            newIssue.Product_Support_Issue__c = 'GM Support Issue';
            newIssue.Product_Support_Issue_Locations__c = 'GM Support Location';
            newIssues.add(newIssue);
        }
        insert newIssues;
        
        //test note
        Case_Notes__c newNote = new Case_Notes__c();
        newNote.Case__c = newCase.Id;
        newNote.Notes_Type__c = 'Call - Outbound';
        newNote.Call_Duration__c = 1;
        newNote.Issue__c ='TEST';
        insert newNote;

        
        
        //set initial params
        ApexPages.currentPage().getParameters().put('id', newCase.id);
        ApexPages.currentPage().getParameters().put('def_account_id', newAcct.id);
        ApexPages.currentPage().getParameters().put('def_contact_id', newCon.id);
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new Case());
        caseController con = new caseController(testController);
        
        
        
        
        con.isNewEmail2Case = true;
        con.showPopup();
        con.prompt();
        con.newCaseNote();
        con.accountUpdatePrompt();
        con.overageOwnerPrompt();
        con.serviceReqestPrompt();
        con.accountMergePrompt();
        con.promptHQDelivery();
        con.promptCSDelivery();
        con.promptAuthDelivery();
        con.saveOnEdit();
        con.save();
        con.addRows();
        con.delWrapper();
        con.getCaseIssues();
        con.newIssueNote();
        con.hideWrappers();
        con.edit();
        
        ApexPages.currentPage().getParameters().put('poptype', 'sendemail');
        ApexPages.currentPage().getParameters().put('emailid', 'null');
        con.sendEMailPopupType();
        con.gotoSendEmail();
        ApexPages.currentPage().getParameters().put('sid', newIssues[0].id);
        con.queryNotes();
        con.resetIssue();
        con.queryAllNotes();
        con.cloneCase();
        
        con.getcaseTypeVals();
        
        ApexPages.currentPage().getParameters().put('ctype', 'Training');
        con.changeCaseType();
        System.assert(con.c.Type == 'Training', 'Expected Training, got '+con.c.Type);
        con.authSendEmail();
        
    }
    
    
    static testMethod void caseController_test2() {
        
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new Case());
        caseController con = new caseController(testController);
        
        con.wrappers.add(new caseController.issuesWrapper(2,''));
        
        con.newCase.Origin = 'Email';
        con.newCase.Type = 'CC Support';
        
        for (integer i=0; i<con.wrappers.size(); i++) {
            con.wrappers[i].getSupportProduct1();
            con.wrappers[i].getSupportProductIssueLocation1();
            con.wrappers[i].getProductSupportIssue1();
            
            con.wrappers[i].prodissuespecific1 = 'Test';
            con.wrappers[i].prodissuelocation1 = 'Test';
            con.wrappers[i].notecomments = 'Test';
            con.wrappers[i].cn.Notes_Type__c = 'Chat - Inbound';
        }
        con.saveCaseIssues();
        con.checkEmail2Case();
        
    }
    
    static testMethod void caseController_test3() {
        
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new Case());
        caseController con = new caseController(testController);
        
        con.newSaveAndNew();
        
    }
    
    static testmethod void caseNoteController_test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.AdminID__c = '123456';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Case c = new Case();
        c.AccountId = newAccount.Id;
        c.ContactId = newContact.Id;
        c.Status = 'Active';
        c.Type = 'CC Support';
        c.Subject = 'Test';
        c.OwnerID = [ SELECT Id FROM User WHERE LastName = 'Werner' LIMIT 1 ].Id; 
        insert c;
        
        
        Case_Issue__c ci = new Case_Issue__c();
        ci.Associated_Case__c = c.Id;
        ci.Product_In_Use__c = 'GM';
        ci.Product_Support_Issue__c = 'GM Support Issue';
        ci.Product_Support_Issue_Locations__c = 'GM Support Location';
        insert ci;
        
        Case_Notes__c cn = new Case_Notes__c();
        cn.Case__c = c.Id;
        cn.notes_type__c = 'chat';
        cn.Associated_Case_Issue__c = ci.Id;
        insert cn;
        
        ApexPages.currentPage().getParameters().put('CF00NS0000001ErJh_lkid', c.Id);
        ApexPages.currentPage().getParameters().put('ci', ci.id);
        ApexPages.currentPage().getParameters().put('id', cn.id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Case_Notes__c());
        caseNoteController con = new CaseNoteController(testController);
        
        con.getCaseIssues();
        con.savenewnote();
        con.saveNote();
        con.cancel();
    }
    
    static testMethod void caseController_test4() {
        
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id, CustomerAdministratorId__c = '65634534');
        insert newCon;
        
        //test case
        Case newCase = new Case();
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;  
        newCase.AccountId = newAcct.id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Subject = 'Test';
        newCase.Type = 'Compliance Services';
        newCase.Total_Project_Amount__c = 25000;
        newCase.Project_Status__c = 'New Contract';
        newCase.Project_Work_Submitted__c = 'Yes';
        newCase.Primary_Project_Type__c = 'BVA';
        
        insert newCase;
        
        Revenue_Recognition__c revrec = [Select ID, Case__c FROM Revenue_Recognition__c WHERE Case__c =: newCase.Id];
        System.assertEquals (revrec.Case__c, newCase.ID);
        
        newCase.Project_Status__c = 'Cancelled';
        newCase.Status ='Closed';
        newCase.Case_Resolution__c ='dsadsadsadsada';
        update newCase;
        
        ApexPages.currentPage().getParameters().put('id', newCase.id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Case());
        caseController con = new caseController(testController);
        
        con.c.Project_Work_Submitted__c = 'No';
        con.saveOnEdit();
        con.saveOnNew();
        
        system.assertEquals(con.primaryAdmin[0].Id, newCon.Id);
        
        
    }
    
    
    @isTest(SeeAllData=true) static  void revRecTest1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.AdminID__c = '1234567';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Case c = new Case();
        c.AccountId = newAccount.Id;
        c.ContactId = newContact.Id;
        c.Status = 'Active';
        c.Type = 'Compliance Services';
        c.Subject = 'Test';
        c.OwnerID = [ SELECT Id FROM User WHERE LastName = 'Powell' LIMIT 1 ].Id; 
        insert c;
        
        c.Project_Type__c = 'BVA';
        c.Primary_Project_Type__c = 'BVA';
        c.Total_Project_Amount__c = 1000;
        update c;
        
        ApexPages.currentPage().getParameters().put('id', c.id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Case());
        caseController con = new caseController(testController);
        
        integer revRecSize = con.revRec.size(); 
        integer revRecTrans = con.viewingPhase.Revenue_Recognition_Transactions__r.size();
        
        con.getViewing();
        con.getRevRecProjectStatus();
        
        ApexPages.currentPage().getParameters().put('addButton', 'status');
        con.addButton();
        
        ApexPages.currentPage().getParameters().put('cancelButton', 'status');
        con.cancelButton();
        
        con.newProjectStatus = [Select Id FROM Revenue_Recognition_Setting__mdt WHERE Project_Status__c = 'Cancelled'].id;
        con.addedBefore = [Select Id FROM Revenue_Recognition_Transaction__c WHERE Recognition_Project_Status__c = 'Ops - Account Prep' AND Revenue_Recognition__c = :con.viewingPhase.id LIMIT 1].id;
        
        con.newStatus();
        con.daysInStatus = '5';
        con.addedBeforeRRT.Expected_Date__c = Date.newInstance(2019, 2, 17);
        
        con.saveStatus();
        
        ApexPages.currentPage().getParameters().put('closeButton', 'status');
        con.closeButton();
        
        Id revRec = [SELECT id FROM Revenue_Recognition__c WHERE id = :con.viewingPhase.id].id;
        
        //test that revRecTrans/status is added        
        List<Revenue_Recognition_Transaction__c> newRevRecTrans = [SELECT Id From Revenue_Recognition_Transaction__c WHERE Revenue_Recognition__c = :revRec];
        system.assertEquals(newRevRecTrans.size(), revRecTrans + 1);
        
        ApexPages.currentPage().getParameters().put('addButton', 'phase');
        con.addButton();
        
        ApexPages.currentPage().getParameters().put('cancelButton', 'phase');
        con.cancelButton();
        
        con.savePhase();
        
        ApexPages.currentPage().getParameters().put('closeButton', 'phase');
        con.closeButton();
        
        //test that another phase/revRec has been added
        List<Revenue_Recognition__c> newRevRecs = [SELECT Id FROM Revenue_Recognition__c WHERE Case__c = :c.id];
        system.assertEquals(newRevRecs.size(), revRecSize + 1);
        
        
        con.rrProjectStatus = 'CF - Scheduling Initial Call';
        
        Revenue_Recognition_Transaction__c rrt = [ SELECT Id, Expected_Date__c FROM Revenue_Recognition_Transaction__c 
                                                  WHERE Revenue_Recognition__c = :revRec AND Recognition_Project_Status__c = 'CF - Assigned' LIMIT 1 ];
        
        rrt.Expected_Date__c = Date.newInstance(2018, 11, 29);
        
        ApexPages.currentPage().getParameters().put('date', rrt.id);
        con.resetDate();
        
        con.saveRevRec();  
        
        Revenue_Recognition_Transaction__c rrt2 = [ SELECT Id, Expected_Date__c FROM Revenue_Recognition_Transaction__c 
                                                   WHERE id = :rrt.id ];
        
        Case updatedCase = [Select Id, Project_Status__c FROM Case WHERE Id = :c.id];
        
        //test that the expected changes back to original date and that case Project Status field is changed due to change on rrProjectStatus on ViewingPhase1
        system.assertNotEquals(rrt2.Expected_Date__c, rrt.Expected_Date__c);
        //system.assertEquals(updatedCase.Project_Status__c, con.rrProjectStatus);
        
        ApexPages.currentPage().getParameters().put('closeButton', 'main');
        con.closeButton();
        
    }  
    
    @isTest(SeeAllData=true)

      static void caseNoteController_test4() {

          user d = [Select id from user where FirstName = 'Daniel' AND LastName = 'Gruszka' limit 1];
          user t = [Select id from user where FirstName = 'Tara' AND LastName = 'Wills' limit 1];
          user p = [Select id from user where FirstName = 'Priyanka' AND LastName = 'Goriparthi' limit 1];
          user m = [Select id from user where FirstName = 'Mac' AND LastName = 'Nosek' limit 1];
          
          

          
          
          Account a = new account();
          a.Name = 'Maciejs Best';
          a.Customer_Status__c = 'Active';
          insert a;
          
          Contact cont = new contact();
          cont.LastName = 'Howard';
          cont.AccountId = a.id;
          insert cont;
    
          Case tc = new Case();
          tc.AccountId = a.id;
          tc.contactId = cont.id;
          tc.status = 'Active';
          tc.type = 'Compliance Services - Data Migration';
          tc.OwnerId = d.id;
          insert tc;
          
          case cs = new case();
          cs.AccountId = a.id;
          cs.contactId = cont.id;
          cs.status = 'Active';
          cs.type = 'CC Support';
          cs.OwnerId = t.id;
          insert cs;
          
          
          Case oc = new Case();
          oc.AccountId = a.id;
          oc.contactId = cont.id;
          oc.status = 'Active';
          oc.type = 'Compliance Services - Onsite';
          oc.OwnerId = p.id;
          
          insert oc;  
          
          Case nc = new Case();
          nc.AccountId = a.id;
          nc.contactId = cont.id;
          nc.status = 'Active';
          nc.type = 'Compliance Services - Print Binder';
          nc.OwnerId = m.id;
          
          insert nc;  
          
          
          ApexPages.currentPage().getParameters().put('id', tc.id);
          ApexPages.StandardController testController = new ApexPages.StandardController(new Case());
          caseController mc = new caseController(testController);
          
          
          
          mc.promptCSDelivery();   
          
                   

          case csus = new case();
          csus.AccountId = a.id;
          csus.contactId = cont.id;
          csus.status = 'Active';
          csus.type = 'Compliance Services - Update Services';
          csus.OwnerId = t.id;
          insert csus;
          
           ApexPages.currentPage().getParameters().put('id', csus.id);
          mc.promptCSDelivery();   

      }
}