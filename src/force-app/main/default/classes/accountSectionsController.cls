public with sharing class accountSectionsController {
    public Account xAcct {get;set;}
    public Account_Referral__c[] xRefs {get;set;}
    public Referral_Settings__c[] xRefSets {get;set;}
    public User xUser {get;set;}
    public Account_Referral__c ref {get;set;}
    public question[] qWrappers {get;set;}
    public string refType {get;set;}
    Map<string, Schema.SObjectField> acctFieldMap {get;set;}
    Map<string, Schema.SObjectField> refFieldMap {get;set;}
    public Corporate_Account_Referral__c selRef {get;set;}
    
    public accountSectionsController() {
        qWrappers = new List<question>();        
    } 
    
    public string getRefs() {
        string refString = '';
        if (xRefs != null) {
            for (Account_Referral__c xRef : xRefs) {
                if (xRef.Type__c == 'Authoring') {
                    refString += 'Authoring;';
                }
                if (xRef.Type__c == 'Chemical Management') {
                    refString += 'Chemical Management;';
                }
                if (xRef.Type__c == 'Ergonomics') { 
                    refString += 'Ergonomics;';
                }
                if (xRef.Type__c == 'Training' || xRef.Type__c == 'ODT') { 
                    refString += 'Training;';
                }
                if (xRef.Type__c == 'EHS') {
                    refString += 'EHS;';
                }
            }
        }
        return refString;
    }
    
    public string getNewRefs() {
        string newRefString = '';
        if (xRefs != null) {
            for (Account_Referral__c xRef : xRefs) {
                if (xRef.Status__c != 'Reviewed') {
                    if (xRef.Type__c == 'Authoring') {
                        newRefString += 'Authoring;';
                    }
                    if (xRef.Type__c == 'Chemical Management') {
                        newRefString += 'Chemical Management;';
                    }
                    if (xRef.Type__c == 'Ergonomics') { 
                        newRefString += 'Ergonomics;';
                    }
                    if (xRef.Type__c == 'Training' || xRef.Type__c == 'ODT') { 
                        newRefString += 'Training;';
                    }
                    if (xRef.Type__c == 'EHS') { 
                        newRefString += 'EHS;';
                    }
                }
            }
        } 
        return newRefString;
    }
    
    public prevRef[] getPrevRefs() {
        prevRef[] prs = new List<prevRef>();
        if (xRefs != null) {
            Map<string, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('Account_Referral__c').getDescribe().fields.getMap();
            Map<Account_Referral__c, Referral_Settings__c[]> refMap = new Map<Account_Referral__c, Referral_Settings__c[]>();
            if (xRefSets != null) {
                for (Account_Referral__c xAr : xRefs) {
                    Referral_Settings__c[] rs = new List<Referral_Settings__c>();
                    for (Referral_Settings__c xRs : xRefSets) {
                        if (xRs.Type__c == xAr.Type__c) {
                            rs.add(xRs);
                        }
                    }
                    refMap.put(xAr, rs);
                }
                for (Account_Referral__c xAr : xRefs) {
                    for (Referral_Settings__c rs : refMap.get(xAr)) {
                        string refLabel = fieldMap.get(rs.Account_Referral_API_Name__c).getDescribe().getLabel();
                        string refValue = string.valueOf(xAr.get(rs.Account_Referral_API_Name__c));
                        string refType = xAr.Type__c;
                        prs.add(new prevRef(refLabel, refValue, refType));
                    }
                }
            }
        }
        if (!prs.isEmpty()) {
            return prs;
        } else {
            return null;
        }
    }
    
    public PageReference queryRefs() {
        string refIds = ApexPages.currentPage().getParameters().get('refId');
        selRef = [SELECT Id, Name, Account__c, Account__r.Name , Critical_Issues__c, CS_Amount_Quoted__c, Current_SDS_Method__c, Date_Reviewed__c, Decision_Makers_Name__c, Demo_Date__c,
                  Email__c, HQ_Amount_Quoted__c, Phone__c, Referral_Type__c, Reviewer__r.Name , Rep__r.Name, Status__c
                  FROM Corporate_Account_Referral__c WHERE Id = : refIds LIMIT 1];
        if (selRef.Status__c != 'Pending') {
            return null;
        } else {
            PageReference pr = Page.Account_corp_referral_Approval;
            pr.getParameters().put('id', selRef.Id );
            pr.getParameters().put('acctId', this.xAcct.Id);
            pr.setRedirect(true);
            return pr;
        }
    }
    
    public PageReference saveComparison() {
        boolean updAcct = false;
        for (question q : qWrappers) {
            for (string key : acctFieldMap.keySet()) {
                if (q.fieldKey == key) {
                    if (q.selVal == 'refVal') {
                        updAcct = true;
                        string ft = string.valueOf(acctFieldMap.get(key).getDescribe().getType());
                        if (ft == 'DOUBLE') {
                            xAcct.put(key, decimal.valueOf(q.refVal));
                        }
                        if (ft == 'INTEGER') {
                            xAcct.put(key, integer.valueOf(q.refVal));
                        }
                        if (ft == 'DATE') {
                            string d1 = q.refVal.substringBefore(' ');
                            string[] d2 = d1.split('-');
                            string d3 = d2[1]+'/'+d2[2]+'/'+d2[0];
                            xAcct.put(key, date.parse(d3));
                        } 
                        if (ft != 'DOUBLE' && ft != 'INTEGER' && ft != 'DATE') {
                            xAcct.put(key, string.valueOf(q.refVal));
                        }
                    } 
                }
            }
        }
        ref.Status__c = 'Reviewed';
        update ref;
        if (updAcct) { 
            update xAcct; 
        }
        return new PageReference('/apex/account_detail_sales?id='+xAcct.id).setRedirect(true);
    }    
    
    public void referralCompare() {
        refType = ApexPages.currentPage().getParameters().get('refType');
        if (refType != null) {
            for (Account_Referral__c xRef : xRefs) {
                if (xRef.Type__c == refType) {
                    ref = xRef;
                }
            }
            createQuestionWrappers();
        } 
    }
    
    public void createQuestionWrappers() {
        qWrappers.clear();
        acctFieldMap = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap();
        refFieldMap = Schema.getGlobalDescribe().get('Account_Referral__c').getDescribe().fields.getMap();
        for (Referral_Settings__c refSet : xRefSets) {
            if (refSet.Type__c == refType) {
                string fieldKey = '';
                string acctVal = '';
                string refVal = '';
                for (string key : acctFieldMap.keySet()) {
                    if (refSet.Account_API_Name__c == string.valueOf(acctFieldMap.get(key).getDescribe().getName())) {
                        acctVal = string.valueOf(xAcct.get(key));
                        fieldKey = key;
                    }
                }
                for (string key : refFieldMap.keySet()) {
                    if (refSet.Account_Referral_API_Name__c == string.valueOf(refFieldMap.get(key).getDescribe().getName())) {
                        refVal = string.valueOf(ref.get(key));
                    }
                }
                qWrappers.add(new question(fieldKey, refSet.Question__c, acctVal, refVal));
            }
        }
    }
    
    //question wrappers
    public class question {
        public string selVal {get;set;}
        public string fieldKey {get;set;}
        public SelectOption[] ops {get;set;}
        public string qVal {get;set;}
        public string acctVal {get;set;}
        public string refVal {get;set;}
        
        public question(string xFieldKey, string xQuestion, string xAcctVal, string xRefVal) {
            fieldKey = xFieldKey;
            qVal = xQuestion;
            acctVal = xAcctVal;
            refVal = xRefVal;
            createOps();
        }
        
        public void createOps() {
            ops = new List<SelectOption>();
            ops.add(new SelectOption('acctVal', ' '+' '+' '));
            ops.add(new SelectOption('refVal', ' '+' '+' '));
            selVal = 'acctVal';
        }
    } 
    
    public class prevRef {
        public string refLabel {get;set;}
        public string refValue {get;set;}
        public string refType {get;set;}
        
        public prevRef(string xLabel, string xValue, string xType) {
            refLabel = xLabel;
            refValue = xValue;
            refType = xType;
        }
    }
    
    
    
}