public class adminToolBookingsDataQueueable implements Queueable, Database.AllowsCallouts {
    /**
* This is called from the adminToolBookingsAPIFetch Scheduled Class
* This calls the Admin Tool Web Services to get the Bookings Data from the Admin Tool
**/
    public void execute(QueueableContext context) {
        String salesforceEnvironment;
        String adminToolEnvironment;
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D300000001HEfEAM'){
            salesforceEnvironment = 'Production';
            adminToolEnvironment = 'Production';
        }else{
            salesforceEnvironment ='Sandbox';
            adminToolEnvironment = 'Staging';
        }
        string adminToolData;
        if(datetime.now() > datetime.newInstance(datetime.now().year(), datetime.now().month(), datetime.now().day(), 2, 15, 0) &&
           datetime.now() < datetime.newInstance(datetime.now().year(), datetime.now().month(), datetime.now().day(), 2, 25, 0)){
               adminToolData = adminToolWebServices.fetchAdminToolData('BookingsReport', date.Today().addDays(-1).format(), adminToolEnvironment);
           }else{
               adminToolData = adminToolWebServices.fetchAdminToolData('BookingsReport', date.Today().format(), adminToolEnvironment);
           }
        adminToolUtility atUtility = new adminToolUtility();
        if(adminToolData != NULL){
            atUtility.upsertOrderData(adminToolData);
        }
    }
}