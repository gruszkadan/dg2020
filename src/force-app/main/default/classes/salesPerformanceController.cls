/*
* Homepage for the Sales Performance tab
* Test class: testSalesPerformanceController
*/
public with sharing class salesPerformanceController {
    public string userID { get; set; }
    public User u { get; set; }
    public date monthLastDay { get; set; }
    public date previousMonthLastDay { get; set; }
    public integer monthDaysLeft { get; set; }
    public integer quarterDaysLeft { get; set; }
    public string monthlySales { get; set; }
    public string monthlyQuota { get; set; }
    public integer mtdSales { get; set; }
    public integer mtdQuota { get; set; }
    public decimal mtdQuotaAttainment { get; set; }
    public integer ytdSales { get; set; }
    public integer ytdQuota { get; set; }
    public decimal ytdQuotaAttainment {get;set;}
    public decimal incentiveAmount {get;set;}
    public decimal incentiveBookings {get;set;}
    public decimal incentiveOther {get;set;}
    public decimal incentiveReferral {get;set;}
    public decimal incentiveDemo {get;set;}
    public decimal incentiveAssociate {get;set;}
    public decimal incentiveOtherTotal {get;set;}
    public Holiday[] holidays { get; set; }
    public String defaultView { get; set; }
    public Opportunity[] opptys { get; set; }
    public integer thisMonthDollars {get;set;}
    public integer nextMonthDollars {get;set;}
    public integer nextNextMonthDollars {get;set;}
    public integer pastDueDollars {get;set;}
    public integer monthNum {get;set;}
    public string currentMonthName {get;set;}
    public string nextMonthName {get;set;}
    public string nextNextMonthName {get;set;}
    //v1.1 Additions
    public Opportunity[] pastDue {get;set;}
    public Opportunity[] thisMonth {get;set;}
    public Opportunity[] nextMonth {get;set;}
    public Opportunity[] nextNextMonth {get;set;}
    public String viewAs { get; set; }
    public String viewAsDefaultView { get; set; }
    
    public salesPerformanceController() {
        
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias, FirstName, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID FROM User WHERE id = :viewAs LIMIT 1];
        if (defaultView == 'VP' && viewAs == userInfo.getUserId()) {
            u = [SELECT id, Alias, FirstName, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID FROM User WHERE UserRole.Name = 'VP Sales' AND isActive = true and LastName != 'Haling' LIMIT 1];
        }
        userID = u.id;
        //get defaultView to display when viewing as
        viewAsDefaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
        //Get the Holidays
        holidays = [SELECT ActivityDate FROM Holiday];
        
        // Month & Quarter Last Day
        Period quarter = [SELECT EndDate FROM Period WHERE type = 'Quarter' AND StartDate = THIS_FISCAL_QUARTER LIMIT 1];
        date quarterLastDay = quarter.EndDate;
        monthLastDay = Date.Today().toStartofMonth().addMonths(1) - 1;
        previousMonthLastDay = Date.Today().toStartofMonth()- 1;
        //Calcualte Days Left for Month & Quarter
        monthDaysLeft = calculateWorkingDays(date.Today(), monthLastDay, holidays);
        quarterDaysLeft = calculateWorkingDays(date.Today(), quarterLastDay, holidays);
        
        pastDueDollars = 0;
        thisMonthDollars = 0;
        nextMonthDollars = 0;
        nextNextMonthDollars = 0;
        mtdSales = 0;
        mtdQuota = 0;
        mtdQuotaAttainment = 0;
        ytdSales = 0;
        ytdQuota = 0;
        ytdQuotaAttainment = 0;
        incentiveAmount = 0;
        incentiveBookings = 0;
        incentiveOther = 0;
        incentiveReferral = 0;
        incentiveDemo = 0;
        incentiveAssociate = 0;
        incentiveOtherTotal = 0;
        monthNum = date.today().month();
        monthlySalesData();
        queryOpptys();
        opportunityData();   
        
        currentMonthName= dateTime.now().format('MMMMM');
        nextMonthName = dateTime.now().addMonths(1).format('MMMMM');
        nextNextMonthName = dateTime.now().addMonths(2).format('MMMMM');
    }
    
    public void queryOpptys(){
        string userField = 'OwnerID';
        string mgrField = '';
        date nextNextMonth = monthLastDay.addMonths(3).toStartOfMonth().addDays(-1);
        if (viewAsDefaultView == 'Manager') {
            mgrField = 'Owner.ManagerId';
        } else if (viewAsDefaultView == 'Director') {
            mgrField = 'Owner.Sales_Director__c';
        } else if (viewAsDefaultView == 'VP') {
            mgrField = 'Owner.Sales_VP__c';
        }
        
        string query = 'Select CloseDate, Amount from Opportunity ';
        query += 'WHERE isClosed = FALSE and CloseDate <= NEXT_N_MONTHS:2 and Opportunity_Type__c != null and (OwnerId = \''+userId+'\' or OwnerId = \'' + userId.left(15) + '\' ';
        if(viewAsDefaultView == 'Manager' || viewAsDefaultView == 'Director' || viewAsDefaultView == 'VP'){
            query += 'or ' + mgrField + '=\'' + userID + '\' or ' + mgrField + '=\'' + userID.left(15) + '\' '; 
        }
        query += ') ';
        query += 'Order By CloseDate ASC';
        opptys = database.query(query);
    }
    
    public void opportunityData() {
        pastDue = new list<Opportunity> ();
        thisMonth = new list<Opportunity> ();
        nextMonth = new list<Opportunity> ();
        nextNextMonth = new list<Opportunity> ();
        for (Opportunity o : opptys) {
            if (o.closeDate <= previousMonthLastDay) {
                pastDue.add(o);
            }
            if (o.closeDate <= monthLastDay) {
                thisMonth.add(o);
            } 
            if (o.closeDate > monthLastDay && o.CloseDate < dateTime.now().addMonths(2)) {
                nextMonth.add(o);
            } 
            if (o.CloseDate > dateTime.now().addMonths(2) && o.CloseDate < dateTime.now().addMonths(3)) {
                nextNextMonth.add(o);
            }
        }
        if (pastDue.size() > 0) {
            for (Opportunity o : pastDue) {
                pastDueDollars = pastDueDollars + integer.valueOf(o.Amount);
            }
        }
        if (thisMonth.size() > 0) {
            for (Opportunity o : thisMonth) {
                thisMonthDollars = thisMonthDollars + integer.valueOf(o.Amount);
            }
        }
        if (nextMonth.size() > 0) {
            for (Opportunity o : nextMonth) {
                nextMonthDollars = nextMonthDollars + integer.valueOf(o.Amount);
            }
        }
        if (nextNextMonth.size() > 0) {
            for (Opportunity o : nextNextMonth) {
                nextNextMonthDollars = nextNextMonthDollars + integer.valueOf(o.Amount);
            }
        }
    }
    
    public List<Sales_Performance_Order__c> getTopFiveOrders() {
        List<Sales_Performance_Order__c> topFiveOrders = new List<Sales_Performance_Order__c> ();
        string userField = 'OwnerID';
        
        if (viewAsDefaultView == 'Manager') {
            userField = 'Sales_Manager__c';
        } else if (viewAsDefaultView == 'Director') {
            userField = 'Sales_Director__c';
        } else if (viewAsDefaultView == 'VP') {
            userField = 'Sales_VP__c';
        }
        
        string query = 'SELECT Account__c, Account__r.Name, Owner.LastName, Booking_Amount__c from Sales_Performance_Order__c ';
        query += 'WHERE Order_Date__c = THIS_YEAR and Type__c=\'Booking\' ';
        query += 'and ' + userField + '=\'' + userID + '\' ORDER BY Booking_Amount__c DESC Limit 5';
        topFiveOrders = database.query(query);
        return topFiveOrders;
    }
    
    public static Integer calculateWorkingDays(Date startDate, Date endDate, Holiday[] holidays) {
        Set<Date> holidaysSet = new Set<Date> ();
        
        for (Holiday currHoliday : holidays)
        {
            holidaysSet.add(currHoliday.ActivityDate);
        }
        
        Integer workingDays = 0;
        
        for (integer i = 0; i <= startDate.daysBetween(endDate); i++)
        {
            Date dt = startDate + i;
            DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());
            String todayDay = currDate.format('EEEE');
            if (todayDay != 'Saturday' && todayDay != 'Sunday' && (!holidaysSet.contains(dt)))
            {
                workingDays = workingDays + 1;
            }
        }
        return workingDays;
    }
    
    public void monthlySalesData() {
        monthlySales = '';
        monthlyQuota = '';
        Sales_Performance_Summary__c[] spm = [SELECT ID, Total_Bookings__c, Quota__c, Month_Number__c, MTD_Total_Incentives__c, Quota_Percent__c, 
                                              YTD_Total_Bookings__c, YTD_Quota__c, YTD_Quota_Percent__c, Associate_Incentives__c, Bookings_Incentives__c,
                                              Other_Incentives__c, Referral_Incentives__c, Demo_Incentives__c
                                              FROM Sales_Performance_Summary__c 
                                              WHERE OwnerID = :userID and Date__c = THIS_YEAR 
                                              ORDER BY Month_Number__c ASC];
        Map<decimal,Sales_Performance_Summary__c> spmMap = new map<decimal,Sales_Performance_Summary__c>();
        for(Sales_Performance_Summary__c s:spm){
            spmMap.put(s.Month_Number__c,s);
        }
        for (integer i=1; i<=12; i++) {
            if(spmMap.get(i) != null){
                Sales_Performance_Summary__c monthSPS = spmMap.get(i);
                if (i == 1) {
                    monthlySales = monthSPS.Total_Bookings__c + ',';
                    monthlyQuota = monthSPS.Quota__c + ',';
                }else{
                    monthlySales = monthlySales + monthSPS.Total_Bookings__c + ',';
                    monthlyQuota = monthlyQuota + monthSPS.Quota__c + ','; 
                }
                if (monthSPS.Month_Number__c == monthNum) {
                    mtdSales = integer.valueOf(monthSPS.Total_Bookings__c);
                    mtdQuota = integer.valueOf(monthSPS.Quota__c);
                    mtdQuotaAttainment = monthSPS.Quota_Percent__c;
                    ytdSales = integer.valueOf(monthSPS.YTD_Total_Bookings__c);
                    ytdQuota = integer.valueOf(monthSPS.YTD_Quota__c);
                    ytdQuotaAttainment = monthSPS.YTD_Quota_Percent__c;
                    incentiveAmount = monthSPS.MTD_Total_Incentives__c;
                    incentiveBookings = monthSPS.Bookings_Incentives__c;
                    incentiveOtherTotal = monthSPS.Other_Incentives__c + monthSPS.Associate_Incentives__c + monthSPS.Demo_Incentives__c;
                    incentiveReferral = monthSPS.Referral_Incentives__c;
                }
            }else{
                if (i == 1) {
                    monthlySales = 0 + ',';
                    monthlyQuota = 0 + ',';
                }else{
                    monthlySales = monthlySales + 0 + ',';
                    monthlyQuota = monthlyQuota + 0 + ',';
                }
            }
        }
    }
}