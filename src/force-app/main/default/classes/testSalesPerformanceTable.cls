@isTest
private class testSalesPerformanceTable {
    
    private static testmethod void test_myTeam() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        User vp;
        salesPerformanceTableController con;
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            
            vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            Sales_Performance_Summary__c[] vpSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=0; i<12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, null);
                sps.Month__c = testDataUtility.findMonth(i + 1);
                sps.Manager__c = null;
                sps.Sales_Team_Manager__c = vp.id;
                vpSPSs.add(sps);
            }
            insert vpSPSs;
            
            Sales_Performance_Summary__c[] dirSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=0; i<12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, vpSPSs[i]);
                sps.Month__c = testDataUtility.findMonth(i + 1);
                sps.Manager__c = vp.id;
                sps.Sales_Team_Manager__c = dir.id;
                dirSPSs.add(sps);
            }
            insert dirSPSs;
            
        }
        
        ApexPages.currentPage().getParameters().put('view', vp.id);
        ApexPages.currentPage().getParameters().put('viewType', 'My Team');
        con = new salesPerformanceTableController();
        
        // assert 12 month wrappers were created
        System.assert(con.monthWrappers.size() == 12);
        
        // assert a month wrapper has both that month's SPS for vp and director
        System.assert(con.monthWrappers[0].orderWrappers.size() == 2);
        
        // assert the YTD totals were calculated correctly - the YTD totals should take the total bookings for each month from the VP sps - 10 x 12
        System.assert(con.ytdTotalBookings == 120);
        
    }
    
    private static testmethod void test_groupRole() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        User vp;
        salesPerformanceTableController con;
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            
            vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            Sales_Performance_Summary__c[] vpSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=0; i<12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, null);
                sps.Month__c = testDataUtility.findMonth(i + 1);
                sps.Manager__c = null;
                sps.Sales_Team_Manager__c = vp.id;
                sps.Group__c = 'Mid-Market';
                vpSPSs.add(sps);
            }
            insert vpSPSs;
            
            Sales_Performance_Summary__c[] dirSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=0; i<12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, vpSPSs[i]);
                sps.Month__c = testDataUtility.findMonth(i + 1);
                sps.Manager__c = vp.id;
                sps.Sales_Team_Manager__c = dir.id;
                sps.Group__c = 'Mid-Market';
                dirSPSs.add(sps);
            }
            insert dirSPSs;
            
        }
        
        ApexPages.currentPage().getParameters().put('viewType', 'Group');
        con = new salesPerformanceTableController();
        
        // assert 12 month wrappers were created
        System.assert(con.monthWrappers.size() == 12);
        
        // assert a month wrapper has both that month's SPS mid-market
        System.assert(con.monthWrappers[0].orderWrappers.size() == 1);
        
        // assert the YTD totals were calculated correctly - the YTD totals should take my bookings for each month for each rep in mid-market
        System.assert(con.ytdTotalBookings == 240);
        
    }
    
    private static testmethod void test_methods() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        User vp;
        User dir;
        salesPerformanceTableController con;
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            
            vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            Sales_Performance_Summary__c[] vpSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=0; i<12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, null);
                sps.Month__c = testDataUtility.findMonth(i + 1);
                sps.Manager__c = null;
                sps.Sales_Team_Manager__c = vp.id;
                vpSPSs.add(sps);
            }
            insert vpSPSs;
            
            Sales_Performance_Summary__c[] dirSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=0; i<12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, vpSPSs[i]);
                sps.Month__c = testDataUtility.findMonth(i + 1);
                sps.Manager__c = vp.id;
                sps.Sales_Team_Manager__c = dir.id;
                dirSPSs.add(sps);
            }
            insert dirSPSs;
            
        }
        
        ApexPages.currentPage().getParameters().put('view', vp.id);
        ApexPages.currentPage().getParameters().put('viewType', 'My Team');
        con = new salesPerformanceTableController();
        
        ApexPages.currentPage().getParameters().put('nextView', dir.id);
        ApexPages.currentPage().getParameters().put('nextPageRep', 'false');
        ApexPages.currentPage().getParameters().put('nm', 'January');
        con.next();
        
    }
    
    private static testmethod void test_security() {
        testDataUtility util = new testDataUtility();
        // Check mid-market rep security
        User mmRep = [SELECT id, LastName, ProfileId FROM User WHERE isActive = true AND Group__c = 'Mid-Market' LIMIT 1];
        System.runAs(mmRep) {
            Sales_Performance_Settings__c settings = util.createSettings(mmRep.ProfileId, false, false, 'Rep');
            insert settings;
            
            // Make sure the rep can view their default page
            salesPerformanceTableController con = new salesPerformanceTableController();
            System.assert(con.hasPermission == true);
            
            // If a rep tries to use parameters to navigate around security
            ApexPages.currentPage().getParameters().put('viewType', 'Group');
            con = new salesPerformanceTableController();
            System.assert(con.hasPermission == false);
        }
        
        // Check director security
        User mmDir = [SELECT id, LastName, ProfileId FROM User WHERE isActive = true AND Group__c = 'Mid-Market' AND Profile.Name = 'Sales Directors' LIMIT 1];
        System.runAs(mmDir) {
            Sales_Performance_Settings__c settings = util.createSettings(mmDir.ProfileId, true, true, 'Director');
            insert settings;
            
            // If a director tries to switch view types, he/she should be able to view it
            ApexPages.currentPage().getParameters().put('viewType', 'Group');
            salesPerformanceTableController con = new salesPerformanceTableController();
            System.assert(con.hasPermission == true);
            
            // Make sure directors also can't navigate URL parameters
            ApexPages.currentPage().getParameters().put('view', mmDir.id);
            con = new salesPerformanceTableController();
            System.assert(con.hasPermission == false); 
        }
    }
    
    private static testmethod void test_salesPerformanceTableUtility() {
        salesPerformanceTableUtility tableUtil = new salesPerformanceTableUtility();
        tableUtil.getyears();
        tableUtil.getviewTypes();
        tableUtil.months();
        
        string month = '';
        
        month = tableUtil.findMonth(1);
        System.assert(month == 'January');
        month = tableUtil.findMonth(2);
        System.assert(month == 'February');
        month = tableUtil.findMonth(3);
        System.assert(month == 'March');
        month = tableUtil.findMonth(4);
        System.assert(month == 'April');
        month = tableUtil.findMonth(5);
        System.assert(month == 'May');
        month = tableUtil.findMonth(6);
        System.assert(month == 'June');
        month = tableUtil.findMonth(7);
        System.assert(month == 'July');
        month = tableUtil.findMonth(8);
        System.assert(month == 'August');
        month = tableUtil.findMonth(9);
        System.assert(month == 'September');
        month = tableUtil.findMonth(10);
        System.assert(month == 'October');
        month = tableUtil.findMonth(11);
        System.assert(month == 'November');
        month = tableUtil.findMonth(12);
        System.assert(month == 'December');
    }
    
    private static testmethod void test_salesPerformanceOrderSetController() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        User vp;
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            
            vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            Sales_Performance_Summary__c[] vpSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=0; i<12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, null);
                sps.Month__c = testDataUtility.findMonth(i + 1);
                sps.Manager__c = null;
                sps.Sales_Team_Manager__c = vp.id;
                vpSPSs.add(sps);
            }
            insert vpSPSs;
            
            Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
            oi1.Month__c = 'January';
            insert oi1;
            
            Order_Item__c oi2 = util.createOrderItem('TestVP', 'test2');
            oi2.Order_ID__c = 'test456';
            oi2.Month__c = 'January';
            insert oi2; 
            
            salesPerformanceOrderSetController setCon = new salesPerformanceOrderSetController('My Team', vp.id, vp.Name, 'false', 'January', '2018');
            Sales_Performance_Order__c[] SPOs = setCon.getorders();
            
            // Assert both orders were found
            System.assert(SPOs.size() == 2);
            
            setCon.getrecPerPageOptions();
            setCon.searchItems();
            setCon.sortToggle();
            
            string nowDisplaying = setCon.nowDisplaying;
            System.assert(nowDisplaying == 'Displaying 1-2 of 2');
            
            boolean hasNext = setCon.hasNext;
            boolean hasPrev = setCon.hasPrev;
            
            System.assert(hasNext == false);
            System.assert(hasPrev == false);
            
            setCon.next();
            setCon.prev();
            
            ApexPages.currentPage().getParameters().put('spoId', SPOs[0].id);
            setCon.viewOrder();
            
            System.assert(setCon.spo.Order_ID__c == SPOs[0].Order_ID__c);
        }
    }
    
    private static testmethod void test_salesPerformanceIncentiveData() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        User vp;
        System.runAs(u) {
            AT_Product_Mapping__c atpm = new AT_Product_Mapping__c();
            atpm.Name = '94';
            atpm.Admin_Tool_Product_Name__c = 'HQ';
            atpm.Category__c = 'MSDS Management';
            atpm.Salesforce_Product_Name__c = 'HQ';
            atpm.Display_on_Account__c = true;
            atpm.Product_Platform__c = 'MSDSonline';
            atpm.Subscription_Based__c = true;
            atpm.Single_Year_Incentive_Rate__c = 10;
            atpm.Multi_Year_Incentive_Rate__c = 20;
            insert atpm;
            
            testDataUtility util = new testDataUtility();
            
            vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            Sales_Performance_Summary__c[] vpSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=0; i<12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, null);
                sps.Month__c = testDataUtility.findMonth(i + 1);
                sps.Manager__c = null;
                sps.Sales_Team_Manager__c = vp.id;
                vpSPSs.add(sps);
            }
            insert vpSPSs;
            
            //Create 2 order items to create  incentive transactions
            Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
            oi1.Month__c = 'January';
            oi1.Salesforce_Product_Name__c = 'HQ';
            oi1.Category__c = 'MSDS Management';
            oi1.Product_Platform__c = 'MSDSonline';
            insert oi1;
            
            Order_Item__c oi2 = util.createOrderItem('TestVP', 'test2');
            oi2.Order_ID__c = 'test456';
            oi2.Month__c = 'January';
            oi2.Salesforce_Product_Name__c = 'HQ';
            oi2.Category__c = 'MSDS Management';
            oi1.Product_Platform__c = 'MSDSonline';
            insert oi2; 
            
            String year = string.valueof(date.today().year());
            salesPerformanceIncentiveData itData = new salesPerformanceIncentiveData('My Team', vp.id, vp.Name, 'false', datetime.now().format('MMMMM'), year);
            
            //Assert that the incentives were pulled back
            system.assertNotEquals(itData.incentives.size(),2);
        }
    }
    
}