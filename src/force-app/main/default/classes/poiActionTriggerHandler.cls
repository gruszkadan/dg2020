public class poiActionTriggerHandler {
    
    // BEFORE UPDATE
    public static void beforeUpdate(Product_Of_Interest_Action__c[] oldActions, Product_Of_Interest_Action__c[] newActions) {
        Product_Of_Interest_Action__c[] closeDateActions = new list<Product_of_Interest_Action__c>();
        
        for (integer i=0; i<newActions.size(); i++) {
            if (newActions[i].Action_Status__c != null) {
                if(!oldActions[i].Action_Status__c.contains('Resolved') && (newActions[i].Action_Status__c.contains('Resolved') && newActions[i].inMerge__c == false)){
                    closeDateActions.add(newActions[i]);
                }
            }
        }
        if(closeDateActions.size() > 0){
            poiActionSession.sessionCloseDate(closeDateActions);
        }
        
    }
    
    // AFTER INSERT
    public static void afterInsert(Product_Of_Interest_Action__c[] oldActions, Product_Of_Interest_Action__c[] newActions) {
        poiUtility.updatePOI(newActions);
    }
    
    // AFTER UPDATE
    public static void afterUpdate(Product_Of_Interest_Action__c[] oldActions, Product_Of_Interest_Action__c[] newActions) {
        Product_Of_Interest_Action__c[] confirmedDateActions = new list<Product_of_Interest_Action__c>();
        // ADD THE ACTIONS TO THIS notInMergeActions LIST IF THEY ARENT IN A MERGE
        Product_Of_Interest_Action__c[] notInMergeActions = new list<Product_Of_Interest_Action__c>();
        for (integer i=0; i<newActions.size(); i++) {
            
            if (newActions[i].Action_Status__c != null) {
                if (oldActions[i].Session_Confirmed_Date__c == null && oldActions[i].Action_Status__c != newActions[i].Action_Status__c 
                    && newActions[i].Action_Status__c != 'Open' && newActions[i].Action_Status__c != 'Working - Pending Response') {
                        confirmedDateActions.add(newActions[i]);
                    }
            }
            
            if (!newActions[i].InMerge__c) {
                notInMergeActions.add(newActions[i]);
            }
            
        }
        if(confirmedDateActions.size() > 0){
            poiActionSession.sessionConfirmedDate(confirmedDateActions, 'poiAction');
        }
        if (notInMergeActions.size() > 0) {
            poiUtility.updatePOI(newActions);
        }
        
    }
}