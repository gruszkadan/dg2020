public with sharing class dailyprodscheduler2 implements Schedulable {
    
    public void execute(SchedulableContext ctx) {
        String[] teams = new List<String>();
        User[] us = [ SELECT Id, Sales_Team__c FROM User WHERE isActive = TRUE AND Role__c = 'Sales Manager' ORDER BY Sales_Team__c ASC ];
        for( User u : us )
        {
            teams.add(u.Sales_Team__c);
        }
        
        for( String team : teams )
        {
            Decimal sum_a_c_t = 0;
            Decimal sum_g_c_t = 0;
            Decimal sum_a_i_c = 0;
            Decimal sum_g_i_c = 0;
            Decimal sum_a_s_d = 0;
            Decimal sum_g_s_d = 0;
            Decimal sum_a_c_d = 0;
            Decimal sum_g_c_d = 0;
            Decimal sum_a_c_l = 0;
            Productivity_Report__c[] prs = [ SELECT Id, Name, Run_Date__c, Rep_Department__c, Rep_Name__c, Rep_Sales_Manager__c, Rep_Role__c, Percent_Completed_Demos__c, 
                                            Percent_Scheduled_Demos__c, Percent_Completed_Tasks__c, Percent_Initial_Contacts__c, Actual_Completed_Tasks_1__c, 
                                            Goals_Completed_Tasks_1__c, Actual_Initial_Contacts_1__c, Goals_Initial_Contacts_1__c, Actual_Scheduled_Demos_1__c, 
                                            Goals_Scheduled_Demos_1__c, Actual_Completed_Demos_1__c, Goals_Completed_Demos_1__c, Actual_Created_Licensing__c, Sales_Team__c 
                                            FROM Productivity_Report__c WHERE Run_Date__c = TODAY AND Sales_Team__c = :team ];   
            for( Productivity_Report__c pr : prs )
            {
                sum_a_c_t = sum_a_c_t + pr.Actual_Completed_Tasks_1__c;
                sum_g_c_t = sum_g_c_t + pr.Goals_Completed_Tasks_1__c;
                sum_a_i_c = sum_a_i_c + pr.Actual_Initial_Contacts_1__c;
                sum_g_i_c = sum_g_i_c + pr.Goals_Initial_Contacts_1__c;
                sum_a_s_d = sum_a_s_d + pr.Actual_Scheduled_Demos_1__c;
                sum_g_s_d = sum_g_s_d + pr.Goals_Scheduled_Demos_1__c;
                sum_a_c_d = sum_a_c_d + pr.Actual_Completed_Demos_1__c;
                sum_g_c_d = sum_g_c_d + pr.Goals_Completed_Demos_1__c;
                sum_a_c_l = sum_a_c_l + pr.Actual_Created_Licensing__c;
            }
            for( Productivity_Report__c pr2: prs )
            {
                pr2.Totals_Actual_Completed_Tasks__c = sum_a_c_t;
                pr2.Totals_Goals_Completed_Tasks__c = sum_g_c_t;
                pr2.Totals_Actual_Initial_Contacts__c = sum_a_i_c;
                pr2.Totals_Goals_Initial_Contacts__c = sum_g_i_c;
                pr2.Totals_Actual_Scheduled_Demos__c = sum_a_s_d;
                pr2.Totals_Goals_Scheduled_Demos__c = sum_g_s_d;
                pr2.Totals_Actual_Completed_Demos__c = sum_a_c_d;
                pr2.Totals_Goals_Completed_Demos__c = sum_g_c_d;
                pr2.Totals_Actual_Created_Licensing__c = sum_a_c_l;
                update pr2;
            }
        }
        
        // Reps and managers email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        PageReference xls = Page.dprexcel_m;
        xls.setRedirect(true);
        List<String> toAddresses = new List<String>();
        Productivity_Report__c[] r_list = [ SELECT Id, User_LU__c, User_LU__r.Email FROM Productivity_Report__c WHERE Run_Date__c = TODAY ];
       
        User[] m_list = [ SELECT Id, Email FROM User WHERE Role__c = 'Sales Manager' AND isActive = TRUE AND Sales_Director__r.Name = 'Eric Jackson' ];
        for( User m : m_list )
        {
            toAddresses.add(m.Email);
        }
        mail.setSubject( 
            'Daily Productivity Report For '+date.TODAY().format() 
        );
        mail.setToAddresses( toAddresses );
        mail.setPlainTextBody( 
            'The following link contains the Daily Productivity Report for '+
            date.TODAY().format()+': https://login.salesforce.com/apex/dprexcel_m' );
        Messaging.SendEmailResult [] r = 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});   
        
        // Directors email
        
        Messaging.SingleEmailMessage mail2 = new Messaging.SingleEmailMessage(); 
        PageReference xls2 = Page.dprexcel_d;
        xls2.setRedirect(true);
        List<String> toAddresses2 = new List<String>();
        User[] m_list2 = [ SELECT Id, Email FROM User WHERE Role__c = 'Sales Manager' AND isActive = TRUE AND Sales_Director__r.Name = 'Kevin Sy' ];
        for( User m2 : m_list2 )
        {
            toAddresses2.add(m2.Email);
        }
        toAddresses2.add('ksy@msdsonline.com');
        toAddresses2.add('ejackson@msdsonline.com');
        mail2.setSubject( 
            'Daily Productivity Report For '+date.TODAY().format() 
        );
        mail2.setToAddresses( toAddresses2 );
        mail2.setPlainTextBody( 
            'The following link contains the Daily Productivity Report for '+
            date.TODAY().format()+': https://login.salesforce.com/apex/dprexcel_d' );
        Messaging.SendEmailResult [] r2 = 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail2});   
    }
}