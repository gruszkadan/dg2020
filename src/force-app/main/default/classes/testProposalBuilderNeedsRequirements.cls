@isTest(SeeAllData = true)
private class testProposalBuilderNeedsRequirements {
    
    
    
    static testMethod void test1(){
        
        Quote__c testQuote = new Quote__c();
        insert testQuote;
        
        Quote_Proposal_Need__c  qpn = new Quote_Proposal_Need__c(Available_For_Use__c = true);
        insert qpn;
        
        ApexPages.currentPage().getParameters().put('id', testQuote.id);
        proposalBuilderNeedsRequirements con = new proposalBuilderNeedsRequirements();
        
        //test to see that QPN is created into wrapper
        ApexPages.currentPage().getParameters().put('need', qpn.id);
       
        con.createNeedsWrapper();
        system.assertEquals(con.needsWrappers[0].need.id, qpn.id);
        system.assertEquals(con.needsWrappers[0].order, 0);
        
        con.orderedString = String.valueOf(qpn.id);
        con.next();
        
        //check that JSON string of IDs is converted back into wrapper correctly
        con.loadWrappers();
        system.assertEquals(con.needsWrappers[0].need.id, qpn.id);
        
        con.getFamilyOptions();
        
        //test that wrapper is converted back to available need, then that the QPN is deleted
        ApexPages.currentPage().getParameters().put('order', String.valueOf(0));
        ApexPages.currentPage().getParameters().put('need', qpn.id);
        
        con.removeWrapper();
        
        ApexPages.currentPage().getParameters().put('delQPN', qpn.id);
        con.deleteQPN();
        
        List<quote_proposal_need__c> qpnList = [Select Id, Name FROM Quote_Proposal_Need__c WHERE id = :qpn.id];
        system.assertEquals(qpnList.size(), 0);
        
        con.clearProductSelection();
 		con.modalQPNId();       
        
        
        
        
        
        
    }
    
}