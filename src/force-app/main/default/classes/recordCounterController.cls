public class recordCounterController {
    public string sobj {get;set;}
    public integer numRecords {get;set;}
    
    public recordCounterController() {
        numRecords = 0;
    }
    
    public void findCount() {
        string query = 'SELECT COUNT() FROM '+sobj;
        numRecords = Database.countQuery(query);
    }

}