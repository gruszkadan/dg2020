@isTest(seeAllData=true)
private class testquoteOpportunityRefresh {
    
    static testmethod void test1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        insert newOpportunity;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Opportunity__c = newOpportunity.Id;
        insert newQuote;
        
        // Lookup an existing item
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = '01t80000002NON4';
        myItem.PricebookEntryId__c ='01u80000006eap9';
        myItem.Quantity__c = 1;
        myItem.Quote__c = newQuote.Id;
        insert myItem;
        
        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        ApexPages.currentPage().getParameters().put('oid', newOpportunity.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteOpportunityRefresh myController = new quoteOpportunityRefresh(testController);
        
        
        
        myController.redirectToOppty();
        myController.refreshOppty();
        
    }
}