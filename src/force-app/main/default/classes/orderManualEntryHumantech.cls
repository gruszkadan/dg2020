public with sharing class orderManualEntryHumantech {
    Public Contract_Line_item__c[] ConLineItems {get;set;}
    Public lineItemWrapper[] LIW {get;set;}
    Public Order_Item__c[] OrderID {get;set;}
    Public Order_Item__c[] OrderItemID {get;set;}
    Public Account AdminID {get;set;}
    Public Account Account {get;set;}
    Public id ConId {get;set;}
    Public Contract__c con {get;set;}
    Public boolean errormsg {get;set;}
    Public String errormessage {get;set;}
    
    public orderManualEntryHumantech() {
        //Pass in contract ID
        ConId = ApexPages.currentPage().getParameters().get('Id');
        
        //Query contract feilds needed on load 
        con = [select id, name, Contract_Length__c, Order_Submitted_Date__c, Sales_Rep__c, Contract_Type__c, Account__r.AdminID__c, Account__r.CustomerID__c, Account__r.Customer_Status__c From contract__c where id =: ConId];
        
        
        //Query all contract line items on contract that we will need on load 
        ConLineItems = [Select id, name, Y1_Final_Price__c, Y2_Final_Price__c, Y3_Final_Price__c, Y4_Final_Price__c, Y5_Final_Price__c, Name__c, Product_Sub_Type__c, Product__c, Year_1__c, Year_2__c, Year_3__c, Year_4__c, Year_5__c, contract__r.Contract_Length__c, contract__r.name, contract__r.num_of_years__c, contract__r.Order_Submitted_By__r.name, contract__r.A2_SaaS_Start_Date__c, contract__r.A2_SaaS_End_Date__c, Product__r.Reoccurring__c, Quantity__c  From Contract_Line_item__c where Contract__c =: ConId];
        
        errormsg = false;
        
        //If query pulls back records 
        //Intialize new list of line item wrappers 
        //loop through contract line items 
        //add line item wrapper to list 
        if(ConLineItems.size() > 0){
            LIW = new List<lineItemWrapper>();
            for(Contract_Line_item__c c : ConLineItems){
                LIW.add(new lineItemWrapper(c));
            }
        }
    }
    
    public class orderItemYearWrapper {
        public Order_Item__c OI {get;set;}
        public Integer Year {get;set;}
        
        public orderItemYearWrapper(Integer xYear, Contract_Line_Item__c CLI){
            //Passing in the year and the contract line item   
            // Set year vairble that was passed in 
            Year=xYear;
            
            //Intialize new order item 
            OI = new Order_Item__c();
            //Set fields on the order items that will be created 
            OI.Contract_Number__c = CLI.contract__r.name;
            OI.Contract_Length__c = CLI.contract__r.Num_of_Years__c;
            OI.Product_Platform__c = 'Ergo';
            OI.Admin_Tool_Product_Name__c = CLI.Name__c;
            OI.Admin_Tool_Product_Type__c = CLI.Product_Sub_Type__c;
            OI.Admin_Tool_Sales_Rep__c = CLI.contract__r.Order_Submitted_By__r.name;
            OI.Sales_Location__c = 'Ann Arbor';
            if(CLI.contract__r.A2_Saas_Start_date__c != NULL){
            	OI.Term_Start_Date__c = CLI.contract__r.A2_Saas_Start_date__c.addYears(xYear -1);
                OI.Term_End_Date__c = OI.Term_Start_Date__c.addYears(1).addDays(-1);
            }
            OI.Term_Length__c = 1;
            OI.Contract_Line_Item__c = CLI.id;
            OI.Admin_Tool_Order_Status__c = 'A';
            OI.Quantity__c = CLI.Quantity__c;
            if(CLI.contract__r.A2_SaaS_End_Date__c != NULL){
            	OI.Contract_Term_End_Date__c = CLI.contract__r.A2_SaaS_End_Date__c;
            }
            // Set invoice amount to the Yx final price for the specfic year of the contract
            // Following we will set the extra OI final price last year price being used 
            if(Year == 1 ){
                OI.Invoice_amount__c = CLI.Y1_Final_Price__c;
            }else if(Year == 2 && (CLI.Contract__r.Contract_Length__c == '1 Year' || CLI.Contract__r.Contract_Length__c == '2 Years' || CLI.Contract__r.Contract_Length__c == '3 Years' || CLI.Contract__r.Contract_Length__c == '4 Years' || CLI.Contract__r.Contract_Length__c == '5 Years')){
                OI.Invoice_amount__c = CLI.Y2_Final_Price__c;
            }else if(Year == 3 && (CLI.Contract__r.Contract_Length__c == '1 Year' || CLI.Contract__r.Contract_Length__c == '2 Years' || CLI.Contract__r.Contract_Length__c == '3 Years' || CLI.Contract__r.Contract_Length__c == '4 Years' || CLI.Contract__r.Contract_Length__c == '5 Years')){
                OI.Invoice_amount__c = CLI.Y3_Final_Price__c;
            }else if(Year == 4 && (CLI.Contract__r.Contract_Length__c == '1 Year' || CLI.Contract__r.Contract_Length__c == '2 Years' || CLI.Contract__r.Contract_Length__c == '3 Years' || CLI.Contract__r.Contract_Length__c == '4 Years' || CLI.Contract__r.Contract_Length__c == '5 Years')){
                OI.Invoice_amount__c = CLI.Y4_Final_Price__c;
            }else if (Year == 5 && (CLI.Contract__r.Contract_Length__c == '1 Year' || CLI.Contract__r.Contract_Length__c == '2 Years' || CLI.Contract__r.Contract_Length__c == '3 Years' || CLI.Contract__r.Contract_Length__c == '4 Years' || CLI.Contract__r.Contract_Length__c == '5 Years')){
                OI.Invoice_amount__c = CLI.Y5_Final_Price__c;
            } 
            
            if(Year == 2 && CLI.Contract__r.Contract_Length__c == '1 Year'){
                OI.Invoice_amount__c = CLI.Y1_Final_Price__c;
            }else if(Year == 3  && CLI.Contract__r.Contract_Length__c == '2 Years'){
                OI.Invoice_amount__c = CLI.Y2_Final_Price__c;
            }else if(Year == 4  && CLI.Contract__r.Contract_Length__c == '3 Years'){
                OI.Invoice_amount__c = CLI.Y3_Final_Price__c;
            }else if(Year == 5 && CLI.Contract__r.Contract_Length__c == '4 Years'){
                OI.Invoice_amount__c = CLI.Y4_Final_Price__c;
            }else if (Year == 6  && CLI.Contract__r.Contract_Length__c == '5 Years') {
                OI.Invoice_amount__c = CLI.Y5_Final_Price__c;
            }  
                     

                     //Set the Order Type to New only the first year the product is on the contract otherwise set it too renewal
                     if(CLI.Year_1__c == true  && Year == 1){
                         OI.Admin_Tool_Order_Type__c = 'New';
                     }
                     else if(CLI.Year_1__c == false  && CLI.Year_2__c == true && Year == 2){
                         OI.Admin_Tool_Order_Type__c = 'New';
                     }
                     else if(CLI.Year_1__c == false  && CLI.Year_2__c == false && CLI.Year_3__c == true && Year == 3){
                         OI.Admin_Tool_Order_Type__c = 'New';
                     }
                     else if(CLI.Year_1__c == false  && CLI.Year_2__c == false && CLI.Year_3__c == false && CLI.Year_4__c == true && Year == 4){
                         OI.Admin_Tool_Order_Type__c = 'New';
                     }
                     else if(CLI.Year_1__c == false  && CLI.Year_2__c == false && CLI.Year_3__c == false && CLI.Year_4__c == false && CLI.Year_5__c == true && Year == 5){
                         OI.Admin_Tool_Order_Type__c = 'New';
                     }else {
                         OI.Admin_Tool_Order_Type__c = 'Renewal';
                     }
                     }
                     }
                     
                     public class lineItemWrapper {
                         public Contract_Line_Item__c CLI {get;set;}
                         public orderItemYearWrapper[] OIwrap {get;set;}
                         
                         public lineItemWrapper(Contract_Line_Item__c xCLI){
                             //Pass in contract line item to be used in wrapper class 
                             // Intilize new list or order Item Year Wrappers 
                             // set contract line item to the variable being passed in 
                             OIwrap = new List<orderItemYearWrapper>();
                             
                             CLI = xCLI;
                             
                             //If the Year_x__C field is checked on contract line item 
                             //create an order item for that specfic year 
                             if(CLI.Year_1__c == true){
                                 OIwrap.add(new orderItemYearWrapper(1, CLI));
                             }
                             if(CLI.Year_2__c ==  true){
                                 OIwrap.add(new orderItemYearWrapper(2,CLI));
                             }
                             if(CLI.Year_3__c == true){
                                 OIwrap.add(new orderItemYearWrapper(3,CLI));
                             }
                             if(CLI.Year_4__c == true){
                                 OIwrap.add(new orderItemYearWrapper(4,CLI));
                             }
                             if(CLI.Year_5__c == true){
                                 OIwrap.add(new orderItemYearWrapper(5,CLI));
                             }   
                             
                             // Create Renewal Order Item for the year after the contract expires 
                             if(CLI.Contract__r.Contract_Length__c == '1 Year' && CLI.Year_1__c == true && CLI.Product__r.Reoccurring__c == true){
                                 OIwrap.add(new orderItemYearWrapper(2, CLI));
                             }Else if(CLI.contract__r.Contract_Length__c == '2 Years' && CLI.Year_1__c == true && CLI.Year_2__c == true && CLI.Product__r.Reoccurring__c == true){
                                 OIwrap.add(new orderItemYearWrapper(3, CLI));
                             }Else if(CLI.contract__r.Contract_length__c == '3 Years' && CLI.Year_1__c == true && CLI.Year_2__c == true && CLI.Year_3__c == true && CLI.Product__r.Reoccurring__c == true){
                                 OIwrap.add(new orderItemYearWrapper(4, CLI));
                             }Else if(CLI.Contract__r.Contract_length__c == '4 Years' && CLI.Year_1__c == true && CLI.Year_2__c == true && CLI.Year_3__c == true && CLI.Year_4__c == true && CLI.Product__r.Reoccurring__c == true){
                                 OIwrap.add(new orderItemYearWrapper(5, CLI));
                             }Else if(CLI.Contract__r.Contract_length__c == '5 Years' && CLI.Year_1__c == true && CLI.Year_2__c == true && CLI.Year_3__c == true && CLI.Year_4__c == true && CLI.Year_5__c == true && CLI.Product__r.Reoccurring__c == true){
                                 OIwrap.add(new orderItemYearWrapper(6, CLI));
                             }
                             
                             
                         }
                     }


                     public PageReference save(){
                         
                         //Query Account and order item fields that we will need to increment and update 
                         Account = [Select id, AdminID__c, CustomerID__c, Customer_Status__c, Humantech_Client_ID__c from account where id = : con.account__c limit 1];
                         AdminID = [Select id, AdminID__c, Customer_Status__c from Account where AdminID__c Like'6%' order by AdminID__c desc limit 1];
                         OrderID = [Select Order_ID__c from Order_item__c where Order_ID__c Like '8%' order by Order_ID__c desc limit 1];
                         OrderItemID = [Select Order_Item_ID__c from Order_item__c where Order_Item_ID__c Like '8%' order by Order_Item_ID__c desc limit 1];
                         account[] largestHumantechId = [Select Humantech_Client_ID__c from Account where Humantech_Client_ID__c Like '7%' order by Humantech_Client_ID__c desc limit 1];
                        
                         
                         
                         // Varaible that will be used to increment order item id
                         integer OrderItemIDincrement;
                         integer OrderIDinteger;
                         integer Admin_ID;
                         integer humantechID;        
                         
                         If(OrderID.size() == 0){
                             OrderIDinteger = 800000;
                         }else{
                             OrderIDinteger = integer.valueOf(OrderID[0].Order_ID__c); 
                         }
                         
                         If(OrderItemID.size() == 0){
                             OrderItemIDincrement = 800000;
                         }else{
                             OrderItemIDincrement = integer.valueOf(OrderItemID[0].Order_Item_ID__c);
                         }

                         if(largestHumantechId.size() == 0){
                            humantechID = 700000;
                         }else{
                            humantechID = integer.valueof(largestHumantechId[0].Humantech_Client_ID__c) + 1;
                         }                         
                         Admin_ID = integer.valueof(AdminID.AdminID__c);                         
                         
                         // loop through account and set fields and add account to list 
                         if((Account.AdminID__c == null || Account.AdminID__c.startswith('2')) && Account.CustomerID__c == null){
                             Admin_ID =  Admin_ID + 1;
                             Account.AdminID__c = string.valueof(Admin_ID);
                             Account.CustomerID__c = string.valueof(Admin_ID);
                             Account.Customer_Status__c = 'Active';
                         }
                        
                        //if not set, set the humantech id
                         if(Account.Humantech_Client_ID__c == NULL){
                            Account.Humantech_Client_ID__c =  string.valueof(humantechID); 
                         } 
                         
                         //Set customer status for the accounts that already have the admin and customer ids
                         Account.Customer_Status__c = 'Active'; 
                         
                         
                         //Intialize a new set and list 
                         List<Order_Item__c> OIList = new List<Order_Item__c>();
                         
                         //Loop through line item wrappers
                         //loop through order item wrapper but acesss the OIwrap list in the line item wrapper class 
                         //Add records to set 
                         for(lineItemWrapper x : LIW){
                             For(orderItemYearWrapper y : x.OIwrap){
                                 OIList.add(y.OI);
                             }
                         }
                         
                         
                         // Loop through OI set and set feilds 
                         // Add to OI list
                         // Add to list that will be used to update OI
                         for(Order_Item__c o : OIList){
                             orderItemIdincrement += 1;   
                             o.Name = 'H-' + orderItemIdincrement;
                             o.Order_ID__c =  String.valueOf(OrderIDinteger + 1);
                             o.Order_Item_ID__c = String.valueOf(orderItemIdincrement);
                             o.Year__c = String.valueOf(con.Order_Submitted_Date__c.year());
                             o.Month__c = dateUtility.MonthName(con.Order_Submitted_Date__c.month(), false);
                             o.Order_Date__c = con.Order_Submitted_Date__c;
                             o.AccountID__c = Account.AdminID__c;
                         }
                         
                         
                         //Set contract field 
                         con.Status__c = 'Order Processed';
                         
                         
                         //Insert order items and Update Account 
                         
                         try{
                             Update Account;
                             Update con;
                             Insert OIlist;
                         }Catch (Exception e){
                             errormsg = true;
                             errormessage = e.getmessage();
                         }
                         
                         
                         //Return to contract 
                         return new PageReference('/'+conid);
                     }
                     }