global class countPrimaryOppContactsUpdater implements Schedulable {
    global void execute(SchedulableContext ctx) {
       Boolean scheduled = true;
        countPrimaryOppContactsBatchable batch = new countPrimaryOppContactsBatchable(scheduled);
        Database.executebatch(batch, 2000);
    }
 }