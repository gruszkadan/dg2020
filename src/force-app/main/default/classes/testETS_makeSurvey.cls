@isTest
private class testETS_makeSurvey {
    
    static testmethod void test_impHQ_1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'IMP: HQ';
        newCase.Subject = 'Test';
        insert newCase;
        
        Case newCase2 = new Case();
        newCase2.AccountID = newAccount.Id;
        newCase2.ContactID = newContact.Id;
        newCase2.Status = 'Active';
        newCase2.Type = 'IMP: HQ';
        newCase2.Subject = 'Test';
        insert newCase2;
        
        test.startTest();
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        update newCase;
        newCase2.Case_Resolution__c = 'sdfsdf';
        newCase2.Status = 'Closed';
        update newCase2;
        test.stopTest();
    }
    static testmethod void test_impHQ_2() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Channel__c = 'ACWA';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        newContact.Opt_In_Surveys__c = false;
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'IMP: HQ';
        newCase.Subject = 'Test';
        insert newCase;
        
        test.startTest();
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        update newCase;
        test.stopTest();
    }
    
    static testmethod void test_cs_1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'Compliance Services';
        newCase.Standalone__c = true;
        newCase.Subject = 'Test';
        insert newCase;
        
        Case newCase2 = new Case();
        newCase2.AccountID = newAccount.Id;
        newCase2.ContactID = newContact.Id;
        newCase2.Status = 'Active';
        newCase2.Project_Status__c = 'Delivered';
        newCase.Project_Status__c = 'Delivered';
        newCase2.Type = 'Compliance Services';
        newCase2.Standalone__c = true;
        newCase2.Subject = 'Test';
        insert newCase2;
        
        test.startTest();
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Project_Work_Submitted__c = 'Yes';
        newCase.Status = 'Closed';
        update newCase;
        newCase2.Case_Resolution__c = 'sdfsdf';
        newCase2.Project_Work_Submitted__c = 'Yes';
        newCase2.Status = 'Closed';
        update newCase2;
        test.stopTest();
    }
    static testmethod void test_cs_2() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Channel__c = 'ACWA';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        newContact.Opt_In_Surveys__c = false;
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Project_Status__c = 'Delivered';
        newCase.Type = 'Compliance Services';
        newCase.Standalone__c = true;
        newCase.Subject = 'Test';
        insert newCase;
        
        test.startTest();
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        newCase.Project_Work_Submitted__c = 'Yes';
        update newCase;
        test.stopTest();
    }
    
    static testmethod void test_ccsup_1() {
         Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'CC Support';
        newCase.Category__c = 'Incident';
        newCase.Priority__c = '1';
        newCase.Subject = 'Test';
        insert newCase;
        
        Case newCase2 = new Case();
        newCase2.AccountID = newAccount.Id;
        newCase2.ContactID = newContact.Id;
        newCase2.Status = 'Active';
        newCase2.Type = 'CC Support';
        newCase2.Category__c = 'Incident';
        newCase2.Priority__c = '1';
        newCase2.Subject = 'Test';
        insert newCase2;
        
        Case_Issue__c ci = new Case_Issue__c();
        ci.Associated_Case__c = newCase.id;
        ci.Product_In_Use__c = 'HQ';
        ci.Product_Support_Issue__c = 'Test issue';
        ci.Product_Support_Issue_Locations__c = 'Test location';
        insert ci;
        
         Case_Issue__c ci2 = new Case_Issue__c();
        ci2.Associated_Case__c = newCase2.id;
        ci2.Product_In_Use__c = 'HQ';
        ci2.Product_Support_Issue__c = 'Test issue';
        ci2.Product_Support_Issue_Locations__c = 'Test location';
        insert ci2;
        
        test.startTest();
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        newCase.Client_Disposition__c = 'Satisfied with Outcome';
        update newCase;
        newCase2.Case_Resolution__c = 'sdfsdf';
        newCase2.Status = 'Closed';
        newCase2.Client_Disposition__c = 'Satisfied with Outcome';
        update newCase2;
        test.stopTest();
    }
    static testmethod void test_ccsup_2() {
         Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.Channel__c = 'ACWA';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        newContact.Opt_In_Surveys__c = false;
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'CC Support';
        newCase.Category__c = 'Incident';
        newCase.Priority__c = '1';
        newCase.Subject = 'Test';
        insert newCase;
        
        Case_Issue__c ci = new Case_Issue__c();
        ci.Associated_Case__c = newCase.id;
        ci.Product_In_Use__c = 'HQ';
        ci.Product_Support_Issue__c = 'Test issue';
        ci.Product_Support_Issue_Locations__c = 'Test location';
        insert ci;
        
        test.startTest();
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        newCase.Client_Disposition__c = 'Satisfied with Outcome';
        update newCase;
        test.stopTest();
    }
}