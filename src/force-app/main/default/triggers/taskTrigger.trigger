trigger taskTrigger on Task 
(before insert, before update, before delete, after insert, after update, after delete, after undelete) { 
    
    if (trigger.isBefore) {
        
        // BEFORE INSERT
        if (trigger.isInsert) {
            taskTriggerHandler.beforeInsert(trigger.new);
        }
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            taskTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
        
        // BEFORE DELETE
        if (trigger.isDelete) {
            taskTriggerHandler.beforeDelete(trigger.old, trigger.new);
        }
        
    }
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            taskTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            taskTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
        // AFTER DELETE
        if (trigger.isDelete) {
            taskTriggerHandler.afterDelete(trigger.old, trigger.new);
        }
        
        // AFTER UNDELETE
        if (trigger.isUndelete) {
            taskTriggerHandler.afterUndelete(trigger.old, trigger.new);
        }
        
    }  
    
    if(!test.isRunningTest()){
        dlrs.RollupService.triggerHandler(); 
    }
}