public class contactUtility {
    
    public static void reassignContact(Contact[] contacts, boolean dmlUpdate) {
        try {
            Set<Id> accountIds = new Set<Id>();
            Map<Id, Id> accountOwnerIdMap = new Map<Id, Id>();
            Map<Id, Id> accountTerritoryIdMap = new Map<Id, Id>();
            Map<Id, String> accountMSDSOwnerEmail = new Map<Id, String>();
            Map<Id, String> accountAuthOwnerEmail = new Map<Id, String>();
            Map<Id, String> accountEHSOwnerEmail = new Map<Id, String>();
            Map<Id, String> accountEHSManagerOwnerEmail = new Map<Id, String>();
            Map<Id, String> accountErgoOwnerEmail = new Map<Id, String>();
            Map<Id, String> accountOTOwnerEmail = new Map<Id, String>();
            Map<Id, String> ErgoOwnerManagerEmail  = new Map<Id, String>();
            
            
            
            // all the accounts whose owner ids to look up
            for (Contact c : contacts) {
                if(c.accountId <> null){
                    accountIds.add( c.accountId );
                }
            }
            // look up each account owner id
            for ( Account acct : [ SELECT id, ownerId, Territory__c, Owner.Email, Authoring_Account_Owner__r.Email, EHS_Owner__r.Email, EHS_Owner__r.Manager.Email, 
                                  Ergonomics_Account_Owner__r.Email, Online_Training_Account_Owner__r.Email, Ergonomics_Account_Owner__r.Manager.Email FROM account WHERE id IN :accountIds ] ) {
                                      accountOwnerIdMap.put( acct.Id, acct.ownerId);
                                      accountTerritoryIdMap.put( acct.Id, acct.Territory__c);
                                      accountMSDSOwnerEmail.put(acct.Id, acct.owner.Email);
                                      accountAuthOwnerEmail.put(acct.Id, acct.Authoring_Account_Owner__r.Email);
                                      accountEHSOwnerEmail.put(acct.Id, acct.EHS_Owner__r.Email);
                                      accountEHSManagerOwnerEmail.put(acct.Id, acct.EHS_Owner__r.Manager.Email);
                                      accountErgoOwnerEmail.put(acct.Id, acct.Ergonomics_Account_Owner__r.Email);
                                      accountOTOwnerEmail.put(acct.Id, acct.Online_Training_Account_Owner__r.Email);
                                      ErgoOwnerManagerEmail.put(acct.Id, acct.Ergonomics_Account_Owner__r.Manager.Email);
                                  }
            
            // change contact owner to its account owner
            for ( Contact c : contacts ) {
                if(c.AccountId <> null){
                    c.ownerId = accountOwnerIdMap.get( c.accountId );
                    c.MSDS_Owner_Email__c = accountMSDSOwnerEmail.get(c.AccountId);
                    c.Authoring_Owner_Email__c = accountAuthOwnerEmail.get(c.AccountId);
                    c.EHS_Account_Owner_Email__c = accountEHSOwnerEmail.get(c.AccountId);
                    c.EHS_Owner_Manager_Email__c = accountEHSManagerOwnerEmail.get(c.AccountId);
                    c.Ergonomics_Owner_Email__c = accountErgoOwnerEmail.get(c.AccountId);
                    c.Online_Training_Owner_Email__c = accountOTOwnerEmail.get(c.AccountId);
                    c.Ergo_Owner_Manager_Email__c = ErgoOwnerManagerEmail.get(c.AccountId);                    
                        
                    if (c.Territory__c == null){
                        c.Territory__c = accountTerritoryIdMap.get(c.accountId);
                    }
                }
            }
            if(dmlUpdate){
                update contacts;
            } 
            
        } catch(Exception e) { 
            salesforceLog.createLog('Contact Trigger', true, 'contactUtility', 'reassignContact', string.valueOf(e));
        }
    }
    
    public static void campaignAssignment(Contact[] contacts) {
        //query the lead source cross reference custom setting
        Campaign[] campaigns = [Select id,name from Campaign where EndDate >=: date.today() and  StartDate <=: date.today() 
                                and (name LIKE 'Referral 2%' or name LIKE 'Customer Care. New Contact%' or name LIKE 'Cold Call%' or name LIKE 'Internet. Inbound Email%' or name LIKE 'Internet. Inbound Call%')];
        CampaignMember[] currentMembers = [select id,CampaignId,ContactId from CampaignMember where contactid in: contacts and campaignid in: campaigns];
        CampaignMember[] cmembers = new list<CampaignMember>();
        for(Contact c:contacts){ 
            CampaignMember member = new CampaignMember();
            for(Campaign cp : campaigns){
                if(c.Communication_Channel__c == 'Referral' && cp.name.startswith('Referral')){
                    member.CampaignId = cp.id;
                    member.ContactId = c.id;
                    member.Status = c.LeadSource;
                }else if(c.Communication_Channel__c.startswith('CC -') && cp.name.startswith('Customer Care. New Contact')){
                    member.CampaignId = cp.id;
                    member.ContactId = c.id;
                    member.Status = c.LeadSource;
                }else if(c.Communication_Channel__c.startswith('Inbound Call') && cp.name.startswith('Internet. Inbound Call')){
                    member.CampaignId = cp.id;
                    member.ContactId = c.id;
                    member.Status = c.LeadSource;
                }else if(c.Communication_Channel__c.startswith('Inbound Email') && cp.name.startswith('Internet. Inbound Email')){
                    member.CampaignId = cp.id;
                    member.ContactId = c.id;
                    member.Status = c.LeadSource;
                }else if ((!c.Communication_Channel__c.startswith('Inbound Email') && !c.Communication_Channel__c.startswith('Inbound Call') && !c.Communication_Channel__c.startswith('CC -') && c.Communication_Channel__c != 'Referral') && cp.name.startswith('Cold Call')){
                    member.CampaignId = cp.id;
                    member.ContactId = c.id;
                    member.Status = c.LeadSource;
                }
            }
            if(member.CampaignId != null){
                if(currentMembers.size() > 0){
                    CampaignMember[] matchlist = new list<CampaignMember>();
                    for(CampaignMember mem:currentMembers){
                        if(member.CampaignId == mem.CampaignId && member.ContactId == mem.ContactId){
                            matchlist.add(mem);
                        }
                    } 
                    if(matchlist.size() == 0){
                        cmembers.add(member);
                        member = new CampaignMember(); 
                    }
                }else{
                    cmembers.add(member);
                    member = new CampaignMember();
                }
            }
            
        }
        if(cmembers.size() > 0){
            insert cmembers;
        }
    }
    
    public static void checkPrimaryAdmin(Contact[] contacts){
        set<id> accountIds = new set<id>();
        for(Contact c:contacts){
            accountIds.add(c.AccountId);
        }
        Account[] contactAccounts = [SELECT id,Customer_Status__c from Account where id in: accountIds];
        for(Contact c:contacts){
            for(Account a:contactAccounts){
                if(c.AccountId == a.id){
                    if(a.Customer_Status__c == 'Active'){
                        c.Primary_Admin__c = true;
                    }
                }
            }
        }
    }
    
    public static void uncheckPrimaryAdmin(Contact[] contacts){
        set<id> accountIds = new set<id>();
        for(Contact c:contacts){
            accountIds.add(c.AccountId);
        }
        Account[] contactAccounts = [SELECT id,Customer_Status__c from Account where id in: accountIds];
        for(Contact c:contacts){
            for(Account a:contactAccounts){
                if(c.AccountId == a.id){
                    if(a.Customer_Status__c == 'Active'){
                        c.Primary_Admin__c = false;
                    }
                }
            }
        }
    }
    
    public static void updateSurveySentDate(Contact[] contacts){
        set<String> surveyNumbers = new set<String>();
        for (Contact c:contacts){
            surveyNumbers.add(c.Survey_Number__c);
        }
        Survey_Feedback__c[] surveys = new list<Survey_Feedback__c>();
        surveys = [Select id, Contact__c, Name From Survey_Feedback__c Where Contact__c in: contacts and Name in: surveyNumbers];
        
        if(surveys.size() > 0){
            for(Contact c:contacts){
                for(Survey_Feedback__c s:surveys){
                    if(c.id == s.Contact__c){
                        if(c.Survey_Number__c == s.Name){
                            s.Survey_Sent__c = true;
                            s.Survey_Sent_Date__c = c.Survey_Email_Sent_Date__c;
                        }
                    }
                }
            }
            update surveys;
        }
    }
    
    public static void accountNPSScoreUpdate(Contact[] contacts) {
        
        //Get out accounts
        set<id> acctIds = new set<id>();
        for (Contact con : contacts) {
            acctIds.add(con.AccountId);
        }
        Account[] accts = [SELECT id, Chemical_Management_NPS_Score__c, Chemical_Management_NPS_Ranking__c, Chemical_Management_NPS_Last_Survey_Date__c, Chemical_Management_Prev_NPS_Score__c
                           FROM Account
                           WHERE id IN : acctIds];
        Account[] updList = new list<Account>();
        
        //Loop through our contacts that are now primary admins
        for (Contact con : contacts) {
            
            //Match our account
            Account acct;
            for (Account a : accts) {
                if (a.id == con.AccountId) {
                    acct = a;
                }
            }
            
            //If the account already has an NPS score, we need to store than under the Prev field
            if (acct.Chemical_Management_NPS_Score__c != null) {
                acct.Chemical_Management_Prev_NPS_Score__c = acct.Chemical_Management_NPS_Score__c;
                setAccountNPSFields(acct, con);
                updList.add(acct);
            } else {
                if (con.Chemical_Management_NPS_Score__c != null) {
                    setAccountNPSFields(acct, con);
                    updList.add(acct);
                }
            }
        }
        
        //Update our accounts
        if (updList.size() > 0) {
            try {
                update updList;
            } catch(exception e) {
                salesforceLog.createLog('NPS Scores', true, 'surveyUtility', 'accountNPSScoreUpdate', string.valueOf(e));
            }
        }
    }
    
    public static void setAccountNPSFields(Account acct, Contact con) {
        //Fill out the current NPS info for the new primary admin on the account
        acct.Chemical_Management_NPS_Score__c = con.Chemical_Management_NPS_Score__c;
        acct.Chemical_Management_NPS_Last_Survey_Date__c = con.Chemical_Management_NPS_Last_Survey_Date__c;
        string ranking;
        if (con.Chemical_Management_NPS_Score__c != null) {
            if (con.Chemical_Management_NPS_Score__c < 7) {
                ranking = 'Detractor';
            } else if (con.Chemical_Management_NPS_Score__c < 9) {
                ranking = 'Passive';
            } else {
                ranking = 'Promoter';
            }
        }
        acct.Chemical_Management_NPS_Ranking__c = ranking;
    }
    
    public static void EnterpriseEmailCheck(Contact[] contacts) {
        string ecList = [SELECT Body
                         FROM StaticResource
                         WHERE Name = 'enterpriseEmailDomainList' LIMIT 1].Body.toString();
        if (ecList != null) {
            for (Contact c : contacts) {
                //To get email doamin: SUBSTITUTE(Email, LEFT(Email, FIND("@", Email)), NULL);  ///Domain could be null: does not break when tested in execute anon
                if (ecList.containsIgnoreCase(';' + c.Email_Domain__c + ';')) {
                    c.ES_Email_Domain__c = true;
                }else{
                    c.ES_Email_Domain__c = false;
                }  
            }
            
        }
        
    }
    
    //new billing contacts utiltity method
    public static void billContactEmail(Contact[] contacts) {
        
		EmailTemplate addTemp;
        EmailTemplate removeTemp;

        addTemp = [Select Id From EmailTemplate where Name='New Billing Contact Alert'];
        removeTemp = [Select Id From EmailTemplate where Name='Remove Billing Contact Alert'];
        
        String userName = UserInfo.getUserName();
        User activeUser = [Select Email From User where Username = : userName limit 1];
        String userEmail = activeUser.Email;

     	List<Messaging.SingleEmailMessage> emailList =  new List<Messaging.SingleEmailMessage>();
                
        for(Contact c : Contacts) {   
               
                List<string> toBilling = new List<string>();
                toBilling.add('billing@ehs.com'); 
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(toBilling);
				mail.setTargetObjectId(c.id);
                //stops contact from being sent an email
                mail.setTreatTargetObjectAsRecipient(false);
              	mail.setWhatId(c.Id);
                if(c.Billing_Contact__c == TRUE){
                    mail.setTemplateId(addTemp.Id);
                }else{
                    mail.setTemplateId(removeTemp.Id);
                }
                mail.setSaveAsActivity(FALSE);
                mail.setUseSignature(FALSE);
                mail.setReplyTo(activeUser.Email);
                emailList.add(mail);
            } 
         Messaging.sendEmail(emailList);
        //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
        }
    
    
    public static void EHSContactCheck(set<id> ContactIds){
        Contact[] Con = [Select id, name, EHS_Contact__c, EHS_Contact_Type__c from Contact where id in: ContactIds];
        
        for(Contact c : con){
            c.EHS_Contact__c = true;
            c.EHS_Contact_Type__c = 'Key Stakeholder';
        }
        
        update con;
        
    }

    
//Method to clear old contact date fields on change of vpm task contact
    public static void clearContactDatesOldContact(VPM_Task__c[] oldTASKS){
        List<Contact> conDates = new List<Contact>();
        Set<Id> contactIds = new Set<Id>();
        //looping through passedin tasks and adding task related contact id to the set
        for(VPM_Task__c v: oldTASKS){
            contactIds.add(v.Contact__c);
        }
        //query to get contact fields
        Contact[] cont = [Select Id, Scheduled_Upgrade_Date__c, Upgrade_Email_1_To_Send_Date__c, Upgrade_Email_2_To_Send_Date__c, Upgrade_Email_3_To_Send_Date__c from Contact where Id in :contactIds];
        //looping through contacts and tasks
        for(Contact c :cont){
            for(VPM_Task__c v: oldTASKS){
                if(v.Contact__c == c.Id){
                    if(v.Email_Sequence_Number__c == 1){
                        c.Upgrade_Email_1_To_Send_Date__c = null;
                    }else if(v.Email_Sequence_Number__c == 2){
                        c.Upgrade_Email_2_To_Send_Date__c = null;
                    }else if(v.Email_Sequence_Number__c == 3){
                        c.Upgrade_Email_3_To_Send_Date__c = null;
                    }
                    if(c.Upgrade_Email_1_To_Send_Date__c == null && c.Upgrade_Email_2_To_Send_Date__c == null && c.Upgrade_Email_3_To_Send_Date__c == null){
                        c.Scheduled_Upgrade_Date__c = null;
                    }
                }
            }
            conDates.add(c);
        }
        update conDates; 
    }
    
    // Method to update new contact dates on change of vpm task contact 
    public static void updateNewContactDates(VPM_Task__c [] newTasks){
        List<Contact> cDates = new List<Contact>();
        //making set of contact Ids and Project Ids
        Set<Id> ContactIds = new Set<Id>();
        Set<Id> ProjectIds = new Set<Id>();
        //looping through VPM Tasks and adding task related contact and project ids to the set
        for(VPM_Task__c T :newTasks){
            ContactIds.add(T.Contact__c);
        }
        for(VPM_Task__c t :newTasks){
            ProjectIds.add(t.VPM_Project__c);
        }
        //Initializing the Map
        Map<Id, Date> mapProjectDate = new Map<Id, Date>();
        //Quering contact and vpm project fields from set Ids
        Contact[] cont = [Select Id, Scheduled_Upgrade_Date__c, Upgrade_Email_1_To_Send_Date__c, Upgrade_Email_2_To_Send_Date__c, Upgrade_Email_3_To_Send_Date__c from Contact where Id in :contactIds];
        VPM_Project__c[] proj = [Select Id, Scheduled_Upgrade_Date__c from VPM_Project__c where Id IN :ProjectIds];
        for(VPM_Project__c P :Proj){
            //Associating the specified value to the specified key in the map.
            mapProjectDate.put(P.Id, P.Scheduled_Upgrade_Date__c);
        }
        for(Contact c :cont){
            for(VPM_Task__c v: newTasks){                
                if(v.Contact__c == c.Id){
                    c.Scheduled_Upgrade_Date__c = mapProjectDate.get(v.VPM_Project__c);
                    if(v.Email_Sequence_Number__c == 1){
                        c.Upgrade_Email_1_To_Send_Date__c = v.Start_Date__c;
                    }else if(v.Email_Sequence_Number__c == 2){
                        c.Upgrade_Email_2_To_Send_Date__c = v.Start_Date__c;
                    }else if(v.Email_Sequence_Number__c == 3){
                        c.Upgrade_Email_3_To_Send_Date__c = v.Start_Date__c;
                    }
                }
            }
            cDates.add(c);
        }
        update cDates; 
    }


}