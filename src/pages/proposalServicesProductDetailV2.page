<apex:page id="proposalHQProductDetail" showHeader="FALSE" sidebar="FALSE" renderAS="PDF" standardController="Quote__c" standardStylesheets="false">
    <apex:stylesheet value="{!URLFOR($Resource.proposalPrintResource, 'default.css')}"/>
    <body style="page-break-after:always;" class="customerCare">
        <div class="header">
            <h1>MSDS Services</h1>
            <p>
                MSDSonline, a <em>VelocityEHS</em> solution, offers a range of services to help you build and maintain a compliant electronic MSDS management program.
            </p>
        </div>
        <div class="sidebar">
            <div class="content">
                <div class="logo">
                    <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/logo-msdsonline.png')}" styleClass="msdsonlineLogo" width="234" height="59" />        
                </div>
                <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/MSDSservices-sidebar.jpg')}" styleClass="customerCarePhoto" width="319" height="179" />        
                <h2>Benefits of MSDSonline</h2>
                <p>
                    <span class="careFeature">Manage the Influx of Safety Data Sheets</span><br/>
                    OSHA’s adoption of GHS has resulted in an MSDS churn that will remake your entire chemical library. We make it simple to organize and deploy MSDSs across your organization.
                </p>
                <p>
                    <span class="careFeature">Auto Updates</span><br/>
                    Our industry leading database of safety data sheets is available 24/7 and is updated with more than 80,000 new or revised MSDSs each month. Safety data sheets matching the ones in your library are automatically sent to you.
                </p>
                <p>
                    <span class="careFeature">Labeling Tools</span><br/>
                    Our system allows you to easily generate OSHA, GHS- and WHMIS-compliant workplace labels, as well custom labels to meet your unique needs.
                </p>
                <p>
                    <span class="careFeature">Safety Data Sheet Authoring</span><br/>
                    If you’re a chemical manufacturer, our MSDS authoring solutions can help you quickly and affordably meet your obligation to author and supply GHS-compliant safety data sheets to your downstream users.  
                </p>
                <p>
                    <span class="careFeature">Quick ROI &amp; Low Cost of Ownership</span><br/>
                    Our solutions eliminate time-consuming, manual administrative tasks. Plus, there’s no software to install or maintain; you get immediate access to new features and enhancements, as well as around-the-clock access to critical chemical information from all technologies, including mobile devices.
                </p>
            </div>
        </div>        
        <div class="pageContent">
            <span class="careFeature">MSDS Library Build</span><br/> 
            <strong>MSDS Scanning &amp; Indexing</strong> – Send us your paper safety data sheets and we’ll professionally scan, convert them to PDF-format, index, and post them to your secure online eBinder.*
            <p><strong>eBinder Valet</strong> – Send us your inventory list and we’ll build your MSDS library, contacting manufacturers to obtain the most recent versions, and adding the MSDSs to your secure online eBinder.* 
            </p>
            <p><strong>Data Conversion</strong> – If your electronic MSDS data already exists, we can import and convert the information into our system, so you won’t lose any of the work you’ve already completed.
            </p>
            <p>
                <span class="careFeature">MSDS Updates/Verification</span><br/> 
                <strong>MSDS Updates</strong> – We’ll work with you to make sure your MSDS library is up-to-date and compliant. Send us periodic updates to your inventory and we’ll make sure your online eBinder* is reviewed and updated with the most recent MSDS available.
            </p> 
            <p><strong>MSDS Verification</strong> – We’ll periodically review your online eBinder* to verify that you have the latest versions, stamping the document with the verified date. If we find more recent versions, we’ll index those MSDSs and post them to your secure online eBinder.* 
            </p>
            <p>
                <span class="careFeature">Special Projects</span><br/> 
                <strong>MSDS Indexing</strong> – We'll index the MSDSs in your library on any information, beyond our standard fields, that is contained within the 16 sections of an MSDS.
            </p> 
            <p><strong>Custom Label Creation</strong> – If you have unique secondary labeling requirements, we’ll work with you to determine the specific content and size of label you require and then create a custom template that you can use directly from your MSDS management account.
            </p>
            <p><strong>Regulatory Cross-Referencing</strong> – Need to quickly identify and report on chemicals that appear on various watch lists? We’ll index the ingredients for your chemical products, and then compare those ingredients against various watch lists and flag your MSDSs appropriately.
            </p>
            <p>
                <span class="careFeature">On-Site Chemical Inventory Audit</span><br/> 
                We’ll come to your workplace and conduct a thorough audit of your site, so you can get a complete assessment of your hazardous chemical footprint.
            </p>
            <p>
                <span class="careFeature">Benefits</span><br/> 
                <strong>Reliable Implementation</strong> – Our approach ensures your implementation is seamless and requires little to no interaction from your IT department.
            </p> 
            <p><strong>Quick &amp; Affordable</strong> – We offer timely project turnaround and competitive rates.</p>
            <p><strong>Excellent Customer Care</strong> – Our dedicated support team takes pride in your satisfaction. You get ongoing access to implementation assistance, product traiing, general and technical support, and more!</p>
            <p>
                * <em>GM, HQ, or HQ RegXR account required</em>
            </p>
        </div>
        <div class="footer">
            Proprietary &amp; Confidential. &copy; Copyright MSDSonline Inc., DBA <em>VelocityEHS</em>. Do not distribute without authorized consent.
            <div class="pageNumber">
                Page <span class="pagenumber"/> of <span class="pagecount"/>
            </div>
        </div>
    </body>
</apex:page>