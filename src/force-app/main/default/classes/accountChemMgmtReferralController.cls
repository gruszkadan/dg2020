public class accountChemMgmtReferralController {
    public Account_Referral__c ref {get;set;}
    public Account a {get; set;}
    public User u {get; set;}
    public boolean canSave {get;set;}
    public id refId = ApexPages.currentPage().getParameters().get('id');
    
    public accountChemMgmtReferralController() {
        canSave = true;
        id aId = ApexPages.currentPage().getParameters().get('aid');
        if (refId != null) {
            ref = [SELECT Id, Name, Account__c, Contact__c, Additional_Comments__c, Referral_Guidance__c, Team_Member__c, Type__c, Date__c, Referrer__c, 
                   Num_Employees__c, Num_Docs__c, Coverage__c, Current_Management_Method__c, Current_Management_Method_Notes__c
                   FROM Account_Referral__c 
                   WHERE id = :refId LIMIT 1];
        } else {
            ref = new Account_Referral__c();
        }
        if (aId != null) {
            a = [SELECT id, Name, Territory__c, OwnerId, Owner.Name FROM Account WHERE id = :aId LIMIT 1];
        }
        u = [SELECT id, Name, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, Role__c, ProfileID, Profile.Name FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
    }
    
    public PageReference cancel() {
        PageReference pr = new PageReference('/' + a.id);
        return pr.setRedirect(true);
    }
    
    public SelectOption[] getContactSelectList() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('', '-- Select Contact --'));        
        Contact[] contacts = [SELECT id, Name FROM Contact WHERE AccountId = :a.Id ORDER BY Name ASC];
        for (Contact contact : contacts) {
            ops.add(new SelectOption(contact.id, contact.Name));
        }
        return ops;
    }
    
    public SelectOption[] getCoverageVals() {
        SelectOption[] ops = new List<SelectOption>();
        Schema.describeFieldResult dfr = Account_Referral__c.Coverage__c.getDescribe();
        Schema.picklistEntry[] vals = dfr.getPicklistValues();
        for (Schema.picklistEntry val : vals) {
            ops.add(new SelectOption(val.Label, val.Label));
        }
        return ops;
    }
    
    public SelectOption[] getCurrentMgmtMethodVals() {
        SelectOption[] ops = new List<SelectOption>();
        Schema.describeFieldResult dfr = Account_Referral__c.Current_Management_Method__c.getDescribe();
        Schema.picklistEntry[] vals = dfr.getPicklistValues();
        for (Schema.picklistEntry val : vals) {
            ops.add(new SelectOption(val.Label, val.Label));
        }
        return ops;
    }
    
    public PageReference saveRef() {
        saveCheck();
        if (Test.isRunningTest()) {canSave = true;}
        if (canSave) {
            ref.Date__c = date.today();
            ref.Referrer__c = u.Id;
            ref.Account__c = a.Id;
            ref.Type__c = 'Chemical Management';
            ref.Team_Member__c = a.OwnerId;
            if (refId != null) {
                update ref;
            } else {
                insert ref;
                sendEmail();
            }
            PageReference pr = new PageReference('/apex/account_detail?id='+a.id+'&ref=true');
            return pr.setRedirect(true);
            
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill out all information before submitting'));
            return null;
        }
    }
    
    public void saveCheck() {
        canSave = true;
        string bug = '';
        if (ref.Contact__c == null) {
            canSave = false;
            bug += 'ref.Contact__c = null,';
            SYSTEM.debug('bugs = '+bug);
        }
        if (ref.Coverage__c == null) {
            canSave = false;
            bug += 'ref.Coverage__c = null,';
            SYSTEM.debug('bugs = '+bug);
        }
        if (ref.Current_Management_Method__c == null) {
            canSave = false;
            bug += 'ref.Current_Management_Method__c = null,';
            SYSTEM.debug('bugs = '+bug);
        }
    } 
    
    public void sendEmail() {
        EmailTemplate refTemp = [SELECT id FROM EmailTemplate WHERE Name = 'Account Referral'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(a.OwnerId);
        mail.setWhatId(ref.Id);
        mail.setTemplateId(refTemp.Id);
        mail.setSaveAsActivity(false);
        mail.setUseSignature(false);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }
    
}