public class ownershipNotificationScheduled implements Schedulable{   
    public void execute(SchedulableContext SC) {
        Messaging.SingleEmailMessage[] notificationEmails = new list <Messaging.SingleEmailMessage>();
        Ownership_Change_Notification__c[] notifications = new list<Ownership_Change_Notification__c>();
        Set <String> ownerEmails = new set<string>(); 
        notifications = [SELECT id,Name,Account_ID__c,Account_Name__c,New_Owner_Email__c,New_Owner_Name__c FROM Ownership_Change_Notification__c];
        //for testing only
       
        
        if(notifications.size() > 0){
            for(Ownership_Change_Notification__c n:notifications){
                ownerEmails.add(n.New_Owner_Email__c);
            }
            for(String e:ownerEmails){
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                String body = 'The following account(s) and/or lead(s) have been transferred to you:<br/><br/>'+
                    +'<table style="width:100%;text-align:left;border-collapse:collapse;"><tr><td style="font-weight:bold;border-bottom: 1px solid #ddd;border-top: 1px solid #ddd;padding:5px">'+
                    +'Name</td><td style="font-weight:bold;border-bottom: 1px solid #ddd;border-top: 1px solid #ddd;padding:5px">Link</td></tr>';
                
                
                email.setToAddresses(e.split(';'));
                
                email.setSubject('Ownership Change Notification');
                for(Ownership_Change_Notification__c n:notifications){
                    if(e == n.New_Owner_Email__c){
                        body += '<tr><td style="border-bottom: 1px solid #ddd;padding:5px">'+n.Account_Name__c+'</td>'+
                            +'<td style="border-bottom: 1px solid #ddd;padding:5px">'+
                            +'<a href="'+System.URL.getSalesforceBaseURL().toExternalForm()+'/'+n.Account_ID__c+'">Click Here</a></td></tr>';
                    }
                }
                body += '</table>';
                email.setHtmlBody(body);
                notificationEmails.add(email);
            }
            Messaging.sendEmail(notificationEmails);
            try {
                delete notifications;
            } catch(exception e) {
                salesforceLog.createLog('Ownership Change Notification', true, 'ownershipNotificationScheduled', 'execute', string.valueOf(e));
            }
        }
    }
}