@isTest
private class testSalesPerformanceItemOrderUtility {
    
    private static testmethod void test1() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            Account testAccount = new Account(Name = 'testAccount');
            insert testAccount;
            
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            DateTime d = date.today();
            String monthName = d.format('MMMMM');
            String year = d.format('yyyy');
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = year;
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = year;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = year;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Month__c = monthName;
            s.Year__c = year; 
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            test.startTest();
            
            Order_Item__c oi = createOrderItem('TestREP', 'test1');
            insert oi;
            Sales_Performance_Item__c[] newSPIs = [SELECT id
                                                   FROM Sales_Performance_Item__c
                                                   WHERE Order_Item__c = : oi.id];
            System.assert(newSPIs.size() == 1);
            oi.Invoice_Amount__c = 100;
            oi.Admin_Tool_Order_Status__c = 'B';
            oi.Account__c = testAccount.id;
            update oi;
            newSPIs = [SELECT id, Booking_Amount__c, Sales_Performance_Order__c
                       FROM Sales_Performance_Item__c
                       WHERE Order_Item__c = : oi.id];
            //System.assert(newSPIs[0].Booking_Amount__c == 100);
            System.assert(newSPIs[0].Sales_Performance_Order__c != null);
            
            test.stopTest();
            
        }
        
    }
    
    private static testmethod void test2() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            rep.Group__c = 'Enterprise Sales';
            update rep;
            
            DateTime d = date.today();
            String monthName = d.format('MMMMM');
            String year = d.format('yyyy');
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = year;
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = year;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = year;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Month__c = monthName;
            s.Year__c = year;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            test.startTest();
            Account newAccount = new Account();
            newAccount.Name = 'Test Account';
            newAccount.Customer_Status__c ='Active';
            newAccount.Demo_Setter1__c = rep.id;
            insert newAccount;
            
            newAccount.Licensing_Base_Price__c = '4149';
            update newAccount;
            test.stopTest();
        }
    }
    
    private static testmethod void test3() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            User assoc = util.createUser('TestAs', 'assoc', vp, dir, mgr);
            insert assoc;
            
            rep.Group__c = 'Enterprise Sales';
            update rep;
            
            assoc.Group__c = 'Enterprise Sales';
            update assoc;
            
            DateTime d = date.today();
            String monthName = d.format('MMMMM');
            String year = d.format('yyyy');
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = year;
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = year;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = year;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Month__c = monthName;
            s.Year__c = year;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            Sales_Performance_Summary__c assocSPS = new Sales_Performance_Summary__c();
            assocSPS.OwnerId = assoc.id;
            assocSPS.Manager__c = assoc.Sales_Performance_Manager_ID__c;
            assocSPS.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            assocSPS.Month__c = monthName;
            assocSPS.Year__c = year;
            assocSPS.Role__c = assoc.Role__c;
            assocSPS.Sales_Team_Manager__c = assoc.Sales_Performance_Manager_ID__c;
            assocSPS.Sales_Rep__c = assoc.id; 
            insert assocSPS;
            
            test.startTest();
            Sales_Incentive_Setting__c si = util.createIncentiveSettings('Enterprise Associate', rep.id, assoc.id);
            Sales_Incentive_Setting__c si2 = util.createIncentiveSettings('Enterprise Associate', mgr.id, rep.id);
            insert si;
            insert si2;
            Order_Item__c oi = util.createOrderItem('TestREP', 'test1');
            insert oi;
            Sales_Performance_Order__c[] assocSPO = [Select ID, Booking_Amount__c, Type__c from Sales_Performance_Order__c where OwnerID=: assoc.id];
            system.assertEquals(1, assocSPO.size());
            system.assertEquals('Associate Credit', assocSPO[0].Type__c);
            oi.Admin_Tool_Sales_Rep__c = 'TestMGR';
            update oi;
            test.stopTest();
        }
    }
    
    
    public static Order_Item__c createOrderItem(string repName, string orderItemId) {
        Order_Item__c oi = new Order_Item__c();
        oi.Name = 'Test';
        oi.Invoice_Amount__c = 10;
        oi.Order_Date__c = date.newInstance(2017, 01, 01);
        oi.Order_ID__c = 'test123';
        oi.Month__c = 'January';
        oi.Order_Item_ID__c = orderItemId;
        oi.Admin_Tool_Product_Name__c = 'HQ';
        oi.Year__c = '2017';
        oi.Contract_Length__c = 3;
        oi.AccountID__c = 'test123';
        oi.Term_Start_Date__c = date.today();
        oi.Term_End_Date__c = date.today().addYears(1);
        oi.Term_Length__c = 1;
        oi.Admin_Tool_Order_Status__c = 'A';
        oi.Contract_Number__c = 'test123';
        oi.Sales_Location__c = 'Chicago';
        oi.Admin_Tool_Sales_Rep__c = repName;
        oi.Admin_Tool_Order_Type__c = 'New';
        return oi;
    }
    
    public static testMethod void changeToRenewal() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            
            DateTime d = date.today();
            String monthName = d.format('MMMMM');
            String year = d.format('yyyy');
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = year;
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = year;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = year;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Month__c = monthName;
            s.Year__c = year;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            Order_Item__c oi = util.createOrderItem('TestREP', 'test1');
            insert oi;
            
            Sales_Performance_Item__c spi1 = [SELECT Id, Booking_Amount__c, Automated_Adjustment_Incentive__c, isRenewal__c FROM Sales_Performance_Item__c WHERE Order_Item__c = :oi.id];
            
            system.assert(spi1.Booking_Amount__c != 0);   
            system.assertEquals(spi1.Automated_Adjustment_Incentive__c, false);
            system.assertEquals(spi1.isRenewal__c, false);
            
            oi.Admin_Tool_Order_Type__c = 'Renewal';
            update oi;
            
            Sales_Performance_Item__c spi2 = [SELECT Id, Booking_Amount__c, Automated_Adjustment_Incentive__c, isRenewal__c FROM Sales_Performance_Item__c WHERE Order_Item__c = :oi.id];
            
            system.assert(spi2.Booking_Amount__c == 0);   
            system.assertEquals(spi2.Automated_Adjustment_Incentive__c, true);
            system.assertEquals(spi2.isRenewal__c, true);
            
        }
        
        
    }
    
    
    
    
    
}