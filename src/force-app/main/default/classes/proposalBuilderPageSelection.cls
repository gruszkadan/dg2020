public class proposalBuilderPageSelection {

    public list<ContentDocument> contentDocs {get;set;}
    public list<ContentWorkspace> LibraryList {get;set;}
    public Id stndDocsId {get;set;}
    public Id mrktDocsId {get;set;}
    public Id sellSheetDocsId {get;set;}
    public contentWrapper[] contentWrappers {get;set;}
    public contentWrapper[] selectedWrappers {get;set;}
    public boolean selected {get;set;}
    public id currPage {get;set;}
    public id quoteId {get;set;}
    public string JSONstring {get;set;}
    public Quote__c thisQuote {get;set;}
    public List<Id> pdtIds {get;set;}
    public Product2[] pdt2List {get;set;}
    public set<String> productDocs {get;set;}
    public Proposal_Generation_Settings__mdt[] additionalPages {get;set;} 
    public Id legalPageId {get;set;}
    public string orderedString {get;set;}
    public List<String> orderedList {get;set;}
    public List<jsonWrapper> orderedObjects {get;set;}
    public Set<string> previouslySelected {get;set;}
    public integer sellSheetCount {get;set;}
    public integer marketingCount {get;set;}
    public integer standardCount {get;set;}
    public boolean hasNeeds {get;set;}
    public Map<string, string> mapDynamicPages {get;set;}
    
    
    public proposalBuilderPageSelection(){
        
        quoteId = ApexPages.currentPage().getParameters().get('id');
        thisQuote = [Select Name, JSON_Proposal_Data__c from Quote__c where id = :quoteId]; 
        hasNeeds = proposalBuilderUtility.containsNeeds(thisQuote.JSON_Proposal_Data__c);
        //check if json is already saved and build list of ids and names
        getJsonData();

        LibraryList = [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Proposal Standard Content' OR Name = 'Proposal Marketing Content' OR Name = 'Solution Sheets'];
        
        for(ContentWorkspace cws : LibraryList){
            if(cws.Name == 'Proposal Standard Content'){
                stndDocsId = cws.Id;
            }
            if(cws.Name == 'Proposal Marketing Content'){
                mrktDocsId = cws.Id;
            }
            if(cws.Name == 'Solution Sheets'){
                sellSheetDocsId = cws.Id;
            }
        }
        
        Quote_Product__c[] quotePdts = [SELECT Product__r.Id, Product__r.Name, Product__r.Sell_Sheet__c FROM Quote_Product__c WHERE Quote__c = :quoteId];
        
        
        productDocs = new set<String>();
        if(quotePdts.size() > 0){
            for (Quote_Product__c qp: quotePdts){
                if( qp.Product__r.Sell_Sheet__c != NULL){
                    productDocs.add(qp.Product__r.Sell_Sheet__c);   
                }
            }
        }
        
        additionalPages = [Select Name__c, Required_Page__c, Content_Document_Id__c, Page_Description__c, Visualforce_Page__c from Proposal_Generation_Settings__mdt];
        
        contentWrappers = new List<contentWrapper>();
        selectedWrappers = new List<contentWrapper>();
        mapDynamicPages = new Map<string, string>();
        
        for(Proposal_Generation_Settings__mdt pgs: additionalPages){
            if(pgs.Content_Document_Id__c != NULL && pgs.Page_Description__c == 'Legal' ){
                legalPageId = pgs.Content_Document_Id__c;
            }else{
                mapDynamicPages.put(pgs.Name__c, pgs.Visualforce_Page__c);
                if(pgs.Required_Page__c == true){
                    selectedWrappers.add(new contentWrapper(pgs.Name__c, 'Required', pgs.Content_Document_Id__c, 'Standard'));
                }else{
                    if(thisQuote.JSON_Proposal_Data__c != NULL){
                        string tempCDID;
                        if(pgs.Content_Document_Id__c != NULL){
                            tempCDID = pgs.Name__c + '=' + pgs.Content_Document_Id__c;
                        }else{
                            tempCDID = pgs.Name__c + '=';
                            
                        }
                        if(previouslySelected.contains(tempCDID)){
                            selectedWrappers.add(new contentWrapper(pgs.Name__c, pgs.Page_Description__c, pgs.Content_Document_Id__c, 'Standard'));
                        }else{
                            contentWrappers.add(new contentWrapper(pgs.Name__c, pgs.Page_Description__c, pgs.Content_Document_Id__c, 'Standard')); 
                        }
                    }else{
                        contentWrappers.add(new contentWrapper(pgs.Name__c, pgs.Page_Description__c, pgs.Content_Document_Id__c, 'Standard'));   
                    }   
                }
            }
        }
        
        List<ContentWorkspaceDoc> standardANDmarketCWSD = [SELECT ContentDocumentId,ContentWorkspaceId
                                                           FROM ContentWorkspaceDoc 
                                                           WHERE ContentWorkspaceId = :stndDocsId OR ContentWorkspaceId = :mrktDocsId];
        
        Set<string> standardDocsIds = new set<String>();
        Set<string> marketingDocsIds = new set<String>();
        
        for(ContentWorkspaceDoc cwd: standardANDmarketCWSD){
            if(cwd.ContentWorkspaceId == stndDocsId){
                standardDocsIds.add(cwd.ContentDocumentId);
            }else{
                marketingDocsIds.add(cwd.ContentDocumentId);
            }
        }

        //get Content data
        contentDocs = [SELECT Description,Id,ParentId,Title, (SELECT Content_Document_Image__c, Proposal_Sell_Sheet_Name__c FROM ContentVersions where isLatest = true) 
                       FROM ContentDocument 
                       WHERE FileType = 'PDF' and Id != :legalPageId and (Id IN :productDocs OR Id IN :standardDocsIds OR Id IN :marketingDocsIds) order by Title asc ];
        
        Set<id> contentDocIdsDeDupeList  = new Set<id>();
        //sort out content data
        for (ContentDocument cd: contentDocs){           
            if(cd.ContentVersions[0].Content_Document_Image__c != NULL){
                string wrapperName = '';
                if(cd.ContentVersions[0].Proposal_Sell_Sheet_Name__c != NULL){
                    wrapperName = cd.ContentVersions[0].Proposal_Sell_Sheet_Name__c;
                }else{
                    wrapperName = cd.Title;
                }
               string tempCDID = wrapperName + '=' + cd.Id;  

                if(productDocs.contains(cd.Id)){                    
                    for(Quote_Product__c qp: quotePdts){  
                        if(qp.Product__r.Sell_Sheet__c == cd.Id && !contentDocIdsDeDupeList.contains(cd.id)){
                            if(thisQuote.JSON_Proposal_Data__c != NULL){
                                if(previouslySelected.contains(tempCDID)){
                                    selectedWrappers.add(new contentWrapper(wrapperName, cd.Description, cd.Id, 'Sell Sheet'));
                                }else{
                                    contentWrappers.add(new contentWrapper(wrapperName, cd.Description, cd.Id, 'Sell Sheet'));
                                }   
                            }else{
                                selectedWrappers.add(new contentWrapper(wrapperName, cd.Description, cd.Id, 'Sell Sheet'));
                            }
                            contentDocIdsDeDupeList.add(cd.Id);
                        }
                    }
                }else{  
                    if(standardDocsIds.contains(cd.Id)){
                        if(thisQuote.JSON_Proposal_Data__c != NULL && previouslySelected.contains(tempCDID)){
                            selectedWrappers.add(new contentWrapper(wrapperName, cd.Description, cd.Id, 'Standard'));
                        }else{
                            contentWrappers.add(new contentWrapper(wrapperName, cd.Description, cd.Id, 'Standard'));
                        }
                    }else{
                        if(marketingDocsIds.contains(cd.Id)){
                            if(thisQuote.JSON_Proposal_Data__c != NULL && previouslySelected.contains(tempCDID)) {
                                selectedWrappers.add(new contentWrapper(wrapperName, cd.Description, cd.Id, 'Marketing'));
                            }else{
                                contentWrappers.add(new contentWrapper(wrapperName, cd.Description, cd.Id, 'Marketing'));
                            }
                        }
                    } 
                }
            }
        }
        
        
        contentWrappers.sort();
        
        if(thisQuote.JSON_Proposal_Data__c != NULL){       
            
            contentWrapper[] templist = new list<contentWrapper>();
            for(jsonWrapper jwrap: orderedObjects){
                for(contentWrapper wrap: selectedWrappers){
                    if(jwrap.Identifier == wrap.uniqueId){
                        wrap.sortOrder = jwrap.Order;
                    }
                }
            }
            
        }
        selectedWrappers.sort();  
    }
    
    
    
    public void getJsonData(){
        if(thisQuote.JSON_Proposal_Data__c != NULL){
            orderedObjects = new List<jsonWrapper>();
            previouslySelected = new set<string>();
            JSONParser parser = JSON.createParser(thisQuote.JSON_Proposal_Data__c);
            while (parser.nextToken() != null) { 
                if (parser.getCurrentToken() == JSONToken.START_ARRAY) {       
                    while (parser.nextToken() != null) {
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            jsonWrapper j = (jsonWrapper)parser.readValueAs(jsonWrapper.class);
                            j.identifier = j.Name + '=' + j.Id;
                            previouslySelected.add(j.Name + '=' + j.Id);
                            
                            orderedObjects.add(j);
                        }
                    }
                }
            }
        }      
    }
    
    
    public void setSelectValue(){
        String uniqueIdentifier = system.currentpagereference().getparameters().get('currPage');
        String currPageName = system.currentpagereference().getparameters().get('currPageName');
        Boolean selected = Boolean.valueOf(system.currentpagereference().getparameters().get('selected'));
        
        if(selected == true){
            for(Integer i = 0;i < contentWrappers.size(); i++){
                if(contentWrappers.get(i).uniqueId == uniqueIdentifier){
                    selectedWrappers.add(new contentWrapper(contentWrappers.get(i).Name, contentWrappers.get(i).Description, contentWrappers.get(i).docId, contentWrappers.get(i).Type));
                    contentWrappers.remove(i);
                }
            } 
        }else{
            for(Integer i = 0;i < selectedWrappers.size(); i++){
                if(selectedWrappers.get(i).uniqueId == uniqueIdentifier){
                    contentWrappers.add(new contentWrapper(selectedWrappers.get(i).Name, selectedWrappers.get(i).Description, selectedWrappers.get(i).docId, selectedWrappers.get(i).Type));
                    contentWrappers.sort();
                    selectedWrappers.remove(i);
                }
            }            
        }
    }
    
    
    public pageReference Next(){
        System.debug(mapDynamicPages);
        List<String> orderedList = orderedString.split(','); 
        
        JSONGenerator gen = JSON.createGenerator(false); 
        gen.writeStartArray();
        for (integer i=0; i<orderedList.size(); i++) {
            gen.writeStartObject();
            string docPhrase = orderedList[i];
            if(docPhrase.substring(docPhrase.length() - 1 ) == '='){
                gen.writeStringField('Id', '');
                gen.writeStringField('vfPage', mapDynamicPages.get(docPhrase.split('=')[0]));
            }else{
                gen.writeStringField('Id', docPhrase.split('=')[1]);
                gen.writeStringField('vfPage', '');
            }
            gen.writeStringField('Name', docPhrase.split('=')[0]);
            gen.writeStringField('identifier', docPhrase);
            gen.writeNumberField('Order', i+1);
            gen.writeEndObject();
        }
        //add in legal page
        gen.writeStartObject();
        gen.writeStringField('Id', legalPageId);
        gen.writeStringField('Name', 'Legal');
        gen.writeStringField('identifier', 'Legal='+legalPageId);
        gen.writeNumberField('Order', orderedList.size()+1);
        gen.writeStringField('vfPage', '');
        gen.writeEndObject();
        JSONstring = gen.getAsString();
        thisQuote.JSON_Proposal_Data__c = '{"jsonWrapper": ' + JSONstring + '}';
        try{
            update thisQuote;
        }
        catch(Exception e) {
            ApexPages.addMessages(e);            
        }
        return new PageReference('/apex/proposal_builder_cover_page?id=' + QuoteId); 
    }
    
    
    public class jsonWrapper{
        public string Name {get;set;}
        public string Id {get;set;}
        public integer Order {get;set;}
        public string identifier {get;set;}
        public string vfPage {get;set;}
    }
    
    
    //wrapper 
    //description is set to 'Required' if document is required to be selected
    public class contentWrapper implements Comparable {
        public string type {get;set;}
        public string name {get;set;}
        public string description {get;set;}
        public id docId {get;set;}
        public integer sortOrder {get;set;}
        public string uniqueId {get;set;}
        
        public contentWrapper(string xName, string xDescription, id xdocId, string xType){
            name = xName;
            description = xDescription;
            docId = xDocId; 
            type = xType; 
            sortOrder = null;
            if(xDocId == null){
                uniqueId = xName + '='; 
            }else{
                uniqueId = xName+ '=' + docId;
            }
        }
        public Integer compareTo(Object compareTo) {
            if(sortOrder == NULL){
                return name.compareTo(((contentWrapper)compareTo).name); 
            }else{
                Integer returnValue = 0;
                if (sortOrder > ((ContentWrapper)compareTo).sortOrder) {
                    returnValue = 1;
                }
                if (sortOrder < ((ContentWrapper)compareTo).sortOrder){
                    returnValue = -1;
                }
                return returnValue; 
            }
        }
    }

}