public class cloneQuote {
    private Quote__c quote;
    public Quote_Product__c[] shoppingCart {get;set;}
    public Quote__c q {get;set;}
    public Quote__c oldQuote {get;set;}
    public String oldQuoteQuery {get;set;}
    public String oldQuoteID {get;set;}
    public String quoteProductsQuery {get;set;}
    
    public cloneQuote(ApexPages.StandardController stdController) {
        oldQuoteID = ApexPages.currentPage().getParameters().get('id');
        
        if ( oldQuoteID != null ) {
            fetchOldQuote();
        }
        
        this.q = new Quote__c();
    }
    
    public PageReference fetchOldQuote(){
        String SobjectApiName = 'Quote__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        oldQuoteQuery = 'select ' + commaSepratedFields + ' from ' +  SobjectApiName + ' where ID =\''+ oldQuoteID + '\'';
        this.oldQuote = Database.query(oldQuoteQuery);        
        return null;
    }
    
    public void archiveOldQuote()
    {
        if(this.oldQuote != null)
        {
            this.oldQuote.Status__c = 'Archived';
            update this.oldQuote;
        }
    }
    
    
    public PageReference createQuote() {
        if ( this.q != null ) {     
            Quote_Product__c[] items = getQuoteItems();
            
            this.q.Account__c = oldQuote.Account__c;
            this.q.Additional_Needs__c = oldQuote.Additional_Needs__c;
            this.q.Bundled_Products__c = oldQuote.Bundled_Products__c;
            this.q.Contact__c = oldQuote.Contact__c;
            this.q.Contract_Length__c = oldQuote.Contract_Length__c;
            this.q.Count_of_Employees__c = oldQuote.Count_of_Employees__c;
            this.q.Customer_Needs__c = oldQuote.Customer_Needs__c;
            this.q.GPO__c = oldQuote.GPO__c;
            this.q.Healthcare_Customer__c = oldQuote.Healthcare_Customer__c;
            this.q.Mark_Archived__c = oldQuote.Mark_Archived__c;
            this.q.Need_Access_Database__c = oldQuote.Need_Access_Database__c;
            this.q.Need_Account__c = oldQuote.Need_Account__c;
            this.q.Need_Authoring__c = oldQuote.Need_Authoring__c;
            this.q.Need_Chemical_Approval__c = oldQuote.Need_Chemical_Approval__c;
            this.q.Need_Chemical_Inventory_Management__c = oldQuote.Need_Chemical_Inventory_Management__c;
            this.q.Need_Chemical_Spill__c = oldQuote.Need_Chemical_Spill__c;
            this.q.Need_Classification_of_Chemicals__c = oldQuote.Need_Classification_of_Chemicals__c;
            this.q.Need_Create_Bar_Codes__c = oldQuote.Need_Create_Bar_Codes__c;
            this.q.Need_Create_Electronic_Library__c = oldQuote.Need_Create_Electronic_Library__c;
            this.q.Need_Database__c = oldQuote.Need_Database__c;
            this.q.Need_eBinder__c = oldQuote.Need_eBinder__c;
            this.q.Need_Eliminate_Scan__c = oldQuote.Need_Eliminate_Scan__c;
            this.q.Need_Environmental_Reporting__c = oldQuote.Need_Environmental_Reporting__c;
            this.q.Need_Fax_Back__c = oldQuote.Need_Fax_Back__c;
            this.q.Need_Generate_Inventory_List__c = oldQuote.Need_Generate_Inventory_List__c;
            this.q.Need_GHS_Labeling__c = oldQuote.Need_GHS_Labeling__c;
            this.q.Need_GHS_Solution__c = oldQuote.Need_GHS_Solution__c;
            this.q.Need_GHS_Training__c = oldQuote.Need_GHS_Training__c;
            this.q.Need_GHS_Transition__c = oldQuote.Need_GHS_Transition__c;
            this.q.Need_Hazard_Label__c = oldQuote.Need_Hazard_Label__c;
            this.q.Need_Implement_Labeling__c = oldQuote.Need_Implement_Labeling__c;
            this.q.Need_Incident_Management__c = oldQuote.Need_Incident_Management__c;
            this.q.Need_Library_Backup__c = oldQuote.Need_Library_Backup__c;
            this.q.Need_Manage_SDS__c = oldQuote.Need_Manage_SDS__c;
            this.q.Need_Mitigate_Human_Error__c = oldQuote.Need_Mitigate_Human_Error__c;
            this.q.Need_Mitigate_Time__c = oldQuote.Need_Mitigate_Time__c;
            this.q.Need_Mobile__c = oldQuote.Need_Mobile__c;
            this.q.Need_MSDS_Available_to_Customers__c = oldQuote.Need_MSDS_Available_to_Customers__c;
            this.q.Need_On_Site_Chem_Inventory__c = oldQuote.Need_On_Site_Chem_Inventory__c;
            this.q.Need_Ongoing_Maint__c = oldQuote.Need_Ongoing_Maint__c;
            this.q.Need_Ongoing_Maintenance__c = oldQuote.Need_Ongoing_Maintenance__c;
            this.q.Need_Onsite_Chem_Inventory__c = oldQuote.Need_Onsite_Chem_Inventory__c;
            this.q.Need_Print_Secondary_Labels__c = oldQuote.Need_Print_Secondary_Labels__c;
            this.q.Need_Regulatory_Cross_Reference__c = oldQuote.Need_Regulatory_Cross_Reference__c;
            this.q.Need_Right_to_Know_Access__c = oldQuote.Need_Right_to_Know_Access__c;
            this.q.Need_Safety_Tool_Kit__c = oldQuote.Need_Safety_Tool_Kit__c;
            this.q.Need_SDS_Accurate__c = oldQuote.Need_SDS_Accurate__c;
            this.q.Need_Tier_Two_Reporting__c = oldQuote.Need_Tier_Two_Reporting__c;
            this.q.Need_Track_Containers__c = oldQuote.Need_Track_Containers__c;
            this.q.Need_Training__c = oldQuote.Need_Training__c;
            this.q.Need_Transition_GHS_Labels__c = oldQuote.Need_Transition_GHS_Labels__c;
            this.q.Need_Unified_System__c = oldQuote.Need_Unified_System__c;
            this.q.Need_Update_SDS__c = oldQuote.Need_Update_SDS__c;
            this.q.Need_Update_Verification__c = oldQuote.Need_Update_Verification__c;
            this.q.Opportunity__c = oldQuote.Opportunity__c;
            this.q.OwnerID = oldQuote.OwnerID;
            this.q.Page_GHS_Adoption_Timeline__c = oldQuote.Page_GHS_Adoption_Timeline__c;
            this.q.Page_HazCom_2012_Revision__c = oldQuote.Page_HazCom_2012_Revision__c;
            this.q.Page_Key_Compliance__c = oldQuote.Page_Key_Compliance__c;
            this.q.Page_Managing_the_GHS_Transition__c = oldQuote.Page_Managing_the_GHS_Transition__c;
            this.q.Page_Needs_Requirements__c = oldQuote.Page_Needs_Requirements__c;
            this.q.Page_Product_Detail_Pages__c = oldQuote.Page_Product_Detail_Pages__c;
            this.q.Page_ROI_Benefits__c = oldQuote.Page_ROI_Benefits__c;
            this.q.Page_ROI_Figures__c = oldQuote.Page_ROI_Figures__c;
            this.q.Page_Why_Go_Electronic__c = oldQuote.Page_Why_Go_Electronic__c;
            this.q.Price_Book__c = oldQuote.Price_Book__c;
            this.q.Travelers_Customer__c = oldQuote.Travelers_Customer__c;
            this.q.Campaign__c = oldQuote.Campaign__c;
            this.q.VPPPA_Customer__c = oldQuote.VPPPA_Customer__c;
            this.q.Department__c = oldQuote.Department__c;
            this.q.Renewal__c = oldQuote.Renewal__c;
            this.q.Type__c = oldQuote.Type__c;
            this.q.Subscription_Model__c  = oldQuote.Subscription_Model__c;
            this.q.Implementation_Model__c = oldQuote.Implementation_Model__c;
            this.q.Promotion__c = oldQuote.Promotion__c;
            this.q.Currency__c = oldQuote.Currency__c;
            this.q.Currency_Rate__c = oldQuote.Currency_Rate__c;
            this.q.Currency_Rate_Date__c = oldQuote.Currency_Rate_Date__c;
            this.q.Licensing_Covered_by_Another_Location__c = oldQuote.Licensing_Covered_by_Another_Location__c;
            this.q.Multiple_Location_Indexing__c = oldQuote.Multiple_Location_Indexing__c;
            this.q.Account_Management_Event__c = oldQuote.Account_Management_Event__c;                
            insert this.q;
            
            
            
            List<Quote_Product__c> quoteItems = new List<Quote_Product__c>();
            
            for( Quote_Product__c item : items ) {
                Quote_Product__c newItem = new Quote_Product__c();
                newItem.Add_On_Product__c = item.Add_On_Product__c;
                newItem.Approval_Required_by__c  =  item.Approval_Required_by__c;
                if(this.oldQuote.Status__c == 'Expired' && item.Approval_Status__c =='Approved'){
                    newItem.Approval_Status__c = 'Not Submitted';
                } else {
                    newItem.Approval_Status__c = item.Approval_Status__c;
                }
                newItem.Auth_Consulting_Service__c = item.Auth_Consulting_Service__c;
                newItem.Auth_Countries_of_Use__c = item.Auth_Countries_of_Use__c;
                newItem.Auth_Date_Quote_Required__c = item.Auth_Date_Quote_Required__c;
                newItem.Auth_End_Date__c = item.Auth_End_Date__c;
                newItem.Auth_Estimated_Start_Date__c = item.Auth_Estimated_Start_Date__c;
                newItem.Auth_Expected_End_Date__c = item.Auth_Expected_End_Date__c;
                newItem.Auth_Hourly_Rate__c = item.Auth_Hourly_Rate__c;
                newItem.Auth_Identical_Product_Grouping__c = item.Auth_Identical_Product_Grouping__c;
                newItem.Auth_Need_Addtl_Info__c = item.Auth_Need_Addtl_Info__c;
                newItem.Auth_Original_Manufacturer__c = item.Auth_Original_Manufacturer__c;
                newItem.Auth_Other_Reg_Format__c = item.Auth_Other_Reg_Format__c;
                newItem.Auth_Other_Service__c = item.Auth_Other_Service__c;
                newItem.Auth_Overage__c = item.Auth_Overage__c;
                newItem.Auth_Pilot_Program__c = item.Auth_Pilot_Program__c;
                newItem.Auth_Project_Breakdown__c = item.Auth_Project_Breakdown__c;
                newItem.Auth_Project_Data__c = item.Auth_Project_Data__c;
                newItem.Auth_Project_Terms__c = item.Auth_Project_Terms__c;
                newItem.Auth_Regulatory_Format__c = item.Auth_Regulatory_Format__c;
                newItem.Auth_Rush_Quote__c = item.Auth_Rush_Quote__c;
                newItem.Auth_Service_Requested__c = item.Auth_Service_Requested__c;
                newItem.Auth_Start_Date__c = item.Auth_Start_Date__c;
                newItem.Auth_Total_Authoring_Labels__c = item.Auth_Total_Authoring_Labels__c;
                newItem.Auth_Total_Docs__c = item.Auth_Total_Docs__c;
                newItem.Auth_Total_Hours__c = item.Auth_Total_Hours__c;
                newItem.Auth_Total_Translated_Docs__c = item.Auth_Total_Translated_Docs__c;
                newItem.Auth_Total_Translated_Labels__c = item.Auth_Total_Translated_Labels__c;
                newItem.Auth_Trade_Secrets_Proprietary__c = item.Auth_Trade_Secrets_Proprietary__c;
                newItem.Auth_Translation_Lang__c = item.Auth_Translation_Lang__c;
                newItem.Bundled_Indexing_Display_Type__c = item.Bundled_Indexing_Display_Type__c;
                newItem.Configured__c  =  item.Configured__c;
                newItem.CS_Scope__c  =  item.CS_Scope__c;
                newItem.Custom_Index_Custom_Internal__c = item.Custom_Index_Custom_Internal__c;
                newItem.Custom_Index_Internal_Part_Num__c = item.Custom_Index_Internal_Part_Num__c;
                newItem.Custom_Index_MSDS_Num__c = item.Custom_Index_MSDS_Num__c;
                newItem.Custom_Index_Other__c = item.Custom_Index_Other__c;
                newItem.Custom_Index_Synonym__c = item.Custom_Index_Synonym__c;
                newItem.Custom_Label_Information__c  =  item.Custom_Label_Information__c;
                newItem.Custom_Quote__c = item.Custom_Quote__c;
                newItem.Custom_Services_Product_Additional_Info__c = item.Custom_Services_Product_Additional_Info__c;
                newItem.Description__c  =  item.Description__c;
                newItem.Discount__c  =  item.Discount__c;
                newItem.Discount_Reason__c = item.Discount_Reason__c;
                newItem.eBinder_Replication_Account__c = item.eBinder_Replication_Account__c;
                newItem.ERS_Battery_Shipper__c = item.ERS_Battery_Shipper__c;
                newItem.ERS_Custom_Script__c = item.ERS_Custom_Script__c;
                newItem.ERS_Dedicated_800_or_Country_Phone__c = item.ERS_Dedicated_800_or_Country_Phone__c;
                newItem.ERS_eBinder_Build_Type__c = item.ERS_eBinder_Build_Type__c;
                newItem.ERS_Fedex_Account__c = item.ERS_Fedex_Account__c;
                newItem.ERS_Fedex_Account_Number__c = item.ERS_Fedex_Account_Number__c;
                newItem.Estimated_Price_Applied__c = item.Estimated_Price_Applied__c;
                newItem.Minimum_Price_Applied__c = item.Minimum_Price_Applied__c;
                newItem.Expected_Delivery_Date__c = item.Expected_Delivery_Date__c;
                newItem.Edit_Qty__c = item.Edit_Qty__c;
                Newitem.Is_Demonstration_Project__c = item.Is_Demonstration_Project__c;
                newItem.Global_Fax_Back_Translations__c = item.Global_Fax_Back_Translations__c;
                newItem.Group_ID__c = item.Group_ID__c;
                newItem.Group_Name__c = item.Group_Name__c;
                newItem.Group_Parent_ID__c = item.Group_Parent_ID__c;
                newItem.Has_Overage_Amount__c = item.Has_Overage_Amount__c;
                newItem.List_Price__c  =  item.List_Price__c;
                newItem.Name__c  =  item.Name__c;
                newItem.New_Revenue_Price__c = item.New_Revenue_Price__c;
                newItem.Notes__c  =  item.Notes__c;
                newItem.ODT_Flex_Max_Num_of_Enrollments__c = item.ODT_Flex_Max_Num_of_Enrollments__c;
                newItem.ODT_num_of_Courses__c = item.ODT_num_of_Courses__c;
                newItem.ODT_Scorm_Num_of_Courses__c = item.ODT_Scorm_Num_of_Courses__c;
                newItem.ODT_Server_Lic_Name_of_Clients_LMS__c = item.ODT_Server_Lic_Name_of_Clients_LMS__c;
                newItem.ODT_Server_Lic_num_of_Courses__c = item.ODT_Server_Lic_num_of_Courses__c;
                newItem.ODT_Server_Lic_Num_of_Employees__c = item.ODT_Server_Lic_Num_of_Employees__c;
                newItem.ODTDev_Stand_Alone__c  =  item.ODTDev_Stand_Alone__c;
                newItem.One_Time__c = item.One_Time__c;
                newItem.Onsite_Contact_Info_Address__c = item.Onsite_Contact_Info_Address__c;
                newItem.Onsite_Escort__c  =  item.Onsite_Escort__c;
                newItem.Onsite_Hourly_Rate__c = item.Onsite_Hourly_Rate__c;
                newItem.Onsite_Locations__c  =  item.Onsite_Locations__c;
                newItem.Onsite_Maximum_Hours__c = item.Onsite_Maximum_Hours__c;
                newItem.Onsite_Maximum_Travel_Expense__c = item.Onsite_Maximum_Travel_Expense__c;
                newItem.Onsite_of_Products__c  =  item.Onsite_of_Products__c;
                newItem.Onsite_Special_Precautionary__c  =  item.Onsite_Special_Precautionary__c;
                newItem.Onsite_Square_Footage__c  =  item.Onsite_Square_Footage__c;
                newItem.Onsite_Timeline__c  =  item.Onsite_Timeline__c;
                newItem.Other_Discount_Reason__c = item.Other_Discount_Reason__c;
                newItem.Overage_List_Price__c = item.Overage_List_Price__c;
                newItem.Overage_Quote_Price__c = item.Overage_Quote_Price__c;
                newItem.PB_Address__c  =  item.PB_Address__c;
                newItem.PB_Collate__c  =  item.PB_Collate__c;
                newItem.PB_Colored_sheet_separator__c  =  item.PB_Colored_sheet_separator__c;
                newItem.PB_Delivery_priority__c  =  item.PB_Delivery_priority__c;
                newItem.PB_Enclose_in_sheet_protector__c  =  item.PB_Enclose_in_sheet_protector__c;
                newItem.PB_How_Many_Copies__c  =  item.PB_How_Many_Copies__c;
                newItem.PB_Include_An_Index__c  =  item.PB_Include_An_Index__c;
                newItem.PB_Include_Archived_Docs__c  =  item.PB_Include_Archived_Docs__c;
                newItem.PB_Include_Inactive_Documents__c = item.PB_Include_Inactive_Documents__c;
                newItem.PB_Location_Names__c = item.PB_Location_Names__c;
                newItem.PB_Print_Layout__c  =  item.PB_Print_Layout__c;
                newItem.PB_Print_on_both_sides__c = item.PB_Print_on_both_sides__c;
                newItem.PB_Staple_each_document__c  =  item.PB_Staple_each_document__c;
                newItem.PB_Three_hole_punch__c  =  item.PB_Three_hole_punch__c;
                newItem.PB_Total_Num_of_Documents__c = item.PB_Total_Num_of_Documents__c;
                newItem.PB_What_Orders_to_Print__c  =  item.PB_What_Orders_to_Print__c;
                newItem.PB_Which_Documents__c  =  item.PB_Which_Documents__c;
                newItem.PI__c = item.PI__c;
                newItem.Premium_Applied__c  =  item.Premium_Applied__c;
                newItem.Price_Override__c  =  item.Price_Override__c;
                if(item.Bundle__c == false){
                newItem.Pricing_Display__c  =  item.Pricing_Display__c;
                }
                newItem.PricebookID__c = item.PricebookID__c;
                newItem.PricebookEntryId__c  =  item.PricebookEntryId__c;
                newItem.Print_Group__c  =  item.Print_Group__c;
                newItem.Product__c  =  item.Product__c;
                newItem.Product_Print_Name__c = item.Product_Print_Name__c;
                newitem.Product_Sub_Type__c = item.Product_Sub_Type__c;
                newItem.Product_Types__c = item.Product_Types__c;
                newItem.Product_Type_Custom__c = item.Product_Type_Custom__c;
                newItem.Quantity__c  =  item.Quantity__c;
                newItem.Quantity_Calculation__c = item.Quantity_Calculation__c;
                newItem.Quantity_Field_Name__c  =  item.Quantity_Field_Name__c;
                newItem.Quote__c = this.q.id;
                newItem.Quote_Price__c  =  item.Quote_Price__c;
                newItem.Renewal_Amount__c = item.Renewal_Amount__c;
                newItem.Show_Year_List__c  =  item.Show_Year_List__c;
                newItem.Standard_MSDS_Inclusion_1__c = item.Standard_MSDS_Inclusion_1__c;
                newItem.Standard_MSDS_Inclusion_2__c = item.Standard_MSDS_Inclusion_2__c;
                newItem.Standard_MSDS_Inclusion_3__c = item.Standard_MSDS_Inclusion_3__c;
                newItem.Total_Line_Value__c  =  item.Total_Line_Value__c;
                newItem.Type__c = item.Type__c;
                newItem.Update_Services_Type__c  =  item.Update_Services_Type__c;
             
                newItem.Y1_Discount__c  =  item.Y1_Discount__c;
                newItem.Y1_List_Price__c  =  item.Y1_List_Price__c;
                newItem.Y1_Quote_Price__c  =  item.Y1_Quote_Price__c;
                newItem.Y1_Travelers_Discount_Applied__c = item.Y1_Travelers_Discount_Applied__c;
              
                newItem.Y2_Discount__c  =  item.Y2_Discount__c;
                newItem.Y2_List_Price__c  =  item.Y2_List_Price__c;
                newItem.Y2_Quote_Price__c  =  item.Y2_Quote_Price__c;
                newItem.Y2_Travelers_Discount_Applied__c = item.Y2_Travelers_Discount_Applied__c;
                
                newItem.Y3_Discount__c  =  item.Y3_Discount__c;
                newItem.Y3_List_Price__c  =  item.Y3_List_Price__c;
                newItem.Y3_Quote_Price__c  =  item.Y3_Quote_Price__c;
                newItem.Y3_Travelers_Discount_Applied__c = item.Y3_Travelers_Discount_Applied__c;
                
                newItem.Y4_Discount__c  =  item.Y4_Discount__c;
                newItem.Y4_List_Price__c  =  item.Y4_List_Price__c;
                newItem.Y4_Quote_Price__c  =  item.Y4_Quote_Price__c;
                newItem.Y4_Travelers_Discount_Applied__c = item.Y4_Travelers_Discount_Applied__c;

                newItem.Y5_Discount__c  =  item.Y5_Discount__c;
                newItem.Y5_List_Price__c  =  item.Y5_List_Price__c;
                newItem.Y5_Quote_Price__c  =  item.Y5_Quote_Price__c;
                newItem.Y5_Travelers_Discount_Applied__c = item.Y5_Travelers_Discount_Applied__c;
                newItem.Year_1__c  =  item.Year_1__c;
                newItem.Year_2__c  =  item.Year_2__c;
                newItem.Year_3__c  =  item.Year_3__c;
                newItem.Year_4__c  =  item.Year_4__c;
                newItem.Year_5__c  =  item.Year_5__c;
                newItem.Promo_Y1__c = item.Promo_Y1__c;
                newItem.Promo_Y2__c = item.Promo_Y2__c;
                newItem.Promo_Y3__c = item.Promo_Y3__c;
                newItem.Promo_Y4__c = item.Promo_Y4__c;
                newItem.Promo_Y5__c = item.Promo_Y5__c;
                newItem.Approval_Limit__c = item.Approval_Limit__c;
                newItem.Director_Approval_Limit__c = item.Director_Approval_Limit__c;
                newItem.VP_Approval_Limit__c = item.VP_Approval_Limit__c;
                newItem.Promo_Discount_Percent__c = item.Promo_Discount_Percent__c;
                newItem.Special_Indexing_Fields__c = item.Special_Indexing_Fields__c;
                newItem.Executed_Services__c = item.Executed_Services__c;
                newItem.Auth_Language_Docs__c = item.Auth_Language_Docs__c;
                newItem.Auth_Num_of_Products__c = item.Auth_Num_of_Products__c;
                newItem.Auth_Regulatory_Format_Docs__c = item.Auth_Regulatory_Format_Docs__c;
                newItem.Auth_Translation_Turnaround_Time__c = item.Auth_Translation_Turnaround_Time__c;
                newItem.Auth_Turn_Around_Time__c = item.Auth_Turn_Around_Time__c;
                newItem.Authoring_Locations__c = item.Authoring_Locations__c;
                newItem.Language_Support_Languages__c = item.Language_Support_Languages__c;
                newItem.Pre_Scheduled_Training_Num_of_Modules__c = item.Pre_Scheduled_Training_Num_of_Modules__c;
                newItem.Pre_Scheduled_Training_Modules__c = item.Pre_Scheduled_Training_Modules__c;
                newItem.Pre_Scheduled_Training_Invitations__c = item.Pre_Scheduled_Training_Invitations__c;
                newItem.Product_Pitched_By__c = item.Product_Pitched_By__c;
                newItem.Proposal_Terms__c = item.Proposal_Terms__c;
                newItem.Attachment__c = item.Attachment__c;
                newItem.Rush__c = item.Rush__c;
                newItem.Rush_Reason__c = item.Rush_Reason__c;
                newItem.Desired_Completion_Date__c = item.Desired_Completion_Date__c;
                newItem.Indexing_Language__c = item.Indexing_Language__c;
                newItem.Rush_Timeframe__c = item.Rush_Timeframe__c;
                newItem.Package_Parent_Id__c = item.Package_Parent_Id__c;
                quoteItems.add(newItem);
            }
            
            insert quoteItems;
            archiveOldQuote();
            return redirectToQuote();
        } else {
            return null;
        }
    }
    
    
    
    public PageReference redirectToQuote() {
        PageReference quotePage = new PageReference('/' + q.Id);
        return quotePage;
    }
    
    public Quote_Product__c[] getQuoteItems(){
        String SobjectApiName = 'Quote_Product__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        quoteProductsQuery = 'select ' + commaSepratedFields + ' from ' + SobjectApiName + ' where Quote__c =\''+ oldquote.ID+ '\'';
        Quote_Product__c[] items = Database.query(quoteProductsQuery);
        return items;
    }
}