public class adminToolSalesforceIDQueueable implements Queueable, Database.AllowsCallouts {
/**
* This is called from the adminToolSalesforceIDAPISend Scheduled Class
* This calls the Admin Tool Web Services to send Salesforce ID Data for Customers (AdminID__c Starts with a 4) to the Admin Tool
**/
    public void execute(QueueableContext context) {
        String salesforceEnvironment;
        String adminToolEnvironment;
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D300000001HEfEAM'){
            salesforceEnvironment = 'Production';
            adminToolEnvironment = 'Production';
        }else{
            salesforceEnvironment ='Sandbox';
            adminToolEnvironment = 'Staging';
        }
        adminToolUtility atUtility = new adminToolUtility();
        Account[] salesforceIds = atUtility.getSalesforceIDs();     
        adminToolWebServices atWS = new adminToolWebServices();
        atWS.sendAdminToolData('SalesforceID', salesforceIDs, adminToolEnvironment);
    }
}