public class caseNoteController {
    private Apexpages.StandardController controller; 
    public Case theCase {get;set;}
    public Case_Notes__c newNote {get;set;}
    public Case_Notes__c theNote {get;set;}
    public Case_Issue__c issue {get;set;}
    public Case_Issue__c noteIssue {get;set;}
    public Id caseId {get;set;}
    public Id issueId {get;set;}
    public Id noteID {get;set;}
    public String selectedissue {get;set;}
    public String issuelabel {get;set;}
    public String noteIssuelabel {get;set;}
    public User u {get;set;}
    public Boolean durationFieldRequired {get;set;}
    
    public caseNoteController(ApexPages.StandardController stdController) {
        
        caseId = ApexPages.currentPage().getParameters().get('CF00NS0000001ErJh_lkid');
        issueId = ApexPages.currentPage().getParameters().get('ci');
        noteID = ApexPages.currentPage().getParameters().get('ID');
        if(noteID != NULL){
            theNote = [Select ID, Case__c, Notes_Type__c, Product__c, Unable_login_gotomeeting__c, Call_Duration__c, Customer_Contacted__c,
                       Date_Work_Received__c, Num_of_Work_Received__c, Issue__c, Associated_Case_Issue__c from Case_Notes__c where ID=:noteID];
            theCase = [SELECT Id, Type, CaseNumber FROM Case WHERE Id=:theNote.Case__c LIMIT 1 ];
            if( theNote.Associated_Case_Issue__c != null ){
            noteIssue = [ SELECT Id, Product_In_Use__c, Product_Support_Issue_Locations__c, Product_Support_Issue__c FROM Case_Issue__c WHERE Id =: theNote.Associated_Case_Issue__c  LIMIT 1 ];
            noteIssuelabel = noteIssue.Product_In_Use__c+' - '+noteIssue.Product_Support_Issue_Locations__c+' - '+noteIssue.Product_Support_Issue__c;
            }
            caseID= theNote.Case__c;
        }
        if(noteID == NULL){
            theCase = [SELECT Id, Type FROM Case WHERE Id = :caseId LIMIT 1 ];
        }
        
        if( issueId != null ){
            issue = [ SELECT Id, Product_In_Use__c, Product_Support_Issue_Locations__c, Product_Support_Issue__c FROM Case_Issue__c WHERE Id =: issueId LIMIT 1 ];
            issuelabel = issue.Product_In_Use__c+' - '+issue.Product_Support_Issue_Locations__c+' - '+issue.Product_Support_Issue__c;
            newNote = new Case_Notes__c(
                Case__c = caseId,
                Associated_Case_Issue__c = issueId );
        }
        
        if( issueId == null ){
            newNote = new Case_Notes__c(Case__c = caseId);
        }
        selectedissue = issueId;
        
        durationFieldRequired = false;
      
        u = [Select Name, Managers_Name__c, Department__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

        //if Director rolls up to Scott or Meredith, mark boolean true to render required block
        if(u.Department__c == 'Customer Care' && (u.Managers_Name__c == 'Scott Alleman' || u.Managers_Name__c == 'Meredith Cywinski'
                                                || u.Managers_Name__c == 'Stephanie Luca' || u.Managers_Name__c == 'Emily Zaikis' || u.Managers_Name__c == 'Michael Bruffey')){
            durationFieldRequired = true;
        }
        
    }
    
    public List<SelectOption> getCaseIssues() {
        List<SelectOption> options = new List<SelectOption>();
        List<Case_Issue__c> issues = [ SELECT Id, Product_In_Use__c, Product_Support_Issue_Locations__c, Product_Support_Issue__c
                                      FROM Case_Issue__c WHERE Associated_Case__c = :caseId ORDER BY CreatedDate DESC ];
        for( Case_Issue__c ci : issues ){
            String theissue = ci.Product_In_Use__c+' - '+ci.Product_Support_Issue_Locations__c+' - '+ci.Product_Support_Issue__c;
            options.add( new SelectOption( ci.Id, theissue ) );
        }
        return options; 
    }
    public PageReference saveNewNote(){
        newNote.Case__c = caseid;
        if( issueId != null ){
            newNote.Associated_Case_Issue__c = selectedissue;
        }
        try{
            insert newNote;
            PageReference pr = Page.casedetail;
            pr.getParameters().put('id', caseId);
            pr.setRedirect(true);
            return pr;  
        }catch(exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, string.valueof(e));
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        
    }
    
    public PageReference saveNote(){
        theNote.Associated_Case_Issue__c = selectedissue;
        update theNote;
        PageReference pr = Page.casedetail;
        pr.getParameters().put('id', caseId);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference cancel(){
        PageReference pr = Page.casedetail;
        pr.getParameters().put('id', theCase.ID);
        pr.setRedirect(true);
        return pr;
    }
}