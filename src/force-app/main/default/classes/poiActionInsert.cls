public class poiActionInsert {
    
    //accepts a list of poiActionUtility.poiAction wrappers and turns them into poi actions, creates poi's if needed, and creates poi action queues if needed
    public static Product_Of_Interest_Action__c[] createPOIActions(poiActionUtility.poiAction[] wrappers) {
        
        //lists to hold all queries leads and contacts
        Lead[] leads = new list<Lead>();    
        Contact[] contacts = new list<Contact>();    
        
        set<Lead> leadsWithoutPOISet = new set<Lead>();
        set<Contact> contactsWithoutPOISet = new set<Contact>();
        
        //lists to hold new actions and new queues
        Product_Of_Interest_Action__c[] newPOIActions = new list<Product_Of_Interest_Action__c>();   
        Product_Of_Interest_Action_Queue__c[] newPOIActionQueues = new list<Product_Of_Interest_Action_Queue__c>();   
        
        //list to hold poiAtion wrappers
        poiAction[] poiActions = new list<poiAction>();    
        
        //list to hold the returned list of newly created poi's
        Product_Of_Interest__c[] newPOIs;   
        set<string> idSet = new set<string>();   
        
        //find our product action mappings
        // this will help determine whether or not an action should be created based on all the activities pulled back from marketo
        // this will also set the product, action, suite of interest, add to poi counter, and needs resolution fields on the action
        POI_Action_Mapping__c[] maps = [SELECT id, List_ID__c, Product__c, Action__c, Suite_Of_Interest__c, Add_To_POI_Counter__c, Needs_Resolution__c
                                        FROM POI_Action_Mapping__c];
        
        //loop through our marketoActions passed from the json class
        for (poiActionUtility.poiAction w : wrappers) {
            
            //loop through action maps
            // if a match is found, we know we need to create an action
            for (POI_Action_Mapping__c m : maps) {
                if (w.primaryAttributeValueId == m.List_ID__c) {
                    
                    //if our action is coming from salesforce, query against the sfId, otherwise use the leadId (marketo id)
                    if (w.sfId != null) {
                        idSet.add(w.sfId);
                    } else {
                        idSet.add(w.leadId);
                    }
                    //begin creating our poiAction wrappers
                    poiAction poia = new poiAction();
                    poia.marketoId = w.leadId;
                    poia.sfId = w.sfId;
                    poia.actionId = w.marketoGUID;
                    poia.listId = m.List_ID__c;
                    poia.product = m.Product__c;
                    poia.action = m.Action__c;
                    poia.suite = m.Suite_Of_Interest__c;
                    poia.addToPOICounter = m.Add_To_POI_Counter__c;
                    poia.needsResolution = m.Needs_Resolution__c;
                    
                    //add the wrapper to our list of wrappers
                    poiActions.add(poia);
                }
            }
        } 
        
        //if there are actions found, perform the following logic to create new actions
        if (poiActions.size() > 0) {
            
            //query for unconverted leads
            leads.addAll(queryLeads(idSet));
            
            //query for contacts
            contacts.addAll(queryContacts(idSet));
            
            //loop through our poiActions and match the unconverted leads and contacts
            for (poiAction poia : poiActions) {
                
                //if we have unconverted leads, loop through them and assign them
                if (leads.size() > 0) {
                    for (Lead l : leads) {
                        //match the marketoIds
                        if (l.Marketo_ID__c == poia.marketoId || l.id == poia.sfId) {
                            //set the lead
                            poia.lead = l;
                            //look for an existing poi record and assign it
                            if (l.Product_Of_Interests__r.size() == 1) {
                                poia.poi = l.Product_Of_Interests__r[0];
                            } else {
                                //if the poi record doesnt exist, add the lead to the list to create a new poi record later
                                leadsWithoutPOISet.add(l);
                            }
                        } 
                    }
                }
                
                //if we have contacts, loop through them and assign them
                if (contacts.size() > 0) {
                    for (Contact c : contacts) {
                        //match the marketoIds
                        if (c.Marketo_ID__c == poia.marketoId || c.id == poia.sfId) {
                            //set the contact
                            poia.contact = c;
                            //look for an existing poi record and assign it
                            if (c.Product_Of_Interests__r.size() == 1) {
                                poia.poi = c.Product_Of_Interests__r[0];
                            } else {
                                //if the poi record doesnt exist, add the contact to the list to create a new poi record later
                                contactsWithoutPOISet.add(c);
                            }
                        }
                    }
                }
            }
            
            //if we have any poiActions without pois, send to the poi insert class to create the pois
            if (leadsWithoutPOISet.size() > 0 || contactsWithoutPOISet.size() > 0) {
                
                //return the list of new poi records
                newPOIs = poiUtility.createPOI(leadsWithoutPOISet, contactsWithoutPOISet);
                
                //loop through our poiActions and if the poi is null, loop through our new pois and assign them
                for (poiAction poia : poiActions) {
                    if (poia.poi == null) {
                        for (Product_Of_Interest__c poi : newPOIs) {
                            
                            //if the poia has a contact assigned, match by the lead id
                            if (poia.contact != null) {
                                if (poi.Contact__c == poia.contact.id) {
                                    poia.poi = poi;
                                }
                            } else if (poia.lead != null) {
                                //if the poia has a lead assigned, match by contact id
                                if (poi.Lead__c == poia.lead.id) {
                                    poia.poi = poi;
                                }
                            }
                            
                        }
                    }
                }
            }
            
            //finally, loop again and create the actual product of interest actions
            for (poiAction poia : poiActions) {
                
                //if a poi is found, create the action and add to the insert list
                if (poia.poi != null) {
                    newPOIActions.add(newPOIAction(poia));
                } else {
                    newPOIActionQueues.add(newPOIActionQueue(poia));
                }
            }
            
            //send the list of new actions to the session class
            if (newPOIActions.size() > 0) {
                poiActionSession.insertSession(newPOIActions);
            }
            
            //finally insert our actions
            insertActionsAndQueues(newPOIActions, newPOIActionQueues);
            
        }
        return newPOIActions;
    }
    
    //method to query leads against passed marketoIds
    public static Lead[] queryLeads(set<string> idSet) {
        if (idSet.size() > 0) {
            return [SELECT id, Marketo_ID__c, POI_Stage_MSDS__c, POI_Stage_EHS__c, POI_Stage_AUTH__c, POI_Stage_ODT__c, POI_Stage_ERGO__c, POI_Status_MSDS__c, POI_Status_AUTH__c,
                    POI_Status_EHS__c, POI_Status_ERGO__c, POI_Status_ODT__c, POI_Status_Change_Date_AUTH__c, POI_Status_Change_Date_EHS__c, POI_Status_Change_Date_ERGO__c,
                    POI_Status_Change_Date_MSDS__c, POI_Status_Change_Date_ODT__c, Num_of_Unable_to_Connect_Tasks_AUTH__c, Num_Of_Unable_To_Connect_Tasks_EHS__c,
                    Num_Of_Unable_To_Connect_Tasks_ERGO__c, Num_Of_Unable_To_Connect_Tasks_MSDS__c, Num_Of_Unable_To_Connect_Tasks_ODT__c,
                    MSDS_Score__c, EHS_Score__c, AUTH_Score__c, ODT_Score__c, ERGO_Score__c,
                    OwnerId, EHS_Owner__c, Authoring_Owner__c, Online_Training_Owner__c, Ergonomics_Owner__c,
                    (SELECT id, POI_Status_MSDS__c, POI_Status_AUTH__c, POI_Status_EHS__c, POI_Status_ERGO__c, POI_Status_ODT__c,
                     POI_Stage_MSDS__c, POI_Stage_EHS__c, POI_Stage_AUTH__c, POI_Stage_ODT__c, POI_Stage_ERGO__c,
                     POI_Status_Change_Date_AUTH__c, POI_Status_Change_Date_EHS__c, POI_Status_Change_Date_ERGO__c, POI_Status_Change_Date_MSDS__c, POI_Status_Change_Date_ODT__c,
                     Num_of_Unable_to_Connect_Tasks_AUTH__c, Num_Of_Unable_To_Connect_Tasks_EHS__c, Num_Of_Unable_To_Connect_Tasks_ERGO__c, Num_Of_Unable_To_Connect_Tasks_MSDS__c, 
                     Num_Of_Unable_To_Connect_Tasks_ODT__c FROM Product_Of_Interests__r)
                    FROM Lead
                    WHERE isConverted = false 
                    AND (Marketo_ID__c IN : idSet 
                         OR id IN : idSet)
                    ORDER BY Marketo_ID__c ASC];
        } else {
            return null;
        } 
    }
    
    //method to query contacts against passed marketoIds
    public static Contact[] queryContacts(set<string> idSet) {
        if (idSet.size() > 0) {
            return [SELECT id, Marketo_ID__c, POI_Stage_MSDS__c, POI_Stage_EHS__c, POI_Stage_AUTH__c, POI_Stage_ODT__c, POI_Stage_ERGO__c, POI_Status_MSDS__c, POI_Status_AUTH__c,
                    POI_Status_EHS__c, POI_Status_ERGO__c, POI_Status_ODT__c, POI_Status_Change_Date_AUTH__c, POI_Status_Change_Date_EHS__c, POI_Status_Change_Date_ERGO__c,
                    POI_Status_Change_Date_MSDS__c, POI_Status_Change_Date_ODT__c, Num_of_Unable_to_Connect_Tasks_AUTH__c, Num_Of_Unable_To_Connect_Tasks_EHS__c,
                    Num_Of_Unable_To_Connect_Tasks_ERGO__c, Num_Of_Unable_To_Connect_Tasks_MSDS__c, Num_Of_Unable_To_Connect_Tasks_ODT__c, 
                    MSDS_Score__c, EHS_Score__c, AUTH_Score__c, ODT_Score__c, ERGO_Score__c,
                    Account.OwnerId, Account.EHS_Owner__c, Account.Authoring_Account_Owner__c, Account.Online_Training_Account_Owner__c, Account.Ergonomics_Account_Owner__c,
                    (SELECT id, POI_Status_MSDS__c, POI_Status_AUTH__c, POI_Status_EHS__c, POI_Status_ERGO__c, POI_Status_ODT__c, 
                     POI_Stage_MSDS__c, POI_Stage_EHS__c, POI_Stage_AUTH__c, POI_Stage_ODT__c, POI_Stage_ERGO__c, 
                     POI_Status_Change_Date_AUTH__c, POI_Status_Change_Date_EHS__c, POI_Status_Change_Date_ERGO__c, POI_Status_Change_Date_MSDS__c, POI_Status_Change_Date_ODT__c,
                     Num_of_Unable_to_Connect_Tasks_AUTH__c, Num_Of_Unable_To_Connect_Tasks_EHS__c, Num_Of_Unable_To_Connect_Tasks_ERGO__c, Num_Of_Unable_To_Connect_Tasks_MSDS__c, 
                     Num_Of_Unable_To_Connect_Tasks_ODT__c FROM Product_Of_Interests__r)
                    FROM Contact
                    WHERE Marketo_ID__c IN : idSet
                    OR id IN : idSet
                    ORDER BY Marketo_ID__c ASC];
        } else {
            return null;
        }
    }
    
    //method to find the stage based on the suite and lead/contact
    public static string findStage(poiAction poia) {
        string stage = '';
        if (poia.suite == 'MSDS Management') {
            stage = poia.poi.POI_Stage_MSDS__c;
        } else if (poia.suite == 'EHS Management') {
            stage = poia.poi.POI_Stage_EHS__c;
        } else if (poia.suite == 'MSDS Authoring') {
            stage = poia.poi.POI_Stage_AUTH__c;
        } else if (poia.suite == 'On-Demand Training') {
            stage = poia.poi.POI_Stage_ODT__c;
        } else if (poia.suite == 'Ergonomics') {
            stage = poia.poi.POI_Stage_ERGO__c;
        }
        return stage;
    }
    
    //method to find the Unable to Complete Tasks based on the suite and lead/contact
    public static decimal findUTC(poiAction poia) {
        decimal utc = NULL;
        if (poia.suite == 'MSDS Management') {
            utc = poia.poi.Num_Of_Unable_To_Connect_Tasks_MSDS__c;
        } else if (poia.suite == 'EHS Management') {
            utc = poia.poi.Num_Of_Unable_To_Connect_Tasks_EHS__c;
        } else if (poia.suite == 'MSDS Authoring') {
            utc = poia.poi.Num_of_Unable_to_Connect_Tasks_AUTH__c;
        } else if (poia.suite == 'On-Demand Training') {
            utc = poia.poi.Num_Of_Unable_To_Connect_Tasks_ODT__c;
        } else if (poia.suite == 'Ergonomics') {
            utc = poia.poi.Num_Of_Unable_To_Connect_Tasks_ERGO__c;
        }
        return utc;
    }
    
    //method to find the Last Status Change Dates based on the suite and lead/contact
    public static date findStatusChangeDate(poiAction poia) {
        date utc = NULL;
        if (poia.suite == 'MSDS Management') {
            utc = poia.poi.POI_Status_Change_Date_MSDS__c;
        } else if (poia.suite == 'EHS Management') {
            utc = poia.poi.POI_Status_Change_Date_EHS__c;
        } else if (poia.suite == 'MSDS Authoring') {
            utc = poia.poi.POI_Status_Change_Date_AUTH__c;
        } else if (poia.suite == 'On-Demand Training') {
            utc = poia.poi.POI_Status_Change_Date_ODT__c;
        } else if (poia.suite == 'Ergonomics') {
            utc = poia.poi.POI_Status_Change_Date_ERGO__c;
        }
        return utc;
    }
    
    //method called to create the action
    public static Product_Of_Interest_Action__c newPOIAction(poiAction poia) {
        Product_Of_Interest_Action__c newPoia = new Product_Of_Interest_Action__c();
        newPoia.Product_Of_Interest__c = poia.poi.id;
        if (poia.lead != null) {
            newPoia.Lead__c = poia.lead.id;
            id ownerId;
            if (poia.suite == 'MSDS Management') {
               ownerId = poia.lead.OwnerId;
            } else if (poia.suite == 'EHS Management') {
                ownerId = poia.lead.EHS_Owner__c;
            } else if (poia.suite == 'MSDS Authoring') {
                ownerId = poia.lead.Authoring_Owner__c;
            } else if (poia.suite == 'On-Demand Training') {
                ownerId = poia.lead.Online_Training_Owner__c;
            } else if (poia.suite == 'Ergonomics') {
                ownerId = poia.lead.Ergonomics_Owner__c;
            }
            if (ownerId == null) {
                ownerId = poia.lead.OwnerId;
            }
            newPoia.OwnerId = ownerId;
        } else {
            newPoia.Contact__c = poia.contact.id;
            id ownerId; 
            if (poia.suite == 'MSDS Management') {
                ownerId = poia.contact.Account.OwnerId;
            } else if (poia.suite == 'EHS Management') {
                ownerId = poia.contact.Account.EHS_Owner__c;
            } else if (poia.suite == 'MSDS Authoring') {
                ownerId = poia.contact.Account.Authoring_Account_Owner__c;
            } else if (poia.suite == 'On-Demand Training') {
                ownerId = poia.contact.Account.Online_Training_Account_Owner__c;
            } else if (poia.suite == 'Ergonomics') {
                ownerId = poia.contact.Account.Ergonomics_Account_Owner__c;
            }
            if (ownerId == null) {
                ownerId = poia.contact.Account.OwnerId;
            }
            newPoia.OwnerId = ownerId;
        }
        newPoia.Marketo_ID__c = poia.marketoId;
        newPoia.Action_ID__c = poia.actionId;
        newPoia.List_ID__c = poia.listId;
        newPoia.Product__c = poia.product;
        newPoia.Action__c = poia.action;
        newPoia.Product_Suite__c = poia.suite;
        newPoia.Add_to_POI_Counter__c = poia.addToPOICounter;
        newPoia.Needs_Resolution__c = poia.needsResolution;
        newPoia.Stage__c = findStage(poia);
        newPoia.Action_Status__c = findStatus(poia);
        newPoia.Number_of_Unable_to_Connect_Tasks__c = findUTC(poia);
        newPoia.Status_Changed_Date__c = findStatusChangeDate(poia);
        return newPoia;
    }
    
    //find the actions status based on the poi's status
    // if there is no status on the poi, its the first action for that suite. it should be open
    // if the status has "resolved" in the name, it means the previous session was closed. this new action should open a new session and be set to open
    // for anything else, it means there is a session open. take the poi's status
    public static string findStatus(poiAction poia) {
        string s = '';
        if (poia.suite == 'MSDS Management') {
            if (poia.poi.POI_Status_MSDS__c == null || poia.poi.POI_Status_MSDS__c.contains('Resolved')) {
                s = 'Open';
            } else {
                s = poia.poi.POI_Status_MSDS__c;
            }
        } else if (poia.suite == 'EHS Management') {
            if (poia.poi.POI_Status_EHS__c == null || poia.poi.POI_Status_EHS__c.contains('Resolved')) {
                s = 'Open';
            } else {
                s = poia.poi.POI_Status_EHS__c;
            }
        } else if (poia.suite == 'MSDS Authoring') {
            if (poia.poi.POI_Status_AUTH__c == null || poia.poi.POI_Status_AUTH__c.contains('Resolved')) {
                s = 'Open';
            } else {
                s = poia.poi.POI_Status_AUTH__c;
            }
        } else if (poia.suite == 'On-Demand Training') {
            if (poia.poi.POI_Status_ODT__c == null || poia.poi.POI_Status_ODT__c.contains('Resolved')) {
                s = 'Open';
            } else {
                s = poia.poi.POI_Status_ODT__c;
            }
        } else if (poia.suite == 'Ergonomics') {
            if (poia.poi.POI_Status_ERGO__c == null || poia.poi.POI_Status_ERGO__c.contains('Resolved')) {
                s = 'Open';
            } else {
                s = poia.poi.POI_Status_ERGO__c;
            }
        }
        return s;
    }
    
    //method called to create the queue
    public static Product_Of_Interest_Action_Queue__c newPOIActionQueue(poiAction poia) {
        Product_Of_Interest_Action_Queue__c newPoiaQ = new Product_Of_Interest_Action_Queue__c();
        //there is no Product_Of_Interest__c to assign here - thats why its in a queue
        newPoiaQ.Marketo_ID__c = poia.marketoId;
        newPoiaQ.Action_ID__c = poia.actionId;
        newPoiaQ.Product__c = poia.product;
        newPoiaQ.List_ID__c = poia.listId;
        newPoiaQ.Action__c = poia.action;
        newPoiaQ.Product_Suite__c = poia.suite;
        newPoiaQ.Add_to_POI_Counter__c = poia.addToPOICounter;
        newPoiaQ.Needs_Resolution__c = poia.needsResolution;
        return newPoiaQ;
    }
    
    //method to insert and poi actions and poi action queues
    public static void insertActionsAndQueues(Product_Of_Interest_Action__c[] newPOIActions, Product_Of_Interest_Action_Queue__c[] newPOIActionQueues) {
        if (newPOIActions.size() > 0) {
            try {
                upsert newPOIActions Action_ID__c;
            } catch(exception e) {
                salesforceLog.createLog('POI', true, 'poiActionInsert', 'createPOIActions', string.valueOf(e));
            }
        }
        
        if (newPOIActionQueues.size() > 0) {
            try {
                upsert newPOIActionQueues Action_ID__c;
            } catch(exception e) {
                salesforceLog.createLog('POI', true, 'poiActionInsert', 'createJSONActions', string.valueOf(e));
            }
        }
    }
    
    //class to hold poi action info
    // its easier to use a wrapper class here than write directly to the object as we dont know whether or not its going to be an action or a queue 
    public class poiAction {
        public string marketoId;
        public string sfId;
        public string actionId;
        public string listId;
        public Lead lead;
        public Contact contact;
        public Product_Of_Interest__c poi;
        public string product;
        public string action;
        public string suite;
        public boolean addToPOICounter;
        public boolean needsResolution;
    }
    
    
}