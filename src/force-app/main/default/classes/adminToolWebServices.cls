public class adminToolWebServices {
    
    //Method to call the Admin Tool API
    public static string fetchAdminToolData(string reportName, string startDate, string environment){
        //Data Check Validation
        boolean dataCheck;
        //Inital Data Check Value
        dataCheck = FALSE;
        //Was the Request a Success
        boolean success;
        //Base of the Endpoint URL
        string endPointURL ='';
        //Data Retrivied
        string adminToolJSONData;
        //Admin Tool Setttings
        List<Admin_Tool_API_Settings__c> atSettings;
        //Init the JSON Generator
        JSONGenerator gen = JSON.createGenerator(true);
        
        //Data Checks
        //Do we have a Report Name
        if(reportName == NULL){
            salesforceLog.createLog('Admin Tool API',TRUE,'adminToolWebServices','fetchAdminToolData','Missing Report Name');
            dataCheck = FALSE;
        } else {
            dataCheck = TRUE;
        }
        if(environment == NULL){
            salesforceLog.createLog('Admin Tool API',TRUE,'adminToolWebServices','fetchAdminToolData','Missing Environment');
            dataCheck = FALSE;
        } else {
            dataCheck = TRUE;
        }
        
        //Get the Admin Tool Settings
        if(dataCheck == TRUE){
            atSettings = [SELECT Username__c, Password__c, Endpoint_User_Data_Report__c, Endpoint_First_Responders_Report__c, Endpoint_Customer_Data_Report__c,
                          Endpoint_Bookings_Report__c, Endpoint_Customer_Data_Report_8_5__c FROM Admin_Tool_API_Settings__c where Environment__c = :environment LIMIT 1];
        }
        //Set the Correct Endpoint Based on the Report Name
        if(atSettings.size() == 1){
            if(reportName =='CustomerData'){
                endPointURL = atSettings[0].Endpoint_Customer_Data_Report__c;
            } else if(reportName =='CustomerData85') {
                endPointURL = atSettings[0].Endpoint_Customer_Data_Report_8_5__c;
            } else if(reportName =='UserData'){
                endPointURL = atSettings[0].Endpoint_User_Data_Report__c;
            } else if(reportName =='FirstResponders'){
                endPointURL = atSettings[0].Endpoint_First_Responders_Report__c;
            } else if(reportName =='BookingsReport'){
                endPointURL = atSettings[0].Endpoint_Bookings_Report__c;
            }
        } else {
            salesforceLog.createLog('Admin Tool API',TRUE,'adminToolWebServices','fetchAdminToolData','Unable to Query Admin Tool API Settings');
        }
        
        //Init the HTTP Request Class
        HttpRequest req = new HttpRequest();
        //Init the HTTP Response Class
        HttpResponse res = new HttpResponse();
        //Init the HTTP Class
        Http http = new Http();
        
        if(endPointURL != NULL){
            //Write the Body of the API Call
            gen.writeStartObject();
            gen.writeStringField('Username', atSettings[0].Username__c);
            gen.writeStringField('Password', atSettings[0].Password__c);
            gen.writeStringField('StartDate', startDate);        
            gen.writeEndObject();
            
            //Put into JSON
            string jsonBody = gen.getAsString();
            
            //Build the Request
            req.setEndpoint(endPointURL);
            req.setTimeout(90000);
            req.setMethod('POST');
            req.setBody(jsonBody);
            req.setHeader('Content-Type','application/json');
            //Send the Request
            res = http.send(req);
            
            //Get the Body of the Response        
            if(res.getStatusCode() == 200) {
                //Check the Sucess of the Request
                success = adminToolAPIJSONUtility.adminToolErrors(res.getBody(),'adminToolWebServices','fetchAdminToolData');
                if (test.isRunningTest()) {
                    success = true;
                }
                //If the request was a success
                if(success){
                    //Pass the JSON Body to the JSON Parser
                    adminToolJSONData = res.getBody();
                    //Write the JSON to the Log
                    salesforceLog.createLog('Admin Tool API',FALSE,'adminToolWebServices','fetchAdminToolData',adminToolJSONData);
                }  
            } else {
                //If there was an error, write to the Salesforce Log
                salesforceLog.createLog('Admin Tool API',TRUE,'adminToolWebServices','fetchAdminToolData',res.getBody());
            }
        }
        return adminToolJSONData;
    }
         
     //Method to call the Admin Tool API
    public void sendAdminToolData(string reportName, list<sObject> records, string environment){
        //Data Check Validation
        boolean dataCheck;
        //Inital Data Check Value
        dataCheck = FALSE;
        //Was the Request a Success
        boolean success;
        //Base of the Endpoint URL
        string endPointURL ='';
        //Admin Tool Setttings
        List<Admin_Tool_API_Settings__c> atSettings;
        //Init the JSON Generator
        JSONGenerator gen = JSON.createGenerator(true);
        //Wrapper for DFU Export
        List<Case> cases;
        adminToolAPIJSONUtility.dfuExport[] cList = new list <adminToolAPIJSONUtility.dfuExport>();
        //Wrapper for SalesforceIDs
        List<Account> accounts;
        adminToolAPIJSONUtility.accountID[] aList = new list <adminToolAPIJSONUtility.accountID>();
        
        
        
        //Data Checks
        //Do we have a Report Name
        if(reportName == NULL){
            salesforceLog.createLog('Admin Tool API',TRUE,'adminToolWebServices','sendAdminToolData','Missing Report Name');
            dataCheck = FALSE;
        } else {
            dataCheck = TRUE;
        }
        if(environment == NULL){
            salesforceLog.createLog('Admin Tool API',TRUE,'adminToolWebServices','sendAdminToolData','Missing Environment');
            dataCheck = FALSE;
        } else {
            dataCheck = TRUE;
        }
        
        //Get the Admin Tool Settings
        if(dataCheck == TRUE){
            atSettings = [SELECT Username__c, Password__c, Endpoint_SalesforceID__c, Endpoint_DFU_Export__c FROM Admin_Tool_API_Settings__c where Environment__c = :environment LIMIT 1];
        }
        //Set the Correct Endpoint Based on the Report Name
        if(atSettings.size() == 1){
            if(reportName =='SalesforceID'){
                endPointURL = atSettings[0].Endpoint_SalesforceID__c;
            } else if(reportName =='DFU Export'){
                endPointURL = atSettings[0].Endpoint_DFU_Export__c;
            } 
        } else {
            salesforceLog.createLog('Admin Tool API',TRUE,'adminToolWebServices','sendAdminToolData','Unable to Query Admin Tool API Settings');
        }
        
        if(reportName =='DFU Export'){
            cases = records;
            for(Case c : cases) {
                adminToolAPIJSONUtility.dfuExport dfuw = new adminToolAPIJSONUtility.dfuExport();
                dfuw.AccountID = c.Account.AdminID__c;
                dfuw.URL = 'https://na29.salesforce.com/'+c.id;
                dfuw.Status = c.DFU_Status__c;
                dfuw.OptOut = c.Opt_Out__c;
                dfuw.CaseOwner = c.Owner.Name;
                dfuw.CloseDate = string.valueOf(c.ClosedDate.format());
                cList.add(dfuw);
            }
        } else if(reportName =='SalesforceID'){
            accounts = records;
            for(Account a : accounts) {
                adminToolAPIJSONUtility.accountID aiw = new adminToolAPIJSONUtility.accountID();
                aiw.AccountID = integer.valueOf(a.AdminID__c);
                aiw.SalesforceID = a.id;
                aList.add(aiw);
            }
        }
        
        //Init the HTTP Request Class
        HttpRequest req = new HttpRequest();
        //Init the HTTP Response Class
        HttpResponse res = new HttpResponse();
        //Init the HTTP Class
        Http http = new Http();
        if(endPointURL != NULL){
            //Write the Body of the API Call
            gen.writeStartObject();
            gen.writeStringField('Action', reportName);
            gen.writeFieldName('Credentials');
            gen.writeStartObject();
            gen.writeStringField('Username',atSettings[0].Username__c); 
            gen.writeStringField('Password',atSettings[0].Password__c);
            gen.writeEndObject();
            gen.writeFieldName('Results');
            if(reportName =='DFU Export'){
            	gen.writeObject(cList);
            } else if(reportName =='SalesforceID'){
                gen.writeObject(aList);
            }
            
            gen.writeEndObject();
            
            //Put into JSON
            string jsonBody = gen.getAsString();
            
            //Build the Request
            req.setEndpoint(endPointURL);
            req.setTimeout(45000);
            req.setMethod('POST');
            req.setBody(jsonBody);
            req.setHeader('Content-Type','application/json');
            
            //Send the Request
            res = http.send(req);
            salesforceLog.createLog('Admin Tool API',FALSE,'adminToolWebServices','sendAdminToolData',endPointURL + jsonBody);
            //Get the Body of the Response        
            if(res.getStatusCode() == 200) {
                //Check the Sucess of the Request
                success = adminToolAPIJSONUtility.adminToolErrors(res.getBody(),'adminToolWebServices','sendAdminToolData');
                if (test.isRunningTest()) {
                    success = true;
                }
            } else {
                //If there was an error, write to the Salesforce Log
                salesforceLog.createLog('Admin Tool API',TRUE,'adminToolWebServices','sendAdminToolData',res.getBody());
            }
        }
            
    }
}