public class orderItemDetailController {
    
    public ID oiId {get;set;}
    public List<Sales_Performance_Item__c> relatedSalesItems {get;set;} 
    public User uOwner {get;set;}
    public User uCreatedBy {get;set;}
    public User uLastModifiedBy {get;set;}
    
    
    public orderItemDetailController(ApexPages.StandardController stdController){             
        oiId = ApexPages.currentPage().getParameters().get('id');   
        relatedSalesItems = [SELECT Name, Month__c, Sales_Rep_Sales_Performance_Summary__c, Sales_Performance_Order__c FROM Sales_Performance_Item__c WHERE Order_Item__c =: oiId];
        
        //Below was used to obtain the pictures displayed
        Order_Item__c orderItem;
        List<User> uList;
        
        orderItem = [SELECT OwnerId, CreatedById, LastModifiedById FROM Order_Item__c WHERE Order_Item__c.id =:oiID]; 
        uList = [SELECT FullPhotoUrl FROM User WHERE Id = :orderItem.OwnerId OR Id = :orderItem.CreatedById OR Id = :orderItem.LastModifiedById];
        for (user u : uList) {
            IF (u.id == orderItem.OwnerId){
                uOwner = u;
            }
            IF (u.id == orderItem.CreatedById){
                uCreatedBy = u;
            }
            IF (u.id == orderItem.LastModifiedById){
                uLastModifiedBy = u;
            }
        }     
        
    }
    
    
}