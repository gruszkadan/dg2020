@isTest
private class testQuoteProductBundle {
    static testMethod void quoteProductBundle_test1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
        
        
        // Lookup an existing item
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = '01t80000002NON4';
        myItem.PricebookEntryId__c ='01u80000006eap9';
        myItem.Quantity__c = 1;
        myItem.Y1_Quote_Price__c = 2.50;
        myItem.Approval_Required_by__c = 'Sales VP';
        myItem.Quote__c = newQuote.Id;
        myItem.Name__c = 'HQ Account';
        insert myItem;
        
        Quote_Product__c myItem1 = new Quote_Product__c();
        myItem1.Product__c = '01t80000002NONJ';
        myItem1.PricebookEntryId__c ='01u80000006eapOAAQ';
        myItem1.Quantity__c = 1;
        myItem1.Y1_Quote_Price__c = 2.50;
        myItem1.Approval_Required_by__c = 'Sales VP';
        myItem1.Quote__c = newQuote.Id;
        myItem1.Name__c = 'Domestic Fax-Back';
        insert myItem1;               
        
        ApexPages.currentPage().getParameters().put('Id', newQuote.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteProductBundle myController = new quoteProductBundle(testController);
        myController.createProductBundle();
        myController.mainProductSelection();
        myController.editMainProductSelection();
        myController.returnToQuote();
        myController.saveAddAnother();
        
    }
    
    static testmethod void quoteProductBundle_test2() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
        
        // Lookup an existing item
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = '01t80000002NON4';
        myItem.PricebookEntryId__c ='01u80000006eap9';
        myItem.Quantity__c = 1;
        myItem.Y1_Quote_Price__c = 2.50;
        myItem.Approval_Required_by__c = 'Sales VP';
        myItem.Name__c = 'HQ Account';
        myItem.Quote__c = newQuote.Id;
        insert myItem;
        
        Quote_Product_Bundle__c qpb = new Quote_Product_Bundle__c();
        qpb.Quote__c = newQuote.Id;
        qpb.Main_Quote_Product__c = myItem.Id;
        insert qpb;
        
        Quote_Product__c myItem1 = new Quote_Product__c();
        myItem1.Product__c = '01t80000002NONJ';
        myItem1.PricebookEntryId__c ='01u80000006eapOAAQ';
        myItem1.Quantity__c = 1;
        myItem1.Y1_Quote_Price__c = 2.50;
        myItem1.Approval_Required_by__c = 'Sales VP';
        myItem1.Quote__c = newQuote.Id;
        myItem1.Name__c = 'Domestic Fax-Back';
        myItem1.Quote_Product_Bundle__c = qpb.id;
        insert myItem1;
        
        Quote_Product__c myItem2 = new Quote_Product__c();
        myItem2.Product__c = '01t80000002NPFT';
        myItem2.PricebookEntryId__c ='01u80000006eeVfAAI';
        myItem2.Quantity__c = 1;
        myItem2.Y1_Quote_Price__c = 2.50;
        myItem2.Approval_Required_by__c = 'Sales VP';
        myItem2.Quote__c = newQuote.Id;
        myItem2.Name__c = 'Verification Services';
        myItem2.Quote_Product_Bundle__c = qpb.id;
        insert myItem2;
        
        ApexPages.currentPage().getParameters().put('Id', newQuote.Id);
        ApexPages.currentPage().getParameters().put('qpMainId', myItem.Id);
        ApexPages.currentPage().getParameters().put('mainProductID', myItem.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteProductBundle myController = new quoteProductBundle(testController);
        myController.mainProductSelection();
        myController.loadQuoteSecondaryProducts(); 
        myController.bundleProducts();
        myController.dID = myItem1.ID;
        myController.removeFromBundle();
        myController.step4();
        myController.verificationBundle();
        myController.updateCanadaOffset();
        
        ApexPages.currentPage().getParameters().put('bundleID', qpb.id);
        myController.deleteBundle();
        
        
        
    }
}