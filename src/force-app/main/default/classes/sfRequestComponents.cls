public class sfRequestComponents {
    public List<sfComponentsWrapper> wrappers {get; set;}
    public static Integer toDelIdent {get; set;}
    public static Integer addCount {get; set;}
    public boolean showNewComp {get;set;}
    private Integer nextIdent=0;
    public Salesforce_Request__c theRequest {get;set;}
    public Salesforce_Request_Component__c[] theRequestComponents {get;set;}
    public Salesforce_Component__c[] theComponents {get;set;}
    public Salesforce_Component__c newC {get;set;}
    public String delEx {get;set;}	
    public string URLstring {get;set;}
    public id sfRequestId {get;set;}
    public string prefix {get;set;}
    public string src_soql {get;set;}
    public string sortOrder {get;set;}
    public string newCompObj {get;set;}
    public string newCompName {get;set;}
    public string newCompType {get;set;}
    public boolean compCreated {get;set;}
    public string[] noObjList {get; set;}
    public string selTypeFilter {get;set;}
    public string selObjectsFilter {get;set;}
    public string selUserFilter {get;set;}
    
    public sfRequestComponents()
    {
        showNewComp = false;
        compCreated = false;
        newCompObj = 'Unspecified';
        sfRequestId = ApexPages.currentPage().getParameters().get('id');	
        URLstring = ApexPages.currentPage().getUrl();
        prefix = string.valueOf([SELECT Id FROM Salesforce_Component__c LIMIT 1].Id).left(3);
        theRequest = [SELECT Id, Name from Salesforce_Request__c where ID=:sfRequestId];
        src_soql = 'SELECT Salesforce_Component__c, Salesforce_Component__r.Name, Salesforce_Request__c, Notes__c, Name, Id, Ownerid, Owner.LastName,'+
            'Deployed__c, Deployed_to_Production_Staging__c, Deployed_to_MSDS_Dev__c, Salesforce_Component__r.Num_of_Open_Requests__c, '+ 
            'Deployed_to_MSDS_Test__c, Type__c, Object__c, Name__c from Salesforce_Request_Component__c where Salesforce_Request__c =:sfRequestId ';
        sortOrder = 'ORDER BY Type__c ASC';
        theRequestComponents = Database.query (src_soql+sortOrder);
        wrappers=new List<sfComponentsWrapper>();
        for (Integer idx=0; idx<1; idx++)
        {
            wrappers.add(new sfComponentsWrapper(nextIdent++));
        }
        noObjList = 'Apex Class;Apex Trigger;Flow;Permission Set;Static Resource;Visualforce Component;Visualforce Page;Workflow Email Alert;Workflow Field Update;Workflow Flow Trigger'.split(';', 0);
    }
    
    public void reorder()
    {
        src_soql = 'SELECT Salesforce_Component__c, Salesforce_Component__r.Name, Salesforce_Request__c, Notes__c, Name, Id, Ownerid, Owner.LastName,'+
            'Deployed__c, Deployed_to_Production_Staging__c, Deployed_to_MSDS_Dev__c, Salesforce_Component__r.Num_of_Open_Requests__c, '+ 
            'Deployed_to_MSDS_Test__c, Type__c, Object__c, Name__c from Salesforce_Request_Component__c where Salesforce_Request__c =:sfRequestId ';
        if (selTypeFilter != null)
        {
            src_soql += 'AND Type__c = \'' + selTypeFilter + '\' ';
        }
        if (selObjectsFilter != null)
        {
            src_soql += 'AND Object__c = \'' + selObjectsFilter + '\' ';
        }
        if (selUserFilter != null)
        {
            src_soql += 'AND CreatedByid = \'' + selUserFilter + '\' ';
        }
        if (sortOrder != null)
        {
            src_soql += sortOrder;
        }
        theRequestComponents = Database.query(src_soql);
    }
    
    public void delWrapper()
    {
        Integer toDelPos=-1;
        for (Integer idx=0; idx<wrappers.size(); idx++)
        {
            if (wrappers[idx].ident==toDelIdent)
            {
                toDelPos=idx;
            }
        }
        
        if (-1!=toDelPos)
        {
            wrappers.remove(toDelPos);
        }
    }
    
    public void addRows()
    {
        for (Integer idx=0; idx<addCount; idx++)
        {
            wrappers.add(new sfComponentsWrapper(nextIdent++));
        }
    }
    
    public PageReference save()
    {
        Salesforce_Request_Component__c[] sftcs = new List<Salesforce_Request_Component__c>();
        for(sfComponentsWrapper wrap : wrappers)
        {
            if (wrap.selType != null && wrap.selComp != null)
            {
                wrap.sftc.Type__c = wrap.selType;
                wrap.sftc.Salesforce_Request__c = theRequest.ID;
                wrap.sftc.Object__c = wrap.selOb;   
                
                Salesforce_Component__c sfc = [SELECT Id, Name FROM Salesforce_Component__c WHERE Id =: wrap.selComp LIMIT 1];
                wrap.sftc.Salesforce_Component__c = sfc.Id;
                wrap.sftc.Name__c = sfc.Name;
                sftcs.add(wrap.sftc);
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Not all components are properly filled out'));
                return null;
            }
        }
        insert sftcs;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'New components added...'));
        return new PageReference('/' + theRequest.ID);
    }
    
    public class sfComponentsWrapper
    {
        public Salesforce_Request_Component__c sftc {get; private set;}
        public Integer ident {get; private set;}
        public string selType {get;set;}
        public string selOb {get;set;}
        public string selComp {get;set;}
        public string compName {get;set;}
        
        public sfComponentsWrapper(Integer inIdent)
        {
            compName = selComp;
            Id sfRequestId = ApexPages.currentPage().getParameters().get('id');	
            ident=inIdent;
            sftc=new Salesforce_Request_Component__c(
                Salesforce_Request__c = sfRequestId
            );
        }
        
        public List<SelectOption> getTypes()
        {
            List<SelectOption> o = new List<SelectOption>();
            o.add(new SelectOption('', '-- Type --', false));
            Schema.DescribeFieldResult fr = Salesforce_Component__c.Type__c.getDescribe();
            Schema.PicklistEntry[] plvs = fr.getPicklistValues();
            for(Schema.PicklistEntry plv : plvs )
            {
                o.add(new SelectOption(plv.getValue(), plv.getLabel())); 
            }
            o.Sort();
            return o;
        }
        
        public List<SelectOption> getComps()
        {
            SelectOption[] o = new List<SelectOption>();
            o.add(new SelectOption('', '-- API Name --', false));
            Salesforce_Component__c[] compList = [SELECT Id, Name FROM Salesforce_Component__c 
                                                  WHERE Type__c =: selType AND Object__c =: selOb
                                                  ORDER BY Name ASC];
            for(Salesforce_Component__c comp : compList)
            {
                o.add(new SelectOption(comp.Id, comp.Name));
            }
            return o;
        }
        
        
        public List<SelectOption> getObs()
        {
            SelectOption[] o = new List<SelectOption>();
            o.add(new SelectOption('Unspecified', '-- Unspecified --', false));
            Set<string> obsSet = new Set<string>();
            Salesforce_Component__c[] SFCcomps = [SELECT id, Name, Object__c FROM Salesforce_Component__c WHERE Object__c != null AND Object__c != 'Unspecified' ORDER BY Object__c ASC];
            for(Salesforce_Component__c comp : SFCcomps)
            {
                obsSet.add(comp.Object__c);
            }
            for(string ob : obsSet)
            {
                o.add(new SelectOption(ob, ob));
            }
            return o;
        }
    }
    
    public PageReference saveEdits()
    {
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(theRequestComponents.size()>0)
                upsert(theRequestComponents);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
        return new PageReference('/' + theRequest.ID);
    }
    
    public void saveExisting()
    {
        update theRequestComponents;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Existing components saved...'));
        
    }
    
    public PageReference delExisting()
    {
        Salesforce_Request_Component__c delComp = [ SELECT Id FROM Salesforce_Request_Component__c WHERE Id = :delEx LIMIT 1 ];
        delete delComp;
        PageReference pr = Page.sfrequestcomponentsadd;
        pr.getParameters().put( 'id', theRequest.Id );
        pr.setRedirect( true );
        return pr;
    }
    
    public PageReference editExisting()
    {
        Salesforce_Request_Component__c editComp = [ SELECT Id FROM Salesforce_Request_Component__c WHERE Id = :delEx LIMIT 1 ];
        PageReference pr = new PageReference('/'+editComp.id+'/e?retURL='+URLstring);
        pr.setRedirect( true );
        return pr;
    }
    
    public PageReference back()
    {
        PageReference pr = new PageReference('/'+sfRequestId);
        pr.setRedirect(true);
        return pr;
    }
    
    public void showNewCompBox ()
    {
        showNewComp = true;
        newCompType = '';
        newCompName = '';
        newCompObj = 'Unspecified';
        newC = new Salesforce_Component__c ();
    }
    
    public void hideNewCompBox ()
    {
        showNewComp = false;
        newCompObj = 'Unspecified';
    }
    
    public void saveAndAdd ()
    {
        compCheck ();
        if (compCreated == true)
        {
            Salesforce_Request_Component__c newSRC = new Salesforce_Request_Component__c ();
            newSRC.Salesforce_Request__c = sfRequestId;
            newSRC.Type__c = newC.Type__c;
            newSRC.Name__c = newC.Name;
            newSRC.Salesforce_Component__c = newC.id;
            
            if (newC.Object__c != null)
            {
                newSRC.Object__c = newC.Object__c;
            }
            
            else
            {
                newSRC.Object__c = 'Unspecified';
            }
            
            insert newSRC;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'New component created and added to this request'));
        }
        reorder ();
    }
    
    // new component check to avoid duplicates
    public void compCheck()
    {
        if (newCompName == null || newCompType == null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Not all required fields are filled out'));
        }
        else
        {
            integer exI = [SELECT id FROM Salesforce_Component__c WHERE Name = :newCompName AND Type__c = :newCompType AND (Object__c = :newCompObj OR Object__c = 'Unspecified') LIMIT 1].size ();
            if (exI == 0)
            {
                newC.Name = newCompName;
                newC.Type__c = newCompType;
                newC.Object__c = newCompObj;
                insert newC;
                compCreated = true;
                hideNewCompBox();
            }
            else
            {
                compCreated = false;
                ApexPages.addmessage (new ApexPages.message(ApexPages.severity.ERROR,'This component already exists'));
            }
        }
    }
    
    public List<SelectOption> getTypesFilter()
    {
        SelectOption[] o = new List<SelectOption>();
        o.add(new SelectOption('', '-- All --', false));
        Set<string> sfrcSet = new Set<string>();
        for (Salesforce_Request_Component__c sfrc : [SELECT Id, Type__c FROM Salesforce_Request_Component__c WHERE Salesforce_Request__c = :sfRequestId])
        {
            if (sfrc.Type__c != null) 
            {
                sfrcSet.add(sfrc.Type__c);
            }
        }
        if (sfrcSet.size() > 0)
        {
            for (string sfrc : sfrcSet)
            {
                o.add(new SelectOption(sfrc, sfrc));
            }
        }
        return o;
    }
    
    public List<SelectOption> getObjectsFilter()
    {
        SelectOption[] o = new List<SelectOption>();
        o.add(new SelectOption('', '-- All --', false));
        Set<string> sfrcSet = new Set<string>();
        for (Salesforce_Request_Component__c sfrc : [SELECT Id, Object__c FROM Salesforce_Request_Component__c WHERE Salesforce_Request__c = :sfRequestId])
        {
            if (sfrc.Object__c != null)
            {
                sfrcSet.add(sfrc.Object__c);
            }
            else
            {
                sfrcSet.add('Unspecified');
            }
        }
        if (sfrcSet.size() > 0)
        {
            for (string sfrc : sfrcSet)
            {
                o.add(new SelectOption(sfrc, sfrc));
            }
        }
        return o;
    }
    
    public List<SelectOption> getUsersFilter()
    {
        SelectOption[] o = new List<SelectOption>();
        o.add(new SelectOption('', '-- All --', false));
        Set<id> usridSet = new Set<id>();
        for (Salesforce_Request_Component__c sfrc : [SELECT Id, Ownerid FROM Salesforce_Request_Component__c WHERE Salesforce_Request__c = :sfRequestId])
        {
            usridSet.add(sfrc.Ownerid);
        }
        for (User usr : [SELECT id, LastName FROM User WHERE id IN :usridSet ORDER BY LastName ASC])
        {
            o.add(new SelectOption(usr.id, usr.LastName));
        }
        return o;
    }
    
    
    // test methods
    static testmethod void canCreateSFRequest() 
    {
        
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        newRequest.Summary__c = 'Test Account';
        insert newRequest;
        
        Salesforce_Component__c newSFC = new Salesforce_Component__c();
        newSFC.Name = 'TestComp';
        newSFC.Object__c = 'Account';
        newSFC.Type__c = 'Apex Class';
        insert newSFC;
        
        Salesforce_Request_Component__c newComp = new Salesforce_Request_Component__c();
        newComp.Salesforce_Request__c = newRequest.Id;
        newComp.Salesforce_Component__c = newSFC.Id;
        newComp.Name__c = 'Test comp';
        newComp.Type__c = 'Apex Class';
        newComp.Notes__c = 'Test Account';
        insert newComp;
        
        ApexPages.currentPage().getParameters().put('Id', newRequest.Id);
        sfRequestComponents myController = new sfRequestComponents();
        myController.sfRequestId = newRequest.id;
        myController.delEx = string.valueOf(newComp.Id);
        myController.saveEdits(); 
        myController.save();
        myController.delWrapper();
        myController.addRows();
        myController.saveExisting();
        myController.editExisting();
        myController.delExisting();
        myController.reorder();
        myController.showNewCompBox();
        myController.back();
        myController.hideNewCompBox();
        myController.saveAndAdd();
        myController.getTypesFilter();
        myController.getObjectsFilter();
        myController.getUsersFilter();
        sfComponentsWrapper sfcw = new sfComponentsWrapper(1);
        sfcw.getTypes();
        sfcw.getComps();
        sfcw.getObs();
    }    
    
    
    
    
}