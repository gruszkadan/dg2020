public with sharing class nonSupportCall {
    public User u {get;set;}
    public Contact c {get;set;}
    public Case newCase;
    public Case_Issue__c newIssue;
    public Case nscCase {get;set;}
    public boolean nonCCRole {get;set;}
    public string ptype {get;set;}
    
    public nonSupportCall(){
        ptype = Apexpages.currentPage().getParameters().get('ptype');
        c = [select ID, AccountID, Name from Contact where Name ='Non Customer' limit 1];
        u = [select id, Name, Department__c, Group__c from User where id =: UserInfo.getUserId() LIMIT 1];
        nonCCRole = FALSE;
        newCase = new Case();
        newIssue = new Case_Issue__c();
    }
    
    public void createCase() {
        if(u.Group__c =='Customer Care'){
            insertCase();
            caseInfo();
            nonCCRole = FALSE;
        } else {
            nonCCRole = TRUE;
        }
    }
    
    public void insertCase() {
        newCase.AccountID = c.AccountId;
        newCase.ContactID = c.Id;
        newCase.Status = 'Closed';
        newCase.Type = 'CC Support';
        newCase.Priority__c = '3';
        newCase.Category__c = 'Service Request';
        newIssue.Product_In_Use__c = 'None';
        if(ptype == 'Chat'){
            newIssue.product_support_issue_locations__c = 'Chat';
            newIssue.product_support_issue__c = 'Not an MSDS support chat';
           newCase.Subject = 'Non Customer Chat';
            newCase.Case_Resolution__c = 'Non Customer Chat';
            newCase.Origin = 'Chat';
        }
        if(ptype == 'Call'){
            newIssue.product_support_issue_locations__c = 'DID / Operator';
            newIssue.product_support_issue__c = 'Not an MSDS support call';
            newCase.Subject = 'Non Customer Call';
            newCase.Case_Resolution__c = 'Non Customer Call';
            newCase.Origin = 'Phone';
        }
        newCase.OwnerID = u.ID;
        insert newCase;
        newIssue.Associated_Case__c = newCase.Id;
        insert newIssue;
       
    }
    
    public void caseInfo() {
        nscCase = [select ID, CaseNumber from Case where ID=:newCase.ID];
        
    }
}