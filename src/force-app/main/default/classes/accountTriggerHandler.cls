public class accountTriggerHandler {
    
    // BEFORE INSERT
    public static void beforeInsert(Account[] accounts) {
        
        Account[] accountsForTerritoryAssignment = new list<Account>();
        Account[] accountsForOwnershipAssignment = new list<Account>();
        Account[] accountsForEnterpriseEHSAssignment = new list<Account>();
        
        for(Account a: accounts){
            // on insert if there's a zip, add to set to update address
            if (a.BillingPostalCode != null && a.BillingCountry == 'United States' || a.BillingCountry == 'Canada' || a.BillingCountry == null) {
                    accountsForTerritoryAssignment.add(a);
            }else{
                accountsForOwnershipAssignment.add(a);
            }
            // on insert if enterprise sales is true, add to set to assign owner
            if (a.Enterprise_Sales__c == true) {
                accountsForEnterpriseEHSAssignment.add(a);
            }
        }
        
        if(accountsForTerritoryAssignment.size() > 0){
            territoryUtility.updateTerritory(accountsForTerritoryAssignment, 'Account', true, 'insert');
        }

        if(accountsForOwnershipAssignment.size() > 0){
            ownershipUtility.updateOwnership(accountsForOwnershipAssignment, null, 'Account', 'insert');
        }
        
        if(accountsForEnterpriseEHSAssignment.size() > 0){
            ownershipUtility.updateEHSEnterpriseOwner(accountsForEnterpriseEHSAssignment);
        }
        
    }
    
    // BEFORE UPDATE
    public static void beforeUpdate(Account[] oldAccounts, Account[] newAccounts) {
        
        Account[] customerStatusChanges = new list<Account>();
        set<Account> accountsForTerritoryAssignment = new set<Account>();
        Account[] accountsForEnterpriseEHSAssignment = new list<Account>();
        id autosim = null;
        
        for (integer i=0; i<newAccounts.size(); i++) {
            //If not the territory reassignment user then run territory assignment
            if (System.UserInfo.getName() != 'Territory Reassignment') {
                //If the account has a zip code - try to find territories
                if(newAccounts[i].BillingPostalCode != null){
                    //if the user changes the customer status from "out of business" to anything else
                    if (oldAccounts[i].Customer_Status__c == 'Out of Business' && newAccounts[i].Customer_Status__c != 'Out of Business') {
                        if (autosim == null) {
                            autosim = [SELECT id FROM User WHERE Alias = 'autosim' LIMIT 1].id;
                            
                        } 
                        if (newAccounts[i].OwnerId == autosim
                            || newAccounts[i].Online_Training_Account_Owner__c == autosim
                            || newAccounts[i].Authoring_Account_Owner__c == autosim
                            || newAccounts[i].EHS_Owner__c == autosim
                            || newAccounts[i].Ergonomics_Account_Owner__c == autosim) {
                                accountsForTerritoryAssignment.add(newAccounts[i]);
                            }
                    } 
                    
                    // when converting leads to a new account
                  /*  if (oldAccounts[i].Authoring_Account_Owner__c == null 
                        || oldAccounts[i].EHS_Owner__c == null
                        || oldAccounts[i].Online_Training_Account_Owner__c == null
                        || oldAccounts[i].Ergonomics_Account_Owner__c == null) {
                            accountsForTerritoryAssignment.add(newAccounts[i]);
                        }
*/
                    // on update if zip has changed, add to set to update address
                    if (oldAccounts[i].BillingPostalCode != newAccounts[i].BillingPostalCode) {
                        // if not in US, CA, or null, don't update the address
                        if (oldAccounts[i].BillingCountry == 'United States' || newAccounts[i].BillingCountry == 'Canada' || newAccounts[i].BillingCountry == null) {
                            accountsForTerritoryAssignment.add(newAccounts[i]);
                        }
                    }
                }
                // on update if enterprise sales changed to true and there is no EHS owner, add to set to assign EHS owner
                if (oldAccounts[i].Enterprise_Sales__c == false && newAccounts[i].Enterprise_Sales__c == true && newAccounts[i].EHS_Owner__c == null) {
                    accountsForEnterpriseEHSAssignment.add(newAccounts[i]);
                }
            }
            
            // If the account's Customer Status is no longer active
            if (newAccounts[i].Customer_Status__c != 'Active' && oldAccounts[i].Customer_Status__c == 'Active') {
                customerStatusChanges.add(newAccounts[i]);
            }
            
            
        }
        
        if(accountsForTerritoryAssignment.size() > 0){
            Account[] accountsForTerritoryAssignmentList = new list<Account>();
            accountsForTerritoryAssignmentList.addAll(accountsForTerritoryAssignment);
            territoryUtility.updateTerritory(accountsForTerritoryAssignmentList, 'Account', true, 'update');
        }
        
        if(accountsForEnterpriseEHSAssignment.size() > 0){
            ownershipUtility.updateEHSEnterpriseOwner(accountsForEnterpriseEHSAssignment);
        }
        
        if (customerStatusChanges.size() > 0) {
            accountUtility.clearCustomerInfo(customerStatusChanges);
        }
        
    }
    
    // BEFORE DELETE
    public static void beforeDelete(Account[] oldAccounts, Account[] newAccounts) {}
    
    // AFTER INSERT
    public static void afterInsert(Account[] oldAccounts, Account[] newAccounts) {
        Account[] newChildAccount = new list<Account>();
        for (integer i=0; i<newAccounts.size(); i++) {
            if(newAccounts[i].Ultimate_Parent__c!= NULL){
                newChildAccount.add(newAccounts[i]);
            }
        }
        if(newChildAccount.size() > 0){
            accountUtility.updateChildSams(newChildAccount);
        }
    }
    
    // AFTER UPDATE
    public static void afterUpdate(Account[] oldAccounts, Account[] newAccounts) {
        Account[] oobAccts = new list<Account>();
        Account[] oldProdAccts = new list<Account>();
        Account[] newProdAccts = new list<Account>();
        Account[] parentChangedESMarkedAccts = new list<Account>();
        Account[] ownerChanges = new list<Account>();
        Account[] licensingBaseChanges = new list<Account>();
        Account[] ultimateParentChanges = new list<Account>();
        Account[] SamChangesOnUltimate = new list<Account>();
        Account[] arrAccounts = new list<Account>();
        
        for (integer i=0; i<newAccounts.size(); i++) {
            // If any of the products change, we may need to change the stage
            if (oldAccounts[i].Active_MSDS_Management_Products__c != newAccounts[i].Active_MSDS_Management_Products__c ||
                oldAccounts[i].Active_Products__c != newAccounts[i].Active_Products__c ||
                oldAccounts[i].EHS_Active_Licensing__c != newAccounts[i].EHS_Active_Licensing__c ||
                oldAccounts[i].Ergo_Active_Licensing__c != newAccounts[i].Ergo_Active_Licensing__c) {
                    oldProdAccts.add(oldAccounts[i]);
                    newProdAccts.add(newAccounts[i]);
                } 
            // If the account's status changes to 'Out of Business'
            if (oldAccounts[i].Customer_Status__c != newAccounts[i].Customer_Status__c && newAccounts[i].Customer_Status__c == 'Out of Business') {
                oobAccts.add(newAccounts[i]);
            }
            // If the account's parent changes or the account is marked ES 
            if (oldAccounts[i].ParentId != newAccounts[i].ParentId ||
                (oldAccounts[i].Enterprise_Sales__c != newAccounts[i].Enterprise_Sales__c && newAccounts[i].Enterprise_Sales__c == TRUE)) {
                    parentChangedESMarkedAccts.add(newAccounts[i]);
                }
            //If any of the account's owners change
            if (oldAccounts[i].OwnerId != newAccounts[i].OwnerId || 
                oldAccounts[i].EHS_Owner__c != newAccounts[i].EHS_Owner__c ||
                oldAccounts[i].Authoring_Account_Owner__c != newAccounts[i].Authoring_Account_Owner__c ||
                oldAccounts[i].Online_Training_Account_Owner__c != newAccounts[i].Online_Training_Account_Owner__c ||
                oldAccounts[i].Ergonomics_Account_Owner__c != newAccounts[i].Ergonomics_Account_Owner__c) {
                    ownerChanges.add(newAccounts[i]);
                }
            // If the account's Licensing Base Amount Changes
            if (newAccounts[i].Licensing_Base_Price__c != null && oldAccounts[i].Licensing_Base_Price__c != newAccounts[i].Licensing_Base_Price__c) {
                licensingBaseChanges.add(newAccounts[i]);
            }
            //If the ultimate parent changes, send children to Utility where we set their SAM
            if (oldAccounts[i].Ultimate_Parent__c != newAccounts[i].Ultimate_Parent__c	) {
                ultimateParentChanges.add(newAccounts[i]);
            }
            
            //If the ultimate parent's SAM changes, send ultimate parent to utility where we update all children's SAM
            if (oldAccounts[i].Platform_Sales_Rep__c != newAccounts[i].Platform_Sales_Rep__c && newAccounts[i].Is_Ultimate_Parent__c == true ) {
                SamChangesOnUltimate.add(newAccounts[i]);
            }
            //If Account ARR changes to > 20,000
            if ((oldAccounts[i].Customer_Annual_Revenue__c == null || oldAccounts[i].Customer_Annual_Revenue__c < 20000) && newAccounts[i].Customer_Annual_Revenue__c >= 20000) {
                arrAccounts.add(newAccounts[i]);
            }
            //If Account ARR changes to < 20,000
            if (oldAccounts[i].Customer_Annual_Revenue__c >= 20000 && newAccounts[i].Customer_Annual_Revenue__c < 20000) {
                arrAccounts.add(newAccounts[i]);
            }
            
            
            
        }
        
        if (oobAccts.size() > 0) {
            poiStageStatusUtility.updateStatus(oobAccts);
        }
        if (oldProdAccts.size() > 0 && newProdAccts.size() > 0) {
            poiStageStatusUtility.updateStageFromActiveProducts(oldProdAccts, newProdAccts);
        }
        if (parentChangedESMarkedAccts.size() > 0) {
            accountUtility.sendToES(parentChangedESMarkedAccts);
        }
        if (ownerChanges.size() > 0) {
            poiActionUtility.updatePoiaOwnership(ownerChanges, 'Account');
           // accountUtility.updateOwnerEmailsOnContact(ownerChanges);
        }
        if (licensingBaseChanges.size() > 0) {
            salesPerformanceOrderUtility.insertESAssociteSPO(licensingBaseChanges);
        }
        if (ultimateParentChanges.size() > 0) {
            accountUtility.updateChildSams(ultimateParentChanges);
        }
        if (SamChangesOnUltimate.size() > 0) {
            accountUtility.findAndUpdateChildSams(samChangesOnUltimate);
        }
        if (arrAccounts.size() > 0) {
            accountUtility.sendArrEmail(arrAccounts);
        }
        
        
        
    }
    
    // AFTER DELETE
    public static void afterDelete(Account[] accts) {
        set<id> winningIds = new set<id>();
        for (Account a : accts) {
            if (a.MasterRecordId != null) {
                winningIds.add(a.MasterRecordId);
            }
        }
        if (winningIds.size() > 0) {
            poiActionUtility.mergePoiaOwnership(winningIds);
        }
        
    }
    
}