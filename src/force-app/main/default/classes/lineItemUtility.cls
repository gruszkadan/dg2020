public class lineItemUtility {
    
    
    
    public static void createOppLineItems(Id quoteId, Id oppId, Quote_Product__c[] quoteProds, boolean contractEasy){
        //query quote and fields that need to be mapped to Opportunity       
        Quote__c quote = [Select Id, Currency__c, Currency_Rate__c, Currency_Rate_Date__c, Promotion__c, Price_Book__c, Contract_Length__c, 
                          VPPPA_Customer__c, Implementation_Model__c, Subscription_Model__c, Licensing_Covered_by_Another_Location__c, Multiple_Location_Indexing__c FROM Quote__c WHERE Id = :quoteId];
       
  
        Opportunity opp = [Select Id, isClosed, quote__c, Contract_Length__c FROM Opportunity WHERE Id = :oppId];
        List<OpportunityLineItem> oppLineItems = [Select Id FROM OpportunityLineItem Where OpportunityId = :oppId];
        
        if(Opp.isClosed == true){
            Opp.StageName = 'System Demo';
            update opp;
        }
        
        
        //if oppLineItems already exist, delete them to create new list of them
        if(oppLineItems != NULL){
            delete oppLineItems;
        }
        
        
        if (quoteId != null ) {
            //multicurrency rates
            if (Quote.Currency__c != null) {
                Opp.Currency__c = quote.Currency__c;
                Opp.Currency_Rate__c = quote.Currency_Rate__c;
                Opp.Currency_Rate_Date__c = quote.Currency_Rate_Date__c;
            } else {
                Opp.Currency__c = 'USD';
                Opp.Currency_Rate__c = 1;
                Opp.Currency_Rate_Date__c = null;
            }
            Opp.Promotion__c = quote.Promotion__c;
            Opp.Pricebook2Id = quote.Price_Book__c;
            Opp.quote__c = quote.id;
            Opp.Contract_Length__c = quote.Contract_Length__c;
            Opp.VPPPA_Customer__c = quote.VPPPA_Customer__c;  
            Opp.Implementation_Model__c = quote.Implementation_Model__c;

            Opp.Subscription_Model__c = quote.Subscription_Model__c;
            Opp.Has_Base_Subscription__c = false;
            Opp.Licensing_Covered_by_Another_Location__c = quote.Licensing_Covered_by_Another_Location__c;
            Opp.Multiple_Location_Indexing__c = quote.Multiple_Location_Indexing__c;
            
            oppLineItems = new List<OpportunityLineItem>();
            
            for(Quote_Product__c item : quoteProds ) {
                if(item.Product__r.Parent_Product__c == null){
                    OpportunityLineItem newItem = new OpportunityLineItem();
                    newItem.Special_Indexing_Fields__c = item.Special_Indexing_Fields__c; 
                    newitem.Add_On_Product__c = item.Add_On_Product__c;
                    newItem.Approval_Required_by__c  =  item.Approval_Required_by__c;
                    newItem.Approval_Status__c  =  item.Approval_Status__c;
                    newItem.Auth_Consulting_Service__c = item.Auth_Consulting_Service__c;
                    newItem.Auth_Countries_of_Use__c = item.Auth_Countries_of_Use__c;
                    newItem.Auth_Date_Quote_Required__c = item.Auth_Date_Quote_Required__c;
                    newItem.Auth_End_Date__c = item.Auth_End_Date__c;
                    newItem.Auth_Estimated_Start_Date__c = item.Auth_Estimated_Start_Date__c;
                    newItem.Auth_Expected_End_Date__c = item.Auth_Expected_End_Date__c;
                    newItem.Auth_Hourly_Rate__c = item.Auth_Hourly_Rate__c;
                    newItem.Auth_Identical_Product_Grouping__c = item.Auth_Identical_Product_Grouping__c;
                    newItem.Auth_Language_Docs__c = item.Auth_Language_Docs__c;
                    newItem.Auth_Need_Addtl_Info__c = item.Auth_Need_Addtl_Info__c;
                    newItem.Auth_Num_of_Products__c = item.Auth_Num_of_Products__c;
                    newItem.Auth_Original_Manufacturer__c = item.Auth_Original_Manufacturer__c;
                    newItem.Auth_Other_Reg_Format__c = item.Auth_Other_Reg_Format__c;
                    newItem.Auth_Other_Service__c = item.Auth_Other_Service__c;
                    newItem.Auth_Pilot_Program__c = item.Auth_Pilot_Program__c;
                    newItem.Auth_Project_Breakdown__c = item.Auth_Project_Breakdown__c;
                    newItem.Auth_Project_Data__c = item.Auth_Project_Data__c;
                    newItem.Auth_Project_Terms__c = item.Auth_Project_Terms__c;
                    newItem.Auth_Regulatory_Format__c = item.Auth_Regulatory_Format__c;
                    newItem.Auth_Regulatory_Format_Docs__c = item.Auth_Regulatory_Format_Docs__c;
                    newItem.Auth_Rush_Quote__c = item.Auth_Rush_Quote__c;
                    newItem.Auth_Service_Requested__c = item.Auth_Service_Requested__c;
                    newItem.Auth_Start_Date__c = item.Auth_Start_Date__c;
                    newItem.Auth_Total_Authoring_Labels__c = item.Auth_Total_Authoring_Labels__c;
                    newItem.Auth_Total_Docs__c = item.Auth_Total_Docs__c;
                    newItem.Auth_Total_Hours__c = item.Auth_Total_Hours__c;
                    newItem.Auth_Total_Translated_Docs__c = item.Auth_Total_Translated_Docs__c;
                    newItem.Auth_Total_Translated_Labels__c = item.Auth_Total_Translated_Labels__c;
                    newItem.Auth_Trade_Secrets_Proprietary__c = item.Auth_Trade_Secrets_Proprietary__c;
                    newItem.Auth_Translation_Lang__c = item.Auth_Translation_Lang__c;
                    newItem.Auth_Translation_Turnaround_Time__c = item.Auth_Translation_Turnaround_Time__c;
                    newItem.Auth_Turn_Around_Time__c = item.Auth_Turn_Around_Time__c;
                    newItem.Authoring_Locations__c = item.Authoring_Locations__c;
                    newItem.Bundle__c = item.Bundle__c;
                    newItem.Bundled_Product__c = item.Bundled_Product__r.Name;
                    newItem.Bundled_Product_ID__c = item.Bundled_Product__r.ID;
                    newItem.Bundled_Indexing_Display_Type__c = item.Bundled_Indexing_Display_Type__c;
                    newItem.Configured__c  =  item.Configured__c;
                    newItem.CS_Scope__c  =  item.CS_Scope__c;
                    newItem.Custom_Label_Information__c  =  item.Custom_Label_Information__c;
                    newItem.Custom_Services_Product_Additional_Info__c = item.Custom_Services_Product_Additional_Info__c;
                    newItem.Custom_Quote__c = item.Custom_Quote__c;
                    newItem.Description  =  item.Description__c;
                    newItem.Discount  =  item.Discount__c;
                    newItem.Discount_Reason__c = item.Discount_Reason__c;
                    newItem.eBinder_Replication_Account__c = item.eBinder_Replication_Account__c;
                    newItem.ERS_Battery_Shipper__c = item.ERS_Battery_Shipper__c;
                    newItem.ERS_Custom_Script__c = item.ERS_Custom_Script__c;
                    newItem.ERS_Dedicated_800_or_Country_Phone__c = item.ERS_Dedicated_800_or_Country_Phone__c;
                    newItem.ERS_eBinder_Build_Type__c = item.ERS_eBinder_Build_Type__c;
                    newItem.ERS_Fedex_Account__c = item.ERS_Fedex_Account__c;
                    newItem.ERS_Fedex_Account_Number__c = item.ERS_Fedex_Account_Number__c;
                    newitem.Is_Demonstration_Project__c = item.Is_Demonstration_Project__c;
                    newItem.Minimum_Price_Applied__c = item.Minimum_Price_Applied__c;
                    newItem.Ergo_Optional_System_Executed_Services__c = item.Ergo_Optional_System_Executed_Services__c;
                    newItem.Expected_Delivery_Date__c = item.Expected_Delivery_Date__c;
                    newItem.Executed_Services__c = item.Executed_Services__c;
                    newItem.Frequency__c = item.Frequency__c;
                    newItem.Group_ID__c = item.Group_ID__c;
                    newItem.Group_Name__c = item.Group_Name__c;
                    newItem.Group_Parent_ID__c = item.Group_Parent_ID__c;
                    newItem.Has_Overage_Amount__c = item.Has_Overage_Amount__c;
                    newItem.Indexing_Type__c = item.Indexing_Type__c;
                    newItem.List_Price__c  =  item.List_Price__c;
                    newItem.Notes__c  =  item.Notes__c;
                    newItem.ODT_Flex_Max_Num_of_Enrollments__c = item.ODT_Flex_Max_Num_of_Enrollments__c;
                    newItem.ODT_num_of_Courses__c = item.ODT_num_of_Courses__c;
                    newItem.ODT_Scorm_Num_of_Courses__c = item.ODT_Scorm_Num_of_Courses__c;
                    newItem.ODT_Server_Lic_Name_of_Clients_LMS__c = item.ODT_Server_Lic_Name_of_Clients_LMS__c;
                    newItem.ODT_Server_Lic_num_of_Courses__c = item.ODT_Server_Lic_num_of_Courses__c;
                    newItem.ODT_Server_Lic_Num_of_Employees__c = item.ODT_Server_Lic_Num_of_Employees__c;
                    newItem.ODTDev_Stand_Alone__c  =  item.ODTDev_Stand_Alone__c;
                    newItem.Onsite_Contact_Info_Address__c = item.Onsite_Contact_Info_Address__c;
                    newItem.Onsite_Escort__c  =  item.Onsite_Escort__c;
                    newItem.Onsite_Hourly_Rate__c = item.Onsite_Hourly_Rate__c;
                    newItem.Onsite_Locations__c  =  item.Onsite_Locations__c;
                    newItem.Onsite_Maximum_Hours__c = item.Onsite_Maximum_Hours__c;
                    newItem.Onsite_Maximum_Travel_Expense__c = item.Onsite_Maximum_Travel_Expense__c;
                    newItem.Onsite_of_Products__c  =  item.Onsite_of_Products__c;
                    newItem.Onsite_Special_Precautionary__c  =  item.Onsite_Special_Precautionary__c;
                    newItem.Onsite_Square_Footage__c  =  item.Onsite_Square_Footage__c;
                    newItem.Onsite_Timeline__c  =  item.Onsite_Timeline__c;
                    newItem.OpportunityID = Opp.Id;
                    newItem.Other_Discount_Reason__c = item.Other_Discount_Reason__c;
                    newItem.Overage_List_Price__c = item.Overage_List_Price__c;
                    newItem.Overage_Quote_Price__c = item.Overage_Quote_Price__c;
                    newItem.PB_Address__c  =  item.PB_Address__c;
                    newItem.PB_Collate__c  =  item.PB_Collate__c;
                    newItem.PB_Colored_sheet_separator__c  =  item.PB_Colored_sheet_separator__c;
                    newItem.PB_Delivery_priority__c  =  item.PB_Delivery_priority__c;
                    newItem.PB_Enclose_in_sheet_protector__c  =  item.PB_Enclose_in_sheet_protector__c;
                    newItem.PB_How_Many_Copies__c  =  item.PB_How_Many_Copies__c;
                    newItem.PB_Include_An_Index__c  =  item.PB_Include_An_Index__c;
                    newItem.PB_Include_Archived_Docs__c  =  item.PB_Include_Archived_Docs__c;
                    newItem.PB_Include_Inactive_Documents__c = item.PB_Include_Inactive_Documents__c;
                    newItem.PB_Location_Names__c = item.PB_Location_Names__c;
                    newItem.PB_Print_Layout__c  =  item.PB_Print_Layout__c;
                    newItem.PB_Print_on_both_sides__c = item.PB_Print_on_both_sides__c;
                    newItem.PB_Staple_each_document__c  =  item.PB_Staple_each_document__c;
                    newItem.PB_Three_hole_punch__c  =  item.PB_Three_hole_punch__c;
                    newItem.PB_Total_Num_of_Documents__c = item.PB_Total_Num_of_Documents__c;
                    newItem.PB_What_Orders_to_Print__c  =  item.PB_What_Orders_to_Print__c;
                    newItem.PB_Which_Documents__c  =  item.PB_Which_Documents__c;
                    newItem.PB_Locations_SDSs_String__c = item.PB_Locations_SDSs_String__c;
                    newItem.PB_Group_By_Location__c = item.PB_Group_By_Location__c;
                    newItem.PB_Location_Separator__c = item.PB_Location_Separator__c;
                    newItem.Premium_Applied__c  =  item.Premium_Applied__c;
                    newItem.Price_Override__c  =  item.Price_Override__c;
                    newItem.PricebookEntryId  =  item.PricebookEntryId__c;
                    newItem.Pricing_Display__c = item.Pricing_Display__c;
                    newItem.Print_Group__c  =  item.Print_Group__c;
                    newItem.Product_Print_Name__c = item.Product_Print_Name__c;
                    newItem.ProductId__c  =  item.Product__c;
                    newitem.Product_Sub_Type__c = item.Product_Sub_Type__c;
                    newItem.qpID__c = item.Id;
                    if(item.Quantity__c >0){
                        if(item.Quantity_Calculation__c == TRUE){
                            newItem.Quantity = item.Quantity__c;
                        }else{
                            newItem.Quantity  =  1;
                        }
                    } else {
                        newItem.Quantity  =  1;
                    }
                    newItem.Quantity__c = item.Quantity__c;
                    newItem.Quantity_Calculation__c = item.Quantity_Calculation__c;
                    newItem.Quantity_Field_Name__c  =  item.Quantity_Field_Name__c;
                    newItem.Quote_Price__c  =  item.Quote_Price__c;
                    newItem.Show_Year_List__c  =  item.Show_Year_List__c;
                    newItem.Standard_MSDS_Inclusion_1__c = item.Standard_MSDS_Inclusion_1__c;
                    newItem.Standard_MSDS_Inclusion_2__c = item.Standard_MSDS_Inclusion_2__c;
                    newItem.Standard_MSDS_Inclusion_3__c = item.Standard_MSDS_Inclusion_3__c;
                    newItem.Total_Line_Value__c  =  item.Total_Line_Value__c;
                    if(item.Quantity_Calculation__c == TRUE && item.Year_1__c == TRUE){
                        if(item.Y1_Quote_Price__c == NULL){
                            newItem.UnitPrice = item.Y1_List_Price__c;
                        } else {
                            newItem.UnitPrice = item.Y1_Quote_Price__c;
                        }
                    } else {
                        newItem.UnitPrice = item.Y1_Total_Price__c;
                    }
                    newItem.Type__c = item.Type__c;
                    newItem.Update_Services_Type__c  =  item.Update_Services_Type__c;
                    newItem.Verification_Percentage__c = item.Verification_Percentage__c;
                    newItem.Y1_Bundled_Price__c = item.Y1_Bundled_Price__c;
                    newItem.Y1_Canada_Bundle_Offset__c = item.Y1_Canada_Bundle_Offset__c;
                    //new
                    newItem.Y1_Custom_List_Price__c = item.Y1_Custom_Proposal_List_Price__c;
                    //end of new
                    newItem.Y1_Discount__c  =  item.Y1_Discount__c;
                    newItem.Y1_List_Price__c  =  item.Y1_List_Price__c;
                    newItem.Y1_Quote_Price__c  =  item.Y1_Quote_Price__c;
                    newItem.Y1_Travelers_Discount_Applied__c = item.Y1_Travelers_Discount_Applied__c;
                    newItem.Y2_Bundled_Price__c = item.Y2_Bundled_Price__c;
                    newItem.Y2_Canada_Bundle_Offset__c = item.Y2_Canada_Bundle_Offset__c;
                    //new
                    newItem.Y2_Custom_List_Price__c = item.Y2_Custom_Proposal_List_Price__c;
                    //end of new
                    newItem.Y2_Discount__c  =  item.Y2_Discount__c;
                    newItem.Y2_List_Price__c  =  item.Y2_List_Price__c;
                    newItem.Y2_Quote_Price__c  =  item.Y2_Quote_Price__c;
                    newItem.Y2_Travelers_Discount_Applied__c = item.Y2_Travelers_Discount_Applied__c;
                    newItem.Y3_Bundled_Price__c = item.Y3_Bundled_Price__c;
                    newItem.Y3_Canada_Bundle_Offset__c = item.Y3_Canada_Bundle_Offset__c;
                    //new
                    newItem.Y3_Custom_List_Price__c = item.Y3_Custom_Proposal_List_Price__c;
                    //end of new
                    newItem.Y3_Discount__c  =  item.Y3_Discount__c;
                    newItem.Y3_List_Price__c  =  item.Y3_List_Price__c;
                    newItem.Y3_Quote_Price__c  =  item.Y3_Quote_Price__c;
                    newItem.Y3_Travelers_Discount_Applied__c = item.Y3_Travelers_Discount_Applied__c;
                    newItem.Y4_Bundled_Price__c = item.Y4_Bundled_Price__c;
                    newItem.Y4_Canada_Bundle_Offset__c = item.Y4_Canada_Bundle_Offset__c;
                    //new
                    newItem.Y4_Custom_List_Price__c = item.Y4_Custom_Proposal_List_Price__c;
                    //end of new
                    newItem.Y4_Discount__c  =  item.Y4_Discount__c;
                    newItem.Y4_List_Price__c  =  item.Y4_List_Price__c;
                    newItem.Y4_Quote_Price__c  =  item.Y4_Quote_Price__c;
                    newItem.Y4_Travelers_Discount_Applied__c = item.Y4_Travelers_Discount_Applied__c;
                    newItem.Y5_Bundled_Price__c = item.Y5_Bundled_Price__c;
                    newItem.Y5_Canada_Bundle_Offset__c = item.Y5_Canada_Bundle_Offset__c;
                    //new
                    newItem.Y5_Custom_List_Price__c = item.Y5_Custom_Proposal_List_Price__c;
                    //end of new
                    newItem.Y5_Discount__c  =  item.Y5_Discount__c;
                    newItem.Y5_List_Price__c  =  item.Y5_List_Price__c;
                    newItem.Y5_Quote_Price__c  =  item.Y5_Quote_Price__c;
                    newItem.Y5_Travelers_Discount_Applied__c = item.Y5_Travelers_Discount_Applied__c;
                    newItem.Year_1__c  =  item.Year_1__c;
                    newItem.Year_2__c  =  item.Year_2__c;
                    newItem.Year_3__c  =  item.Year_3__c;
                    newItem.Year_4__c  =  item.Year_4__c;
                    newItem.Year_5__c  =  item.Year_5__c;
                    newItem.Language_Support_Languages__c = item.Language_Support_Languages__c;
                    newItem.Pre_Scheduled_Training_Num_of_Modules__c = item.Pre_Scheduled_Training_Num_of_Modules__c;
                    newItem.Pre_Scheduled_Training_Modules__c = item.Pre_Scheduled_Training_Modules__c;
                    newItem.Pre_Scheduled_Training_Invitations__c = item.Pre_Scheduled_Training_Invitations__c;
                    newItem.Product_Pitched_By__c = item.Product_Pitched_By__c;
                    newItem.Proposal_Terms__c = item.Proposal_Terms__c;
                    newItem.Attachment__c = item.Attachment__c;
                    newItem.Rush__c = item.Rush__c;
                    newItem.Rush_Reason__c = item.Rush_Reason__c;
                    newItem.Desired_Completion_Date__c = item.Desired_Completion_Date__c;
                    newItem.Indexing_Language__c = item.Indexing_Language__c;
                    newItem.Rush_Timeframe__c = item.Rush_Timeframe__c;
                    if(item.Product_Print_Name__c == 'Base Subscription'){
                        Opp.Has_Base_Subscription__c = true;
                    }
                    oppLineItems.add(newItem);
                }
            }
            update opp;
            insert oppLineItems;

            //If using the easy button then close the opportunity and update.
            //NOTE: Need 2 separate updates because the opportunity needs to be updated with the correct PB before olis are inserted and an opp cannot be closed until it has oli's
            if(contractEasy == true){
                Opp.StageName = 'Closed/Won';
                Opp.CloseDate = system.today();
                 update opp;
            }

        }        
        
        
        
    }
    
    
    
    
}