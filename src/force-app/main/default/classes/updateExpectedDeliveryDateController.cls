public class updateExpectedDeliveryDateController {
    public OpportunityLineItem[] OLI {get;set;}
    public Quote_Product__c[] QP {get;set;}
    public id OppId {get;set;}
    public Opportunity opp {get;set;}
    
    public updateExpectedDeliveryDateController() {
        //Pass in Opportunity Id to know which Opportunity we are on 
        oppId = ApexPages.currentPage().getParameters().get('Id');
        //query oppportunity line items that are equal to the opportunity id we passed in  
        OLI = [Select id, name, qpID__c, Expected_Delivery_Date__c, Product2id, product2.name from OpportunityLineItem where Product2.Needs_Expected_Delivery_Date__c = true AND Opportunity.id =: oppId];   
        opp = [Select id, name from opportunity where id =: oppid];
    }
    
    Public PageReference Save(){
        //Initialize new set of ID that will hold the quoote product ids 
        set<id> QPIDs = new set<id>(); 
        
        //Loop through opportunity line items query that we wrote above 
        //Add quote product ids from out opportunity line items to our set 
        for(OpportunityLineItem o : OLI){
            
            QPIDs.add(o.qpID__c); 
        }
        
       // Query quote products ids that are equivalent to our qpids in out set 
        QP = [select id, name, Expected_Delivery_Date__c from Quote_Product__c where id in: QPIDs];
        
        //loop through oppotunity line items 
        //loop through quote products 
        //If the opportunity line item id matches the quote product id proceed 
        //set the quote product Expected Delivery Date to the opportunity line item Expected Delivery Date
        for(OpportunityLineItem x : OLI){
            for(Quote_Product__c y : QP){
                if(y.id == x.qpID__c){
                    y.Expected_Delivery_Date__c = x.Expected_Delivery_Date__c; 
                }
            }
        }
        // update both the opportunity line item and the quote product 
        update OLI;
        update QP;
        
        // return user to the opportunity detail page of the current opportunity 
        PageReference ref =  new PageReference('/' + oppId);
        
        return ref;  
        
    }
}