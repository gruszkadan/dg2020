public class salesContractApproval {
    Approval__c app;
    Contract__c c;
    string[] contractReasons = new List<string>();
    string[] lineItemReasons = new List<string>();
    Contract_Line_Item__c[] items = new List<Contract_Line_Item__c>();
    Contract_Line_Item__c[] lineItemChanges = new List<Contract_Line_Item__c>();
    id[] productIds = new list<id>();
    Set<string> approvers = new Set<string>();
    boolean newApp = false;
    boolean lang = false;
    boolean sales = false; 
    boolean orders = false; 
    boolean ehs = false;
    boolean authoring = false; 
    boolean finance = false; 
    boolean operations = false;
    boolean director = false;
    public boolean wasRefreshed = false;
    public boolean wasApproved = false;
    public boolean backToApproved = true;
    public boolean isInitialCheck = false; 
    String contractProductSuites;
    Contract_Automation_Setting__mdt contractAutomationSetting;
    public decimal sumLicensingY1Total;
    
    // main method called when checking the contract for approval criteria
    public void check(id cid, boolean isSubmit) {
        if (isSubmit == false) {  
            contractAutomationSetting = [SELECT Installments_Director_Approval_Minimum__c, Split_Billing_Director_Approval_Minimum__c
                                         FROM Contract_Automation_Setting__mdt
                                         WHERE DeveloperName = 'Default'
                                         LIMIT 1];
            
            // if the user isnt submitting, just create the app record
            findContract(cid);                                          // query the contract
            findExistingApp();                                          // search for an existing approval record. if none found, make a new one
            findItems();                                                // query line items
            contractCheck();                                            // run through the contract and record criteria on approval record
            lineItemCheck();                                            // run through the line items and check to see if they changed or are new
            approvalReasons();                                          // save the reasons on the approval record
            createApproverList();                                       // check criteria and create a list of approvers
            if (newApp == true) {
                if (sales || ehs || orders || authoring || finance || operations || lang || director) {
                    app.Status__c = 'Pending';
                    c.Status__c = 'Active';
                    insert app;
                    update c;
                } 
            } else {
                if (sales || ehs || orders || authoring || finance || operations || lang || director) {
                    app.Status__c = 'Pending';
                    c.Status__c = 'Active';
                    update app;
                    update c;
                }
            }
            if (backToApproved == true) {                               // if all approval criteria is removed, set the contract back to approved
                c.Status__c = 'Approved';
                update c;
            } 
        } else {                                                        // if user is submitting the contract, an app should already exist
            findContract(cid);                                          // query the contract
            findExistingApp();                                          // query the app record
            flagContract();                                             // mark the fields on the contract for the approval process to read
            update app;
            update c;
        }
    }
    
    // query line items
    public void findItems() {
        items = [SELECT id, Product__c, Product__r.Contract_Product_Type__c, Y1_Final_Price__c, Name__c, Contract_Terms__c, Approval_Required__c, Original_Char_Count__c, Contract__c, Product__r.Contract_Print_Group__c
                 FROM Contract_Line_Item__c 
                 WHERE Contract__c = :c.id ORDER BY Contract_Sort_Order__c];
        sumLicensingY1Total = 0;
        for(Contract_Line_Item__c cli:items){
            productIds.add(cli.Product__c);
            //sum the y1 final price of all licensing products on the contract
            if(cli.Product__r.Contract_Product_Type__c == 'Licensing'){
                sumLicensingY1Total += cli.Y1_Final_Price__c;
            }
        }
        contractProductSuites = productUtility.findProductSuites(productIds);
        
    }
    
    // query the contract
    public void findContract(id cid) {
        c = [SELECT id, Contract_Type__c, OwnerId, Standard_MSA__c, Deferred_Invoice__c, Delayed_Billing_Only__c, Year_1_Total__c, Calculated_Year_1_Total__c, Modified_Down_Payment__c, Opt_out__c, 
             Related_Contract__c, Installments__c, split_billing__c, Name, Account__c, Account__r.name, Billing_Street__c, Billing_City__c, Billing_State__c, Billing_Postal_Code__c, Billing_Country__c, 
             Shipping_Street__c, Shipping_City__c, Shipping_State__c, Shipping_Postal_Code__c, Shipping_Country__c, Non_Standard_Service_Approval__c, Webpliance_PPI_Approval__c, US_CS_Approval__c,
             Deferred_Invoice_Date__c, Delayed_Billing_Date__c, Approval_Step__c, Status__c, Order_Notes__c, Unsigned_Contract__c, Approval_Process_Comments__c, Custom_Billing_Milestones__c, 
             Next_Approver_List__c, Reassigned_Approver__c, Contract_Terms_Changed__c, Reassigned_Approver__r.LastName, Authoring_Product_Approval__c, Prevent_Double_Approval__c, 
             Other_Special_Payment_Terms__c, Approval_Step_Name__c, Installment_Notes__c, Split_Billing_Notes__c, Opt_Out_Notes__c, Other_Special_Payment_Term_Notes__c, Rep_Approval_Reasons__c,
             Custom_Payment_Net_Terms__c, Payment_Net_Terms__c, Billing_Currency__c, Billing_Currency_Rate__c, Billing_Currency_Rate_Date__c, Execute_By__c, Execute_By_Date__c, Promotion__c, Promotion__r.End_Date__c,
             Ergo_Optional_Services_Approval__c, Account__r.Active_MSDS_Management_Products__c, Billing_Frequency__c, Split_Billing_Number_of_Splits__c, Related_Contract_Type__c, Contract_End_Date_new__c
             FROM Contract__c WHERE id = :cid];
    }
    
    // query the approval record or create a new one if there isnt one already
    public void findExistingApp() {
        if ([SELECT id FROM Approval__c WHERE Contract__c = :c.id].size() > 0) {
            map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            map<string, Schema.SObjectField> fieldMap = schemaMap.get('Approval__c').getDescribe().fields.getMap();
            string csv = ''; 
            for (string fieldName : fieldMap.keyset()) {
                if (csv == null || csv == '') {
                    csv = fieldName;
                } else {
                    csv = csv + ', ' + fieldName;
                }
            }
            string query = 'SELECT ' + csv + ' FROM Approval__c WHERE Contract__c = \'' + c.id + '\' LIMIT 1';
            app = Database.query(query);
            if (app.Status__c == 'Refreshed') {             
                wasRefreshed = true;
            } 
            if (app.Status__c == 'Approved') {
                wasApproved = true;
            }
        } else {                                                                            // if no approval record is found, make a new one
            newApp = true;
            app = new Approval__c();
            app.Contract__c = c.id;
        }
    }
    
    // run this to go through and flag any criteria needing approval
    public void contractCheck() {
        
        
        // MULTICURRENCIES //
        if (c.Billing_Currency__c != 'USD' && c.Billing_Currency__c != null) {
            //if the contract's fx date isn't today, then it must be using a previously quoted currency or both the quote and contract were created on the same day
            if (c.Billing_Currency_Rate_Date__c != date.today()) {
                DatedConversionRate rate = [SELECT id, ConversionRate, IsoCode
                                            FROM DatedConversionRate
                                            WHERE StartDate = TODAY
                                            AND IsoCode = :c.Billing_Currency__c LIMIT 1];
                decimal currentY1Total = c.Calculated_Year_1_Total__c * rate.ConversionRate;
                decimal quotedY1Total = c.Calculated_Year_1_Total__c * c.Billing_Currency_Rate__c;
                if (currentY1Total < quotedY1Total * 0.95) {
                    if (app.FX_Rate__c != 'Approved' || wasRefreshed) {
                        contractReasons.add('Price using today\'s FX rate has decreased more than 5% from quoted price');
                        app.FX_Rate__c = 'Approval Needed';
                        finance = true;
                        backToApproved = false;
                    }
                }
            }
        }
        // PROMO EXECUTE BY DATE //
        if (c.Promotion__c != null) {
            if (c.Promotion__r.End_Date__c < date.today().addDays(30)) {
                if (c.Execute_By__c != 'Specific Date' || c.Execute_By_Date__c > c.Promotion__r.End_Date__c) {
                    contractReasons.add('Execute by date beyond promo expiration date');
                    app.Promo_Execute_By_Date__c = 'Approval Needed';
                    director = true;
                    backToApproved = false;
                }
            }
        }
        // NET 90 //
        if (c.Custom_Payment_Net_Terms__c && c.Payment_Net_Terms__c == 'Net 90') {
            if (app.Custom_Net_Terms__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Net terms > 90');
                app.Custom_Net_Terms__c = 'Approval Needed';
                finance = true;
                backToApproved = false;
            }
        } else {
            if (app.Custom_Net_Terms__c != 'Approved') {
                app.Custom_Net_Terms__c = null;
            }
        }
        // OTHER SPECIAL PAYMENT TERMS //
        if (c.Other_Special_Payment_Terms__c == true) {                                     // check to see if the contract has other special payment terms 
            if (app.Other_Special_Payment_Terms__c != 'Approved' || wasRefreshed) {         // check to see that this hasnt already been approved or that the contract was refreshed            
                contractReasons.add('Other special payment terms');                         // add the reason to a list of reasons
                app.Other_Special_Payment_Terms__c = 'Approval Needed';                     // flag the approval record for approval
                director = true;finance = true;                                                // make sure to add sales (haling) to list of approvers
                backToApproved = false;                                                     // approval critiera is present and the contact won't go back to approved
            }
        } else {                                                                            // if other special payment terms gets unchecked
            if (app.Other_Special_Payment_Terms__c != 'Approved') {                         // other special payment terms hasnt been approved, make it null so it doesn't get flagged for approval
                app.Other_Special_Payment_Terms__c = null;
            }
        }
        // SPLIT BILLING //
        if (c.Split_Billing__c == true) {
            if (app.Split_Billing__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Split billing');
                app.Split_Billing__c = 'Approval Needed';
                //sales = true; finance = true;
                finance = true;
                //new
                if(sumLicensingY1Total/c.Split_Billing_Number_of_Splits__c > contractAutomationSetting.Split_Billing_Director_Approval_Minimum__c){
                    director = true;
                }
                //end of new
               backToApproved = false;
            }         
  
        } else {
            if (app.Split_Billing__c != 'Approved') {
                app.Split_Billing__c = null;
            }
        }

        
        // INSTALLMENTS //
        if (c.Installments__c == true) {
            if (app.Installments__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Installments');
                app.Installments__c = 'Approval Needed';
                finance = true;
                
                //Check to see if sales director needs to be included
                
                //Start by creating a map to hold the conversions for selected billing frequency
                Map<string,decimal> billingFrequencyMap = new map<string,decimal>();
                billingFrequencyMap.put('Monthly',12);
                billingFrequencyMap.put('Quarterly',4);
                billingFrequencyMap.put('Semi Annually',2);
                billingFrequencyMap.put('Annually',1);
                
                //If a match is found based on the contracts billing frequency
                if(billingFrequencyMap.get(c.Billing_Frequency__c) != null){
                    
                    //if the sum of licensing products/billing frequency is greater than or equal to the director approval minimum - send to sales director for approval
                    if((sumLicensingY1Total/billingFrequencyMap.get(c.Billing_Frequency__c)) >= contractAutomationSetting.Installments_Director_Approval_Minimum__c){
                        director = true;
                    }
                    //If No match was found and the Billing Frequency on the contract is "See Exhibit A" - Always send to sales director for approval    
                }else if (c.Billing_Frequency__c == 'See Exhibit A'){
                    director = true;
                }
                backToApproved = false;
            } else {
                if (app.Installments__c != 'Approved') {
                    app.Installments__c = null;
                }
            }
        }
 
        
        //RELATED CONTRACT ADD-ON //
        
        if(c.Related_Contract_Type__c == 'Add-On' && c.Contract_End_Date_new__c == null){
            if (app.Related_Contract__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Related Contract Subscription Dates');
                app.Related_Contract__c = 'Approval Needed';
                orders = true;
                backToApproved = false;
            } else {
                if (app.Related_Contract__c != 'Approved') {
                    app.Related_Contract__c = null;
                }
            }
        }
        
        // MODIFIED DOWN PAYMENT //
        if (c.Modified_Down_Payment__c == true) {
            if (app.Modified_Down_Payment__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Modified down payment');
                app.Modified_Down_Payment__c = 'Approval Needed';
                director = true;
                finance = true;
                backToApproved = false;
            }
        } else {
            if (app.Modified_Down_Payment__c != 'Approved') {
                app.Modified_Down_Payment__c = null;
            }
        }
        
        
        // Custom Billing Milestones //
        if (c.Custom_Billing_Milestones__c == true) {
            if (app.Billing_Milestones__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Custom Billing Milestones');
                app.Billing_Milestones__c = 'Approval Needed';
                //sales = true;
                finance = true;
                director = true;
                backToApproved = false;
            }
        } else {
            if (app.Billing_Milestones__c != 'Approved') {
                app.Billing_Milestones__c = null;
            }
        }

        // NON STANDARD MSA //
        if (c.Standard_MSA__c == 'No') {
            if (app.Non_Standard_MSA__c != 'Approved') {
                contractReasons.add('Non-standard MSA');
                app.Non_Standard_MSA__c = 'Approval Needed';
                backToApproved = false;
                System.debug('Here '+ contractProductSuites);
                if(contractProductSuites != 'EHS Management;' && contractProductSuites != ''){
                    sales = true;
                }
                if(contractProductSuites.contains('EHS Management') && contractProductSuites != ''){
                    EHS = true;
                }
            }
        } else {
            if (app.Non_Standard_MSA__c != 'Approved') {
                app.Non_Standard_MSA__c = null;
            }
        }
        // OPT OUT //
        if (c.Opt_Out__c == true) {
            if (app.Opt_Out__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Opt-out clause');
                app.Opt_Out__c = 'Approval Needed';
                director = true;finance = true;
                backToApproved = false;
            }
        } else {
            if (app.Opt_Out__c != 'Approved') {
                app.Opt_Out__c = null;
            }
        }
        // NON STANDARD SERVICE OFFERINGS //
        if (c.Non_Standard_Service_Approval__c == true) {
            if (app.Non_Standard_Service__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Non-standard services offerings');
                app.Non_Standard_Service__c = 'Approval Needed';
                director = true; operations = true;
                backToApproved = false;
            }
        } else {
            if (app.Non_Standard_Service__c != 'Approved') {
                app.Non_Standard_Service__c = null;
            }
        }
        // DELAYED BILLING ONLY //
        if (c.Delayed_Billing_Only__c == true && c.Delayed_Billing_Date__c > (date.today() + 60)) {
            if (app.Delayed_Billing__c != 'Approved' || c.Delayed_Billing_Date__c > app.Approved_Delayed_Billing_Date__c) {
                contractReasons.add('Deferred invoice > 60 days');
                app.Delayed_Billing__c = 'Approval Needed';
                director = true; finance = true;
                backToApproved = false;
            }
        } else {
            if (app.Delayed_Billing__c != 'Approved' || c.Delayed_Billing_Date__c < (date.today() + 60)) {
                app.Delayed_Billing__c = null;
            }
        }
        // DEFERRED INVOICE AND BILLING //
        if (c.Deferred_Invoice__c == true && c.Deferred_Invoice_Date__c > (date.today() + 60)) {
            if (app.Deferred_Invoice__c != 'Approved' || c.Deferred_Invoice_Date__c > app.Approved_Deferred_Invoice_Date__c) {
                contractReasons.add('Deferred invoice due and subscription > 60 days');
                app.Deferred_Invoice__c = 'Approval Needed';
                director = true; finance = true;
                backToApproved = false;
            }
        } else {
            if (app.Deferred_Invoice__c != 'Approved' || c.Deferred_Invoice_Date__c < (date.today() + 60)) {
                app.Deferred_Invoice__c = null;
            }
        }
        
        // UNSIGNED CONTRACTS //
        if (c.Unsigned_Contract__c == true){
            if (app.Unsigned_Contract__c != 'Approved') {
                //Only check for approval if mixed product suites OR if MSDS Management products only and Y1 Total > 500
                if((contractProductSuites == 'MSDS Management;' && c.Calculated_Year_1_Total__c > 500) 
                   || (contractProductSuites != 'MSDS Management;' && contractProductSuites != ''))
                {
                    contractReasons.add('Unsigned Contract');
                    app.Unsigned_Contract__c = 'Approval Needed';  
                    if(contractProductSuites != '' && contractProductSuites != 'EHS Management;'){
                        orders = true;
                    }
                    if(contractProductSuites.contains('EHS Management')){
                        ehs = true;
                    }
                    backToApproved = false;
                }
            }
        } else {
            if (app.Unsigned_Contract__c != 'Approved') {
                app.Unsigned_Contract__c = null;
            }
        }
                
        
        
        // SERVICES ONLY
        boolean ehsMgmt = false;
        boolean msds = false;
        boolean complianceSolutions = false;
        boolean services = false;
        boolean servicesOnly = false;
        for (Contract_Line_Item__c item : items) {
            if (item.Product__r.Contract_Print_Group__c == 'EHS Management') {
                ehsMgmt = true;
            } else if (item.Product__r.Contract_Print_Group__c == 'MSDS Management') {
                msds = true;
            } else if (item.Product__r.Contract_Print_Group__c == 'Other Compliance Solutions') {
                complianceSolutions = true;
            } else if (item.Product__r.Contract_Print_Group__c == 'Services' && !item.Name__c.contains('Authoring') && !item.Name__c.contains('Ergonomics')) {
                services = true;
            }
        }
        // If it has services and nothing else
        if (services) {
            if (!ehsMgmt && !msds && !complianceSolutions) {
                // Check the account's active products
                if (c.Account__r.Active_MSDS_Management_Products__c == null) {
                    servicesOnly = true;
                }
            }
        }
        if (servicesOnly) {
            if (app.Services_Only__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Contract contains only services items');
                app.Services_Only__c = 'Approval Needed';
                operations = true;
                backToApproved = false;
            }
        } else {
            if (app.Services_Only__c != 'Approved') {
                app.Services_Only__c = null;
            }
        }
    }
    
    
    // run through the line items and see if the language has changed
    public void lineItemCheck() {
        for (Contract_Line_Item__c item : items) {
            // if approval isnt required
            if (item.Approval_Required__c == false) {
                // if there are contract terms
                if (item.Contract_Terms__c != null) {           
                    // if the original char count is greater than 0
                    if (item.Original_Char_Count__c != 0) {                                                         
                        if (item.Contract_Terms__c.stripHTMLtags().length() != item.Original_Char_Count__c) {       
                            item.Approval_Required__c = true;                                                       
                            lineItemChanges.add(item);
                            c.Contract_Terms_Changed__c = true;
                        } 
                    } 
                    if (item.Original_Char_Count__c == 0) {                                                         
                        item.Original_Char_Count__c = item.Contract_Terms__c.stripHTMLtags().length();
                    }
                }
            } else {
                lineItemChanges.add(item);                                                                          
            }
        }
        if (lineItemChanges.size() > 0) {                                                                           
            update lineItemChanges;
            lineItemReasons();
            lang = true;
            backToApproved = false;
        }
    }
    
    // create reasons for each line item needing approval and set the correct approver
    public void lineItemReasons() {
        for (Contract_Line_Item__c item : lineItemChanges) {
            if (item.Name__c != 'Webpliance' &&                                                                     // if it isnt a special case item
                item.Name__c != 'Private Partner Interface (PPI)' && 
                item.Name__c != 'Custom Label' && 
                item.Name__c != 'Custom Services Project' && 
                item.Name__c != 'Authoring - Complex Formula Fee' && 
                item.Name__c != 'Ergonomics Optional System Services' && 
                item.Name__c != 'Authoring - Alias SDS Creation' && 
                item.Name__c != 'Authoring - Reseller SDS Creation' &&
                item.Name__c != 'ERS - Label or Website Support' &&
                item.Name__c != 'ERS - Custom Services') {
                    lineItemReasons.add(item.Name__c+': Changed contract language');
                    lang = true;
                } 
            if (c.Ergo_Optional_Services_Approval__c == true || wasRefreshed) {                                                     // if US CS approval is true or the contract was refreshed, this line item needs new language
                if (item.Name__c == 'Ergonomics Optional System Services') {
                    lineItemReasons.add(item.Name__c+': Needs new contract language');
                    orders = true;
                }
            }
            if (c.Ergo_Optional_Services_Approval__c == false) {                                                                        // if US CS approval is false, that means language was already approved. if language was changed, it only goes back to the 
                if (item.Name__c == 'Ergonomics Optional System Services') {
                    lineItemReasons.add(item.Name__c+': Changed contract language');
                    lang = true;
                }
            }
            if (c.US_CS_Approval__c == true || wasRefreshed) {                                                      // if US CS approval is true or the contract was refreshed, this line item needs new language
                if (item.Name__c == 'Custom Label' || item.Name__c == 'Custom Services Project' || item.Name__c == 'ERS - Label or Website Support' || item.Name__c == 'ERS - Custom Services') {
                    lineItemReasons.add(item.Name__c+': Needs new contract language');
                    orders = true; operations = true;
                }
            }
            if (c.US_CS_Approval__c == false) {                                                                     // if US CS approval is false, that means language was already approved. if language was changed, it only goes back to the manager
                if (item.Name__c == 'Custom Label' || item.Name__c == 'Custom Services Project' || item.Name__c == 'ERS - Label or Website Support' || item.Name__c == 'ERS - Custom Services') {
                    lineItemReasons.add(item.Name__c+': Changed contract language');
                    lang = true;
                }
            }
            if (c.Webpliance_PPI_Approval__c == true || wasRefreshed) {
                if (item.Name__c == 'Webpliance' || item.Name__c == 'Private Partner Interface (PPI)') {
                    lineItemReasons.add(item.Name__c+': Needs new contract language');
                    director = true; orders = true; operations = true;
                }
            }
            if (c.Webpliance_PPI_Approval__c == false) {
                if (item.Name__c == 'Webpliance' || item.Name__c == 'Private Partner Interface (PPI)') {
                    lineItemReasons.add(item.Name__c+': Changed contract language');
                    lang = true;
                }
            }
            if (c.Authoring_Product_Approval__c == true || wasRefreshed) {
                if (item.Name__c == 'Authoring - Complex Formula Fee' || item.Name__c == 'Authoring - Alias SDS Creation' || item.Name__c == 'Authoring - Reseller SDS Creation') {
                    lineItemReasons.add(item.Name__c+': Needs new contract language');
                    authoring = true;
                }
            }
            if (c.Authoring_Product_Approval__c == false) {
                if (item.Name__c == 'Authoring - Complex Formula Fee' || item.Name__c == 'Authoring - Alias SDS Creation' || item.Name__c == 'Authoring - Reseller SDS Creation') {
                    lineItemReasons.add(item.Name__c+': Changed contract language');
                    lang = true;
                }
            }
        }
    }
    
    // create the reasons and record on app record (probably not the best way to go about this?)
    public void approvalReasons() {                         
        Set<string> reasonSet = new Set<string>();
        if (contractReasons.size() > 0) {                                                           // if there are approval reasons on the contract, add the reasons to the set
            for (string cr : contractReasons) {
                reasonSet.add(cr);
            }
        }
        if (lineItemReasons.size() > 0) {                                                           // if there are line items needing approval, add the reasons to the set
            for (string lir : lineItemReasons) {
                reasonSet.add(lir);
            }
        }
        app.Approval_Reasons__c = '';                                                               // clear the reasons on the app, loop through the list of new reasons, and save the app
        for (string ar : reasonSet) {
            app.Approval_Reasons__c += ar+';';
        }
        app.Approval_Reasons__c = app.Approval_Reasons__c.removeStart(';').removeEnd(';');          // sometimes an extra semicolon is placed at the start / end so this will remove them
        createRepReasons();                                                                         // create reasons for the reps to see
    }
    
    public void createRepReasons() {
        c.Rep_Approval_Reasons__c = app.Approval_Reasons__c.replace(';', ', ');
    }
    
    // find the list of approvers
    public void createApproverList() {  
        string appList = 'Manager;';                                                    // first approver is always the manager
        integer num = 1;
        if (director == true) {
            appList += 'Director;';
            num++;
        }
        if (sales == true) {                                                            // if sales is true, add sales (chuck) to the list of approvers
            appList += 'Sales;';
            num++;                                                                      // this is to count how many steps are needed in the process
        }
        if (authoring == true) {
            appList += 'Authoring;';
            num++;
        }
        if (orders == true) {
            appList += 'Orders;';
            num++;
        }
        if (ehs == true) {
            appList += 'EHS;';
            num++;
        }
        if (finance == true) {
            appList += 'Finance;';
            num++;
        }
        if (operations == true) {
            appList += 'Operations;';
            num++;
        }
        
        
        app.Approvers__c = appList;                                                     // set the approvers list
        app.Num_Of_Steps__c = num;                                                      // set the number of steps
        app.Current_Step_Num__c = 1;                                                    // set the app to the first step
    }
    
    // set the approval fields on the contract so the approval process can read it
    public void flagContract() {
        for (string ar : app.Approval_Reasons__c.split(';', 0)) {
            if (ar.contains('Changed contract language')) {                             
                c.Contract_Terms_Changed__c = true;
                c.Status__c = 'Active';
            }
        }
        for (string approver : app.Approvers__c.split(';', 0)) {                        // check to see what approvers are needed and mark the contract accordingly
            if (approver == 'Sales') {
                c.Approve_Sales__c = true;
                c.Status__c = 'Active';
            } 
            if (approver == 'Orders') {
                c.Approve_Orders__c = true;
                c.Status__c = 'Active';
            }
            if (approver == 'Authoring') {
                c.Approve_Authoring__c = true;
                c.Status__c = 'Active';
            }
            if (approver == 'Finance') {
                c.Approve_Finance__c = true;
                c.Status__c = 'Active';
            }
            if (approver == 'Operations') {
                c.Approve_Operations__c = true;
                c.Status__c = 'Active';
            }
            if (approver == 'Director') {
                c.Approve_Sales_Director__c = true;
                c.Status__c = 'Active';
            }
            if (approver == 'EHS') {
                c.Approve_EHS__c = true;
                c.Status__c = 'Active';
            }
        }
    }
    
}