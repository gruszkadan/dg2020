public class opportunityUtility {
    
    public static void opportunityCreated(Opportunity[] Opptys) {
        //Set to Hold UserIDs we want to Query
        set<id> userIDs = new set<id>();
        //Go through all the opportunities and add the UserIDs to the Set
        for(Opportunity o : opptys){
            userIDs.add(System.UserInfo.getUserId());
        }
        //Query the Users
        User[] users = [Select ID, Role__c from User where ID in: userIDs];
        for(Opportunity o : opptys){
            for(User u : Users){
                //Get Information for the User Creating the Opportunity
                if(u.Id == System.UserInfo.getUserId()){
                    o.Created_By_Role__c = u.Role__c;
                }
            }
        }
    }
    
    //Add Accounts Active Products (if any)
    public static void addProducts(Opportunity[] Opptys) {
        //Set to hold AccountIDs we want to query
        set<id> accountIDs = new set<id>();
        //Go through all the Opportunities and add the AccountIDs to a set
        for(Opportunity o : opptys){
            accountIDs.add(o.AccountID);
        }
        //Query the Accounts
        Account[] Acct = [Select ID, Active_MSDS_Management_Products__c, Active_Compliance_Services_Products__c, Active_Additional_Compliance_Solutions__c, Active_Products__c, 
                          EHS_Active_Licensing__c, EHS_Active_Services__c, EHS_Active_Add_On_Services__c, Ergo_Active_Licensing__c, Ergo_Active_Services__c
                          from Account where ID in: accountIDs];
        for(Opportunity o : Opptys){
            for(Account a : Acct){
                //Authoring Products
                string authProds = 'MSDS Authoring;MSDS Translation;Regulatory Consulting';
                //ODT Products
                string odtProds = 'Environmental Toolkit;On-Demand Training;Safety Tool Kit';
                //Authoring & ODT Products
                string authODTProds = authProds+';'+odtProds;
                //Get Information for the User who Owns the Task
                if(a.Id == o.AccountID){
                    //Sets to hold products
                    set <string> auth = new set<string>();
                    set <string> odt = new set<string>();
                    set <string> acs = new set<string>();
                    set <string> cs = new set<string>();
                    if(a.Active_Compliance_Services_Products__c != NULL){
                        for (string prod : a.Active_Compliance_Services_Products__c.split(';')){
                            if (authProds.contains(prod)) {
                                auth.add(prod);
                            } else if (odtProds.contains(prod)) {
                                odt.add(prod);
                            } else {
                                cs.add(prod);
                            }
                        }
                    }
                    string activeCS;
                    if(cs.size()>0){
                        for(string c: cs){
                            if(activeCS == null || activeCS ==''){
                                activeCS= c;
                            } else {
                                activeCS = activeCS +';' + c;
                            }
                        }
                    }
                    if(a.Active_Additional_Compliance_Solutions__c != NULL){
                        for (string prod : a.Active_Additional_Compliance_Solutions__c.split(';')){
                            if (authProds.contains(prod)) {
                                auth.add(prod);
                            } else if (odtProds.contains(prod)) {
                                odt.add(prod);
                            } else {
                                acs.add(prod);
                            }
                        }
                    }
                    string activeACS;
                    if(acs.size()>0){
                        for(string ac: acs){
                            if(activeACS == null || activeACS ==''){
                                activeACS= ac;
                            } else {
                                activeACS = activeACS +';' + ac;
                            }
                        }
                    }
                    string activeODT ='';
                    if(odt.size()>0){
                        for(string ot: odt){
                            if(activeODT == null || activeODT ==''){
                                activeODT= ot;
                            } else {
                                activeODT = activeODT +';' + ot;
                            }
                        }
                    }
                    string activeAuth ='';
                    if(auth.size()>0){
                        for(string au: auth){
                            if(activeAuth == null || activeAuth ==''){
                                activeAuth= au;
                            } else {
                                activeAuth = activeAuth +';' + au;
                            }
                        }
                    }
                    o.Active_Additional_Compliance_Solutions__c = activeACS;
                    o.Active_Authoring_Products__c = activeAuth;
                    o.Active_Compliance_Services__c = activeCS;
                    o.Active_EHS_Add_On_Services__c = a.EHS_Active_Add_On_Services__c;
                    o.Active_EHS_Licensing__c = a.EHS_Active_Licensing__c;
                    o.Active_EHS_Services__c = a.EHS_Active_Services__c;
                    o.Active_Ergonomics_Licensing__c = a.Ergo_Active_Licensing__c;
                    o.Active_Ergonomics_Services__c = a.Ergo_Active_Services__c;
                    o.Active_MSDS_Management_Products__c = a.Active_MSDS_Management_Products__c;
                    o.Active_Online_Training_Products__c = activeODT;
                }
            }
        }
    }
    
    //Used to auto-create opportunities and line items from quotes
    public static void createOpportunityFromQuote(Quote__c[] quotes){
        
        //Create a set of quote owner ids used to query users
        set<id> ownerIds = new set<id>();
        for(Quote__c quote:quotes){
            ownerIds.add(quote.OwnerId);
        }
        
        //Create a map of users to reference when creating opportunities
        map<id,User> userMap = new map<id,User>([SELECT id,Group__c FROM User WHERE id in:ownerIds]);
        
        //Dynamic query for quotes and additional fields - start with  finding all field api names on the object
        Map<String, Schema.SObjectField> fieldMap = Quote__c.sObjectType.getDescribe().fields.getMap();
        Set<String> fieldNames = fieldMap.keySet();
        
        //Additional fields to query
        string addlFields = ',Account__r.Name,Account__r.Date_Connected_with_DM__c,Account_Management_Event__r.Renewal_Date__c';
        
        //Build the query string and run query
        quotes = Database.query('select ' + string.join((Iterable<String>)fieldNames, ',') + addlFields +
                                +' from Quote__c'
                                +' where id in:quotes');
        
        //List of new opportunities to be inserted
        Opportunity[] newOpps = new list<Opportunity>();
        
        //Create a new opportunity for each quote and map fields from the quote
        for(Quote__c quote:quotes){
            Opportunity opp = new Opportunity();
            opp.Name = quote.Account__r.Name;
            opp.StageName = 'New';
            opp.CloseDate = date.today();
            if(quote.Account_Management_Event__r.Renewal_Date__c != null){
                opp.CloseDate = quote.Account_Management_Event__r.Renewal_Date__c;
            }
            opp.Quote__c = quote.ID;
            opp.AccountID = quote.Account__c;
            if(Quote.Contact__c != null){
                opp.Num_Of_Primary_Contacts__c = 1;
            }
            opp.OwnerId = quote.OwnerId;
            opp.Contract_Length__c = quote.Contract_Length__c;
            opp.Term_in_Years__c = quote.Contract_Length__c;
            opp.GPO__c = quote.GPO__c;
            opp.Owner_Group__c = userMap.get(quote.OwnerId).Group__c;
            opp.Bundled_Products__c = quote.Bundled_Products__c;
            opp.Healthcare_Customer__c = quote.Healthcare_Customer__c;
            opp.Travelers_Customer__c = quote.Travelers_Customer__c;
            opp.Lead_Type__c = quote.Lead_Type__c;
            opp.Date_Connected_with_DM__c = quote.Account__r.Date_Connected_with_DM__c;
            opp.VPPPA_Customer__c = quote.VPPPA_Customer__c;
            opp.Pricebook2Id = quote.Price_Book__c;
            opp.Licensing_Covered_by_Another_Location__c = quote.Licensing_Covered_by_Another_Location__c;
            opp.Multiple_Location_Indexing__c = quote.Multiple_Location_Indexing__c;
            if(quote.Contact__c != null){
                opp.ContactID__c = quote.Contact__c;
            }
            opp.Opportunity_Type__c = quote.Type__c;
            opp.Type = quote.Type__c;
            opp.Account_Management_Event__c = quote.Account_Management_Event__c;
            opp.Currency__c = quote.Currency__c;
            opp.Currency_Rate__c = quote.Currency_Rate__c;
            opp.Currency_Rate_Date__c = quote.Currency_Rate_Date__c;
            newOpps.add(opp);
        }
        
        //Insert the new opportunities
        insert newOpps;
        
        //List of opportunity contacts to be inserted
        OpportunityContactRole[] oppContacts = new list<OpportunityContactRole>();
        
        // For each new opportunity create a new opportunity contact and update the quote with the opportunity id
        for (integer i=0; i<newOpps.size(); i++) {
            //Create Opportunity Contact
            OpportunityContactRole oppContact = new OpportunityContactRole();
            oppContact.OpportunityId = newOpps[i].Id;
            oppContact.ContactId = quotes[i].Contact__c;
            oppContact.IsPrimary = TRUE;
            oppContacts.add(oppContact);
            
            //Update Quote
            quotes[i].Opportunity__c = newOpps[i].Id;
        }
        
        //Insert the opportunity contacts
        insert oppContacts;
        
        //Update the quotes 
        update quotes;
        
        //Create opportunity line items
        createOpportunityLineItemsFromQuote(quotes);
    }
    
    public static void createOpportunityLineItemsFromQuote(Quote__c[] quotes){
        
        //Dynamic query for quote products and additional fields - start with finding all field api names on the object
        Map<String, Schema.SObjectField> fieldMap = Quote_Product__c.sObjectType.getDescribe().fields.getMap();
        Set<String> fieldNames = fieldMap.keySet();
        
        //Additional fields to query
        string addlFields = ',Quote__r.Opportunity__c,Bundled_Product__r.Name,Bundled_Product__r.Id';
        
        //Build the query string and run query
        Quote_Product__c[] quoteProducts = Database.query('select ' + string.join((Iterable<String>)fieldNames, ',') +addlFields+
                                                          +' from Quote_Product__c'
                                                          +' where Quote__c in:quotes');
        
        //List to hold new opportunity line items to be inserted
        OpportunityLineItem[] newOpportunityLineItems = new list<OpportunityLineItem>();
        
        //Create a new opportunity line item for each quote product mapping fields from the quote product
        for(Quote_Product__c item : quoteProducts) {
            OpportunityLineItem newItem = new OpportunityLineItem();
            newItem.Add_On_Product__c = item.Add_On_Product__c;
            newItem.Approval_Required_by__c  =  item.Approval_Required_by__c;
            newItem.Approval_Status__c  =  item.Approval_Status__c;
            newItem.Auth_Consulting_Service__c = item.Auth_Consulting_Service__c;
            newItem.Auth_Countries_of_Use__c = item.Auth_Countries_of_Use__c;
            newItem.Auth_Date_Quote_Required__c = item.Auth_Date_Quote_Required__c;
            newItem.Auth_End_Date__c = item.Auth_End_Date__c;
            newItem.Auth_Estimated_Start_Date__c = item.Auth_Estimated_Start_Date__c;
            newItem.Auth_Expected_End_Date__c = item.Auth_Expected_End_Date__c;
            newItem.Auth_Hourly_Rate__c = item.Auth_Hourly_Rate__c;
            newItem.Auth_Identical_Product_Grouping__c = item.Auth_Identical_Product_Grouping__c;
            newItem.Auth_Need_Addtl_Info__c = item.Auth_Need_Addtl_Info__c;
            newItem.Auth_Original_Manufacturer__c = item.Auth_Original_Manufacturer__c;
            newItem.Auth_Other_Reg_Format__c = item.Auth_Other_Reg_Format__c;
            newItem.Auth_Other_Service__c = item.Auth_Other_Service__c;
            newItem.Auth_Pilot_Program__c = item.Auth_Pilot_Program__c;
            newItem.Auth_Project_Breakdown__c = item.Auth_Project_Breakdown__c;
            newItem.Auth_Project_Data__c = item.Auth_Project_Data__c;
            newItem.Auth_Project_Terms__c = item.Auth_Project_Terms__c;
            newItem.Auth_Regulatory_Format__c = item.Auth_Regulatory_Format__c;
            newItem.Auth_Rush_Quote__c = item.Auth_Rush_Quote__c;
            newItem.Auth_Service_Requested__c = item.Auth_Service_Requested__c;
            newItem.Auth_Start_Date__c = item.Auth_Start_Date__c;
            newItem.Auth_Total_Authoring_Labels__c = item.Auth_Total_Authoring_Labels__c;
            newItem.Auth_Total_Docs__c = item.Auth_Total_Docs__c;
            newItem.Auth_Total_Hours__c = item.Auth_Total_Hours__c;
            newItem.Auth_Total_Translated_Docs__c = item.Auth_Total_Translated_Docs__c;
            newItem.Auth_Total_Translated_Labels__c = item.Auth_Total_Translated_Labels__c;
            newItem.Auth_Trade_Secrets_Proprietary__c = item.Auth_Trade_Secrets_Proprietary__c;
            newItem.Auth_Translation_Lang__c = item.Auth_Translation_Lang__c;
            newItem.Bundle__c = item.Bundle__c;
            newItem.Bundled_Product__c = item.Bundled_Product__r.Name;
            newItem.Bundled_Product_ID__c = item.Bundled_Product__r.ID;
            newItem.Bundled_Indexing_Display_Type__c = item.Bundled_Indexing_Display_Type__c;
            newItem.Configured__c  =  item.Configured__c;
            newItem.CS_Scope__c  =  item.CS_Scope__c;
            newItem.Custom_Label_Information__c  =  item.Custom_Label_Information__c;
            newItem.Custom_Quote__c = item.Custom_Quote__c;
            newItem.Custom_Services_Product_Additional_Info__c = item.Custom_Services_Product_Additional_Info__c;
            newItem.Description  =  item.Description__c;
            newItem.Discount  =  item.Discount__c;
            newItem.Discount_Reason__c = item.Discount_Reason__c;
            newItem.eBinder_Replication_Account__c = item.eBinder_Replication_Account__c;
            newItem.ERS_Battery_Shipper__c = item.ERS_Battery_Shipper__c;
            newItem.ERS_Custom_Script__c = item.ERS_Custom_Script__c;
            newItem.ERS_Dedicated_800_or_Country_Phone__c = item.ERS_Dedicated_800_or_Country_Phone__c;
            newItem.ERS_eBinder_Build_Type__c = item.ERS_eBinder_Build_Type__c;
            newItem.ERS_Fedex_Account__c = item.ERS_Fedex_Account__c;
            newItem.ERS_Fedex_Account_Number__c = item.ERS_Fedex_Account_Number__c;
            newitem.Is_Demonstration_Project__c = item.Is_Demonstration_Project__c;
            newItem.Minimum_Price_Applied__c = item.Minimum_Price_Applied__c;
            newItem.Expected_Delivery_Date__c = item.Expected_Delivery_Date__c;
            newItem.Frequency__c = item.Frequency__c;
            newItem.Group_ID__c = item.Group_ID__c;
            newItem.Group_Name__c = item.Group_Name__c;
            newItem.Group_Parent_ID__c = item.Group_Parent_ID__c;
            newItem.Has_Overage_Amount__c = item.Has_Overage_Amount__c;
            newItem.Indexing_Type__c = item.Indexing_Type__c;
            newItem.List_Price__c  =  item.List_Price__c;
            newItem.New_Revenue_Price__c = item.New_Revenue_Price__c;
            newItem.Notes__c  =  item.Notes__c;
            newItem.ODT_Flex_Max_Num_of_Enrollments__c = item.ODT_Flex_Max_Num_of_Enrollments__c;
            newItem.ODT_num_of_Courses__c = item.ODT_num_of_Courses__c;
            newItem.ODT_Scorm_Num_of_Courses__c = item.ODT_Scorm_Num_of_Courses__c;
            newItem.ODT_Server_Lic_Name_of_Clients_LMS__c = item.ODT_Server_Lic_Name_of_Clients_LMS__c;
            newItem.ODT_Server_Lic_num_of_Courses__c = item.ODT_Server_Lic_num_of_Courses__c;
            newItem.ODT_Server_Lic_Num_of_Employees__c = item.ODT_Server_Lic_Num_of_Employees__c;
            newItem.ODTDev_Stand_Alone__c  =  item.ODTDev_Stand_Alone__c;
            newItem.One_Time__c = item.One_Time__c;
            newItem.Onsite_Contact_Info_Address__c = item.Onsite_Contact_Info_Address__c;
            newItem.Onsite_Escort__c  =  item.Onsite_Escort__c;
            newItem.Onsite_Hourly_Rate__c = item.Onsite_Hourly_Rate__c;
            newItem.Onsite_Locations__c  =  item.Onsite_Locations__c;
            newItem.Onsite_Maximum_Hours__c = item.Onsite_Maximum_Hours__c;
            newItem.Onsite_Maximum_Travel_Expense__c = item.Onsite_Maximum_Travel_Expense__c;
            newItem.Onsite_of_Products__c  =  item.Onsite_of_Products__c;
            newItem.Onsite_Special_Precautionary__c  =  item.Onsite_Special_Precautionary__c;
            newItem.Onsite_Square_Footage__c  =  item.Onsite_Square_Footage__c;
            newItem.Onsite_Timeline__c  =  item.Onsite_Timeline__c;
            newItem.OpportunityID = item.Quote__r.Opportunity__c;
            newItem.Other_Discount_Reason__c = item.Other_Discount_Reason__c;
            newItem.Overage_List_Price__c = item.Overage_List_Price__c;
            newItem.Overage_Quote_Price__c = item.Overage_Quote_Price__c;
            newItem.PB_Address__c  =  item.PB_Address__c;
            newItem.PB_Collate__c  =  item.PB_Collate__c;
            newItem.PB_Colored_sheet_separator__c  =  item.PB_Colored_sheet_separator__c;
            newItem.PB_Delivery_priority__c  =  item.PB_Delivery_priority__c;
            newItem.PB_Enclose_in_sheet_protector__c  =  item.PB_Enclose_in_sheet_protector__c;
            newItem.PB_How_Many_Copies__c  =  item.PB_How_Many_Copies__c;
            newItem.PB_Include_An_Index__c  =  item.PB_Include_An_Index__c;
            newItem.PB_Include_Archived_Docs__c  =  item.PB_Include_Archived_Docs__c;
            newItem.PB_Include_Inactive_Documents__c = item.PB_Include_Inactive_Documents__c;
            newItem.PB_Location_Names__c = item.PB_Location_Names__c;
            newItem.PB_Print_Layout__c  =  item.PB_Print_Layout__c;
            newItem.PB_Print_on_both_sides__c = item.PB_Print_on_both_sides__c;
            newItem.PB_Staple_each_document__c  =  item.PB_Staple_each_document__c;
            newItem.PB_Three_hole_punch__c  =  item.PB_Three_hole_punch__c;
            newItem.PB_Total_Num_of_Documents__c = item.PB_Total_Num_of_Documents__c;
            newItem.PB_What_Orders_to_Print__c  =  item.PB_What_Orders_to_Print__c;
            newItem.PB_Which_Documents__c  =  item.PB_Which_Documents__c;
            newItem.PI__c = item.PI__c;
            newItem.Premium_Applied__c  =  item.Premium_Applied__c;
            newItem.Price_Override__c  =  item.Price_Override__c;
            newItem.PricebookEntryId  =  item.PricebookEntryId__c;
            newItem.Pricing_Display__c = item.Pricing_Display__c;
            newItem.Print_Group__c  =  item.Print_Group__c;
            newItem.Product_Print_Name__c = item.Product_Print_Name__c;
            newItem.ProductId__c  =  item.Product__c;
            newitem.Product_Sub_Type__c = item.Product_Sub_Type__c;
            newItem.Product_Types__c = item.Product_Types__c;
            newItem.Product_Type_Custom__c = item.Product_Type_Custom__c;
            newItem.qpID__c = item.Id;
            newItem.Quantity__c = item.Quantity__c;
            if(item.Quantity__c >0){
                if(item.Quantity_Calculation__c == TRUE){
                    newItem.Quantity = item.Quantity__c;
                }else{
                    newItem.Quantity  =  1;
                }
            } else {
                newItem.Quantity  =  1;
            }
            newItem.Quantity_Calculation__c = item.Quantity_Calculation__c;
            newItem.Quantity_Field_Name__c  =  item.Quantity_Field_Name__c;
            newItem.Quote_Price__c  =  item.Quote_Price__c;
            newItem.Renewal_Amount__c = item.Renewal_Amount__c;
            newItem.Show_Year_List__c  =  item.Show_Year_List__c;
            newItem.Standard_MSDS_Inclusion_1__c = item.Standard_MSDS_Inclusion_1__c;
            newItem.Standard_MSDS_Inclusion_2__c = item.Standard_MSDS_Inclusion_2__c;
            newItem.Standard_MSDS_Inclusion_3__c = item.Standard_MSDS_Inclusion_3__c;
            newItem.Total_Line_Value__c  =  item.Total_Line_Value__c;
            newItem.Type__c = item.Type__c;
            if(item.Quantity_Calculation__c == TRUE && item.Year_1__c == TRUE){
                if(item.Y1_Quote_Price__c == NULL){
                    newItem.UnitPrice = item.Y1_List_Price__c;
                } else {
                    newItem.UnitPrice = item.Y1_Quote_Price__c;
                }
            } else {
                newItem.UnitPrice = item.Y1_Total_Price__c;
            }
            newItem.Update_Services_Type__c  =  item.Update_Services_Type__c;
            newItem.Verification_Percentage__c = item.Verification_Percentage__c;
            newItem.Y1_Bundled_Price__c = item.Y1_Bundled_Price__c;
            newItem.Y1_Canada_Bundle_Offset__c = item.Y1_Canada_Bundle_Offset__c;
            newItem.Y1_Custom_List_Price__c = item.Y1_Custom_Proposal_List_Price__c;                
            newItem.Y1_Discount__c  =  item.Y1_Discount__c;
            newItem.Y1_List_Price__c  =  item.Y1_List_Price__c;
            newItem.Y1_Quote_Price__c  =  item.Y1_Quote_Price__c;
            newItem.Y1_Travelers_Discount_Applied__c = item.Y1_Travelers_Discount_Applied__c;
            newItem.Y2_Bundled_Price__c = item.Y2_Bundled_Price__c;
            newItem.Y2_Canada_Bundle_Offset__c = item.Y2_Canada_Bundle_Offset__c;
            newItem.Y2_Custom_List_Price__c = item.Y2_Custom_Proposal_List_Price__c;
            newItem.Y2_Discount__c  =  item.Y2_Discount__c;
            newItem.Y2_List_Price__c  =  item.Y2_List_Price__c;
            newItem.Y2_Quote_Price__c  =  item.Y2_Quote_Price__c;
            newItem.Y2_Travelers_Discount_Applied__c = item.Y2_Travelers_Discount_Applied__c;
            newItem.Y3_Bundled_Price__c = item.Y3_Bundled_Price__c;
            newItem.Y3_Canada_Bundle_Offset__c = item.Y3_Canada_Bundle_Offset__c;
            newItem.Y3_Custom_List_Price__c = item.Y3_Custom_Proposal_List_Price__c;
            newItem.Y3_Discount__c  =  item.Y3_Discount__c;
            newItem.Y3_List_Price__c  =  item.Y3_List_Price__c;
            newItem.Y3_Quote_Price__c  =  item.Y3_Quote_Price__c;
            newItem.Y3_Travelers_Discount_Applied__c = item.Y3_Travelers_Discount_Applied__c;
            newItem.Y4_Bundled_Price__c = item.Y4_Bundled_Price__c;
            newItem.Y4_Canada_Bundle_Offset__c = item.Y4_Canada_Bundle_Offset__c;
            newItem.Y4_Custom_List_Price__c = item.Y4_Custom_Proposal_List_Price__c;
            newItem.Y4_Discount__c  =  item.Y4_Discount__c;
            newItem.Y4_List_Price__c  =  item.Y4_List_Price__c;
            newItem.Y4_Quote_Price__c  =  item.Y4_Quote_Price__c;
            newItem.Y4_Travelers_Discount_Applied__c = item.Y4_Travelers_Discount_Applied__c;
            newItem.Y5_Bundled_Price__c = item.Y5_Bundled_Price__c;
            newItem.Y5_Canada_Bundle_Offset__c = item.Y5_Canada_Bundle_Offset__c;
            newItem.Y5_Custom_List_Price__c = item.Y5_Custom_Proposal_List_Price__c;
            newItem.Y5_Discount__c  =  item.Y5_Discount__c;
            newItem.Y5_List_Price__c  =  item.Y5_List_Price__c;
            newItem.Y5_Quote_Price__c  =  item.Y5_Quote_Price__c;
            newItem.Y5_Travelers_Discount_Applied__c = item.Y5_Travelers_Discount_Applied__c;
            newItem.Year_1__c  =  item.Year_1__c;
            newItem.Year_2__c  =  item.Year_2__c;
            newItem.Year_3__c  =  item.Year_3__c;
            newItem.Year_4__c  =  item.Year_4__c;
            newItem.Year_5__c  =  item.Year_5__c;
            newItem.Type__c = item.Type__c;
            newItem.Executed_Services__c = item.Executed_Services__c;
            newItem.Special_Indexing_Fields__c = item.Special_Indexing_Fields__c; 
            newItem.Language_Support_Languages__c = item.Language_Support_Languages__c;
            newItem.Pre_Scheduled_Training_Num_of_Modules__c = item.Pre_Scheduled_Training_Num_of_Modules__c;
            newItem.Pre_Scheduled_Training_Modules__c = item.Pre_Scheduled_Training_Modules__c;
            newItem.Pre_Scheduled_Training_Invitations__c = item.Pre_Scheduled_Training_Invitations__c;
            newItem.Product_Pitched_By__c = item.Product_Pitched_By__c;
            newItem.Proposal_Terms__c = item.Proposal_Terms__c;
            newItem.Attachment__c = item.Attachment__c;
            newItem.Rush__c = item.Rush__c;
            newItem.Rush_Reason__c = item.Rush_Reason__c;
            newItem.Desired_Completion_Date__c = item.Desired_Completion_Date__c;
            newItem.Indexing_Language__c = item.Indexing_Language__c;
            newItem.Rush_Timeframe__c = item.Rush_Timeframe__c;
            newOpportunityLineItems.add(newItem);
        }
        
        //Insert the new opportunity line items
        insert newOpportunityLineItems;
    }
    
}