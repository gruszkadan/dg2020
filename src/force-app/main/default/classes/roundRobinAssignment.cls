public class roundRobinAssignment {
    
    public static void roundRobinOwnershipAssignment(list<sObject> records, string sObjectType, string OwnerType) {
        sObjectField OwnerField;
        sObjectField OwnerID;
        
        list<Round_Robin_Assignment__c> ownerAssignments = [SELECT id, Order__c, Used_Last__c, UserID__c, Type__c
                                                            FROM Round_Robin_Assignment__c
                                                            Where Type__c =: OwnerType
                                                            ORDER BY Order__c ASC NULLS LAST];
        
        If(sObjectType == 'Lead'){  
            if(OwnerType == 'Enterprise'){
                OwnerField = Lead.Chemical_Management_Owner__c; 
                OwnerId = Lead.OwnerId;
            } Else  {
                OwnerField = Lead.Ergonomics_Owner__c;
            } 
        }
        
        If(sObjectType == 'Account'){  
            OwnerField = Account.Ergonomics_Account_Owner__c;
        } 
        
        
        if (records.size() > 0) {
            decimal numLeads = records.size();
            decimal numOwners = ownerAssignments.size();
            integer lastOwnerNum = 0;
            if ((numLeads / numOwners) <= 1) {
                for (integer i = 0; i < records.size(); i++) {
                    records[i].put(Ownerfield, ownerAssignments[i].UserID__c);
                    lastOwnerNum = integer.valueOf(ownerAssignments[i].Order__c);
                }
            } else {
                integer numLoops = integer.valueOf((numLeads / numOwners).round(SYSTEM.roundingMode.CEILING));  
                integer numDiff = ownerAssignments.size() - ((numLoops * ownerAssignments.size()) - records.size());
                for (integer i = 0; i < numLoops; i ++) {
                    if (i != numLoops - 1) {
                        for (integer i2 = 0; i2 < numOwners; i2 ++) {
                            records[i * ownerAssignments.size() + i2].put(Ownerfield, ownerAssignments[i2].UserID__c);
                        }
                    } else {
                        for (integer i2 = 0; i2 < numDiff; i2 ++) {
                            records[i * ownerAssignments.size() + i2].put(Ownerfield, ownerAssignments[i2].UserID__c);
                            lastOwnerNum = integer.valueOf(ownerAssignments[i2].Order__c);
                        }
                    }
                }
            }
            
            //set the ownerids = the chemical management owner
            if (sObjectType == 'Lead' && OwnerType == 'Enterprise'){
                for (Sobject o: records) {
                     o.put(OwnerId, (string)o.get(OwnerField));
                    
                }
            }
            
            for (Round_Robin_Assignment__c owner : ownerAssignments) {
                integer ownerOrder = integer.valueOf(owner.Order__c);
                if (ownerOrder > lastOwnerNum) {
                    owner.Used_Last__c = false;
                    owner.Order__c = ownerOrder - lastOwnerNum;         
                }
                if (ownerOrder < lastOwnerNum) {
                    owner.Used_Last__c = false;
                    owner.Order__c = ownerOrder + (ownerAssignments.size() - lastOwnerNum);
                }
                if (ownerOrder == lastOwnerNum) {
                    owner.Used_Last__c = true;
                    owner.Order__c = ownerAssignments.size();
                }
            }
            update ownerAssignments;
        }
    }
}