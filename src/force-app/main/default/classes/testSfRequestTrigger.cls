@isTest
private class testSfRequestTrigger {
    
    
    private static testmethod void test1() {
        
        Salesforce_Request__c test = new Salesforce_Request__c();
        test.Latest_Comment__c = 'Test Comment';
        insert test;
        
        test.Latest_Comment__c = 'Test Comment 2';
        update test;
        
        //Query Note that has been created to test new info
        Salesforce_Request_Note__c reqNote = [Select ID, Comments__c FROM Salesforce_Request_Note__c WHERE Salesforce_Update__c = :test.ID];
        
        System.assertEquals(reqNote.Comments__c, 'Test Comment 2');
        
        
        
    }
    
    
    
    
}