@isTest()
Private class testAmeApprovalController {
    static testMethod void test1(){
        //This scenrio we are tesing when we submit an ame and decide not to close tasks or roll over to new ame 
        //Test Account for scenario 1 that will be linked to the ame we will create below
        Account TestAct = new Account();
        TestAct.Name = 'Old In The Way';
        
        Insert TestAct;
        
        //Test account for scenario 1 that will be used in the ame we will create below
        Contact TestCont = new Contact();  
        TestCont.FirstName = 'David';
        TestCont.LastName = 'Grisman';
        TestCont.AccountId = TestAct.Id;
        
        Insert TestCont;
        
        //Test ame for scenario  1, we will relate the contact and account to this ame that we created below
        Account_Management_Event__c TestAme = new Account_Management_Event__c();
        TestAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();
        TestAme.Account__c = TestAct.Id;
        TestAme.Contact__c = TestCont.Id;
        TestAme.Status__c = 'Active';
        
        Insert TestAme;
        //Task related to the ame created above 
        Task NewT = new Task();
        NewT.Subject = 'TestTask';
        Newt.Status = 'Not Started';
        Newt.Priority = 'Normal';
        NewT.WhatId = TestAme.Id;
        
        insert NewT;
        
        
        
        //Submit AME for approval
        //Check to see if the url parameter was set to ame Id
        //Pass in the custom controller we are using 
        //Submit AME for approval
        ameutility.submitforapproval(TestAme.id, 'approvalcomments');
        
        //Query AME to see if the ame status was set to Pending Approval 
        Account_Management_Event__c ameSet = [Select id, status__c from Account_Management_Event__c where id =: TestAme.id];
        system.assertequals('Pending Approval',ameSet.Status__c);    
        
        
        ApexPages.currentPage().getParameters().put('Id', TestAme.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(TestAme);
        ameApprovalController myController = new ameApprovalController(testController); 
        mycontroller.Submit();
        
        //Query task to see if the status was set to completed 
        Task CheckTask = [SELECT Id, Status From Task Where id =: newT.id];
        system.assertEquals('Completed',CheckTask.Status);
    }
    
    static testMethod void test2(){
        // In this test we are approving and rolling tasks to new ame 
        //Test account for scenario 2 that will be linked to the ame we will create below
        Account TestAct = new Account();
        TestAct.Name = 'Bash Brothers';
        
        Insert TestAct;
        //Test contact needed to test scenario 2
        Contact TestCont = new Contact();  
        TestCont.FirstName = 'Brooks';
        TestCont.LastName = 'Koepka';
        TestCont.AccountId = TestAct.Id;
        
        Insert TestCont;
        //New ame needed to test scenario  2, we will link the contact and account created above to this ame 
        Account_Management_Event__c SecondTest = new Account_Management_Event__c();
        
        SecondTest.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();
        SecondTest.Account__c = TestAct.Id;
        SecondTest.Contact__c = TestCont.Id;
        SecondTest.Status__c = 'Active';
        
        Insert SecondTest;
        //New task we need to insert and relate to the ame created above 
        Task NewT2 = new Task();
        NewT2.Subject = 'TestTask';
        NewT2.Status = 'Not Started';
        NewT2.Priority = 'Normal';
        NewT2.WhatId = SecondTest.Id;
        
        insert NewT2;
        
        //put parameter in url this will have our ame ID in it
        //call utullity submit method 
        //pass in custom controller used in the page 
        ApexPages.currentPage().getParameters().put('Id', SecondTest.Id);
        ameutility.submitforapproval(SecondTest.id, 'approvalcomments');
        ApexPages.StandardController testController = new ApexPages.StandardController(SecondTest);
        ameApprovalController myController = new ameApprovalController(testController); 
        
        //set the approval status and select to roll over tasks 
        mycontroller.approvalStatus = 'Approve';
        mycontroller.closeorRollover = 'Roll';
        
        //Submit approval
        mycontroller.Submit();
        
        
        //Query tasks and see if they rolled over to a new ame 
        Task CheckTask2 = [SELECT Id, whatId From Task Where id =: NewT2.id];
        system.assert(CheckTask2.whatId != SecondTest.id);
        
        
         //Query AME to see if status was set back to closed once ame is approved 
        Account_Management_Event__c ameSet = [Select id, status__c from Account_Management_Event__c where id =: SecondTest.id];
        system.assertequals('Closed', ameset.Status__c);
        
    }
    
    static testMethod void test3(){
        //In this scenario we will test when we the ame submitted for approval gets rejected  
        //Ceate new account for test 3
        Account TestAct = new Account();
        TestAct.Name = 'Tab';
        
        Insert TestAct;
        //Create test contact used for scenario 3
        Contact TestCont = new Contact();  
        TestCont.FirstName = 'Ernest';
        TestCont.LastName = 'Anastasio';
        TestCont.AccountId = TestAct.Id;
        
        Insert TestCont;
        //Create new ame for this scenario 
        Account_Management_Event__c thirdTest = new Account_Management_Event__c();
        thirdTest.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();
        thirdTest.Account__c = TestAct.Id;
        thirdTest.Contact__c = TestCont.Id;
        thirdTest.Status__c = 'Active';
        
        Insert thirdTest;
        //Create new task that will be related to the ame created above 
        Task NewT3 = new Task();
        NewT3.Subject = 'TestTask';
        NewT3.Status = 'Not Started';
        NewT3.Priority = 'Normal';
        NewT3.WhatId = thirdTest.Id;
        
        insert NewT3;
        
        //Put the id of ame in the url parameter 
        //call submit method from ame utility 
        //pass is custom controller used in the class we are testing 
        ApexPages.currentPage().getParameters().put('Id', thirdTest.Id);
        ameutility.submitforapproval(thirdTest.id, 'approvalcomments');
        
        
        //Query ame and check to see if the status was set to pending approval 
        Account_Management_Event__c ameCheck = [Select id, status__c from Account_Management_Event__c where id =: thirdTest.id];
        system.assertequals(ameCheck.Status__c,'Pending Approval');
        ApexPages.StandardController testController = new ApexPages.StandardController(thirdTest);
        ameApprovalController myController = new ameApprovalController(testController); 
        
        //set approval status to reject
        //submit for approval 
        mycontroller.approvalStatus = 'Reject';
        mycontroller.Submit();
        
        //Query tasks and see if the task was chnaged to completed 
        Task CheckTask3 = [SELECT Id, Status From Task Where id =: newT3.id];
        system.assertEquals('Completed',CheckTask3.Status);
        
        //Query AME to see if status was set back to active 
        Account_Management_Event__c ameSet = [Select id, status__c from Account_Management_Event__c where id =: thirdTest.id];
        system.assertequals('Active', ameset.Status__c);
    }
}