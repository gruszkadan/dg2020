@isTest(SeeAllData=true)
private class testContractController {
    
    public static testmethod void testcontractController_1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Quote_Promotion__c promo = new Quote_Promotion__c();
        promo.Name = 'Test Promo';
        promo.Start_Date__c = date.today();
        promo.End_Date__c = date.today().addDays(90);
        promo.Code__c = 'Test';
        insert promo;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Promotion__c = promo.id;
        insert newQuote;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Quote__c = newQuote.Id; 
        insert newOpportunity;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.First_Approver__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        newContract.Initial_Approval_Check__c = FALSE;
        newContract.Opportunity__c = newOpportunity.id;
        newContract.Approve_Sales__c = true;
        newContract.status__c = 'Active';
        newContract.OwnerId = [Select id from User where Sales_Director__c != Null AND Managerid != NULL LIMIT 1].id;
        insert newContract; 
        
        System.debug(newContract.OwnerId );
        
          // create a test product
        Product2 newProd2 = new Product2();
        newProd2.Name = 'Webpliance';
        newProd2.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        newProd2.Contract_Print_Group__c = 'MSDS Management';
        insert newProd2;
        
        Product2 newProd3 = new Product2();
        newProd3.Name = 'GM Account';
        newProd3.Proposal_Print_Group__c = 'Other Compliance Solutions';
        newProd3.Contract_Print_Group__c = 'Other Compliance Solutions';
        insert newProd3;
        
        Product2 newProd4 = new Product2();
        newProd4.Name = 'Ergonomics Licensing';
        newProd4.Proposal_Print_Group__c = 'EHS Management';
        newProd4.Contract_Print_Group__c = 'EHS Management';
        insert newProd4;
        
        Product2 newProd = new Product2();
        newProd.Name = 'eBinder Valet';
        newProd.Proposal_Print_Group__c = 'Services';
        newProd.Contract_Print_Group__c = 'Services';
        insert newProd;
        
        Contract_Line_Item__c service1 = new Contract_Line_Item__c();
        service1.Contract__c = newContract.Id;
        service1.Product__c = newProd.id;
        service1.Quantity__c = 100;
        service1.Y1_List_Price__c = 50;
        service1.Sort_order__c = 1;
        service1.Year_1_Manual__c = 0;
        service1.Year_2_Manual__c = 0;
        service1.Year_3_Manual__c = 0;
        service1.Year_4_Manual__c = 0;
        service1.Year_5_Manual__c = 0;
        service1.Name__c = newProd.Name;
        insert service1;
        
        Contract_Line_Item__c service2 = new Contract_Line_Item__c();
        service2.Contract__c = newContract.Id;
        service2.Product__c = newProd.id;
        service2.Sort_order__c = 2;
        service2.Name__c = newProd.Name;
        
        insert service2;
        
        Contract_Line_Item__c management1 = new Contract_Line_Item__c();
        management1.Contract__c = newContract.Id;
        management1.Product__c = newProd2.id;
        management1.Sort_order__c = 1;
        management1.Name__c = newProd2.Name;
        insert management1;
        management1.Contract_Terms__c = 'test terms';
        management1.Contract_Terms_Char_Count__c = null;
        update management1;
        
        Contract_Line_Item__c management2 = new Contract_Line_Item__c();
        management2.Contract__c = newContract.Id;
        management2.Product__c = newProd2.id; 
        management2.Quantity__c = 100;
        management2.Y1_List_Price__c = 500;
        management2.Y1_Quote_Price__c = 500;
        management2.Sort_order__c = 2;
        management2.Name__c = newProd2.Name;
        insert management2;
        
        Contract_Line_Item__c compliance1 = new Contract_Line_Item__c();
        compliance1.Contract__c = newContract.Id;
        compliance1.Product__c = newProd3.id;
        compliance1.Sort_order__c = 1;
        compliance1.Name__c = newProd3.Name;
        insert compliance1;
        
        Contract_Line_Item__c compliance2 = new Contract_Line_Item__c();
        compliance2.Contract__c = newContract.Id;
        compliance2.Product__c = newProd3.id;
        compliance2.Sort_order__c = 2;
        compliance2.Name__c = newProd3.Name;
        insert compliance2;
        
        Contract_Line_Item__c ehs1 = new Contract_Line_Item__c();
        ehs1.Contract__c = newContract.Id;
        ehs1.Product__c = newProd4.id;
        ehs1.Sort_order__c = 1;
        ehs1.Name__c = newProd4.Name;
        insert ehs1;
        
        Contract_Line_Item__c ehs2 = new Contract_Line_Item__c();
        ehs2.Contract__c = newContract.Id;
        ehs2.Product__c = newProd4.id;
        ehs2.Sort_order__c = 2;
        ehs2.Name__c = newProd4.Name;
        insert ehs2;
        
        Attachment myFile = new Attachment();
        myFile.parentId = newContract.id;
        myFile.Name='test file';
        myFile.Body= blob.valueOf('Test Data');  
        insert myFile;
        
        Attachment emsa = new Attachment();
        emsa.ParentId = newContract.id;
        emsa.Name = 'Ergonomics Terms and Conditions.pdf';
        emsa.Body = blob.valueOf('Ergo MSA Test');
        insert emsa;
        
       test.startTest();
        
        ApexPages.currentPage().getParameters().put('id', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
        contractController myController = new contractController(testController);
        
        //myController.theContract = newContract;
        myController.getMyFile();
        myController.myFile = myFile;
        myController.getAtts();
        myController.getApp();
        myController.refreshFromOpportunity();
        myController.getDiscountCodes();
        myController.getInitalContractApprovers();
        myController.acceptOrderChanges();
        myController.processOrderChanges();
        myController.submitCOFChanges();
        myController.acceptOrder();
        myController.processOrder();
        myController.submitCOFtoOrders();
        myController.backToContract();
        myController.sendToOrders();
        myController.sendBackToOrders();
        myController.declineOrder();
        myController.theContract.Approval_Submission_Comments__c = 'test';
        myController.saveSubmitApproval();
        myController.contractEdit();
        myController.manualBuild();
        myController.gotoReview();
        myController.updateStatus();
        mycontroller.MarkContractUnderReview();
        system.assertEquals('Under Review', myController.theContract.status__c);

        Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
        req.setComments('test');
        req.setAction('Reject');
        string work = '';
        for (ProcessInstanceWorkItem workItem : [SELECT p.id FROM ProcessInstanceWorkItem p WHERE p.ProcessInstance.TargetObjectid = :myController.theContract.id]) {
            work = String.ValueOf(workItem.id);            
        }
        req.setWorkItemid(work);
        Approval.Processresult result = Approval.process(req);
        
        myController.firstUnsignedApprover = '00580000003UChI';
        myController.theContract.Unsigned_Contract__c = true;
        myController.theContract.Approve_Sales__c = false;
        myController.theContract.Status__c = 'Approved'; 
        myController.sendToOrder2();
        myController.unsignedContractCheck();
        myController.fileExt = 'pdf';
        myController.markDead();
        
        mycontroller.theContract.Product_Group__c = 'EHS';
        //myController.checkApprover();
        myController.contractFee();
        myController.countChars();
                
        //myController.getErgoMSA();
        //myController.ergoMSAfileExt = '.pdf';
        //myController.uploadNewErgoMSA();
        ApexPages.currentPage().getParameters().put('viewItemId',management1.id);
        ApexPages.currentPage().getParameters().put('viewItemNotesType','Discount');
        myController.viewNotes();
        test.stopTest();
    } 
    
    
    public static testmethod void testcontractControllerTurnOverNotes() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Quote_Promotion__c promo = new Quote_Promotion__c();
        promo.Name = 'Test Promo';
        promo.Start_Date__c = date.today();
        promo.End_Date__c = date.today().addDays(90);
        promo.Code__c = 'Test';
        insert promo;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Promotion__c = promo.id;
        insert newQuote;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Quote__c = newQuote.Id; 
        insert newOpportunity;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.First_Approver__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        newContract.Initial_Approval_Check__c = FALSE;
        newContract.Opportunity__c = newOpportunity.id;
        newContract.Approve_Sales__c = true;
        newContract.OwnerId = [Select id from User where Sales_Director__c != Null AND Managerid != NULL LIMIT 1].id;
        insert newContract; 
        
        System.debug(newContract.OwnerId );
        
          // create a test product
        Product2 newProd2 = new Product2();
        newProd2.Name = 'Webpliance';
        newProd2.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        newProd2.Contract_Print_Group__c = 'MSDS Management';
        insert newProd2;
        
        Product2 newProd3 = new Product2();
        newProd3.Name = 'HQ Account;';
        newProd3.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        newProd3.Contract_Print_Group__c = 'MSDS Management';
        newProd3.Requires_Sales_Turnover_Notes__c = TRUE;
        insert newProd3;
        
        Product2 newProd4 = new Product2();
        newProd4.Name = 'Ergonomics Licensing';
        newProd4.Proposal_Print_Group__c = 'EHS Management';
        newProd4.Contract_Print_Group__c = 'EHS Management';
        newprod4.Product_Suite__c = 'EHS Management';
        insert newProd4;
        
        Product2 newProd = new Product2();
        newProd.Name = 'eBinder Valet';
        newProd.Proposal_Print_Group__c = 'Services';
        newProd.Contract_Print_Group__c = 'Services';
        insert newProd;
        
        Contract_Line_Item__c service1 = new Contract_Line_Item__c();
        service1.Contract__c = newContract.Id;
        service1.Product__c = newProd.id;
        service1.Quantity__c = 100;
        service1.Y1_List_Price__c = 50;
        service1.Sort_order__c = 1;
        service1.Year_1_Manual__c = 0;
        service1.Year_2_Manual__c = 0;
        service1.Year_3_Manual__c = 0;
        service1.Year_4_Manual__c = 0;
        service1.Year_5_Manual__c = 0;
        service1.Name__c = newProd.Name;
        insert service1;
        
        Contract_Line_Item__c service2 = new Contract_Line_Item__c();
        service2.Contract__c = newContract.Id;
        service2.Product__c = newProd.id;
        service2.Sort_order__c = 2;
        service2.Name__c = newProd.Name;
        
        insert service2;
        
        Contract_Line_Item__c management1 = new Contract_Line_Item__c();
        management1.Contract__c = newContract.Id;
        management1.Product__c = newProd2.id;
        management1.Sort_order__c = 1;
        management1.Name__c = newProd2.Name;
        insert management1;
        management1.Contract_Terms__c = 'test terms';
        management1.Contract_Terms_Char_Count__c = null;
        update management1;
        
        Contract_Line_Item__c management2 = new Contract_Line_Item__c();
        management2.Contract__c = newContract.Id;
        management2.Product__c = newProd2.id; 
        management2.Quantity__c = 100;
        management2.Y1_List_Price__c = 500;
        management2.Y1_Quote_Price__c = 500;
        management2.Sort_order__c = 2;
        management2.Name__c = newProd2.Name;
        insert management2;
        
        Contract_Line_Item__c hq = new Contract_Line_Item__c();
        hq.Contract__c = newContract.Id;
        hq.Product__c = newProd3.id;
        hq.Sort_order__c = 1;
        hq.Name__c = newProd3.Name;
        insert hq;
        
        Contract_Line_Item__c compliance2 = new Contract_Line_Item__c();
        compliance2.Contract__c = newContract.Id;
        compliance2.Product__c = newProd3.id;
        compliance2.Sort_order__c = 2;
        compliance2.Name__c = newProd3.Name;
        insert compliance2;
        
        Contract_Line_Item__c ehs1 = new Contract_Line_Item__c();
        ehs1.Contract__c = newContract.Id;
        ehs1.Product__c = newProd4.id;
        ehs1.Sort_order__c = 1;
        ehs1.Name__c = newProd4.Name;
        insert ehs1;
        
        Contract_Line_Item__c ehs2 = new Contract_Line_Item__c();
        ehs2.Contract__c = newContract.Id;
        ehs2.Product__c = newProd4.id;
        ehs2.Sort_order__c = 2;
        ehs2.Name__c = newProd4.Name;
        insert ehs2;
        
        test.startTest();
        
        ApexPages.currentPage().getParameters().put('id', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
        contractController myController = new contractController(testController);
        myController.createNotes();
        myController.hasValidTurnOverNotes();
        myController.sendToOrder2();
        test.stopTest();
        
    }
    
    
    
    public static testmethod void testContract_manual_build() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '4 Years';
        insert newContract;  
        
        Product2 ehsprod = new Product2(Name = 'Test EHS Product', Contract_Print_Group__c = 'EHS Management');
        insert ehsprod;
        
        Product2[] products = new List<Product2>();
        Product2[] prods = [SELECT id, Name, Contract_Print_Group__c FROM Product2 
                            WHERE Name = 'HQ Account' 
                            OR Name = 'HQ Implementation Fee'
                            OR Name = 'Spill Center'];
        products.addAll(prods);
        products.addAll(prods);
        products.add(ehsprod);
        products.add(ehsprod);
        
        Contract_Line_Item__c[] items = new List<Contract_Line_Item__c>();
        for (integer i=0; i<products.size(); i++) {
            Contract_Line_Item__c item = new Contract_Line_Item__c();
            item.Contract__c = newContract.id;
            item.Product__c = products[i].id;
            item.Sort_Order__c = i;
            if (i==0) {
                item.Year_1_Manual__c = 999.99;
                item.Year_2_Manual__c = 999.99;
                item.Year_3_Manual__c = 999.99;
                item.Year_4_Manual__c = 999.99;
            }
            items.add(item);
        }
        insert items;
      
        ApexPages.currentPage().getParameters().put('id', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
        contract_Manual_build con = new contract_manual_build(testController);
        
        con.checkManualEdits();
        con.saveAll();
        
               
        con.clearManualFields(items[0], true);
        
        Address__c newAddress = new Address__c();
        newAddress.Account__c = newAccount.Id;
        newAddress.City__c = 'test';
        newAddress.Country__c = 'USA';
        newAddress.IsPrimary__c = true;
        newAddress.State__c = 'IL';
        newAddress.Street__c = '222';
        newAddress.Zip_Postal_Code__c = '612365';
        con.getAddressString(newAddress.id); 
    }
    
    public static testmethod void testContract_manual_build_Part2() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        insert newContract;  
        
        Product2 ehsprod = new Product2(Name = 'Test EHS Product', Contract_Print_Group__c = 'EHS Management');
        insert ehsprod;
        
        Product2[] products = new List<Product2>();
        Product2[] prods = [SELECT id, Name, Contract_Print_Group__c FROM Product2 
                            WHERE Name = 'HQ Account' 
                            OR Name = 'HQ Implementation Fee'
                            OR Name = 'Spill Center'];
        products.addAll(prods);
        products.addAll(prods);
        products.add(ehsprod);
        products.add(ehsprod);
        
        Contract_Line_Item__c[] items = new List<Contract_Line_Item__c>();
        for (integer i=0; i<products.size(); i++) {
            Contract_Line_Item__c item = new Contract_Line_Item__c();
            item.Contract__c = newContract.id;
            item.Product__c = products[i].id;
            item.Sort_Order__c = i;
            items.add(item);
        }
        insert items;
      
        ApexPages.currentPage().getParameters().put('id', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
        contract_Manual_build con = new contract_manual_build(testController);
        
        con.checkManualEdits();
        con.saveAll();
        con.getAddressString(address.id);
    
    }
 
     public static testmethod void testOrders() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Quote_Promotion__c promo = new Quote_Promotion__c();
        promo.Name = 'Test Promo';
        promo.Start_Date__c = date.today();
        promo.End_Date__c = date.today().addDays(90);
        promo.Code__c = 'Test';
        insert promo;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Promotion__c = promo.id;
        insert newQuote;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Quote__c = newQuote.Id; 
        insert newOpportunity;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.First_Approver__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        newContract.Initial_Approval_Check__c = FALSE;
        newContract.Opportunity__c = newOpportunity.id;
        newContract.Approve_Sales__c = true;
        newContract.status__c = 'Active';
        newContract.A2_Suggested_URL__c = 'www.xyz.com';
        newContract.OwnerId = [Select id from User where Sales_Director__c != Null AND Managerid != NULL LIMIT 1].id;
        insert newContract; 
        
        System.debug(newContract.OwnerId );
        
          // create a test product
        Product2 newProd2 = new Product2();
        newProd2.Name = 'Webpliance';
        newProd2.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        newProd2.Contract_Print_Group__c = 'MSDS Management';
        insert newProd2;
        
        Product2 newProd3 = new Product2();
        newProd3.Name = 'GM Account';
        newProd3.Proposal_Print_Group__c = 'Other Compliance Solutions';
        newProd3.Contract_Print_Group__c = 'Other Compliance Solutions';
        insert newProd3;
        
        Product2 newProd4 = new Product2();
        newProd4.Name = 'Ergonomics Licensing';
        newProd4.Proposal_Print_Group__c = 'EHS Management';
        newProd4.Contract_Print_Group__c = 'EHS Management';
        insert newProd4;
        
        Product2 newProd = new Product2();
        newProd.Name = 'eBinder Valet';
        newProd.Proposal_Print_Group__c = 'Services';
        newProd.Contract_Print_Group__c = 'Services';
        insert newProd;
        
        Contract_Line_Item__c service1 = new Contract_Line_Item__c();
        service1.Contract__c = newContract.Id;
        service1.Product__c = newProd.id;
        service1.Quantity__c = 100;
        service1.Y1_List_Price__c = 50;
        service1.Sort_order__c = 1;
        service1.Year_1_Manual__c = 0;
        service1.Year_2_Manual__c = 0;
        service1.Year_3_Manual__c = 0;
        service1.Year_4_Manual__c = 0;
        service1.Year_5_Manual__c = 0;
        service1.Name__c = newProd.Name;
        insert service1;
        
        Contract_Line_Item__c service2 = new Contract_Line_Item__c();
        service2.Contract__c = newContract.Id;
        service2.Product__c = newProd.id;
        service2.Sort_order__c = 2;
        service2.Name__c = newProd.Name;
        
        insert service2;
        
        Contract_Line_Item__c management1 = new Contract_Line_Item__c();
        management1.Contract__c = newContract.Id;
        management1.Product__c = newProd2.id;
        management1.Sort_order__c = 1;
        management1.Name__c = newProd2.Name;
        insert management1;
        management1.Contract_Terms__c = 'test terms';
        management1.Contract_Terms_Char_Count__c = null;
        update management1;
        
        Contract_Line_Item__c management2 = new Contract_Line_Item__c();
        management2.Contract__c = newContract.Id;
        management2.Product__c = newProd2.id; 
        management2.Quantity__c = 100;
        management2.Y1_List_Price__c = 500;
        management2.Y1_Quote_Price__c = 500;
        management2.Sort_order__c = 2;
        management2.Name__c = newProd2.Name;
        insert management2;
        
        Contract_Line_Item__c compliance1 = new Contract_Line_Item__c();
        compliance1.Contract__c = newContract.Id;
        compliance1.Product__c = newProd3.id;
        compliance1.Sort_order__c = 1;
        compliance1.Name__c = newProd3.Name;
        insert compliance1;
        
        Contract_Line_Item__c compliance2 = new Contract_Line_Item__c();
        compliance2.Contract__c = newContract.Id;
        compliance2.Product__c = newProd3.id;
        compliance2.Sort_order__c = 2;
        compliance2.Name__c = newProd3.Name;
        insert compliance2;
        
        Contract_Line_Item__c ehs1 = new Contract_Line_Item__c();
        ehs1.Contract__c = newContract.Id;
        ehs1.Product__c = newProd4.id;
        ehs1.Sort_order__c = 1;
        ehs1.Name__c = newProd4.Name;
        insert ehs1;
        
        Contract_Line_Item__c ehs2 = new Contract_Line_Item__c();
        ehs2.Contract__c = newContract.Id;
        ehs2.Product__c = newProd4.id;
        ehs2.Sort_order__c = 2;
        ehs2.Name__c = newProd4.Name;
        insert ehs2;
        
        Attachment myFile = new Attachment();
        myFile.parentId = newContract.id;
        myFile.Name='test file';
        myFile.Body= blob.valueOf('Test Data');  
        insert myFile;
        
        Attachment emsa = new Attachment();
        emsa.ParentId = newContract.id;
        emsa.Name = 'Ergonomics Terms and Conditions.pdf';
        emsa.Body = blob.valueOf('Ergo MSA Test');
        insert emsa;
        
       test.startTest();
        
        ApexPages.currentPage().getParameters().put('id', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
        contractController myController = new contractController(testController);
        
        //myController.theContract = newContract;
        myController.getMyFile();
        myController.myFile = myFile;
        myController.getAtts();
        myController.getApp();
        myController.refreshFromOpportunity();
        myController.getDiscountCodes();
        myController.getInitalContractApprovers();
        myController.acceptOrderChanges();
        myController.processOrderChanges();
        myController.submitCOFChanges();
        myController.acceptOrder();
        myController.processOrder();
        myController.submitCOFtoOrders();
        myController.backToContract();
        myController.sendToOrders();
        myController.sendBackToOrders();
        myController.declineOrder();
        myController.theContract.Approval_Submission_Comments__c = 'test';
        myController.saveSubmitApproval();
        myController.contractEdit();
        myController.manualBuild();
        myController.gotoReview();
        myController.sendToOrdersErgo();
        myController.contractEditHumantech();
        mycontroller.MarkContractUnderReview();
        system.assertEquals('Under Review', myController.theContract.status__c);
     }

}