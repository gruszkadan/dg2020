public class proposalBuilderNeedsRequirements {
    
    public Id quoteId {get;set;}
    public Id u {get;set;}
    public Id delId {get;set;}
    public String editQPNId {get;set;}
    public String query {get;set;}
    public Quote__c quote {get;set;}
    public List<Quote_Proposal_Need__c> quoteProposalNeeds {get;set;}
    public List<product2> prods {get;set;}
    public String removedNeeds {get;set;}
    public String famSelection {get;set;}
    public String prodSelection {get;set;}
    public Quote__c quoteProds {get;set;}
    public String queryNeeds {get;set;}
    public needsWrapper[] needsWrappers {get;set;}
    public string orderedString {get;set;}
    public List<String> orderedList {get;set;}
    public List<Quote_Proposal_Need__c> queryWrappers {get;set;}
    public List<String> productNames {get;set;}
    
    
    
    public proposalBuilderNeedsRequirements(){
        
        u = UserInfo.getUserId();
        
        needsWrappers = new List<needsWrapper>();
        
        quoteId = ApexPages.currentPage().getParameters().get('id');
        
        quote = [SELECT Id, Name, Needs_JSON__c, (Select Product__r.Name, Product__r.Family FROM Quote_Products__r) FROM Quote__c WHERE id = :quoteId];        
        
        getProductOptions();
        
        //create list of the product names that are related to Quote to filter Available Needs
        productNames = new List<String>();
        
        for(Quote_Product__c qp: quoteProds.quote_Products__r){
            productNames.add(qp.Product__r.Name);
        }
        
        needsTable();
        
        if(quote.Needs_JSON__c != null){
            loadWrappers();
        }
        
        system.debug(quoteProposalNeeds);
        
    }
    
    public List<SelectOption> getFamilyOptions(){
        
        //start w Set so Familys aren't duplicated
        Set<SelectOption> famOptions = new Set<SelectOption>();  
        
        famOptions.add(new selectOption('', '- Select Product Family -'));
        
        //loop through quote products linked to quote and add family to selectList
        for(Quote_Product__c qp : quote.Quote_Products__r){
            
            famOptions.add(new selectOption(qp.Product__r.Family, qp.Product__r.Family));
        }
        
        
        //filter out duplicate families and sort
        List<SelectOption> optionList = new List<SelectOption>(famOptions);
        optionList.sort();
        
        return optionList;
        
        
    }
    
    
    public List<SelectOption> getProductOptions(){
        
        //dynamic query for products filtered by selected product family and quote
        query = 'SELECT Id, Name, (Select Product__r.Name, Product__r.Family FROM Quote_Products__r ';
        
        if(famSelection != null){
            query += 'WHERE Product__r.Family = \''+famSelection+'\'';
        }
        
        query += ') FROM Quote__c WHERE id = \''+quoteId+'\'';
        
        quoteProds = database.query(query);
        
        Set<SelectOption> productOptions = new Set<SelectOption>();  
        
        productOptions.add(new selectOption('', '- Select Product -'));
        
        for(Quote_Product__c qp : quoteProds.Quote_Products__r){
            
            productOptions.add(new selectOption(qp.Product__r.Name, qp.Product__r.Name));
        }
        
        List<SelectOption> productList = new List<SelectOption>(productOptions);
        productList.sort();
        return productList;
        
        
    }
    
    
    public void needsTable(){
        
        
        queryNeeds =  'SELECT Id, Need__c, Description__c, Global__c, Display_Need_For_All_Products__c, Need_Heading__c, CreatedBy.Id, (SELECT Product__r.Name, Product__r.Family FROM Product_Needs__r WHERE Product__r.Name IN:productNames'; 
        
        
        //if product Family is selected, add to query to filter out needs
        if(famSelection != null){
            queryNeeds +=  ' AND Product__r.Family = \''+famSelection+'\'';
            
            //if product is selected, add to query to filter out needs
            if(prodSelection != null && prodSelection != ''){
                queryNeeds +=  ' AND Product__r.Name = \''+prodSelection+'\'';
            }
        }
        
        if(prodSelection != null && famSelection == null){
            queryNeeds +=  ' AND Product__r.Name = \''+prodSelection+'\'';
        }
        
        queryNeeds += ') FROM Quote_Proposal_Need__c WHERE Available_For_Use__c = true AND (Global__c = true OR (Global__c = false AND CreatedBy.Id = :u))';
        
        quoteProposalNeeds = database.query(queryNeeds);
        
    }
    
    
    public void clearProductSelection(){
        
        //once prod family selected, clear product selection and requery
        prodSelection = null;
        needsTable();
        
        
    }
    
    public void deleteQPN() {
        
        Id delId = ApexPages.currentPage().getParameters().get('delQPN');
        
        for(Quote_Proposal_Need__c q : quoteProposalNeeds){
            if(q.Id == delId){
                delete q;  
            }  
        }
        
        //delete Product Needs related to QPN
        List<Product_Need__c> deleteProds = [SELECT Id FROM Product_Need__c WHERE Quote_Proposal_Need__c = :delId];
        delete deleteProds;
        
        
    }
    
    public class needsWrapper{
        
        public Quote_Proposal_Need__c need {get;set;}
        public Integer order {get;set;}
        
        public needsWrapper(Quote_Proposal_Need__c xNeed, Integer xOrder){
            need = xNeed;
            order = xOrder;
        }
        
        
    } 
    
    
    public void createNeedsWrapper(){
        
        Id need = ApexPages.currentPage().getParameters().get('need');
        
        //Add QPN to removed Needs List to Filter out of Available Needs
        //Create Wrapper with QPN ID
        for(Quote_Proposal_Need__c q : quoteProposalNeeds){
            if(q.id == need){
                
                if(removedNeeds == null){
                    removedNeeds = q.id; 
                } else{
                    removedNeeds += ' ' +q.id;
                }
                
                integer order = needsWrappers.size();
                needsWrappers.add(new needsWrapper(q, order));
                
            }
        }
        
        
    }        
    
    public void removeWrapper(){
        
        Integer order = Integer.valueOf(ApexPages.currentPage().getParameters().get('order'));
        String need = String.valueOf(ApexPages.currentPage().getParameters().get('need'));
        
        //remove wrapper and remove ID from removedNeeds so it displays in available needs section
        needsWrappers.remove(order);
        removedNeeds = removedNeeds.remove(need);
        
        //loop through list and reorder
        for(integer x = 0; x < needsWrappers.size(); x++){
            needsWrappers[x].order = x;
        }
        
    }
    
    public void modalQPNId(){
        
        editQPNId = ApexPages.currentPage().getParameters().get('editQPN');
        
    }
    
    
    public PageReference Next(){
        
        
        //create List of IDs from jquery String; Serialize into JSON
        orderedList = orderedString.split(','); 
        
        if(orderedString == '' || orderedString == null){
            
            quote.Needs_JSON__c = null;
        }else{
            
            quote.Needs_JSON__c = JSON.serialize(orderedList);
            
        }
        
        update quote;
        
        return new PageReference('/apex/proposal_builder_proposed_solution?id=' + QuoteId); 
    }
    
    
    public void loadWrappers(){
        
        
        //deserialize the JSON into list of QPN IDs
        List<String> orderedList = (List<String>)JSON.deserialize(quote.Needs_JSON__c, List<String>.class);
        
        //query QPNS that are in List of Ids
        queryWrappers = [SELECT Id, Name, Need__c, Description__c, Global__c, Need_Heading__c FROM Quote_Proposal_Need__c WHERE Id IN :orderedList AND Available_For_Use__c = true];
        
        
        //loop through list of Ids and QPNS called; if QPN.id = String of Id, build wrapper in order they appear in List
        //add QPN to removedNeeds List so it will not appear in available needs table
        for(String jsonId : orderedList){
            for(Quote_Proposal_Need__c q : queryWrappers){
                
                if(jsonId == q.id){
                    
                    integer order = needsWrappers.size();
                    needsWrappers.add(new needsWrapper(q, order));
                    removedNeeds += ' ' +q.id;
                    
                }
                
            }
            
        }
        
    }
    
    
}