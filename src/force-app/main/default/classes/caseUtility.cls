//Test Class: testCaseTrigger

public class caseUtility {
    
    public static void caseHoverIssues(Case[] cases) {
        Case_Issue__c[] cis = [SELECT Id, Product_In_Use__c, Product_Support_Issue_Locations__c, Product_Version__c, Product_Support_Issue__c, Associated_Case__c
                               FROM Case_Issue__c
                               WHERE Associated_Case__c IN :cases 
                               ORDER BY CreatedDate ASC];
        
        for (Case c : cases) {
            c.Hover_Info__c = '';
            for (Case_Issue__c ci : cis) {
                if (ci.Associated_Case__c == c.id) {
                    c.Hover_Info__c = c.Hover_Info__c+
                        'PRODUCT IN USE: '+ci.Product_In_Use__c;
                    if (ci.Product_Version__c != NULL) {
                        c.Hover_Info__c = c.Hover_Info__c+
                            ' (' + ci.Product_Version__c +  ');';
                    } else{
                        c.Hover_Info__c = c.Hover_Info__c+ ';' ; 
                    }                          
                    c.Hover_Info__c = c.Hover_Info__c +
                        'ISSUE LOCATION: '+ci.Product_Support_Issue_Locations__c+';'+
                        'SPECIFIC ISSUE: '+ci.Product_Support_Issue__c+';';
                    if (c.Client_Disposition__c != null) {
                        c.Hover_Info__c = c.Hover_Info__c+
                            'CLIENT DISPOSITION: '+c.Client_Disposition__c;
                    } else {
                        c.Hover_Info__c = c.Hover_Info__c+
                            'CLIENT DISPOSITION: None';
                    }
                }
            }
        }
    }
    
    public static void webToCaseEmailMessage(Case[] cases) {
        EmailMessage[] emailMessages = new list<EmailMessage>();
        for(Case c:cases){
            //Order of Description String - Subject||FirstName||LastName||Company||ZipCode||Phone||Email||Comments
            String[] caseInformation = c.Web_Form_Data__c.split(Pattern.quote('||'));
            EmailMessage emailMsg = new EmailMessage();
            emailMsg.ParentId = c.id;
            emailMsg.Incoming = true;
            emailMsg.Status = '0';
            emailMsg.ToAddress = 'customerhelp@ehs.com';
            emailMsg.Subject = caseInformation[0].substringAfter(': ');
            emailMsg.FromName = caseInformation[1].substringAfter(': ')+' '+caseInformation[2].substringAfter(': ');
            emailMsg.FromAddress = caseInformation[6].substringAfter(': ');
            emailMsg.HtmlBody =  caseInformation[0]+'<br/>'+
                caseInformation[1]+'<br/>'+
                caseInformation[2]+'<br/>'+
                caseInformation[3]+'<br/>'+
                caseInformation[4]+'<br/>'+
                caseInformation[5]+'<br/>'+
                caseInformation[6]+'<br/>'+
                caseInformation[7];
            emailMsg.TextBody = emailMsg.HtmlBody.stripHTMLTags();
            emailMessages.add(emailMsg);
        }
        if(emailMessages.size() > 0){
            try {
                insert emailMessages;
            } catch(exception e) {
                salesforceLog.createLog('Case', true, 'caseUtility', 'webToCaseEmailMessage', +string.valueOf(e));
            }
        }
    }
    
    
    public static void validate90UpgradeCases(Case[] cases){
        
        
        Id[] relatedAccountIds = new List<Id>();
        for(Case c : cases){
            //if(relatedAccountIds.contains(c.AccountId)){
            //   c.addError('Only one 9.0 Upgrade Training Case is permitted on an account. This update contains multiple 9.0 Upgrade Training Cases on ' + c.Account_Name__c);  
            // }else{
            relatedAccountIds.add(c.AccountId);
            //}
        } 
        
        Case[] Other90UpgradeTrainings = new List<Case>();
        Other90UpgradeTrainings = [Select AccountID from Case where AccountID IN :relatedAccountIds AND Type = '9.0 Upgrade Training'];  
        
        Id[] other90UpgradeCaseIds = new List<id>();
        for(case c: Other90UpgradeTrainings ){
            other90UpgradeCaseIds.add(c.AccountId);
        }        
        
        VPM_Project__c[] relatedOpenProjects = new List<VPM_Project__c>();
        relatedOpenProjects = [Select Name, Account__c from VPM_Project__c where Account__c IN :relatedAccountIds AND Project_Status__c = 'Open' and Type__c = 'Chemical Management Upgrade'];
        System.debug( relatedOpenProjects.size() + ' relatedOpenProjects: ' +  relatedOpenProjects); 
        
        
        Id[] accountIDsWithProjects = new List<id>();
        for(VPM_Project__c pm: relatedOpenProjects){
            accountIDsWithProjects.add(pm.Account__c);
        }
        System.debug( accountIDsWithProjects.size() + ' accountIDsWithProjects: ' +  accountIDsWithProjects);       
        
        for (Case c: cases) {
            if(other90UpgradeCaseIds.size() > 0){
                if (other90UpgradeCaseIds.contains(c.AccountId)){
                    //adding error will prevent insert
                    c.addError('Only one 9.0 Upgrade Training Case is permitted on an account'); 
                }
            }
            
            //check for project
            system.debug(c.AccountId);
            if(!accountIDsWithProjects.contains(c.AccountId)){
                c.addError('Account associated with this case does not contain an open Chemical Management Upgrade Project');
            }  
        } 
    }
    
    public static void DFUStatus(Case[] cases){
        
        map<string, Case> ObjMap = new map<string, Case>();
        //looping through the cases and adding accountId and case to map 
        for(Case c: cases )
        {
                ObjMap.put(c.AccountID, c);
        }
        
        //querying required fields from map key set accounts
        List<Account> accounts = [SELECT Id, DFU_Status__c, DFU_Case_ID__c 
                                  FROM Account WHERE ID IN :ObjMap.KeySet()];
        List<Account> accountUpdateList = new List<Account>();
        
        for(Account a: accounts)
        {
            //passing in the accountId to the map, to get the associated case and setting it to c
            Case c = ObjMap.get(a.ID);
            if(c.Status =='Closed'){
                a.DFU_Status__c = c.DFU_Status__c;
            } else {
                a.DFU_Status__c = c.Status;
            }
            accountUpdateList.add(a);
        }
        
        if(accountUpdateList.size() > 0)
        {
            update accountUpdateList;
        }
    }
    
    public static void updateProjectStatusTimeStamps(Case[] cases) {
        //Case[] casesToUpdate = new List<Case>();
        for(Case c:cases){
            switch on c.Project_Status__c {
                when 'Action Required - Consultation' {
                    c.Action_Required_Consultation_Date__c = System.today();
                }
                when 'Action Required - Measurements' {
                    c.Action_Required_Measurements_Date__c = System.today();
                } 
                when 'Cancelled - No Response' {
                    c.Cancelled_No_Response_Date__c = System.today();
                } 
                when 'Cancelled - Per Request' {
                    c.Cancelled_Per_Request_Date__c = System.today();
                } 
                when 'CF - Account Review' {
                    c.CF_Account_Review_Date__c = System.today();
                } 
                when 'CF - Assigned' {
                    c.CF_Assigned_Date__c = System.today();
                }
                when 'CF - Confirm Onsite Date' {
                    c.CF_Confirm_Onsite_Date__c = System.today();
                }  
                when 'CF - Confirm Receipt' {
                    c.CF_Confirm_Receipt_Date__c = System.today();
                } 
                when 'CF - List Sent to Client' {
                    c.CF_List_sent_to_Client_Date__c = System.today();
                } 
                when 'CF - Order Shipped' {
                    c.CF_Order_Shipped_Date__c = System.today();
                } 
                when 'CF - Order to Vendor' {
                    c.CF_Order_to_Vendor_Date__c = System.today();
                } 
                when 'CF - Pending Delivery' {
                    c.CF_Pending_Delivery_Date__c = System.today();
                } 
                when 'CF - Pending Initial CS Service' {
                    c.CF_Pending_Initial_CS_Service_Date__c = System.today();
                }                
                when 'CF - Pending Onsite' {
                    c.CF_Pending_Onsite_Date__c = System.today();
                }
                when 'CF - Project Deferred' {
                    c.CF_Project_Deferred_Date__c = System.today();
                }
                when 'CF - Scheduling Initial Call' {
                    c.CF_Scheduling_Initial_Call_Date__c = System.today();
                }
                when 'CF - Waiting for Invoice' {
                    c.CF_Waiting_for_Invoice_Date__c = System.today();
                }
                when 'CF - Waiting for Onsite' {
                    c.CF_Waiting_for_Onsite_Date__c = System.today();
                }
                when 'Client - Delivered' {
                    c.Client_Delivered_Date__c = System.today();
                } 
                when 'Client - Overage Review' {
                    c.Client_Overage_Review_Date__c = System.today();
                } 
                when 'Client - Project Review' {
                    c.Client_Project_Review_Date__c = System.today();
                } 
                when 'Client - Submission Pending' {
                    c.Client_Submission_Pending_Date__c = System.today();
                } 
                when 'In Progress - Ergonomist' {
                    c.In_Progress_Ergonomist_Date__c = System.today();
                } 
                when 'No Show' {
                    c.No_Show_Date__c = System.today();
                } 
                when 'On Leave' {
                    c.On_Leave_Date__c = System.today();
                } 
                when 'Ops - Account Prep' {
                    c.Ops_Account_Prep_Date__c = System.today();
                }
                when 'Ops - List Submitted' {
                    c.Ops_List_Submitted_Date__c = System.today();
                } 
                when 'Ops - Obtainment w Indexing' {
                    c.Ops_Obtainment_w_Indexing_Date__c = System.today();
                } 
                when 'Ops - Post Migration' {
                    c.Ops_Post_Migration_Date__c = System.today();
                } 
                when 'Ops - Post Vendor' {
                    c.Ops_Post_Vendor_Date__c = System.today();
                } 
                when 'Ops - Special Indexing' {
                    c.Ops_Special_Indexing_Date__c = System.today();
                } 
                when 'Ops - Standard Indexing' {
                    c.Ops_Standard_Indexing_Date__c = System.today();
                } 
                when 'Ops - Vendor Prep' {
                    c.Ops_Vender_Prep_Date__c = System.today();
                }
                when 'Ops - Waiting on Dev' {
                    c.Ops_Waiting_on_Dev_Date__c = System.today();
                } 
                when 'Pending Consultation' {
                    c.Pending_Consultation_Date__c = System.today();
                }
                when 'See Case Notes - Escalation' {
                    c.See_Case_Notes_Escalation_Date__c = System.today();
                }
                when 'Vendor - In Progress' {
                    c.Vendor_In_Progress_Date__c = System.today();
                }
                when 'Vendor - Onsite in Progress' {
                    c.Vendor_Onsite_in_Progress_Date__c = System.today();
                }
                when 'Cancelled' {
                    c.Cancelled_Date__c = System.today();
                }
                when 'CF - Waiting on Next Event' {
                    c.CF_Waiting_on_Next_Event_Date__c = System.today();
                }
            }
        }      
    }
    
    
    
    
}