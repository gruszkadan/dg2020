public class accountUtility {
    public static void sendToES(Account[] accounts) {
        Account[] accountsToUpdate = new list<Account>();
        Account[] accountsToNotify = new list<Account>();
        
        accounts = [SELECT id, OwnerId, ParentId, Parent.OwnerId, Parent.Enterprise_Sales__c, Parent.ParentId, Parent.Parent.OwnerId, Parent.Parent.Enterprise_Sales__c,
                    Parent.Parent.ParentId, Parent.Parent.Parent.OwnerId, Parent.Parent.Parent.Enterprise_Sales__c, Parent.Parent.Parent.ParentId,
                    Parent.Parent.Parent.Parent.OwnerId, Parent.Parent.Parent.Parent.Enterprise_Sales__c, Parent.Parent.Parent.Parent.ParentId,
                    Parent.Parent.Parent.Parent.Parent.OwnerId, Parent.Parent.Parent.Parent.Parent.Enterprise_Sales__c, Parent.Parent.Parent.Parent.Parent.ParentId,
                    (SELECT id FROM Opportunities WHERE Suite_of_Interest__c = 'MSDS Management' AND Owner_Group__c = 'Mid-Market' AND isClosed = FALSE)
                    FROM Account where id in: accounts];
        for(Account a:accounts){
            if(a.Opportunities.size() == 0){
                if(a.Parent.Parent.Parent.Parent.Parent.ParentId != null){
                    Account tla = new Account();
                    tla = [SELECT id, Name, Enterprise_Sales__c, OwnerId
                           FROM Account
                           WHERE id = :a.Parent.Parent.Parent.Parent.Parent.ParentId
                           LIMIT 1];
                    if(tla.Enterprise_Sales__c == true){
                        if(a.OwnerId != tla.OwnerId){
                            a.OwnerId = tla.OwnerId;
                            accountsToNotify.add(a);
                        }
                        a.Enterprise_Sales__c = true;
                        accountsToUpdate.add(a);
                    }
                }else if(a.Parent.Parent.Parent.Parent.ParentId != null && a.Parent.Parent.Parent.Parent.Parent.Enterprise_Sales__c == true){
                    if(a.OwnerId != a.Parent.Parent.Parent.Parent.Parent.OwnerId){
                        a.OwnerId = a.Parent.Parent.Parent.Parent.Parent.OwnerId;
                        accountsToNotify.add(a);
                    }
                    a.Enterprise_Sales__c = true;
                    accountsToUpdate.add(a);
                }else if(a.Parent.Parent.Parent.ParentId != null && a.Parent.Parent.Parent.Parent.Enterprise_Sales__c == true){
                    if(a.OwnerId != a.Parent.Parent.Parent.Parent.OwnerId){
                        a.OwnerId = a.Parent.Parent.Parent.Parent.OwnerId;
                        accountsToNotify.add(a);
                    }
                    a.Enterprise_Sales__c = true;
                    accountsToUpdate.add(a);
                }else if(a.Parent.Parent.ParentId != null && a.Parent.Parent.Parent.Enterprise_Sales__c == true){
                    if(a.OwnerId != a.Parent.Parent.Parent.OwnerId){
                        a.OwnerId = a.Parent.Parent.Parent.OwnerId;    
                        accountsToNotify.add(a);
                    }
                    a.Enterprise_Sales__c = true;
                    accountsToUpdate.add(a);
                }else if(a.Parent.ParentId != null && a.Parent.Parent.Enterprise_Sales__c == true){
                    if(a.OwnerId != a.Parent.Parent.OwnerId){
                        a.OwnerId = a.Parent.Parent.OwnerId;
                        accountsToNotify.add(a);
                    }
                    a.Enterprise_Sales__c = true;
                    accountsToUpdate.add(a);
                }else if(a.ParentId != null && a.Parent.Enterprise_Sales__c == true){
                    if(a.OwnerId != a.Parent.OwnerId){
                        a.OwnerId = a.Parent.OwnerId;
                        accountsToNotify.add(a);
                    }
                    a.Enterprise_Sales__c = true;
                    accountsToUpdate.add(a);
                }
            }
        }
        if(accountsToUpdate.size() > 0){
            try {
                update accountsToUpdate;
                if(accountsToNotify.size() > 0){
                    ownershipChangeNotifications.createOCN(accountsToNotify);
                }
            } catch(exception e) {
                salesforceLog.createLog('Account', true, 'accountUtility', 'sendToES', string.valueOf(e));
            }
        }
    }
    
    public static void clearCustomerInfo(Account[] accounts) {
        for (Account a:accounts){
            a.Subscription_End_Date__c = null;
            a.Customer_Annual_Revenue_EHS__c = null;
            a.Customer_Annual_Revenue_Ergo__c = null;
            a.Customer_Annual_Revenue__c = null;
            a.EHS_Subscription_Model__c = null;
        }
    }
    
    public static void updateOwnerEmailsOnContact(Account[] accounts){
        Contact[] contactsToUpdate = new list<Contact>();
        contactsToUpdate = [Select ID, AccountID from Contact where AccountID in: accounts];
        if(contactsToUpdate.size() >0){
            contactUtility.reassignContact(contactsToUpdate, true);  
        }
    }
    
    //after insert or after update of Ultimate parent- sends in children (accounts with parent accounts), sets SAM to be Ulimate parent's SAM
    public static void updateChildSams(Account[] accounts){
        
        //requery accounts passed in to get values or related fields
        Account[] childAccounts = [Select ID, Ultimate_Parent__r.Platform_Sales_Rep__c from Account where ID in: accounts];   
        
        for(Account a:childAccounts){
            
            a.Platform_Sales_Rep__c = a.Ultimate_Parent__r.Platform_Sales_Rep__c;
        }
        try{
            update childAccounts;
        }catch(exception e) {
            salesforceLog.createLog('Account', true, 'accountUtility', 'updateChildSams', string.valueOf(e));
        }
    }
    
    public static void sendARREmail(Account[] accounts){
        
       //query email of Users with ARR notification setting checked
        MSDS_Custom_Settings__c[] peopleToEmail = [Select SetupOwner.Email FROM MSDS_Custom_Settings__c WHERE ARR_20k_Notification__c = true];
        
        
        //create list of Email Addresses to receive email notification
        list<String> toAddresses = new List<String>();
        
        for(MSDS_Custom_Settings__c m : peopleToEmail){
            
            toAddresses.add(m.SetupOwner.Email);
            
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        mail.setToAddresses(toAddresses);
        mail.setUseSignature(false);
        mail.setSubject('Account ARR Changes');
        mail.sethtmlbody(createARREmailBody(accounts));        
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});            
        
    }
    
    public static string createARREmailBody(Account[] accounts){
        
        //create separate lists for above or below 20,000
        list<Account> arrBelow20k = new List<Account>();
        list<Account> arrAbove20k = new List<Account>();
        
        for(Account a: accounts){
            if(a.Customer_Annual_Revenue__c >= 20000){
                arrAbove20k.add(a);
            }
           else if(a.Customer_Annual_Revenue__c < 20000){
                arrBelow20k.add(a); 
            }
            
        }
        
        string htmlBody = 'The Chemical Management ARR of the following Account(s) has been changed to greater/less than $20,000. Details for the Account(s) can be found below.</br></br>';
        
        
        //create a table for any account that has their ARR changed to greater than 20,000. Populate columns with Account Name/Link and ARR
        if(arrAbove20k.size() > 0){
            htmlBody += '<u>ARR greater than or equal to 20,000</u>:</br></br>'+ 
                '<table style="font-size: 75%;width: 100%;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;border-collapse: collapse;border-color: grey;border: 1px solid #dddbda;">';
            
            htmlBody += '<tr style="font-size: 0.75rem;line-height: 1.25;color: #706e6b;text-transform: uppercase;letter-spacing: 0.0625rem;">'+
                '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Name</th>'+
                '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">ARR</th>'+
                '</tr>';
            for (Account a: arrAbove20k){
                htmlBody +='<tr>'+
                    '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+'<a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/'+a.id+'>'+a.Name+'</a></td>'+
                    '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;"> $'+a.Customer_Annual_Revenue__c+'</td>'+
                    '</tr>';
            }
            htmlBody += '</table></br></br>';
        }
        
        //create a table for any account that has their ARR changed to less than 20,000. Populate columns with Account Name/Link and ARR
        if(arrBelow20k.size() > 0){
            htmlBody += '<u>ARR less than 20,000</u>:</br></br>'+ 
                '<table style="font-size: 75%;width: 100%;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;border-collapse: collapse;border-color: grey;border: 1px solid #dddbda;">';
            htmlBody += '<tr style="font-size: 0.75rem;line-height: 1.25;color: #706e6b;text-transform: uppercase;letter-spacing: 0.0625rem;">'+
                '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Name</th>'+
                '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">ARR</th>'+
                '</tr>';
            for (Account a: arrBelow20k){
                htmlBody +='<tr>'+
                    '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+'<a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/'+a.id+'>'+a.Name+'</a></td>'+
                    '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;"> $'+a.Customer_Annual_Revenue__c+'</td>'+
                    '</tr>';
            }
            htmlBody += '</table>';
        }     
        
        return htmlBody;
    }
    
    //before update if SAM of ultimate parent changes, update children SAMs - sends in ultimate parent 
    public static void findAndUpdateChildSams(Account[] accounts){
        
        Account[] childAccounts = [Select ID, Ultimate_Parent__r.Platform_Sales_Rep__c from Account where Ultimate_Parent__c in: accounts];
        if(childAccounts.size() > 0){
            for (Account a: childaccounts){
                a.Platform_Sales_Rep__c = a.Ultimate_Parent__r.Platform_Sales_Rep__c;
            }
        }
        try{
            update childAccounts;
        }catch(exception e) {
            salesforceLog.createLog('Account', true, 'accountUtility', 'findAndUpdateChildSams', string.valueOf(e));
        }   
    }
    
    
    
    
    
    
}