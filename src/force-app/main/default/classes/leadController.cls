public with sharing class leadController {
    Decimal count = 0;
    public Lead lead;
    public User u {get;set;}
    public EmailTemplate leadOwnerChange {get;set;}
    private Apexpages.StandardController controller;
    private msdsCustomSettings helper = new msdsCustomSettings();
    public Product_of_Interest__c[] poiList {get;set;}
    public Product_of_Interest__c poi {get;set;}
    public boolean isSandbox {get;set;}
    public boolean showSalesInsight {get;set;}
    public boolean displayPopup {get; set;}
    public boolean showDataDotCom {get;set;}
    public String ownerToChange {get;set;}
    private leadCustomSettings leadhelper = new leadCustomSettings();
    public boolean editChemicalMgtOwner {get;set;}
    public boolean editAuthoringOwner {get;set;}
    public boolean editErgonomicsOwner {get;set;}
    public boolean editOnlineTrainingOwner {get;set;}
    public boolean editEHSOwner {get;set;}
    public boolean editSecondaryLeadOwner {get;set;}
    public boolean viewPOIDetail {get;set;}
    public boolean editErgonomicsSecondaryLeadOwner {get;set;}
    Id leadId {get;set;}
    
    
    public leadController(ApexPages.StandardController stdController) {
        this.controller = stdController;
        // Get information about the Opportunity being worked on
        leadId = ApexPages.currentPage().getParameters().get('id');
        if ( leadId != null ) {
            this.lead = [SELECT Id, OwnerID, Chemical_Management_Owner__c, EHS_Owner__c, Authoring_Owner__c, Online_Training_Owner__c, Ergonomics_Owner__c, Secondary_Lead_Owner__c, Recycle_Count__c, Territory__c  FROM Lead WHERE Id = :leadId]; 
        }
        u = [Select id, Name, FirstName, Email, ManagerID, Sales_Team__c, Department__c, Group__c, department_name__c, Profile.Name from User where id =: UserInfo.getUserId() LIMIT 1];   
        if(this.Lead.Recycle_Count__c != null)
            count = this.lead.Recycle_Count__c;
        leadOwnerChange = [Select Id From EmailTemplate where Name='Lead Assignment'];
        isSandbox = URL.getSalesforceBaseUrl().getHost().left(4).equalsignorecase('c.cs');
        showDataDotCom = helper.viewDataDotCom();
        showSalesInsight = helper.showSalesInsight();
        editChemicalMgtOwner = leadhelper.canChangeChemicalMgtOwner();
        editAuthoringOwner = leadhelper.canChangeAuthoringOwner();
        editErgonomicsOwner = leadhelper.canChangeErgonomicsOwner();
        editOnlineTrainingOwner = leadhelper.canChangeOnlineTrainingOwner();
        editEHSOwner = leadhelper.canChangeEHSOwner();
        editSecondaryLeadOwner = leadhelper.canChangeSecondaryLeadOwner();
        viewPOIDetail = POI_Security__c.getInstance().View_POI_Detail__c;
        editErgonomicsSecondaryLeadOwner = leadhelper.CanEditErgonomicsSecondaryLeadOwner();
        
        
        
        
        poiList = new list <Product_of_Interest__c>();
        queryPOI();
        if(poiList.size() > 0){
            poi = poiList[0];
        }
    }
    
    public boolean getviewPOI() {
        boolean viewPOI = [SELECT View_POI__c FROM User WHERE id = : UserInfo.getUserId() LIMIT 1].View_POI__c;
        return viewPOI;
    }
    
    public void transferToES (){
        try {
            Lead[] rrLeads = new list <lead>();
            rrLeads.add(this.Lead);
            roundRobinAssignment.roundRobinOwnershipAssignment(rrLeads,'Lead','Enterprise');
            this.lead.Enterprise_Sales_Lead__c = true;
            
            //query default users
            leadCustomSettings lcd = new leadCustomSettings();
            leadCustomSettings.defaultOwners def = lcd.findDefaultOwners();
            
            //if the lead's EHS owner is not the special countries owner, change to default enterprise owner
            if (this.lead.EHS_Owner__c != def.specialEHSOwner) {
                this.lead.EHS_Owner__c = def.entEHSOwner;
            }
            update this.Lead;
        }
        
        catch(Exception e){
            ApexPages.addMessages(e);
        }
    }
    
    public List<SelectOption> getStatus() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Open','Open'));
        options.add(new SelectOption('Dead - Duplicate Lead','Dead - Duplicate Lead'));
        options.add(new SelectOption('Dead - Current Customer','Dead - Current Customer'));
        options.add(new SelectOption('Dead - Not a Target','Dead - Not a Target'));
        options.add(new SelectOption('Dead - No Actionable Info','Dead - No Actionable Info'));
        options.add(new SelectOption('Dead - Out of Business','Dead - Out of Business'));
        
        return options;
    }
    
    public void incrementCounter() {
        count++;
        try
        {
            this.lead = [Select id from Lead where Id=:this.lead.Id];
            this.lead.Recycle_Count__c = count;
            update this.lead;
        }
        catch(exception e)
        {
            ApexPages.addMessages(e);
        }
    }
    
    public Decimal getCount() {
        return count;
    }
    
    public PageReference sendToES() {
        transferToES();
        pageReference lead_detail = Page.lead_detail;
        lead_detail.setRedirect(true);
        lead_detail.getParameters().put('id',this.lead.ID); 
        return lead_detail;    
    }
    
    public PageReference sendToMM(){
        lead[] leadsToNotify = new List<lead>();
        this.lead.Enterprise_Sales_Lead__c = false;
        if(this.lead.Territory__c != NULL){
            Id territoryID = this.lead.Territory__c;
            Territory__c territory = [Select Territory_Owner__c from Territory__c where Id = :territoryID LIMIT 1];
            this.lead.OwnerId = territory.Territory_Owner__c;
        }else{
            this.lead.OwnerId = ownershipUtility.findDefaultChemManagementOwner();
        }
        
        try{
            update this.lead;
            leadsToNotify.add(this.lead);
            ownershipChangeNotifications.createLeadOCN(leadsToNotify);            
        }
        catch(exception e){
            ApexPages.addMessages(e);
        }
        
        pageReference lead_detail = Page.lead_detail;
        lead_detail.setRedirect(true);
        lead_detail.getParameters().put('id',this.lead.ID); 
        return lead_detail;
    }
    
    public PageReference fullDetails() {
        PageReference lead_full_detail = Page.lead_full_detail;
        lead_full_detail.setRedirect(true);
        lead_full_detail.getParameters().put('id',Lead.id); 
        return lead_full_detail; 
    }
    
    public void closePopup() {
        displayPopup = false;
    }
    
    public void showPopup() {
        displayPopup = true;
    }
    
    public void showOwnerToChange(){
        ownerToChange = ApexPages.currentPage().getParameters().get('ownerToChange');
    }
    
    public PageReference ownershipChangeWithNotification(){
        this.controller.save();
        Lead ownerChange = [SELECT id, OwnerID, Chemical_Management_Owner__c, EHS_Owner__c, Authoring_Owner__c, Online_Training_Owner__c, Ergonomics_Owner__c, Secondary_Lead_Owner__c, Ergonomics_Secondary_Lead_Owner__c
                            FROM Lead
                            WHERE id = :leadId
                            LIMIT 1];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setHtmlBody('Lead has been assigned to you. <br/><br/>'+
                              'Please click on the link below to view the record.<br/><br/>'+
                              'Link to Lead: <a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+this.lead.id+'">Click Here</a>');
        mail.setSubject('Lead transferred to you');
        if(ownerToChange =='cm'){
            mail.setTargetObjectId(ownerChange.OwnerId);
        }
        if(ownerToChange =='ot'){
            mail.setTargetObjectId(ownerChange.Online_Training_Owner__c);
        }
        if(ownerToChange =='auth'){
            mail.setTargetObjectId(ownerChange.Authoring_Owner__c);
        }
        if(ownerToChange =='ehs'){
            mail.setTargetObjectId(ownerChange.EHS_Owner__c);
        }
        if(ownerToChange =='ergo'){
            mail.setTargetObjectId(ownerChange.Ergonomics_Owner__c);
        }        
        if(ownerToChange =='slo'){
            mail.setTargetObjectId(ownerChange.Secondary_Lead_Owner__c);
        }
        if(ownerToChange =='ESLO'){
            mail.setTargetObjectId(ownerChange.Ergonomics_Secondary_Lead_Owner__c);
        }
        
        
        
        if(Test.isRunningTest())
        {
            mail.setTargetObjectId([SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id);
        }
        mail.setSaveAsActivity(FALSE);
        mail.setUseSignature(FALSE);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        PageReference lead_detail = Page.lead_detail;
        lead_detail.setRedirect(true);
        lead_detail.getParameters().put('id',Lead.id); 
        return lead_detail;
    }
    
    public void queryPOI(){
        String SobjectApiName = 'Product_of_Interest__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String poiQuery = 'select ' + commaSepratedFields + ' from ' + SobjectApiName + ' where Lead__c = :LeadId LIMIT 1';
        
        
        poiList = Database.query(poiQuery);
    }
    
    //redirects older pages to the lead_detail page
    public PageReference redirectToNewPage() { 
        PageReference leadDetail = new PageReference('/' + lead.ID);
        leadDetail.setRedirect(true);
        return leadDetail;
    }
  
    
}