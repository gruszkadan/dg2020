trigger contentVersionTrigger on ContentVersion (after insert) {
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            contentVersionTriggerHandler.afterInsert(trigger.old,trigger.new);
        }
    }
}