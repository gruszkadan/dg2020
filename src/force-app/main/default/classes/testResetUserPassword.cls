@isTest(SeeAllData=true)
private class testResetUserPassword {
    
    public static testMethod void userPasswordReset(){
        User theUser = [SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1];
        resetUserPassword myController = new resetUserPassword();
        
        myController.describe();
        Process.PluginRequest request = new Process.PluginRequest( new Map<String, Object>{
            'userid' => theUser.id});
        process.PluginResult result = myController.invoke( request );
    }   
}