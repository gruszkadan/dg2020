public with sharing class contractNoteController {
    private Apexpages.StandardController controller;
    public Contract__c theContract {get;set;} 
    public Contract_Note__c note;
    public User u {get;set;}
    public QueueSobject q {get;set;}
    public EmailTemplate noteAlert {get;set;}
    public EmailTemplate contractDecline {get;set;}
    public EmailTemplate contractRepUpdate {get;set;}
    public boolean repUpdate {get;set;}
    private contractCustomSettings contract_helper = new contractCustomSettings();
    public boolean viewOrdersButtons {get;set;}
    public string environment {get;set;}
    
    public contractNoteController(ApexPages.StandardController stdController) {
        this.note = (Contract_Note__c)stdController.getRecord();
        viewOrdersButtons = contract_helper.viewOrdersButtons();
        Id theContractID= System.currentPageReference().getParameters().get('CF00N800000057HgX_lkid');
        String repUpdateParam = System.currentPageReference().getParameters().get('repUpdate'); 
        theContract = [Select ID, Name,Contract_Type__c, CreatedByID, OwnerID, Order_Submitted_By__c, Order_Submitted_By__r.Sales_Manager__c, Order_Submitted_By__r.Manager.Name,
                       Status__c from Contract__c where ID =:theContractID];
        u = [Select id, Name, FirstName, Email from User where id =: UserInfo.getUserId() LIMIT 1];
        noteAlert = [Select Id From EmailTemplate where Name='Contract Note Alert'];
        contractDecline = [Select Id From EmailTemplate where Name='Contract Orders Rejection'];
        contractRepUpdate = [Select Id From EmailTemplate where Name='Contract Rep Update'];
        if(repUpdateParam == '1'){
            repUpdate = true;
        }
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D300000001HEfEAM'){
            environment = 'Production';
        } else {
            environment ='Sandbox';
        }
    }
    
    public void sendAlertEmailRenewal() {
        string[] ccAddresses = new List<string>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // if the current user is the contract owner
        if (u.Id == theContract.OwnerID) {
            // if there is a submitter
            if (theContract.Order_Submitted_By__c != null) {
                // the owner will be sending the note to the submitter
                mail.setTargetObjectId(this.theContract.Order_Submitted_By__c);
            } else {
                // if there is no submitter, send the note to the contract creator
                mail.setTargetObjectId(this.theContract.CreatedByID); 
            } 
        }
        
        // the current user is the submitter
        if (u.id == theContract.Order_Submitted_By__c) {
            // check to see if the contract owner is a user
            if (string.valueOf(theContract.OwnerId.getSObjectType()) == 'User') {
                // send the note to the contract owner
                mail.setTargetObjectId(this.theContract.OwnerID);
            } else {
                // if the owner is a queue, query the default recipient and others in orders to be added as cc's
                MSDS_Custom_Settings__c[] targetRecipient = [SELECT SetupOwnerId 
                                                             FROM MSDS_Custom_Settings__c 
                                                             WHERE Default_Contract_Note_Recipient__c = true LIMIT 1];
                User[] ccRecipients = [SELECT id, Email
                                       FROM User
                                       WHERE Profile.Name = 'CC - Order Operations'
                                       AND isActive = true
                                       ORDER BY CreatedDate ASC];
                // add the cc recipients
                for (User recipient : ccRecipients) {
                    ccAddresses.add(recipient.Email);
                }
                if(environment == 'Sandbox'){
                    ccAddresses.add('dgruszka@ehs.com');
                }else{
                    ccAddresses.add('orders@ehs.com');
                }
                mail.setCcAddresses(ccAddresses);
                // set the target to the target recipient
                if (targetRecipient.size() > 0) {
                    mail.setTargetObjectId(targetRecipient[0].SetupOwnerId);
                } else {
                    // if there is no target recipient (no custom setting has been set), set the first user in the above list as the target
                    mail.setTargetObjectId(ccRecipients[0].id);
                }
            }
        }
        
        // if the current user isnt the contract owner or the submitter
        if (u.ID != theContract.OwnerId && u.ID != theContract.Order_Submitted_By__c) {
            // send the note to the contract owner
            mail.setTargetObjectId(this.theContract.OwnerID);	
        }
        
        if (environment == 'Sandbox' ) {
            ccAddresses.add('rwerner@ehs.com');
            mail.setCcAddresses(ccAddresses);
        }
        mail.setWhatId(this.note.Id);
        mail.setTemplateId(noteAlert.Id);
        mail.setSaveAsActivity(FALSE);
        mail.setUseSignature(FALSE);
        if (viewOrdersButtons == true) {
            if (environment == 'Sandbox') {
                mail.setReplyTo('test@ehs.com');
            } else {
                mail.setReplyTo('orders@ehs.com');
            }
        } else {
            mail.setReplyTo(u.Email);
        }
        mail.setSenderDisplayName('noreply@ehs.com');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    }
    
    public void sendAlertEmailNew(){
        string[] ccAddresses = new List<string>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // if the current user is the contract owner 
        if (u.Id == theContract.OwnerId) {
            // if there is a submitter
            if (theContract.Order_Submitted_By__c != null) {
                // send the note to the submitter
                mail.setTargetObjectId(this.theContract.Order_Submitted_By__c);
                // add the manager to cc
                ccAddresses.add([SELECT Email FROM User WHERE Name = :theContract.Order_Submitted_By__r.Manager.Name LIMIT 1].email);
            } else {
                // if there is no submitter, send to the contract creator
                mail.setTargetObjectId(this.theContract.CreatedByID);
            } 
        }
        
        // the current user is the submitter
        if (u.id == theContract.Order_Submitted_By__c) {
            // check to see if the contract owner is a user
            if (string.valueOf(theContract.OwnerId.getSObjectType()) == 'User') {
                // send the note to the contract owner
                mail.setTargetObjectId(this.theContract.OwnerID);
            } else {
                // if the owner is a queue, query the default recipient and others in orders to be added as cc's
                MSDS_Custom_Settings__c[] targetRecipient = [SELECT SetupOwnerId 
                                                             FROM MSDS_Custom_Settings__c 
                                                             WHERE Default_Contract_Note_Recipient__c = true LIMIT 1];
                User[] ccRecipients = [SELECT id, Email
                                       FROM User
                                       WHERE Profile.Name = 'CC - Order Operations'
                                       AND isActive = true
                                       ORDER BY CreatedDate ASC];
                // add the cc recipients
                for (User recipient : ccRecipients) {
                    ccAddresses.add(recipient.Email);
                }
                if(environment == 'Sandbox'){
                    ccAddresses.add('dgruszka@ehs.com');
                }else{
                    ccAddresses.add('orders@ehs.com');
                }
                mail.setCcAddresses(ccAddresses);
                // set the target to the target recipient
                if (targetRecipient.size() > 0) {
                    mail.setTargetObjectId(targetRecipient[0].SetupOwnerId);
                } else {
                    // if there is no target recipient (no custom setting has been set), set the first user in the above list as the target
                    mail.setTargetObjectId(ccRecipients[0].id);
                }
            }
        }
        
        if(u.ID <> theContract.OwnerId && u.ID <> theContract.Order_Submitted_By__c){
            // mail.setTargetObjectId(u.id);	
            mail.setTargetObjectId(this.theContract.OwnerID);	
        }
        
        if(environment == 'Sandbox' ){
            ccAddresses.add('rwerner@ehs.com');
        }
        if(ccAddresses != NULL){
            mail.setCcAddresses(ccAddresses);
        }
        mail.setWhatId(this.note.Id);
        mail.setTemplateId(noteAlert.Id);
        mail.setSaveAsActivity(FALSE);
        mail.setUseSignature(FALSE);
        if (viewOrdersButtons == true) {
            if(environment == 'Sandbox') {
                mail.setReplyTo('test@ehs.com');
            } else {
                mail.setReplyTo('orders@ehs.com');
            }
        } else {
            mail.setReplyTo(u.Email);
        }
        mail.setSenderDisplayName('noreply@ehs.com');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public void sendDeclineEmail(){
        String[] ccAddresses = new list<String>();
        if(theContract.Contract_Type__c == 'New') {
            ccAddresses.add([SELECT Email FROM User WHERE Name = :theContract.Order_Submitted_By__r.Manager.Name LIMIT 1].email); 
        }       
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(this.theContract.Order_Submitted_By__c);
        mail.setWhatId(this.note.Id);
        if(ccAddresses != NULL){
            mail.setCcAddresses(ccAddresses);
        }
        mail.setTemplateId(contractDecline.Id);
        mail.setSaveAsActivity(FALSE);
        mail.setUseSignature(FALSE);
        mail.setReplyTo(u.Email);
        mail.setSenderDisplayName(u.Name);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public void sendRepUpdateEmail(){
        String[] ccAddresses = new List<String>();
        if(theContract.Contract_Type__c == 'New') {
            User manager = [ SELECT Id, Manager.Id FROM User WHERE Id = :theContract.Order_Submitted_By__c LIMIT 1 ];
            ccAddresses.add( [ SELECT Email FROM User WHERE ID = :manager.ID LIMIT 1 ].email );
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(this.theContract.OwnerID);
        mail.setWhatId(this.note.Id);
        if(ccAddresses != NULL){
            mail.setCcAddresses(ccAddresses);
        }
        mail.setTemplateId(contractRepUpdate.Id);
        mail.setSaveAsActivity(FALSE);
        mail.setUseSignature(FALSE);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public void changeContractStatus () {
        if (theContract.Id != null ) {
            try {
                Contract__c theContract = [select Id, OwnerID, Status__c from Contract__c where Id=:theContract.Id];
                theContract.Status__c = 'Pending Rep Update';
                update theContract;
            }
            catch(Exception e){
                ApexPages.addMessages(e);
            }
        } 
    }
    
    public void pendingOrdersContractStatus () {
        if (theContract.Id != null ) {
            try {
                Contract__c theContract = [select Id, OwnerID, Status__c from Contract__c where Id=:theContract.Id];
                theContract.Status__c = 'Pending Orders';
                update theContract;
            }
            catch(Exception e){
                ApexPages.addMessages(e);
            }
        } 
    }  
    
    public PageReference onSave(){
        insert note;
        if(theContract.Contract_Type__c =='New'){
            sendAlertEmailNew();
        } else {
            sendAlertEmailRenewal();
        }
        PageReference contractPage = new ApexPages.StandardController(theContract).view();
        contractPage.setRedirect(true);
        return contractPage;
    }
    
    public PageReference repUpdate(){
        insert note;
        if( theContract.Contract_Type__c == 'New' )
        {
            sendRepUpdateEmail();
        }
        pendingOrdersContractStatus();
        PageReference contractPage = new ApexPages.StandardController(theContract).view();
        contractPage.setRedirect(true);
        return contractPage;
    }
    
    public PageReference declineOrderSubmit(){
        insert note;
        sendDeclineEmail();
        changeContractStatus();
        PageReference contractPage = new ApexPages.StandardController(theContract).view();
        contractPage.setRedirect(true);
        return contractPage;
    }
}