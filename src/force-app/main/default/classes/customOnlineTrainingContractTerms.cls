public class customOnlineTrainingContractTerms {
    @InvocableMethod (label='EHS Training Language')
    public static void customOnlineTrainingLanguage(List<id> contractLineItemId) {
        //The training line item that will need to be updated with the new language
        Contract_Line_Item__c lineItemToUpdate = [SELECT id,Y1_Total_Price__c,Name,Name__c,OpportunityId__c, Y1_Total_with_Bundle__c, Year_1_Manual__c, Product__r.Contract_Product_Name__c,Contract__c,Contract__r.Billing_Currency__c, Contract__r.Billing_Currency_Rate__c
                                                  FROM Contract_Line_Item__c
                                                  WHERE id =:contractLineItemId[0]
                                                  LIMIT 1];
        //Query Contract Line Items
        Contract_Line_Item__c[] lineItems = [SELECT id,Y1_Total_Price__c,Product__r.Contract_Product_Name__c,Contract__r.Billing_Currency__c, Contract__r.Billing_Currency_Rate__c
                                             FROM Contract_Line_Item__c
                                             WHERE Contract__c =: lineItemToUpdate.Contract__c
                                             AND
                                             OpportunityId__c =: lineItemToUpdate.OpportunityId__c
                                             AND
                                             (is_EHS_Module__c = true
                                              OR Name__c = 'Base Subscription')
                                             ORDER BY Product__r.Contract_Product_Name__c ASC];
        
        //Query for the terms
        string flowName;
        If(lineItemToUpdate.Name__c == 'Custom Online Training'){
            flowName = 'custom_online_training';
        }else{
            flowName = 'on_site_training';
        }
        String contractTerms = [SELECT id, Description__c
                                FROM Description_Dictionary__c
                                WHERE Flow_Referance_Name__c =:flowName
                                LIMIT 1].Description__c;
        
        
        //build the list of modules
        integer currentLoop = 0;
        String modulesList;
        if(lineItems.size() > 0){
            for (Contract_Line_Item__c cli:lineItems) {
                currentLoop++;
                if(modulesList == null){
                    modulesList = cli.Product__r.Contract_Product_Name__c; 
                }else if(currentLoop != lineItems.size()){
                    modulesList += ', '+cli.Product__r.Contract_Product_Name__c; 
                }else{
                    modulesList += ' and '+cli.Product__r.Contract_Product_Name__c; 
                }
            }
        }
        
        if(lineItemToUpdate != null && lineItemToUpdate.id != null){
            
            //string to hold the main terms that will be appended to the line item
            string itemTerms = contractTerms;
            
            //Update the modules in the terms
            if(modulesList != null){
                itemTerms = itemTerms.replace('&lt;Modules&gt;', modulesList);
            }
            //Build Muli-Currency total amount string
            string y1PriceConverted = '$';
            if(lineItemToUpdate.Contract__r.Billing_Currency__c != null && lineItemToUpdate.Contract__r.Billing_Currency__c == 'GBP'){
                y1PriceConverted = '&pound;';
            }else if(lineItemToUpdate.Contract__r.Billing_Currency__c != null && lineItemToUpdate.Contract__r.Billing_Currency__c == 'EUR'){
                y1PriceConverted = '&euro;'; 
            }
            decimal rate = 1;
            if(lineItemToUpdate.Contract__r.Billing_Currency_Rate__c != null){
                rate = lineItemToUpdate.Contract__r.Billing_Currency_Rate__c;
            }
            //decimal convertedTotal = Math.ceil(lineItemToUpdate.Y1_Total_Price__c * rate);
            //normally returnManualOOrTotalwithBundled(cli.Y1_Total_with_Bundle__c, cli.Year_1_Manual__c, currencyRate);
            decimal convertedTotal = contractUtility.returnManualOOrTotalwithBundled(lineItemToUpdate.Y1_Total_Price__c, null, rate);

            y1PriceConverted += string.valueof(convertedTotal.format());
            if(lineItemToUpdate.Contract__r.Billing_Currency__c != null){
                y1PriceConverted += ' '+lineItemToUpdate.Contract__r.Billing_Currency__c;
            }else{
                y1PriceConverted += ' USD';
            }
            itemTerms = itemTerms.replace('&lt;amount&gt;', y1PriceConverted);
            
            //Set the new contract terms on the item;
            lineItemToUpdate.Contract_Terms__c = itemTerms;
            
            update lineItemToUpdate;
        }
    }
}