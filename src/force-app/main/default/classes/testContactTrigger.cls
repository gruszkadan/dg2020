@isTest(seeAllData=true)
private class testContactTrigger {
    
    static testmethod void test_reassignContact() {
        Territory__c t = new Territory__c(Name='TEST1');
        insert t;
        
        
        Campaign  cam = new Campaign(Name = 'Referral 2017', StartDate = date.today(), EndDate = date.today().addDays(30));
        insert cam;
        
        Campaign  cam2 = new Campaign(Name = 'Cold Call 2017', StartDate = date.today(), EndDate = date.today().addDays(30));
        insert cam2;
        
        Campaign  cam3 = new Campaign(Name = 'Internet. Inbound Email 2017', StartDate = date.today(), EndDate = date.today().addDays(30));
        insert cam3;
        
        
        User u = [SELECT id FROM User WHERE Department__c = 'Sales' AND Group__c = 'Mid-Market' AND isActive = true LIMIT 1];
        
        Account a = new Account(Name='Acct1', Territory__c=t.id, OwnerId=u.id);
        insert a;
        
        Contact c = new Contact(FirstName='Test',LastName='Test',AccountId=a.id, Communication_Channel__c = 'Referral', Chemical_Management_NPS_Score__c = 5);
        insert c;
        
        c.Communication_Channel__c ='Cold Call';
        c.Unsubscribe_All__c = TRUE;
        c.CustomerAdministratorId__c = '400012';
        c.LeadSource = 'CC - Orders';
        update c;
        
        Survey_Feedback__c survey = new Survey_Feedback__c(contact__c = c.id);
        insert survey;
        c.CustomerAdministratorId__c = '';
        c.Survey_Automation_Step__c = 'AS - Survey';
        c.Survey_Email_Sent_Date__c = date.today();
        update c;
        
        
        c = [SELECT id, OwnerId FROM Contact WHERE id = : c.id LIMIT 1];
        
        System.assert(a.OwnerId == c.OwnerId);
        
    }
    static testmethod void test_primarySolutionPOI() {
        
        Account a = new Account(Name='Acct1');
        insert a;
        
        Contact con = new Contact(FirstName='Test',LastName='Test',AccountId=a.id, Communication_Channel__c = 'Referral', Primary_Solution_Product_of_Interest__c = 'MSDS Management');
        insert con;
        
        Campaign c = new Campaign(Name='Test Campaign');
        insert c;
        
        CampaignMember cm = new CampaignMember(ContactId = con.id, Status = 'Sent', CampaignId = c.id);
        insert cm;
        
        con.Primary_Solution_Product_of_Interest__c = 'MSDS Management;MSDS Authoring;Workplace Training';
        update con;
        
        cm = [select id,Primary_solution_Product_of_interest__c from CampaignMember where id =: cm.id];
        system.assert(cm.Primary_Solution_Product_of_Interest__c == con.Primary_Solution_Product_of_Interest__c);
    }
    
}