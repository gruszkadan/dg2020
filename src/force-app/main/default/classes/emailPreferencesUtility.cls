public class emailPreferencesUtility {
    
    //Checking "Unsubscribe All" automatically unchecks the opt-in preferences when saved
    public static void unsubscribeAll(list<sObject> records, string sObjectType) {
        sObjectField blogSubscription;
        sObjectField marketingEducational;
        sObjectField marketingPromotional;
        sObjectField newsletter;
        sObjectField salesConsulting;
        
        if (sObjectType == 'Lead') {
            blogSubscription = Lead.Opt_In_Blog_Subscription__c;
            marketingEducational = Lead.Opt_In_Marketing_Educational__c ;
            marketingPromotional = Lead.Opt_In_Marketing_Promotional__c;
            newsletter = Lead.Opt_In_Newsletter__c;
            salesConsulting = Lead.Opt_In_Sales_Consulting__c;
        } else if (sObjectType == 'Contact') {
            blogSubscription = Contact.Opt_In_Blog_Subscription__c;
            marketingEducational = Contact.Opt_In_Marketing_Educational__c  ;
            marketingPromotional = Contact.Opt_In_Marketing_Promotional__c;
            newsletter = Contact.Opt_In_Newsletter__c;
            salesConsulting = Contact.Opt_In_Sales_Consulting__c;
        }
        for (sObject r : records) {
            r.put(blogSubscription, false);
            r.put(marketingEducational, false);
            r.put(marketingPromotional, false);
            r.put(newsletter, false);
            r.put(salesConsulting, false);
        }  
    }
    
    //Default Sales Consulting Opt-In on Record Creation
    public static void salesConsultingOptIn(list<sObject> records, string sObjectType) {
        sObjectField salesConsultingField;
        sObjectField leadSourceTypeField;
        sObjectField countryField;
        sObjectField unsubscribeAllField;
        sObjectField optInSalesConsultingDateField;
        sObjectField accountId;
        Account[] accounts = new list<Account>();
        if (sObjectType == 'Lead') {
            salesConsultingField = Lead.Opt_In_Sales_Consulting__c;
            leadSourceTypeField = Lead.Lead_Source_Type__c;
            countryField = Lead.Country;
            unsubscribeAllField = Lead.Unsubscribe_All__c;
            optInSalesConsultingDateField = Lead.Opt_In_Sales_Consulting_Date__c;
        } else if (sObjectType == 'Contact') {
            salesConsultingField = Contact.Opt_In_Sales_Consulting__c;
            leadSourceTypeField = Contact.Lead_Source_Type__c;
            unsubscribeAllField = Contact.Unsubscribe_All__c;
            optInSalesConsultingDateField = Contact.Opt_In_Sales_Consulting_Date__c;
            accountId = Contact.AccountId;
            Set<id> accountIds = new set<id>();
            for(sObject c:records){
                accountIds.add((id)c.get('accountId'));
            }
            if(accountIds.size() > 0){
                accounts = [Select id,BillingCountry from account where id in: accountIds];
            }
        }
        for (sObject r : records) {
            boolean salesConsulting = (boolean)r.get(salesConsultingField);
            string leadSourceType = (string)r.get(leadSourceTypeField);
            string country;
            if(sObjectType == 'Lead'){
                country = (string)r.get(countryField);
            }else if (sObjectType == 'Contact'){
                for(Account a:accounts){
                    if((id)r.get('accountId') == a.id){
                        country = a.BillingCountry;
                    }
                }
            }
            boolean unsubscribeAll = (boolean)r.get(unsubscribeAllField);
            date optInSalesConsultingDate = (date)r.get(optInSalesConsultingDateField);
            if(leadSourceType =='Cold Call' || leadSourceType =='Referral'){
                if(country !='Canada'){
                    if(!unsubscribeAll){
                        if(optInSalesConsultingDate == NULL){
                            r.put(salesConsultingField, true);
                            r.put(optInSalesConsultingDateField, Date.today());
                        }
                    }
                }
            }
        }  
    }
}