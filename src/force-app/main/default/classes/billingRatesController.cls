public class billingRatesController 
{
    public Account a {get;set;}
    public id aid {get;set;}
    public User u {get;set;}
    public msdsCustomSettings helper = new msdsCustomSettings();
    public boolean editRates {get;set;}
    
    public billingRatesController()
    {
        aid = ApexPages.currentPage().getParameters().get('id');
        if(aid != null)
        {
            a = [SELECT Id, Name, System_Architect_Amount__c, Implementation_Consultant_Amount__c, Project_Manager_Amount__c, Implementation_Specialist_Amount__c, 
                 Quality_Assurance_Analyst_Amount__c, Training_Amount__c, System_Architect_Currency_Type__c, Implementation_Consultant_Currency_Type__c, 
                 Project_Manager_Currency_Type__c, Implementation_Specialist_Currency_Type__c, Quality_Assurance_Analyst_Currency_Type__c, Training_Currency_Type__c
                 FROM Account WHERE id = :aid LIMIT 1];
        }
        editRates = helper.editCustomerBillingRates();
    }
    
    public pageReference gotoCompEdit()
    {
        pageReference pr = new pageReference('/apex/accountComponentsEdit?id=' + aid);
        pr.getParameters().put('compName', 'billingRates');
        return pr.setRedirect(true);
    }
    
    public pageReference saveEdits()
    {
        update a;
        pageReference pr = new pageReference('/apex/account_detail_standard?id=' + aid);
        return pr.setRedirect(true);
    }
    
    public pageReference cancelEdits()
    {
        pageReference pr = new pageReference('/apex/account_detail_standard?id=' + aid);
        return pr.setRedirect(true);  
    }
}