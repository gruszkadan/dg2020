@isTest 
private class testOutOfBusinessAccounts { 
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    public static testmethod void t1 ()
    {
        Account acct = new Account ();
        acct.Name = 'ATest';
        acct.BillingPostalCode = '123456';
        acct.Customer_Status__c = 'Out of Business';
        acct.Num_of_SDS__c = 5;
        acct.Num_of_Employees__c = 100;
        insert acct;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = acct.Id;
        insert newContact;
        
        Test.startTest();
        String jobId = System.schedule('OutOfBusinessApexClassTest',
                                       CRON_EXP, 
                                       new outOfBusinessAccountScheduler());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        acct = [SELECT id, OwnerId, Territory__c FROM Account WHERE id = :acct.id LIMIT 1];
        System.assertEquals (NULL, acct.Territory__c);
        Test.stopTest();
    }
    
}