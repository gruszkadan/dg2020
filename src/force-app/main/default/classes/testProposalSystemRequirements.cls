@isTest(SeeAllData = true)
private class testProposalSystemRequirements {

    
    static testMethod void test1(){
        
        Quote__c testQuote = new Quote__c();
        insert testQuote;

        ApexPages.currentPage().getParameters().put('id', testQuote.id);
        
        proposalSystemRequirements con = new proposalSystemRequirements();
        
        Quote_Product__c quoteProd = new Quote_Product__c();
        quoteProd.Quote__c = testQuote.id;
        
        Product2 testProduct = new Product2();
        testProduct.Name = 'testProd';
        testProduct.Proposal_System_Requirements_Category__c = 'EHS Platform';
        insert testProduct;
        
        quoteProd.Product__c = testProduct.id;
        insert quoteProd;
        
        con = new proposalSystemRequirements();
        
        //test that wrappers created with appropriate fields
        system.assertEquals(con.productCategoryWrappers[0].productCategory, 'EHS Platform');
        
        //test that proposal system requirements only selected when paired with Quote Product; Category of PSR = Product's Proposal System Requirements Category
        system.assertEquals(con.psr.size(), 1);
        
        
    }
    
    
}