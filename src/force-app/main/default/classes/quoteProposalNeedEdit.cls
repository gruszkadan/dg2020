public class quoteProposalNeedEdit {
    
    public id needId {get;set;} 
    public User u {get;set;}
    public Quote_Proposal_Need__c need {get;set;}
    public productWrapper[] productWrappers {get;set;}
    public descriptionWrapper[] descriptionWrappers {get;set;}
    public List<Product2> products {get;set;}
    public List<String> errorMessages {get;set;}
    public string productFamily {get;set;}
    public string query {get;set;}
    public string bullet {get;set;}
    public string descr {get;set;}
    public string productFamilyList {get;set;}
    public string productList {get;set;}
    public boolean error {get;set;}
    
    
    
    public quoteProposalNeedEdit(ApexPages.StandardController stdController){
        
        
        descriptionWrappers = new list<descriptionWrapper>();
        
        needId = ApexPages.currentPage().getParameters().get('id');
        
        need = [Select Name, Need__c, Description__c, Global__c, Available_For_Use__c, Product__c,
                Product_Family__c, Display_Need_For_All_Products__c, Need_Heading__c FROM Quote_Proposal_Need__c WHERE Id = :needId];
        
        queryProducts();
        buildDescriptionBullets();
        
    }
    
    public void queryProducts(){
        
        query = 'Select Name, Id, Family, (Select Product__c FROM Product_Needs__r WHERE Quote_Proposal_Need__c = :needId) FROM Product2 WHERE IsActive = true AND Can_Have_Needs__c = true ';
        
        if(productFamily != null){
            query += 'AND Family = \''+productFamily+'\'';
        }
        
        products = database.query(query);
        products.sort();
        
        productWrappers = new List<productWrapper>();
        
        
        //If product is linked to quote proposal need, mark as selected and existing 
        //If product isn't linked to quote proposal need, mark as available and not existing
        for(product2 p : products){
            if(p.Family != null){
                if(p.Product_Needs__r.size() > 0){
                    productWrappers.add(new productWrapper(p, 'Selected', true));
                }
                else{
                    productWrappers.add(new productWrapper(p, 'Available', false));
                }
            }
        }
    }
    
    public List<SelectOption> getFamilyOptions() {
        
        Set<SelectOption> options = new Set<SelectOption>();  
        
        options.add(new selectOption('', '- All -'));
        
        for(product2 p : products){
            if(p.Family != null){
                options.add(new SelectOption(p.family, p.family));
            }
        }
        
        List<SelectOption> optionList = new List<SelectOption>(options);
        
        optionList.sort();
        
        return optionList;
    }
    
    public void moveToSelected(){
        
        for(productWrapper p : productWrappers){
            if(p.Action == true){
                p.productStatus = 'Selected'; 
                p.Action = false;
            } 
        }
    }
    
    public void moveToAvailable(){
        
        for(productWrapper p : productWrappers){
            if(p.Action == true){
                p.productStatus = 'Available'; 
                p.Action = false;
            } 
        }
    }
    
    public class productWrapper{
        public product2 prod {get;set;}
        public boolean action {get;set;}
        public string productStatus {get;set;}
        public boolean existing {get;set;}
        
        public productWrapper(product2 xProd, string xProductStatus, boolean xExisting){
            
            prod = xProd;
            productStatus = xProductStatus;
            existing = xExisting;
        }
    }
    
    public class descriptionWrapper{
        public integer order {get;set;}
        public string bullet {get;set;}
        
        public descriptionWrapper(string xBullet, integer xOrder){
            order = xOrder; 
            bullet = xBullet;
        }
    }
    
    public void addWrapper(){
        
        integer order = descriptionWrappers.size();
        
        descriptionWrappers.add(new descriptionWrapper(bullet,order));
        
    }
    
    public void removeBullet(){
        
        Integer bullet = Integer.valueOf(ApexPages.currentPage().getParameters().get('bullet'));
        
        descriptionWrappers.remove(bullet);  
        
        //loop through list and reorder
        for(integer x = 0; x < descriptionWrappers.size(); x++){
            descriptionWrappers[x].order = x;
        }
        
    }
    
    public void buildDescriptionBullets(){
        
        //parse description field into wrappers
        String descr = need.Description__c;
        
        List<String> descrList = descr.split(' \\</li>\\<li> ');
        
        for(integer x = 0; x < descrList.size(); x++){
            descriptionWrappers.add(new descriptionWrapper(descrList[x],x));
        }
        
    }
    
    public PageReference needsManagementHome(){
        
         return new PageReference('/apex/quote_proposal_need_management'); 
        
    }
    
    public void updateNeed(){
        
        productFamilyList = '';
        productList = null;
        descr = '';
        
        //convert description bullets into string
        for(descriptionWrapper d : descriptionWrappers){
            if(d.bullet != null && d.bullet != '' && !d.bullet.isWhiteSpace()){
                if(String.isBlank(descr)){
                    descr = d.bullet;
                } else{
                    descr += ' </li><li> ' + d.bullet;
                }
            }
        }
        
        //create set for products selected and their respective family; Use set to dedup
        Set<String> fam = new Set<String>();
        Set<String> pro = new Set<String>();
        
        
        for(productWrapper p : productWrappers){
            if(p.productStatus == 'Selected'){
                fam.add(p.prod.family);
                pro.add(p.prod.name);
            }
        }
        
        //convert family set into comma seperated string
        for(string s: fam){
            
            if(String.isBlank(productFamilyList)){
                productFamilyList = s;
            } else{
                productFamilyList += ', ' +s; 
            }
        }
        
        //convert product set into comma seperated string
        for(string p: pro){
            
            if(String.isBlank(productList)){
                productList = p;
            } else{
                productList += ', ' +p; 
            }
        }
        
        errorMessages = new List<String>();        
        
        if(productList == null && need.Display_Need_For_All_Products__c == false){
            errorMessages.add('Selected Products');
        } 
        if(need.Need__c == null){
            errorMessages.add('Need');
        } 
        if(need.Need_Heading__c == null){
            errorMessages.add('Need Heading');
        } 
        if(String.isBlank(descr)){
            errorMessages.add('Description');
        } 
        
        
        if(errorMessages.size() == 0){
            need.Description__c = descr;
            need.Product__c = productList;
            need.Product_Family__c = productFamilyList;
            
            try{
               
                update need;    
            
            } catch(DMLException e){
              
                errorMessages.add(String.valueOf(e));
           
            }
        }
        
        
        List<Product_Need__c> prodNeeds = new List<Product_Need__c>();
        List<Product_Need__c> deleteNeeds = new List<Product_Need__c>();
        
        //loop through selected productWrappers. Link each with new Product Need and respective Quote Proposal Need
        for(productWrapper p : productWrappers){
            if(p.productStatus == 'Selected' && p.existing == false){
                Product_Need__c prodNeed = new Product_Need__c();
                prodNeed.Product__c = p.prod.id;
                prodNeed.Quote_Proposal_Need__c = need.id; 
                prodNeeds.add(prodNeed);
            }
            //delete product needs that get moved from selected to available during edit
            if(p.productStatus == 'Available' && p.existing == true){
                deleteNeeds.add(p.prod.product_needs__r);
            }
        }
        
        if(errorMessages.size() == 0){
            
            try{
                insert prodNeeds;
                delete deleteNeeds;    
            } catch(DMLException e){
                errorMessages.add(String.valueOf(e));
            }
            
 
        }
   
    
        if(errormessages.size() > 0){
            error = false;
        }else{
            error = true;
        }
        
    
    
    }
    
}