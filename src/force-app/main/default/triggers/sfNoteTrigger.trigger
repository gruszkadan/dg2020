trigger sfNoteTrigger on Salesforce_Request_Note__c (after insert) {

       if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            sfNoteTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
    
       }
    
}