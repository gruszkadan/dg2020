<apex:page id="thePage" cache="false" controller="salesPerformanceSetup" showHeader="false" sidebar="false" standardStylesheets="true" lightningStylesheets="true" applyBodyTag="false" docType="html-5.0">
    <style>
        .slds-scope{
        font-size: 0.8125rem;
        font-family: "Salesforce Sans", Arial, sans-serif !important;
        line-height: 1.5;
        }
        ul li,ol li{
        margin-left:0px;
        }
        span.dateFormat{
        display:none;
        }
        div.datePicker{
        background-image:none;
        background-color: white;
        webkit-box-shadow:none;
        box-shadow:none;
        font-family: "Salesforce Sans",Arial,sans-serif !important;
        font-size: initial !important;
        1px solid #d8dde6 !important;
        box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.16) !important;
        border-radius: 0.25rem !important;
        }
        .datePicker .dateBar{
        margin-top: 0px;
        background: #fdfdfd;
        }
        .calLeft{
        background-image:url({!URLFOR($Asset.slds, '/assets/icons/utility/left_60.png')}) !important;
        background-size: 15px;
        width:15px !important;
        height:15px !important;
        margin-top: 9px !important;
        }
        .calLeftOn{
        background-image:url({!URLFOR($Asset.slds, '/assets/icons/utility/left_60.png')}) !important;
        background-size: 15px;
        width:15px !important;
        height:16px !important;
        background-position: initial !important;
        opacity: 0.5;
        margin-top: 8px !important;
        }
        .calRight{
        background-image:url({!URLFOR($Asset.slds, '/assets/icons/utility/right_60.png')}) !important;
        background-size: 15px;
        background-position: initial !important;
        }
        .calRightOn{
        background-image:url({!URLFOR($Asset.slds, '/assets/icons/utility/right_60.png')}) !important;
        background-size: 15px;
        background-position: initial !important;
        opacity: 0.5;
        }
        #calYearPicker{
        background-color: #fff;
        color: #16325c;
        border: 1px solid #d8dde6;
        border-radius: .25rem;  
        height: calc(1.875rem + (1px * 2));
        transition: border 0.1s linear, background-color 0.1s linear;
        }
        #calMonthPicker{
        background-color: #fff;
        color: #16325c;
        border: 1px solid #d8dde6;
        border-radius: .25rem;  
        height: calc(1.875rem + (1px * 2));
        transition: border 0.1s linear, background-color 0.1s linear;
        }
        .calBody{
        padding: 0 !important;
        }
        .calDays{
        font-size: 0.75rem !important;
        border-collapse: collapse;
        border-spacing: 0;
        box-sizing: border-box;
        display: table;
        border-color: grey;
        line-height: 1.5;
        }
        .calDays tr{
        vertical-align: middle !important;
        }
        .calDays th{
        color: #54698d !important;
        }
        .calDays td{
        width: 1.5rem;
        height: 1.5rem;
        position: relative;
        min-width: 1.5rem;
        line-height: 1.5rem;
        border-radius: 50%;
        padding: 0.25rem !important;
        text-align: center;
        font-size: 0.75rem;
        }
        .datePicker .calDays .todayDate{
        background-color: #f4f6f9;
        color: black;
        }
        .datePicker .calDays .selectedDate{
        background-color: #005fb2;
        color: white;
        border:none;
        }
        .calToday{
        font-size: 0.75rem !important;
        }
    </style>
    <c:salesPerformanceMaintenance rendered="{!$Setup.Sales_Performance_Settings__c.Maintenance_Mode__c}"/>
    <div style="{!if(!$Setup.Sales_Performance_Settings__c.Maintenance_Mode__c,'','display:none;')}">
        <div class="slds-scope">
            <c:salesPerformanceGlobalHeader uId="{!$User.Id}" page="sales_performance_setup"/>
            <c:salesPerformanceHeader uId="{!userId}" tab="setup" va="{!viewAs}"/>
            <apex:actionStatus onstart="showSpinner()" onstop="showAlert({!isSuccess})" id="blockElement"/>
            <apex:actionStatus onstart="showSpinner()" onstop="hideSpinner()" id="newRow"/>
            <apex:form >
                <div class="slds-m-left_large">
                    <nav role="navigation" aria-label="Breadcrumbs">
                        <ol class="slds-breadcrumb slds-list_horizontal slds-p-vertical_x-small" >
                            <li class="slds-breadcrumb__item slds-text-title_caps">
                                <a href="/apex/sales_performance_setup">
                                    Setup
                                </a>
                            </li>
                            <li class="slds-breadcrumb__item slds-text-title_caps">
                                <a style="pointer-events: none;cursor: default; font-weight:bold;">
                                    Mid-Market Associate Incentive Settings
                                </a>
                            </li>
                        </ol>
                    </nav> 
                </div>
                <article class="slds-card slds-m-horizontal_medium">
                    <div class="slds-card__header slds-grid">
                        <header class="slds-media slds-media_center slds-has-flexi-truncate">
                            <div class="slds-media__figure">
                                <span class="slds-icon_container slds-icon-utility-thunder" title="description of icon when needed">
                                    <svg class="slds-icon slds-icon_small slds-icon-text-default" aria-hidden="true">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#moneybag')}"/>
                                    </svg>
                                </span>
                            </div>
                            <div class="slds-media__body">
                                <h2>
                                    <div style="cursor:default" class="slds-card__header-link">
                                        <span class="slds-text-heading_small" style="font-weight: 700;font-size: 1rem;line-height: 1.25;font-family: 'Salesforce Sans', Arial, sans-serif;">Mid-Market Opportunity Incentive Settings</span>
                                    </div>
                                </h2>
                            </div>
                        </header>
                    </div>
                    
                    <apex:outputPanel id="doesNotHavePermission" rendered="{!viewMMOpportunityIncentives=false}" >
                        <center>
                            <div class="slds-p-around_xx-large slds-card__footer">
                                <div class="slds-p-top_small">
                                    <span class="slds-icon_container">
                                        <svg class="slds-icon slds-icon_large slds-icon-text-default" style="fill:#c23934" aria-hidden="true">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#error')}" />
                                        </svg>
                                    </span>
                                </div>
                                <h1 class="slds-text-heading_medium slds-p-vertical_small">Error: You are not authorized to access this page.</h1>
                            </div>
                        </center>
                    </apex:outputPanel>
                    
                    <div class="slds-m-top_medium slds-card__footer" style="{!IF(viewMMOpportunityIncentives, '', 'display:none;')}">
                        <apex:outputText id="alertOutput">
                            <div id="alert" class="alert slds-notify_container slds-is-relative slds-hide" style="padding:0px;">
                                <div id="alert_type" class="alertType slds-notify slds-notify_toast" role="alert">
                                    <span id="success_icon" class="successIcon slds-icon_container slds-m-right_small slds-no-flex slds-align-top slds-hide slds-icon-utility-success">
                                        <svg class="slds-icon slds-icon_small" aria-hidden="true">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#success')}"></use>
                                        </svg>
                                    </span>
                                    <div id="success_msg" class="successMessage slds-notify__content slds-hide">
                                        <h2 class="slds-text-heading_small">
                                            Mid-Market Opportunity Incentive Settings Updated
                                        </h2>
                                    </div>
                                    <span id="error_icon" class="errorIcon slds-icon_container slds-m-right_small slds-no-flex slds-align-top slds-hide slds-icon-utility-error">
                                        <svg class="slds-icon slds-icon_small" aria-hidden="true">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#error')}"></use>
                                        </svg>
                                    </span>
                                    <div id="error_msg" class="errorMessage slds-notify__content slds-hide">
                                        <h2 class="slds-text-heading_small">
                                            There was an error
                                        </h2>
                                        <p>
                                            {!errorMessage}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </apex:outputText>
                        <apex:outputPanel id="theData">
                            <center>
                                <table class="slds-table slds-table_bordered slds-no-row-hover slds-border_left slds-border_right" style="width:75%">
                                    <thead>
                                        <tr class="slds-text-heading--label">
                                            <th scope="col">
                                                <div class="slds-truncate" title="Action"></div>
                                            </th>
                                            <th scope="col">
                                                <div class="slds-truncate" title="Rep">Rep</div>
                                            </th>
                                            <th scope="col">
                                                <div class="slds-truncate" title="% of Bookings">% of Bookings</div>
                                            </th>
                                            <th scope="col">
                                                <div class="slds-truncate" title="Incentive Stop Date">Incentive Stop Date</div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <apex:variable value="{!0}" var="cnt"/>
                                        <apex:repeat value="{!Settings}" var="ess">
                                            <tr id="row" class="{!IF(ess.Incentive_Stop_Date__c != null && ess.Incentive_Stop_Date__c < TODAY(),'slds-theme_alert-texture slds-theme_warning','')}">
                                                <td style="width: 50px;">
                                                    <div class="slds-form-element">
                                                        <div class="slds-form-element__control">
                                                            <apex:commandLink action="{!removeSetting}" title="Remove" styleClass="sds-icon slds-icon-text-default slds-icon_small" onclick="if(!confirm('Are you sure you want to delete this setting?')) return false;">
                                                                <svg class="slds-button__icon" aria-hidden="true">
                                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/action-sprite/svg/symbols.svg#remove')}"></use>
                                                                </svg>
                                                                <apex:param name="index" value="{!cnt}"/>
                                                            </apex:commandLink>
                                                            <apex:variable var="cnt" value="{!cnt+1}"/> 
                                                        </div>
                                                    </div>
                                                </td>
                                                <td id="Associate" data-label="Associate">                                       
                                                    <div class="slds-truncate slds-select_container" title="{!ess.Associate__c}">   
                                                        <apex:selectList styleClass="slds-select" id="esAssociate" value="{!ess.Associate__c}" size="1">
                                                            <apex:selectOptions value="{!Associates}"/>
                                                            <div style="display:none">
                                                                {!ess.Associate__c}
                                                            </div>
                                                        </apex:selectList>
                                                    </div>
                                                </td>
                                                <td id="Sales Representative" data-label="Sales Representative">
                                                    <div class="slds-form-element">
                                                        <div class="slds-form-element__control">
                                                            <apex:inputField styleClass="slds-input" value="{!ess.Percent_Of_Bookings__c}"/>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="slds-form-element">
                                                        <div class="slds-form-element__control">
                                                            <apex:inputField styleClass="slds-input" style="width: 9em;" value="{!ess.Incentive_Stop_Date__c}"/>
                                                            <apex:outputText style="" rendered="{!ess.Incentive_Stop_Date__c != null && ess.Incentive_Stop_Date__c < TODAY()}">
                                                                &emsp;
                                                                <span class="slds-icon_container slds-icon-utility-warning slds-m-right_x-small">
                                                                    <svg class="slds-icon slds-icon_x-small" aria-hidden="true">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#warning')}"></use>
                                                                    </svg>
                                                                </span>
                                                                <h2 style="font-family: 'Salesforce Sans', Arial, sans-serif"> 
                                                                    No longer receiving credit. Pending removal at close of month.
                                                                </h2>
                                                            </apex:outputText>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </apex:repeat>
                                    </tbody>
                                </table>
                                <div class="slds-p-vertical_x-small">
                                    <apex:commandLink status="newRow" reRender="newRow,theData" styleClass="slds-button slds-button_brand" value="Add Row" action="{!addSetting}"/>
                                    <apex:commandLink styleClass="slds-button slds-button_brand" status="blockElement" reRender="blockElement,theForm" value="Save Changes" action="{!saveChanges}"/>
                                </div>
                                <div id="spinner" class="slds-spinner_container slds-hide">
                                    <div role="status" class="slds-spinner slds-spinner_medium slds-spinner_brand">
                                        <div class="slds-assistive-text">Loading Data...</div>
                                        <div class="slds-spinner__dot-a"></div>
                                        <div class="slds-spinner__dot-b"></div>
                                    </div>
                                </div>
                            </center>
                        </apex:outputPanel>
                    </div>
                </article>
            </apex:form>
        </div>
        <c:salesPerformanceFooter />
        <script>
        var j$ = jQuery.noConflict();
        function showAlert(success) {
            j$(".alertType").removeClass("slds-theme_success");
            j$(".alertType").removeClass("slds-theme_error");
            j$(".successIcon").addClass("slds-hide");
            j$(".successMessage").addClass("slds-hide");
            j$(".errorIcon").addClass("slds-hide");
            j$(".errorMessage").addClass("slds-hide");
            if(success){
                j$(".alertType").addClass("slds-theme_success");
                j$(".successIcon").removeClass("slds-hide");
                j$(".successMessage").removeClass("slds-hide");
            } else {
                j$(".alertType").addClass("slds-theme_error");
                j$(".errorIcon").removeClass("slds-hide");
                j$(".errorMessage").removeClass("slds-hide");
            }
            j$("#spinner").addClass("slds-hide");
            j$(".alert").slideDown("5000");
            j$(".alert").delay("5500").slideUp("5000");
            
        }
        function showSpinner() {
            j$("#spinner").removeClass("slds-hide");
        }
        function hideSpinner() {
            j$("#spinner").addClass("slds-hide");
        }
        </script>
    </div>
</apex:page>