public class adminToolUtility {
    
    public void upsertCustomerData(string adminToolData){
        Account[] updatedAccounts = new list<Account> ();
        map<String, adminToolAPIJSONUtility.customerData> ReviewVPM = new map<String, adminToolAPIJSONUtility.customerData>();       
        JSONParser parser = JSON.createParser(adminToolData);
        while (parser.nextToken() != null) { 
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {       
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        adminToolAPIJSONUtility.customerData at = (adminToolAPIJSONUtility.customerData)parser.readValueAs(adminToolAPIJSONUtility.customerData.class);
                        
                        ReviewVPM.put(at.AccountID, at);
                        
                        Account a=new Account();
                        a.Number_of_Actual_SDSs__c = at.MSDSCount;
                        a.Number_of_Purchased_Admins__c = at.AdminsPurchased;
                        a.Name = at.CustomerCompanyName;
                        a.Phone = at.Phone;
                        a.AdminID__c = at.AccountID;
                        a.AID__c = at.AID;
                        //previously was a.At_Risk__c = at.AtRisk.  If active and at Risk, set to true. If inactive, set to false.
                        if (at.AtRisk && at.AtRiskStatus=='Active'){
                            a.At_Risk__c = TRUE;
                        } else {
                            a.At_Risk__c = FALSE;
                        }
                        a.BillingCity = at.City;
                        a.BillingCountry = at.Country;
                        a.Billing_Exceptions__c = at.BillingException;
                        a.Billing_Exceptions_Notes__c = at.BillingExceptionNotes;
                        if(at.State != NULL){
                            a.BillingState = at.State;
                        } else {
                            a.BillingState = at.Province;
                        }
                        a.BillingStreet = at.AddressLine1;
                        a.BillingPostalCode = at.ZipCode;
                        a.Channel__c = at.ChannelCode;
                        a.Contract_Length__c = at.ContractTermLength;
                        if(at.ContractTermEndDate != NULL){
                            a.Contract_Term_Exp_Date__c = date.valueOf(at.ContractTermEndDate)+1;
                        }
                        //a.Customer_Annual_Revenue__c = at.AnnualRevenue;
                        a.Customer_Status__c = at.CustomerStatus;
                        a.CustomerID__c = at.PersonID;
                        a.GPO__c = at.GPO1;
                        a.GPO2__c = at.GPO2;
                        a.GUID__c = at.URL;
                        if(at.NAICS == '11'){
                            a.NAICS_Code__c = '11 - Agriculture, Forestry, Fishing & Hunting';
                        } else if(at.NAICS == '21'){
                            a.NAICS_Code__c = '21 - Mining, Quarrying, and Oil and Gas Extraction';
                        } else if(at.NAICS == '22'){
                            a.NAICS_Code__c = '22 - Utilities';
                        } else if(at.NAICS == '23'){
                            a.NAICS_Code__c = '23 - Construction';
                        } else if(at.NAICS == '31-33'){
                            a.NAICS_Code__c = '31-33 - Manufacturing';
                        } else if(at.NAICS == '42'){
                            a.NAICS_Code__c = '42 - Wholesale Trade';
                        } else if(at.NAICS == '44-45'){
                            a.NAICS_Code__c = '44-45 - Retail Trade';
                        } else if(at.NAICS == '48-49'){
                            a.NAICS_Code__c = '48-49 - Transportation & Warehousing';
                        } else if(at.NAICS == '51'){
                            a.NAICS_Code__c = '51 - Information';
                        } else if(at.NAICS == '52'){
                            a.NAICS_Code__c = '52 - Finance & Insurance';
                        } else if(at.NAICS == '53'){
                            a.NAICS_Code__c = '53 - Real Estate, Rental & Leasing';
                        } else if(at.NAICS == '54'){
                            a.NAICS_Code__c = '54 - Professional, Scientific, & Technical Services';
                        } else if(at.NAICS == '55'){
                            a.NAICS_Code__c = '55 - Management of Companies & Enterprises';
                        } else if(at.NAICS == '56'){
                            a.NAICS_Code__c = '56 - Administrative & Support & Waste Management & Remediation Services';
                        } else if(at.NAICS == '61'){
                            a.NAICS_Code__c = '61 - Educational Services';
                        } else if(at.NAICS == '62'){
                            a.NAICS_Code__c = '62 - Health Care & Social Assistance';
                        } else if(at.NAICS == '71'){
                            a.NAICS_Code__c = '71 - Arts, Entertainment, & Recreation';
                        } else if(at.NAICS == '72'){
                            a.NAICS_Code__c = '72 - Accommodation & Food Services';
                        } else if(at.NAICS == '81'){
                            a.NAICS_Code__c = '81 - Other Services (except public administration)';
                        } else if(at.NAICS == '92'){
                            a.NAICS_Code__c = '92 - Public Administration (Fed/State/Local Government)';
                        }
                        a.Retention_Owner__c = at.RetentionOwner;
                        a.VIP__c = at.VIP;
                        updatedAccounts.add(a);
                    }
                }
            }
        }
        
        //Iff a corresponding VPM project exists and has updates, update the VPM
        VPM_Project__c[] vpmProjToUpdate = new List<VPM_Project__c>();
        List<String> AdminIds = new List<String>(ReviewVPM.keySet());
        string accountPendingUpdateWithoutProject = NULL;
        
        if(AdminIds.size() > 0){
            VPM_Project__c[] vpmProj = [Select id, account__r.AdminID__c, Exception__c, Upgrade_Status__c, Scheduled_Upgrade_Date__c, Actual_Upgrade_Date__c 
                                        from VPM_Project__c 
                                        where account__r.AdminID__c IN :AdminIds AND Type__c ='Chemical Management Upgrade'];
            
            for(adminToolAPIJSONUtility.customerData AtData : ReviewVPM.values()){
                boolean corrrespondingProjectFound = false;
                for(VPM_Project__c vpm :vpmProj){    
                    boolean hasChanges = false;
                    
                    if(vpm.account__r.AdminID__c == AtData.AccountID){
                        corrrespondingProjectFound = true;                    
                        string exclude;
                        if(AtData.ExcludeFromUpgrade){
                            exclude = 'Yes';
                        }else{
                            exclude = 'No';
                        }
                        if(exclude != vpm.Exception__c){
                            vpm.Exception__c = exclude;
                            hasChanges = true;
                        }
                        if(AtData.UpradeStatus != vpm.Upgrade_Status__c){                        
                            vpm.Upgrade_Status__c = AtData.UpradeStatus;
                            hasChanges = true;
                        }
                        if((AtData.ScheduledUpgradeDate != NULL && date.ValueOf(AtData.ScheduledUpgradeDate) != vpm.Scheduled_Upgrade_Date__c) || (AtData.ScheduledUpgradeDate == NULL && vpm.Scheduled_Upgrade_Date__c != NULL)){
                            if(AtData.ScheduledUpgradeDate != NULL){
                                vpm.Scheduled_Upgrade_Date__c = date.ValueOf(AtData.ScheduledUpgradeDate);
                            }else{
                                vpm.Scheduled_Upgrade_Date__c = null;
                            }
                            hasChanges = true;
                        }
                        if((AtData.ActualUpgradeDate != NULL && date.ValueOf(AtData.ActualUpgradeDate) != vpm.Actual_Upgrade_Date__c) || (AtData.ActualUpgradeDate == NULL  && vpm.Actual_Upgrade_Date__c != NULL)){
                            if(AtData.ActualUpgradeDate != NULL){
                                vpm.Actual_Upgrade_Date__c = date.ValueOf(AtData.ActualUpgradeDate);
                            }else{
                                vpm.Actual_Upgrade_Date__c = null;
                            }
                            hasChanges = true;
                        }
                        if(hasChanges){
                            vpmProjToUpdate.add(vpm);
                        }
                        break;
                    }
                }
                if(corrrespondingProjectFound == false && AtData.ScheduledUpgradeDate != NULL){
                    if (accountPendingUpdateWithoutProject == NULL){
                        accountPendingUpdateWithoutProject = AtData.AccountID;
                    }else{
                        accountPendingUpdateWithoutProject += ', ' + AtData.AccountID;
                    }
                }
            }
        } 
        
        //update accounts
        if (updatedAccounts.size() > 0) {
            try {
                upsert updatedAccounts AdminID__c;
            } catch(System.DMLexception e) {
                salesforceLog.createLog('Admin Tool API', TRUE, 'adminToolUtility', 'upsertCustomerData-updateAccounts', string.valueOf(e));
            }
        }
        
        //update projects
        if(vpmProjToUpdate.size() > 0){                    
            try {
                update vpmProjToUpdate;
            }catch(System.DMLexception e) {
                salesforceLog.createLog('Admin Tool API', TRUE, 'adminToolUtility', 'upsertCustomerData-updateVpmProjects', string.valueOf(e));
            }
        }
        
        //notify SF if VPMProjects are missing
        if(accountPendingUpdateWithoutProject != NULL){
            salesforceLog.createLog('Admin Tool API', TRUE, 'adminToolUtility', 'upsertCustomerData-missingVpmProjects', 'The following accounts have a scheduled upgrade date but no VPM project was found: '+ accountPendingUpdateWithoutProject);
        }
        
    }
    
    public void upsertUserData(string adminToolData){
        Contact[] adminToolContacts = new list<Contact> ();
        Contact[] updatedContacts = new list<Contact> ();
        Contact[] newContacts = new list<Contact> ();
        Contact[] foundContacts = new list<Contact> ();
        set<string> adminUserIDs = new set<string>();
        set<string> usersUserIDs = new set<string>();
        set<string> userIDs = new set<string>();
        JSONParser parser = JSON.createParser(adminToolData);
        while (parser.nextToken() != null) { 
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {       
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        adminToolAPIJSONUtility.userData ud = (adminToolAPIJSONUtility.userData)parser.readValueAs(adminToolAPIJSONUtility.userData.class);
                        boolean validEmail;
                        if(ud.Description !='Viewer' && ud.Description !='General User'){
                            Contact c = new Contact();
                            c.CustomerAdministratorId__c = string.valueOf(ud.AccountID);
                            if(ud.PrimaryAccountAdministrator == false){
                                c.CustomerUserId__c = string.valueOf(ud.CustomerUserID); 
                            }
                            c.Role__c = ud.Description;
                            validEmail = validateEmail(ud.Email);
                            if(validEmail){
                                c.Email = ud.Email;
                            }
                            c.FirstName = ud.FirstName.left(40);
                            c.LastName = ud.LastName;
                            c.Phone = ud.Phone;
                            c.Admin_Tool_Status__c = ud.Status;
                            adminToolContacts.add(c);
                            userIDs.add(string.valueOf(ud.AccountID));
                            if(c.CustomerUserID__c !=NULL){
                                usersUserIDs.add(string.valueOf(c.CustomerUserID__c));
                            } else {
                                adminUserIDs.add(string.valueOf(c.CustomerAdministratorId__c));
                            }
                        }
                    }
                }
            }
        }
        if(adminToolContacts.size() >0){
            foundContacts = [Select ID, CustomerAdministratorId__c, CustomerUserId__c, Role__c, Email, FirstName, LastName, Phone, Status__c
                             from Contact where CustomerAdministratorId__c in: adminUserIDs OR CustomerUserId__c in: usersUserIDs];
            if(foundContacts.size()>0){
                for(Contact fc : foundContacts){
                    for(integer i=0; i<adminToolContacts.size();i++){
                        if(adminToolContacts[i].CustomerUserId__c == fc.CustomerUserId__c && fc.CustomerAdministratorId__c == NULL){
                            fc.Admin_Tool_Status__c = adminToolContacts[i].Admin_Tool_Status__c;
                            fc.CustomerUserId__c = adminToolContacts[i].CustomerUserId__c;
                            if(adminToolContacts[i].Email != NULL){
                                fc.Email = adminToolContacts[i].Email;
                            }
                            fc.FirstName = adminToolContacts[i].FirstName;
                            if(adminToolContacts[i].LastName != NULL){
                                fc.LastName = adminToolContacts[i].LastName;
                            } else {
                                fc.LastName = 'Unknown';
                            }
                            fc.Phone = adminToolContacts[i].Phone;
                            fc.Role__c = adminToolContacts[i].Role__c;
                            updatedContacts.add(fc);
                            adminToolContacts.remove(i);
                        } else if (adminToolContacts[i].CustomerUserId__c == NULL && adminToolContacts[i].CustomerAdministratorId__c == fc.CustomerAdministratorId__c && adminToolContacts[i].Email == fc.Email){
                            fc.Admin_Tool_Status__c = adminToolContacts[i].Admin_Tool_Status__c;
                            fc.CustomerAdministratorId__c = adminToolContacts[i].CustomerAdministratorId__c;
                            if(adminToolContacts[i].Email != NULL){
                                fc.Email = adminToolContacts[i].Email;
                            }
                            fc.FirstName = adminToolContacts[i].FirstName;
                            if(adminToolContacts[i].LastName != NULL){
                                fc.LastName = adminToolContacts[i].LastName;
                            } else {
                                fc.LastName = 'Unknown';
                            }
                            fc.Phone = adminToolContacts[i].Phone;
                            fc.Role__c = adminToolContacts[i].Role__c;
                            updatedContacts.add(fc);
                            adminToolContacts.remove(i);
                        }
                    }
                }
            }
            if(foundContacts.size()==0 || adminToolContacts.size()>0){
                Map<String, Account> accounts = new Map<String, Account>();    
                for (Account a : [Select AdminID__c, ID from Account where AdminID__c in: userIDs]) {
                    accounts.put(a.AdminID__c, a);
                }
                if(accounts.size()>0){
                    adminToolContacts.sort();
                    //Iterate descending & remove one by one
                    for(Integer i = adminToolContacts.size() -1; i >=0; i--){
                        adminToolContacts[i].AccountId = accounts.get(adminToolContacts[i].CustomerAdministratorId__c).Id;
                        adminToolContacts[i].Lead_Created_Date__c = date.today();
                        adminToolContacts[i].Opt_In_Blog_Subscription__c = TRUE;
                        adminToolContacts[i].Opt_In_Marketing_Educational__c = TRUE;
                        adminToolContacts[i].Opt_In_Marketing_Promotional__c = TRUE;
                        adminToolContacts[i].Opt_In_Newsletter__c = TRUE;
                        adminToolContacts[i].Opt_In_Sales_Consulting__c = TRUE;
                        adminToolContacts[i].Status__c = 'Active';
                        if(adminToolContacts[i].CustomerUserId__c != NULL){
                            adminToolContacts[i].CustomerAdministratorId__c = NULL;
                            adminToolContacts[i].LeadSource = NULL;
                        }
                        if(adminToolContacts[i].Role__c =='Manager'){
                            adminToolContacts[i].LeadSource ='S List - Manager';
                        }
                        if(adminToolContacts[i].Role__c =='Administrator'){
                            adminToolContacts[i].LeadSource ='S List - Admin';
                        }
                        if(adminToolContacts[i].Role__c =='Account Administrator'){
                            adminToolContacts[i].LeadSource ='S List - Primary Admin';
                        }
                        
                        newContacts.add(adminToolContacts[i]);
                        adminToolContacts.remove(i);
                    }
                }
                if(adminToolContacts.size()>0){
                    string notFound;
                    for(Contact nf : adminToolContacts){
                        if(notFound == NULL){
                            notFound = '{'+nf.CustomerAdministratorId__c+', '+nf.CustomerUserId__c+', '+nf.Role__c+', '+nf.Email+', '+nf.FirstName+', '+nf.LastName+', '+nf.Phone+', '+nf.Admin_Tool_Status__c+'}';
                        } else {
                            notFound += ',{'+nf.CustomerAdministratorId__c+', '+nf.CustomerUserId__c+', '+nf.Role__c+', '+nf.Email+', '+nf.FirstName+', '+nf.LastName+', '+nf.Phone+', '+nf.Admin_Tool_Status__c+'}';
                            
                        }
                    }
                    salesforceLog.createLog('Admin Tool API', TRUE, 'adminToolUtility', 'upsertUserData', 'Cound not find an account for these contacts '+notFound);    
                }
            }
            if (updatedContacts.size() > 0) {
                try {
                    update updatedContacts;
                } catch(System.DMLexception e) {
                    salesforceLog.createLog('Admin Tool API', TRUE, 'adminToolUtility', 'upsertUserData', string.valueOf(e));
                }
            }
            if (newContacts.size() > 0) {
                try {
                    insert newContacts;
                } catch(System.DMLexception e) {
                    salesforceLog.createLog('Admin Tool API', TRUE, 'adminToolUtility', 'upsertUserData', string.valueOf(e));
                }
            }
        }
    }
    
    public static Boolean validateEmail(String email) {
        Boolean res = true; 
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);  
        if (!MyMatcher.matches()) 
            res = false;
        return res; 
    }
    
    public void upsertOrderData(string adminToolData){
        Order_Item__c[] orderItems = new list<Order_Item__c> ();
        JSONParser parser = JSON.createParser(adminToolData);
        while (parser.nextToken() != null) { 
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {       
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        adminToolAPIJSONUtility.bookingsData at = (adminToolAPIJSONUtility.bookingsData)parser.readValueAs(adminToolAPIJSONUtility.bookingsData.class);
                        Order_Item__c oi = new Order_Item__c();
                        oi.Contract_Number__c = at.ContractNumber;
                        oi.Contract_Length__c = at.ContractTerm;
                        oi.AccountID__c = string.valueOf(at.AccountID);
                        oi.Implementation_Model__c = at.Implementation;
                        if(at.InvoiceAmount != NULL){
                            oi.Invoice_Amount__c = at.InvoiceAmount;
                        }else{
                            oi.Invoice_Amount__c = 0;
                        }
                        oi.Month__c = at.Month;
                        if(at.NewOrRenewal =='NEW'){
                            oi.Admin_Tool_Order_Type__c = 'New';
                        } else {
                            oi.Admin_Tool_Order_Type__c = 'Renewal';
                        }
                        if(at.OrderDate != NULL){
                            dateTime oDate = at.OrderDate;
                            date orderDateOnly = oDate.dateGMT();
                            oi.Order_Date__c = orderDateOnly;
                        }
                        oi.Order_ID__c = string.valueOf(at.OrderID);
                        oi.Order_Item_ID__c = string.valueOf(at.OrderItemID);
                        oi.Admin_Tool_Product_Name__c = at.ProductName;
                        oi.Admin_Tool_Sales_Rep__c = at.SalesRep;
                        oi.Sales_Location__c = at.SalesLocation;
                        oi.Admin_Tool_Order_Status__c = at.Status;
                        oi.Subscription_Model__c = at.Subscription;
                        if(at.TermEndDate != NULL){
                            oi.Term_End_Date__c = date.valueOf(at.TermEndDate)+1;
                        }
                        oi.Term_Length__c = at.TermLength;
                        if(at.TermStartDate != NULL){
                            oi.Term_Start_Date__c = date.valueOf(at.TermStartDate)+1;
                        }
                        oi.Admin_Tool_Product_Type__c = at.Type;
                        oi.Year__c = string.valueOf(at.Year);
                        oi.Renewal_Amount__c = at.RenewalPrice;
                        if(at.ProductPlatform != NULL){
                            oi.Product_Platform__c = at.ProductPlatform;
                        }
                        oi.Version__c = at.Version;
                        if(at.ContractTermEndDate != NULL){
                            dateTime cteDate = at.ContractTermEndDate;
                            date contractTermEndDateOnly = cteDate.dateGMT();
                            oi.Contract_Term_End_Date__c = contractTermEndDateOnly;
                        }
                        oi.Do_Not_Renew__c = at.DoNotRenew;
                        oi.Unique_MSA__c = at.UniqueMSA;
                        oi.Authorized_for_Commercial_Use__c = at.AuthorizedForCommercialUse;
                        oi.Public_Facing_HQ__c = at.PublicFacingHQ;
                        orderItems.add(oi);
                        System.debug(oi);
                    }
                }
            }
        }
        System.debug(orderItems.size());
        
        if (orderItems.size() > 0) {
            try {                
                upsert orderItems Order_Item_ID__c;
                
            } catch(System.DMLexception e) {
                System.debug('error '+ string.valueOf(e));
                salesforceLog.createLog('Admin Tool API', TRUE, 'adminToolUtility', 'upsertOrderData', string.valueOf(e));
            }
        }
    }
    
    public Account[] getSalesforceIDs(){
        List<Account> acct = [Select AdminID__c, ID from Account where AdminID__c != NULL and SystemModstamp = TODAY];
        return acct;
    }
    
    public Case[] getdfuExport(){
        Set<id> caseIDs = new Set<id>();
        for(AggregateResult results :[Select ID, AccountID, MAX(ClosedDate) from Case where Status = 'Closed' and Type ='Delivery Follow Up' and SystemModstamp = TODAY and Account.AdminID__c != NULL Group By AccountID, ID])
            caseIDs.add((id)results.get('ID'));
        Case[] cse =[Select ID, AccountID, Account.AdminID__c, ClosedDate, DFU_Status__c, Opt_Out__c, 
                     Owner.Name from Case where Status = 'Closed' and Type ='Delivery Follow Up' and ID IN :caseIds];
        return cse;
    }
}