trigger poiActionTrigger on Product_Of_Interest_Action__c 
(before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    if (trigger.isBefore) {
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            poiActionTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
    }
    
    
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            poiActionTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            poiActionTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
    }

    
}