@isTest()
private class testCountPrimaryOppContactsBatchable {
    
    public static testmethod void test1() {
        
        Account act = new Account(Name = 'Test Account', NAICS_Code__c = '23 - Construction');
        insert act;
        
        Contact con1 = new Contact(FirstName = 'Test', LastName ='Person1', AccountId = act.Id);
        insert con1;
        
        Contact con2 = new Contact(FirstName = 'Test', LastName ='Person2', AccountId = act.Id);
        insert con2;
        
        Opportunity op1 = new Opportunity(AccountId = act.id, Name = 'Test Opportunity 1', StageName = 'Demo Scheduled', CloseDate = date.today()+2);
        insert op1;
        
        Opportunity op2 = new Opportunity(AccountId = act.id, Name = 'Test Opportunity 2', StageName = 'Demo Scheduled', CloseDate = date.today()+2);
        insert op2;
        
        OpportunityContactRole op1cr1 = new OpportunityContactRole(OpportunityId = op1.Id, ContactId = con1.Id, IsPrimary = true);
        insert op1cr1;
        
        OpportunityContactRole op1cr2 = new OpportunityContactRole(OpportunityId = op1.Id, ContactId = con2.Id, IsPrimary = false);
        insert op1cr2;
        
        OpportunityContactRole op2cr1 = new OpportunityContactRole(OpportunityId = op2.Id, ContactId = con2.Id, IsPrimary = false);
        insert op2cr1;
        
        countPrimaryOppContactsBatchable upb = new countPrimaryOppContactsBatchable(false);
        id batchprocessId = Database.executeBatch(upb);
    }
    
}