public class contractCofHumantechServices {
    private ApexPages.StandardController controller;
    public String contractId {get;set;}
    public List<Contract_Line_Item__c> contractItems {get;set;}
    
    public contractCofHumantechServices (ApexPages.StandardController stdController){
        contractId = ApexPages.currentPage().getParameters().get('id');
        this.controller = stdController;
        queryContractItems();
    }
    
    // query the contract items
    public void queryContractItems() {
        String SobjectApiName = 'Contract_Line_Item__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String OrderBy = ' Order By Contract_Sort_Order__c ASC';
        String query = 'select ' + commaSepratedFields +' from ' + SobjectApiName + ' where Contract__c=\''+contractId+'\' '+ OrderBy;
        // return the contract items
        contractItems = Database.query(query);
    }
    
}