@isTest(seeAllData=true)
private class testpoiActionInsert {
    
    static testmethod void test_createPOIActions() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        insert c;
        Lead l = testLead();
        insert l;
        
        test.startTest();
        
        poiActionUtility.poiAction[] actions = new list<poiActionUtility.poiAction>();
        poiActionUtility.poiAction action1 = new poiActionUtility.poiAction();
        action1.primaryAttributeValueId = '18157';
        action1.sfId = c.id;
        action1.marketoGUID = 'testc1';
        actions.add(action1);
        poiActionUtility.poiAction action2 = new poiActionUtility.poiAction();
        action2.primaryAttributeValueId = '18020';
        action2.sfId = l.id;
        action2.marketoGUID = 'testl1';
        actions.add(action2);
         poiActionUtility.poiAction action3 = new poiActionUtility.poiAction();
        action3.primaryAttributeValueId = '18254';
        action3.sfId = l.id;
        action3.marketoGUID = 'testc2';
        actions.add(action3);
        poiActionUtility.poiAction action4 = new poiActionUtility.poiAction();
        action4.primaryAttributeValueId = '18202';
        action4.sfId = c.id;
        action4.marketoGUID = 'testc3';
        actions.add(action4);
        poiActionUtility.poiAction action5 = new poiActionUtility.poiAction();
        action5.primaryAttributeValueId = '18202';
        action5.leadid = '12345';
        action5.marketoGUID = 'test1';
        actions.add(action5);
        POIActionInsert.createPOIActions(actions);
        
        test.stopTest();
    }
    
    public static Account testAccount() {
        return new Account(Name='Test', NAICS_Code__c='Test');
    }
    
    public static Contact testContact(id accountId) {
        return new Contact(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', AccountId=accountId);
    }
    
    public static Lead testLead() {
        return new Lead(FirstName='Test', LastName='Test', Company='Company1', Communication_Channel__c='Inbound Call');
    }
}