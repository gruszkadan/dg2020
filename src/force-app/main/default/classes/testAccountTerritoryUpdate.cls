@isTest()
private class testAccountTerritoryUpdate {
    @isTest(SeeAllData=true)
    public static void t1 ()
    {
        Territory__c t = new Territory__c ();
        t.Name = 'Test';
        t.Territory_Owner__c ='00580000003UChIAAW';
        insert t;
        
        County_Zip_Code__c cZ = new County_Zip_Code__c();
        cZ.Territory__c = t.Id;
        cz.City__c ='Streamwood';
        cz.State__c = 'IL';
        cz.Zip_Code__c = 'ABCDE';
        cz.County__c ='Cook';
        cz.Country__c ='United States';
        insert cZ;
        
        Account acct = new Account ();
        acct.Name = 'ATest';
        acct.BillingPostalCode = 'ABCDE';
        insert acct;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = acct.Id;
        insert newContact;
        
        acct = [SELECT id, OwnerId, Territory__c FROM Account WHERE id = :acct.id LIMIT 1];
        System.assertEquals (t.Id, acct.Territory__c);
        System.assertEquals ('00580000003UChIAAW', acct.OwnerID);
    }
    @isTest(SeeAllData=true)
    public static void t2 ()
    {
        Enterprise_Account_Assignments__c eaaN = new Enterprise_Account_Assignments__c ();
        eaaN.Name = '000';
        eaaN.Industry__c = 'Test industry';
        id kmiOwnerId = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        eaaN.EHS_OwnerId__c = kmiOwnerId;
        insert eaaN;
        
        Account acct = new Account ();
        acct.Name = 'ATest';
        acct.Enterprise_Sales__c = true;
        acct.Primary_NAICS_Code__c = '000';
        acct.Enterprise_Sales_Industry__c = 'Test industry';
        insert acct;
        
        acct = [SELECT id, OwnerId, EHS_Owner__c FROM Account WHERE id = :acct.id LIMIT 1];
        System.assertEquals (kmiOwnerId, acct.EHS_Owner__c);
    }
    @isTest(SeeAllData=true)
    public static void t3 ()
    {
        Territory__c t1 = new Territory__c ();
        t1.Name = 'Test';
        t1.Territory_Owner__c ='00580000003UChIAAW';
        t1.Ergonomics_Owner__c = '00580000003UChIAAW';
        insert t1;
        
        County_Zip_Code__c cZ1 = new County_Zip_Code__c();
        cZ1.Territory__c = t1.Id;
        cz1.City__c ='Oakville';
        cz1.State__c = 'ON';
        cz1.Zip_Code__c = '456';
        cz1.County__c ='Hamilton';
        cz1.Country__c ='Canada';
        insert cZ1;
        
        Account acct1 = new Account ();
        acct1.Name = 'ATest';
        acct1.BillingPostalCode = '456 987';
        acct1.BillingCountry ='Canada';
        insert acct1;
        
        acct1 = [SELECT id, OwnerId, Ergonomics_Account_Owner__c, Territory__c FROM Account WHERE id = :acct1.id LIMIT 1];
        System.assertEquals (t1.Id, acct1.Territory__c);
        System.assertEquals ('00580000003UChIAAW', acct1.OwnerID);
        System.assertEquals ('00580000003UChIAAW', acct1.Ergonomics_Account_Owner__c);
    }
    @isTest(SeeAllData=true)
    public static void t4 ()
    {
        Territory__c t3 = new Territory__c ();
        t3.Name = 'Test';
        t3.Territory_Owner__c ='00580000003UChIAAW';
        insert t3;
        
        County_Zip_Code__c cZ2 = new County_Zip_Code__c();
        cZ2.Territory__c = t3.Id;
        cz2.City__c ='Streamwood';
        cz2.State__c = 'IL';
        cz2.Zip_Code__c = 'ABCDE';
        cz2.County__c ='Cook';
        cz2.Country__c ='United States';
        insert cZ2;
        
        Account acct2 = new Account ();
        acct2.Name = 'ATest';
        insert acct2;
        
        Contact newContact1 = new Contact();
        newContact1.FirstName = 'Bob';
        newContact1.LastName = 'Bob';
        newContact1.AccountID = acct2.Id;
        insert newContact1;
        
        
        if(acct2.Id != NULL){
            acct2.BillingPostalCode ='ABCDE';
            update acct2;
        }
        newContact1 = [SELECT id, OwnerId, Territory__c FROM Contact WHERE id = :newContact1.id LIMIT 1];
        System.assertEquals (t3.Id, newContact1.Territory__c);
    }
    @isTest(SeeAllData=true)
    public static void t5 ()
    {
        Territory__c t4 = new Territory__c ();
        t4.Name = 'Test';
        t4.Territory_Owner__c ='00580000003UChIAAW';
        insert t4;
        
        County_Zip_Code__c cZ4 = new County_Zip_Code__c();
        cZ4.Territory__c = t4.Id;
        cz4.City__c ='Oakville';
        cz4.State__c = 'ON';
        cz4.Zip_Code__c = '456 123';
        cz4.County__c ='Hamilton';
        cz4.Country__c ='Canada';
        insert cZ4;
        
        Territory__c t41 = new Territory__c ();
        t41.Name = 'Test 41';
        t41.Territory_Owner__c ='00580000003UChIAAW';
        insert t41;
        
        County_Zip_Code__c cZ41 = new County_Zip_Code__c();
        cZ41.Territory__c = t41.Id;
        cZ41.City__c ='Oakville';
        cZ41.State__c = 'ON';
        cZ41.Zip_Code__c = '456';
        cZ41.County__c ='Hamilton';
        cZ41.Country__c ='Canada';
        insert cZ41;
        
        Account acct4 = new Account ();
        acct4.Name = 'ATest';
        acct4.BillingPostalCode = '456 987';
        acct4.BillingCountry ='Canada';
        insert acct4;
        
        acct4 = [SELECT id, OwnerId, Territory__c FROM Account WHERE id = :acct4.id LIMIT 1];
        System.assertEquals (t41.Id, acct4.Territory__c);
    }
    @isTest(SeeAllData=true)
    public static void t6 ()
    {
        Territory__c t5 = new Territory__c ();
        t5.Name = 'Test';
        t5.Territory_Owner__c ='00580000003UChIAAW';
        insert t5;
        
        County_Zip_Code__c cZ5 = new County_Zip_Code__c();
        cZ5.Territory__c = t5.Id;
        cz5.City__c ='Oakville';
        cz5.State__c = 'ON';
        cz5.Zip_Code__c = '456 123';
        cz5.County__c ='Hamilton';
        cz5.Country__c ='Canada';
        insert cZ5;
        
        Territory__c t51 = new Territory__c ();
        t51.Name = 'Test 51';
        t51.Territory_Owner__c ='00580000003UChIAAW';
        insert t51;
        
        County_Zip_Code__c cZ51 = new County_Zip_Code__c();
        cZ51.Territory__c = t51.Id;
        cZ51.City__c ='Oakville';
        cZ51.State__c = 'ON';
        cZ51.Zip_Code__c = '456';
        cZ51.County__c ='Hamilton';
        cZ51.Country__c ='Canada';
        insert cZ51;
        
        Account acct5 = new Account ();
        acct5.Name = 'ATest';
        acct5.Customer_Status__c = 'Out of Business';
        acct5.BillingPostalCode = '456 123';
        acct5.BillingCountry ='Canada';
        acct5.Num_of_SDS__c = 100;
        acct5.Num_of_Employees__c = 100;
        insert acct5;
        
        acct5 = [SELECT id, OwnerId, Territory__c FROM Account WHERE id = :acct5.id LIMIT 1];
        System.assertEquals (t5.Id, acct5.Territory__c);
        
        id autosim = [SELECT id FROM User WHERE Alias = 'autosim' LIMIT 1].id;
        acct5.OwnerId = autosim;
        update acct5;
        acct5 = [SELECT id, OwnerId, Territory__c FROM Account WHERE id = :acct5.id LIMIT 1];
        system.assert(acct5.OwnerId == autosim);
        
        acct5.Customer_Status__c = 'Active';
        update acct5;
        acct5 = [SELECT id, OwnerId, Territory__c FROM Account WHERE id = :acct5.id LIMIT 1];
        system.assert(acct5.OwnerId == t51.Territory_Owner__c);
        
    } 
    
    
    static testmethod void ergonomicsOwnershipAssignment() {
        //Test class method to cover Ergonomics Owner Assignment in ownershipUtility   
        user EJ = [Select id, name from user where name = 'Eric Jackson']; 
        User ErgoOwner1 = [Select id, name from user where name = 'Nick Lipnisky' and isactive = true limit 1];  
        
           //Create and set lead custom setting for Eric Jackson and make him lead owner
        Lead_Custom_Settings__c LC = Lead_Custom_Settings__c.getOrgDefaults();
        lc.SetupOwnerId = EJ.id;
        lc.Can_Change_Authoring_Owner__c = true;
        lc.Can_Change_Chemical_Mgt_Owner__c = true;
        lc.Can_Change_EHS_Owner__c = true;
        lc.Can_Change_Ergonomics_Owner__c = true;
        lc.Can_Change_Online_Training_Owner__c = true;
        lc.Can_Change_Secondary_Lead_Owner__c = true;
        lc.Can_Edit_Ergonomics_Secondary_Lead_Owner__c = true;
        lc.Default_Authoring_Owner__c = true;
        lc.Default_Chemical_Management_Owner__c = true;
        lc.Default_EHS_Owner__c = true;
        lc.Default_Ergo_Owner__c = true;
        lc.Default_International_Lead_Owner__c = true;
        lc.Default_Online_Training_Owner__c = true;
        lc.Enterprise_EHS_Owner__c = true;
        lc.Special_Country_EHS_Owner__c = true;
        
        insert lc;
        
        account a = new account();
        a.Name = 'NLU';
        a.Ergonomics_Account_Owner__c = ErgoOwner1.id; 
        
        insert a;
        
        contact c = new contact();
        c.AccountId = a.id;
        c.FirstName = 'Big';
        c.LastName = 'Randy';
        c.Email = 'test@mac.com';
        
        insert c;
        
        lead l1 = new lead();
        l1.Company = a.Name;
        l1.LastName = 'Leader';
        l1.Communication_Channel__c = 'Cold Call';
        l1.OwnerId = ej.id;
        
        insert l1;
        
        lead lead1 = [select id, Ergonomics_Owner__c from lead where id =: l1.id];
        
        system.assertEquals(a.Ergonomics_Account_Owner__c, lead1.Ergonomics_Owner__c);
        
    }
}