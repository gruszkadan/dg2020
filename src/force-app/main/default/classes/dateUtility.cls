public class dateUtility {
    
    
    
    public static integer numberOfBusinessDays(date firstDay, date lastDay, List<Holiday> holidays){
        
        if(holidays == NULL || holidays.size() == 0){
        	holidays = [SELECT ActivityDate FROM Holiday];
        }
       
        Set<Date> holidaysSet = new Set<Date> ();
        for (Holiday currHoliday : holidays)
        {
            holidaysSet.add(currHoliday.ActivityDate);
        }
        Integer busDays = 0;
        for (integer i = 0; i < math.abs(firstDay.daysBetween(lastDay)); i++)
        {
            Date dt;
            if(lastDay < firstDay){
                dt = firstDay - i;
            }else{
                dt = firstDay + i;
            }
            DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());
            String todayDay = currDate.format('EEEE');
            if (todayDay != 'Saturday' && todayDay != 'Sunday' && (!holidaysSet.contains(dt)))
            {
                if(lastDay < firstDay){
                    busDays = busDays - 1;
                }else{
                    busDays = busDays + 1; 
                }
            }
        }
        return busDays;
    }
    
    
    
    public static date calculateBusinessDay(date firstDay, integer busDays, List<Holiday> holidays) {
        
        if(holidays == NULL || holidays.size() == 0){
            holidays = [SELECT ActivityDate FROM Holiday];
        }
        Set<Date> holidaysSet = new Set<Date> ();
        for (Holiday currHoliday : holidays)
        {
            holidaysSet.add(currHoliday.ActivityDate);
        }
        integer ABSOLUTEbusDays = math.abs(busDays);
        Date endDate = firstDay;
        while(ABSOLUTEbusDays > 0){
            Date tomorrowsDay;
            if(busDays < 0){
                tomorrowsDay = endDate.addDays(-1);
            }else{
                tomorrowsDay = endDate.addDays(1);
            }
            
            DateTime endDateTime = DateTime.newInstance(tomorrowsDay.year(), tomorrowsDay.month(), tomorrowsDay.day());
            String endDateString = endDateTime.format('EEEE');
            if(endDateString != 'Saturday' && endDateString != 'Sunday'){
                if(!holidaysSet.contains(tomorrowsDay)){
                    ABSOLUTEbusDays = ABSOLUTEbusDays - 1;
                }
            }
            if(busDays < 0){
                endDate = endDate.addDays(-1);
            }else{
                endDate = endDate.addDays(1);
            }
        }
        return endDate;
    }
    

    public static string MonthName(integer i, boolean abbreviateIt){
        string month = '';
        switch on i {
            when 1 {
                month = 'January';                
            } when 2 {
                month = 'February';                
            } when 3 {
                month = 'March';                
            } when 4 {
                month = 'April';                
            } when 5 {
                month = 'May';                
            } when 6 {
                month = 'June';                
            } when 7 {
                month = 'July';                
            } when 8 {
                month = 'August';
            } when 9 {
                month = 'September';
            } when 10 {
                month = 'October';
            } when 11 {                
                month = 'November';
            } when 12 {
                month = 'December';
            }
        }
        if(abbreviateIt && month != 'May' && month != ''){
            if(month == 'September'){
                month = month.Left(4);
            }else{
                month = month.Left(3); 
            }
        }
        
        return month;
    }
    
    
}