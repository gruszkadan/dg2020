@isTest(seeAllData=true)
private class testUltimateParentUpdater {
    
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test1() {
        
        Account parent = new Account(Name = 'Parent', Is_Ultimate_Parent__c = true);
        insert parent;
        
        Account child1 = new Account(Name = 'Child 1', ParentId = parent.id, Ultimate_Parent__c = parent.id);
        insert child1;
        
        Account child2 = new Account(Name = 'Child 2', ParentId = child1.id, Ultimate_Parent__c = parent.id);
        insert child2;
        
        Account child3 = new Account(Name = 'Child 3', ParentId = child2.id, Ultimate_Parent__c = parent.id);
        insert child3;
        
        Account child4 = new Account(Name = 'Child 4', ParentId = child3.id);
        insert child4;
        
        Account child5 = new Account(Name = 'Child 5', ParentId = child4.id);
        insert child5;
        
        Account child6 = new Account(Name = 'Child 6', ParentId = child5.id);
        insert child6;
        
        Account child7 = new Account(Name = 'Child 7', ParentId = child6.id);
        insert child7;
        
        Account child8 = new Account(Name = 'Child 8', Ultimate_Parent__c = parent.id);
        insert child8;
        
        Ultimate_Parent_Accounts__c upa1 = new Ultimate_Parent_Accounts__c(Name = 'upa1', AccountId__c = child4.id, Date__c = date.today());
        insert upa1;
        
        Ultimate_Parent_Accounts__c upa2 = new Ultimate_Parent_Accounts__c(Name = 'upa2', AccountId__c = child8.id, Date__c = date.today());
        insert upa2;
        
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new ultimateParentUpdater());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        
            
        
    }

}