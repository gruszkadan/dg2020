public class adminToolAPIJSONUtility {
    public static boolean adminToolErrors(string jsonStr, string logClass, string logMethod) {
        string errorMessage;
        boolean success;
        JSONParser parser = JSON.createParser(jsonStr);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'success')) {
                //Get the Value
                parser.nextToken();
                //Success Value
                success = parser.getBooleanValue();
                if(success == FALSE){
                    parser.nextToken();
                    parser.nextToken();
                    parser.nextToken();
                    parser.nextToken();
                    parser.nextToken();
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'data')) {
                        parser.nextToken();
                        while (parser.nextToken() != null) {
                            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'ExceptionMessage')){
                                parser.nextToken();
                                errorMessage = parser.getText();
                            }
                        }
                    }
                    salesforceLog.createLog('Admin Tool API',TRUE,logClass,logMethod,errorMessage);
                }
            }
        }
        return success;
    }
    
    public class customerData {
		public String AddressLine1;
		public String AddressLine2;
		public Decimal AdminsPurchased;
		public String AID;
		public Decimal AnnualRevenue;
		public Boolean AtRisk;
		public String AtRiskStatus;
		public Boolean BillingException;
		public String BillingExceptionNotes;
		public String ChannelCode;
		public String City;
		public String ContractStartDate;
		public DateTime ContractTermEndDate;
		public String ContractTermLength;
		public String Country;
		public String AccountID;
		public String CustomerCompanyAlias;
		public String CustomerCompanyName;
		public String PersonID;
		public String CustomerStatus;
		public String GPO1;
		public String GPO2;
		public Decimal MSDSCount;
		public String NAICS;
		public String Notes;
		public String Parent;
		public String Phone;
		public String Province;
		public String Reference;
		public String ReferenceInactiveDate;
		public String ReferenceNotes;
        public String RetentionOwner;
        public String State;
        public String URL;
        public String UserSiteAdminsCount;
        public Boolean VIP;
        public String ZipCode;
        public string ScheduledUpgradeDate;
        public string ActualUpgradeDate;
        public string UpradeStatus;
        public boolean ExcludeFromUpgrade;
    }
    
    public class userData {
		public Integer AccountID {get;set;} 
		public Integer CustomerUserID {get;set;} 
		public String Description {get;set;} 
		public String Email {get;set;} 
		public String FirstName {get;set;} 
		public String LastName {get;set;} 
		public String Phone {get;set;} 
		public String Status {get;set;} 
        public Boolean PrimaryAccountAdministrator {get;set;}
    }
    
    public class bookingsData {
		public String ContractNumber {get;set;} 
		public Integer ContractTerm {get;set;} 
		public Integer AccountID {get;set;} 
		public String CustomerCompanyName {get;set;} 
		public String Implementation {get;set;} 
		public Decimal InvoiceAmount {get;set;} 
		public String Month {get;set;} 
		public String NewOrRenewal {get;set;} 
		public DateTime OrderDate {get;set;} 
		public Integer OrderID {get;set;} 
		public Integer OrderItemID {get;set;} 
		public String ProductName {get;set;} 
		public String SalesRep {get;set;} 
		public String SalesLocation {get;set;} 
		public String Status {get;set;} 
		public String Subscription {get;set;} 
		public DateTime TermEndDate {get;set;} 
		public Integer TermLength {get;set;} 
		public DateTime TermStartDate {get;set;} 
        public String Type {get;set;}
        public Integer Year {get;set;}
        public Decimal RenewalPrice {get;set;}
        public String ProductPlatform {get;set;}
        public String Version {get;set;}
        public DateTime ContractTermEndDate {get;set;} 
        public Boolean DoNotRenew {get;set;}
        public Boolean UniqueMSA {get;set;}
        public Boolean AuthorizedForCommercialUse {get;set;}
        public Boolean PublicFacingHQ {get;set;}
    }
    
    public class accountID {
        public Integer AccountID;
        public string SalesforceID;
    }
    
    public class dfuExport {
        public string AccountID;
        public string URL;
        public string Status;
        public boolean OptOut;
        public string CaseOwner;
        public string CloseDate;
    }
}