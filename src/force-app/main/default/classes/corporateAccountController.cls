public with sharing class corporateAccountController {
    private Apexpages.StandardController controller; 
    public Account a;
    public User u {get;set;}
    public EmailTemplate corpAccountsReferralEmail {get;set;}
    datetime myDateTime = datetime.now();
    public String newRef {get;set;}
    
    public corporateAccountController(ApexPages.StandardController stdController) {
        Id accountId = ApexPages.currentPage().getParameters().get('id');
       
        this.controller = stdController;
        if ( accountId != null ) {
            this.a = [SELECT Id, Name, OwnerID, Owner.Sales_Director__c, Corporate_Account_Status__c,
                      Corporate_Account_Rejection_Reason__c, Corporate_Referral_Status__c From Account WHERE Id = :accountId];
        }
        u = [Select id, Name, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, ProfileID from User where id =: UserInfo.getUserId() LIMIT 1];

    }
    
    //Runs when Manager/ Director Approves the Corporate Account
    public void corpAccountApproval () {
        if ( this.a != null ) {
            try {
                Account corpAcct = [select Id, Corporate_Account_Status__c, Corporate_Account__c, Corporate_Account_Rejection_Reason__c from Account where Id=:this.a.Id];
                corpAcct.Corporate_Account__c = true;
                corpAcct.Corporate_Account_Status__c = 'Approved';
                corpAcct.Corporate_Account_Checked_By__c = u.Name;
                corpAcct.Corporate_Account_Review_Date__c = date.TODAY();
                corpAcct.Corporate_Account_Rejection_Reason__c = NULL;
                update corpAcct;
            }
            catch(Exception e){
                ApexPages.addMessages(e);
            }
        } 
    }
    
   
    
  
    
    //Runs when Manager/ Director Rejects the Corporate Account
    public void corpAccountReject () {
        if ( this.a != null ) {
            try {
                Account corpAcct = [select Id, Corporate_Account_Status__c, Corporate_Account__c from Account where Id=:this.a.Id];
                corpAcct.Corporate_Account_Status__c = 'Rejected';
                corpAcct.Corporate_Account_Checked_By__c = u.Name;
                corpAcct.Corporate_Account_Review_Date__c = mydatetime;
                update corpAcct;
            }
            catch(Exception e){
                ApexPages.addMessages(e);
            }
        } 
    }
    
    
    
   
   
    
    //Button Action when a Manager/ Director Approves the Corporate Account
    public PageReference approveCorpAccount() {
        this.controller.save();
        corpAccountApproval();
        PageReference account_detail = Page.account_detail;
        account_detail.setRedirect(true);
        account_detail.getParameters().put('id',a.ID); 
        return null; 
    }
    
    //Button Action when a Manager/ Director Rejects the Corporate Account
    public PageReference rejectCorpAccount() {
        this.controller.save();
        corpAccountReject();
        PageReference account_detail = Page.account_detail;
        account_detail.setRedirect(true);
        account_detail.getParameters().put('id',a.ID); 
        return null; 
    }   
    
   
    
    //Button Action when a User Clicks the "View Account Button" when approving the corporate account
    public PageReference reload(){
        PageReference reload = Page.account_corp_approval;
        reload.setRedirect(true);
        reload.getParameters().put('ID',a.Id);
        return reload;
    }
}