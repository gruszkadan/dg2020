public class sfNoteTriggerHandler {
    
    // AFTER INSERT
    public static void afterInsert(Salesforce_Request_Note__c[] oldSfNotes, Salesforce_Request_Note__c[] newSfNotes) {
        
        Salesforce_Request_Note__c[] reqNotes = new list<Salesforce_Request_Note__c>();
        
        for(integer i=0; i<newSfNotes.size(); i++){
            if(newSfNotes[i].Jira_Comment__c != true){
                reqNotes.add(newSfNotes[i]);
            }
        }
        
        if(reqNotes.size() > 0){
            sfRequestUtility.latestNote(reqNotes);
        }
        
    }
    
}