public class TSWtestComponetController {
    public string AccId {get;set;}
    
  public TSWtestComponetController(){
    AccId ='0011g00000Ls5yp';
  }
    
    /*
     * <c:contactRelatedList objectName="Contact" 
                              fieldsCSV="Name,Salutation,Referred_By_Name__c,Authoring_Contact__c,LDR_Contact__c,Pronunciation__c,Referred_By__c,Lead_Type__c,Title,Email,Phone,MobilePhone,Direct_Line__c,Billing_Contact__c,Primary_Contact__c,Primary_Admin__c,Role__c,Status__c,Unsubscribe_All__c,Opt_In_Sales_Consulting__c,AccountId,ODT_Contact__c,Ergonomics_Contact__c,EHS_Contact__c" 
                              recordLimit="50" parentFieldId="{!Account.Id}" pageSize="7" parentFieldName="AccountID" orderByFieldName="primary_contact__c DESC, Primary_admin__c DESC, Billing_Contact__c DESC, Lastname" 
                              sortDirection="asc" hideButtons="true" urlForNewRecord="{!URLFOR($Action.Opportunity.New,null)}"/>
*/
    
    @RemoteAction
    public static List<Contact> showContacts(String accName){
        system.debug('inside method: '+accName);
        accName = '%'+ accName+'%';
        List<Contact> lst_contacts = new List<Contact>([select id,Name,Salutation,Referred_By_Name__c,Authoring_Contact__c,LDR_Contact__c,Pronunciation__c,Referred_By__c,
                                                        Lead_Type__c,Title,Email,Phone,MobilePhone,Direct_Line__c,Billing_Contact__c,Primary_Contact__c,Primary_Admin__c,
                                                        Role__c,Status__c,Unsubscribe_All__c,Opt_In_Sales_Consulting__c,AccountId,ODT_Contact__c,Ergonomics_Contact__c,EHS_Contact__c 
                                                        from contact 
                                                        where Account.Name LIKE : accName]);
        return lst_contacts;

    }
    
    
        @RemoteAction
    public static List<Contact> getSpecificContacts(id accId){
        system.debug('inside method');
        List<Contact> lst_contacts = new List<Contact>([select id,Name,Salutation,Referred_By_Name__c,Authoring_Contact__c,LDR_Contact__c,Pronunciation__c,Referred_By__c,Lead_Type__c,Title,Email,Phone,MobilePhone,Direct_Line__c,Billing_Contact__c,Primary_Contact__c,Primary_Admin__c,Role__c,Status__c,Unsubscribe_All__c,Opt_In_Sales_Consulting__c,AccountId,ODT_Contact__c,Ergonomics_Contact__c,EHS_Contact__c 
                                                        from contact 
                                                        where Account.Id =: accId]);
        return lst_contacts;

    }
    

}