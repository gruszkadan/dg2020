trigger eventTrigger on Event 
(before insert, before update, before delete, after insert, after update, after delete, after undelete) { 
    
    if (trigger.isBefore) {
        
        // BEFORE INSERT
        if (trigger.isInsert) {
            eventTriggerHandler.beforeInsert(trigger.new);
        }
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            eventTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
        
        // BEFORE DELETE
        if (trigger.isDelete) {
            eventTriggerHandler.beforeDelete(trigger.old, trigger.new);
        }
        
    }
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            eventTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            eventTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
        // AFTER DELETE
        if (trigger.isDelete) {
            eventTriggerHandler.afterDelete(trigger.old, trigger.new);
        }
        
        // AFTER UNDELETE
        if (trigger.isUndelete) {
            eventTriggerHandler.afterUndelete(trigger.old, trigger.new);
        }
        
    }  
    
    if(!test.isRunningTest()){
        dlrs.RollupService.triggerHandler(); 
    }
}