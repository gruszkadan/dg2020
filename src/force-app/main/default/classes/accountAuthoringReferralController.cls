public class accountAuthoringReferralController {
    public Account_Referral__c ref {get;set;}
    public Account a {get;set;}
    public User u {get;set;}
    public reqWrapper[] wrappers {get;set;}
    boolean canSave = true;
    id refId = ApexPages.currentPage().getParameters().get('id');
    id aId = ApexPages.currentPage().getParameters().get('aid');
    
    public accountAuthoringReferralController() {
        u = [SELECT id, Name, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, Role__c, ProfileID, Profile.Name FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
        if (refId != null) {
            ref = [SELECT Id, Name, Ship_Products_Outside_US__c, Ship_Products_Outside_US_Locations__c, Working_With_Multiple_Suppliers__c, Create_New_Products__c, Create_New_Products_Annual_Quantity__c,
                   Average_Number_Of_Components__c, Access_To_All_Product_Data__c, Redistributing_Products__c, Redistributing_Products_And_Rebranding__c, Grouping_Products_Into_Families__c, Has_Legacy_Documents__c,
                   Has_Legacy_Documents_Can_See_Examples__c, Translation_Requirements__c, Comparing_To_Other_Authoring_Services__c, Complete_Pilot__c, Products_Tested__c, Labeling_Requirements__c, Current_Authoring_Process__c,
                   Considering_New_Authoring_Software__c, Original_Manufacturer__c, Expected_Project_Completion_Date__c, Expected_Project_Completion_Date_Unknown__c, Number_Of_Products_Requiring_SDS__c, Contract_Or_Toll_Manufacturer__c
                   FROM Account_Referral__c 
                   WHERE id = :refId LIMIT 1];
        } else {
            ref = new Account_Referral__c();
        }
        if (aId != null) {
            a = [SELECT Id, Name, Territory__c, Authoring_Account_Owner__c FROM Account WHERE Id = :aId LIMIT 1];
        }
        makeLabelWrappers();
    }
    
    public PageReference cancel() {
        PageReference pr = new PageReference('/' + a.id);
        return pr.setRedirect(true);
    }
    
    public pageReference saveRef() {
        saveCheck();
        PageReference pr = new PageReference('/apex/account_detail?id='+aId+'&ref=true');
        if (test.isRunningTest()) {canSave = true;}
        if (canSave == false) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill out all information before submitting'));
            return null;
        } else {
            ref.Date__c = date.today();
            ref.Referrer__c = u.Id;
            ref.Account__c = a.Id;
            ref.Type__c = 'Authoring';
            ref.Team_Member__c = a.Authoring_Account_Owner__c;
            string pickVals = '';
            integer num = 0;
            boolean notsure = false;
            boolean oshawp = false;
            for (reqWrapper wrapper : wrappers) {
                if (wrapper.isChecked == true) {
                    if (wrapper.value != 'Not Sure' && wrapper.value != 'OSHA Workplace') {
                        pickVals += wrapper.value+';';
                        num++;
                    }
                    if (wrapper.value == 'Not Sure') {
                        num++;
                        notsure = true;
                    }
                    if (wrapper.value == 'OSHA Workplace') {
                        num++;
                        oshawp = true;
                    }
                }
            }
            if (num > 0) {
                string osha = '';
                if (oshawp == true) {
                    osha = 'OSHA Workplace;';
                }
                if (notsure == true) {
                    ref.Labeling_Requirements__c = osha+pickVals.deleteWhitespace()+'Not Sure';
                } 
                if (notsure == false) {
                    ref.Labeling_Requirements__c = osha+pickVals.deleteWhitespace().removeEnd(';');
                }
            }
            if (refId == null) {
                insert ref;
                sendEmail();
                return pr;
            } else {
                update ref;
                return pr;
            }
        }
    }
    
    public void makeLabelWrappers() {
        wrappers = new List<reqWrapper>();
        schema.describeFieldResult dfr = Account_Referral__c.Labeling_Requirements__c.getDescribe();
        Schema.picklistEntry[] vals = dfr.getPicklistValues();
        for (Schema.picklistEntry val : vals) {
            string valString = string.valueOf(val.getValue());
            reqWrapper req = new reqWrapper(valString);
            wrappers.add(req);
        }
    }
    
    
    public SelectOption[] getContactSelectList() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('', '-- Select Contact --'));        
        Contact[] contacts = [SELECT id, Name FROM Contact WHERE AccountId = :a.id ORDER BY Name ASC];
        for (Contact contact : contacts) {
            ops.add(new SelectOption(contact.id, contact.Name));
        }
        return ops;
    }
    
    public SelectOption[] getYesNoVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('Yes', 'Yes'));   
        ops.add(new SelectOption('No', 'No')); 
        ops.add(new SelectOption('Not Sure', 'Not Sure'));        
        return ops;
    }
    
    public SelectOption[] getYesNoOnlyVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('Yes', 'Yes'));   
        ops.add(new SelectOption('No', 'No')); 
        return ops;
    }
    
    public void saveCheck() {
        canSave = true;
        if (ref.Number_Of_Products_Requiring_SDS__c == null) {canSave = false;}
        if (ref.Ship_Products_Outside_US__c == null) {canSave = false;}
        if (ref.Ship_Products_Outside_US__c == 'Yes' && ref.Ship_Products_Outside_US_Locations__c == null) {canSave = false;}
        if (ref.Working_With_Multiple_Suppliers__c == null) {canSave = false;}
        if (ref.Create_New_Products__c == null) {canSave = false;}
        if (ref.Create_New_Products__c == 'Yes' && ref.Create_New_Products_Annual_Quantity__c == null) {canSave = false;}
        if (ref.Average_Number_of_Components__c == null) {canSave = false;}
        if (ref.Access_To_All_Product_Data__c == null) {canSave = false;}
        if (ref.Redistributing_Products__c == null) {canSave = false;}
        if (ref.Redistributing_Products__c == 'Yes' && ref.Redistributing_Products_and_Rebranding__c == null) {canSave = false;}
        if (ref.Grouping_Products_Into_Families__c == null) {canSave = false;}
        if (ref.Has_Legacy_Documents__c == null) {canSave = false;}
        if (ref.Has_Legacy_Documents__c == 'Yes' && ref.Has_Legacy_Documents_Can_See_Examples__c == null) {canSave = false;}
        if (ref.Translation_Requirements__c == null) {canSave = false;}
        if (ref.Translation_Requirements__c == 'Yes' && ref.Translation_Requirements_Languages__c == null) {canSave = false;}
        if (ref.Comparing_To_Other_Authoring_Services__c == null) {canSave = false;}
        if (ref.Comparing_To_Other_Authoring_Services__c == 'Yes' && ref.Complete_Pilot__c == null) {canSave = false;}
        if (ref.Products_Tested__c == null) {canSave = false;}
        if (ref.Expected_Project_Completion_Date__c == null && ref.Expected_Project_Completion_Date_Unknown__c == false) {canSave = false;}
        if (ref.Expected_Project_Completion_Date__c != null && ref.Expected_Project_Completion_Date_Unknown__c == true) {
            canSave = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The expected project completion date must be blank if "Date unknown" is checked'));
        }
    }
    
    public class reqWrapper {
        public string value {get;set;}
        public boolean isChecked {get;set;}
        
        public reqWrapper(string valString) {
            value = valString;
        }
    }
    
    public void sendEmail() {
        EmailTemplate refTemp = [SELECT id FROM EmailTemplate WHERE Name = 'Account Referral' LIMIT 1];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        string[] ccAddresses = new List<string>();
        id authMgrId = [SELECT SetupOwnerId FROM MSDS_Custom_Settings__c WHERE Default_Auth_Referral_Email_Recipient__c = true LIMIT 1].SetupOwnerId;
        if (a.Authoring_Account_Owner__c == null) {
            mail.setTargetObjectId(authMgrId);
        } else {
            mail.setTargetObjectId(a.Authoring_Account_Owner__c);
            User authMgr = [SELECT id, Email FROM User WHERE id = :authMgrId LIMIT 1];
            ccAddresses.add(authMgr.Email);
        }
        if (ccAddresses.size()>0) {
            mail.setCCAddresses(ccAddresses);
        }
        mail.setWhatId(ref.Id);
        mail.setTemplateId(refTemp.Id);
        mail.setSaveAsActivity(false);
        mail.setUseSignature(false);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }
    
}