public class poiStageStatusQueueableStep2 implements Queueable {
    
    //this string holds the results we want to email ourselves
    string results = '<b>NIGHTLY POI STAGE/STATUS SCHEDULER RESULTS PART 2</b><br/><br/>';
    
    public void execute(QueueableContext context) {
        
        //check for actions needing the stage set to "Sales Inactive"
        stage_salesInactive();
        
        email(results);
    }
    
    // When days since last activity hits 90 and no open tasks, set to 'Sales Inactive' if the stage is already 'Sales Accepted', 'Sales Engaged'
    public void stage_salesInactive() {
        sObject[] updList = new list<sObject>();
        Contact[] cList = new list<Contact>();
        Lead[] lList = new list <Lead>();
        
        // Create our list of sObjects, query contacts/leads and add to list
        sObject[] persons = new list<sObject>();
        persons.addAll(queryContacts('Contact'));
        persons.addAll(queryLeads('Lead'));
        
        string stages = 'Sales Accepted;Sales Engaged';
        
        // Loop through our persons
        if (persons.size() > 0) {
            
            // Fields and lists
            sObjectField stageMSDS;
            sObjectField stageEHS;
            sObjectField stageAUTH;
            sObjectField stageODT;
            sObjectField stageERGO;
            sObjectField activityDateMSDS;
            sObjectField activityDateEHS;
            sObjectField activityDateAUTH;
            sObjectField activityDateODT;
            sObjectField activityDateERGO;
            sObjectField createdDate;
            Product_Of_Interest_Action__c[] actions;
            Task[] tasks;
            
            // Loop through our persons. Figure out which object it is and assign the fields and lists
            persons.sort();
            for (sObject p : persons) {
                boolean upd = false;
                
                // Booleans to determine if suite sessions are open
                boolean openMSDS = false;
                boolean openEHS = false;
                boolean openAUTH = false;
                boolean openODT = false;
                boolean openERGO = false;
                
                // Determine the object type and set the lists
                string prefix = string.valueOf(p.id).left(3);
                if (prefix == '003') {
                    Contact c = (Contact)p;
                    stageMSDS = Contact.POI_Stage_MSDS__c;
                    stageEHS = Contact.POI_Stage_EHS__c;
                    stageAUTH = Contact.POI_Stage_AUTH__c;
                    stageODT = Contact.POI_Stage_ODT__c;
                    stageERGO = Contact.POI_Stage_ERGO__c;
                    activityDateMSDS = Contact.Last_Activity_Date_Chemical_Management__c;
                    activityDateEHS = Contact.Last_Activity_Date_EHS__c;
                    activityDateAUTH = Contact.Last_Activity_Date_Authoring__c;
                    activityDateODT = Contact.Last_Activity_Date_Online_Training__c;
                    activityDateERGO = Contact.Last_Activity_Date_Ergonomics__c;
                    createdDate = Contact.CreatedDate;
                    actions = c.Product_Of_Interest_Actions__r;
                    tasks = c.Tasks;
                } else {
                    Lead l = (Lead)p;
                    stageMSDS = Lead.POI_Stage_MSDS__c;
                    stageEHS = Lead.POI_Stage_EHS__c;
                    stageAUTH = Lead.POI_Stage_AUTH__c;
                    stageODT = Lead.POI_Stage_ODT__c;
                    stageERGO = Lead.POI_Stage_ERGO__c;
                    activityDateMSDS = Lead.Last_Activity_Date_Chemical_Management__c;
                    activityDateEHS = Lead.Last_Activity_Date_EHS__c;
                    activityDateAUTH = Lead.Last_Activity_Date_Authoring__c;
                    activityDateODT = Lead.Last_Activity_Date_Online_Training__c;
                    activityDateERGO = Lead.Last_Activity_Date_Ergonomics__c;
                    createdDate = Lead.CreatedDate;
                    actions = l.Product_Of_Interest_Actions__r;
                    tasks = l.Tasks;
                }
                
                // If actions are found, we need to determine which suites are open
                if (actions.size() > 0) {
                    for (Product_Of_Interest_Action__c a : actions) {
                        if (a.Product_Suite__c == 'MSDS Management') {
                            openMSDS = true;
                        } else if (a.Product_Suite__c == 'EHS Management') {
                            openEHS = true;
                        } else if (a.Product_Suite__c == 'MSDS Authoring') {
                            openAUTH = true;
                        } else if (a.Product_Suite__c == 'On-Demand Training') {
                            openODT = true;
                        } else if (a.Product_Suite__c == 'Ergonomics') {
                            openERGO = true;
                        }
                    }
                }
                
                // MSDS
                // If there are no open suite sessions more than 90 days old
                if (!openMSDS) {
                    // If the POI Stage is 'Sales Accepted' or 'Sales Engaged'
                    if (stages.contains((string)p.get(stageMSDS))) {
                        // Check for open tasks for that suite
                        boolean openTask = false;
                        boolean canUpd = false;
                        if (tasks.size() > 0) {
                            for (Task t : tasks) {
                                if (t.Suite_Of_Interest__c == 'MSDS Management') {
                                    openTask = true;
                                }
                            }
                        }
                        // If no open tasks, continue on
                        if (!openTask) {
                            // Next, check the last activity date
                            if ((date)p.get(activityDateMSDS) != null) {
                                // Make sure the date is more than 90 days old
                                if ((date)p.get(activityDateMSDS) < date.today().addDays(-90)) {
                                    // The stage was 'Sales Accepted' or 'Sales Engaged'
                                    // There are no open tasks
                                    // The last activity date was greater than 90 days ago
                                    canUpd = true;
                                }
                            } else {
                                // If there is no last activity date, we need to look at the created date and see if it's 90+ days old
                                if ((dateTime)p.get(createdDate) < date.today().addDays(-90)) {
                                    // The stage was 'Sales Accepted' or 'Sales Engaged'
                                    // There are no open tasks
                                    // The created date was greater than 90 days ago
                                    canUpd = true;
                                }
                            }
                        }
                        if (canUpd) {
                            p.put(stageMSDS, 'Sales Inactive');
                            upd = true;
                        }
                    }
                }
                
                // EHS
                // If there are no open suite sessions more than 90 days old
                if (!openEHS) {
                    // If the POI Stage is 'Sales Accepted' or 'Sales Engaged'
                    if (stages.contains((string)p.get(stageEHS))) {
                        // Check for open tasks for that suite
                        boolean openTask = false;
                        boolean canUpd = false;
                        if (tasks.size() > 0) {
                            for (Task t : tasks) {
                                if (t.Suite_Of_Interest__c == 'EHS Management') {
                                    openTask = true;
                                }
                            }
                        }
                        // If no open tasks, continue on
                        if (!openTask) {
                            // Next, check the last activity date
                            if ((date)p.get(activityDateEHS) != null) {
                                // Make sure the date is more than 90 days old
                                if ((date)p.get(activityDateEHS) < date.today().addDays(-90)) {
                                    // The stage was 'Sales Accepted' or 'Sales Engaged'
                                    // There are no open tasks
                                    // The last activity date was greater than 90 days ago
                                    canUpd = true;
                                }
                            } else {
                                // If there is no last activity date, we need to look at the created date and see if it's 90+ days old
                                if ((dateTime)p.get(createdDate) < date.today().addDays(-90)) {
                                    // The stage was 'Sales Accepted' or 'Sales Engaged'
                                    // There are no open tasks
                                    // The created date was greater than 90 days ago
                                    canUpd = true;
                                }
                            }
                        }
                        if (canUpd) {
                            p.put(stageEHS, 'Sales Inactive');
                            upd = true;
                        }
                    }
                }
                
                // AUTH
                // If there are no open suite sessions more than 90 days old
                if (!openAUTH) {
                    // If the POI Stage is 'Sales Accepted' or 'Sales Engaged'
                    if (stages.contains((string)p.get(stageAUTH))) {
                        // Check for open tasks for that suite
                        boolean openTask = false;
                        boolean canUpd = false;
                        if (tasks.size() > 0) {
                            for (Task t : tasks) {
                                if (t.Suite_Of_Interest__c == 'MSDS Authoring') {
                                    openTask = true;
                                }
                            }
                        }
                        // If no open tasks, continue on
                        if (!openTask) {
                            // Next, check the last activity date
                            if ((date)p.get(activityDateAUTH) != null) {
                                // Make sure the date is more than 90 days old
                                if ((date)p.get(activityDateAUTH) < date.today().addDays(-90)) {
                                    // The stage was 'Sales Accepted' or 'Sales Engaged'
                                    // There are no open tasks
                                    // The last activity date was greater than 90 days ago
                                    canUpd = true;
                                }
                            } else {
                                // If there is no last activity date, we need to look at the created date and see if it's 90+ days old
                                if ((dateTime)p.get(createdDate) < date.today().addDays(-90)) {
                                    // The stage was 'Sales Accepted' or 'Sales Engaged'
                                    // There are no open tasks
                                    // The created date was greater than 90 days ago
                                    canUpd = true;
                                }
                            }
                        }
                        if (canUpd) {
                            p.put(stageAUTH, 'Sales Inactive');
                            upd = true;
                        }
                    }
                }
                
                // ODT
                // If there are no open suite sessions more than 90 days old
                if (!openODT) {
                    // If the POI Stage is 'Sales Accepted' or 'Sales Engaged'
                    if (stages.contains((string)p.get(stageODT))) {
                        // Check for open tasks for that suite
                        boolean openTask = false;
                        boolean canUpd = false;
                        if (tasks.size() > 0) {
                            for (Task t : tasks) {
                                if (t.Suite_Of_Interest__c == 'On-Demand Training') {
                                    openTask = true;
                                }
                            }
                        }
                        // If no open tasks, continue on
                        if (!openTask) {
                            // Next, check the last activity date
                            if ((date)p.get(activityDateODT) != null) {
                                // Make sure the date is more than 90 days old
                                if ((date)p.get(activityDateODT) < date.today().addDays(-90)) {
                                    // The stage was 'Sales Accepted' or 'Sales Engaged'
                                    // There are no open tasks
                                    // The last activity date was greater than 90 days ago
                                    canUpd = true;
                                }
                            } else {
                                // If there is no last activity date, we need to look at the created date and see if it's 90+ days old
                                if ((dateTime)p.get(createdDate) < date.today().addDays(-90)) {
                                    // The stage was 'Sales Accepted' or 'Sales Engaged'
                                    // There are no open tasks
                                    // The created date was greater than 90 days ago
                                    canUpd = true;
                                }
                            }
                        }
                        if (canUpd) {
                            p.put(stageODT, 'Sales Inactive');
                            upd = true;
                        }
                    }
                }
                
                // ERGO
                // If there are no open suite sessions more than 90 days old
                if (!openERGO) {
                    // If the POI Stage is 'Sales Accepted' or 'Sales Engaged'
                    if (stages.contains((string)p.get(stageERGO))) {
                        // Check for open tasks for that suite
                        boolean openTask = false;
                        boolean canUpd = false;
                        if (tasks.size() > 0) {
                            for (Task t : tasks) {
                                if (t.Suite_Of_Interest__c == 'Ergonomics') {
                                    openTask = true;
                                }
                            }
                        }
                        // If no open tasks, continue on
                        if (!openTask) {
                            // Next, check the last activity date
                            if ((date)p.get(activityDateERGO) != null) {
                                // Make sure the date is more than 90 days old
                                if ((date)p.get(activityDateERGO) < date.today().addDays(-90)) {
                                    // The stage was 'Sales Accepted' or 'Sales Engaged'
                                    // There are no open tasks
                                    // The last activity date was greater than 90 days ago
                                    canUpd = true;
                                }
                            } else {
                                // If there is no last activity date, we need to look at the created date and see if it's 90+ days old
                                if ((dateTime)p.get(createdDate) < date.today().addDays(-90)) {
                                    // The stage was 'Sales Accepted' or 'Sales Engaged'
                                    // There are no open tasks
                                    // The created date was greater than 90 days ago
                                    canUpd = true;
                                }
                            }
                        }
                        if (canUpd) {
                            p.put(stageERGO, 'Sales Inactive');
                            upd = true;
                        }
                    }
                }
                
                // If we should update it, add it to the list
                if (upd) {
                    updList.add(p);
                }
            }
        }
        
        for(sObject so : updList){
            string prefix = string.valueOf(so.id).left(3);
            if (prefix == '003') {
                cList.add((Contact)so);
            } else {
                lList.add((Lead)so);
            }
        }
        
        // Update and write to email log
        if (updList.size() > 0) {
            
            try {
                if(clist.size()>0){
                    update clist;
                }
                if(lList.size()>0){
                    update lList;
                }
                
                results += '# POI Statuses set to stage: \'Sales Inactive\' = '+updList.size()+'<br/><br/>';
            } catch(exception e) {
                salesforceLog.createLog('POI', true, 'poiStatusStageScheduler', 'stage_salesInactive', string.valueOf(e));
            }
        } else {
            results += '# actions set to stage: \'Sales Inactive\' = 0 <br/><br/>';
        }
        
    }
    
    // Query contacts
    public sObject[] queryContacts(string sObjType) {
        return Database.query(queryString(sObjType));
    }
    
    // Query leads
    public sObject[] queryLeads(string sObjType) {
        return Database.query(queryString(sObjType));
    }
    
    // Query string
    public string queryString(string sObjType) {
        string q = 'SELECT id, CreatedDate, LastName, POI_Stage_MSDS__c, POI_Stage_EHS__c, POI_Stage_AUTH__c, POI_Stage_ODT__c, POI_Stage_ERGO__c, POI_Status_MSDS__c, POI_Status_EHS__c, POI_Status_AUTH__c, '+
            'POI_Status_ODT__c, POI_Status_ERGO__c, Num_Of_Unable_To_Connect_Tasks_MSDS__c, Num_Of_Unable_To_Connect_Tasks_EHS__c, Num_Of_Unable_To_Connect_Tasks_AUTH__c, Num_Of_Unable_To_Connect_Tasks_ODT__c, '+
            'Num_Of_Unable_To_Connect_Tasks_ERGO__c, POI_Status_Change_Date_MSDS__c, POI_Status_Change_Date_EHS__c, POI_Status_Change_Date_AUTH__c, POI_Status_Change_Date_ODT__c, POI_Status_Change_Date_ERGO__c, '+
            'Last_Activity_Date_Chemical_Management__c, Last_Activity_Date_EHS__c, Last_Activity_Date_Authoring__c, Last_Activity_Date_Online_Training__c, Last_Activity_Date_Ergonomics__c, '+
            '(SELECT id, Contact__c, Lead__c, Product_Suite__c, Status_Changed_Date__c, Session_Open_Date__c FROM Product_Of_Interest_Actions__r WHERE Session_Open__c = true AND Session_Open_Date__c >= LAST_N_DAYS : 90), '+
            '(SELECT id, Suite_Of_Interest__c, WhoId FROM Tasks WHERE isClosed = false) ';
        if (sObjType == 'Contact') {
            q += 'FROM Contact WHERE ';
        } else if (sObjType == 'Lead') {
            q += 'FROM Lead WHERE isConverted = false AND (';
        }
        q += '((POI_Stage_MSDS__c = \'Sales Accepted\' OR POI_Stage_MSDS__c = \'Sales Engaged\') AND ((Last_Activity_Date_Chemical_Management__c = null AND CreatedDate < LAST_N_DAYS : 90) OR (Last_Activity_Date_Chemical_Management__c < LAST_N_DAYS : 90))) '+
            'OR ((POI_Stage_EHS__c = \'Sales Accepted\' OR POI_Stage_EHS__c = \'Sales Engaged\') AND ((Last_Activity_Date_EHS__c = null AND CreatedDate < LAST_N_DAYS : 90) OR (Last_Activity_Date_EHS__c < LAST_N_DAYS : 90))) '+
            'OR ((POI_Stage_AUTH__c = \'Sales Accepted\' OR POI_Stage_AUTH__c = \'Sales Engaged\') AND ((Last_Activity_Date_Authoring__c = null AND CreatedDate < LAST_N_DAYS : 90) OR (Last_Activity_Date_Authoring__c < LAST_N_DAYS : 90))) '+
            'OR ((POI_Stage_ODT__c = \'Sales Accepted\' OR POI_Stage_ODT__c = \'Sales Engaged\') AND ((Last_Activity_Date_Online_Training__c = null AND CreatedDate < LAST_N_DAYS : 90) OR (Last_Activity_Date_Online_Training__c < LAST_N_DAYS : 90))) '+
            'OR ((POI_Stage_ERGO__c = \'Sales Accepted\' OR POI_Stage_ERGO__c = \'Sales Engaged\') AND ((Last_Activity_Date_Ergonomics__c = null AND CreatedDate < LAST_N_DAYS : 90) OR (Last_Activity_Date_Ergonomics__c < LAST_N_DAYS : 90)))';
        if (sObjType == 'Lead') {
            q += ')';
        }
        
        
        return q;
    }
    
    // Final results email
    public void email(string htmlBody) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new list<string>{'salesforceops@ehs.com'});
        mail.setReplyTo('salesforceops@ehs.com');
        mail.setSubject('poiStatusStageScheduler results - '+date.today());
        mail.setHtmlBody(htmlBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}