/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_VPM_TaskTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_VPM_TaskTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new VPM_Task__c());
    }
}