@isTest(seeAllData=true)
private class testOnsiteLocations {
    
    static testMethod void onsiteLocatons_onSite() {
        
        //insert test account
        Account a = new Account(Name='Test');
        insert a;
        
        //insert test contact
        Contact c = new Contact(AccountId=a.id, LastName='Test');
        insert c;
        
        Quote__c q = new Quote__c();
        q.Account__c = a.id;
        q.Contact__c = c.id;
        q.Contract_Length__c = '1 Year';
        insert q;
        
        Quote_Product__c qp = new Quote_Product__c();
        qp.Quantity__c = 2;
        qp.Quote__c = q.id;
        qp.Name__c = 'On-Site Chemical Inventory';
        qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'On-Site Chemical Inventory' LIMIT 1].id;
        qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'On-Site Chemical Inventory' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        qp.Onsite_Location_Data__c = '{"locations":[{"sqFootage":3,"rowNumber":1,"physicalAddress":"asd","numOfProducts":3,"nameOfContact":"asd","escortAvailable":true,"contactNumberEmail":"asd","closestAirport":"asd","addlComments":"asd"},{"sqFootage":0,"rowNumber":2,"physicalAddress":"asdasd","numOfProducts":0,"nameOfContact":"","escortAvailable":false,"contactNumberEmail":"","closestAirport":"","addlComments":""}]}';
        qp.Onsite_Protective_Equipment__c = 'a';
        qp.Onsite_Shots_Vaccinations__c = 'a';
        qp.Onsite_Specific_Safety_Training__c = 'a';
        qp.Onsite_Timeline__c = 'a';
        qp.Onsite_Vendormate_Registration__c = 'a';
        insert qp;
        
        
        ApexPages.currentPage().getParameters().put('id', q.id);
        quoteProductEntryStep3 con = new quoteProductEntryStep3();
        con.addOnsiteLocation();
        ApexPages.currentPage().getParameters().put('rowNum', string.valueof(3));
        con.removeOnsiteLocation();
        con.saveCustomQuoteItems();
        con.saveCustomQuoteItems();
        con.attachOnsite();
        con.attachOnsiteStep3();
        con.showHideOnsiteTable();
    }
    
    static testMethod void test_onsiteAttachLocationList() {
        
        //insert test account
        Account a = new Account(Name='Test');
        insert a;
        
        //insert test contact
        Contact c = new Contact(AccountId=a.id, LastName='Test');
        insert c;
        
        Quote__c q = new Quote__c();
        q.Account__c = a.id;
        q.Contact__c = c.id;
        q.Contract_Length__c = '1 Year';
        insert q;
        
        Quote_Product__c qp = new Quote_Product__c();
        qp.Quantity__c = 2;
        qp.Quote__c = q.id;
        qp.Name__c = 'On-Site Chemical Inventory';
        qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'On-Site Chemical Inventory' LIMIT 1].id;
        qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'On-Site Chemical Inventory' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        insert qp;
        
        ApexPages.currentPage().getParameters().put('id', qp.id);
        onsiteAttachLocationList con = new onsiteAttachLocationList();
        con.newAttach.Name = 'Test.doc';
        con.newAttach.Body = blob.valueOf('TEST BODY');
        con.save();
    }
    
     static testMethod void test_onsiteInventoryQuestionnaire() {
        
        //insert test account
        Account a = new Account(Name='Test');
        insert a;
        
        //insert test contact
        Contact c = new Contact(AccountId=a.id, LastName='Test');
        insert c;
        
        Quote__c q = new Quote__c();
        q.Account__c = a.id;
        q.Contact__c = c.id;
        q.Contract_Length__c = '1 Year';
        insert q;
        
        Quote_Product__c qp = new Quote_Product__c();
        qp.Quantity__c = 2;
        qp.Quote__c = q.id;
        qp.Name__c = 'On-Site Chemical Inventory';
        qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'On-Site Chemical Inventory' LIMIT 1].id;
        qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'On-Site Chemical Inventory' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        qp.Onsite_Location_Data__c = '{"locations":[{"sqFootage":3,"rowNumber":1,"physicalAddress":"asd","numOfProducts":3,"nameOfContact":"asd","escortAvailable":true,"contactNumberEmail":"asd","closestAirport":"asd","addlComments":"asd"},{"sqFootage":0,"rowNumber":2,"physicalAddress":"asdasd","numOfProducts":0,"nameOfContact":"","escortAvailable":false,"contactNumberEmail":"","closestAirport":"","addlComments":""}]}';
        qp.Onsite_Protective_Equipment__c = 'a';
        qp.Onsite_Shots_Vaccinations__c = 'a';
        qp.Onsite_Specific_Safety_Training__c = 'a';
        qp.Onsite_Timeline__c = 'a';
        qp.Onsite_Vendormate_Registration__c = 'a';
        insert qp;
        
        ApexPages.currentPage().getParameters().put('id', qp.id);
        onsiteInventoryQuestionnaire con = new onsiteInventoryQuestionnaire();
    }
    
    
}