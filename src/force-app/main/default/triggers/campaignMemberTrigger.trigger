trigger campaignMemberTrigger on CampaignMember
(before insert, before update, before delete, after insert, after update, after delete, after undelete) {
     if (trigger.isBefore) {
        if (trigger.isInsert) {
            campaignMemberTriggerHandler.beforeInsert(trigger.new);
        }
     }
    dlrs.RollupService.triggerHandler();
}