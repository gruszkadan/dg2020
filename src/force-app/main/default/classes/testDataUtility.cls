@isTest
public class testDataUtility {
    public Profile p;
    public UserRole vp;
    public UserRole director;
    public UserRole manager;
    public UserRole rep;
    public UserRole assoc;
    
    public testDataUtility() {
        p = [SELECT id, Name FROM Profile WHERE Name = 'SF Operations' LIMIT 1]; 
        vp = [SELECT id, Name FROM UserRole WHERE Name = 'VP Sales' LIMIT 1];
        director = [SELECT id, Name FROM UserRole WHERE Name = 'Director of Sales' LIMIT 1];
        manager = [SELECT id, Name FROM UserRole WHERE Name LIKE '%Sales Manager' LIMIT 1];
        rep = [SELECT id, Name FROM UserRole WHERE Name LIKE '%Sales Executive' LIMIT 1];
        assoc = [SELECT ID, Name from UserRole where Name LIKE 'Enterprise Sales Associate%' LIMIT 1];
    }
    
    public User createUser(string name, string userType, User salesVp, User salesDirector, User salesManager) {
        User u = new User();
        u.Alias = name;
        u.Email = name + '@testorg.com';
        u.EmailEncodingKey = 'UTF-8'; 
        u.LastName = name;
        u.LanguageLocaleKey = 'en_US';
        u.ForecastEnabled = true;
        u.LocaleSidKey = 'en_US';
        u.Rep_Name_in_Admin_Tool__c = name;
        u.ProfileId = p.id;
        u.TimeZoneSidKey = 'America/Los_Angeles'; 
        u.UserName = name + '@testorg.com';
        u.Department__c = 'Sales';
        if (userType == 'vp') {
            u.UserRoleId = vp.id;
        } else if (userType == 'director') {
            u.UserRoleId = director.id;
            u.Sales_VP__c = salesVp.id;
            u.Sales_Director__c = salesVp.id;
            u.ManagerId = salesVp.id;
            u.Sales_Performance_Manager_ID__c = salesVp.id;
        } else if (userType == 'manager') {
            u.UserRoleId = manager.id;
            u.Sales_VP__c = salesVp.id;
            u.Sales_Director__c = salesDirector.id;
            u.ManagerId = salesDirector.id;
            u.Sales_Performance_Manager_ID__c = salesDirector.id;
        } else if (userType == 'assoc') {
            u.UserRoleId = assoc.id;
            u.Sales_VP__c = salesVp.id;
            u.Sales_Director__c = salesDirector.id;
            u.ManagerId = salesManager.id;
            u.Sales_Performance_Manager_ID__c = salesManager.id;
        }else {
            u.UserRoleId = rep.id;
            u.Sales_VP__c = salesVp.id;
            u.Sales_Director__c = salesDirector.id;
            u.ManagerId = salesManager.id;
            u.Sales_Performance_Manager_ID__c = salesManager.id;
        }
        return u;
    }
    
    public Sales_Performance_Summary__c createSPS(User u, Sales_Performance_Summary__c mgrSPS) {
        Sales_Performance_Summary__c sps = new Sales_Performance_Summary__c();
        sps.OwnerId = u.id;
        sps.Manager__c = u.Sales_Performance_Manager_ID__c;
        if (mgrSPS != null) {
            sps.Manager_Sales_Performance_Summary__c = mgrSPS.id;
        }
        sps.Month__c = findMonth(integer.valueOf(date.today().month()));
        sps.Year__c = string.valueOf(date.today().year());
        sps.Role__c = u.Role__c;
        sps.Sales_Team_Manager__c = u.Sales_Performance_Manager_ID__c;
        sps.Sales_Rep__c = u.id;
        sps.My_Bookings__c = 10;
        sps.Total_Bookings__c = 10;
        sps.Quota__c = 10;
        sps.YTD_Total_Bookings__c = 10;
        sps.YTD_Quota__c = 10;
        sps.YTD_My_Bookings__c = 10;
        return sps;
    }
    
    public Order_Item__c createOrderItem(string repName, string orderItemId) {
        Order_Item__c oi = new Order_Item__c();
        oi.Name = 'Test';
        oi.Invoice_Amount__c = 10;
        oi.Order_Date__c = date.today();
        oi.Order_ID__c = 'test123';
        oi.Order_Item_ID__c = orderItemId;
        oi.Admin_Tool_Product_Name__c = 'HQ';
        oi.Year__c = string.valueOf(date.today().year()); 
        oi.Month__c = findMonth(date.today().month());
        oi.Contract_Length__c = 3;
        oi.AccountID__c = 'test123';
        oi.Term_Start_Date__c = date.today();
        oi.Term_End_Date__c = date.today().addYears(1);
        oi.Term_Length__c = 1;
        oi.Admin_Tool_Order_Status__c = 'A';
        oi.Contract_Number__c = 'test123';
        oi.Sales_Location__c = 'Chicago';
        oi.Admin_Tool_Sales_Rep__c = repName;
        oi.Admin_Tool_Order_Type__c = 'New';
        return oi;
    }
    
    public static string findMonth(integer m) {
        string month = '';
        if (m == 1) {
            month = 'January';
        } else if (m == 2) {
            month = 'February';
        } else if (m == 3) {
            month = 'March';
        } else if (m == 4) {
            month = 'April';
        } else if (m == 5) {
            month = 'May';
        } else if (m == 6) {
            month = 'June';
        } else if (m == 7) {
            month = 'July';
        } else if (m == 8) {
            month = 'August';
        } else if (m == 9) {
            month = 'September';
        } else if (m == 10) {
            month = 'October';
        } else if (m == 11) {
            month = 'November';
        } else if (m == 12) {
            month = 'December';
        }
        return month;
    }
    
    public Account createAccount(string name) {
        Account acct = new Account();
        acct.Name = name;
        acct.NAICS_Code__c = 'Test';
        acct.AdminID__c = 'test123';
        return acct;
    }
    
    public Opportunity createOppty(id acctId, date closeDate) {
        Opportunity opp = new Opportunity();
        opp.AccountID = acctId;
        opp.Name = 'Test';
        opp.CloseDate = closeDate;
        opp.StageName = 'System Demo';
        opp.Opportunity_Type__c = 'New';
        opp.Amount = 100;
        return opp;
    }
    
    public Sales_Performance_Settings__c createSettings(id setupId, boolean sidebar, boolean viewType, string defView) {
        Sales_Performance_Settings__c settings = new Sales_Performance_Settings__c();
        settings.SetupOwnerId = setupId;
        settings.Bookings_Overview_Sidebar__c = sidebar;
        settings.View_Type_Picklist__c = viewType;
        settings.Default_View__c = defView;
        return settings;
    }
    
    public Sales_Incentive_Setting__c createIncentiveSettings(string recordTypeName, id repID, id assocID) {
        Sales_Incentive_Setting__c siSettings = new Sales_Incentive_Setting__c();
        siSettings.RecordTypeId = [SELECT ID
                                   FROM RecordType
                                   WHERE SobjectType = 'Sales_Incentive_Setting__c' and Name=: recordTypeName Limit 1].ID;
        if(recordTypeName == 'Enterprise Associate'){
            siSettings.Associate__c = assocID;
            siSettings.Sales_Representative__c = repID;
            siSettings.Percent_Of_Bookings__c = 1;
            siSettings.Licensing_Goal_Kicker__c = 500;
        }
        if(recordTypeName == 'Mid-Market Associate'){
            siSettings.Associate__c = assocID;
            siSettings.Sales_Representative__c = repID;
            siSettings.Percent_Of_Bookings__c = 1;
        }
        return siSettings;
    }
    
    public Sales_Incentive_Setting__c createOpportunityIncentiveSettings(id assocID) {
        Sales_Incentive_Setting__c siSettings = new Sales_Incentive_Setting__c();
        siSettings.RecordTypeId = [SELECT ID
                                   FROM RecordType
                                   WHERE SobjectType = 'Sales_Incentive_Setting__c' and Name=: 'Mid-Market Opportunity' Limit 1].ID;
        siSettings.Associate__c = assocID;
        siSettings.Percent_Of_Bookings__c = 10;
        return siSettings;
    }
}