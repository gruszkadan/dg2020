<apex:page standardController="Salesforce_Request__c" extensions="sfRequest" action="{!redirect}" standardStylesheets="true">
    <style>
        input[name=newNote] {
        display: none;
        }
        .noRecordsFound{border: 1px solid #D4DADC;padding: 4px;} 
    </style> 
    <apex:form >
        <apex:sectionHeader title="Salesforce Request" subtitle="{!Salesforce_Request__c.name}" rendered="{!pagetype != 'merge'}"/>
        <apex:sectionHeader title="Salesforce Merge" subtitle="{!Salesforce_Request__c.name}" rendered="{!pagetype = 'merge'}"/>
        <apex:pageBlock title="Salesforce Request Detail" mode="mainDetail">
            <apex:pageMessages showDetail="true" />
            <apex:pageBlockButtons >
                <apex:commandButton value="Save" action="{!save}" id="saveButton" style="display: none;" />
                <apex:commandButton value="Cancel" action="{!cancel}" onclick="resetInlineEdit()" id="cancelButton" style="display: none;"  />
                <apex:commandButton id="startTimer" rendered="{!openTask.size=0 && (u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || u.LastName = 'Devine')}" action="{!startTimer}" value="Start Timer"/>
                <apex:commandButton id="endTimer" rendered="{!openTask.size>0 && (u.Profile.Name='System Administrator' || u.Profile.Name='SF Operations' || u.LastName = 'Devine')}" action="{!endTimer}" value="End Timer"/>
                <apex:commandButton id="editButton" rendered="{!pagetype != 'merge' && (u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations')}" action="{!edit}" value="Edit"/>
                <apex:commandButton id="completeButton" rendered="{!pagetype = 'merge' && (u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || u.LastName = 'Devine')}" action="{!completeMerge}" value="Complete Merge"/>
                <apex:commandButton id="accept" rendered="{!$User.Id <> Salesforce_Request__c.OwnerId && pagetype != 'merge' && (u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || u.LastName = 'Devine')}" action="{!acceptOwnership}" value="Accept"/>
                <apex:commandButton value="Complete" action="{!completebutton}" rendered="{!sfr.Status__c != 'Completed' && (u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || u.LastName = 'Devine')}"/>
            </apex:pageBlockButtons> 
            <apex:pageBlockSection title="Information" columns="2"> 
                <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                        hideOnEdit="editButton"
                                        event="ondblclick"  
                                        rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || u.LastName = 'Devine'}" 
                                        resetFunction="resetInlineEdit"/>
                <apex:outputField title="Request #" value="{!Salesforce_Request__c.Name}"/>
                <apex:outputField title="Owner" value="{!Salesforce_Request__c.OwnerId}">&nbsp;
                    <apex:outputLink rendered="{!u.SF_Request_Owner_Change__c = TRUE}" value="/{!Salesforce_Request__c.ID}/a?retURL=/{!Salesforce_Request__c.ID}">[Change]</apex:outputLink>
                </apex:outputField>
                <apex:outputField title="Summary" value="{!Salesforce_Request__c.Summary__c}" rendered="{!pagetype != 'merge'}"/>
                <apex:outputField title="Priority" value="{!Salesforce_Request__c.Priority__c}" rendered="{!pagetype != 'merge'}"/>
                <apex:outputField title="Request Type" value="{!Salesforce_Request__c.Request_Type__c}">
                    <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                            hideOnEdit="editButton"
                                            event="ondblclick" 
                                            rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || terrEnrich = true || u.LastName = 'Devine'}"
                                            resetFunction="resetInlineEdit"/>
                </apex:outputField>
                <apex:outputField title="Status" value="{!Salesforce_Request__c.Status__c}">
                    <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                            hideOnEdit="editButton"
                                            event="ondblclick" 
                                            rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || terrEnrich = true || u.LastName = 'Devine'}"
                                            resetFunction="resetInlineEdit"/>
                </apex:outputField>
                <apex:outputField rendered="{!OR(Salesforce_Request__c.Request_Type__c = 'Admin', Salesforce_Request__c.Request_Type__c = '') && pagetype != 'merge'}" title="Admin Work Type" value="{!Salesforce_Request__c.Admin_Work_Type__c}"/>
                <apex:outputField title="Requested By" value="{!Salesforce_Request__c.Requested_By__c}"/>
                <apex:outputField rendered="{!OR(Salesforce_Request__c.Request_Type__c != 'Admin', Salesforce_Request__c.Request_Type__c = '') && pagetype != 'merge'}" title="Request Complexity" value="{!Salesforce_Request__c.Complexity_1_High_3_Low__c}"/>
                <apex:outputField title="Follow Up Date" value="{!Salesforce_Request__c.Follow_Up_Date__c}" rendered="{!pagetype != 'merge'}"/>
                <apex:outputField rendered="{!OR(Salesforce_Request__c.Request_Type__c = 'Project', Salesforce_Request__c.Request_Type__c = '') && pagetype != 'merge'}" title="Estimated Project Size" value="{!Salesforce_Request__c.Estimated_Project_Size__c}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Description" columns="1"  rendered="{!pagetype != 'merge'}">
                <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                        hideOnEdit="editButton"
                                        event="ondblclick" 
                                        rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || u.LastName = 'Devine'}"
                                        resetFunction="resetInlineEdit"/>
                <apex:outputField title="Description" value="{!Salesforce_Request__c.Description__c}"/>
                <apex:outputField title="Resolution" value="{!Salesforce_Request__c.Resolution__c}">
                    <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                            hideOnEdit="editButton"
                                            event="ondblclick" 
                                            rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || terrEnrich = true || u.LastName = 'Devine'}"
                                            resetFunction="resetInlineEdit"/>
                </apex:outputField>
                <apex:outputField title="Cannot Add Reason" value="{!Salesforce_Request__c.Cannot_Add_Reason__c}"/>
            </apex:pageBlockSection>  
            <apex:pageBlockSection title="Primary Account" columns="1" rendered="{!pagetype = 'merge'}">
                <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                        hideOnEdit="editButton"
                                        event="ondblclick" 
                                        rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations'}"
                                        resetFunction="resetInlineEdit"/>
                <apex:outputField label="Primary Account URL" value="{!Salesforce_Request__c.Primary_Merge_Account__c}"/>
                <apex:outputField label="Phone # To Keep" value="{!Salesforce_Request__c.Phone_To_Keep__c}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Description" columns="1" rendered="{!pagetype = 'merge'}">
                <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                        hideOnEdit="editButton"
                                        event="ondblclick" 
                                        rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations'}"
                                        resetFunction="resetInlineEdit"/>
                <apex:outputField label="Description" value="{!Salesforce_Request__c.Description__c}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Request Dates" columns="2">
                <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                        hideOnEdit="editButton"
                                        event="ondblclick" 
                                        rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations'}"
                                        resetFunction="resetInlineEdit"/>
                <apex:outputField title="Requested Date" value="{!Salesforce_Request__c.Requested_Date__c}"/>
                <apex:outputField title="Actual Start Date" value="{!Salesforce_Request__c.Start_Date__c}" rendered="{!pagetype != 'merge'}">
                    <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                            hideOnEdit="editButton"
                                            event="ondblclick" 
                                            rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || terrEnrich = true}"
                                            resetFunction="resetInlineEdit"/>
                </apex:outputField>
                <apex:outputField title="Estimated Start Date" value="{!Salesforce_Request__c.Estimated_Development_Start_Date__c}" rendered="{!pagetype != 'merge'}"/>
                <apex:outputField title="Actual QA Date" value="{!Salesforce_Request__c.Actual_QA_Start_Date__c}"/>
                <apex:outputField title="Estimated QA Start Date" value="{!Salesforce_Request__c.Estimated_QA_Start_Date__c}" rendered="{!pagetype != 'merge'}"/>
                <apex:outputField title="Actual Completed Date" value="{!Salesforce_Request__c.Completed_Date__c}">
                    <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                            hideOnEdit="editButton"
                                            event="ondblclick" 
                                            rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || terrEnrich = true}"
                                            resetFunction="resetInlineEdit"/>
                </apex:outputField>
                <apex:outputField title="Estimated Completion Date" value="{!Salesforce_Request__c.Estimated_Completion_Date__c}" rendered="{!pagetype != 'merge'}"/>   
                <apex:outputField rendered="{!OR(Salesforce_Request__c.Request_Type__c = 'Project', Salesforce_Request__c.Request_Type__c = '') && pagetype != 'merge'}" title="Requirements Due Date" value="{!Salesforce_Request__c.Requirements_Due_Date__c}"/>
            </apex:pageBlockSection>      
            <apex:pageblockSection id="request_time" title="Request Times" columns="2"  rendered="{!U.Profile.Name='SF Operations'}">     
                <apex:outputField: title="Estimated Duration (In Minutes)" value="{!Salesforce_Request__c.Estimated_Duration__c}"/>
                             <apex:outputField: title="Actual Minutes" value="{!Salesforce_Request__c.Actual_Minutes__c}"/>
                <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                        hideOnEdit="editButton"
                                        event="ondblclick" 
                                        rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations' || terrEnrich = true}"
                                        resetFunction="resetInlineEdit"/>
                                <apex:outputField: title="Estimated Duration (In Hours)" value="{!Salesforce_Request__c.Estimated_Duration_In_Hours__c}"/> 
   
                <apex:outputField: title="Actual Hours" value="{!Salesforce_Request__c.Actual_Hours__c}"/>
            </apex:pageblockSection> 
            <apex:pageblockSection id="team_breakdown" title="Team Breakdown" columns="1"  rendered="{!U.Profile.Name='SF Operations'}"> 
                <apex:outputPanel >
                    <table class="list" cellspacing= "0" border= "0">
                        <tr class="headerRow">
                            <th>Name</th>
                            <th># Hours Worked</th>
                            <th> Date Last Worked</th>
                        </tr>
                        <tr class="dataRow">
                            <td>Mark McCauley</td>
                            <td><apex:outputtext value="{!MarkTotalHour}"/></td>
                            <td><apex:outputtext value="{!MarkLastWorked}"/></td>
                        </tr>
                        <tr class="dataRow">
                            <td>Daniel Gruszka</td>
                            <td><apex:outputtext value="{!DanTotalHour}"/></td>
                            <td><apex:outputText value="{!DanLastWorked}"/></td> 
                        </tr>
                        <tr class="dataRow">
                            <td>Al Powell</td>
                            <td><apex:outputtext value="{!AlTotalHour}"/></td>
                            <td><apex:outputtext value="{!AlLastWorked}"/></td>
                        </tr>
                        <tr class="dataRow">
                            <td>Tara Wills</td>
                            <td><apex:outputtext value="{!TaraTotalHour}"/></td>
                            <td><apex:outputtext value="{!TaraLastWorked}"/></td>
                        </tr> 
                    </table>
                </apex:outputPanel>     
            </apex:pageblockSection> 
            <apex:pageblockSection id="system" title="System Information" columns="2">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Created By"/>
                    <apex:outputPanel >
                        {!Salesforce_Request__c.CreatedBy.Name}&nbsp;<apex:outputField value="{!Salesforce_Request__c.CreatedDate}"/>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Last Modified By"/>
                    <apex:outputPanel >
                        {!Salesforce_Request__c.LastModifiedBy.Name}&nbsp;<apex:outputField value="{!Salesforce_Request__c.LastModifiedDate}"/>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageblockSection>
        </apex:pageBlock>
    </apex:form>
    <apex:relatedList list="Salesforce_Request_Merges__r" rendered="{!pagetype = 'merge'}"/>
    <apex:relatedList list="Salesforce_Update_Notes__r"/>
    <apex:relatedList rendered="{!(u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations') && pagetype != 'merge'}" list="Work_Request_Tasks__r" title="Timer"/>
    <apex:relatedList list="CombinedAttachments" rendered="{!pagetype != 'merge'}"/>
    <apex:form rendered="{!Salesforce_Request__c.Request_Type__c = 'Project' || Salesforce_Request__c.Request_Type__c ='Enhancement'}">
        <apex:pageBlock title="Salesforce Request Test Scenarios" id="scenBlock">
            <apex:pageBlockButtons location="top">
                <apex:commandButton action="{!addScenario}" value="Add/Edit" rendered="{!(u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations')}"/>
                <apex:commandButton value="Re-Order Steps" action="{!scenDetailReorder}" rendered="{!(u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations')}"/>
                <span style="padding-left:100px;">
                    <apex:outputText style="padding-left:50px">
                        <b><u>Status:</u></b>
                        &emsp;
                        <b><font size="4" color="green">✔</font></b>&nbsp;-&nbsp;Pass&nbsp;({!numPass})
                        &emsp;
                        <b><font size="4" color="red">✘</font></b>&nbsp;-&nbsp;Fail&nbsp;({!numFail})
                        &emsp;
                        <b><font size="4" color="gold">✱</font></b>&nbsp;-&nbsp;Not Yet Tested&nbsp;({!numNull})
                    </apex:outputText>
                </span>
            </apex:pageBlockButtons>
            <apex:outputPanel layout="block" styleClass="noRecordsFound" rendered="{!theRequestScenarios.size = 0}"> No records to display </apex:outputPanel>
            <apex:outputPanel rendered="{!theRequestScenarios.size > 0}" >
                <apex:pageBlockTable value="{!theRequestScenarios}" var="trs" id="scenTable">
                    <apex:column headerValue="Step" width="90px">
                        <apex:commandLink value="{!trs.Step__c}" action="{!scenarioDetail}"> 
                            <apex:param value="{!trs.id}" name="scenid"/>
                        </apex:commandLink>
                    </apex:column>
                    <apex:column headerValue="Name" width="90px" value="{!trs.Name}" />
                    <apex:column headerValue="Owner" width="90px" value="{!trs.Owner__c}" />
                    <apex:column width="40px" headerValue="Result">
                        <b><font size="3" color="green"><apex:outputText rendered="{!trs.Result__c = 'Pass'}" value="✔"/></font></b>
                        <b><font size="3" color="red"><apex:outputText rendered="{!trs.Result__c = 'Fail'}" value="✘"/></font></b>
                        <b><font size="3" color="gold"><apex:outputText rendered="{!trs.Result__c = ''}" value="✱"/></font></b>
                    </apex:column>
                </apex:pageBlockTable>
            </apex:outputPanel>
        </apex:pageBlock>
    </apex:form>
    
    <apex:form rendered="{!(u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations') && pagetype != 'merge'}">
        <apex:pageBlock title="Salesforce Request Components" id="compBlock" rendered="{!(u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations') && pagetype != 'merge'}">
            <apex:inlineEditSupport showOnEdit="saveButton1"
                                    event="ondblclick" 
                                    rendered="{!u.Profile.Name = 'System Administrator' || u.Profile.Name = 'SF Operations'}"
                                    resetFunction="resetInlineEdit"/>
            <apex:pageBlockButtons location="top">
                <apex:commandButton action="{!addComponent}" value="Add/Edit"/>
                <apex:commandButton action="{!saveComponentEdits}" value="Save" id="saveButton1" rerender="compBlock" style="display: none;"/>
            </apex:pageBlockButtons>
            <apex:pageBlockTable value="{!theRequestComponents}" var="trc" id="compTable">
                <apex:column style="width:150px;">
                    <apex:facet name="header">
                        <apex:outputPanel layout="block">
                            <apex:commandLink value="Type" action="{!reorderComps}" rerender="compTable" rendered="{!sortOrder = 'ORDER BY Type__c DESC' || sortOrder != 'ORDER BY Type__c ASC'}">
                                <apex:param assignTo="{!sortOrder}" value="ORDER BY Type__c ASC"/>
                            </apex:commandLink>
                            <apex:commandLink value="Type" action="{!reorderComps}" rerender="compTable" rendered="{!sortOrder = 'ORDER BY Type__c ASC'}">
                                <apex:param assignTo="{!sortOrder}" value="ORDER BY Type__c DESC"/>
                            </apex:commandLink>
                            &nbsp;
                            <apex:outputText value="˅" rendered="{!sortOrder = 'ORDER BY Type__c ASC'}"/>
                            <apex:outputText value="˄" rendered="{!sortOrder = 'ORDER BY Type__c DESC'}"/>
                        </apex:outputPanel>
                    </apex:facet>
                    {!trc.Type__c}
                </apex:column>
                <apex:column style="width:275px;">
                    <apex:facet name="header">
                        <apex:outputPanel layout="block">
                            <apex:commandLink value="API Object" action="{!reorderComps}" rerender="compTable" rendered="{!sortOrder = 'ORDER BY Object__c DESC' || sortOrder != 'ORDER BY Object__c ASC'}">
                                <apex:param assignTo="{!sortOrder}" value="ORDER BY Object__c ASC"/>
                            </apex:commandLink>
                            <apex:commandLink value="API Object" action="{!reorderComps}" rerender="compTable" rendered="{!sortOrder = 'ORDER BY Object__c ASC'}">
                                <apex:param assignTo="{!sortOrder}" value="ORDER BY Object__c DESC"/>
                            </apex:commandLink>
                            &nbsp;
                            <apex:outputText value="˅" rendered="{!sortOrder = 'ORDER BY Object__c ASC'}"/>
                            <apex:outputText value="˄" rendered="{!sortOrder = 'ORDER BY Object__c DESC'}"/>
                        </apex:outputPanel>
                    </apex:facet>
                    {!trc.Object__c}
                </apex:column>
                <apex:column style="width:275px;">
                    <apex:facet name="header">
                        <apex:outputPanel layout="block">
                            <apex:commandLink value="API Name" action="{!reorderComps}" rerender="compTable" rendered="{!sortOrder = 'ORDER BY Name__c DESC' || sortOrder != 'ORDER BY Name__c ASC'}">
                                <apex:param assignTo="{!sortOrder}" value="ORDER BY Name__c ASC"/>
                            </apex:commandLink>
                            <apex:commandLink value="API Name" action="{!reorderComps}" rerender="compTable" rendered="{!sortOrder = 'ORDER BY Name__c ASC'}">
                                <apex:param assignTo="{!sortOrder}" value="ORDER BY Name__c DESC"/>
                            </apex:commandLink>
                            &nbsp;
                            <apex:outputText value="˅" rendered="{!sortOrder = 'ORDER BY Name__c ASC'}"/>
                            <apex:outputText value="˄" rendered="{!sortOrder = 'ORDER BY Name__c DESC'}"/>
                        </apex:outputPanel>
                    </apex:facet>
                    <a href="/{!trc.Salesforce_Component__c}">{!trc.Name__c}</a>&nbsp;
                    <apex:outputText value="({!trc.Salesforce_Component__r.Num_of_Open_Requests__c})" rendered="{!trc.Salesforce_Component__r.Num_of_Open_Requests__c != 0 && trc.Salesforce_Component__r.Num_of_Open_Requests__c != 1}" style="color:red"/>
                </apex:column>
                <apex:column headerValue="MSDS Dev" width="90px">
                    <apex:outputField value="{!trc.Deployed_to_MSDS_Dev__c}"/>
                </apex:column>
                <apex:column headerValue="MSDS Test" width="90px">
                    <apex:outputField value="{!trc.Deployed_to_MSDS_Test__c}"/>
                </apex:column>
                <apex:column headerValue="Prod Stage" width="90px">
                    <apex:outputField value="{!trc.Deployed_to_Production_Staging__c}"/>
                </apex:column>
                <apex:column headerValue="Production" width="90px">
                    <apex:outputField value="{!trc.Deployed__c}"/>
                </apex:column>
                <apex:column headerValue="Notes">
                    <apex:outputField value="{!trc.Notes__c}"/>
                </apex:column>
            </apex:pageBlockTable>
        </apex:pageBlock>
    </apex:form>
    <apex:iframe />
</apex:page>