@isTest(SeeAllData=false)
public class testBackFillRevRec {

        static testMethod void Test1() {

        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;

        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;

        id caseOwner = [SELECT id FROM User WHERE LastName = 'Wills' LIMIT 1].id; 

        Case[] casesToInsert = new List<Case>();
       // for(integer i = 0; i<2; i++){
            Case newCase = new Case();
            newCase.OwnerID = caseOwner;
            newCase.AccountId = newAcct.Id;
            newCase.ContactId = newCon.Id;
            newCase.Status = 'Active';
            newCase.Subject = 'Test';
            newCase.Type = 'Compliance Services';
            newCase.Primary_project_type__c = 'BVA';
            newCase.Total_Project_Amount__c = 9.99;
            casesToInsert.add(newCase);
        //}

        //Upon insertion of cases witht he fields above not set to null, a rev rec and transactions will be created.
        insert casesToInsert;

        Revenue_Recognition__c[] revRecs = [SELECT id, project_status__c, Start_at_Step__c, (SELECT id, Recognition_Project_Status__c, Sequence_Number__c, step__c
                                             FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                            FROM Revenue_Recognition__c
                                            WHERE case__c in:casesToInsert];  
            
            
            Revenue_Recognition_Transaction__c[] rrtToDelete = new List<Revenue_Recognition_Transaction__c>();
            
            for(Revenue_Recognition__c rr: revRecs){
                rr.Start_at_Step__c = 2;
                update rr;
                
                for(Revenue_Recognition_Transaction__c rrt: rr.Revenue_Recognition_Transactions__r){
                    
                    if(rrt.step__c > 2){
                        rrtToDelete.add(rrt);
                    }else{
                        System.debug(rrt.Sequence_Number__c);
                    }
                }
            }
            //System.debug(rrtToDelete);
            delete rrtToDelete;
            
            BackFillRevRec.backFillTransactions();
            
            Revenue_Recognition__c[] revRecCheck = [SELECT id, project_status__c, (SELECT id,Sequence_Number__c,Recognition_Project_Status__c, step__c
                                             FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                            FROM Revenue_Recognition__c
                                            WHERE case__c in:casesToInsert]; 
            
            Revenue_Recognition_Transaction__c[] step1 = new List<Revenue_Recognition_Transaction__c>();
            Revenue_Recognition_Transaction__c[] step2 = new List<Revenue_Recognition_Transaction__c>();
            Revenue_Recognition_Transaction__c[] steps3andMore = new List<Revenue_Recognition_Transaction__c>(); 
               
            for(Revenue_Recognition__c rr: revRecs){
                for(Revenue_Recognition_Transaction__c rrt: rr.Revenue_Recognition_Transactions__r){
                    if(rrt.Step__c ==  1){
                        step1.add(rrt);
                    }
                    if(rrt.Step__c ==  2){
                        step2.add(rrt);
                    }
                    if(rrt.Step__c >  2){
                        steps3andMore.add(rrt);
                    }
                }
                           }
             System.assert(step1.size() ==  1);
            //System.assert(step2.size() ==  2);
             System.assert(steps3andMore.size() > 5);


        }
    
}