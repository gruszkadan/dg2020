global class ultimateParentBatchable implements Database.Batchable<SObject>, Database.Stateful {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        User auto = [SELECT id FROM User WHERE Alias = 'rwern' LIMIT 1]; 
        auto.Validation_Rules_Active__c = false; 
        update auto; 
        
        //each batch will run this query and return the list of records to the execute method
        if (!test.isRunningTest()) {
            return Database.getQueryLocator([SELECT id, Name, Ultimate_Parent__c, ParentId FROM Account]);
        } else {
            return Database.getQueryLocator([SELECT id, Name, Ultimate_Parent__c, ParentId FROM Account LIMIT 100]);
        }
    }
    
    global void execute(Database.BatchableContext bc, list<SObject> batch) {   
        //set to hold accounts that have no parent id and therefore could be an ultimate parent
        set<id> ultAcctIds = new set<id>();
        for (Account acct : (list<Account>) batch) {
            if (acct.ParentId == null) {
                ultAcctIds.add(acct.id);
            }
        }
        
        if (ultAcctIds.size() > 0) {
            //list to hold all accounts that have any parentids matching those possible ultimate parent accounts
            Account[] allAccts = new list<Account>();
            allAccts.addAll([SELECT id, Name, Ultimate_Parent__c,
                             ParentId,
                             Parent.ParentId,
                             Parent.Parent.ParentId,
                             Parent.Parent.Parent.ParentId,
                             Parent.Parent.Parent.Parent.ParentId,
                             Parent.Parent.Parent.Parent.Parent.ParentId 
                             FROM Account
                             WHERE ParentId IN : ultAcctIds
                             OR Parent.ParentId IN : ultAcctIds
                             OR Parent.Parent.ParentId IN : ultAcctIds
                             OR Parent.Parent.Parent.ParentId IN : ultAcctIds
                             OR Parent.Parent.Parent.Parent.ParentId IN : ultAcctIds
                             OR Parent.Parent.Parent.Parent.Parent.ParentId IN : ultAcctIds]);
            
            if (allAccts.size() > 0) {
                set<id> areUltsIds = new set<id>();
                set<id> notUltsIds = new set<id>();                
                for (id ultAcctId : ultAcctIds) {
                    boolean isUlt = false;
                    for (Account acct : allAccts) {
                        if (acct.Parent.Parent.Parent.Parent.Parent.ParentId != null) {
                            if (acct.Parent.Parent.Parent.Parent.Parent.ParentId == ultAcctId) {
                                acct.Ultimate_Parent__c = ultAcctId;
                                acct.Is_Ultimate_Parent__c = false;
                                isUlt = true;
                            }
                        }
                        if (acct.Parent.Parent.Parent.Parent.ParentId != null) {
                            if (acct.Parent.Parent.Parent.Parent.ParentId == ultAcctId) {
                                acct.Ultimate_Parent__c = ultAcctId;
                                acct.Is_Ultimate_Parent__c = false;
                                isUlt = true;
                            }
                        }
                        if (acct.Parent.Parent.Parent.ParentId != null) {
                            if (acct.Parent.Parent.Parent.ParentId == ultAcctId) {
                                acct.Ultimate_Parent__c = ultAcctId;
                                acct.Is_Ultimate_Parent__c = false;
                                isUlt = true;
                            }
                        }
                        if (acct.Parent.Parent.ParentId != null) {
                            if (acct.Parent.Parent.ParentId == ultAcctId) {
                                acct.Ultimate_Parent__c = ultAcctId;
                                acct.Is_Ultimate_Parent__c = false;
                                isUlt = true;
                            }
                        }
                        if (acct.Parent.ParentId != null) {
                            if (acct.Parent.ParentId == ultAcctId) {
                                acct.Ultimate_Parent__c = ultAcctId;
                                acct.Is_Ultimate_Parent__c = false;
                                isUlt = true;
                            }
                        }
                        if (acct.ParentId != null) {
                            if (acct.ParentId == ultAcctId) {
                                acct.Ultimate_Parent__c = ultAcctId;
                                acct.Is_Ultimate_Parent__c = false;
                                isUlt = true;
                            }
                        }
                    }
                    if (isUlt) {
                        areUltsIds.add(ultAcctId);
                    } else {
                        notUltsIds.add(ultAcctId);
                    }
                }
                if (areUltsIds.size() > 0) {
                    Account[] ultAccts = [SELECT id FROM Account WHERE id IN : areUltsIds];
                    for (Account acct : ultAccts) {
                        acct.Is_Ultimate_Parent__c = true;
                    }
                    update ultAccts;
                }
                
                if (notUltsIds.size() > 0) {
                    Account[] ultAccts = [SELECT id FROM Account WHERE id IN : notUltsIds];
                    for (Account acct : ultAccts) {
                        acct.Is_Ultimate_Parent__c = false;
                    }
                    update ultAccts;
                }
                
                
                
                update allAccts;
            }
        }
       
    }      
    
    global void finish(Database.BatchableContext bc) {
		
        User auto = [SELECT id FROM User WHERE Alias = 'rwern' LIMIT 1];
        auto.Validation_Rules_Active__c = true;
        update auto;
        
    }
    
}