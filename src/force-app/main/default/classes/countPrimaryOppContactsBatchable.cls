global class countPrimaryOppContactsBatchable implements Database.Batchable<SObject>, Database.Stateful {
    Private Boolean isScheduled;
    
    global countPrimaryOppContactsBatchable (Boolean scheduled){
        isScheduled = scheduled;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        if(isScheduled){
            return Database.getQueryLocator([SELECT id, Num_of_Primary_Contacts__c, (SELECT id,isPrimary from OpportunityContactRoles where IsPrimary = true)
                                             FROM Opportunity
                                             WHERE SystemModstamp = TODAY]);
        }else{
            if (!test.isRunningTest()) {
                return Database.getQueryLocator([SELECT id, Name,(SELECT id, isPrimary, OpportunityId FROM OpportunityContactRoles WHERE isPrimary = true)
                                                 FROM Opportunity]);
            } else {
                return Database.getQueryLocator([SELECT id, Name,(SELECT id, isPrimary, OpportunityId FROM OpportunityContactRoles WHERE isPrimary = true)
                                                 FROM Opportunity LIMIT 100]);
            }
        }            
    }
    
    global void execute(Database.BatchableContext bc, list<SObject> batch) {  
        
        User u = [SELECT id, Validation_Rules_Active__c FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
        u.Validation_Rules_Active__c = false;
        update u;
        
        for (Opportunity op : (list<Opportunity>) batch) {
            op.Num_of_Primary_Contacts__c = op.OpportunityContactRoles.size();
        }
        
        update batch;
        
        u.Validation_Rules_Active__c = true;
        update u;
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
    
}