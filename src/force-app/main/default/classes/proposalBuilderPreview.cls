public class proposalBuilderPreview {
    
    public Quote__c quote {get;set;}
    public Id quoteId {get;set;}
    public boolean hasNeeds {get;set;}
    public List<Quote__c> quoteOptions {get;set;}
    public boolean pendingApproval {get;set;}
    public List<Attachment> pdfAttachments {get;set;}
    
    
    public proposalBuilderPreview(){
        
        pdfAttachments = new List<Attachment>();
        
        pendingApproval = false;
        
        quoteId = ApexPages.currentPage().getParameters().get('id');
        
        quote = [SELECT Name, Quote_Options__c, JSON_Proposal_Data__c, Approve_Expired_Promo_Pricing_Terms__c, (Select id, Name FROM Attachments) FROM Quote__c WHERE id = :quoteId];
        
        
        //if the main quote has a quote option, grab the other quotes with the same quote option
        if(quote.quote_options__c != null){
            
            quoteOptions = [SELECT Id, Approve_Expired_Promo_Pricing_Terms__c FROM Quote__c WHERE Include_Option_In_Proposal__c = true AND Quote_Options__c = :quote.Quote_Options__c];
            
            //loop through quote options; if any need approval, mark pendingApproval as true
            for(Quote__c q : quoteOptions){
                if(q.Approve_Expired_Promo_Pricing_Terms__c == true){
                    pendingApproval = true;
                }
            }
            
            //if main quote doesn't have quote option, check if main quote needs approval. if true, mark pendingApproval = true
        }else{
            
            if(quote.Approve_Expired_Promo_Pricing_Terms__c == true){
                pendingApproval = true;
            }
        }
        
        //loop through attachments for main quote. make list of attachments that have pdf as their extension
        for(Attachment a : quote.Attachments){
            
            String attachmentName =  a.Name.split('\\.').get(1);
            
            if(attachmentName == 'pdf'){
                pdfAttachments.add(a);
            }
            
        }  
        
        //check if Needs Requirements Page was selected. Determine if Needs Step displayed
        hasNeeds = proposalBuilderUtility.containsNeeds(quote.JSON_Proposal_Data__c);
        
    }
    
    public PageReference createAttachments(){
        
        blob pdfBlob;        
        
        //blob of proposal print PDF for attachment body
        if (!test.isRunningTest()) {
            pdfBlob = new PageReference('/apex/proposal_print_pdf?id='+quoteId+'&pdf=true').setRedirect(false).getContentAsPDF();
        } else {
            pdfBlob = Blob.valueOf('Unit Test Attachment Body');
        }
        
        
        
        //get Sales Director of current user to submit quote for approval
        id dirId = [SELECT Sales_Director__c FROM User WHERE id = : UserInfo.getUserId() LIMIT 1].Sales_Director__c;
        
        integer pdfs =  pdfAttachments.size() + 1;
        
        if(pendingApproval == true){
            
            //link attachment image of proposal print pdf to quote and insert
            Attachment a = new attachment();
            a.ParentId = quoteId;
            a.Name = Quote.Name + '-v' + pdfs + '.pdf'; 
            a.body = pdfBlob;
            a.IsPrivate = true;
            insert a;
            
            //mark main quote pending approval and submit approval request to user's sales director
            quote.Status__c = 'Pending Approval';
            quote.Proposal_in_Approval__c = a.id;
            update quote;
            
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Submitting request for approval ['+a.Name+']');
            req.setNextApproverIds(new Id[] {dirId});
            req.setObjectId(quote.id);
            Approval.ProcessResult result = Approval.process(req);
            
        }else{
            
            //link attachment image of proposal print pdf to quote and insert
            Attachment a = new attachment();
            a.ParentId = quoteId;
            a.Name = Quote.Name + '-v' + pdfs + '.pdf'; 
            a.body = pdfBlob;
            insert a;
            
        }
        
        return new PageReference('/'+quote.id).setRedirect(true);
        
    }
    
    
    
    
}