public class salesPerformanceReportingController {
    public String viewAs {get;set;}
    public string userID {get;set;}
    public string userName {get;set;}
    public string userEmail {get;set;}
    public User u {get;set;}
    public String defaultView {get;set;}
    public String reportType {get;set;}
    public Integer stepNumber {get;set;}
    public Sales_Performance_Summary__c[] spsResults {get;set;}
    public Sales_Performance_Request__c[] pendingSprResults {get;set;}
    public boolean valError {get;set;}
    public string selectedYear {get;set;}//picklist value
    public string selectedMonth {get;set;} //picklist value
    public requestWrapper[] requestWrappers {get;set;}
    public List<string> emailList {get;set;}
    public string errorString {get;set;} 
    public Map<id, Sales_Performance_Request__c> pendingRelatedSprs {get;set;}
    public string emailComments {get;set;}
    public Sales_Incentive_Setting__c[] sisRecords = new List<Sales_Incentive_Setting__c>();
    public RecordType recordTypeID {get;set;}
    public integer modelNum {get;set;}
    public boolean day6To15 {get;set;}
    public boolean reportSent{get; set;}
    public string monthWord {get;set;}
    public boolean canViewMonthly {get;set;}
    public boolean sendToFinace {get;set;}
    public boolean viewReporting {get;set;}
    
    
    public salesPerformanceReportingController(){
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias,FirstName, Profile.Name, LastName, Name, email, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID 
             FROM User 
             WHERE id = :viewAs LIMIT 1];
        userID = u.id;
        userName = u.Name;
        userEmail = u.Email;
        //get defaultView to display when viewing as
        defaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
       	sendToFinace = Sales_Performance_Settings__c.getInstance().Can_Send_To_Finance__c;
        canViewMonthly = Sales_Performance_Settings__c.getInstance().Can_View_Monthly_Reporting__c;
        //disabled page if not permissioned to access
         viewReporting = Sales_Performance_Settings__c.getInstance().Can_View_Reporting__c;
        //default step is 1
        stepNumber = 1;
        //default is model step 1
        modelNum = 1;
        selectedMonth ='Select Month';
    }

    
    
    public void canSendtoFinanceDates(){
        //Datetime dt = Datetime.now();
        DateTime lastMonthDT = Datetime.now().addMonths(-1);
        String lastMonthNum = string.ValueOf(lastMonthDT.month());
        System.debug(selectedMonth);
        System.debug(LastMonthNum); 
        if(lastMonthNum == selectedMonth){
            date monthFirstDay = Date.Today().toStartofMonth();        
            Holiday[] holidays = [SELECT ActivityDate FROM Holiday];
            Set<Date> holidaysSet = new Set<Date> ();
            for (Holiday currHoliday : holidays)
            {
                holidaysSet.add(currHoliday.ActivityDate);
            }
            Integer workingDays = 0;
            for (integer i = 0; i <= monthFirstDay.daysBetween(Date.Today()); i++)
            {
                Date dt = monthFirstDay + i;
                DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());
                String todayDay = currDate.format('EEEE');
                if (todayDay != 'Saturday' && todayDay != 'Sunday' && (!holidaysSet.contains(dt)))
                {
                    workingDays = workingDays + 1;
                }
            }
            integer businessDay = workingDays;
            if(businessDay > 6 && Date.today().day() < 16){
                day6To15 = true;
            }else{
                day6To15 = false;
            }
        }else{
            day6To15 = false;
        }
        System.debug(day6To15);
    }  
    

    // year picklist on salesPerformanceReporting
    public SelectOption[] getyears() {
        SelectOption[] ops = new list<SelectOption>();
        ops.add(new SelectOption('Select Year', 'Select Year'));
        integer firstYear = 2018;
        integer nowYear = date.today().year();
        ops.add(new SelectOption(string.valueOf(firstYear), string.valueOf(firstYear)));
        for (integer i = 0; i < (nowYear - firstYear); i++) {
            integer yearVal = firstYear + (i + 1);
            ops.add(new SelectOption(string.valueOf(yearVal), string.valueOf(yearVal)));
        }
        return ops;
    }
    
    
    public SelectOption[] getmonths() {
        integer startMonthInt;
        integer endMonthInt;
        string nowYear = string.valueOf(date.today().year());
        if(nowYear == '2018'){
            startMonthInt = 5;
            endMonthInt = date.today().month();
        }else{
            startMonthInt = 1;
            if (nowYear == SelectedYear){
                endMonthInt = date.today().month();
            }else{
                endMonthInt = 12;
            }
    	}
        SelectOption[] ops = new list<SelectOption>();
        ops.add(new SelectOption('Select Month', 'Select Month'));
        for(integer i = startMonthInt; i < endMonthInt + 1; i++){
            //note: we are just getting the months in this method - year shown below will not alter results
            ops.add(new selectOption(string.valueOf(i), Datetime.valueOf( '1990-' + string.valueOf(i) + '-15 00:00:00').format('MMMMM')));
        }
        return ops;
    }
    
    
    //Method that sets the selected report type and month once chosen by the user
    public void selectReport(){
        valError=false;
        if((SelectedYear != 'Select Year' && SelectedMonth != 'Select Month' && reportType == 'Month Summary') || reportType == 'Pending Approvals' ){ 
            if (reportType == 'Month Summary'){                
                string midMonth = selectedYear + '-' + selectedMonth + '-15 00:00:00';
                Datetime midMonthDT = Datetime.valueOf(midMonth);
                monthWord = midMonthDT.format('MMMMM');
               //monthWord = Datetime.valueOf(selectedYear + '-' + selectedMonth + '-15 00:00:00').format('MMMM');
                querySpsResults(); 
            }
            if (reportType == 'Pending Approvals'){
                queryPendingSprResults(); 
            }
            canSendtoFinanceDates();
            stepNumber = 2;
        }
        else{
            valError=true;
        } 
    }
    
    
    //method queries values needed for table in Step 2
    public void querySpsResults(){
        requestWrappers = new List<requestWrapper>();
        spsResults = [SELECT Employee_ID__c, Sales_Rep__r.Name, My_Bookings__c, Bookings_Incentives__c, Demo_Incentives__c,  
                      Associate_Incentives__c, Referral_Incentives__c, Other_Incentives__c, Group__c, Manager__r.Name, 
                      (SELECT Current_Approver__c,Name,Status__c,Type__c, Owner.Name, Month_Incentive_Applied__c,  Year_Incentive_Applied__c, Current_Approval_Step__c
                       FROM Sales_Performance_Requests__r WHERE Status__c = 'Pending Approval')
                      FROM Sales_Performance_Summary__c 
                      WHERE Year__c = :selectedYear 
                      AND Month__c = :monthWord ORDER BY Sales_Rep__r.Name];
        
        pendingRelatedSprs = new Map<id, Sales_Performance_Request__c>();
        for (Sales_Performance_Summary__c sps: spsResults){
            if (sps.Sales_Performance_Requests__r.size() > 0){
                //hadPendingSprs = true;
                for (Sales_Performance_Request__c spr: sps.Sales_Performance_Requests__r){
                    pendingRelatedSprs.put(spr.id, spr);
                }
            }
        }
        if (pendingRelatedSprs.size()>0){ 
            List<ProcessInstance> approvals = [SELECT Id, ProcessDefinitionId, Status, TargetObjectId,
                                               (SELECT id,ProcessInstanceId,ActorId FROM WorkItems)
                                               FROM ProcessInstance 
                                               WHERE TargetObjectId IN :pendingRelatedSprs.keyset()
                                               AND CompletedDate = NULL];
            if (approvals.size() > 0){
                for (ProcessInstance pi: approvals){
                    requestWrappers.add(new requestWrapper(pendingRelatedSprs.get(pi.TargetObjectId), pi.workItems[0]));
                }
            }
        }
    }
    
    //query all sprs pending approval
    public void queryPendingSprResults(){
        pendingSprResults= [SELECT Current_Approver__c,Name,Status__c,Type__c, Owner.Name, Month_Incentive_Applied__c, Year_Incentive_Applied__c FROM Sales_Performance_Request__c WHERE Status__c = 'Pending Approval' ORDER BY Name DESC];      
    } 
    
    
    //goes back to step 1 and clears query
    public void backToStep1(){
        spsResults = null;
        pendingSprResults = null;
        selectedYear =  'Select Year';
        selectedMonth = 'Select Month';
        reportType = null;
        stepNumber = 1;
    }
    
    
    //checks for existing incentive summary download records and creates a new record if one does not exist; populates sisRecords
    public void checkSisRecord(){
        integer selyear = integer.valueof(selectedYear);
        integer selmonth = integer.valueOf(selectedMonth);
        sisRecords = [SELECT ID FROM Sales_Incentive_Setting__c WHERE RecordType.Name='Incentive Summary Download' AND Calendar_year(Incentive_Date__c)=:selYear AND Calendar_Month(Incentive_Date__c)=:selMonth]; 
        if (sisRecords.size() < 1){
            //make a new record of type incentive summary download
            insertSisRecord('Incentive Summary Download');
        }
    }
    
    
    //create new record - Incentive Summary Download or Incentive Finance Submission
    public void insertSisRecord(string sisType){
        integer selyear = integer.valueof(selectedYear);
        integer selmonth = integer.valueOf(selectedMonth);
        
        recordTypeID = [SELECT ID
                        FROM RecordType
                        WHERE SobjectType = 'Sales_Incentive_Setting__c' and Name=:sisType Limit 1];
        
        Sales_Incentive_Setting__c sis = new Sales_Incentive_Setting__c();
        emailList = financeEmails();
        string emailString;
        if(emailList.size() > 0){
        for(integer i = 0; i < emailList.size(); i++){
            if(i==0){
                emailString +=  emailList[i];
            }else{
                emailString +=  ',' + emailList[i];
            }
        }   
        }
        
        sis.RecordTypeID = recordTypeID.ID;
        sis.Incentive_Date__c = Date.newInstance(selyear, selmonth, 1);
        if (sisType == 'Incentive Finance Submission'){
            sis.Comments__c = emailComments;
            sis.Sent_Date__c = System.now();
            sis.Sent_To__c =  emailString;
        }
        insert sis;
        sisRecords = [SELECT ID FROM Sales_Incentive_Setting__c WHERE RecordType.Name=:sisType AND Calendar_year(Incentive_Date__c)=:selYear AND Calendar_Month(Incentive_Date__c)=:selMonth ORDER BY CreatedDate DESC];
    }
    
    //takes type or set type at top???
    public void addAttachement(string sisType){
        if (sisRecords.size() > 0){
            blob excelBlob;   
            if(Test.isRunningTest()){
                excelBlob = blob.valueOf('Unit.Test');
            }else{
            excelBlob = new PageReference('/apex/monthly_finance_report?month='+selectedMonth+'&year='+selectedYear).getContent();
            }
            //set these elsewhere set name or type at get set level?
             string excelName = monthWord + ' ' + selectedYear + ' ' + sisType +'.xls';
            Attachment downloadRecord = new Attachment(parentId = sisRecords[0].Id, name=excelName, body = excelBlob);  
            insert downloadRecord;
        }
    }

    
     public void downloadToExcel(){
         checkSisRecord();
         addAttachement('Incentive Summary Download');
     }

    
    public PageReference returnToReporting(){
        spsResults = null;
        pendingSprResults = null;
        selectedYear = 'Select Year';
        selectedMonth = 'Select Month';
        reportType = null;
        stepNumber = 1;
        modelNum = 1;
        return new PageReference('/apex/sales_performance_reporting');    
    }
    
    
    public void completeProcess(){
        try{
            approveReject();  
            insertSisRecord('Incentive Finance Submission');
            addAttachement('Finance Incentives Report');
            sendFinanceEmail();
            reportSent=true;
            modelNum = 2; 
        }catch(exception e){
                modelNum = 2;
                reportSent=false;
                errorString = string.valueOf(e);
            	system.debug(errorString);
        } 

    }
    
    
    
    public void sendFinanceEmail(){
        //make an email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(emailList);  
        mail.setSubject('Finance Incentives Report');
        mail.setReplyTo(userEmail);
        mail.setHtmlBody(emailComments);
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        Attachment a = [select Name, Body from Attachment where ParentId = :sisRecords[0].Id];
        Messaging.EmailfileAttachment efa = new Messaging.Emailfileattachment();
        efa.setFileName(a.Name);
        efa.setBody(a.Body);
        fileAttachments.add(efa);
        mail.setFileAttachments(fileAttachments);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});  
    }
    
    
    
    public List<string> financeEmails(){
        //query for email addresses;
        Email_Settings__c[] emails = [Select Email__c from Email_Settings__c where Type__c = 'Finance Incentives Report'];
        List<string> emailList = new list<string>();
        for(integer i=0; i < emails.size(); i++){
            if (emails[i].Email__c !=Null){
                emailList.add(emails[i].Email__c);
            }
        }
        return emailList;
    }
    
    
    
    public void clearApprovers(){
        List<Sales_Performance_Request__c> updateSprApprovers = new List<Sales_Performance_Request__c>();
        
        for (requestWrapper wrap: requestWrappers){
            integer stepnum = integer.valueOf(wrap.spr.Current_Approval_Step__c);
            
            //clear fields and add spr to update list
            if (stepnum == 1){
                wrap.spr.Assigned_Approver_2__c = NULL;
                wrap.spr.Assigned_Approver_3__c = NULL;
                wrap.spr.Assigned_Approver_4__c = NULL;
                wrap.spr.Assigned_Approver_5__c = NULL;
                wrap.spr.Assigned_Approver_6__c = NULL;
                wrap.spr.Assigned_Approver_7__c = NULL;
                wrap.spr.Assigned_Approver_8__c = NULL;
                wrap.spr.Assigned_Approver_9__c = NULL;
                wrap.spr.Assigned_Approver_10__c = NULL;
            }
            if (stepnum == 2){
                wrap.spr.Assigned_Approver_3__c = NULL;
                wrap.spr.Assigned_Approver_4__c = NULL;
                wrap.spr.Assigned_Approver_5__c = NULL;
                wrap.spr.Assigned_Approver_6__c = NULL;
                wrap.spr.Assigned_Approver_7__c = NULL;
                wrap.spr.Assigned_Approver_8__c = NULL;
                wrap.spr.Assigned_Approver_9__c = NULL;
                wrap.spr.Assigned_Approver_10__c = NULL;
            }
            if (stepnum == 3){
                wrap.spr.Assigned_Approver_4__c = NULL;
                wrap.spr.Assigned_Approver_5__c = NULL;
                wrap.spr.Assigned_Approver_6__c = NULL;
                wrap.spr.Assigned_Approver_7__c = NULL;
                wrap.spr.Assigned_Approver_8__c = NULL;
                wrap.spr.Assigned_Approver_9__c = NULL;
                wrap.spr.Assigned_Approver_10__c = NULL;
            }
            
            if (stepnum == 4){
                wrap.spr.Assigned_Approver_5__c = NULL;
                wrap.spr.Assigned_Approver_6__c = NULL;
                wrap.spr.Assigned_Approver_7__c = NULL;
                wrap.spr.Assigned_Approver_8__c = NULL;
                wrap.spr.Assigned_Approver_9__c = NULL;
                wrap.spr.Assigned_Approver_10__c = NULL;
            }
            if (stepnum == 5){
                
                wrap.spr.Assigned_Approver_6__c = NULL;
                wrap.spr.Assigned_Approver_7__c = NULL;
                wrap.spr.Assigned_Approver_8__c = NULL;
                wrap.spr.Assigned_Approver_9__c = NULL;
                wrap.spr.Assigned_Approver_10__c = NULL;
            }
            if (stepnum == 6){
                wrap.spr.Assigned_Approver_7__c = NULL;
                wrap.spr.Assigned_Approver_8__c = NULL;
                wrap.spr.Assigned_Approver_9__c = NULL;
                wrap.spr.Assigned_Approver_10__c = NULL;
            }     
            if (stepnum == 7){
                wrap.spr.Assigned_Approver_8__c = NULL;
                wrap.spr.Assigned_Approver_9__c = NULL;
                wrap.spr.Assigned_Approver_10__c = NULL;
            }
            if (stepnum == 8){
                wrap.spr.Assigned_Approver_9__c = NULL;
                wrap.spr.Assigned_Approver_10__c = NULL;
            }
            if (stepnum == 9){
                wrap.spr.Assigned_Approver_10__c = NULL;
            }
            updateSprApprovers.add(wrap.spr);
        }           
     update updateSprApprovers;

    }
    

    public void approveReject(){
        clearApprovers();
        List<Approval.ProcessWorkitemRequest> preqList = new List<Approval.ProcessWorkitemRequest>();  
        for (requestWrapper wrap: requestWrappers){
            if(wrap.approvalAction == NULL){
                valError = true;
            }
            Approval.ProcessWorkitemRequest preq = new Approval.ProcessWorkitemRequest();
            preq.setComments(wrap.approvalComments);
            preq.setAction(wrap.approvalAction);
            preq.setWorkitemId(wrap.piwi.id);
            preqList.add(preq);
        }
        Approval.ProcessResult[] results = Approval.process(preqList);
    }
    
    

    //used to reset approvals when reeclicking submit to finance button (opening modal)
    public void resetApprovals(){
        valError = false;
        errorString = NULL;
        for(requestWrapper rw:requestWrappers){
            rw.approvalAction = NULL;
            rw.approvalComments = NULL;
        }
    }
    
    //wrapper
    public class requestWrapper{
        public string approvalComments {get;set;}
        public string approvalAction {get;set;}
        public Sales_Performance_Request__c spr {get;set;}
        public ProcessInstanceWorkItem piwi {get;set;}
        
        public requestWrapper(Sales_Performance_Request__c xSpr, ProcessInstanceWorkItem xPiwi){
            //approvalAction = 'Approve';
            //approvalComments = 'A final decision process was executed due to the end of month deadline.';
            spr = xSpr;
            piwi = xPiwi;           
        }
    }

    
}