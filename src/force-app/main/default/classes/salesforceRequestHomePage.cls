public class salesforceRequestHomePage {
    
    public List<Salesforce_Request__c> requests {get; set;}
    public salesforceRequestHomePage() {
        requests = [SELECT Id, Name, Summary__c, Requested_By__r.Name, CreatedDate, Status__c, Owner.Name
                         from Salesforce_Request__c where Request_Type__c !='Merge' AND Owner.Name != 'Audrey Savage' and Status__c NOT IN
                         ('Duplicate Request','Completed','Cannot Add','Chosen Not to Add') ORDER BY CreatedDate DESC LIMIT 10];
    }
}