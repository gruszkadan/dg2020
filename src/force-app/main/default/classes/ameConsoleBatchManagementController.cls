public class ameConsoleBatchManagementController {
    
    public ameWrapper[] pendingReviewPaginated {get;set;}
    public ameWrapper[] pendingActivationPaginated {get;set;}    

    //public map<string, id> findRepId {get;set;}
    public map<id, string> RepIdByLastName {get;set;}
    public map<id, string> RepIdByFirstName {get;set;}
    public string selectedAction {get;set;}
    public id pendingRenewalQueue {get;set;}
    public List<id> keys {get;set;}
    public id ameBeingEdited {get;set;}
    public string ameBeingEditedName {get;set;}
    public orderItemWrapper[] oiAvailable4Selection {get;set;}
    public string errorString {get;set;}
    public boolean showModalConfirm {get;set;}
    public id keepOpenOnLoad {get;set;} 
    
    public Set<String> repsWithAmes {get;set;}
    public string errorMessage {get;set;}
    public string viewing {get;set;} 
    public id currentlyViewing {get;set;}
    public string tabOnLoad {get;set;}
    
    //new
    public string currentTab {get;set;}
    public integer offsetValue {get;set;}  
    public integer limitNumber {get;set;} 
    public integer firstAME {get;set;}
    public integer lastAME {get;set;}
    public integer totalAMEs {get;set;} 
    public id renewalId {get;set;}
    public string sortField {get;set;}
    public string sortDirection {get;set;}
    public string prevSortField {get;set;} 
    

    
    public ameConsoleBatchManagementController(){
        keepOpenOnLoad = ApexPages.currentPage().getParameters().get('ame'); 
        tabOnLoad = ApexPages.currentPage().getParameters().get('view'); 
       // findRepId = new map<string, id>();
        RepIdByLastName = new map<id, String>();
        RepIdByFirstName = new map<id, String>();
        keys = new List<Id>();
        selectedAction ='';
        showModalConfirm = false;
        errorMessage = '';

        //find the pending renal queue to identify ames not yet assigned
        pendingRenewalQueue = [Select queue.id from queueSobject where queue.developerName = 'Pending_Renewals'].queue.id;
        renewalId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
		        
        //create a map of retention users full names for matching, a map of last names for tables/stats section, and a list of rentention user ids for looping is vf page
        for(user u: [Select id, Name, FirstName, LastName from User where isActive = true AND UserRole.Name != 'Director of Customer Success' AND Profile.Name LIKE '%Retention%' Order By LastName ASC] ){
          
            //findRepId.put(u.Name, u.id);
            RepIdByFirstName.put(u.id, u.FirstName);
            RepIdByLastName.put(u.id, u.LastName);
            keys.add(u.id);
        }
        
        if(tabOnLoad == NULL){
            currentTab='review';
        }else{
            currentTab = tabOnLoad;
        }
        
        runOnLoad();       
        
    }
    
    public void runOnLoad(){
        pendingReviewPaginated = new List<ameWrapper>();
        pendingActivationPaginated = new List<ameWrapper>();        
        totalAMEs = null;

        //set variables and run query with pagination 
        offsetValue = 0;
        sortField = 'account__r.Name';
        prevSortField = 'account__r.Name';
        sortDirection = 'asc';
        limitNumber = 50;        
        paginationQuery();
        
    }
    
    
    public void nextRenewals(){
        offsetValue = offsetValue + limitNumber;
        paginationQuery();
    }

    public void previousRenewals(){
        offsetValue = offsetValue  - limitNumber;
        if(offsetValue < 0){
            offsetValue = 0;
        }
        //System.debug(offsetValue + 'first:'+ firstAME + ' limit: ' + limitNumber);
        paginationQuery();
    }

    
    
    
	public void paginationQuery(){

        string q = 'Select id, Name, OwnerId, Owner.Name, Owner.LastName, account__c, Status__c, Count_in_Stats__c, Renewal_Date__c, account__r.Name, account__r.Retention_Owner__c, Not_Yet_Processed__c, AME_ARR__c, ' +
        '(Select id, Booking_Amount__c, Renewal_Amount__c, Name, Contract_Term_End_Date__c, Salesforce_Product_Name__c from Order_items__r Order by Renewal_Amount__c DESC) FROM account_management_event__c '+
        'WHERE RecordTypeId = \''+ renewalId + '\' AND Status__c = \'Not Yet Created\' ';

        if(currentTab == 'pendingActivation'){
            q += 'AND Not_Yet_Processed__c = FALSE ';
            if(totalAMEs == null){
                totalAMEs = integer.ValueOf([select count(id) ct from account_management_event__c WHERE RecordType.Name = 'Renewal' AND Status__c = 'Not Yet Created' AND Not_Yet_Processed__c = FALSE ][0].get('ct'));
            }
        }else{
            q += 'AND Not_Yet_Processed__c = TRUE ';
            if(totalAMEs == null){
                totalAMEs = integer.ValueOf([select count(id) ct from account_management_event__c WHERE RecordType.Name = 'Renewal' AND Status__c = 'Not Yet Created' AND Not_Yet_Processed__c = TRUE ][0].get('ct'));
            }
        }
        if(sortField != NULL){            
            q += ' ORDER BY ' + string.escapeSingleQuotes(sortField) + ' ' + string.escapeSingleQuotes(sortDirection) + ' ';
        }
        
       	q += ' LIMIT ' + limitNumber + ' OFFSET '+ offsetValue +' ';
        //System.debug(q);

        List<account_management_event__c> aList = database.query(q);
        prevSortField = sortField;
        firstAME = offsetValue + 1;
        lastAME = offsetValue + aList.size();
        
        ameWrapper[] amesViewing = new list<ameWrapper>();
        for(account_management_event__c  nyc: aList){
            amesViewing.add(new ameWrapper(nyc));
        }
       
        //System.debug(amesViewing.size());
        if(currentTab == 'pendingActivation'){
            pendingActivationPaginated = amesViewing;           
        }else{
            pendingReviewPaginated = amesViewing;            
        }
    } 
    
    public void toggleAndQuery(){
        if(sortDirection == 'asc'){
            sortDirection ='desc nulls last';
        }else{
            sortDirection ='asc';
        }
        if(prevSortField != sortField){
            sortDirection ='asc';
        }
        offsetValue = 0;
        paginationQuery();

    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        for(user u: [Select id, Name, FirstName, LastName from User where isActive = true AND UserRole.Name != 'Director of Customer Success' AND Profile.Name LIKE '%Retention%' Order By LastName ASC] ){
            options.add(new SelectOption(u.id, u.LastName));
        }
        return options; 
    }

        
    public pageReference saveAMEs(){       
        account_management_event__c[] amesToUpdate = new List<account_management_event__c>();   
        String pendingType = system.currentpagereference().getparameters().get('viewToReturnTo');        
        if(pendingType == 'pendingActivation' ){
            for(ameWrapper w : pendingActivationPaginated){                           
                amesToUpdate.add(w.ame);
            }
        }else{
            for(ameWrapper w : pendingReviewPaginated ){
                if(w.ame.OwnerId == null){
                    w.ame.OwnerId = pendingRenewalQueue;
                } 
                amesToUpdate.add(w.ame);
            } 
        }        
        update amesToUpdate;
        PageReference pageRef = new PageReference('/apex/ame_console_batch_management');
        pageRef.getParameters().put('view', pendingType);
        pageRef.setRedirect(true);
        return pageRef;
        
    }
    
    
    public void setOwner(){
        String newOwner = system.currentpagereference().getparameters().get('owner');  
        String identifier = system.currentpagereference().getparameters().get('ameChanged'); 
        String pendingType = system.currentpagereference().getparameters().get('viewToSave');  
         
        if(pendingType == 'review'){
            for(ameWrapper w : pendingReviewPaginated ){
                if(w.ame.id == identifier){

                    if(newOwner != NULL){
                        w.ame.OwnerId = newOwner;
                    }                
                    break;
                }
            }
        }else{
           for (ameWrapper w : pendingActivationPaginated ){
                if(w.ame.id == identifier){

                    if(newOwner != NULL){
                        w.ame.OwnerId = newOwner;
                    }            
                    break;
                }
            }
        }
        
    }
    
    /*
    //immediate update when a new owner is selected or a checkbox is clicked
    public void immediateUpdateAme(){
        String newOwner = system.currentpagereference().getparameters().get('owner');  
        String identifier = system.currentpagereference().getparameters().get('ameChanged'); 
        String pendingType = system.currentpagereference().getparameters().get('view');  
         
        if(pendingType == 'review'){
            for(ameWrapper w : pendingReview ){
                if(w.ame.id == identifier){

                    if(newOwner != NULL){
                        w.ame.OwnerId = newOwner;
                    }
                    if(w.ame.OwnerId == null){
                        w.ame.OwnerId = pendingRenewalQueue;
                    }
                    update w.ame;                 
                    break;
                }
            }
        }else{
           for (ameWrapper w : pendingActivation ){
                if(w.ame.id == identifier){

                    if(newOwner != NULL){
                        w.ame.OwnerId = newOwner;
                    }
                    if(w.ame.OwnerId == null){
                        w.ame.OwnerId = pendingRenewalQueue;
                    }
                    update w.ame;                 
                    break;
                }
            }
        }
        getNotYetCreatedAmes();
        calcBatchStats();
        makeBars();
    }
	*/

//indivdual delete option available to delete a single processed ame
	public PageReference immediateDelete(){          
        String identifier = system.currentpagereference().getparameters().get('ameToDelete'); 

         for(Integer i = (pendingActivationPaginated.size()-1); i>=0; i--){
            if(pendingActivationPaginated[i].ame.id == identifier){
                try{
                    delete pendingActivationPaginated[i].ame;
                }catch (Exception e){
                     errorMessage = 'There was an error while attempting to delete the record '+ pendingActivationPaginated[i].ame.name +':'  + string.ValueOf(e); 
                }
                break;
            }
         }
        PageReference pageRef = new PageReference('/apex/ame_console_batch_management');  
        pageRef.getParameters().put('view', 'pendingActivation');
        pageRef.setRedirect(true);
        return pageRef;
    }

	//find the ame's account's order items for the edit modal
    public void loadAccountOI(){
        
        showModalConfirm = false;
        errorString = '';
        String account = system.currentpagereference().getparameters().get('ameAccount');
        ameBeingEdited = system.currentpagereference().getparameters().get('ameToUpdate');
        ameBeingEditedName = system.currentpagereference().getparameters().get('name');      

        oiAvailable4Selection = new List<orderItemWrapper>();
        for(order_item__c o : [Select id, Name, Booking_Amount__c, Renewal_Amount__c, Account__c, Account_Management_Event__c, Salesforce_Product_Name__c,  Contract_Term_End_Date__c FROM Order_Item__c WHERE Admin_Tool_Order_Status__c = 'A' AND Account__c = :account]){
            oiAvailable4Selection.add(new orderItemWrapper(o, ameBeingEdited));
        }
    }

    //update eh order items associated with an ame (for edit modal); a minimum of 1 order item per ame is required
    public pageReference associateOrderItems(){
        tabOnLoad = null;
        boolean parametersAreValid = false;
        for(ameWrapper w : pendingReviewPaginated ){
            if(w.ame.id == ameBeingEdited){ 
                if(oiAvailable4Selection[0].oi.Account__c == w.ame.account__c){
                    parametersAreValid = true;
                    tabOnLoad = 'review';
                }
            }
        }
        if(tabOnLoad != 'review'){
            for(ameWrapper w : pendingActivationPaginated ){
                if(w.ame.id == ameBeingEdited){ 
                    if(oiAvailable4Selection[0].oi.Account__c == w.ame.account__c){
                        parametersAreValid = true;
                        tabOnLoad = 'pendingActivation';
                    }
                }
            }     
        }
        
        integer numAssociatedOI = 0;

        if(parametersAreValid){
            Order_Item__c[] oiToUpdate = new List<Order_Item__c>();
            for(orderItemWrapper w : oiAvailable4Selection){
                if(w.oiSelected){
                    numAssociatedOI ++;
                    w.oi.Account_Management_Event__c = ameBeingEdited;
                    oiToUpdate.add(w.oi);
                }else if (w.oi.Account_Management_Event__c == ameBeingEdited){
                    w.oi.Account_Management_Event__c = NULL;
                    oiToUpdate.add(w.oi);
                }
            }
            if(numAssociatedOI > 0){
            //if(oiToUpdate.size() > 0){
                  update oiToUpdate;
                  showModalConfirm = true;             
                  return null;
            }else{
                errorString = 'An AME must be associated with at least one Order Item.';
                return null;
            }
        }else{
            errorString = 'Invalid parameters. Contact the Systems Administrator if the issue persists.';
            return null;
        }
    }
    
    //redirect button after edit modal changes have been processed
    public pageReference returnToBatchConsole(){
        PageReference pageRef = new PageReference('/apex/ame_console_batch_management');
        pageRef.getParameters().put('ame', ameBeingEdited);
        pageRef.getParameters().put('view', tabOnLoad);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    //mass deleted an ame that has been selected
    public pageReference deleteAMEs(){
        errorMessage = '';
        //String identifier = system.currentpagereference().getparameters().get('ameChanged');  
                
        List<account_management_event__c> toDel = new List<account_management_event__c>();
        for(ameWrapper w : pendingReviewPaginated ){
            if(w.ameSelected){
                toDel.add(w.ame);
            }
        }
        if(toDel.size() > 0){
            try{
               delete toDel; 
            }catch(Exception e) {
               errorMessage = 'There was an error while attempting to delete records: ' + string.ValueOf(e); 
                return null;
            }
            
        }else{
            errorMessage = 'No AMEs were selected for deletion.';
            return null;
        }
        
        PageReference pageRef = new PageReference('/apex/ame_console_batch_management');        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    //mass update ame's to indicate they have been processed
   public pageReference processAMEs(){
       errorMessage = '';
        List<account_management_event__c> toUp = new List<account_management_event__c>();
        for(ameWrapper w : pendingReviewPaginated ){
            if(w.ameSelected){
       
                if(w.ame.OwnerId != Null & w.ame.OwnerId != pendingRenewalQueue){                
             		w.ame.Not_Yet_Processed__c = false;
                    toUp.add(w.ame);
                }else{
                    errorMessage = 'Please assign all selected AMEs to an owner.';
                    return null;
                }
            }
        }
        if(toUp.size() > 0){
            try{
                update toUp;
                PageReference pageRef = new PageReference('/apex/ame_console_batch_management');
                pageRef.setRedirect(true);
                return pageRef;
            }catch(Exception e){
                errorMessage = 'There was an error updating the AMEs: ' + string.valueOf(e);
                return null;
            }            
        }else{
            errorMessage = 'Select the AMEs you wish to process';
            return null;
        }
    }
    

    public class ameWrapper { 
        public string type {get;set;}
        public  boolean ameSelected {get;set;}
        public account_management_event__c ame {get;set;}
        public Order_Item__c[] allOrderItems {get;set;}
        public List<orderItemWrapper> oiWrappers {get;set;}
        public decimal arr {get;set;}
        public id ameOwner {get;set;} 

        public ameWrapper(account_management_event__c xAME){
            ameSelected = false;
            arr = 0;
            ame = xAME;
            allOrderItems = new List<Order_Item__c>(); 
            oiWrappers = new List<orderItemWrapper>();
            allOrderItems.addAll(xAME.Order_items__r);
            
            for(Order_Item__c o :allOrderItems ){
                if(o.Renewal_Amount__c != NULL){
                    arr += o.Renewal_Amount__c;
                }
            }
        }
    }
    
    public class orderItemWrapper { 
        public  boolean selected {get;set;}
        public Order_Item__c OI {get;set;}
        public boolean oiSelected {get;set;}
        
        public orderItemWrapper(Order_Item__c xOrderItem, id targetAME){
            OI = xOrderItem;
            if(xOrderItem.Account_Management_Event__c == targetAME){
                oiSelected = true;
            }else{
                oiSelected = false;
            }
        }
    }

}