public class poiActionUtility {
    
    public static void createSFPOIActions(sObject[] records) {
        set<id> acctIds = new set<id>();
        Account_Referral__c[] refs = new list<Account_Referral__c>();
        poiActionUtility.poiAction[] wrappers = new list<poiActionUtility.poiAction>();
        
        for (sObject r : records) {
            refs.add((Account_Referral__c)r);
        }
        if (refs.size() > 0) {
            for (Account_Referral__c ref : refs) {
                string sfId = ref.Contact__c;
                string listId;
                string actionId = ref.Name;
                if (ref.Type__c != null) {
                    if (ref.Type__c == 'Chemical Management') {
                        listId = 'SF2';
                    } else if (ref.Type__c == 'EHS') {
                        listId = 'SF5';
                    } else if (ref.Type__c == 'Authoring') {
                        listId = 'SF3';
                    } else if (ref.Type__c == 'ODT') {
                        listId = 'SF6';
                    } else if (ref.Type__c == 'Ergonomics') {
                        listId = 'SF4';
                    }
                }
                if (sfId != null && listId != null && actionId != null) {
                    poiActionUtility.poiAction w = new poiActionUtility.poiAction();
                    w.sfId = sfId;
                    w.primaryAttributeValueId = listId;
                    w.marketoGUID = actionId;
                    wrappers.add(w);
                }
            }
        }
        
        if (wrappers.size() > 0) {
            poiActionInsert.createPOIActions(wrappers);
        }
    }
    
    //when a lead or contact is inserted/updated, we need to check if there is a marketo id
    // if there is a marketo id we need to run this to create actions from any queues assigned to the lead/contact
    public static void checkPOIActionQueues(sObject[] records, string sObjectType) {
        
        //init list of new poi actions to insert
        Product_Of_Interest_Action__c[] newPOIActions = new list<Product_Of_Interest_Action__c>();
        
        //init list of queues to delete
        Product_Of_Interest_Action_Queue__c[] delQueues = new list<Product_Of_Interest_Action_Queue__c>();
        
        //init a variable to hold the marketoId field
        sObjectField marketoId;
        if (sObjectType == 'Lead') {
            marketoId = Lead.Marketo_ID__c;
        } else {
            marketoId = Contact.Marketo_ID__c;
        }
        
        //add the lead/contact id and marketo ids to a set
        set<string> marketoIds = new set<string>();
        for (sObject r : records) {
            marketoIds.add((string)r.get(marketoId));
        }
        
        //query for all queues with matching marketo ids
        Product_Of_Interest_Action_Queue__c[] allQueues = [SELECT id, Marketo_ID__c, Action__c, Action_ID__c, Add_To_POI_Counter__c, List_ID__c, Needs_Resolution__c, Product__c, Product_Suite__c
                                                           FROM Product_Of_Interest_Action_Queue__c
                                                           WHERE Marketo_ID__c IN : marketoIds
                                                           ORDER BY CreatedDate ASC];
        
        //if queues have been found, start our more advanced logic
        if (allQueues.size() > 0) {
            set<sObject> recordsWithQueuesSet = new set<sObject>();
            sObject[] recordsWithQueues = new list<sObject>();
            for (sObject r : records) {
                for (Product_Of_Interest_Action_Queue__c q : allQueues) {
                    if ((string)r.get(marketoId) == q.Marketo_ID__c) {
                        recordsWithQueuesSet.add(r);
                    }
                }
            }
            if (recordsWithQueuesSet.size() > 0) {
                recordsWithQueues.addAll(recordsWithQueuesSet);
            } 
            
            //find or create pois
            Product_Of_Interest__c[] pois = findCreatePois(recordsWithQueues, sObjectType);
            
            //requery the pois to get ownership info
            set<id> poiIds = new set<id>();
            for (Product_Of_Interest__c poi : pois) {
                poiIds.add(poi.id);
            }
            
            pois = [SELECT id, Lead__c, Contact__c, Marketo_ID__c, POI_Stage_MSDS__c, POI_Stage_AUTH__c, POI_Stage_EHS__c, POI_Stage_ERGO__c, POI_Stage_ODT__c,
                    POI_Status_MSDS__c, POI_Status_AUTH__c, POI_Status_EHS__c, POI_Status_ERGO__c, POI_Status_ODT__c,
                    Contact__r.Account.OwnerId, Contact__r.Account.EHS_Owner__c, Contact__r.Account.Authoring_Account_Owner__c, Contact__r.Account.Online_Training_Account_Owner__c, 
                    Contact__r.Account.Ergonomics_Account_Owner__c, Lead__r.OwnerId, Lead__r.EHS_Owner__c, Lead__r.Authoring_Owner__c, Lead__r.Online_Training_Owner__c, Lead__r.Ergonomics_Owner__c
                    FROM Product_Of_Interest__c
                    WHERE id IN : poiIds];
            
            //once we have our pois, we need to convert the queues into actions
            for (Product_Of_Interest__c poi : pois) {
                
                for (Product_Of_Interest_Action_Queue__c q : allQueues) {
                    
                    //match the queue to the poi with a matching marketo id
                    if (q.Marketo_ID__c == poi.Marketo_ID__c) {
                        
                        //convert the queue and add the poi action to the insert list
                        newPOIActions.add(convertQueueToAction(poi, q));
                        
                        //add the queue to the list to delete
                        delQueues.add(q);
                    }
                }
            }
            
            //add session info, upsert actions
            if (newPOIActions.size () > 0) {
                poiActionSession.insertSession(newPOIActions);
                try {
                    upsert newPOIActions Action_ID__c;
                } catch(exception e) {
                    salesforceLog.createLog('POI', true, 'poiActionUtility', 'checkPOIActionQueues', string.valueOf(e));
                }
            }
            
            //delete the queues
            if (delQueues.size() > 0) {
                try {
                    delete delQueues;
                } catch(exception e) {
                    salesforceLog.createLog('POI', true, 'poiActionUtility', 'checkPOIActionQueues', string.valueOf(e));
                }
            }
            
            
        }
        
    }
    
    //this method takes in a poi record and a queue record and creates a poi action
    public static Product_Of_Interest_Action__c convertQueueToAction(Product_Of_Interest__c poi, Product_Of_Interest_Action_Queue__c q) {
        Product_Of_Interest_Action__c poia = new Product_Of_Interest_Action__c();
        poia.Product_Of_Interest__c = poi.id;
        id ownerId;
        if (poi.Contact__c != null) {
            poia.Contact__c = poi.Contact__c;
            //ownership
            if (q.Product_Suite__c == 'MSDS Management') {
                ownerId = poi.Contact__r.Account.OwnerId;
            } else if (q.Product_Suite__c == 'EHS Management') {
                ownerId = poi.Contact__r.Account.EHS_Owner__c;
            } else if (q.Product_Suite__c == 'MSDS Authoring') {
                ownerId = poi.Contact__r.Account.Authoring_Account_Owner__c;
            } else if (q.Product_Suite__c == 'On-Demand Training') {
                ownerId = poi.Contact__r.Account.Online_Training_Account_Owner__c;
            } else if (q.Product_Suite__c == 'Ergonomics') {
                ownerId = poi.Contact__r.Account.Ergonomics_Account_Owner__c;
            }
        } else {
            poia.Lead__c = poi.Lead__c;
            //ownership
            if (q.Product_Suite__c == 'MSDS Management') {
                ownerId = poi.Lead__r.OwnerId;
            } else if (q.Product_Suite__c == 'EHS Management') {
                ownerId = poi.Lead__r.EHS_Owner__c;
            } else if (q.Product_Suite__c == 'MSDS Authoring') {
                ownerId = poi.Lead__r.Authoring_Owner__c;
            } else if (q.Product_Suite__c == 'On-Demand Training') {
                ownerId = poi.Lead__r.Online_Training_Owner__c;
            } else if (q.Product_Suite__c == 'Ergonomics') {
                ownerId = poi.Lead__r.Ergonomics_Owner__c;
            }
        }
        if (ownerId != null) {
            poia.OwnerId = ownerId;
        }
        poia.Marketo_ID__c = poi.Marketo_ID__c;
        poia.Action_ID__c = q.Action_ID__c;
        poia.Product__c = q.Product__c;
        poia.Action__c = q.Action__c;
        poia.Product_Suite__c = q.Product_Suite__c;
        poia.Add_to_POI_Counter__c = q.Add_To_POI_Counter__c;
        poia.Needs_Resolution__c = q.Needs_Resolution__c;
        //stage
        if (q.Product_Suite__c == 'MSDS Management') {
            poia.Stage__c = poi.POI_Stage_MSDS__c;
        } else if (q.Product_Suite__c == 'EHS Management') {
            poia.Stage__c = poi.POI_Stage_EHS__c;
        } else if (q.Product_Suite__c == 'MSDS Authoring') {
            poia.Stage__c = poi.POI_Stage_AUTH__c;
        } else if (q.Product_Suite__c == 'Ergonomics') {
            poia.Stage__c = poi.POI_Stage_ERGO__c;
        } else if (q.Product_Suite__c == 'On-Demand Training') {
            poia.Stage__c = poi.POI_Stage_ODT__c;
        }
        return poia; 
    }
    
    //this method will take in a list of leads/contacts and either query and return the poi or create a new poi and return it
    public static Product_Of_Interest__c[] findCreatePois(sObject[] records, string sObjectType) {
        set<id> recordIds = new set<id>();
        for (sObject r : records) {
            recordIds.add(r.id);
        }
        Lead[] leadsWithoutPois = new list<Lead>();
        Contact[] contactsWithoutPois = new list<Contact>();
        
        set<Lead> leadsWithoutPOISet = new set<Lead>();
        set<Contact> contactsWithoutPOISet = new set<Contact>();
        
        Product_Of_Interest__c[] pois = new list<Product_Of_Interest__c>();
        //first look for existing pois
        Product_Of_Interest__c[] allPois = [SELECT id, Lead__c, Contact__c, Marketo_ID__c, POI_Stage_MSDS__c, POI_Stage_AUTH__c, POI_Stage_EHS__c, POI_Stage_ERGO__c, POI_Stage_ODT__c,
                                            POI_Status_MSDS__c, POI_Status_AUTH__c, POI_Status_EHS__c, POI_Status_ERGO__c, POI_Status_ODT__c
                                            FROM Product_Of_Interest__c
                                            WHERE Lead__c IN : recordIds
                                            OR Contact__c IN : recordIds];
        
        //loop and match records with any pois found
        for (sObject r : records) {
            
            //if there are pois, loop and look for a match
            if (allPois.size() > 0) {
                Product_Of_Interest__c poi;
                for (Product_Of_Interest__c p : allPois) {
                    if (p.Lead__c == r.id || p.Contact__c == r.id) {
                        poi = p;
                        pois.add(p);
                    }
                }
                //if no match was found, add the contact/lead to the list to create new pois for
                if (poi == null) {
                    if (sObjectType == 'Lead') {
                        leadsWithoutPOISet.add((Lead)r);
                    } else {
                        contactsWithoutPOISet.add((Contact)r);
                    }
                }
            } else {
                //if there were no pois found in the query, add the lead/contact to the list
                if (sObjectType == 'Lead') {
                    leadsWithoutPOISet.add((Lead)r);
                } else {
                    contactsWithoutPOISet.add((Contact)r);
                }
            }
        }
        
        if (leadsWithoutPOISet.size() > 0 || contactsWithoutPOISet.size() > 0) {
            pois.addAll(poiUtility.createPOI(leadsWithoutPOISet, contactsWithoutPOISet));
        }
        
        return pois;
    }
    
    //poi action wrapper
    public class poiAction {
        public string marketoGUID;
        public string leadId;
        public string primaryAttributeValueId;
        public string sfId;
    }
    
    public static void backupPoiActions(Map<id,Product_of_Interest_Action__c[]> poiActionBackupMap){
        Attachment[] actionBackups = new list<Attachment>();
        for(id key : poiActionBackupMap.keyset()){
            id masterId = key;
            Blob body = blob.valueOf(string.valueOf(poiActionBackupMap.get(key)));
            DateTime d = datetime.now();
            String timeStr = d.format('MMMMM dd, yyyy');
            Attachment actionBackup = new Attachment(parentId = masterID , name='Merged Actions'+' - '+timeStr+'.txt', body = body);
            actionBackups.add(actionBackup);
        }
        
        try {
            insert actionBackups;
        } catch(exception e) {
            salesforceLog.createLog('POI', true, 'poiActionUtility', 'backupPoiActions', string.valueOf(e));
        }
    }
    
    public static void updatePoiaOwnership(sObject[] xrecords, string sObjectType) {
        Product_Of_Interest_Action__c[] updList = new list<Product_Of_Interest_Action__c>();
        sObjectField msds;
        sObjectField ehs;
        sObjectField auth;
        sObjectField odt;
        sObjectField ergo;
        sObject[] records = new list<sObject>();
        if (sObjectType != 'Contact') {
            records.addAll(xrecords);
        } else {
            set<id> accountIds = new set<id>();
            for (Contact c : (list<Contact>)xrecords) {
                accountIds.add(c.AccountId);
            }
            for (Account acct : [SELECT id, OwnerId, EHS_Owner__c, Authoring_Account_Owner__c, Online_Training_Account_Owner__c, Ergonomics_Account_Owner__c
                                 FROM Account
                                 WHERE id IN : accountIds]) {
                                     records.add(acct);
                                 }
        }
        set<id> recordIds = new set<id>();
        for (sObject record : records) {
            recordIds.add(record.id);
        }
        string query = 'SELECT id, Product_Suite__c, ';
        if (sObjectType == 'Account' || sObjectType == 'Contact') {
            query += 'Contact__r.AccountId FROM Product_Of_Interest_Action__c WHERE Contact__r.AccountId IN : recordIds AND Session_Open__c = true';
            msds = Account.OwnerId;
            ehs = Account.EHS_Owner__c;
            auth = Account.Authoring_Account_Owner__c;
            odt = Account.Online_Training_Account_Owner__c;
            ergo = Account.Ergonomics_Account_Owner__c;
        } else {
            query += 'Lead__c FROM Product_Of_Interest_Action__c WHERE Lead__c IN : recordIds AND Session_Open__c = true';
            msds = Lead.OwnerId;
            ehs = Lead.EHS_Owner__c;
            auth = Lead.Authoring_Owner__c;
            odt = Lead.Online_Training_Owner__c;
            ergo = Lead.Ergonomics_Owner__c;
        }
        // query our actions
        Product_Of_Interest_Action__c[] allPoias = Database.query(query);
        if (allPoias.size() > 0) {
            for (sObject record : records) {
                Product_Of_Interest_Action__c[] poias = new list<Product_Of_Interest_Action__c>();
                for (Product_Of_Interest_Action__c poia : allPoias) {
                    if (sObjectType == 'Account' || sObjectType == 'Contact') {
                        if (poia.Contact__r.AccountId == record.id) {
                            poias.add(poia);
                        }
                    } else {
                        if (poia.Lead__c == record.id) {
                            poias.add(poia);
                        }
                    }
                } 
                for (Product_Of_Interest_Action__c poia : poias) {
                    id ownerId;
                    if (poia.Product_Suite__c == 'MSDS Management') {
                        ownerId = (id)record.get(msds);
                    } else if (poia.Product_Suite__c == 'EHS Management') {
                        ownerId = (id)record.get(ehs);
                    } else if (poia.Product_Suite__c == 'MSDS Authoring') {
                        ownerId = (id)record.get(auth);
                    } else if (poia.Product_Suite__c == 'On-Demand Training') {
                        ownerId = (id)record.get(odt);
                    } else if (poia.Product_Suite__c == 'Ergonomics') {
                        ownerId = (id)record.get(ergo);
                    }
                    if (ownerId != null) {
                        poia.OwnerId = ownerId;
                    }
                    updList.add(poia);
                }
            } 
        }
        if (updList.size() > 0) {
            update updList;
        }
    }
    
    public static void mergePoiaOwnership(set<id> ids) {
        Product_Of_Interest_Action__c[] updList = new list<Product_Of_Interest_Action__c>();
        Product_Of_Interest_Action__c[] poias = [SELECT id, OwnerId, Product_Suite__c, Contact__c, Contact__r.AccountId, Contact__r.Account.OwnerId, Contact__r.Account.EHS_Owner__c, 
                                                 Contact__r.Account.Authoring_Account_Owner__c, Contact__r.Account.Online_Training_Account_Owner__c, Contact__r.Account.Ergonomics_Account_Owner__c
                                                 FROM Product_Of_Interest_Action__c
                                                 WHERE Contact__r.AccountId IN : ids
                                                 AND Session_Open__c = true];
        if (poias.size() > 0) {
            for (Product_Of_Interest_Action__c poia : poias) {
                id ownerId;
                if (poia.Product_Suite__c == 'MSDS Management') {
                    ownerId = poia.Contact__r.Account.OwnerId;
                } else if (poia.Product_Suite__c == 'EHS Management') {
                    ownerId = poia.Contact__r.Account.EHS_Owner__c;
                } else if (poia.Product_Suite__c == 'MSDS Authoring') {
                    ownerId = poia.Contact__r.Account.Authoring_Account_Owner__c;
                } else if (poia.Product_Suite__c == 'On-Demand Training') {
                    ownerId = poia.Contact__r.Account.Online_Training_Account_Owner__c;
                } else if (poia.Product_Suite__c == 'Ergonomics') {
                    ownerId = poia.Contact__r.Account.Ergonomics_Account_Owner__c;
                }
                if (ownerId == null) {
                    ownerId = poia.Contact__r.Account.OwnerId;
                }
                if (poia.OwnerId != ownerId) {
                    poia.OwnerId = ownerId;
                    updList.add(poia);
                }
            }
        }
        if (updList.size() > 0) {
            update updList;
        }
    }
}