public with sharing class quoteProductFileAttach {
	// the parent object it
	public Id sobjId {get; set;}
	
	// list of existing attachments - populated on demand
	public List<Attachment> attachments;
	
	// list of new attachments to add
	public List<Attachment> newAttachments {get; set;}
	
	// constructor
	public quoteProductFileAttach()
	{
		// instantiate the list with a single attachment
		newAttachments=new List<Attachment>{new Attachment()};
	}	
	
	// retrieve the existing attachments
    public List<Attachment> getAttachments()
    {
    	// only execute the SOQL if the list hasn't been initialised
    	if (null==attachments)
    	{
    		attachments=[select Id, ParentId, Name, Description from Attachment where parentId=:sobjId];
    	}
    	
    	return attachments;
    }
	
	// Save action method
	public void save()
	{
		List<Attachment> toInsert=new List<Attachment>();
		for (Attachment newAtt : newAttachments)
		{
			if (newAtt.Body!=null)
			{
				newAtt.parentId=sobjId;
				toInsert.add(newAtt);
			}
		}
		insert toInsert;
		newAttachments.clear();
		newAttachments.add(new Attachment());
		
		// null the list of existing attachments - this will be rebuilt when the page is refreshed
		attachments=null;
	}
	
	
	/******************************************************
	 *
	 * Unit Tests
	 *
	 ******************************************************/
	 
	private static testMethod void testController()
	{
		Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
                
        // Lookup an existing item
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = '01t80000002NON4';
        myItem.PricebookEntryId__c ='01u80000006eap9';
        myItem.Quantity__c = 1;
        myItem.Quote__c = newQuote.Id;
        insert myItem;
        
		quoteProductFileAttach controller=new quoteProductFileAttach();
		controller.sobjId=myItem.id;
		
		System.assertEquals(0, controller.getAttachments().size());
		
		System.assertEquals(1, controller.newAttachments.size());
				
		// populate the first and third new attachments
		List<Attachment> newAtts=controller.newAttachments;
		newAtts[0].Name='Unit Test 1';
		newAtts[0].Description='Unit Test 1';
		newAtts[0].Body=Blob.valueOf('Unit Test 1');

		controller.save();
		
		System.assertEquals(1, controller.getAttachments().size());
	}
}