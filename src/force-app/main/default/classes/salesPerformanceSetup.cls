/*
* Test class: testSalesPerformanceSetup
*/
public class salesPerformanceSetup {
    //List for Settings to be Saved
    public List<Sales_Incentive_Setting__c> allSettings = new List<Sales_Incentive_Setting__c>();
    //List for Deleted Settings
    public List<Sales_Incentive_Setting__c> deleteSettings = new List<Sales_Incentive_Setting__c>();    
    // RecordType ID
    public RecordType[] recordTypeID {get;set;}
    public String viewAs {get;set;}
    public String viewAsDefaultView {get;set;}
    public string userID {get;set;}
    public User u {get;set;}
    public String defaultView {get;set;}
    public boolean viewReporting {get;set;}
    public string errorMessage {get;set;}
    public boolean isSuccess {get;set;}
    public string alertIcon {get;set;}
    public boolean midMarket {get;set;}
    public boolean enterprise {get;set;}
    public string groupName {get;set;}
    public boolean mmDemoRates {get;set;} 
    public boolean esDemoRates {get;set;}
    public boolean midMarketOpportunityIncentives {get;set;}
    public boolean viewESAssociateSettings {get;set;}
    public boolean viewMMAssociateSettings {get;set;}
    public boolean viewMMDemoRates {get;set;}
    public boolean viewESDemoRates {get;set;}
    public boolean viewMMOpportunityIncentives {get;set;}
    
    public salesPerformanceSetup(){
        //Set Default Variables
        midMarket = FALSE;
        enterprise = FALSE;
        mmDemoRates = FALSE;
        esDemoRates = FALSE;
        midMarketOpportunityIncentives = FALSE;
        //Get the Name of the Page
        String pageName = ApexPages.currentPage().getUrl().substringAfter('apex/');
        
        //Check to see if we are working on the Mid-Market or Enterprise Associate Settings
        if(pageName =='sales_performance_mm_associate_settings'){
            midMarket = TRUE;
            groupName = 'Mid-Market';
        } 
        if(pageName =='sales_performance_es_associate_settings'){
            enterprise = TRUE;
            groupname = 'Enterprise Sales';
        }
        
        //Check to See if we are working on the Mid-Market Demo Rates
        if(pageName =='sales_performance_mm_demo_rates'){
            mmDemoRates = TRUE;
        }
        
        //Check to See if we are working on the ES Demo Rates
        if(pageName =='sales_performance_es_demo_rates'){
            esDemoRates = TRUE;
        }
        
        if(pageName =='sales_performance_mm_opportunity_rates'){
            midMarketOpportunityIncentives = TRUE;
            groupName = 'Mid-Market';
        }
        
        querySettings();
        
        if(allSettings == null){
            allSettings = new list<Sales_Incentive_Setting__c>();
        }
        
        
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        //Check/set permissions
        viewReporting = Sales_Performance_Settings__c.getInstance().Can_View_Reporting__c;
        viewESAssociateSettings = Sales_Performance_Settings__c.getInstance().Can_View_ES_Associate_Settings__c;
        viewMMAssociateSettings = Sales_Performance_Settings__c.getInstance().Can_View_MM_Associate_Settings__c;
        viewMMDemoRates = Sales_Performance_Settings__c.getInstance().Can_View_MM_Demo_Incentives_Rates__c;
        viewESDemoRates = Sales_Performance_Settings__c.getInstance().Can_View_ES_Demo_Incentives_Rates__c;
        viewMMOpportunityIncentives = Sales_Performance_Settings__c.getInstance().Can_View_MM_Opportunity_Settings__c;
        
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias, FirstName, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID 
             FROM User 
             WHERE id = :viewAs LIMIT 1];
        if (defaultView == 'VP' && viewAs == userInfo.getUserId()) {
            u = [SELECT id, Alias, FirstName, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID
                 FROM User
                 WHERE UserRole.Name = 'VP Sales' AND isActive = true and LastName != 'Haling' LIMIT 1];
        }
        userID = u.id;
        //get defaultView to display when viewing as
        viewAsDefaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
    }
    
    public List<Sales_Incentive_Setting__c> getSettings(){
        return allSettings;
    }
    
    public List<SelectOption> getAssociates() {
        set<id> currentAssociateIds = new set<id>();
        for(Sales_Incentive_Setting__c sis:allSettings){
            currentAssociateIds.add(sis.Associate__c);
        }
        User[] assc;
        if(midMarketOpportunityIncentives){
            assc = [Select ID, Name from User where ((isActive = TRUE and Role__c Not In ('Sales Manager','Director') and Group__c=: groupName) OR id in:currentAssociateIds) Order By Name ASC];
        }else{
            assc = [Select ID, Name from User where ((isActive = TRUE and (Role__c='Associate' or Role__c = 'Trainee') and Group__c=: groupName) OR id in:currentAssociateIds) Order By Name ASC];
        }
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for(User sA:assc){
            options.add(new SelectOption(sa.ID,sa.Name));
        }
        return options;
    }
    
    public List<SelectOption> getReps() {
        set<id> currentRepIds = new set<id>();
        for(Sales_Incentive_Setting__c sis:allSettings){
            currentRepIds.add(sis.Sales_Representative__c);
        }
        User[] reps = [Select ID, Name from User where ((isActive = TRUE and Role__c Not In ('Trainee','Associate','Sales Manager','Director') and Group__c=:groupName) OR id in:currentRepIds) Order By Name ASC];
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for(User sr:reps){
            options.add(new SelectOption(sr.ID,sr.Name));
        }
        return options;
    }
    
    public void addSetting(){
        Sales_Incentive_Setting__c sis = new Sales_Incentive_Setting__c();
        sis.RecordTypeID = RecordTypeID[0].ID;
        allSettings.add(sis);
    }
    
    public void removeSetting(){
        isSuccess = TRUE;
        alertIcon = 'success';
        errorMessage = '';
        Integer indexVal = Integer.valueof(system.currentpagereference().getparameters().get('index'));
        //If the Setting is an existing Setting then add it to the list to delete from the databse
        if(allSettings[indexVal].Id != null)
            deleteSettings.add(allSettings[indexVal]);
        //Remove the Setting from the table    
        allSettings.remove(indexVal);  
    } 
    
    public void saveChanges(){
        boolean okToSave = TRUE;
        isSuccess = TRUE;
        alertIcon = 'success';
        errorMessage = '';
        for(Sales_Incentive_Setting__c si : allSettings){
            //Check to Make Sure MM Associate Settings are Filled Out
            if(midMarket || enterprise){
                if(si.Associate__c == NULL || si.Sales_Representative__c == NULL ||
                   si.Percent_Of_Bookings__c == NULL){
                       okToSave = FALSE;
                   }
            }
            //Check to Make Sure ES Associate Settings are Filled Out
            /*if(enterprise) {
if(si.Associate__c == NULL || si.Sales_Representative__c == NULL ||
si.Percent_Of_Bookings__c == NULL || si.Licensing_Goal_Kicker__c == NULL ){
okToSave = FALSE;
}
}*/
            //Check to Make Sure MM Demo Incentives are filled out correctly
            if(mmDemoRates || esDemoRates){
                if(si.Num_of_Demos__c == NULL || si.Demo_Incentive__c == NULL){
                    okToSave = FALSE;
                }
            }
            if(midMarketOpportunityIncentives){
                if(si.Associate__c == NULL || si.Percent_Of_Bookings__c == NULL){
                    okToSave = FALSE;
                }
            }
        }
        
        if(okToSave == TRUE){
            //update existing Setting and insert new ones
            try {
                upsert allSettings;
                querySettings();
                errorMessage ='Records Updated!';
            } catch(System.DMLexception e) {
                errorMessage = string.valueOf(e);
                isSuccess = false;
                alertIcon = 'error';
            }
        } else {
            isSuccess = false;
            alertIcon = 'error';
            //Error Message for MM Associate Settings
            if(midMarket || enterprise){
                errorMessage ='Missing Information... Please make sure that the Associate, Representative & Incentive Rate are filled out.';                
            } 
            //SF-53877
            //Error Message for ES Associate Settings
            /* if(enterprise){
errorMessage ='Missing Information... Please make sure that the Associate, Representative, Incentive Rate & the Licensing Goal Kicker are filled out.';
} */
            //Error Message for MM Demo Rates
            if(mmDemoRates || esDemoRates){
                errorMessage ='Missing Information... Please make sure that the # of Completed Demos & Demo Incentive is filled out.';
            } 
            if(midMarketOpportunityIncentives){
                errorMessage ='Missing Information... Please make sure that the Associate & Incentive Rate is filled out.';
            } 
        }
        //delete the Setting that were removed
        if(deleteSettings.size() > 0){
            delete deleteSettings;
            deleteSettings = new List<Sales_Incentive_Setting__c>();
        }
    } 
    
    public void querySettings(){
        //Get the Mid-Market Associate Record Type ID and the Associate Settings
        if(midMarket){
            recordTypeID = [SELECT ID
                            FROM RecordType
                            WHERE SobjectType = 'Sales_Incentive_Setting__c' and Name='Mid-Market Associate' Limit 1];
            
            allSettings = [SELECT ID, Associate__c, Sales_Representative__c, Percent_Of_Bookings__c 
                           FROM Sales_Incentive_Setting__c 
                           WHERE RecordType.Name='Mid-Market Associate'];
        }
        //Get the Enterprise Associate Record Type ID and the Associate Settings
        if(enterprise){
            recordTypeID = [SELECT ID
                            FROM RecordType
                            WHERE SobjectType = 'Sales_Incentive_Setting__c' and Name='Enterprise Associate' Limit 1];
            
            allSettings = [SELECT ID, Associate__c, Sales_Representative__c, Percent_Of_Bookings__c, Licensing_Goal_Kicker__c 
                           FROM Sales_Incentive_Setting__c 
                           WHERE RecordType.Name='Enterprise Associate'];
        } 
        //Get the Mid-Market Demo Rates and Record Type ID
        if(mmDemoRates){
            recordTypeID = [SELECT ID
                            FROM RecordType
                            WHERE SobjectType = 'Sales_Incentive_Setting__c' and Name='Mid-Market Demo Incentive' Limit 1];
            
            allSettings = [SELECT ID, Num_of_Demos__c, Demo_Incentive__c, Eligible_for_Team_Sales_Incentive__c 
                           FROM Sales_Incentive_Setting__c 
                           WHERE RecordType.Name='Mid-Market Demo Incentive'
                           ORDER BY Num_of_Demos__c ASC];
            
        }
        //Get the Enterprise Demo Rates and Record Type ID
        if(esDemoRates){
            recordTypeID = [SELECT ID
                            FROM RecordType
                            WHERE SobjectType = 'Sales_Incentive_Setting__c' and Name='Enterprise Demo Incentive' Limit 1];
            allSettings = [SELECT ID, Num_of_Demos__c, Demo_Incentive__c, Eligible_for_Team_Sales_Incentive__c 
                           FROM Sales_Incentive_Setting__c 
                           WHERE RecordType.Name='Enterprise Demo Incentive'
                           ORDER BY Num_of_Demos__c ASC];
        }
        
        //Get the Mid Market Opportunity Rates and Record Type ID
        if(midMarketOpportunityIncentives){
            groupName = 'Mid-Market';
            recordTypeID = [SELECT ID
                            FROM RecordType
                            WHERE SobjectType = 'Sales_Incentive_Setting__c' and Name='Mid-Market Opportunity' Limit 1];
            allSettings = [SELECT ID, Associate__c, Percent_Of_Bookings__c, Incentive_Stop_Date__c 
                           FROM Sales_Incentive_Setting__c 
                           WHERE RecordType.Name='Mid-Market Opportunity'
                           ORDER BY Associate__r.Name ASC];
        }
    }
    
}