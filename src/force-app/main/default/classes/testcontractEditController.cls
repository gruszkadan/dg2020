@isTest(seeAllData = true)
private class testcontractEditController {
    
    static testmethod void test1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Subscription_End_Date__c = '10/10';
        insert newAccount;
        
        Account relatedAccount = new Account();
        relatedAccount.Name = 'Related Account';
        relatedAccount.Subscription_End_Date__c = '10/10';
        insert relatedAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact; 
        
        Contact secondContact = new Contact();
        secondContact.FirstName = 'Bob';
        secondContact.LastName = 'Barker';
        secondContact.AccountID = newAccount.Id;
        insert secondContact;
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        //newContract.Billing_Contact__c = newContact.Id;
       // newContract.Shipping_Contact__c = newContact.Id;
        newContract.Other_Special_Payment_Terms__c = FALSE;
        newContract.Opt_Out__c = FALSE;
        newContract.Installments__c = FALSE;
        newContract.Split_Billing__c = FALSE;
        newContract.Modified_Down_Payment__c = FALSE;
        newContract.Non_Standard_Service_Approval__c = FALSE;
        newContract.Deferred_Invoice_Date__c = date.TODAY();
        newContract.Delayed_Billing_Date__c = date.TODAY();
        newContract.Address__c = address.Id;
        newContract.Billing_Country__c = 'United States';
        newContract.Custom_Payment_Net_Terms__c = true;
        newContract.Payment_Net_Terms__c = 'Net 30';
        newContract.Standard_MSA__c = 'Yes';
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        newContract.Initial_Approval_Check__c = FALSE;
        newContract.Related_Contract__c = 'test1';
        newContract.Related_Contract_Type__c = 'Add-On';
        newContract.Related_Contract_Account__c = newAccount.id;
        insert newContract;  
        
        Product2 newProd =  [ SELECT id, Name, Contract_Print_Group__c FROM Product2 WHERE Name = 'HQ Account' LIMIT 1 ];
        
        Contract_Line_Item__c service1 = new Contract_Line_Item__c();
        service1.Contract__c = newContract.Id;
        service1.Product__c = newProd.id;
        service1.Sort_order__c = 1;
        service1.Year_1_Manual__c = 0;
        service1.Year_2_Manual__c = 0;
        service1.Year_3_Manual__c = 0;
        service1.Year_4_Manual__c = 0;
        service1.Year_5_Manual__c = 0;
        service1.Original_Char_Count__c = 10;
        service1.Contract_Terms__c = 'Test terms';
        insert service1;
        
        ApexPages.currentPage().getParameters().put('id', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
        contractEditController myController = new contractEditController(testController);
        myController.theContract = newContract;
 //testing before insert method
 		System.assert(myController.isValid() == false);
 
        myController.theContract.Contact__c = newContact.Id; 
        myController.theContract.Billing_Contact__c = newContact.Id; 
        myController.theContract.Shipping_Contact__c = newContact.Id;
        myController.previousContact = newContact.Id; 
        
        myController.theContract.Contact__c = secondContact.Id;
        myController.setDefaultContact();
        System.assert(myController.theContract.Billing_Contact__c == secondContact.Id);
//end of before insert testing  
//
        myController.theContract.Opt_Out__c = TRUE;
        myController.theContract.Other_Special_Payment_Terms__c = TRUE;
        myController.theContract.Installments__c = TRUE;
        myController.theContract.Split_Billing__c = TRUE;
        myController.theContract.Modified_Down_Payment__c = TRUE;
        myController.theContract.Non_Standard_Service_Approval__c = TRUE;
        myController.theContract.Related_Contract__c = 'test123';
        myController.theContract.Deferred_Invoice__c = true;
        myController.theContract.Delayed_Billing_Only__c = true;
        myController.theContract.Deferred_Invoice_Date__c = date.TODAY() + 100;
        myController.theContract.Delayed_Billing_Date__c = date.TODAY() + 100;
        myController.theContract.Billing_Country__c = 'Russia';
        myController.theContract.Standard_MSA__c = 'No';
        myController.theContract.Contract_Length__c = '1 Year';
        myController.relCon_startDate = '05/05/15';
        myController.relCon_endDate = '05/05/16';
        myController.getContactSelectList();
        myController.setName();
        myController.getFXRates();
        myController.getPaymentNetTerms();
        myController.saveSelectedAccounts();
        myController.addAddress();
        String addressId = address.Id;
        myController.getAddressString(addressId);
        myController.selectedBillingAddressId = address.Id;
        myController.selectedShippingAddressId = address.Id;
        myController.selectBillingAddress();
        myController.selectShippingAddress();
        myController.saveEdits();
        myController.getrelatedContractYesNo();
        myController.multicurrencyDeferralDates();
        myController.theContract.Contract_Type__c = 'Renewal';
        myController.relatedContractEndDate(1);
        
        myController.theContract.Contract_End_Date__c = null;
        myController.relatedContractEndDate(3);
        
        myController.theContract.Standard_Payment_Terms__c = 'Yes';
        myController.resetPaymentTerms();
        
        myController.theContract.Contract_Type__c = 'Renewal';
        myController.theContract.Custom_Payment_Net_Terms__c = true;
        myController.theContract.Payment_Net_Terms__c = 'Net 90';
        myController.theContract.Split_Billing__c = true;
        myController.isValid();
        
        
    }
    
    static testmethod void test2() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Subscription_End_Date__c = '10/10';
        newAccount.Customer_Status__c = 'Active';
        insert newAccount;
        
        Account newChildAccount = new Account();
        newChildAccount.Name = 'Test Account';
        newChildAccount.Subscription_End_Date__c = '10/10';
        newChildAccount.parentId = newAccount.id;
        insert newChildAccount;
        
        Account newChildAccount2 = new Account();
        newChildAccount2.Name = 'Test Account';
        newChildAccount2.Subscription_End_Date__c = '10/10';
        newChildAccount2.parentId = newAccount.id;
        insert newChildAccount2;
        
        Account relatedAccount = new Account();
        relatedAccount.Name = 'Related Account';
        relatedAccount.Subscription_End_Date__c = '10/10';
        relatedAccount.Customer_Status__c = 'Active';
        insert relatedAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact; 
        
        Contact secondContact = new Contact();
        secondContact.FirstName = 'Bob';
        secondContact.LastName = 'Barker';
        secondContact.AccountID = newAccount.Id;
        insert secondContact;
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Billing_Contact__c = newContact.Id;
        newContract.Shipping_Contact__c = newContact.Id;
        
        newContract.Other_Special_Payment_Terms__c = FALSE;
        newContract.Opt_Out__c = FALSE;
        newContract.Installments__c = FALSE;
        newContract.Split_Billing__c = FALSE;
        newContract.Modified_Down_Payment__c = FALSE;
        newContract.Non_Standard_Service_Approval__c = FALSE;
        newContract.Deferred_Invoice_Date__c = date.TODAY();
        newContract.Delayed_Billing_Date__c = date.TODAY();
        newContract.Address__c = address.Id;
        newContract.Billing_Country__c = 'United States';
        newContract.Custom_Payment_Net_Terms__c = true;
        newContract.Payment_Net_Terms__c = 'Net 30';
        newContract.Standard_MSA__c = 'Yes';
        newContract.Num_of_Locations__c = 5;
        newContract.Coverage_Type__c = 'Locations';
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        newContract.Initial_Approval_Check__c = FALSE;
        newContract.Related_Contract__c = 'test1';
        newContract.Related_Contract_Type__c = 'Add-On';
        newContract.Related_Contract_Account__c = newAccount.id;
        insert newContract;  
        
        
        Contract_Account_Junction__c caj = new Contract_Account_Junction__c();
        caj.Account__c = newAccount.Id;
         caj.Contract__c = newContract.Id;
         caj.Product_Platform__c = 'MSDS Management';
        insert caj;
        
        Product2 newProd =  [ SELECT id, Name, Contract_Print_Group__c FROM Product2 WHERE Name = 'HQ Account' LIMIT 1 ];
        
        Contract_Line_Item__c service1 = new Contract_Line_Item__c();
        service1.Contract__c = newContract.Id;
        service1.Product__c = newProd.id;
        service1.Sort_order__c = 1;
        service1.Year_1_Manual__c = 0;
        service1.Year_2_Manual__c = 0;
        service1.Year_3_Manual__c = 0;
        service1.Year_4_Manual__c = 0;
        service1.Year_5_Manual__c = 0;
        service1.Original_Char_Count__c = 10;
        service1.Contract_Terms__c = 'Test terms';
        insert service1;
        
        ApexPages.currentPage().getParameters().put('id', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
        contractEditController myController = new contractEditController(testController);
        myController.theContract = newContract;
 //testing before insert method

        ApexPages.currentPage().getParameters().put('viewingPlatform', 'EHS');
        myController.setViewingPlatform();
        
        myController.saveSelectedAccounts();
        myController.saveSelectedEHSAccounts();
        myController.saveSelectedErgoAccounts();
        myController.theContract.Contract_Start_Type__c = 'Upon Execution';
        myController.accountList[0].selectedForEHS = TRUE;
        myController.accountList[0].selectedForErgo = TRUE;
        myController.accountList[0].selected = TRUE;
       	myController.saveEdits();
        myController.getrelatedContractYesNo();
        myController.setAcct();
     
        
 	/*
        myController.theContract.Contact__c = newContact.Id; 
        myController.theContract.Billing_Contact__c = newContact.Id; 
        myController.theContract.Shipping_Contact__c = newContact.Id;
        myController.previousContact = newContact.Id; 
        
        myController.theContract.Contact__c = secondContact.Id;
        myController.setDefaultContact();
        System.assert(myController.theContract.Billing_Contact__c == secondContact.Id);
//end of before insert testing  
//
        myController.theContract.Opt_Out__c = TRUE;
        myController.theContract.Other_Special_Payment_Terms__c = TRUE;
        myController.theContract.Installments__c = TRUE;
        myController.theContract.Split_Billing__c = TRUE;
        myController.theContract.Modified_Down_Payment__c = TRUE;
        myController.theContract.Non_Standard_Service_Approval__c = TRUE;
        myController.theContract.Related_Contract__c = 'test123';
        myController.theContract.Deferred_Invoice__c = true;
        myController.theContract.Delayed_Billing_Only__c = true;
        myController.theContract.Deferred_Invoice_Date__c = date.TODAY() + 100;
        myController.theContract.Delayed_Billing_Date__c = date.TODAY() + 100;
        myController.theContract.Billing_Country__c = 'Russia';
        myController.theContract.Standard_MSA__c = 'No';
        myController.theContract.Contract_Length__c = '1 Year';
        myController.relCon_startDate = '05/05/15';
        myController.relCon_endDate = '05/05/16';
        myController.getContactSelectList();
        myController.setName();
        myController.getFXRates();
        myController.getPaymentNetTerms();
        
        myController.saveSelectedAccounts();
        myController.addAddress();
        String addressId = address.Id;
        myController.getAddressString(addressId);
        myController.selectedBillingAddressId = address.Id;
        myController.selectedShippingAddressId = address.Id;
        myController.selectBillingAddress();
        myController.selectShippingAddress();
        myController.saveEdits();
        myController.getrelatedContractYesNo();
        myController.multicurrencyDeferralDates();
        myController.theContract.Contract_Type__c = 'Renewal';
        myController.relatedContractEndDate(1);
        
        myController.theContract.Contract_End_Date__c = null;
        myController.relatedContractEndDate(3);
        
        myController.theContract.Standard_Payment_Terms__c = 'Yes';
        myController.resetPaymentTerms();
        
        myController.theContract.Contract_Type__c = 'Renewal';
        myController.theContract.Custom_Payment_Net_Terms__c = true;
        myController.theContract.Payment_Net_Terms__c = 'Net 90';
        myController.theContract.Split_Billing__c = true;
        myController.isValid();
        */
    }
    
    
    
    static testmethod void HumanTechContract() {
        Account newAccount = new Account();
        newAccount.Name = 'Humantech Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Human';
        newContact.LastName = 'Tech';
        newContact.AccountID = newAccount.Id;
        insert newContact; 
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.id;
        newContract.Contact__c = newContact.id;
        newContract.Contract_Length__c = '1 Year';
        newContract.Status__c = 'Approved';
        newContract.A2_Agreement_Type__c = 'New';
        
        insert newContract;
        
        
        Product2 newProd =  [SELECT id, Name FROM Product2 WHERE Name = 'Humantech Office Ergonomics - Single Sign On (SSO)' LIMIT 1 ];
        
        Contract_Line_Item__c service1 = new Contract_Line_Item__c();
        service1.Contract__c = newContract.Id;
        service1.Product__c = newProd.id;
    
        insert service1;
        
        ApexPages.currentPage().getParameters().put('id', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
        contractEditController myController = new contractEditController(testController);
        myController.theContract = newContract;
        
        
        system.assertEquals(true, mycontroller.Reoccurring);
        
        
    }

    
    
}