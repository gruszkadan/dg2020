public with sharing class contractPrint {
    private ApexPages.StandardController controller;
    public String contractId {get;set;}
    public Contract__c theContract {get;set;}
    public List<Contract_Line_Item__c> contractItems {get;set;}
    public List<Contract_Line_Item__c> contractTerms {get;set;}
    public Account theAccount {get;set;}
    public Contact theContact {get;set;}
    public Address__c theAddress {get;set;}
    public string test {get;set;}
    public string terms {get;set;}
    public Integer countTerms {get;set;}
    public Integer contractLength {get;set;}
    public String offerVaildDate {get;set;}
    public boolean showContractNumber {get;set;}
    public string iso {get;set;}
    public contractUtility util {get;set;}
    public List<Id> product2Ids {get;set;}
    public string productPlatforms {get;set;}
    public string velocityEHSAddress {get;set;}
    public string taxID {get;set;}
    public boolean coverageMatch {get;set;}
    public string contractFees {get;set;}
    
    public contractPrint(ApexPages.StandardController stdController) {
        util = new contractUtility();
        contractId = ApexPages.currentPage().getParameters().get('id');
        this.controller = stdController;
        if (contractId != null ) {
            queryContract();
            if (theContract.Billing_Currency__c != null) {
                iso = theContract.Billing_Currency__c;
            } else {
                iso = 'USD';
            }
        } else {
            theContract = (Contract__c)stdController.getRecord();
        }
        
        theAccount = [Select ID, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode from Account where ID =:theContract.Account__c];
        theContact = [Select ID, Name from Contact where ID =:theContract.Contact__c];
        
        queryContractItems();
        //Determine the Product Platforms on the Contract
        productPlatforms = productUtility.findProductSuites(product2Ids);
        
        //Get the VelocityEHS Address that Needs to be used on the COF
        velocityEHSAddress = util.companyAddress(productPlatforms);
        
        //Get the Tax ID
        taxID = util.taxID(productPlatforms, theContract.Billing_Currency__c);
        
        //Check to see if the Coverage Matches across all Platforms
        coverageMatch = util.platformsMatch(productPlatforms, theContract);
        
        //Formats the Contract Fees
        contractFees = util.formatBilingFeesForPrint(theContract);
       
        if(theContract.Contract_Length__c =='1 Year'){
            contractLength = 1;
        } else if (theContract.Contract_Length__c =='2 Years'){
            contractLength = 2;	
        } else if (theContract.Contract_Length__c =='3 Years'){
            contractLength = 3;	
        } else if (theContract.Contract_Length__c =='4 Years'){
            contractLength = 4;
        } else {
            contractLength = 5;	
        }
        
        showContractNumber = FALSE;  
        
        if(theContract.Execute_By__c =='Within 30 Days'){
            Date thirtyDays = date.today().addDays(30);
            offerVaildDate = thirtyDays.format();
        } else {
            offerVaildDate = theContract.Execute_By_Date__c.format();
        }
        contractNumberDisplay();
        if(theContract.Include_COF_Summary_Table__c){
            util.buildSummaries(theContract.COF_Group_By__c, contractItems, theContract.Billing_Currency_Rate__c);
        }
        util.buildTable(theContract.COF_Group_By__c, contractItems, theContract.Billing_Currency_Rate__c, theContract.Include_COF_Summary_Table__c);
        
    }
    
    // query the contract
    public void queryContract() {
        String SobjectApiName = 'Contract__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String additionalFields = ', Sales_Rep__r.Name, Billing_Contact__r.Name, Shipping_Contact__r.Name';
        String query = 'select ' + commaSepratedFields + additionalFields +' from ' + SobjectApiName + ' where ID=\''+contractID+'\' ';
        // return the contract items
        theContract = Database.query(query);
    }
    
    // query the contract items
    public void queryContractItems() {
        String SobjectApiName = 'Contract_Line_Item__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String additionalFields = ', Product__r.ID, Product__r.Name, Product__r.Contract_Print_Group__c, Product__r.ProductCode, Product__r.Contract_Product_Name__c, Product__r.Product_Print_Grouping__c, Product__r.Product_Platform__c, Product__r.Contract_Product_Type__c, contract__r.Quoted_Currency_Rate__c, contract__r.Billing_Currency_Rate__c ';
        String OrderBy = ' Order By Contract_Sort_Order__c ASC';
        String query = 'select ' + commaSepratedFields + additionalFields +' from ' + SobjectApiName + ' where Contract__c=\''+theContract.Id+'\' '+ OrderBy;
        // return the contract items
        contractItems = Database.query(query);
        contractTerms = new List<Contract_Line_Item__c>();
        product2Ids = new List<Id>();
        
        
        for(Contract_Line_Item__c q: contractItems) {
            product2Ids.add(q.Product__r.ID);
            if(q.Contract_Terms_Char_Count__c >0){
                contractTerms.add(q);
            }
            
        }
    }
    
    public void contractNumberDisplay () {
        if (theContract.Status__c =='Approved' || theContract.Status__c =='Out for Signature' || theContract.Status__c =='Signed' || 
            theContract.Status__c =='Sent to Orders' || theContract.Status__c =='Pending Orders' || theContract.Status__c =='Pending Rep Update' || 
            theContract.Status__c =='Order Processed') {
                showContractNumber = TRUE;
            } else {
                showContractNumber = FALSE;
            }
    }

}