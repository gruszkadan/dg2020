@isTest(seeAllData=true)
public class testOrderItemMissingAccounts {
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void test1() {
        Account testAccount = new Account(Name = 'testAccount');
        insert testAccount;
        
        Order_Item__c oi = new Order_Item__c();
        oi.Name = 'Test';
        oi.Invoice_Amount__c = 10;
        oi.Order_Date__c = date.today();
        oi.Order_ID__c = 'test123';
        oi.Order_Item_ID__c = 'test123';
        oi.Month__c = 'January';
        oi.Admin_Tool_Product_Name__c = 'HQ';
        oi.Year__c = '2017';
        oi.Contract_Length__c = 3;
        oi.AccountID__c = '123TEST';
        oi.Term_Start_Date__c = date.today();
        oi.Term_End_Date__c = date.today().addYears(1);
        oi.Term_Length__c = 1;
        oi.Admin_Tool_Order_Status__c = 'A';
        oi.Contract_Number__c = 'test123';
        oi.Sales_Location__c = 'Chicago';
        oi.Admin_Tool_Sales_Rep__c = 'Daniel Gruszka';
        oi.Admin_Tool_Order_Type__c = 'New';
        insert oi;
        
        testAccount.AdminId__c='123TEST';
        update testAccount;
        
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new orderItemMissingAccountsScheduled());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
    }
    
}