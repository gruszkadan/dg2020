public with sharing class contactController {
    private ApexPages.StandardController controller {get; set;}
    public Account theAccount {get;set;}
    public Contact theContact {get;set;}
    public Product_of_Interest__c[] poiList {get;set;}
    public Product_of_Interest__c poi {get;set;}
    public User theUser {get;set;}
    public boolean canEdit {get;set;}
    private msdsCustomSettings helper = new msdsCustomSettings();
    public boolean showQuotes {get;set;}
    public boolean showContracts {get;set;}
    public boolean showSalesInsight {get;set;}
    public string defaultLeadSource {get;set;}
    public boolean isSandbox {get;set;}
    public boolean viewLDR {get;set;}
    public boolean showsurvey {get;set;}
    public boolean showDataDotCom {get;set;}
    public boolean showChangeOfContact {get;set;}
    public boolean viewPOIDetail {get;set;}
    public boolean canCreateLeadsOffContact {get;set;}
    public string poiID {get;set;}
    id ContactId {get;set;}
      
    
    public contactController(ApexPages.StandardController stdController) {
        this.controller = stdController;
        contactId = ApexPages.currentPage().getParameters().get('id');
        Id accountId = ApexPages.currentPage().getParameters().get('accid');
        
        theUser = [Select id, Name, FirstName, Email, ManagerID, Group__c, Department__c, ProfileID, Profile.Name from User where id =: UserInfo.getUserId() LIMIT 1];
        isSandbox = URL.getSalesforceBaseUrl().getHost().left(4).equalsignorecase('c.cs');
        viewLDR =  helper.viewLDR();
        showSurvey = helper.viewsurveys();
        showDataDotCom = helper.viewDataDotCom();
        showChangeOfContact = helper.changeOfContact();
        viewPOIDetail = POI_Security__c.getInstance().View_POI_Detail__c;
        canCreateLeadsOffContact = MSDS_Custom_Settings__c.getInstance().Can_Create_Leads_Off_Contact__c;
        
        if(contactID != null){
            theContact = [select ID, AccountID, CustomerAdministratorID__c, CustomerUserID__c, ES_Email_Domain__c from Contact where ID=:contactId]; 
            theAccount = [select ID, Customer_Status__c from Account where ID=:theContact.AccountId];
            showQuotes = helper.showQuotes();
            showContracts = helper.showContracts();
            showSalesInsight = helper.showSalesInsight();
        } else {
            theContact = new Contact();
            theContact.Lead_Created_Date__c = datetime.now();
            if(accountID != NULL){    
                theAccount = [select ID, Phone, BillingCountry from Account where ID=: accountID];
                theContact.Phone = theAccount.Phone;
                theContact.MailingCountry = theAccount.BillingCountry;
                theContact.AccountId = theAccount.Id;
            }
            if(theUser.Department__c =='Customer Care'){
                defaultLeadSource = helper.defaultLeadSource();
                if(defaultLeadSource != NULL){
                    theContact.Communication_Channel__c = defaultLeadSource;
                } else {
                    theContact.Communication_Channel__c = 'CC - Orders';
                }
            }
        }
        checkEditSettings();
        poiList = new list <Product_of_Interest__c>();
        queryPOI();
        if(poiList.size() > 0){
            poi = poiList[0];
        }
        
    }
    
    public boolean getviewPOI() {
        boolean viewPOI = [SELECT View_POI__c FROM User WHERE id = : UserInfo.getUserId() LIMIT 1].View_POI__c;
        return viewPOI;
    }
    
    public void checkEditSettings() {
        Boolean EditActiveCustomer = helper.editActiveCustomer();
        if(theContact.Id != NULL){
            if(theAccount.Customer_Status__c =='Active' && EditActiveCustomer == true){
                canEdit = true;
            }
            if(theAccount.Customer_Status__c =='Active' && theContact.CustomerAdministratorId__c != null && EditActiveCustomer == false){
                canEdit = false;
            }
            if(theAccount.Customer_Status__c =='Active' && theContact.CustomerAdministratorId__c == null && EditActiveCustomer == false){
                canEdit = true;
            }
            if(theAccount.Customer_Status__c != 'Active'){
                canEdit = true;
            }
        } else {
            if(EditActiveCustomer == true){
                canEdit = true;
            } 
        }
    }
    
    
    public PageReference newContactSavenew() {
        Id accountId = ApexPages.currentPage().getParameters().get('accid');  
        try{
            insert theContact;
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
        if(accountid != NULL){
            PageReference cont1 = new PageReference('/apex/contact_new?accid=' + theAccount.ID);
            cont1.setRedirect(true);
            return cont1;
        } else {
            PageReference cont2 = new PageReference('/apex/contact_new');
            cont2.setRedirect(true);
            return cont2;
        }
    }
    
    public PageReference createNewLead(){
        
        if(theContact.Id != null){
            PageReference lead_new = new PageReference('/apex/lead_new?contactId=' + theContact.Id);
            return lead_new;
        } else{
            return null;        
        }
    }
    
    public PageReference newContactSave() { 
        boolean emailIsES = false;
        try{
            insert theContact;
            Contact updatedContact = [Select ES_Email_Domain__c from Contact where ID=:theContact.Id];
            if (theContact.ES_Email_Domain__c != updatedContact.ES_Email_Domain__c && updatedContact.ES_Email_Domain__c == true ) {
                emailIsES = true;
            }
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
        if(emailIsES){
            return new PageReference('/' + theContact.ID + '?showESEmail=true' );
        }else{
           return new PageReference('/' + theContact.ID); 
        }
    }
    
    public PageReference requestUpdate() {
        PageReference adminToolUpdateRequest = Page.adminToolUpdateRequest;
        adminToolUpdateRequest.setRedirect(true);
        adminToolUpdateRequest.getParameters().put('contactID',theContact.Id);
        adminToolUpdateRequest.getParameters().put('type','c');
        return adminToolUpdateRequest;
    }
    
    public PageReference changeOfContact(){
        PageReference cOc = Page.change_of_contact;
        cOc.setRedirect(true);
        cOc.getParameters().put('contactID',theContact.Id);
        return cOc;
    }
    
    public PageReference saveATUpdate() {
        this.controller.save();
        PageReference adminToolUpdateRequest = Page.adminToolUpdateRequest;
        adminToolUpdateRequest.setRedirect(true);
        adminToolUpdateRequest.getParameters().put('contactID',theContact.Id);
        adminToolUpdateRequest.getParameters().put('type','c');
        return adminToolUpdateRequest;
    }   
    
    public void queryPOI(){
        String SobjectApiName = 'Product_of_Interest__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String poiQuery = 'select ' + commaSepratedFields + ' from ' + SobjectApiName + ' where Contact__c = :ContactId LIMIT 1';
        
        
        poiList = Database.query(poiQuery);
    }
    
    

    //displays warning message upon save when user is mid-market and email domain is changed   
    public PageReference checkEmailThenSave() { 
        boolean emailIsES = false;
        try {
            this.controller.save();
            Contact updatedContact = [Select ES_Email_Domain__c from Contact where ID=:contactId];  
            if (theContact.ES_Email_Domain__c != updatedContact.ES_Email_Domain__c && updatedContact.ES_Email_Domain__c == true && theUser.Group__c =='Mid-Market'){
                emailIsES=true;
            }
        }
        catch (Exception e){
            ApexPages.addMessages(e);
            return null;
        }
        if(emailIsES){
            return new PageReference('/' + theContact.ID + '?showESEmail=true' );
        }else{
            return new PageReference('/' + theContact.ID); 
        }
    }
    
    //redirects older pages to the contact_detail page
    public PageReference redirectToNewPage() { 
        PageReference contactDetail = new PageReference('/' + theContact.ID);
        contactDetail.setRedirect(true);
        return contactDetail;
    }
    
}