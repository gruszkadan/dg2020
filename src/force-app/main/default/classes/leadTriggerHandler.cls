public class leadTriggerHandler {
    
    // BEFORE INSERT
    public static void beforeInsert(Lead[] leads) {
       
        //new List of Leads with an email (email domain)
        Lead[] hasEmail = new list<Lead>();
        for (integer i=0; i<leads.size(); i++) {
            if (leads[i].Domain__c != null){
                hasEmail.add(leads[i]);
            }   
        } 
        
        //set the lead created date
        leadUtility.leadCreatedDateAndGroup(leads); 
        
        //check for enterprise
        leadUtility.leadEnterpriseCheck(leads);
        
        //check for enterprise emails
        leadUtility.leadEnterpriseEmailCheck(hasEmail, TRUE); 
        
        //check for territory and update owners
        territoryUtility.updateTerritory(leads, 'Lead', true, 'insert');
        
        //check for communication channel and set lead source values
        leadSourceUtility.updateLeadSourceValues(leads, 'insert', 'Lead');
        
        // //Opt-In Sales Consulting Email Preference
        emailPreferencesUtility.salesConsultingOptIn(leads, 'Lead');
        
    }
    
    // BEFORE UPDATE
    public static void beforeUpdate(Lead[] oldLeads, Lead[] newLeads) {
        leadUtility.leadCreatedDateAndGroup(newLeads); 
        Lead[] terrLeads = new list<Lead>();
        Lead[] unsubscribedLeads = new list<Lead>();
        Lead[] sourceChangedLeads = new list<Lead>();
        Lead[] ownershipChanges = new list<Lead>();
        Lead[] primarySolutionPOIUpdates = new list<Lead>();
        Lead[] emailUpdates = new List<Lead>();

        
        //check to see if status changed
        Lead[] statusLeads = new list<Lead>();
        
        // Check to see if any POI Statuses changed
        Lead[] poiStatusChanges = new list<Lead>();
        
        for (integer i=0; i<newLeads.size(); i++) {
            //check for postal code change
            if (oldLeads[i].PostalCode != newLeads[i].PostalCode
                || oldLeads[i].NAICS_Code__c != newLeads[i].NAICS_Code__c) {
                    terrLeads.add(newLeads[i]);
                }
            //check to see if unsubscribe all was changed to true
            if(oldLeads[i].Unsubscribe_All__c == FALSE && newLeads[i].Unsubscribe_All__c == TRUE){
                unsubscribedLeads.add(newLeads[i]);
            }
            //check to see if lead source was changed
            if (oldLeads[i].LeadSource != newLeads[i].LeadSource) {
                sourceChangedLeads.add(newLeads[i]);
            }
            
            // The user has the ability to change the ownership manually. If the OwnerId changes, we need to double check to make sure we
            //also set the Chemical Management Owner
            if (oldLeads[i].OwnerId != newLeads[i].OwnerId) {
                ownershipChanges.add(newLeads[i]);
            }
            
            if ((oldLeads[i].Status != newLeads[i].Status) && 
                (newLeads[i].Status == 'Dead - No Actionable Info'
                 || newLeads[i].Status == 'Dead - Solicitation'
                 || newLeads[i].Status == 'Dead - Competitor'
                 || newLeads[i].Status == 'Dead - Out of Business')) {
                     statusLeads.add(newLeads[i]);
                 }
            // If the POI Status changes, we may need to update the stage
            if (oldLeads[i].POI_Status_MSDS__c != newLeads[i].POI_Status_MSDS__c ||
                oldLeads[i].POI_Status_EHS__c != newLeads[i].POI_Status_EHS__c ||
                oldLeads[i].POI_Status_AUTH__c != newLeads[i].POI_Status_AUTH__c ||               
                oldLeads[i].POI_Status_ODT__c != newLeads[i].POI_Status_ODT__c ||
                oldLeads[i].POI_Status_ERGO__c != newLeads[i].POI_Status_ERGO__c) {
                    poiStatusChanges.add(newLeads[i]);
                }
            //check for primary solution poi changes
            if (oldLeads[i].Primary_Solution_Product_of_Interest__c != newLeads[i].Primary_Solution_Product_of_Interest__c) {
                primarySolutionPOIUpdates.add(newLeads[i]);
            }
            //check for email domain changes      
            if (oldLeads[i].Domain__c != newLeads[i].Domain__c ) { 
                emailUpdates.add(newLeads[i]);
            }
            
        }
        
        //update the territory
        if (terrLeads.size() > 0) {
            territoryUtility.updateTerritory(terrLeads, 'Lead', true, 'update');
        }
        
        //unsubscribe everything on the lead
        if (unsubscribedLeads.size() > 0) {
            emailPreferencesUtility.unsubscribeAll(unsubscribedLeads, 'Lead');
        }
        
        //find the new lead source values
        if (sourceChangedLeads.size() > 0) {
            leadSourceUtility.updateLeadSourceValues(sourceChangedLeads, 'update', 'Lead');
        }  
        
        if (ownershipChanges.size() > 0) {
            leadUtility.checkChemMgmtOwners(ownershipChanges);
        }
        if (statusLeads.size() > 0) {
            poiStageStatusUtility.updateStatus(statusLeads);
        }
        
        if (poiStatusChanges.size() > 0) {
            poiStageStatusUtility.updateStageFromStatus(poiStatusChanges, 'Lead');
        }
        
        if (primarySolutionPOIUpdates.size() > 0) {
            campaignMemberUtility.primarySolutionPOI(primarySolutionPOIUpdates, 'Lead');
        }
        
         if (emailUpdates.size() > 0) {
            leadUtility.leadEnterpriseEmailCheck(emailUpdates, FALSE );
        }
        
        poiMergeConversion.personMerge(newLeads, 'Lead');
    }
    
    // BEFORE DELETE
    public static void beforeDelete(Lead[] oldLeads, Lead[] newLeads) {}
    
    // AFTER INSERT
    public static void afterInsert(Lead[] oldLeads, Lead[] newLeads) {
        Lead[] leadsWithMarketoId = new list<Lead>();
        for (integer i=0; i<newLeads.size(); i++) {
            if (newLeads[i].Marketo_ID__c != null) {
                leadsWithMarketoId.add(newLeads[i]);
            }
        }
        if (leadsWithMarketoId.size() > 0) {
            poiUtility.poiMarketoIdUpdate(leadsWithMarketoId, 'Lead');
            poiActionUtility.checkPOIActionQueues(leadsWithMarketoId, 'Lead');
        }
    }
    
    // AFTER UPDATE
    public static void afterUpdate(Lead[] oldLeads, Lead[] newLeads) {
        //check for lead conversion
        Lead[] lcdLeads = new list<Lead>();
        //check for change in marketo id
        Lead[] leadsWithMarketoId = new list<Lead>();
        Lead[] oldStageLeads = new list<Lead>();
        Lead[] newStageLeads = new list<Lead>();
        Lead[] poiScoreUpdates = new list<Lead>();
        Lead[] ownerChanges = new list<Lead>();
        
        for (integer i=0; i<newLeads.size(); i++) {
            if (!oldLeads[i].isConverted && newLeads[i].isConverted) {
                lcdLeads.add(newLeads[i]);
            }
            if (newLeads[i].Marketo_ID__c != null && oldLeads[i].Marketo_ID__c != newLeads[i].Marketo_ID__c) {
                leadsWithMarketoId.add(newLeads[i]);
            }
            //The stage on the contact may be changed by Marketo or a Salesforce event (task, opportunity, etc). We need to go back to our actions and
            // update the appropriate ones with the new stage. This will then fire an update to the POI.
            if (newLeads[i].POI_Stage_MSDS__c != oldLeads[i].POI_Stage_MSDS__c
                || newLeads[i].POI_Stage_AUTH__c != oldLeads[i].POI_Stage_AUTH__c
                || newLeads[i].POI_Stage_EHS__c != oldLeads[i].POI_Stage_EHS__c
                || newLeads[i].POI_Stage_ERGO__c != oldLeads[i].POI_Stage_ERGO__c
                || newLeads[i].POI_Stage_ODT__c != oldLeads[i].POI_Stage_ODT__c
                || newLeads[i].POI_Status_MSDS__c != oldLeads[i].POI_Status_MSDS__c
                || newLeads[i].POI_Status_AUTH__c != oldLeads[i].POI_Status_AUTH__c
                || newLeads[i].POI_Status_EHS__c != oldLeads[i].POI_Status_EHS__c
                || newLeads[i].POI_Status_ERGO__c != oldLeads[i].POI_Status_ERGO__c
                || newLeads[i].POI_Status_ODT__c != oldLeads[i].POI_Status_ODT__c
                || newLeads[i].Num_of_Unable_to_Connect_Tasks_MSDS__c != oldLeads[i].Num_of_Unable_to_Connect_Tasks_MSDS__c
                || newLeads[i].Num_of_Unable_to_Connect_Tasks_AUTH__c != oldLeads[i].Num_of_Unable_to_Connect_Tasks_AUTH__c
                || newLeads[i].Num_of_Unable_to_Connect_Tasks_EHS__c != oldLeads[i].Num_of_Unable_to_Connect_Tasks_EHS__c
                || newLeads[i].Num_of_Unable_to_Connect_Tasks_ERGO__c != oldLeads[i].Num_of_Unable_to_Connect_Tasks_ERGO__c
                || newLeads[i].Num_of_Unable_to_Connect_Tasks_ODT__c != oldLeads[i].Num_of_Unable_to_Connect_Tasks_ODT__c
                || newLeads[i].POI_Status_Change_Date_MSDS__c != oldLeads[i].POI_Status_Change_Date_MSDS__c
                || newLeads[i].POI_Status_Change_Date_AUTH__c != oldLeads[i].POI_Status_Change_Date_AUTH__c
                || newLeads[i].POI_Status_Change_Date_EHS__c != oldLeads[i].POI_Status_Change_Date_EHS__c
                || newLeads[i].POI_Status_Change_Date_ERGO__c != oldLeads[i].POI_Status_Change_Date_ERGO__c
                || newLeads[i].POI_Status_Change_Date_ODT__c != oldLeads[i].POI_Status_Change_Date_ODT__c) {
                    oldStageLeads.add(oldLeads[i]);
                    newStageLeads.add(newLeads[i]);
                }
            // If Marketo updates the contact's score, update any POI's
            if (oldLeads[i].MSDS_Score__c != newLeads[i].MSDS_Score__c ||
                oldLeads[i].EHS_Score__c != newLeads[i].EHS_Score__c ||
                oldLeads[i].AUTH_Score__c != newLeads[i].AUTH_Score__c ||
                oldLeads[i].ODT_Score__c != newLeads[i].ODT_Score__c ||
                oldLeads[i].ERGO_Score__c != newLeads[i].ERGO_Score__c) {
                    poiScoreUpdates.add(newLeads[i]); 
                }
            if (oldLeads[i].OwnerId != newLeads[i].OwnerId ||
                oldLeads[i].EHS_Owner__c != newLeads[i].EHS_Owner__c ||
                oldLeads[i].Authoring_Owner__c != newLeads[i].Authoring_Owner__c ||
                oldLeads[i].Online_Training_Owner__c != newLeads[i].Online_Training_Owner__c ||
                oldLeads[i].Ergonomics_Owner__c != newLeads[i].Ergonomics_Owner__c) {
                    ownerChanges.add(newLeads[i]);
                }
        }
        if (lcdLeads.size() > 0) {
            leadUtility.leadCreatedDateAndSource(lcdLeads);
            poiMergeConversion.conversionPersonFields(lcdLeads);
        }
        if (leadsWithMarketoId.size() > 0) {
            poiUtility.poiMarketoIdUpdate(leadsWithMarketoId, 'Lead');
            poiActionUtility.checkPOIActionQueues(leadsWithMarketoId, 'Lead');
        }
        if (oldStageLeads.size() > 0 && newStageLeads.size() > 0) {
            poiUtility.updateStageStatusUTCStatusChangeDateFromLeadContact(oldStageLeads, newStageLeads);
        }
        if (poiScoreUpdates.size() > 0) {
            poiUtility.poiScoreUpdate(poiScoreUpdates, 'Lead');
        }
        if (ownerChanges.size() > 0) {
            poiActionUtility.updatePoiaOwnership(ownerChanges, 'Lead');
        }
        
    }
    
    // AFTER DELETE
    public static void afterDelete(Lead[] oldLeads, Lead[] newLeads) {
        sObject[] mergedLeads = new list<sObject>();
        for(integer i=0; i<oldLeads.size(); i++){
            if(oldLeads[i].MasterRecordId != null){
                mergedLeads.add(oldLeads[i]);
            }
        }
        if(mergedLeads.size() > 0){
            poiMergeConversion.personMergeCustomSetting(mergedLeads, 'Lead');
        }
    }
    
    // AFTER UNDELETE
    public static void afterUndelete(Lead[] oldLeads, Lead[] newLeads) {}
    
}