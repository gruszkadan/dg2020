@isTest(SeeAllData=true)
private class testNotifications 
{
    
    static testmethod void testNotificationController1() 
    {
        Notification__c newNotif = new Notification__c();
        newNotif.Audience__c = 'Test Team';
        newNotif.Description__c = 'test';
        newNotif.Due_Date__c = date.today() + 15;
        newNotif.Publish_Date__c = datetime.now();
        newNotif.Status__c = 'Scheduled';
        newNotif.Title__c = 'test';
        insert newNotif;
        
        Notification_Settings__c newSet = new Notification_Settings__c();
        newSet.Name = 'Test Team';
        newSet.Display_Group__c = 'Admin';
        newSet.Query__c = 'SELECT id, Name, Role__c, FirstName, LastName, Department__c FROM User WHERE Role__c = \'Salesforce Admin\'';
        insert newSet;
        
        ApexPages.currentPage().getParameters().put('id', newNotif.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Notification__c());
        notificationController con = new notificationController(testController);
        
        con.selAud = newSet.id;
        
        con.getGroupList();
        con.getDeptVals();
        con.getGroupUsers();
        con.manualStatusChange();
        con.saveSchedule();
        con.save();
        con.edit();
        con.reShow();
        con.cancel();
        con.convertToDraft();
        con.upload();
    }
    
    static testmethod void testNotificationController2() 
    {
        Notification__c newNotif = new Notification__c();
        newNotif.Audience__c = 'Specific User(s)';
        newNotif.Selected_Users__c = 'Ryan Werner,';
        newNotif.Description__c = 'test';
        newNotif.Due_Date__c = date.today() + 15;
        newNotif.Publish_Date__c = datetime.now();
        newNotif.Status__c = 'Draft';
        newNotif.Title__c = 'test';
        insert newNotif;
        
        ApexPages.currentPage().getParameters().put('id', newNotif.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Notification__c());
        notificationController con = new notificationController(testController);
        
        con.selAud = 'User';
        con.searchString = 'Ryan';
        con.clearAllUsers = true;
        
        con.getDeptVals();
        con.findUserWrappers();
        con.resetGroup();
        con.addSelUsers();
        con.storeSelUsers();
        con.saveSelUsers();
        con.clearSelected();
        con.saveSchedule();
    }
    
    static testmethod void testNotificationController3() 
    {
        Notification__c newNotif = new Notification__c();
        newNotif.Audience__c = 'Test Team';
        newNotif.Description__c = 'test';
        newNotif.Due_Date__c = date.today() + 15;
        newNotif.Publish_Date__c = datetime.now();
        newNotif.Status__c = 'Draft';
        newNotif.Title__c = 'test';
        insert newNotif;
        
        Notification_Settings__c newSet = new Notification_Settings__c();
        newSet.Name = 'Test Team';
        newSet.Display_Group__c = 'Admin';
        newSet.Query__c = 'SELECT id, Name, Role__c, FirstName, LastName, Department__c FROM User WHERE Role__c = \'Salesforce Admin\'';
        insert newSet;
        
        ApexPages.currentPage().getParameters().put('id', newNotif.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Notification__c());
        notificationController con = new notificationController(testController);
        
        con.selAud = null;
        
        con.saveDraft();
        con.saveSchedule();
    }
    
    static testmethod void testNotificationController4() 
    {
        Notification__c newNotif = new Notification__c();
        newNotif.Audience__c = 'Test Team';
        newNotif.Description__c = 'test';
        newNotif.Due_Date__c = date.today() + 15;
        newNotif.Status__c = 'Draft';
        newNotif.Title__c = 'test';
        insert newNotif;
        
        Notification_Settings__c newSet = new Notification_Settings__c();
        newSet.Name = 'Test Team';
        newSet.Display_Group__c = 'Admin';
        newSet.Query__c = 'SELECT id, Name, Role__c, FirstName, LastName, Department__c FROM User WHERE Role__c = \'Salesforce Admin\'';
        insert newSet;
        
        ApexPages.currentPage().getParameters().put('id', newNotif.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Notification__c());
        notificationController con = new notificationController(testController);
        
        con.selAud = 'User';
        
        con.saveSchedule();
    }
    
    static testmethod void testNotificationController5() 
    {
        Notification__c newNotif = new Notification__c();
        newNotif.Audience__c = 'Test Team';
        newNotif.Description__c = 'test';
        newNotif.Due_Date__c = date.today() + 15;
        newNotif.Publish_Date__c = datetime.now().addHours(-5);
        newNotif.Status__c = 'Draft';
        newNotif.Title__c = 'test';
        insert newNotif;
        
        Notification_Settings__c newSet = new Notification_Settings__c();
        newSet.Name = 'Test Team';
        newSet.Display_Group__c = 'Admin';
        newSet.Query__c = 'SELECT id, Name, Role__c, FirstName, LastName, Department__c FROM User WHERE Role__c = \'Salesforce Admin\'';
        insert newSet;
        
        ApexPages.currentPage().getParameters().put('id', newNotif.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Notification__c());
        notificationController con = new notificationController(testController);
        
        con.selAud = 'User';
        
        con.saveSchedule();
    }
    
    static testmethod void testNotificationController6() 
    {
        Notification__c newNotif = new Notification__c();
        newNotif.Audience__c = 'Test Team';
        newNotif.Description__c = 'test';
        newNotif.Due_Date__c = date.today() - 15;
        newNotif.Publish_Date__c = datetime.now();
        newNotif.Status__c = 'Draft';
        newNotif.Title__c = 'test';
        insert newNotif;
        
        Notification_Settings__c newSet = new Notification_Settings__c();
        newSet.Name = 'Test Team';
        newSet.Display_Group__c = 'Admin';
        newSet.Query__c = 'SELECT id, Name, Role__c, FirstName, LastName, Department__c FROM User WHERE Role__c = \'Salesforce Admin\'';
        insert newSet;
        
        ApexPages.currentPage().getParameters().put('id', newNotif.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Notification__c());
        notificationController con = new notificationController(testController);
        
        con.selAud = 'User';
        
        con.saveSchedule();
    }
    
    static testmethod void testNotificationController7() 
    {
        Notification__c newNotif = new Notification__c();
        newNotif.Audience__c = 'Test Team';
        newNotif.Description__c = 'test';
        newNotif.Due_Date__c = date.today() + 15;
        newNotif.Publish_Date__c = datetime.now().addDays(17);
        newNotif.Status__c = 'Draft';
        newNotif.Title__c = 'test';
        insert newNotif;
        
        Notification_Settings__c newSet = new Notification_Settings__c();
        newSet.Name = 'Test Team';
        newSet.Display_Group__c = 'Admin';
        newSet.Query__c = 'SELECT id, Name, Role__c, FirstName, LastName, Department__c FROM User WHERE Role__c = \'Salesforce Admin\'';
        insert newSet;
        
        ApexPages.currentPage().getParameters().put('id', newNotif.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Notification__c());
        notificationController con = new notificationController(testController);
        
        con.selAud = 'User';
        
        con.saveSchedule();
    }
    
    static testmethod void testNotificationController8() 
    {
        Notification__c newNotif = new Notification__c();
        newNotif.Audience__c = 'Test Team';
        newNotif.Description__c = 'test';
        newNotif.Due_Date__c = date.today() + 15;
        newNotif.Publish_Date__c = datetime.now();
        newNotif.Status__c = 'Draft';
        insert newNotif;
        
        Notification_Settings__c newSet = new Notification_Settings__c();
        newSet.Name = 'Test Team';
        newSet.Display_Group__c = 'Admin';
        newSet.Query__c = 'SELECT id, Name, Role__c, FirstName, LastName, Department__c FROM User WHERE Role__c = \'Salesforce Admin\'';
        insert newSet;
        
        ApexPages.currentPage().getParameters().put('id', newNotif.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Notification__c());
        notificationController con = new notificationController(testController);
        
        con.selAud = 'User';
        
        con.saveSchedule();
    }
    static testmethod void testNotificationHomePageController1() 
    {
        Notification__c newNotif = new Notification__c();
        newNotif.Audience__c = 'Test Team';
        newNotif.Description__c = 'test';
        newNotif.Title__c = 'test title';
        newNotif.Due_Date__c = date.today() + 15;
        newNotif.Publish_Date__c = datetime.now();
        newNotif.Status__c = 'Published';
        insert newNotif;
        
        Notification_Settings__c newSet = new Notification_Settings__c();
        newSet.Name = 'Test Team';
        newSet.Display_Group__c = 'Admin';
        newSet.Query__c = 'SELECT id, Name, Role__c, FirstName, LastName, Department__c FROM User WHERE Role__c = \'Salesforce Admin\'';
        insert newSet;
        
        Notification_Audience__c[] testAuds = new List<Notification_Audience__c>();
        for (integer tI = 0; tI < 22; tI ++)
        {
            Notification_Audience__c newAud = new Notification_Audience__c();
            newAud.Already_Shown__c = false;
            newAud.Notification__c = newNotif.id;
            newAud.User__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
            testAuds.add(newAud);
        }
        insert testAuds;
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new Notification_Audience__c());
        notificationHomePageController con = new notificationHomePageController(testController);
        
        con.nas = testAuds;
        con.na = testAuds[0];
        con.naId = testAuds[0].id;
        
        con.mapToPage();
        con.getStatusList();
        con.checkNewNotif();
        con.checkOldNotif();
        con.viewNotif();
        con.getStatusList();
        con.onLoad();
        
        System.assertEquals(con.numOfPages, 3);
    }
    
    static testmethod void testNotificationHomePageController2() 
    {
        Notification__c newNotif = new Notification__c();
        newNotif.Audience__c = 'Test Team';
        newNotif.Description__c = 'test';
        newNotif.Title__c = 'test title';
        newNotif.Due_Date__c = date.today() + 15;
        newNotif.Publish_Date__c = datetime.now();
        newNotif.Status__c = 'Published';
        insert newNotif;
        
        Notification_Settings__c newSet = new Notification_Settings__c();
        newSet.Name = 'Test Team';
        newSet.Display_Group__c = 'Admin';
        newSet.Query__c = 'SELECT id, Name, Role__c, FirstName, LastName, Department__c FROM User WHERE Role__c = \'Salesforce Admin\'';
        insert newSet;
        
        Notification_Audience__c[] testAuds = new List<Notification_Audience__c>();
        for (integer tI = 0; tI < 22; tI ++)
        {
            Notification_Audience__c newAud = new Notification_Audience__c();
            newAud.Already_Shown__c = false;
            newAud.Notification__c = newNotif.id;
            newAud.User__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
            testAuds.add(newAud);
        }
        insert testAuds;
        
        ApexPages.currentPage().getParameters().put('notif', testAuds[0].id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Notification_Audience__c());
        notificationHomePageController con = new notificationHomePageController(testController);
        
        con.nas = testAuds;
        con.na = testAuds[0];
        con.naId = testAuds[0].id;
        
        con.mapToPage();
        con.getStatusList();
        con.checkNewNotif();
        con.checkOldNotif();
        con.viewNotif();
        con.getStatusList();
        con.onLoad();
        
        System.assertEquals(con.na.id, testAuds[0].id);
    }
}