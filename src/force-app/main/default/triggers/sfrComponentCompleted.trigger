trigger sfrComponentCompleted on Salesforce_Request__c (after update) {
    set<Id> completedIdSet = new set<Id>();
    set<Id> reOpenIdSet = new set<Id>();
    for(Integer i = 0; i < trigger.new.size(); i++){
        if(trigger.old[i].status__c != 'Completed' && trigger.new[i].status__c == 'Completed'){
            completedIdSet.add(trigger.new[i].Id);
        }
        if(trigger.old[i].status__c == 'Completed' && trigger.new[i].status__c != 'Completed'){
            reOpenIdSet.add(trigger.new[i].Id);
        }
    }
    //Runs when completing a request
    if(completedIdSet.size() > 0){
    	Salesforce_Request_Component__c[] components = [SELECT Id, Deployed__c, Deployed_to_MSDS_Dev__c, Deployed_to_Production_Staging__c, Request_Open__c, Salesforce_Request__c, Deployed_to_MSDS_Test__c 
                                                        FROM Salesforce_Request_Component__c WHERE Salesforce_Request__c IN : completedIdSet];
        for(Id sfId : completedIdSet){
            for(Salesforce_Request_Component__c comp : components){
                if(sfId == comp.Salesforce_Request__c){
                    comp.Deployed__c = TRUE;
                    comp.Deployed_to_MSDS_Dev__c = TRUE;
                    comp.Deployed_to_Production_Staging__c = TRUE;
                    comp.Deployed_to_MSDS_Test__c = TRUE;
                    comp.Request_Open__c = FALSE;
                }
            }
        } 
        update components;
    }
    //Runs if we need to re-open a request
    if(reOpenIdSet.size() > 0){
    	Salesforce_Request_Component__c[] components = [SELECT Id, Deployed__c, Deployed_to_MSDS_Dev__c, Deployed_to_Production_Staging__c, Request_Open__c, Salesforce_Request__c, Deployed_to_MSDS_Test__c 
                                                        FROM Salesforce_Request_Component__c WHERE Salesforce_Request__c IN : reOpenIdSet];
        for(Id sfId : reOpenIdSet){
            for(Salesforce_Request_Component__c comp : components){
                if(sfId == comp.Salesforce_Request__c){
                    comp.Request_Open__c = TRUE;
                }
            }
        } 
        update components;
    }
}