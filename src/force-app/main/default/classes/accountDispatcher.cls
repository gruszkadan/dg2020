public class accountDispatcher {
    private final ApexPages.StandardController controller;
    private msdsCustomSettings helper = new msdsCustomSettings();
    public User u {get;set;}
    public boolean viewLDR {get;set;}
    public boolean viewSales {get;set;}
    public boolean viewMin {get;set;}
    
    public accountDispatcher(ApexPages.StandardController stdController) {
        id acctId = ApexPages.currentPage().getParameters().get('id');
        u = [SELECT id, Name, FirstName, Email, ManagerID, Manager.Email, ProfileID, Profile.Name, Group__c
             FROM User
             WHERE id = :UserInfo.getUserId() LIMIT 1];
        viewLDR = helper.viewLDR();
        viewSales = helper.viewSales();
        if (!viewLDR) {
            if (acctId != null) {
                viewMin = [SELECT Use_Minimized_Account_Page__c
                                   FROM Account
                                   WHERE id = :acctId LIMIT 1].Use_Minimized_Account_Page__c;
            }
        } else {
            viewMin = false;
        }
    } 
    
    //detail page redirect
    public PageReference redirect() {  
        if (viewMin) {
            return gotoMin();
        }
        else if (viewSales == true) {
            return gotoSales();
        } else {
            return gotoStandard();
        }
    }
    
    //edit page redirect
    public PageReference redirect2() {  
        if (viewSales == true) {
            return gotoEditSales();
        }
        else {
            return gotoEditStandard();
        }  
    }
    
    //goto account_detail_standard
    public PageReference gotoStandard() {
        PageReference pr = Page.Account_Detail_Standard;
        pr.getParameters().putAll(ApexPages.currentPage().getParameters());
        return pr.setRedirect(true);
    }
    
    //goto account_detail_minimized
    public PageReference gotoMin() {
        PageReference pr = Page.Account_Detail_Minimized;
        pr.getParameters().putAll(ApexPages.currentPage().getParameters());
        return pr.setRedirect(true);
    }
    
    //goto account_detail_sales
    public PageReference gotoSales() {
        PageReference pr = Page.Account_Detail_Sales;
        pr.getParameters().putAll(ApexPages.currentPage().getParameters());
        return pr.setRedirect(true);
    }
    
    //goto account_edit_standard
    public PageReference gotoEditStandard() {
        PageReference pr = Page.Account_Edit_Standard;
        pr.getParameters().putAll(ApexPages.currentPage().getParameters());
        return pr.setRedirect(true);
    } 
    
    //goto account_edit_sales
    public PageReference gotoEditSales() {
        PageReference pr = Page.Account_Edit_Sales;
        pr.getParameters().putAll(ApexPages.currentPage().getParameters());
        return pr.setRedirect(true);
    }
    
}