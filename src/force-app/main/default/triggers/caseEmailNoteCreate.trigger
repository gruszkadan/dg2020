trigger caseEmailNoteCreate on EmailMessage (after insert) {
    
    // list to add notes to for inserting
    Case_Notes__c[] notes = new List<Case_Notes__c>();
    
    // set to hold parent ids for querying
    Set<id> parentIds = new Set<id>();
    
    // loop through the emails and add the parent ids (cases) to the set
    for (EmailMessage message : trigger.new) {
        parentIds.add(message.ParentId);
    }
    
    Case_Notes__c[] otherNotes = [SELECT id, CreatedDate, Issue__c, Case__c
                             FROM Case_Notes__c
                             WHERE Case__c IN : parentIds
                             ORDER BY CreatedDate ASC];
    
    // query the case issues for each case
    Case_Issue__c[] issues = [SELECT id, Associated_Case__c, Associated_Case__r.Email2Case_Selected_Case_Issue_Id__c
                              FROM Case_Issue__c
                              WHERE Associated_Case__c IN :parentIds];
    
    // set to hold ids of cases needing to be cleared
    Set<id> resetCaseIds = new Set<id>();
    
    for (EmailMessage message : trigger.new) {
        if(message.Num_of_Docs__c == 0 || message.Num_of_Docs__c == null ){
            Case_Issue__c[] foundIssues = new List<Case_Issue__c>();
            string issueId = null;
            if (issues.size() > 0) {
                for (Case_Issue__c issue : issues) {
                    if (issue.Associated_Case__c == message.ParentId) {
                        foundIssues.add(issue);
                        if (issueId == null) {
                            if (issue.Associated_Case__r.Email2Case_Selected_Case_Issue_Id__c != null) {
                                issueId = issue.Associated_Case__r.Email2Case_Selected_Case_Issue_Id__c;
                                resetCaseIds.add(issue.Associated_Case__c);
                            }
                        }
                    }
                }
            }
            
            string substring1 = '--------------- Original Message ---------------';
            string substring2 = '-----Original Message-----';
            string substring3a = 'Support Specialist';
            string strippedNoteBody = message.TextBody.substringBefore(substring1).substringBefore(substring2).substringBefore(substring3a);
            
            Case_Notes__c note = new Case_Notes__c();
            note.Case__c = message.ParentID;
            note.Issue__c = strippedNoteBody;
            note.Full_Email_Text__c = message.TextBody;
            
            // if only one issue is found
            if (foundIssues.size() == 1) {
                note.Associated_Case_Issue__c = foundIssues[0].id;
            }
            if (message.Incoming) {
                note.Notes_Type__c = 'Email - Inbound';
            } else {
                if (foundIssues.size() > 1) {
                    note.Associated_Case_Issue__c = issueId;
                }
                note.Notes_Type__c = 'Email - Outbound';
                note.CreatedById = message.CreatedById;
            }
            notes.add(note);
        }
    }
    if (notes.size() > 0) {
        insert notes;
    }
    // clear out the case issue field
    if (resetCaseIds.size() > 0) {
        Case[] updCases = [SELECT id, Email2Case_Selected_Case_Issue_Id__c
                           FROM Case
                           WHERE id IN :resetCaseIds];
        for (Case c : updCases) {
            c.Email2Case_Selected_Case_Issue_Id__c = null;
        }
        update updCases;
    }
    
    
}