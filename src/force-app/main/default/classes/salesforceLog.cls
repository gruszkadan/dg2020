public class salesforceLog {
    
    public static void createLog(string program, boolean isError, string logClass, string logMethod, string logMessage){
        Integer sizeOfLogMessage = logMessage.length();
        Salesforce_Log__c log = new Salesforce_Log__c();
        if(isError){
            log.Type__c = 'Error';
            log.Program__c = program;
            log.Class__c = logClass;
            log.Method__c = logMethod;
            if(sizeOfLogMessage >131000){
                log.Log__c ='See Attachment'; 
            } else {
                log.Log__c = logMessage;
            }
            insert log;
            if(sizeOfLogMessage >131000){
                Attachment logAttachment = new Attachment(parentId = log.Id , name= log.Name+'.txt', body = blob.valueOf(logMessage));
                try {
                    insert logAttachment;
                } catch(exception e) {
                    
                }
            }
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {'salesforceops@ehs.com'};
            message.subject = 'Salesforce Log - Error Found';
            message.setHtmlBody('The following error has been logged:<br/><br/>'+
                '<u>Program:</u>&nbsp;&nbsp;'+program+'<br/>'+
                '<u>Class:</u>&nbsp;&nbsp;'+logClass+'<br/>'+
                '<u>Method:</u>&nbsp;&nbsp;'+logMethod+'<br/>'+
                '<u>Error:</u>&nbsp;&nbsp;'+logMessage+'<br/>'+
                '<br/><br/>'+
                '<a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+log.id+'">Link To Log</a>');
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        }else if(!isError){
            log.Type__c = 'Log';
            log.Program__c = program;
            log.Class__c = logClass;
            log.Method__c = logMethod;
             if(sizeOfLogMessage >131000){
               log.Log__c ='See Attachment'; 
            } else {
            	log.Log__c = logMessage;
            }
            insert log;
            if(sizeOfLogMessage >131000){
                Attachment logAttachment = new Attachment(parentId = log.Id , name= log.Name+'.txt', body = blob.valueOf(logMessage));
                try {
                    insert logAttachment;
                } catch(exception e) {
                    
                }
            }
        }
    }
}