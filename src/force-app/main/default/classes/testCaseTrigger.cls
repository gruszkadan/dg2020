@isTest()
public class testCaseTrigger {
    static testMethod void test1() {
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //test case
        Case newCase = new Case();
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
        newCase.AccountId = newAcct.id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Subject = 'Test';
        newCase.Type = 'CC Support';
        newCase.Origin = 'Web Form';
        newCase.Web_Form_Data__c = 'Subject: Technical Assistance||First Name: Jonathan||Last Name: PiperC||Company: VelocityEHS||ZipCode: 60654||Phone: 555-555-5555||Email: jpiper@ehs.com||Comments: I need help.';
        insert newCase;
        
        Case_Issue__c[] newIssues = new List<Case_Issue__c>();
        for (integer i=0; i<2; i++) {
            Case_Issue__c newIssue = new Case_Issue__c();
            newIssue.Associated_Case__c = newCase.id;
            newIssue.Product_In_Use__c = 'GM';
            newIssue.Product_Support_Issue__c = 'GM Support Issue';
            newIssue.Product_Support_Issue_Locations__c = 'GM Support Location';
            newIssues.add(newIssue);
        }
        insert newIssues;
        
        newCase.Subject = 'Test2';
        update newCase;
        
    }
    
    
    //Checks validation rule in CaseUtiltiy
    static testMethod void test2() {
        //for caseUtility Validation
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //testing validation rules in caseUtiltiy
        Case newCase = new Case();
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
        newCase.AccountId = newAcct.id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Subject = 'Test';
        newCase.Type = '9.0 Upgrade Training';
        
        boolean hasErrorInsertingA = false;
        boolean hasErrorInsertingB = false;
        boolean hasErrorInsertingC = false;
        
        //insert will fail - no open project
        try{
            insert newCase ;
        }catch(DmlException ex){
            hasErrorInsertingA = true;
        }
        System.assert(hasErrorInsertingA == true);
        
        //insert will fail - no open project 
        VPM_Project__c project = new VPM_Project__c(Account__c = newAcct.Id,	Project_Status__c = 'Closed' , Type__c = 'Chemical Management Upgrade' );
        insert project;
        
        try{
            insert newCase ;
        }catch(DmlException ex){
            hasErrorInsertingB = true;
        }
        System.assert(hasErrorInsertingB == true);   
        
        //insert project and case
        project.Project_Status__c = 'Open';
        update project;
        insert newCase;
        
        //insert will fail - only one 9.0 upgrade per account
        try{
            Case anotherCase = new Case();
            anotherCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
            anotherCase.AccountId = newAcct.id;
            anotherCase.ContactId = newCon.Id;
            anotherCase.Status = 'Active';
            anotherCase.Subject = 'Test';
            anotherCase.Type = '9.0 Upgrade Training';
            insert anotherCase ;
        }catch(DmlException ex){
            hasErrorInsertingC = true;
        }
        System.debug(hasErrorInsertingC);
        
    }
    
    
    
    
    static testMethod void test3() {
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //test project - type 9.0 Upgrade Requires a open project
        VPM_Project__c openProject = new VPM_Project__c(Account__c = newAcct.Id,	Project_Status__c = 'Open' , Type__c = 'Chemical Management Upgrade');
        insert openProject;
        
        
        //test case
        Case newCase = new Case();
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
        newCase.AccountId = newAcct.id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Subject = 'Test';
        newCase.Type = '9.0 Upgrade Training';
        insert newCase;
        
        VPM_Milestone__c milestonesExist = [Select Status__c from VPM_Milestone__c where Case__c = :newCase.Id ];
        System.assert(milestonesExist.Status__c ==  'Open');
        
        newCase.Status =  'Closed';
        newCase.Case_Resolution__c = 'Test 2';
        update newCase;
        VPM_Milestone__c NoMilestonesExist = [Select Status__c from VPM_Milestone__c where Case__c = :newCase.Id ];
        System.assert(NoMilestonesExist.Status__c ==  'Closed');        
    }
    
    static testMethod void test4() {
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //test project - type 9.0 Upgrade Requires a open project
        VPM_Project__c openProject = new VPM_Project__c(Account__c = newAcct.Id,	Project_Status__c = 'Open', Type__c = 'Chemical Management Upgrade');
        insert openProject;
        
        
        //test case
        Case newCase = new Case();
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
        newCase.AccountId = newAcct.id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Closed';
        newCase.Case_Resolution__c = 'Test 3';
        newCase.Subject = 'Test';
        newCase.Type = '9.0 Upgrade Training';
        insert newCase;
        
        VPM_Milestone__c milestonesExist = [Select Status__c from VPM_Milestone__c where Case__c = :newCase.Id ];
        System.assert(milestonesExist.Status__c ==  'Closed');
    }
    
    
    //test 5 tests the updateProjectStatusTimeStamps method
    static testMethod void test5PartA() {
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //test case
        Case CaseA = new Case();
        CaseA.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
        CaseA.AccountId = newAcct.id;
        CaseA.ContactId = newCon.Id;
        CaseA.Status = 'Active';
        CaseA.Subject = 'Test';
        CaseA.Type = 'Compliance Services';
        CaseA.Origin = 'Web Form';
        CaseA.Web_Form_Data__c = 'Subject: Technical Assistance||First Name: Jonathan||Last Name: PiperC||Company: VelocityEHS||ZipCode: 60654||Phone: 555-555-5555||Email: jpiper@ehs.com||Comments: I need help.';
        insert CaseA;
        
        //test case #2
        Case CaseB = new Case();
        CaseB.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
        CaseB.AccountId = newAcct.id;
        CaseB.ContactId = newCon.Id;
        CaseB.Status = 'Active';
        CaseB.Subject = 'Test';
        CaseB.Type = 'Ergo Consultation';
        CaseB.Project_Status__c = 'CF - Assigned';
        CaseB.Origin = 'Web Form';
        CaseB.Web_Form_Data__c = 'Subject: Technical Assistance||First Name: Jonathan||Last Name: PiperC||Company: VelocityEHS||ZipCode: 60654||Phone: 555-555-5555||Email: jpiper@ehs.com||Comments: I need help.';
        insert CaseB;
        
        Case[] UpdateList = new List<Case>();
        
        CaseA.Project_Status__c =  'CF - Pending Onsite';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'Pending Consultation';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'CF - Scheduling Initial Call';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'Action Required - Measurements';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        //check that the date is not set for Action Required – Measurements on Case A and that the datetime is set to today for CaseB
        Case CheckCaseA = [Select Action_Required_Measurements_Date__c FROM CASE where id = :CaseA.id];
        Case CheckCaseB = [Select Action_Required_Measurements_Date__c FROM CASE where id = :CaseB.id];
        System.assert(CheckCaseA.Action_Required_Measurements_Date__c == null);
        System.assert(CheckCaseB.Action_Required_Measurements_Date__c.day() == System.now().day());
        System.assert(CheckCaseB.Action_Required_Measurements_Date__c.month() == System.now().month());
        System.assert(CheckCaseB.Action_Required_Measurements_Date__c.year() == System.now().year());
        
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'CF - Pending Initial CS Service';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'Action Required - Consultation';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'Client - Submission Pending';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'In Progress - Ergonomist';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'See Case Notes - Escalation';
        UpdateList.add(CaseA);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'Client - Project Review';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'Client - Delivered';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'Ops - Account Prep';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'Cancelled - No Response';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'CF - Account Review';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'Cancelled - Per Request';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        
        Case CheckTwoCaseA = [Select CF_Pending_Initial_CS_Service_Date__c, Client_Project_Review_Date__c, CF_Account_Review_Date__c FROM CASE where id = :CaseA.id];
        Case CheckTwoCaseB = [Select Cancelled_Per_Request_Date__c, In_Progress_Ergonomist_Date__c, Action_Required_Consultation_Date__c, Client_Project_Review_Date__c FROM CASE where id = :CaseB.id];
        System.assert(CheckTwoCaseA.CF_Pending_Initial_CS_Service_Date__c != null);
        System.assert(CheckTwoCaseA.Client_Project_Review_Date__c != null);
        System.assert(CheckTwoCaseA.CF_Account_Review_Date__c != null);
        System.assert(CheckTwoCaseB.Cancelled_Per_Request_Date__c != null);
        System.assert(CheckTwoCaseB.In_Progress_Ergonomist_Date__c != null);
        System.assert(CheckTwoCaseB.Action_Required_Consultation_Date__c != null);
        System.assert(CheckTwoCaseB.Client_Project_Review_Date__c == null);
        
        
    }
    
    static testMethod void test5PartB() {
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //test case
        Case CaseA = new Case();
        CaseA.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
        CaseA.AccountId = newAcct.id;
        CaseA.ContactId = newCon.Id;
        CaseA.Status = 'Active';
        CaseA.Subject = 'Test';
        CaseA.Type = 'Compliance Services';
        CaseA.Origin = 'Web Form';
        CaseA.Web_Form_Data__c = 'Subject: Technical Assistance||First Name: Jonathan||Last Name: PiperC||Company: VelocityEHS||ZipCode: 60654||Phone: 555-555-5555||Email: jpiper@ehs.com||Comments: I need help.';
        insert CaseA;
        
        //test case #2
        Case CaseB = new Case();
        CaseB.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
        CaseB.AccountId = newAcct.id;
        CaseB.ContactId = newCon.Id;
        CaseB.Status = 'Active';
        CaseB.Subject = 'Test';
        CaseB.Type = 'Ergo Consultation';
        CaseB.Project_Status__c = 'CF - Assigned';
        CaseB.Origin = 'Web Form';
        CaseB.Web_Form_Data__c = 'Subject: Technical Assistance||First Name: Jonathan||Last Name: PiperC||Company: VelocityEHS||ZipCode: 60654||Phone: 555-555-5555||Email: jpiper@ehs.com||Comments: I need help.';
        insert CaseB;
        
        Case[] UpdateList = new List<Case>();
        
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'Ops - Vender Prep';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'No Show';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'Ops - Obtainment w Indexing';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'On Leave';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'Ops - Post Vendor';
        UpdateList.add(CaseA);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'Ops - Waiting on Dev';
        UpdateList.add(CaseA);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'Ops - Standard Indexing';
        UpdateList.add(CaseA);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'Ops - Special Indexing';
        UpdateList.add(CaseA);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'CF - Pending Delivery';
        UpdateList.add(CaseA);
        Update UpdateList; 
        
        Case CheckTwoCaseA = [Select Ops_Obtainment_w_Indexing_Date__c, Ops_Special_Indexing_Date__c, Ops_Standard_Indexing_Date__c, Ops_Waiting_on_Dev_Date__c FROM CASE where id = :CaseA.id];
        Case CheckTwoCaseB = [Select Ops_Waiting_on_Dev_Date__c, On_Leave_Date__c FROM CASE where id = :CaseB.id];
        System.assert(CheckTwoCaseA.Ops_Obtainment_w_Indexing_Date__c != null);
        System.assert(CheckTwoCaseA.Ops_Special_Indexing_Date__c != null);
        System.assert(CheckTwoCaseA.Ops_Standard_Indexing_Date__c != null);
        System.assert(CheckTwoCaseA.Ops_Waiting_on_Dev_Date__c != null);
        System.assert(CheckTwoCaseB.Ops_Waiting_on_Dev_Date__c == null);
        System.assert(CheckTwoCaseB.On_Leave_Date__c != null);
        
    }
    
    
    static testMethod void test5PartC() {
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //test case
        Case CaseA = new Case();
        CaseA.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
        CaseA.AccountId = newAcct.id;
        CaseA.ContactId = newCon.Id;
        CaseA.Status = 'Active';
        CaseA.Subject = 'Test';
        CaseA.Type = 'Compliance Services - Print Binder';
        CaseA.Project_Status__c = 'Vendor - In Progress'; 
        CaseA.Origin = 'Web Form';
        CaseA.Web_Form_Data__c = 'Subject: Technical Assistance||First Name: Jonathan||Last Name: PiperC||Company: VelocityEHS||ZipCode: 60654||Phone: 555-555-5555||Email: jpiper@ehs.com||Comments: I need help.';
        insert CaseA;
        
        //test case #2
        Case CaseB = new Case();
        CaseB.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
        CaseB.AccountId = newAcct.id;
        CaseB.ContactId = newCon.Id;
        CaseB.Status = 'Active';
        CaseB.Subject = 'Test';
        CaseB.Type = 'Compliance Services - Onsite';
        CaseB.Origin = 'Web Form';
        CaseB.Web_Form_Data__c = 'Subject: Technical Assistance||First Name: Jonathan||Last Name: PiperC||Company: VelocityEHS||ZipCode: 60654||Phone: 555-555-5555||Email: jpiper@ehs.com||Comments: I need help.';
        insert CaseB;
        
        Case[] UpdateList = new List<Case>();
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'CF - Order Shipped';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'CF - Confirm Onsite Date';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c = 'CF - Order to Vendor';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'Ops - List Submitted';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        UpdateList = new List<Case>();
        CaseA.Project_Status__c =  'CF - Confirm Receipt';
        UpdateList.add(CaseA);
        CaseB.Project_Status__c =  'CF - List Sent to Client';
        UpdateList.add(CaseB);
        Update UpdateList;
        
        CaseB.Project_Status__c =  'CF - Waiting for Invoice';
        Update CaseB;
        
        
        Case CheckTwoCaseA = [Select CF_Order_Shipped_Date__c, CF_Order_to_Vendor_Date__c FROM CASE where id = :CaseA.id];
        Case CheckTwoCaseB = [Select CF_Confirm_Onsite_Date__c, CF_List_Sent_to_Client_Date__c, CF_Waiting_for_Invoice_Date__c FROM CASE where id = :CaseB.id];
        System.assert(CheckTwoCaseA.CF_Order_Shipped_Date__c != null);
        System.assert(CheckTwoCaseA.CF_Order_to_Vendor_Date__c != null);
        System.assert(CheckTwoCaseB.CF_Confirm_Onsite_Date__c != null);
        System.assert(CheckTwoCaseB.CF_List_Sent_to_Client_Date__c != null);
        System.assert(CheckTwoCaseB.CF_Waiting_for_Invoice_Date__c != null);
        
    }
    
    
    static testMethod void TestRevRecs1(){
        
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //Find a case owner
        id caseOwner = [SELECT id FROM User WHERE LastName = 'Wills' LIMIT 1].id; 
        
        Case newCase = new Case();
        newCase.OwnerID = caseOwner;
        newCase.AccountId = newAcct.Id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Subject = 'Test';
        newCase.Type = 'Compliance Services - Update Services';
        newCase.Project_Status__c = 'CF - Assigned';
        newCase.Total_Project_Amount__c = 1000;
        insert newCase;
        
        
        
        
    }
    
    static testMethod void TestRevRecs2(){
        
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //Find a case owner
        id caseOwner = [SELECT id FROM User WHERE LastName = 'Wills' LIMIT 1].id; 
        
        Case newCase = new Case();
        newCase.OwnerID = caseOwner;
        newCase.AccountId = newAcct.Id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Subject = 'Test';
        newCase.Type = 'Compliance Services';
        insert newCase;
        
        newCase.Total_Project_Amount__c = 2000;
        newCase.Project_Status__c = 'CF - Assigned';
        newCase.Type = 'Compliance Services - Update Services';
        update newCase;
        
        
    }
    
    static testMethod void TestRevRecs3(){
        
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //Find a case owner
        id caseOwner = [SELECT id FROM User WHERE LastName = 'Wills' LIMIT 1].id; 
        
        Case newCase = new Case();
        newCase.OwnerID = caseOwner;
        newCase.AccountId = newAcct.Id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Subject = 'Test';
        newCase.Type = 'Compliance Services';
        newCase.Total_Project_Amount__c = 2000;
        newCase.Project_Status__c = 'CF - Assigned';
        
        
        insert newCase;
        
        newCase.Primary_Project_Type__c = 'BVA';
        update newCase;
        
        
    }
    
    
    
    
}