trigger trainingDate on Case_Notes__c (after insert, after update) {
	
	Map<String, Case_Notes__c> cNoteMap = new Map<String, Case_Notes__c>();
    
    for (Case_Notes__c cNote : System.Trigger.new)
	{ 	
		Case c = [SELECT ID, AccountID, Type from Case where ID = :cNote.Case__c];
        if( c.Type == 'IMP: HQ' || c.Type == 'Delivery Follow Up' || c.Type == 'Follow Up' )
        {
		Account acct = [SELECT ID, First_Training_Date__c, Last_Training_Date__c from Account where ID = :c.AccountID];
	 	Case_Notes__c[] firstTraining = [SELECT CreatedDate from Case_Notes__c where Case__r.AccountID=:c.AccountID and (Case__r.Type ='IMP: HQ' or Case__r.Type ='Delivery Follow Up' or Case__r.Type ='Follow Up') and Training__c =1 Order By CreatedDate ASC Limit 1];	 	
	 	Case_Notes__c[] lastTraining = [SELECT CreatedDate from Case_Notes__c where Case__r.AccountID=:c.AccountID and (Case__r.Type ='IMP: HQ' or Case__r.Type ='Delivery Follow Up' or Case__r.Type ='Follow Up') and Training__c =1 Order By CreatedDate DESC Limit 1];
	 {
	 	if(cNote.Notes_Type__c == 'Training' || cNote.Notes_Type__c == 'Training - Client Waived' || cNote.Notes_Type__c == 'Webinar - Training')
			{
				if(firstTraining.size() >0 && lastTraining.size() >0 && acct.Id !=NULL)
				{
				acct.Last_Training_Date__c = lastTraining[0].CreatedDate;
				acct.First_Training_Date__c = firstTraining[0].CreatedDate;
				}
			}  
			update acct;			
		}
        }
	 }
}