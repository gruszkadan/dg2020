public with sharing class quoteNew {
    public Account currentAccount {get;set;}
    public Contact currentContact {get;set;}
    public Opportunity currentOpportunity {get;set;}
    public CampaignMember[] campaign {get;set;}
    public CampaignMember primaryCampaign {get;set;}
    public Campaign unknownCampaign {get;set;}   
    public User u {get;set;}
    public Renewal__c renewal {get;set;}
    public string selCurrency {get;set;}
    public string selPromo {get;set;}
    public Quote__c quote;
    PricebookEntry[] entries;
    prodWrapper[] wrappers;
    public Quote_Promotion__c promo {get;set;}
    public Quote_Promotion_Product__c[] promoProds {get;set;}
    
    public quoteNew(ApexPages.StandardController stdController) {
        this.quote = (Quote__c)stdController.getRecord();
        wrappers = new list<prodWrapper>();
        Id accountID = System.currentPageReference().getParameters().get('CF00N800000055Rbg_lkid');
        Id contactID = System.currentPageReference().getParameters().get('CF00N800000055Rbj_lkid');    
        Id opportunityID = System.currentPageReference().getParameters().get('CF00N800000055Rcb_lkid');
        String renewalID = System.currentPageReference().getParameters().get('renewal');
        String ameID = System.currentPageReference().getParameters().get('ameId');
        currentAccount = [Select ID, Name, Num_of_Employees__c, Num_of_Beds__c from Account where ID =:accountID];
        unknownCampaign = [Select ID, Name from Campaign where Name='Unknown' limit 1];
        u = [Select id, Name, LastName, Email, ManagerID, Department__c, Group__c from User where id =: UserInfo.getUserId() LIMIT 1];
        if(renewalID != NULL){
            renewal = [Select ID from Renewal__c where ID=:renewalID];
        }
        quote.Department__c = u.Department__c;
        if(renewalID != NULL){
            quote.Type__c ='Renewal';
            quote.Renewal__c = renewal.Id;
        }
        if(ameID != NULL){
            quote.Account_Management_Event__c = ameId;
        }
        if (u.Group__c != 'Retention') {		
            quote.Type__c ='New';
        }
    }
    
    //the contract length selectlist varies depending on the promo
    public SelectOption[] getContractLengthVals() {
        SelectOption[] ops = new list<SelectOption>();
        if (selPromo == null) { 
            ops.add(new SelectOption('','--None--'));
            ops.add(new SelectOption('1 Year','1 Year'));
            ops.add(new SelectOption('2 Years','2 Years'));
            ops.add(new SelectOption('3 Years','3 Years'));
            ops.add(new SelectOption('4 Years','4 Years'));
            ops.add(new SelectOption('5 Years','5 Years'));
        } else {
            if (promo.id != null) {
                if (promo.Minimum_Contract_Length__c == null) {
                    ops.add(new SelectOption('','--None--'));
                    ops.add(new SelectOption('1 Year','1 Year'));
                    ops.add(new SelectOption('2 Years','2 Years'));
                    ops.add(new SelectOption('3 Years','3 Years'));
                    ops.add(new SelectOption('4 Years','4 Years'));
                    ops.add(new SelectOption('5 Years','5 Years'));
                }
                if (promo.Minimum_Contract_Length__c == '1 Year') {
                    ops.add(new SelectOption('1 Year','1 Year'));
                    ops.add(new SelectOption('2 Years','2 Years'));
                    ops.add(new SelectOption('3 Years','3 Years'));
                    ops.add(new SelectOption('4 Years','4 Years'));
                    ops.add(new SelectOption('5 Years','5 Years'));
                }
                if (promo.Minimum_Contract_Length__c == '2 Years') {
                    ops.add(new SelectOption('2 Years','2 Years'));
                    ops.add(new SelectOption('3 Years','3 Years'));
                    ops.add(new SelectOption('4 Years','4 Years'));
                    ops.add(new SelectOption('5 Years','5 Years'));
                }
                if (promo.Minimum_Contract_Length__c == '3 Years') {
                    ops.add(new SelectOption('3 Years','3 Years'));
                    ops.add(new SelectOption('4 Years','4 Years'));
                    ops.add(new SelectOption('5 Years','5 Years'));
                }
                if (promo.Minimum_Contract_Length__c == '4 Years') {
                    ops.add(new SelectOption('4 Years','4 Years'));
                    ops.add(new SelectOption('5 Years','5 Years'));
                }
                if (promo.Minimum_Contract_Length__c == '5 Years') {
                    ops.add(new SelectOption('5 Years','5 Years'));
                }
            }
        }
        return ops;
    }
    
    //query all active promos and create a selectlist out of them
    public SelectOption[] getPromoSelectList() {
        SelectOption[] ops = new list<SelectOption>();
        ops.add(new SelectOption('', '--None--'));
        for (Quote_Promotion__c promo : [SELECT id, Name
                                         FROM Quote_Promotion__c
                                         WHERE Status__c = 'Active' ORDER BY Name ASC]) {
                                             ops.add(new SelectOption(promo.id, promo.Name));
                                         }
        return ops;
    }
    
    //find the details of the promo selected so the user can see what theyre selecting
    public void findPromoDetails() {
        if (selPromo != null) {
            promo = [SELECT id, Name, Code__c, Start_Date__c, End_Date__c, Minimum_Contract_Length__c
                     FROM Quote_Promotion__c
                     WHERE id = :selPromo LIMIT 1];
            if (promo.id != null) {
                promoProds = [SELECT id, Name__c, Discount__c, Discount_Method__c, Auto_Add__c, Year_1__c, Year_2__c, Year_3__c, Year_4__c, Year_5__c, Is_Main_Bundled_Product__c
                              FROM Quote_Promotion_Product__c
                              WHERE Quote_Promotion__c = :promo.id
                              AND Added_By_Category__c = false];
            }
        } else {
            promo = null;
            quote.Contract_Length__c = '';
        }
    }
    
    public Quote__c getQuote() {
        if(quote == null) quote = new Quote__c();
        return quote;
    }
    
    public List<SelectOption> getContactSelectList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- Select Contact --',false));        
        List<Contact> contacts = [SELECT Id, Name FROM Contact where AccountID=:currentAccount.Id Order By Name ASC LIMIT 500];
        for (Contact contact : contacts) {
            options.add(new SelectOption(contact.ID, contact.Name));
        }
        return options;
    }
    
    public void setName() {
        Contact contact = getContact();
        if (contact != null) quote.Contact__c = contact.ID;
    }
    
    public Contact getContact() {        
        List<Contact> contacts = [SELECT ID, Name FROM Contact WHERE Id = :quote.Contact__c LIMIT 1];
        if (contacts != null && contacts.size()>0) {
            return contacts[0];
        } else {
            return null;
        }
    }
    
    public void campaignAssignment(){
        if (u.Group__c != 'Retention') {		// if(u.Department__c !='Customer Care'){
            quote = [select ID, Contact__c, Contact__r.Lead_Type__c from Quote__c where Id=:quote.Id];
            if(quote.ID != NULL){
                campaign = [Select CampaignId, Campaign.Name, CreatedDate From CampaignMember where ContactID=:quote.Contact__c ORDER By CreatedDate DESC Limit 1];
            }
            if(quote.ID != NULL && campaign.size()>0){
                primaryCampaign = [Select CampaignId, Campaign.Name, CreatedDate From CampaignMember where ContactID=:quote.Contact__c ORDER By CreatedDate DESC Limit 1];
            }
            if(campaign.size()==0){
                quote.Campaign__c = unknownCampaign.Id;
            } else {
                quote.Campaign__c = primaryCampaign.CampaignId;
            }
            quote.Lead_Type__c = quote.Contact__r.Lead_Type__c;
            update quote;
        }
    }
    
    public boolean canSave() {
        boolean s = true;
        if (quote.Account__c == null) { 
            s = false; 
        }
        if (quote.Contact__c == null) { 
            s = false; 
        }
        if (quote.Contract_Length__c == null) {
            s = false; 
        }
        return s;
    }
    
    //when the user creates the quote and proceeds to the product entry page
    public PageReference productEntry() {
        if (canSave()) {
            Pricebook2 pb = [Select ID, Name, EHS_Pricing__c from PriceBook2 where isActive = TRUE and Name ='Standard Price Book' limit 1];
            quote.Currency__c = selCurrency;
            if (selCurrency != 'USD') {
                DatedConversionRate[] rate = [SELECT id, ConversionRate, IsoCode, StartDate
                                              FROM DatedConversionRate
                                              WHERE IsoCode = : selCurrency
                                              AND (StartDate = TODAY 
                                                   OR StartDate = YESTERDAY)
                                              ORDER BY StartDate DESC];
                if (rate.size() > 0) {
                    quote.Currency_Rate__c = rate[0].ConversionRate;
                    quote.Currency_Rate_Date__c = rate[0].StartDate;
                } else {
                    quote.Currency_Rate__c = 1;
                    quote.Currency_Rate_Date__c = date.today();
                }
                
                
            } else {
                quote.Currency_Rate__c = 1;
                quote.Currency_Rate_Date__c = date.today();
            }
            
            
            //if the user selected a promo, make sure to store that promo on the quote
            if (selPromo != null) {
                quote.Promotion__c = selPromo; 
            }
            
            //insert the quote
            insert quote;
            
            //if there is a promotion we need to query and auto-add the products
            if (quote.Promotion__c != null) {
                
                //create a list of prods to insert
                Quote_Product__c[] insList = new list<Quote_Product__c>();
                //create a list of bundles
                Quote_Product_Bundle__c[] bundles = new list<Quote_Product_Bundle__c>();
                //set to hold product ids
                set<id> prodIds = new set<id>();
                //set to hold the ids of any prods added as a package
                set<id> packIds = new set<id>();
                
                //query the promo prods associated with the promo that need to be auto-added
                Quote_Promotion_Product__c[] promoProds = [SELECT id, Name__c, Auto_Add__c, Bundled_Into__c, Is_Main_Bundled_Product__c, Discount__c, Discount_Method__c, Product__c, Indexing_Type__c, Product_Package_Id__c,
                                                           Year_1__c, Year_2__c, Year_3__c, Year_4__c, Year_5__c, Product__r.Name, Group__c, Is_Main_Group_Product__c, Cant_Edit_Years__c, Cant_Edit_Discount__c, Quantity__c
                                                           FROM Quote_Promotion_Product__c
                                                           WHERE Quote_Promotion__c = :quote.Promotion__c
                                                           AND Auto_Add__c = true
                                                           AND Added_By_Category__c = false];
                
                //add the product ids to the set to query later
                if (promoProds.size() > 0) {
                    for (Quote_Promotion_Product__c promoProd : promoProds) {
                        prodIds.add(promoProd.Product__c);                        
                    }
                }
                
                //query the pricebook entries associated with the product ids
                if (prodIds.size() > 0) {
                    entries = [SELECT id, Product2id, Product2.Name, Product2.Proposal_Subpoint__c, Product2.Quantity_Calculation__c, Product2.Proposal_Print_Group__c, Product2.ProductCode, Product2.Custom_Quote__c,
                               Product2.Quantity_Field_Name__c, Product2.Product_Package__c, Product2.Has_Overage_Amount__c, Product2.Overage_Amount__c, Product2.Add_On_Product__c, Pricebook2id, Implementation_Cost__c,
                               Quantity_Field_Name__c, Product2.Auto_Select_Years__c, Product2.Show_Qty_Price__c, Product2.Has_Special_Indexing_Fields__c, Product2.Product_Package_Name__c, Price_Break_1__c, Product2.Auto_Select_Quantity__c,
                               Product2.Calculate_Price_After_Product_Entry__c
                               FROM PricebookEntry
                               WHERE Product2id IN :prodIds
                               AND Pricebook2id = :pb.id];
                    
                    //loop through the entries and create the wrappers, passing the promo prod and the pricebook entry
                    if (entries.size() > 0) {
                        for (Quote_Promotion_Product__c promoProd : promoProds) {
                            for (PricebookEntry entry : entries) {
                                if (entry.Product2id == promoProd.Product__c) {
                                    wrappers.add(new prodWrapper(promoProd, entry));
                                }
                            }
                        }
                    }
                }
                
                //create a list to ensure no prods are accidentally added twice
                string deDupeList = '';
                
                if (wrappers.size() > 0) {
                    
                    //loop through the wrappers we created
                    for (prodWrapper wrap : wrappers) {
                        
                        //double check the product hasnt been already added
                        if (!deDupeList.contains(wrap.entry.Product2.Name)) {
                            
                            //add this prod to the dedupe list
                            deDupeList += wrap.entry.Product2.Name+';';
                            
                            //create the quote product
                            Quote_Product__c qp = new Quote_Product__c();
                            qp.Quote__c = quote.id;
                            qp.Added_By_Promo__c = true;
                            qp.Name__c = wrap.entry.Product2.Name;
                            qp.Proposal_Subpoint__c = wrap.entry.Product2.Proposal_Subpoint__c;
                            qp.Quantity_Calculation__c = wrap.entry.Product2.Quantity_Calculation__c;
                            qp.Print_Group__c = wrap.entry.Product2.Proposal_Print_Group__c;
                            qp.Product_Print_Name__c = wrap.entry.Product2.ProductCode;
                            qp.Custom_Quote__c = wrap.entry.Product2.Custom_Quote__c; 
                            qp.Quantity_Field_Name__c = wrap.entry.Quantity_Field_Name__c; 
                            qp.Is_Product_Package__c = wrap.entry.Product2.Product_Package__c;
                            qp.Has_Overage_Amount__c = wrap.entry.Product2.Has_Overage_Amount__c;
                            qp.Overage_List_Price__c = wrap.entry.Product2.Overage_Amount__c;
                            qp.Add_On_Product__c = wrap.entry.Product2.Add_On_Product__c;
                            qp.Product__c = wrap.entry.Product2.Id;
                            qp.PricebookID__c = wrap.entry.Pricebook2Id;
                            qp.PricebookEntryId__c = wrap.entry.Id;
                            
                            //product grouping (example: eBinder valet and indexing fields)
                            if (wrap.prod.Group__c != null) {
                                //set the group name and id
                                qp.Group_Name__c = wrap.prod.Product__r.Name;
                                qp.Group_Id__c = wrap.prod.Group__c;
                            }
                            
                            //set quote type
                            if (quote.Type__c == 'New') {
                                qp.Type__c = 'New';
                            } else {
                                qp.Type__c = 'Renewal';
                            }
                            
                            //if renewal, set renewal info
                            if (quote.Type__c=='Renewal'){
                                qp.Renewal_Amount__c = 0.00;
                                qp.PI__c = 0.00;
                                qp.New_Revenue_Price__c = 0.00;
                            }
                            
                            if (wrap.entry.Product2.Show_Qty_Price__c == false) {
                                qp.Quantity__c = 1;
                            }
                            
                            if (wrap.entry.Product2.Has_Special_Indexing_Fields__c == true) {
                                qp.Indexing_Type__c = wrap.prod.Indexing_Type__c;
                            }
                            
                            if ((quote.Department__c == 'Customer Care' && u.Group__c == 'Retention') && 
                                (wrap.entry.Product2.Name == 'HQ Account'
                                 ||
                                 wrap.entry.Product2.Name == 'HQ RegXR Account'
                                 ||
                                 wrap.entry.Product2.Name == 'HQ Implementation Fee'
                                 ||
                                 wrap.entry.Product2.Name == 'HQ RegXR Implementation Fee')) {	
                                     qp.Quantity__c = currentAccount.Num_of_Employees__c;
                                 }
                            
                            if (quote.Contract_Length__c == '5 Years') {
                                if ((quote.Department__c == 'Customer Care' && u.Group__c == 'Retention') || wrap.entry.Product2.Auto_Select_Years__c == true) {
                                    qp.Year_1__c = True;
                                    qp.Year_2__c = True;
                                    qp.Year_3__c = True;
                                    qp.Year_4__c = True;
                                    qp.Year_5__c = True;
                                }
                            }
                            if (quote.Contract_Length__c == '4 Years') {
                                if ((quote.Department__c == 'Customer Care' && u.Group__c == 'Retention') || wrap.entry.Product2.Auto_Select_Years__c == true) {
                                    qp.Year_1__c = True;
                                    qp.Year_2__c = True;
                                    qp.Year_3__c = True;
                                    qp.Year_4__c = True;
                                }
                            }
                            if (quote.Contract_Length__c =='3 Years') {
                                if ((quote.Department__c == 'Customer Care' && u.Group__c == 'Retention') || wrap.entry.Product2.Auto_Select_Years__c == true) {	
                                    qp.Year_1__c = True;
                                    qp.Year_2__c = True;
                                    qp.Year_3__c = True;
                                }
                            }
                            if (quote.Contract_Length__c =='2 Years') {
                                if((quote.Department__c == 'Customer Care' && u.Group__c == 'Retention') || wrap.entry.Product2.Auto_Select_Years__c == true) {	
                                    qp.Year_1__c = True;
                                    qp.Year_2__c = True;
                                }
                            }
                            if (quote.Contract_Length__c =='1 Year') {
                                if ((quote.Department__c == 'Customer Care' && u.Group__c == 'Retention') || wrap.entry.Product2.Auto_Select_Years__c == true) {
                                    qp.Year_1__c = True;
                                }
                            }
                            if (wrap.entry.Product2.Name =='EH&S Implementation Fee') {
                                qp.Year_1__c = True;
                            } 
                            
                            //set the quantities
                            if (wrap.prod.Quantity__c != null) {
                                qp.Quantity__c = wrap.prod.Quantity__c;
                            }
                            if (wrap.entry.Implementation_Cost__c != null) {
                                if (wrap.entry.Quantity_Field_Name__c == '# of Employees') {
                                    qp.Quantity__c = currentAccount.Num_of_Employees__c;
                                }
                            }
                            if (wrap.entry.Product2.Auto_Select_Quantity__c) {
                                if (quote.Healthcare_Customer__c == 'Yes') {
                                    qp.Quantity__c = currentAccount.Num_of_Beds__c;
                                } else {
                                    qp.Quantity__c = currentAccount.Num_of_Employees__c;
                                }
                            }
                            
                            //add to list to insert
                            insList.add(qp);
                        }
                    }
                }
                
                if (insList.size() > 0) {
                    
                    //insert the quote products
                    insert insList;
                    
                    //Check for any grouped products
                    boolean groupedProductsFound = false;
                    for (Quote_Product__c qp : insList) {
                        //If Parent Found
                        if (qp.Group_ID__c != null && qp.Group_ID__c == qp.Product__c) {
                            groupedProductsFound = true;
                            //Loop over products again to find any children
                            for(Quote_Product__c qp2 : insList){
                                //If child found then set child fields
                                if (qp2.Group_ID__c != null && qp2.Group_ID__c != qp2.Product__c && qp2.Group_ID__c == qp.Product__c) {
                                    qp2.Group_Name__c = qp.Name__c;
                                    qp2.Group_ID__c = qp.id;
                                    qp2.Group_Parent_ID__c = qp.id;
                                }
                            }
                            //set parent fields
                            qp.Group_ID__c = null;
                            qp.Group_Name__c = qp.Name__c;
                            qp.Group_Parent_ID__c = qp.id;
                        }
                    }
                    
                    if(groupedProductsFound){
                        update insList;
                    }
                    
                    //loop through the wrappers and assign the newly created quote products
                    for (prodWrapper wrap : wrappers) {
                        for (Quote_Product__c qp : insList) {
                            if (qp.Name__c == wrap.prod.Name__c) {
                                wrap.qp = qp;
                            }
                        }
                    }
                }
                
                //move on to bundling products
                
                //list to hold products to update
                Quote_Product__c[] updList = new list<Quote_Product__c>();
                
                //loop through the wrappers
                for (prodWrapper wrap : wrappers) {
                    
                    //look for the main bundled products
                    if (wrap.prod.Is_Main_Bundled_Product__c) {
                        wrap.qp.Main_Bundled_Product__c = true;
                        
                        //create a new bundle
                        Quote_Product_Bundle__c bundle = new Quote_Product_Bundle__c();
                        bundle.Quote__c = quote.id;
                        bundle.Main_Quote_Product__c = wrap.qp.id;
                        updList.add(wrap.qp);
                        insert bundle;
                        
                        //loop again and look for any prods needing to be bundled into the new bundle
                        for (prodWrapper wrap2 : wrappers) {
                            if (wrap2.prod.Bundled_Into__c == wrap.prod.id) {
                                wrap2.qp.Bundle__c = true;
                                wrap2.qp.Bundled_Product__c = wrap.qp.id;
                                wrap2.qp.Quote_Product_Bundle__c = bundle.id;
                                updList.add(wrap2.qp);
                            }
                        }
                    }
                }
                
                //update the products that have been bundled
                if (updList.size() > 0) {
                    update updList;
                }
                
                
            }
            if (u.Group__c != 'Retention') {	
                campaignAssignment();
            }
            PageReference productEntry = Page.quoteProductEntry;
            productEntry.setRedirect(true);
            productEntry.getParameters().put('id',quote.id); 
            return  productEntry;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required information'));
            return null; 
        }
    }
    
    public SelectOption[] getCurrencyTypes() {
        SelectOption[] ops = new List<SelectOption>();
        for (string s : 'USD;AUD;CAD;EUR;GBP'.split(';',0)) {
            ops.add(new SelectOption(s, s));
        }
        return ops;
    }
    
    //wrapper class for auto-add promo prods
    public class prodWrapper {
        public Quote_Promotion_Product__c prod {get;set;}
        public PricebookEntry entry {get;set;}
        public Quote_Product__c qp {get;set;}
        
        public prodWrapper(Quote_Promotion_Product__c xProd, PricebookEntry xEntry) {
            prod = xProd;
            entry = xEntry;
        }
    }
    
}