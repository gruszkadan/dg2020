global class OrderItemBatchFeeder implements Iterator<Order_Item__c>, Iterable<Order_Item__c> {
    Order_Item__c[] source;
    
    global Iterator<Order_Item__c> iterator() {
        return this;
    }
    
    global OrderItemBatchFeeder(Order_Item__c[] source) {
        this.source = source;
    }
    
    global Order_Item__c next() {
        return source.remove(0);
    }
    
    global boolean hasNext() {
        return source!=null && !source.isempty();
    }
}