public class poiStageStatusQueueableStep1 implements Queueable {
    
    //this string holds the results we want to email ourselves
    string results = '<b>NIGHTLY POI STAGE/STATUS SCHEDULER RESULTS PART 1</b><br/><br/>';
    
    public void execute(QueueableContext context) {
        
        //check for actions needing to be set to Action_Status__c = 'Resolved - Unable to Connect'
        status_resolvedUnableToConnect();
        
        email(results);
    }
    
    //Change POI Status to Resolved Unable to Connect
    public void status_resolvedUnableToConnect() {
        //String to Hold Status We Care about in the Query
        String actionStatus='Working - Pending Response';
        
        //Query the Contacts
        Contact[] contacts = [SELECT ID, POI_Stage_MSDS__c, POI_Stage_EHS__c, POI_Stage_AUTH__c, POI_Stage_ODT__c, POI_Stage_ERGO__c, POI_Status_MSDS__c, POI_Status_EHS__c, POI_Status_AUTH__c, 
                              POI_Status_ODT__c, POI_Status_ERGO__c, Num_Of_Unable_To_Connect_Tasks_MSDS__c, Num_Of_Unable_To_Connect_Tasks_EHS__c, Num_Of_Unable_To_Connect_Tasks_AUTH__c,
                              Num_Of_Unable_To_Connect_Tasks_ODT__c, Num_Of_Unable_To_Connect_Tasks_ERGO__c, POI_Status_Change_Date_MSDS__c, POI_Status_Change_Date_EHS__c,
                              POI_Status_Change_Date_AUTH__c, POI_Status_Change_Date_ODT__c, POI_Status_Change_Date_ERGO__c FROM Contact
                              WHERE(
                                  //MSDS
                                  POI_Status_MSDS__c = :actionStatus
                                  AND POI_Status_Change_Date_MSDS__c < LAST_N_DAYS:30
                                  AND Num_Of_Unable_To_Connect_Tasks_MSDS__c >= 3
                              )OR(
                                  //EHS
                                  POI_Status_EHS__c = :actionStatus
                                  AND POI_Status_Change_Date_EHS__c < LAST_N_DAYS:30
                                  AND Num_Of_Unable_To_Connect_Tasks_EHS__c >= 3
                              )OR(
                                  //AUTH
                                  POI_Status_AUTH__c = :actionStatus
                                  AND POI_Status_Change_Date_AUTH__c < LAST_N_DAYS:30
                                  AND Num_Of_Unable_To_Connect_Tasks_AUTH__c >= 3
                              )OR(
                                  //ERGO
                                  POI_Status_ERGO__c = :actionStatus
                                  AND POI_Status_Change_Date_ERGO__c < LAST_N_DAYS:30
                                  AND Num_Of_Unable_To_Connect_Tasks_ERGO__c >= 3
                              )OR(
                                  //ODT
                                  POI_Status_ODT__c = :actionStatus
                                  AND POI_Status_Change_Date_ODT__c < LAST_N_DAYS:30
                                  AND Num_Of_Unable_To_Connect_Tasks_ODT__c >= 3
                              )];
        
        //Query the Leads
        Lead[] leads = [SELECT ID, POI_Stage_MSDS__c, POI_Stage_EHS__c, POI_Stage_AUTH__c, POI_Stage_ODT__c, POI_Stage_ERGO__c, POI_Status_MSDS__c, POI_Status_EHS__c, POI_Status_AUTH__c, 
                        POI_Status_ODT__c, POI_Status_ERGO__c, Num_Of_Unable_To_Connect_Tasks_MSDS__c, Num_Of_Unable_To_Connect_Tasks_EHS__c, Num_Of_Unable_To_Connect_Tasks_AUTH__c,
                        Num_Of_Unable_To_Connect_Tasks_ODT__c, Num_Of_Unable_To_Connect_Tasks_ERGO__c, POI_Status_Change_Date_MSDS__c, POI_Status_Change_Date_EHS__c,
                        POI_Status_Change_Date_AUTH__c, POI_Status_Change_Date_ODT__c, POI_Status_Change_Date_ERGO__c FROM Lead
                        WHERE (
                            //MSDS
                            POI_Status_MSDS__c = :actionStatus
                            AND POI_Status_Change_Date_MSDS__c < LAST_N_DAYS:30
                            AND Num_Of_Unable_To_Connect_Tasks_MSDS__c >= 3
                            AND isConverted=FALSE
                        )OR(
                            //EHS
                            POI_Status_EHS__c = :actionStatus
                            AND POI_Status_Change_Date_EHS__c < LAST_N_DAYS:30
                            AND Num_Of_Unable_To_Connect_Tasks_EHS__c >= 3
                            AND isConverted=FALSE
                        )OR(
                            //AUTH
                            POI_Status_AUTH__c = :actionStatus
                            AND POI_Status_Change_Date_AUTH__c < LAST_N_DAYS:30
                            AND Num_Of_Unable_To_Connect_Tasks_AUTH__c >= 3
                            AND isConverted=FALSE
                        )OR(
                            //ERGO
                            POI_Status_ERGO__c = :actionStatus
                            AND POI_Status_Change_Date_ERGO__c < LAST_N_DAYS:30
                            AND Num_Of_Unable_To_Connect_Tasks_ERGO__c >= 3
                            AND isConverted=FALSE
                        )OR(
                            //ODT
                            POI_Status_ODT__c = :actionStatus
                            AND POI_Status_Change_Date_ODT__c < LAST_N_DAYS:30
                            AND Num_Of_Unable_To_Connect_Tasks_ODT__c >= 3
                            AND isConverted=FALSE
                        )];
        
        //New Action Status Value
        string newActionStatus ='Resolved - Unable to Connect';
        
        //Add Contacts to sObjectList
        if(contacts.size()>0){
            for(Contact c : contacts){
                //MSDS
                if(c.POI_Status_MSDS__c == actionStatus && c.POI_Status_Change_Date_MSDS__c.daysBetween(date.today()) > 30 && c.Num_Of_Unable_To_Connect_Tasks_MSDS__c >= 3){
                    c.POI_Status_MSDS__c = newActionStatus;
                    c.POI_Status_Change_Date_MSDS__c = date.today();
                }
                //EHS
                if(c.POI_Status_EHS__c == actionStatus && c.POI_Status_Change_Date_EHS__c.daysBetween(date.today()) > 30 && c.Num_Of_Unable_To_Connect_Tasks_EHS__c >= 3){
                    c.POI_Status_EHS__c = newActionStatus;
                    c.POI_Status_Change_Date_EHS__c = date.today();
                }
                //AUTH
                if(c.POI_Status_AUTH__c == actionStatus && c.POI_Status_Change_Date_AUTH__c.daysBetween(date.today()) > 30 && c.Num_Of_Unable_To_Connect_Tasks_AUTH__c >= 3){
                    c.POI_Status_AUTH__c = newActionStatus;
                    c.POI_Status_Change_Date_AUTH__c = date.today();
                }
                //ERGO
                if(c.POI_Status_ERGO__c == actionStatus && c.POI_Status_Change_Date_ERGO__c.daysBetween(date.today()) > 30 && c.Num_Of_Unable_To_Connect_Tasks_ERGO__c >= 3){
                    c.POI_Status_ERGO__c = newActionStatus;
                    c.POI_Status_Change_Date_ERGO__c = date.today();
                }
                //ODT
                if(c.POI_Status_ODT__c == actionStatus && c.POI_Status_Change_Date_ODT__c.daysBetween(date.today()) > 30 && c.Num_Of_Unable_To_Connect_Tasks_ODT__c >= 3){
                    c.POI_Status_ODT__c = newActionStatus;
                    c.POI_Status_Change_Date_ODT__c = date.today();
                }
            }
        }
        
        //Add Leads to sObjectList
        if(leads.size()>0){
            for(Lead l : leads){
                //MSDS
                if(l.POI_Status_MSDS__c == actionStatus && l.POI_Status_Change_Date_MSDS__c.daysBetween(date.today()) > 30 && l.Num_Of_Unable_To_Connect_Tasks_MSDS__c >= 3){
                    l.POI_Status_MSDS__c = newActionStatus;
                    l.POI_Status_Change_Date_MSDS__c = date.today();
                }
                //EHS
                if(l.POI_Status_EHS__c == actionStatus && l.POI_Status_Change_Date_EHS__c.daysBetween(date.today()) > 30 && l.Num_Of_Unable_To_Connect_Tasks_EHS__c >= 3){
                    l.POI_Status_EHS__c = newActionStatus;
                    l.POI_Status_Change_Date_EHS__c = date.today();
                }
                //AUTH
                if(l.POI_Status_AUTH__c == actionStatus && l.POI_Status_Change_Date_AUTH__c.daysBetween(date.today()) > 30 && l.Num_Of_Unable_To_Connect_Tasks_AUTH__c >= 3){
                    l.POI_Status_AUTH__c = newActionStatus;
                    l.POI_Status_Change_Date_AUTH__c = date.today();
                }
                //ERGO
                if(l.POI_Status_ERGO__c == actionStatus && l.POI_Status_Change_Date_ERGO__c.daysBetween(date.today()) > 30 && l.Num_Of_Unable_To_Connect_Tasks_ERGO__c >= 3){
                    l.POI_Status_ERGO__c = newActionStatus;
                    l.POI_Status_Change_Date_ERGO__c = date.today();
                }
                //ODT
                if(l.POI_Status_ODT__c == actionStatus && l.POI_Status_Change_Date_ODT__c.daysBetween(date.today()) > 30 && l.Num_Of_Unable_To_Connect_Tasks_ODT__c >= 3){
                    l.POI_Status_ODT__c = newActionStatus;
                    l.POI_Status_Change_Date_ODT__c = date.today();
                }
            }
        }   
        
        if (contacts.size() > 0 || leads.size()>0) {
            try {
                if (contacts.size() > 0){
                    update contacts;
                } 
                if (leads.size() > 0){
                    update leads;
                } 
                results += '# of Leads actions set to status: \'Resolved - Unable to Connect\' = '+Leads.size()+'<br/><br/>';
                results += '# of Contacts actions set to status: \'Resolved - Unable to Connect\' = '+Contacts.size()+'<br/><br/>';
            } catch(exception e) {
                salesforceLog.createLog('POI', true, 'poiUnableToConnect', 'status_resolvedUnableToConnect', string.valueOf(e));
            }
        } else {
            results += '# actions set to status: \'Resolved - Unable to Connect\' = 0 <br/><br/>';
        }
    }
    
     // Final results email
    public void email(string htmlBody) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new list<string>{'salesforceops@ehs.com'});
        mail.setReplyTo('salesforceops@ehs.com');
        mail.setSubject('poiStatusStageScheduler results - '+date.today());
        mail.setHtmlBody(htmlBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}