@isTest(seeAllData=true)
private class testdynamicQuery {

    static testmethod void test1() {
        //insert an account
        Account acct = new Account(Name='Acct1', Phone='7778889999');
        insert acct;
        
        //insert a contact
        Contact con = new Contact(FirstName='Test', LastName='Test', AccountId = acct.id);
        insert con;
        
        //test the query
        string addFields = 'Account.Name';
        Contact assertCon = (Contact)new dynamicQuery().query('Contact', addFields, 'AccountId = \''+acct.id+'\'');
        
        //assert the results
        System.assert(assertCon.LastName == con.LastName);
        
        //insert contacts
        Contact[] cons = new list<Contact>();
        for (integer i=0; i<3; i++) {
            Contact c = new Contact();
            c.FirstName = 'Test'+i;
            c.LastName = 'Test'+i;
            c.AccountId = acct.id;
            cons.add(c);
        }
        insert cons;
        
        //test the list query
        Contact[] assertCons = (Contact[])new dynamicQuery().queryList('Contact', addFields, 'AccountId = \''+acct.id+'\'', 'LastName DESC', '100');
        
        //assert the results
        System.assert(assertCons.size() == cons.size()+1);
        
    }
    
}