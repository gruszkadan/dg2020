public class orderItemManualBuild {
    Order_Item__c[] lastManualOrderItem;
    public orderWrapper[] orders {get;set;}
    public decimal orderNumber {get;set;}
    public decimal orderItemNumber {get;set;}
    public Order_Item__c[] errors {get;set;}
    public String viewingOrder {get;set;}
    public orderItemManualBuild() {
        orders = new list<orderWrapper>();
        errors = new list<Order_Item__c>();
        lastManualOrderItem = [Select id,Order_ID__c,Order_Item_ID__c,Temporary_Order_Number__c,Temporary_Order_Item_Number__c
                               from Order_Item__c 
                               where Temporary_Order_Item_Number__c != null 
                               order by Temporary_Order_Item_Number__c desc LIMIT 1];
        if(!lastManualOrderItem.isEmpty()){
            orderNumber = lastManualOrderItem[0].Temporary_Order_Number__c+1;
            orderItemNumber = lastManualOrderItem[0].Temporary_Order_Item_Number__c+1;
        }else{
            orderNumber = 10000;
            orderItemNumber = 10000;
        }
        orders.add(new orderWrapper(orderNumber, orderItemNumber));
        orderNumber++;
        orderItemNumber++;
        viewingOrder = orders[0].adminToolOrderID;
    }
    
    public List<SelectOption> getAdminToolSalesReps() {
        User[] reps = [SELECT id, Rep_Name_In_Admin_Tool__c 
                       FROM User 
                       WHERE Rep_Name_In_Admin_Tool__c != null AND isActive = true 
                       ORDER BY Rep_Name_In_Admin_Tool__c ASC];
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for(User u:reps){
            options.add(new SelectOption(u.Rep_Name_In_Admin_Tool__c,u.Rep_Name_In_Admin_Tool__c));
        }
        return options;
    }
    
    public List<SelectOption> getAdminToolProductNames() {
        AT_Product_Mapping__c[] pms = [SELECT id, Admin_Tool_Product_Name__c 
                                       FROM AT_Product_Mapping__c
                                       WHERE Display_in_Manual_Entry__c = TRUE
                                       ORDER BY Admin_Tool_Product_Name__c ASC];
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for(AT_Product_Mapping__c pm:pms){
            options.add(new SelectOption(pm.Admin_Tool_Product_Name__c,pm.Admin_Tool_Product_Name__c));
        }
        return options;
    }
    
    public void addOrderItem(){
        String oid = ApexPages.currentPage().getParameters().get('atoid');
        for(orderWrapper wrap:orders){
            if(wrap.adminToolOrderID == oid){
                wrap.orderItems.add(new Order_Item__c(Order_Id__c = wrap.adminToolOrderID,
                                                      Order_Item_Id__c = 'TI-'+orderItemNumber,
                                                      Temporary_Order_Number__c = wrap.orderItems[0].Temporary_Order_Number__c,
                                                      Temporary_Order_Item_Number__c = orderItemNumber));
                orderItemNumber++;
            }
        }
        refreshOrderIds();
        viewingOrder = oid;
    }
    
    public void removeOrderItem(){
        String oid = ApexPages.currentPage().getParameters().get('orderId'); 
        String oiid = ApexPages.currentPage().getParameters().get('orderItemId'); 
        for(orderWrapper wrap:orders){
            if(wrap.adminToolOrderID == oid){
                for(Integer i = 0; i < wrap.orderItems.size(); i++){
                    if(wrap.orderItems.get(i).Order_Item_ID__c == oiid){
                        wrap.orderItems.remove(i);
                    }
                }
            }
        }
        refreshOrderIds();
        viewingOrder = oid;
    }
    
    public void addOrder(){
        orders.add(new orderWrapper(orderNumber, orderItemNumber));
        viewingOrder = 'T-'+orderNumber;
        orderNumber++;
        orderItemNumber++;
        refreshOrderIds();
    }
    
    public void removeOrder(){
        String oid = ApexPages.currentPage().getParameters().get('removeOrderId'); 
        for(Integer i = 0; i < orders.size(); i++){
            if(orders.get(i).adminToolOrderID == oid){
                orders.remove(i);
            }
        }
        refreshOrderIds();
        viewingOrder = orders[0].adminToolOrderID;
    }
    
    public void save(){
        errors = new list<Order_Item__c>();
        boolean pass = true;
        for(orderWrapper wrap:orders){
            for(Order_Item__c oi:wrap.orderItems){
                if(oi.Contract_Number__c != null &&
                   oi.Contract_Length__c != null &&
                   oi.AccountID__c != null &&
                   oi.Invoice_Amount__c != null &&
                   oi.Admin_Tool_Product_Name__c != null &&
                   oi.Admin_Tool_Sales_Rep__c != null &&
                   oi.Order_Date__c != null &&
                   oi.Term_Start_Date__c != null &&
                   oi.Term_Length__c != null &&
                   oi.Term_End_Date__c != null)
                {
                    oi.Month__c = string.valueof(datetime.newInstance(oi.Order_Date__c.year(), oi.Order_Date__c.month(),oi.Order_Date__c.day()).format('MMMMM'));
                    oi.Year__c = string.valueof(oi.Order_Date__c.Year());
                }else{
                    pass = false;
                    errors.add(oi);
                }
            }
        }
        if(pass){
            refreshOrderIds();
            Order_Item__c[] itemsToInsert = new list<Order_Item__c>();
            for(orderWrapper wrap:orders){
                itemsToInsert.addAll(wrap.orderItems);
            }
            if(itemsToInsert.size() > 0){
                insert itemsToInsert;
            }
        }
    }
    
    public void refreshOrderIds(){
        lastManualOrderItem = [Select id,Order_ID__c,Order_Item_ID__c,Temporary_Order_Number__c,Temporary_Order_Item_Number__c
                               from Order_Item__c 
                               where Temporary_Order_Item_Number__c != null 
                               order by Temporary_Order_Item_Number__c desc LIMIT 1];
        if(!lastManualOrderItem.isEmpty()){
            orderNumber = lastManualOrderItem[0].Temporary_Order_Number__c+1;
            orderItemNumber = lastManualOrderItem[0].Temporary_Order_Item_Number__c+1;
        }else{
            orderNumber = 10000;
            orderItemNumber = 10000;
        }
        for(orderWrapper wrap:orders){
            wrap.adminToolOrderID = 'T-'+orderNumber;
            for(Order_Item__c oi:wrap.orderItems){
                oi.Order_Id__c = wrap.adminToolOrderID;
                oi.Order_Item_Id__c = 'TI-'+orderItemNumber;
                oi.Temporary_Order_Number__c = orderNumber;
                oi.Temporary_Order_Item_Number__c = orderItemNumber;
                orderItemNumber++;
            }
            orderNumber++;
        }
    }
    
    public class orderWrapper {
        public string adminToolOrderID {get;set;}
        public Order_Item__c[] orderItems {get;set;}
        orderWrapper(decimal orderID, decimal orderItemID){
            orderItems = new list<Order_Item__c>();
            orderItems.add(new Order_Item__c(Order_Id__c = 'T-'+orderID, 
                                             Order_Item_Id__c = 'TI-'+orderItemID, 
                                             Temporary_Order_Number__c = orderID, 
                                             Temporary_Order_Item_Number__c = orderItemID, 
                                             Order_Date__c = null));
            adminToolOrderID = 'T-'+orderID;
        }
    }
    
}