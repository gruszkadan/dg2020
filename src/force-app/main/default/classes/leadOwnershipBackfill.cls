global class leadOwnershipBackfill implements Database.Batchable<SObject>, Database.Stateful {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (!test.isRunningTest()) {
            return Database.getQueryLocator([SELECT id, OwnerId, Territory__c, Enterprise_Sales__c, Territory__r.Territory_Owner__c, Territory__r.Online_Training_Owner__c, 
                                             Territory__r.Authoring_Owner__c, Territory__r.Ergonomics_Owner__c, Territory__r.EHS_Owner__c, Country
                                             FROM Lead WHERE isConverted = false]);
        } else {
            return Database.getQueryLocator([SELECT id, OwnerId, Territory__c, Enterprise_Sales__c, Territory__r.Territory_Owner__c, Territory__r.Online_Training_Owner__c, 
                                             Territory__r.Authoring_Owner__c, Territory__r.Ergonomics_Owner__c, Territory__r.EHS_Owner__c, Country
                                             FROM Lead WHERE isConverted = false
                                             LIMIT 100]);
        }
    }
    
    global void execute(Database.BatchableContext bc, list<SObject> batch) {  
        set<id> ownerIds = new set<id>();
        set<id> queueIds = new set<id>();
        for (Lead l : (list<Lead>) batch) {
            string id3 = string.valueOf(l.OwnerId).left(3);
            if (id3 == '005') {
                ownerIds.add(l.OwnerId);
            } else {
                queueIds.add(l.OwnerId);
            }
        }
        User[] owners = [SELECT id, Group__c 
                         FROM User
                         WHERE id IN : ownerIds];    
        wrapper[] wrappers = new list<wrapper>();
        for (Lead l : (list<Lead>) batch) {
            if (string.valueOf(l.OwnerId).left(3) == '005') {
                System.debug(string.valueOf(l.OwnerId).left(3));
                for (User o : owners) {
                    if (o.id == l.OwnerId) {
                        wrappers.add(new wrapper(l, o));
                    }
                }
            } else {
                wrappers.add(new wrapper(l, null));
            }
        }
        defaultOwners def = new defaultOwners();
        Lead[] updateList = new list<Lead>();
        Lead[] leadsNeedingEntRR = new list<Lead>();
        for (wrapper w : wrappers) {
            boolean hasTerritory = false;
            boolean specialCountry = false;
            boolean ent = false;
            boolean hasEntOwner = false;
            boolean hasEHSEntOwner = false;
            boolean hasOwner = w.hasOwner;
            if (w.lead.Territory__c != null) {
                hasTerritory = true;
            }
            string sc = 'Australia;New Zealand;India;Pakistan;UAE;Qatar;Saudi Arabia;Bahrain;Kuwait;Oman;';
            for (string s : sc.split(';')) {
                if (s == w.lead.Country) {
                    specialCountry = true;
                }
            }
            if (w.lead.Enterprise_Sales__c) {
                ent = true;
            }
            if (w.owner.Group__c == 'Enterprise Sales') {
                hasEntOwner = true;
            }
            if (w.owner.Group__c == 'EHS Enterprise Sales') {
                hasEHSEntOwner = true;
            }
            if (hasTerritory) {
                if (ent) {
                    //CHEMICAL MANAGEMENT OWNER
                    if (hasEntOwner) {
                        w.lead.Chemical_Management_Owner__c = w.lead.OwnerId;
                    } else {
                        leadsNeedingEntRR.add(w.lead);
                    }
                    //TRAINING OWNER - use territory owner
                    w.lead.Online_Training_Owner__c = w.lead.Territory__r.Online_Training_Owner__c;
                    //AUTHORING OWNER - use territory owner
                    w.lead.Authoring_Owner__c = w.lead.Territory__r.Authoring_Owner__c;
                    //EHS OWNER - use current owner if current owner is an enterprise ehs rep, otherwise use brian richey
                    if (hasEHSEntOwner) {
                        w.lead.EHS_Owner__c = w.lead.OwnerId;
                    } else {
                        w.lead.EHS_Owner__c = def.entEHSOwner;
                    }
                    //ERGONOMICS OWNER - use territory owner
                    w.lead.Ergonomics_Owner__c = w.lead.Territory__r.Ergonomics_Owner__c;
                } else {
                    // HAS TERRITORY - NOT ENTERPRISE
                    //CHEMICAL MANAGEMENT OWNER - the owner shouldnt be an enterprise rep otherwise it would have fallen into the enterprise category,
                    w.lead.Chemical_Management_Owner__c = w.lead.Territory__r.Territory_Owner__c;
                    //TRAINING OWNER - use territory owner
                    w.lead.Online_Training_Owner__c = w.lead.Territory__r.Online_Training_Owner__c;
                    //AUTHORING OWNER - use territory owner
                    w.lead.Authoring_Owner__c = w.lead.Territory__r.Authoring_Owner__c;
                    //EHS OWNER - if there was an ent ehs owner, this would fall into the ent category
                    w.lead.EHS_Owner__c = w.lead.Territory__r.EHS_Owner__c;
                    //ERGONOMICS OWNER - use territory owner
                    w.lead.Ergonomics_Owner__c = w.lead.Territory__r.Ergonomics_Owner__c;
                }
            } else {
                if (specialCountry) {
                    if (ent) {
                        //NO TERRITORY - SPECIAL COUNTRY - ENTERPRISE
                        //CHEMICAL MANAGEMENT OWNER - use current owner if current owner is an enterprise rep, otherwise send to round-robin
                        if (hasEntOwner) {
                            w.lead.Chemical_Management_Owner__c = w.lead.OwnerId;
                        } else {
                            leadsNeedingEntRR.add(w.lead);
                        }
                        //TRAINING OWNER - use default
                        w.lead.Online_Training_Owner__c = def.defOTOwner;
                        //AUTHORING OWNER - use default
                        w.lead.Authoring_Owner__c = def.defAuthOwner;
                        //EHS OWNER - use current owner if current owner is an ent ehs rep, otherwise use special country owner
                        if (hasEHSEntOwner) {
                            w.lead.EHS_Owner__c = w.lead.OwnerId;
                        } else {
                            w.lead.EHS_Owner__c = def.specialEHSOwner;
                        }
                        //ERGONOMICS OWNER - use default
                        w.lead.Ergonomics_Owner__c = def.defErgoOwner;
                    } else {
                        //NO TERRITORY - SPECIAL COUNTRY - NOT ENTERPRISE
                        //CHEMICAL MANAGEMENT OWNER - default
                        w.lead.Chemical_Management_Owner__c = def.defChemMgtOwner;
                        //TRAINING OWNER - use default
                        w.lead.Online_Training_Owner__c = def.defOTOwner;
                        //AUTHORING OWNER - use default
                        w.lead.Authoring_Owner__c = def.defAuthOwner;
                        //EHS OWNER - use special
                        w.lead.EHS_Owner__c = def.specialEHSOwner;
                        //ERGONOMICS OWNER - use default
                        w.lead.Ergonomics_Owner__c = def.defErgoOwner;
                    }
                } else {
                    if (ent) {
                        // NO TERRITORY - NO SPECIAL COUNTRY - ENTERPRISE
                        //CHEMICAL MANAGEMENT OWNER - use current owner if current owner is an enterprise rep, otherwise send to round-robin
                        if (hasEntOwner) {
                            w.lead.Chemical_Management_Owner__c = w.lead.OwnerId;
                        } else {
                            leadsNeedingEntRR.add(w.lead);
                        }
                        //TRAINING OWNER - use default
                        w.lead.Online_Training_Owner__c = def.defOTOwner;
                        //AUTHORING OWNER - use default
                        w.lead.Authoring_Owner__c = def.defAuthOwner;
                        //EHS OWNER - if current owner is ehs enterprise rep use current owner, otherwise use default
                        if (hasEHSEntOwner) {
                            w.lead.EHS_Owner__c = w.lead.OwnerId;
                        } else {
                            w.lead.EHS_Owner__c = def.entEHSOwner;
                        }
                        //ERGONOMICS OWNER - use default
                        w.lead.Ergonomics_Owner__c = def.defErgoOwner;
                    } else {
                        // NO TERRITORY - NO SPECIAL COUNTRY - NOT ENTERPRISE
                        //CHEMICAL MANAGEMENT OWNER - the owner shouldnt be an enterprise rep otherwise it would have fallen into the enterprise category, so use default
                        w.lead.Chemical_Management_Owner__c = def.defChemMgtOwner;
                        //TRAINING OWNER - use default
                        w.lead.Online_Training_Owner__c = def.defOTOwner;
                        //AUTHORING OWNER - use default
                        w.lead.Authoring_Owner__c = def.defAuthOwner;
                        //EHS OWNER - use default owner
                        w.lead.EHS_Owner__c = def.defEHSOwner;
                        //ERGONOMICS OWNER - use default
                        w.lead.Ergonomics_Owner__c = def.defErgoOwner;
                    }
                }
            }
            if (w.lead.Chemical_Management_Owner__c != null) {
                w.lead.OwnerId = w.lead.Chemical_Management_Owner__c;
            }
            updateList.add(w.lead);
        }
        
        //if we have leads needing rr, send them 
        if (leadsNeedingEntRR.size() > 0) {
            roundRobin(leadsNeedingEntRR);
        }
        
        update updateList;
    }      
    
    global void finish(Database.BatchableContext bc) {
        
    }
    
    global static void roundRobin(Lead[] leads) {
        list<Round_Robin_Assignment__c> ownerAssignments = [SELECT id, Order__c, Used_Last__c, UserID__c 
                                                            FROM Round_Robin_Assignment__c
                                                            ORDER BY Order__c ASC NULLS LAST];
        if (leads.size() > 0) {
            decimal numLeads = leads.size();
            decimal numOwners = ownerAssignments.size();
            integer lastOwnerNum = 0;
            if ((numLeads / numOwners) <= 1) {
                for (integer i = 0; i < leads.size(); i++) {
                    leads[i].Chemical_Management_Owner__c = ownerAssignments[i].UserID__c;
                    lastOwnerNum = integer.valueOf(ownerAssignments[i].Order__c);
                }
            } else {
                integer numLoops = integer.valueOf((numLeads / numOwners).round(SYSTEM.roundingMode.CEILING));  
                integer numDiff = ownerAssignments.size() - ((numLoops * ownerAssignments.size()) - leads.size());
                for (integer i = 0; i < numLoops; i ++) {
                    if (i != numLoops - 1) {
                        for (integer i2 = 0; i2 < numOwners; i2 ++) {
                            leads[i * ownerAssignments.size() + i2].Chemical_Management_Owner__c = ownerAssignments[i2].UserID__c;
                        }
                    } else {
                        for (integer i2 = 0; i2 < numDiff; i2 ++) {
                            leads[i * ownerAssignments.size() + i2].Chemical_Management_Owner__c = ownerAssignments[i2].UserID__c;
                            lastOwnerNum = integer.valueOf(ownerAssignments[i2].Order__c);
                        }
                    }
                }
            }
            
            for (Lead l : leads) {
                if (l.Chemical_Management_Owner__c != null) {
                    l.OwnerId = l.Chemical_Management_Owner__c;
                }
            }
            
            for (Round_Robin_Assignment__c owner : ownerAssignments) {
                integer ownerOrder = integer.valueOf(owner.Order__c);
                if (ownerOrder > lastOwnerNum) {
                    owner.Used_Last__c = false;
                    owner.Order__c = ownerOrder - lastOwnerNum;         
                }
                if (ownerOrder < lastOwnerNum) {
                    owner.Used_Last__c = false;
                    owner.Order__c = ownerOrder + (ownerAssignments.size() - lastOwnerNum);
                }
                if (ownerOrder == lastOwnerNum) {
                    owner.Used_Last__c = true;
                    owner.Order__c = ownerAssignments.size();
                }
            }
            update ownerAssignments;
        }
    }
    
    global class defaultOwners {
        public id defChemMgtOwner;
        public id defAuthOwner;
        public id defErgoOwner;
        public id defOTOwner;
        public id defEHSOwner;
        public id entEHSOwner;
        public id specialEHSOwner;
        public string allIds;
        public defaultOwners() {
            allIds = '';
            for (Lead_Custom_Settings__c lcs : [SELECT SetupOwnerId, Default_Authoring_Owner__c, Default_Ergo_Owner__c, Default_Online_Training_Owner__c, Default_EHS_Owner__c, Default_Chemical_Management_Owner__c,
                                                Special_Country_EHS_Owner__c, Enterprise_EHS_Owner__c
                                                FROM Lead_Custom_Settings__c
                                                WHERE Default_Chemical_Management_Owner__c = true
                                                OR Default_Authoring_Owner__c = true
                                                OR Default_Ergo_Owner__c = true
                                                OR Default_Online_Training_Owner__c = true
                                                OR Default_EHS_Owner__c = true
                                                OR Special_Country_EHS_Owner__c = true
                                                OR Enterprise_EHS_Owner__c = true]) { 
                                                    if (lcs.Default_Chemical_Management_Owner__c) {
                                                        defChemMgtOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Default_Authoring_Owner__c) {
                                                        defAuthOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Default_Ergo_Owner__c) {
                                                        defErgoOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Default_Online_Training_Owner__c) {
                                                        defOTOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Default_EHS_Owner__c) {
                                                        defEHSOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Enterprise_EHS_Owner__c) {
                                                        entEHSOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Special_Country_EHS_Owner__c) {
                                                        specialEHSOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                }
            
            
            
            
        }
    }
    
    global class wrapper {
        Lead lead;
        User owner;
        boolean hasOwner;
        boolean needsRR;
        
        public wrapper(Lead l, User o) {
            lead = l;
            if (o != null) {
                hasOwner = true;
                owner = o;
            } else {
                hasOwner = false;
            }
        }
    }
    
    
    
}