public class accountLastQuote {
	public String accountId {get;set;}

	public List<Quote__c>getQuoteLink() {
      if (accountId != null) {
        Account a = [Select Id, (Select ID, Account__c from Quotes__r order by createdDate desc limit 1) from Account where ID=:accountID];
        return a.Quotes__r;
      }
      return new List<Quote__c> ();
    }
}