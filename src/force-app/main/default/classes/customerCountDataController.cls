public class customerCountDataController {
   
    public integer totalWithoutChemTelAuth {get;set;}
   // public integer totalHQandRegX {get;set;}
    public integer totalPPI {get;set;}
    public integer totalERS {get;set;} 
    public integer totalCompliance {get;set;}
    public integer totalSafetyKit {get;set;}
    public integer totalEnviroKit {get;set;}
    public integer totalFaxBack {get;set;}

    public map<String, integer> suiteCounts {get;set;}
    public map<String, integer> productCounts {get;set;}


    public customerCountDataController() {

        productCounts = new  map<String, integer>();
        suiteCounts = new  map<String, integer>();      


        //Suites to show:
        String[] suitesToQuery = new List<String>{'MSDS Management', 'EHS Management', 'Ergonomics','MSDS Authoring','On-Demand Training'};
        
        //Query 1: get suite data
        AggregateResult[] totalsBySuite = [SELECT COUNT_DISTINCT(CustomerID__c) disCount, Product_Suite__c from Order_Item__c where Admin_Tool_Order_Status__c ='A' Group by Product_Suite__c];
        
        
        //build map for suites and then add any missing categories to the map
        for(AggregateResult ar: totalsBySuite){
            suiteCounts.put((String) ar.get('Product_Suite__c'), (integer) ar.get('disCount'));
        }        
        for(string suite : suitesToQuery){
            if(!suiteCounts.containsKey(suite)){
                suiteCounts.put(suite, 0);
            }
        }

        //list of all products we want to display - eventually pull from metadata (ideally with a sort order)
        List<String> productsToDisplay = new List<String>{'HQ','HQ RegXR', 'GM', 'GM Pro', 'GM Upgrade',  'Webpliance', 'ChemTel - Emergency Response',
        'Air', 'Audit and Inspection', 'Compliance Management', 'Corrective Action', 'IH Program Management', 'Incident Management', 'Management of Change(MOC)', 
        'Performance Metrics', 'Risk Analysis', 'Safety Meetings','Title V', 'Training Tracking','TRI (Toxic Release Inventory)', 'Waste','Water',
        'Ergo', 'The Humantech System', 'Ergopoint',
        'MSDS Authoring', 'Online Training', 'ChemTel - MSDS Authoring'
        };

        //Query 2: get product info:
        AggregateResult[] countsByName = [SELECT COUNT_DISTINCT(CustomerID__c) disCount, Admin_Tool_Product_Name__c, Product_Suite__c from Order_Item__c where Admin_Tool_Order_Status__c ='A' AND Admin_Tool_Product_Name__c IN :productsToDisplay Group by Product_Suite__c, Admin_Tool_Product_Name__c];
        
        //make a map for each result pulled back
        for(AggregateResult ar: countsByName){            
            productCounts.put((String) ar.get('Admin_Tool_Product_Name__c'), (integer) ar.get('disCount'));           
        }
        //keep visualforce page from breaking; add mapping if no data was found
        for(string pdtName : productsToDisplay){
            if(!productCounts.containsKey(pdtName)){
                productCounts.put(pdtName, 0);
            }
        }

        //Queries 3-11: get remaining counts which group products
        //totalHQandRegX = integer.Valueof([SELECT COUNT_DISTINCT(CustomerID__c) num from Order_Item__c where Admin_Tool_Order_Status__c ='A' and Admin_Tool_Product_Name__c ='HQ RegXR'][0].get('num'));
        totalPPI = integer.Valueof([SELECT COUNT_DISTINCT(CustomerID__c) num from Order_Item__c where Admin_Tool_Order_Status__c ='A' and Admin_Tool_Product_Name__c LIKE'%PPI'][0].get('num'));
        totalERS = integer.Valueof([SELECT COUNT_DISTINCT(CustomerID__c) num from Order_Item__c where Admin_Tool_Order_Status__c ='A' and Admin_Tool_Product_Name__c LIKE '%Emergency Response Services%'][0].get('num'));
        totalCompliance = integer.Valueof([SELECT COUNT_DISTINCT(CustomerID__c) num from Order_Item__c where Admin_Tool_Order_Status__c ='A' and Admin_Tool_Product_Name__c Like'%Compliance Management'][0].get('num'));
        totalSafetyKit = integer.Valueof([SELECT COUNT_DISTINCT(CustomerID__c) num from Order_Item__c where Admin_Tool_Order_Status__c ='A' and Admin_Tool_Product_Name__c like 'Safety Tool Kit%'][0].get('num'));
        totalEnviroKit = integer.Valueof([SELECT COUNT_DISTINCT(CustomerID__c) num from Order_Item__c where Admin_Tool_Order_Status__c ='A' and Admin_Tool_Product_Name__c like 'Environmental Toolkit%'][0].get('num'));
        totalFaxBack = integer.Valueof([SELECT COUNT_DISTINCT(CustomerID__c) num from Order_Item__c where Admin_Tool_Order_Status__c ='A' and Admin_Tool_Product_Name__c like 'Fax Back Service%'][0].get('num'));
        totalWithoutChemTelAuth = integer.Valueof([SELECT COUNT_DISTINCT(CustomerID__c) num from Order_Item__c where Admin_Tool_Order_Status__c ='A' and Admin_Tool_Product_Name__c !='ChemTel - MSDS Authoring'][0].get('num'));
    }

}