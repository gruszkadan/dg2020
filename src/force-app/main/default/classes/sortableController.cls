public with sharing class sortableController {
    public Sort_Tester__c[] tests {get;set;}
    
    public sortableController() {
       tests = [SELECT id, Name, Sort_Order__c FROM Sort_Tester__c ORDER BY Sort_Order__c];
    }    
}