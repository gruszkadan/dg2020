@isTest(seeAllData=true)
private class testAccountTrigger {
	
    @isTest private static void test1() {
        //query default users
        leadCustomSettings lcd = new leadCustomSettings();
        leadCustomSettings.defaultOwners def = lcd.findDefaultOwners();
        
        Account acct = new Account(Name = 'Test1');
        insert acct;
        Account checkAcct = [Select id, OwnerId, Authoring_Account_Owner__c, Online_Training_Account_Owner__c, EHS_Owner__c, Ergonomics_Account_Owner__c from Account where id = : acct.id];
        
        //With no enterprise, no parent account, and no territory, the accounts should set to defaults
        System.assert(checkAcct.OwnerId == def.defChemMgtOwner);
        System.assert(checkAcct.Ergonomics_Account_Owner__c == def.defErgoOwner);
        System.assert(checkAcct.Authoring_Account_Owner__c == def.defAuthOwner);
        System.assert(checkAcct.Online_Training_Account_Owner__c == def.defOTOwner);
        
        
        acct.Active_MSDS_Management_Products__c = 'HQ';
        update acct;       
        
        acct.BillingPostalCode = '12345';
        acct.Customer_Status__c = 'Out of Business';
        acct.OwnerId = [SELECT id FROM User WHERE Alias = 'autosim' LIMIT 1].id;
        update acct; 
        
		acct.Customer_Status__c = 'Active';        
        update acct; 
        
        delete acct;
    }

    

    @isTest private static void test2(){

        User[] owners = [SELECT id, LastName FROM User WHERE isActive = true AND Department__c = 'Sales' AND Group__c = 'Mid-Market' ORDER BY LastName DESC LIMIT 5];
        User Mark = [Select id from User WHERE isActive = true and Name = 'Mark McCauley' Limit 1];

        //query default users
        leadCustomSettings lcd = new leadCustomSettings();
        leadCustomSettings.defaultOwners def = lcd.findDefaultOwners();

        //create territory
        Territory__c terr = new Territory__c();
        terr.Name = 'XUSA';
        terr.OwnerId = owners[0].id;
        terr.Territory_Owner__c = owners[0].id;
        terr.Online_Training_Owner__c = owners[1].id;
        terr.Authoring_Owner__c = owners[2].id;
        terr.Ergonomics_Owner__c = owners[3].id;
        terr.EHS_Owner__c = owners[4].id;
        insert terr;

        //create zip
        County_Zip_Code__c zip = new County_Zip_Code__c(City__c = 'Chicago', State__c = 'IL', Territory__c = terr.id, Zip_Code__c = '99998', County__c = 'Cook', Country__c = 'United States');
        insert zip;

        id[] accts = new list<id>();

        Account acctWithTerritory = new Account(Name = 'Test2', BillingPostalCode = '99998');
        insert acctWithTerritory;
        accts.add(acctWithTerritory.id);
        
        
        Account acctWithNoTerritory = new Account(Name = 'Test3', BillingPostalCode = '88888', BillingCountry='Ireland' );
        insert acctWithNoTerritory;
        accts.add(acctWithNoTerritory.id);
       

        Account[] checkTerritoryAccts = [Select id, Name, OwnerId, Authoring_Account_Owner__c, Online_Training_Account_Owner__c, EHS_Owner__c, Ergonomics_Account_Owner__c from Account where id IN: accts];			
        Account[] toUpdate = new List<Account>();
		
        for(account a: checkTerritoryAccts){
            System.debug(a);
            if(a.Name == 'Test3'){
               System.assert(a.OwnerId == def.defInternationalOwner);
        		System.assert(a.Ergonomics_Account_Owner__c == def.defErgoOwner);
        		System.assert(a.Authoring_Account_Owner__c == def.defAuthOwner);
        		System.assert(a.Online_Training_Account_Owner__c ==  def.defOTOwner);
                
                a.BillingPostalCode = '99998';
        		a.Enterprise_Sales__c = true;
                toUpdate.add(a);
                
            }
             if(a.Name == 'Test2'){
                System.assert(a.OwnerId == terr.Territory_Owner__c);
        		System.assert(a.Ergonomics_Account_Owner__c == def.defErgoOwner);
        		System.assert(a.Authoring_Account_Owner__c == terr.Authoring_Owner__c);
        		System.assert(a.Online_Training_Account_Owner__c ==  terr.Online_Training_Owner__c);
                a.Ergonomics_Account_Owner__c = Mark.id;
                toUpdate.add(a);                
               
            }            
        }
        
        /*
        update toUpdate;
 
        Account[] updatedAccount = [Select id, Name, OwnerId, Authoring_Account_Owner__c, Online_Training_Account_Owner__c, EHS_Owner__c, Ergonomics_Account_Owner__c from Account where id IN :toUpdate];			
        System.debug(updatedAccount);      

        
        Account NameMatch = new Account(Name = 'Test2', Customer_Status__c = 'Inactive');
        insert NameMatch;
        Account matchingNameAcct = [Select id, Name, OwnerId, Authoring_Account_Owner__c, Online_Training_Account_Owner__c, EHS_Owner__c, Ergonomics_Account_Owner__c from Account where id = :NameMatch.id AND Customer_Status__c = 'Inactive'];	
        System.debug(matchingNameAcct);
*/
        
    }
    
    
     @isTest private static void test3(){

        User[] owners = [SELECT id, LastName FROM User WHERE isActive = true AND Department__c = 'Sales' AND Group__c = 'Mid-Market' ORDER BY LastName DESC LIMIT 5];
        User Mark = [Select id from User WHERE isActive = true and Name = 'Mark McCauley' Limit 1];

        //query default users
        leadCustomSettings lcd = new leadCustomSettings();
        leadCustomSettings.defaultOwners def = lcd.findDefaultOwners();

        //create territory
        Territory__c terr = new Territory__c();
        terr.Name = 'XUSA';
        terr.OwnerId = owners[0].id;
        terr.Territory_Owner__c = owners[0].id;
        terr.Online_Training_Owner__c = owners[1].id;
        terr.Authoring_Owner__c = owners[2].id;
        terr.Ergonomics_Owner__c = owners[3].id;
        terr.EHS_Owner__c = owners[4].id;
        insert terr;

        //create zip
        County_Zip_Code__c zip = new County_Zip_Code__c(City__c = 'Chicago', State__c = 'IL', Territory__c = terr.id, Zip_Code__c = '99998', County__c = 'Cook', Country__c = 'United States');
        insert zip;

        id[] accts = new list<id>();

        Account parent = new Account(Name = 'Parent');
        insert parent;
        accts.add(parent.id);
         
         parent.OwnerId = Mark.id;
         parent.Ergonomics_Account_Owner__c = Mark.id;
         update parent;
        
        
        Account child = new Account(Name = 'Child', BillingPostalCode = '99998', ParentId = parent.id, Enterprise_Sales__c = true);
        insert child;
        accts.add(child.id);
         
        Account child2 = new Account(Name = 'Child2', BillingPostalCode = '77777', ParentId = parent.id);
         insert child2;
         accts.add(child2.id);
         
         
         Account[] checkHierarchy = [Select id, Name, OwnerId, Authoring_Account_Owner__c, Online_Training_Account_Owner__c, EHS_Owner__c, Ergonomics_Account_Owner__c from Account where id IN: accts Order by BillingPostalCode DESC];			
         
         System.assert(checkHierarchy[0].OwnerId == Mark.id);
         System.assert(checkHierarchy[1].OwnerId == Mark.id);
         System.assert(checkHierarchy[1].Ergonomics_Account_Owner__c == Mark.id);
         System.assert(checkHierarchy[1].Authoring_Account_Owner__c == terr.Authoring_Owner__c);
         System.assert(checkHierarchy[1].Online_Training_Account_Owner__c ==  terr.Online_Training_Owner__c);     
         System.assert(checkHierarchy[2].OwnerId != Mark.id);
         System.assert(checkHierarchy[2].Ergonomics_Account_Owner__c == Mark.id);
         System.assert(checkHierarchy[2].Authoring_Account_Owner__c == def.defAuthOwner);
         System.assert(checkHierarchy[2].Online_Training_Account_Owner__c ==  def.defOTOwner); 
         
     }
    

}