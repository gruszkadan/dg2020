@isTest
public class testAmeConsoleBatchManagement {
     static testMethod void test1(){


         
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Primary_Admin__c = true;
        insert newContact;
        
        
        //Create new AME
        Account_Management_event__c Ame1 = new Account_Management_event__c();
        //Set the required fields for test 
        Ame1.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        Ame1.Account__c = newAccount.id;
        Ame1.Contact__c = newContact.id;
        Ame1.Status__c = 'Not Yet Created';
        Ame1.Not_Yet_Processed__c = TRUE;
        insert Ame1;

        testDataUtility Util = new testDataUtility(); 
        order_item__c newOrderItem =  Util.createOrderItem('House Sale', '994321');
        newOrderItem.Product_Platform__c = 'MSDSonline';
        newOrderItem.Renewal_Amount__c = 100;
        newOrderItem.Account__c = newAccount.Id;
        newOrderItem.Account_Management_Event__c = Ame1.id; 
        newOrderItem.Admin_Tool_Order_Status__c = 'A';
        insert newOrderItem;
  
         
        //Create new AME
        Account_Management_event__c Ame2 = new Account_Management_event__c();
        //Set the required fields for test 
        Ame2.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        Ame2.Account__c = newAccount.id;
        Ame2.Contact__c = newContact.id;
        Ame2.Status__c = 'Active';
        insert Ame2;

        order_item__c newOrderItemA =  Util.createOrderItem('House Sale', '994323');
        newOrderItemA.Product_Platform__c = 'MSDSonline';
        newOrderItemA.Renewal_Amount__c = 10000;
        newOrderItemA.Account__c = newAccount.Id;
        newOrderItemA.Account_Management_Event__c = Ame2.id;
        newOrderItemA.Admin_Tool_Order_Status__c = 'A';
        insert newOrderItemA;
         
       Account_Management_event__c AmeToDel = new Account_Management_event__c();
        //Set the required fields for test 
        AmeToDel.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        AmeToDel.Account__c = newAccount.id;
        AmeToDel.Contact__c = newContact.id;
        AmeToDel.Status__c = 'Not Yet Created';
        AmeToDel.Not_Yet_Processed__c = False;    
        insert AmeToDel;
         
         Account_Management_event__c AmeToDel2 = new Account_Management_event__c();
        //Set the required fields for test 
        AmeToDel2.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        AmeToDel2.Account__c = newAccount.id;
        AmeToDel2.Contact__c = newContact.id;
        AmeToDel2.Status__c = 'Not Yet Created';
        AmeToDel2.Not_Yet_Processed__c = False;    
        insert AmeToDel2;
         
        ameToDel = [Select RecordTypeId, RecordType.Name,Status__c,Not_Yet_Processed__c FROM Account_Management_event__c where id = :AmeToDel.id]; 
        System.debug(ameToDel);
         
        user u = [select id from user where isActive = true AND UserRole.Name != 'Director of Customer Success' AND Profile.Name LIKE '%Retention%' Limit 1];

        Test.startTest();
        ameConsoleBatchManagementController con = new ameConsoleBatchManagementController(); 
         
        system.currentpagereference().getparameters().put('ameChanged', Ame1.id); 
        System.currentpagereference().getparameters().put('viewToSave', 'review');  
        System.currentpagereference().getparameters().put('owner', u.id); 
        con.setOwner();
         
     
        system.currentpagereference().getparameters().put('ameAccount', newAccount.Id);
        system.currentpagereference().getparameters().put('ameToUpdate', Ame1.id);
        system.currentpagereference().getparameters().put('name', Ame1.name);           
         con.loadAccountOI(); 
         
         con.currentTab = 'pendingActivation';
         con.runOnLoad();
         System.debug(con.pendingActivationPaginated);
         system.currentpagereference().getparameters().put('ameToDelete', AmeToDel.Id);
         con.immediateDelete();
         Account_Management_event__c[] checkDel = [Select id, name from Account_Management_event__c where id = :AmeToDel.Id];
         System.debug(checkDel.size());
         
        con.currentTab = 'review';
         con.runOnLoad();
         System.debug(con.keys[0]);
         
         system.currentpagereference().getparameters().put('owner', con.keys[0]);  
         system.currentpagereference().getparameters().put('ameChanged', Ame1.id); 
         system.currentpagereference().getparameters().put('viewToReturnTo', 'review');           
         con.saveAMEs();
         
         con.oiAvailable4Selection[0].oiSelected = true;
		 con.associateOrderItems();
         System.debug(con.pendingReviewPaginated.size());
         con.pendingReviewPaginated[0].ameSelected = true;
         con.processAMEs();

         con.currentTab = 'pendingActivation';
         con.runOnLoad();
         system.currentpagereference().getparameters().put('owner', con.keys[0]);  
         system.currentpagereference().getparameters().put('ameChanged', Ame1.id); 
         system.currentpagereference().getparameters().put('view', 'pendingActivation');           
         con.saveAMEs();
         
         con.setOwner();
         
         con.currentTab = 'review';
         con.deleteAMEs();
         con.returnToBatchConsole();
         con.nextRenewals();
         con.previousRenewals();
         con.toggleAndQuery();
         con.getItems();
       
         
          ameRenewalStatsController stats = new ameRenewalStatsController(); 
          stats.getTheStats();
         Test.stopTest();
    }
}