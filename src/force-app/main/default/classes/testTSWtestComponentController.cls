@isTest
public class testTSWtestComponentController {
    
    static testmethod void test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        newAccount.Corporate_Account__c = true;
        newAccount.Corporate_Pricing_Expiration_Date__c = date.today() + 30;
        newAccount.Corporate_Account_Locations_Link__c = 'testlink';
        newAccount.Corporate_Pricing_Description__c = 'test';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.LastName = 'Test';
        newContact.Accountid = newAccount.id;
        insert newContact;
        
        Contact newContactTwo = new Contact();
        newContactTwo.LastName = 'Test Two';
        newContactTwo.Accountid = newAccount.id;
        insert newContactTwo;

        TSWtestComponetController con = new TSWtestComponetController();
        Contact[] testContacts = TSWtestComponetController.showContacts('Test Account');
        Contact[] testContacts2 = TSWtestComponetController.getSpecificContacts(newAccount.Id);
    }

}