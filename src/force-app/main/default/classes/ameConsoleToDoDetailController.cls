public class ameConsoleToDoDetailController {
    public Id ameRecordId {get;set;}
    public String cView {get;set;}
    public String sField {get;set;}
    public String sDirection {get;set;}
    public String vUser {get;set;}
    public Account_Management_Event__c ame {get;set;}
    public Account_Management_Event__c atRiskAME {get;set;}
    public Account_Management_Event__c cancellationAME {get;set;}
    public Account_Management_Event__c relatedAME {get;set;}
    public Account_Management_Event__c cancellationQueue {get;set;}
    public string approvalcomments {get;set;}
    public boolean showEdit {get;set;}
    Public String RecordType {get;set;}
    public id rtAtRisk {get;set;}
    public id rtRenew {get;set;}
    public id rtRAM {get;set;}
    public id rtCancel {get;set;}
    public id rtVip {get;set;}
    public Order_Item__c[] ameOrderItems {get;set;}
    public orderItemWrapper[] oItemWrapper {get;set;}
    public String errorMessage {get;set;}
    public String alertIcon {get;set;}
    public Boolean isSuccess {get;set;}
    public boolean canInlineEdit {get;set;}
    public Opportunity[] Opp {get;set;}
    public Opportunity[] oList {get;set;}
    public boolean showReqCanButton {get;set;}
    public boolean saveReqCancel {get;set;}
    
    public boolean getInitializeRecord(){ 
        
        //Settingup variables of rectord type id's
        rtRenew = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        rtAtRisk = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('At Risk').getRecordTypeId();  
        rtRAM = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();  
        rtCancel = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();  
        rtVip = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('VIP Account Management').getRecordTypeId(); 
        
        // This is the object for which we required data.
        Map<String, Schema.SObjectField> fieldMap = Account_Management_Event__c.sObjectType.getDescribe().fields.getMap();
        
        // Get all of the fields on the object
        Set<String> fieldNames = fieldMap.keySet();
        
        // query for opportunity fields used on this page 
          Opp = [Select Id, Closed_Reasons__c
               from opportunity 
               where Account_Management_Event__c =: ameRecordId
               order by CreatedDate desc
               limit 1];
        
        
        //additional fields to query
        String addlFields = ',RecordType.Name,Account__r.Synopsis_MSDS__c,Account__r.Name,Contact__r.Name,Related_AME__r.Name,Related_AME__r.Account__c,Owner.Name,Related_AME__r.RecordType.Name,(SELECT Id, Related_AME__c, RecordType.Name FROM Account_Management_Events__r WHERE RecordType.Name = \'Cancellation\' LIMIT 1) ';

        // Build a Dynamic Query String.
        ame = Database.query('select ' + string.join((Iterable<String>)fieldNames, ',') +addlFields+ ' from Account_Management_Event__c where id=\''+ameRecordId+'\'');
        
        if(ame.Account_Management_Events__r.size() > 0){
            showReqCanButton = true;
        }else{
            showReqCanButton = false;
        }
        
        //Disable inline edit when Status is Pending Approval
        if(ame.Status__c=='Pending Approval'){
            canInlineEdit=False;
        }else{
            canInlineEdit=true;
        }
        return false;
        
    }   
    
    // method to edit output fields.
    public void inputFieldEdit(){
        showEdit = true;
    }  
    
    //to display select drop-down list from contacts field
    public List<SelectOption> getContactSelectList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- Select Contact --',false));        
        List<Contact> contacts = [SELECT Id, Name FROM Contact where AccountID=:ame.Account__c Order By Primary_Admin__c DESC, Name ASC LIMIT 100];
        for (Contact contact : contacts) {
            options.add(new SelectOption(contact.ID, contact.Name));
        }
        return options;
    }
    
    //Submit for Approval method
    public pageReference submitforapproval(){
        //Query for Renewal RecordType
        List<Account_Management_Event__c> relatedAME = [Select Id, RecordType.Name, Related_AME__c
                                                    From Account_Management_Event__c 
                                                    Where RecordType.Name ='Renewal' AND Id =: ame.Related_AME__c LIMIT 1];
        if(relatedAME.size() > 0){
            //Query for opportunity StageName
            oList = [select Id, StageName from Opportunity Where isClosed = False AND Account_Management_Event__c =: relatedAME[0].Id];
            //If RT is Cancellation and RelatedAME is renewal
            If(ame.RecordTypeId == rtCancel && relatedAME[0].RecordType.Name == 'Renewal'){
                //If RelatedAME has related opportunities
                if(oList.size()>0){
                    ameUtility.pendingOpp(oList); 
                }
            }
        }
        ameUtility.submitforapproval(ameRecordId, approvalcomments);
        PageReference pr = Page.ame_console_to_do;
        pr.getParameters().put('Id', ameRecordId);
        pr.getParameters().put('currentView', cView);
        pr.getParameters().put('sortField', sField);
        pr.getParameters().put('sortDirection', sDirection);
        pr.getParameters().put('va', vUser);
        pr.setRedirect(true);
        return pr;
    }
    
    
    
    //convert to At Risk method 
    //Assigning values to the At Risk fields
    public void convertToAtRisk()
    { 
        errorMessage = null;
        atRiskAME = new Account_Management_Event__c();
        atRiskAME.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('At Risk').getRecordTypeId(); 
        atRiskAME.Related_AME__c =  ame.id;
        atRiskAME.Account__c =  ame.Account__c;
        atRiskAME.Contact__c =  ame.Contact__c;
        atRiskAME.OwnerId =  ame.OwnerId;
        atRiskAME.Status__c = 'Active'; 
    }    
    
    //Save method for convert to at risk
    public pageReference saveAtRisk(){ 
        pageReference pr = null;
        if(ameUtility.canSave(atRiskAME)){
            try{
                insert atRiskAME;
            }
            catch(System.DMLexception e) {
                errorMessage = string.valueOf(e);
                return pr;
            }
            pr = page.ame_console_to_do;
            pr.setRedirect(true);
            pr.getParameters().put('Id', atRiskAME.Id);
            pr.getParameters().put('currentView', cView);
            pr.getParameters().put('sortField', sField);
            pr.getParameters().put('sortDirection', sDirection);
            pr.getParameters().put('va', vUser);
        }else{
            errorMessage = 'Please make sure all required fields are filled out';
        }
        return pr;
    }  
    
    
    //Cancel method
    public pageReference Cancel(){
        pageReference pr = page.ame_console_to_do;
        pr.getParameters().put('Id', ameRecordId);
        pr.getParameters().put('currentView', cView);
        pr.getParameters().put('sortField', sField);
        pr.getParameters().put('sortDirection', sDirection);
        pr.getParameters().put('va', vUser);
        return pr;
    }
    
    //Delete method
    public pageReference xDelete(){
        delete ame;
        pageReference pr = page.ame_console_to_do;
        pr.getParameters().put('currentView', cView);
        pr.getParameters().put('sortField', sField);
        pr.getParameters().put('sortDirection', sDirection);
        pr.getParameters().put('va', vUser);
        pr.setRedirect(true);
        return pr;
    }
    
    //querying order item fields
    public void orderItems(){
        ameOrderItems = [SELECT Id, Admin_Tool_Product_Name__c, Renewal_Amount__c, Term_Start_Date__c, Term_End_Date__c, Do_Not_Renew__c, Account__r.Channel__c, Cancelled_On_AME__c 
                         FROM Order_Item__c 
                         WHERE Admin_Tool_Order_Status__c = 'A' AND Account__c = :ame.Account__c];
    }
    
    //set fields for new Cancellation AME 
    public void rCancellation()
    {
        errorMessage = null;
        cancellationAME = new Account_Management_Event__c();
        cancellationAME.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId(); 
        cancellationAME.Contact__c =  ame.Contact__c;
        cancellationAME.Account__c =  ame.Account__c;
        cancellationAME.OwnerId =  ame.OwnerId;
        cancellationAME.Status__c = 'Active'; 
        cancellationAME.Related_AME__c = ameRecordId;
        //saveReqCancel = null;
        orderItems();
        loadOrderItemWrappers();
    }
    
    //Save method for cancellation AME 
    public void saveCancellation(){
        if(ameUtility.canSave(cancellationAME)){
            //Creating a list of order items
            Order_Item__c[] selectedOrderItemsList = new list<Order_Item__c>();
            //if( isSelected = true )
            //Looping through the list if the user selects an item in the table 
            for (orderItemWrapper o : oItemWrapper){
                if(o.isSelected == true){ 
                    selectedOrderItemsList.add(o.orderItem);
                }
            }
            //Update the list if size>0
            if(selectedOrderItemsList.size() > 0){
                insert cancellationAME;
                saveReqCancel =true;
                ameUtility.canAME(cancellationAME.Id, selectedOrderItemsList);
            }else{
            saveReqCancel = false;
            errorMessage = 'Please make sure to select at least one order item';
            }
        }else{
            saveReqCancel = false;
            errorMessage = 'Please make sure to fill all the required fields';
        }
     
    }
    
    
    //New list to store selected wrappers
    public void loadOrderItemWrappers(){
        oItemWrapper = new List<orderItemWrapper>(); 
        if(ameOrderItems.size() > 0){
            for(Order_Item__c oI:ameOrderItems){
                oItemWrapper.add(new orderItemWrapper(oI));
            }
        }
    }
    
    
    //save method to update ameRecord for the main page
    public pageReference save(){ 
        pageReference pr = null;
        if(ameUtility.cansave(ame)){
            try{
                update ame;
            }
            catch(System.DMLexception e) {
                errorMessage = string.valueOf(e);
                return pr;
            }
            pr = page.ame_console_to_do;
            pr.getParameters().put('Id', ameRecordId);
            pr.getParameters().put('currentView', cView);
            pr.getParameters().put('sortField', sField);
            pr.getParameters().put('sortDirection', sDirection);
            pr.getParameters().put('va', vUser);   
            pr.setRedirect(true);
        }else{
            errorMessage = 'Please make sure all required fields are filled out';
        }
        return pr;
    }
    
    //sent to orders Method
    public pageReference sentToOrders(){
        ameutility.cancellationQueue(ame);
        pageReference pr = page.ame_console_to_do;
        pr.getParameters().put('Id', ameRecordId);
        pr.getParameters().put('currentView', cView);
        pr.getParameters().put('sortField', sField);
        pr.getParameters().put('sortDirection', sDirection);
        pr.getParameters().put('va', vUser);  
        pr.setRedirect(true);
        return pr;
    }
  
    public pageReference returnAmeConsole(){
        string Type = ApexPages.currentPage().getParameters().get('returnId'); 
        pageReference pr = page.ame_console_to_do;
        pr.getParameters().put('Id', Type);
        pr.getParameters().put('currentView', cView);
        pr.getParameters().put('sortField', sField);
        pr.getParameters().put('sortDirection', sDirection);
        pr.getParameters().put('va', vUser);   
        pr.setRedirect(true);
        return pr;
    }

        
    //orderitem wrapper
    public class orderItemWrapper {
        public Order_Item__c orderItem {get;set;}
        public boolean isSelected {get;set;}
        
        //Setting the two variables defined in the above class   
        public orderItemWrapper(Order_Item__c xOrderItem){
            orderItem = xOrderItem;
            isSelected = false;
        }
    }
}