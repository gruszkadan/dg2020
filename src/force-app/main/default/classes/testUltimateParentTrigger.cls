@isTest(seeAllData=true)
private class testUltimateParentTrigger {
    
    //test update
    static testmethod void test1() {
        
        Account parent = new Account(Name = 'Parent Account');
        insert parent;
        
        Account child = new Account(Name = 'Child Account');
        insert child;
        
        child.ParentId = parent.id;
        update child;
        
    }
    
    //test insert
    static testmethod void test2() {
        
        Account parent = new Account(Name = 'Parent Account');
        insert parent;
        
        Account child = new Account(Name = 'Child Account', ParentId = parent.id);
        insert child;
        
    }

}