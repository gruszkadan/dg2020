public class ownershipChangeNotifications {
    
    public static void createOCN(Account[] accounts){
        Ownership_Change_Notification__c[] newOCN = new list<Ownership_Change_Notification__c>();
        Ownership_Change_Notification__c[] existingOCN = new list<Ownership_Change_Notification__c>();
        Ownership_Change_Notification__c[] deleteOCN = new list<Ownership_Change_Notification__c>();
        existingOCN = [SELECT id,Name FROM Ownership_Change_Notification__c];
        accounts = [SELECT id,Name,OwnerId,Owner.Email,Owner.Name FROM Account where id in:accounts];
        if(existingOCN.size() > 0){
            for(Ownership_Change_Notification__c ocn:existingOCN){
                for(Account a:accounts){
                    if(a.id == ocn.Name){
                        deleteOCN.add(ocn);
                    }
                }
            }
        }
        for(Account a:accounts){
            Ownership_Change_Notification__c ocn = new Ownership_Change_Notification__c();
            ocn.Name = a.id;
            ocn.Account_ID__c = a.id;
            ocn.Account_Name__c = a.Name;
            ocn.New_Owner_Email__c = a.Owner.Email;
            ocn.New_Owner_Name__c = a.Owner.Name;
            newOCN.add(ocn);
        }
        if(deleteOCN.size() > 0){
            try {
                delete deleteOCN;
            } catch(exception e) {
                salesforceLog.createLog('Ownership Change Notification', true, 'ownershipChangeNotifications', 'createOCN', string.valueOf(e));
            }
        }
        try {
            insert newOCN;
        } catch(exception e) {
            salesforceLog.createLog('Ownership Change Notification', true, 'ownershipChangeNotifications', 'createOCN', string.valueOf(e));
        }
    }
    
 
    //new version for lead notifcations
    public static void createLeadOCN(Lead[] leads){
       
        Ownership_Change_Notification__c[] newOCN = new list<Ownership_Change_Notification__c>();
        Ownership_Change_Notification__c[] existingOCN = new list<Ownership_Change_Notification__c>();
        Ownership_Change_Notification__c[] deleteOCN = new list<Ownership_Change_Notification__c>();
        existingOCN = [SELECT id,Name FROM Ownership_Change_Notification__c];
        leads = [SELECT id,Name, Company, OwnerId,Owner.Email,Owner.Name FROM lead where id in:leads];

        if(existingOCN.size() > 0){
            for(Ownership_Change_Notification__c ocn:existingOCN){
                for( lead l:leads){
                    if(l.id == ocn.Name){
                        deleteOCN.add(ocn);
                    }
                }
            }
        }

        for(Lead l:leads){
            
            Ownership_Change_Notification__c ocn = new Ownership_Change_Notification__c();
            ocn.Name = l.id;
            ocn.Account_ID__c = l.id;
            ocn.Account_Name__c = l.Company;
            ocn.New_Owner_Email__c = l.Owner.Email;
            ocn.New_Owner_Name__c = l.Owner.Name;
            newOCN.add(ocn);
        }
        
        if(deleteOCN.size() > 0){
            try {
                delete deleteOCN;
            } catch(exception e) {
                salesforceLog.createLog('Ownership Change Notification', true, 'ownershipChangeNotifications', 'createLeadOCN', string.valueOf(e));
            }
        }
        try {
            insert newOCN;
        } catch(exception e) {
            salesforceLog.createLog('Ownership Change Notification', true, 'ownershipChangeNotifications', 'createLeadOCN', string.valueOf(e));
        }
    }

    
    
}