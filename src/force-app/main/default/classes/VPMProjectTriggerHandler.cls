public class VPMProjectTriggerHandler {
    public static void afterUpdate(VPM_Project__c[] oldProject, VPM_Project__c[] newProject) {
        VPM_Project__c[] vpmProjectEmailDates = new list<VPM_Project__c>();
        for (integer i=0; i<newProject.size(); i++) {
            if(oldProject[i].Scheduled_Upgrade_Date__c != newProject[i].Scheduled_Upgrade_Date__c || oldProject[i].Actual_Upgrade_Date__c != newProject[i].Actual_Upgrade_Date__c){
               vpmProjectEmailDates.add(newProject[i]);
            }
        }
        if(vpmProjectEmailDates.size() > 0){
            VPMTaskUtility.upgradeEmailVpmProjects(vpmProjectEmailDates);
        }
    }
}