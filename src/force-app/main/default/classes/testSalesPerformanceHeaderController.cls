@isTest
private class testSalesPerformanceHeaderController {
    private static testmethod void test1() {
        Sales_Performance_Settings__c setting = new Sales_Performance_Settings__c();
        setting.SetupOwnerId = UserInfo.getUserId();
        setting.Default_View__c = 'Rep';
        setting.View_As_Enabled__c = true;
        setting.View_As_Multiple_Sales_Teams__c = true;
        insert setting;
        
        salesPerformanceHeaderController s = new salesPerformanceHeaderController();
        s.UserID = UserInfo.getUserId();
        s.activeTab = 'Dashboard';
        s.getSubordinateUsers();
        s.viewAsRedirect();
        System.assert(s.errorMsg != null);
        s.viewAsName = 'Nick Kempf';
        s.viewAsRedirect();
        s.resetView();
    }
    
}