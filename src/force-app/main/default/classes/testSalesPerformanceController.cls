@isTest
private class testSalesPerformanceController {
    private static testmethod void test1() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        User vp;
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
        }
        
        System.runAs(vp) {
            testDataUtility util = new testDataUtility();
            
            Sales_Performance_Summary__c[] vpSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=1; i<=12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, null);
                sps.Month__c = testDataUtility.findMonth(i);
                sps.Manager__c = null;
                sps.Sales_Team_Manager__c = vp.id;
                sps.Total_Bookings__c = i;
                sps.Quota__c = i;
                vpSPSs.add(sps);
            }
            insert vpSPSs;
            
            Account acct = util.createAccount('Test');
            insert acct;
            
            Opportunity opp1 = util.createOppty(acct.id, date.today());
            insert opp1;
            
            Opportunity opp2 = util.createOppty(acct.id, date.today().addMonths(1));
            insert opp2;
            
            Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
            oi1.Month__c = 'January';
            insert oi1;
            
            test.startTest();
            
            salesPerformanceController con = new salesPerformanceController();
            
            // Assert the opportunities were found for this and next month
            System.assert(con.thisMonth.size() == 1);
            System.assert(con.nextMonth.size() == 1);
            
            string monthlySales = '';
            for (integer i=1; i<=12; i++) {
                con.monthNum = i;
                con.monthlySalesData();
                monthlySales += i + '.00';
                if (i <= 12) {
                    monthlySales += ',';
                }
            }
            System.assert(con.monthlySales == monthlySales);
            con.getTopFiveOrders();
            test.stopTest();
        }
        
    }
    
    private static testmethod void testOpportunityOutlook() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        User vp;
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
        }
        
        System.runAs(vp) {
            testDataUtility util = new testDataUtility();
            
            Sales_Performance_Summary__c[] vpSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=1; i<=12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, null);
                sps.Month__c = testDataUtility.findMonth(i);
                sps.Manager__c = null;
                sps.Sales_Team_Manager__c = vp.id;
                sps.Total_Bookings__c = i;
                sps.Quota__c = i;
                vpSPSs.add(sps);
            }
            insert vpSPSs;
            
            Account acct = util.createAccount('Test');
            insert acct;
            
            Opportunity opp1 = util.createOppty(acct.id, date.today());
            insert opp1;
            
            Opportunity opp2 = util.createOppty(acct.id, date.today().addMonths(1));
            insert opp2;
            
            Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
            oi1.Month__c = 'January';
            insert oi1;
            
            test.startTest();
            
            salesPerformanceOpportunityOutlook con = new salesPerformanceOpportunityOutlook();
            
            // Assert the opportunities were found for this month
            System.assert(con.opptys.size() == 1);
            
            con.getStageName();
            
            // close the opportuninites
            con.opptys[0].StageName = 'Closed/Won';
            con.opptys[0].CloseDate = date.today();
            
            con.opptys[0].StageName = 'Closed/Won';
            con.opptys[0].CloseDate = date.today();
            
            // save the closed opps
            con.saveOpportunties();
            // make sure the save was successful
            System.assert(con.isSuccess == true);
            
            con.opportunityFilters[0].isSelected = true;
            con.applyOpportunityFilter();
            con.removeAllOpportunityFilters();
            test.stopTest();
        }
        
    }
}