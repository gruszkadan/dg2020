@isTest(SeeAllData=true)
private class testSFRequestDetail {
    
    static testmethod void test1() {     
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        Salesforce_Request__c req = new Salesforce_Request__c();
        req.Summary__c = 'test';
        req.Description__c = 'test';
        insert req;
        
        // Create the email body
        email.plainTextBody = 'this is a test';
        email.fromAddress ='mmccauley@ehs.com';
        email.subject = 'RE: New Salesforce Request Note - ' + req.Name;
        
        createSFRequestNoteFromEmail edr = new createSFRequestNoteFromEmail();
        edr.handleInboundEmail(email,env);
        
        
        Messaging.InboundEmailResult result = edr.handleInboundEmail(email, env);
        System.assertEquals(result.success, true);
    }
    
    static testmethod void test2() {
        
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        newRequest.Summary__c = 'Test Account';
        newRequest.request_type__c = 'Merge';
        newRequest.primary_merge_account__c = 'www.primary.com';
        newRequest.phone_to_keep__c = '(000)000-0000';
        insert newRequest;
        
        Salesforce_Request_Merge__c newMerge = new Salesforce_Request_Merge__c();
        newMerge.Salesforce_Request__c = newRequest.Id;
        newMerge.Secondary_Account_URL__c = 'www.secondary1.com';
        insert newMerge;
        
        ApexPages.currentPage().getParameters().put('Id', newRequest.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request__c());
        sfRequestNew myController = new sfRequestNew(testController);
        
        myController.saveEdits(); 
        myController.onsave();
        myController.save();
        myController.delWrapper();
        myController.addRows();
        mycontroller.getRequest();
    }    
    
    static testmethod void test3() {
        
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        newRequest.Summary__c = 'Test Account';
        newRequest.Sidebar_Pin__c = 'Werner';
        newRequest.request_type__c = 'Merge';
        newRequest.primary_merge_account__c = 'www.primary.com';
        newRequest.phone_to_keep__c = '(000)000-0000';
        insert newRequest;
        
        Salesforce_Request_Merge__c newMerge = new Salesforce_Request_Merge__c();
        newMerge.Salesforce_Request__c = newRequest.Id;
        newMerge.Secondary_Account_URL__c = 'www.secondary1.com';
        insert newMerge;
        
        ApexPages.currentPage().getParameters().put('Id', newRequest.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request__c());
        sfRequestDetail myController = new  sfRequestDetail(testController);
        mycontroller.sfr = newRequest;
        myController.completeButton();
        myController.saveComponentEdits();
        myController.addScenario();
        myController.scenarioDetail();
        myController.scenDetailReorder();
    } 
    
    static testmethod void test4() {
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        newRequest.Summary__c = 'Test Account';
        newRequest.request_type__c = 'Admin';
        insert newRequest;
        
        ApexPages.currentPage().getParameters().put('Id', newRequest.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request__c());
        sfRequestDetail myController = new  sfRequestDetail(testController);
        
        mycontroller.sfr = newRequest;
        myController.startTimer(); 
    } 
    
    static testmethod void test5() {
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        newRequest.Summary__c = 'Test Account';
        newRequest.request_type__c = 'Admin';
        newRequest.OwnerId ='00580000003UChI';
        insert newRequest;
        
        Salesforce_Request_Task__c newSFTask = new Salesforce_Request_Task__c();
        newSFTask.Work_Request__c = newRequest.Id;
        newSFTask.Owner__c ='00580000003UChI';
        newSFTask.Start_Date__c = dateTime.now();
        insert newSFTask;       
        
        ApexPages.currentPage().getParameters().put('Id', newRequest.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request__c());
        sfRequestDetail myController = new  sfRequestDetail(testController);
        
        mycontroller.sfr = newRequest;
        myController.endTimer(); 
    } 
    static testmethod void test6() {
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        newRequest.Summary__c = 'Test Account';
        newRequest.request_type__c = 'Admin';
        newRequest.OwnerId ='00580000003UChI';
        newRequest.Requested_By__c='00580000003UChI';
        insert newRequest;
        
        ApexPages.currentPage().getParameters().put('CF00N800000056H7C_lkid', newRequest.Id);
        ApexPages.currentPage().getParameters().put('CF00N800000056H7C', newRequest.Name);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request_Note__c());
        sfRequestNote myNoteController = new  sfRequestNote(testController);
        myNoteController.note.To_Recipient__c = [SELECT ID, Email FROM USER WHERE FirstName = 'Mark' AND LastName = 'McCauley' AND isActive= TRUE Limit 1].Email; 
        myNoteController.note.Salesforce_Update__c= newRequest.ID;        
        myNoteController.note.Comments__c='testests';
        myNoteController.onSave();
    }
    static testmethod void testComponentCompleted() {
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        newRequest.Summary__c = 'Test Account';
        newRequest.request_type__c = 'Enhancement';
        newRequest.OwnerId = [SELECT ID FROM USER WHERE FirstName = 'Joseph' AND LastName = 'Berkowitz' Limit 1].Id;
        newRequest.Requested_By__c= [SELECT ID FROM USER WHERE FirstName = 'Joseph' AND LastName = 'Berkowitz' Limit 1].Id;
        insert newRequest;
        
        Salesforce_Component__c newComponent = new Salesforce_Component__c();
        newComponent.Name = 'Test Component';
        newComponent.Object__c = 'Test Object';
        newComponent.Type__c = 'Custom Field';
        insert newComponent;
        
        Salesforce_Request_Component__c newRC = new Salesforce_Request_Component__c();
        newRC.Name__c = 'Test Component';
        newRC.Object__c = 'Test Object';
        newRC.Type__c = 'Custom Field';
        newRC.Salesforce_Request__c = newRequest.Id;
        newRC.Salesforce_Component__c = newComponent.Id;
        insert newRC; 
        
        newRequest.start_date__c = date.Today();
        newRequest.completed_date__c = date.Today();
        newRequest.status__c = 'Completed';
        update newRequest;
        
        newRequest.status__c = 'Requested';
        update newRequest;
    }
    
    
    
    
    
    static testmethod void testTeamBreakdown(){
        
        Salesforce_Request__c x = new Salesforce_Request__c();
        insert x;
        
        User d = [SELECT ID FROM User WHERE Name = 'Daniel Gruszka'];
        User a = [SELECT ID FROM User WHERE Name = 'Al Powell'];
        User t = [SELECT ID FROM User WHERE Name = 'Tara Wills'];
        User m = [SELECT ID FROM User WHERE Name = 'Mark McCauley'];
        User n = [SELECT ID FROM User Where Name = 'Mac Nosek'];
        User p = [SELECT ID FROM User Where Name = 'Priyanka Goriparthi'];
        
        
        Salesforce_Request_Task__c dan = new Salesforce_Request_Task__c(Owner__c = d.ID, Start_Date__c = DateTime.Now(), End_Date__c = DateTime.Now().addMinutes(60), Work_Request__c = x.id);
        Salesforce_Request_Task__c al = new Salesforce_Request_Task__c(Owner__c = a.ID, Start_Date__c = DateTime.Now(), End_Date__c = DateTime.Now().addMinutes(60), Work_Request__c = x.id);
        Salesforce_Request_Task__c tara = new Salesforce_Request_Task__c(Owner__c = t.ID, Start_Date__c = DateTime.Now(), End_Date__c = DateTime.Now().addMinutes(60), Work_Request__c = x.id);
        Salesforce_Request_Task__c mark = new Salesforce_Request_Task__c(Owner__c = m.ID, Start_Date__c = DateTime.Now(), End_Date__c = DateTime.Now().addMinutes(60), Work_Request__c = x.id);
        Salesforce_Request_Task__c mac = new Salesforce_Request_Task__c(Owner__c = n.Id, Start_Date__c = DateTime.Now(), End_Date__c = DateTime.Now().addMinutes(60), Work_Request__c = x.id);
        Salesforce_Request_Task__c priyanka = new Salesforce_Request_Task__c(Owner__c = p.Id, Start_Date__c = DateTime.now(), End_Date__c = DateTime.Now().addMinutes(60), Work_Request__c = x.id);
        
        insert dan;
        insert al;
        insert tara;
        insert mark;
        insert mac;
        insert priyanka;
        
        id  sfrId = x.id;  
        ApexPages.currentPage().getParameters().put('id', sfrId);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request__c());
        sfRequestDetail myController = new sfRequestDetail(testController);
        
        System.assertEquals(1, myController.danTotalHour);
        System.assertEquals(DateTime.Now().addMinutes(60).format(), myController.danLastWorked);
        System.assertEquals(1, myController.alTotalHour);
        System.assertEquals(DateTime.Now().addMinutes(60).format(), myController.alLastWorked);
        System.assertEquals(1, myController.taraTotalHour);
        System.assertEquals(DateTime.Now().addMinutes(60).format(), myController.taraLastWorked);
        System.assertEquals(1, myController.markTotalHour);
        System.assertEquals(DateTime.Now().addMinutes(60).format(), myController.markLastWorked);
        System.assertEquals(1, myController.macTotalHour);
        System.assertEquals(DateTime.Now().addMinutes(60).format(), myController.macLastWorked);
        System.assertEquals(1, myController.priyankaTotalHour);
        System.assertEquals(DateTime.Now().addMinutes(60).format() , myController.priyankaLastWorked);
        
        
    }
    
    static testmethod void deleteTask(){
        
       Salesforce_Request__c newRequest = new Salesforce_Request__c();
        insert newRequest;
        
        Salesforce_Request_Task__c t = new Salesforce_Request_Task__c(Start_Date__c = DateTime.Now(), End_Date__c = DateTime.Now().addMinutes(60), Work_Request__c = newRequest.id);
        insert t;
        
        ApexPages.currentPage().getParameters().put('Id', newRequest.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request__c());
        sfRequestDetail myController = new  sfRequestDetail(testController);
        
        ApexPages.currentPage().getParameters().put('taskId', t.Id);
        System.assertEquals(1, myController.ReqTask.size());
        
        myController.deleteTask(); 
        myController = new  sfRequestDetail(testController);
        System.assertEquals(0, myController.ReqTask.size());
        
    } 
    
    static testmethod void deleteNote(){
        
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        insert newRequest;
        
        Salesforce_Request_Note__c n = new Salesforce_Request_Note__c(Salesforce_Update__c = newRequest.id);
        insert n;
        
        ApexPages.currentPage().getParameters().put('Id', newRequest.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request__c());
        sfRequestDetail myController = new  sfRequestDetail(testController);
        
        ApexPages.currentPage().getParameters().put('noteId', n.Id);
        System.assertEquals(1, myController.ReqNote.size());
        
        myController.deleteNote(); 
        myController = new  sfRequestDetail(testController);
        System.assertEquals(0, myController.ReqNote.size());
        
    } 
    
    
    static testmethod void deleteAttachment(){
        
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        insert newRequest;
        
        Attachment a = new Attachment(ParentID = newRequest.id);
        a.Name = 'Picture';
        String myString = 'StringToBlob';
        Blob myBlob = Blob.valueof(myString);
        a.Body = myBlob;
        insert a;
        
        ApexPages.currentPage().getParameters().put('Id', newRequest.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request__c());
        sfRequestDetail myController = new  sfRequestDetail(testController);
        
        ApexPages.currentPage().getParameters().put('attachmentId', a.Id);
        System.assertEquals(1, myController.Attachment.size());
        
        myController.deleteAttachment(); 
        myController = new  sfRequestDetail(testController);
        System.assertEquals(0, myController.Attachment.size());
        
    } 
    
    
    static testMethod void mergeRedirect(){
        
        
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        newRequest.request_type__c = 'Merge';
        insert newRequest;
        
        ApexPages.currentPage().getParameters().put('Id', newRequest.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request__c());
        sfRequestDetail myController = new  sfRequestDetail(testController);
        
        myController.mergeRedirect();
        
        
    }
    
    
    
}