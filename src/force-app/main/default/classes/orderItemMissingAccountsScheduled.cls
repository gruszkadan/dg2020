public class orderItemMissingAccountsScheduled implements Schedulable{   
    public void execute(SchedulableContext SC) {
        Order_Item__c[] orderItems = [SELECT id, Account__c, AccountId__c FROM Order_Item__c WHERE Account__c = null and AccountId__c != null];
        if(orderItems.size() > 0){
            Order_Item__c[] orderItemsToUpdate = new list<Order_Item__c>();
            Account[] accounts = new list<Account>();
            set<String> adminIds = new set<String>();
            for(Order_Item__c oi:orderItems){
                if(oi.AccountID__c != null){
                    adminIds.add(oi.AccountID__c);
                }
            }
            if(adminIds.size() > 0){
                accounts = [SELECT id, AdminId__c FROM Account WHERE AdminId__c in: adminIds];
            }
            if(accounts.size() > 0){
                for(Account a:accounts){
                    for(Order_Item__c oi:orderItems){
                        if(oi.AccountID__c == a.AdminID__c){
                            oi.Account__c = a.id;
                            orderItemsToUpdate.add(oi);
                        }
                    }
                }
            }
            if(orderItems.size() != orderItemsToUpdate.size()){
                salesforceLog.createLog('Order Items', true, 'orderItemMissingAccountsScheduled', 'execute', 'No matching accounts found for '+integer.valueof(orderItems.size() - orderItemsToUpdate.size())+' Order Item(s)');
            }
            if(orderItemsToUpdate.size() > 0){
                try {
                    update orderItemsToUpdate;
                } catch(exception e) {
                    salesforceLog.createLog('Order Items', true, 'orderItemMissingAccountsScheduled', 'execute', string.valueOf(e));
                }
            }
        }
    }
}