@isTest(seeAllData=true)
public class testCsSpamQueuePurgeScheduled {
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void test1() {
        //test account
        Account newAcct = new Account(Name = 'Test');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //test case
        Case newCase = new Case();
        newCase.AccountId = newAcct.id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Type = 'CC Support';
        newCase.Subject = 'Test';
        newCase.Origin = 'Email';
        newCase.OwnerID =  [SELECT id from Group where Name = 'Spam Customer Support' Limit 1].id;  
        insert newCase;
        system.debug('OwnerName is '+ newCase.Owner.Name);
        system.debug('OwnerId is '+ newCase.OwnerId);
        
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new csSpamQueuePurgeScheduled());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
    }
}