trigger VPMMilestoneTrigger on VPM_Milestone__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
 if (trigger.isBefore) {
        
        // BEFORE INSERT
        if (trigger.isInsert) {
            VPMMilestoneTriggerHandler.beforeInsert(trigger.new);
        }
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            VPMMilestoneTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
        
        // BEFORE DELETE
        if (trigger.isDelete) {
            VPMMilestoneTriggerHandler.beforeDelete(trigger.old, trigger.new);
        }
        
    }
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            VPMMilestoneTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            VPMMilestoneTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
        // AFTER DELETE
        if (trigger.isDelete) {
            VPMMilestoneTriggerHandler.afterDelete(trigger.old);
        }
        
    }
}