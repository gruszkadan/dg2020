/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Case_IssueTest
{
    private static testmethod void testTrigger()
    {
        // Force the dlrs_Case_IssueTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Case_Issue__c());
    }
}