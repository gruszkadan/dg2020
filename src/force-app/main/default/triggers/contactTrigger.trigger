trigger contactTrigger on Contact  
(before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            contactTriggerHandler.beforeInsert(trigger.new);
        }
        if (trigger.isUpdate) {
            contactTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
    }
    
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            contactTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        if (trigger.isUpdate) {
            contactTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        if (trigger.isDelete) {
            contactTriggerHandler.afterDelete(trigger.old, trigger.new);
        }
    }
    dlrs.RollupService.triggerHandler();
}