@isTest(SeeAllData=true)
private class testOpportunityController {
    
    static testMethod void opportunityController_test1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.NAICS_Code__c = 'Test';
        newAccount.Active_Compliance_Services_Products__c ='eBinder Valet';
        newAccount.Active_Products__c ='HQ Account';
        newAccount.Active_Additional_Compliance_Solutions__c ='Environmental Toolkit;MSDS Authoring';
        insert newAccount;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Closed/Won';
        newOpportunity.Opportunity_Type__c = 'Renewal';
        insert newOpportunity;
        
        product2 p = new product2();
        p.Needs_Expected_Delivery_Date__c = true;
        p.Name = 'Deployed Consulting';
        
        insert p;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Opportunity__c = newOpportunity.Id;
        newQuote.Contract_Length__c = '3 Years';
        insert newQuote;
        
        // Lookup an existing item
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = '01t80000002NON4';
        myItem.PricebookEntryId__c ='01u80000006eap9';
        myItem.Quantity__c = 1;
        myItem.Quote__c = newQuote.Id;
        myItem.Year_1__c = TRUE;
        myItem.Year_2__c = TRUE;
        myItem.Year_3__c = TRUE;
        insert myItem;
        
        Contract__c c = new Contract__c();
        c.Account__c = newAccount.id;
        c.Opportunity__c = newOpportunity.id;
        c.Order_Submitted_By__c = [SELECT id FROM User WHERE Alias = 'rwern' LIMIT 1].id;
        insert c;
        
        Contact nc = new Contact();
        nc.FirstName = 'Bob';
        nc.LastName = 'Weir';
        nc.AccountId = newaccount.id;
        
        insert nc;
        
        Account_Management_Event__c ame = new Account_Management_Event__c();
        ame.Contact__c = nc.id;
        ame.Account__c = newaccount.id;
        
        insert ame;
        
        
        
        Opportunity o = new Opportunity(); 
        o.Account_Management_Event__c = ame.id;
        o.AccountId = newaccount.id;
        o.Name = 'Testing Cancelled Products';
        o.CloseDate = date.today()+2;
        o.StageName = 'New';
        
        Insert o;
        
        opportunitylineitem oli = new opportunitylineitem();
        oli.OpportunityId = newOpportunity.id;
        oli.ProductID__c = p.id;    
        oli.Quantity = 1;
        oli.TotalPrice = 1000;
        oli.PricebookEntryId = '01u80000006eap9';
        insert oli;
        
        
        Order_Item__c oi = new Order_Item__c();
        oi.Cancelled_On_AME__c = o.Account_Management_Event__c;
        oi.Year__c = '2019';
        oi.Account__c = newaccount.id;
        oi.Status__c = 'Active';
        oi.Account_Management_Event__c = ame.id;
        
        
        insert oi;
        
        
        ApexPages.currentPage().getParameters().put('id', newOpportunity.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Opportunity());
        opportunityController myController = new opportunityController(testController);
        myController.isEHS = false;
        myController.qID = newQuote.Id;
        myController.oppty.Id = newOpportunity.Id;
        myController.oppty.AccountId = newOpportunity.AccountID;
        
        myController.findContracts();
        
        myController.oppty.Opportunity_Type__c = 'Renewal';
        myController.oppty.StageName = 'Closed/Won';
        myController.save();
        System.assert(myController.canSave == true);
        
        myController.createQuoteWrappers();
        myController.refreshOppty();
        myController.upsertPrimaryAddress();
        myController.findContracts();
        myController.specialIndexingPriceUpdate();
        myController.contractTermsNeeded();
        myController.createAutomatedContract();
        myController.createManualContract();
        myController.newQuote();
        myController.changeStageName();
        myController.renewalOppClosedWon();
        
        //Set Opportunity AME to cover if statment and query in controller 
        mycontroller.oppty.Account_Management_Event__c = ame.id;
        
        // call method after field was set to complete coverage of test class 
        mycontroller.CanceledProducts();
        
        
    }
    static testMethod void UpdateExpectedDeliveryDate() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.NAICS_Code__c = 'Test';
        newAccount.Active_Compliance_Services_Products__c ='eBinder Valet';
        newAccount.Active_Products__c ='HQ Account';
        newAccount.Active_Additional_Compliance_Solutions__c ='Environmental Toolkit;MSDS Authoring';
        insert newAccount;
        
        Opportunity o = new Opportunity();
        o.AccountID = newAccount.Id;
        o.Name = 'Test';
        o.CloseDate = system.today(); 
        o.StageName = 'System Demo';
        insert o;
        
        
        product2 hq = [select id, name, PricebookEntryId__c from product2 where Name =: 'HQ Account'];
        
        hq.Needs_Expected_Delivery_Date__c = true;
        
        update hq;
        
        Contact nc = new Contact();
        nc.FirstName = 'Bob';
        nc.LastName = 'Weir';
        nc.AccountId = newaccount.id;
        
        insert nc; 
        
        
        opportunitylineitem oli = new opportunitylineitem();
        oli.OpportunityId = o.id;
        oli.ProductID__c = hq.id;    
        oli.Quantity = 1;
        oli.TotalPrice = 1000;
        oli.PricebookEntryId = hq.PricebookEntryId__c;
        insert oli;
        
        
        
        ApexPages.currentPage().getParameters().put('id', o.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(o);
        opportunityController myController = new opportunityController(testController);
        myController.oppty.Id = o.Id;
        myController.oppty.AccountId = o.AccountID;
        
    }
    
    static testMethod void ClosedOppSurveyWon() {
        //Create Data
        Account newAccount = new Account();
        newAccount.Name = 'Travis Matthews';
        newAccount.NAICS_Code__c = 'Test';
        insert newAccount;
        
        Contact nc = new Contact();
        nc.FirstName = 'Matt';
        nc.LastName = 'Naggy';
        nc.AccountId = newaccount.id;
        insert nc;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Close Opp Won';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'System Demo';
        newOpportunity.Opportunity_Type__c = 'New';
        insert newOpportunity;
        
        opportunitylineitem[] olisToInsert = new list<opportunitylineitem>();
        
        
        opportunitylineitem oliEHS = new opportunitylineitem();
        oliEHS.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and product2.name = 'Audit & Inspection' limit 1].id;
        oliEHS.OpportunityId = newOpportunity.id;
        oliEHS.Product2ID = [select id, name from product2 where name = 'Audit & Inspection' limit 1].id;    
        oliEHS.Quantity = 1;
        oliehs.Product_Suite__c = 'EHS Management';
        oliEHS.TotalPrice = 1000;
        olisToInsert.add(oliEHS);
        
        opportunitylineitem oliAUTH = new opportunitylineitem();
        oliAUTH.OpportunityId = newOpportunity.id;
        oliAUTH.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'Authoring - Alias SDS Creation' limit 1].id;
        oliAUTH.Product2Id = [select id, name from product2 where name = 'Authoring - Alias SDS Creation' limit 1].id;    
        oliAUTH.Quantity = 1;
        oliauth.Product_Suite__c = 'MSDS Authoring';
        oliAUTH.TotalPrice = 2500;
        olisToInsert.add(oliAUTH);
        
        opportunitylineitem oliERGO = new opportunitylineitem();
        oliERGO.OpportunityId = newOpportunity.id;
        oliERGO.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'Deployed Consulting' limit 1].id;
        oliERGO.Product2ID = [select id, name from product2 where name = 'Deployed Consulting' limit 1].id;    
        oliERGO.Quantity = 1;
        oliergo.Product_Suite__c = 'Ergonomics';
        oliERGO.TotalPrice = 2500;
        oliErgo.Expected_Delivery_Date__c = date.today() + 90;
        olisToInsert.add(oliERGO);
        
        opportunitylineitem oliCHEM = new opportunitylineitem();
        oliCHEM.OpportunityId = newOpportunity.id;
        oliCHEM.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'HQ Account' limit 1].id;
        oliCHEM.Product2ID = [select id, name from product2 where name = 'HQ Account' limit 1].id;    
        oliCHEM.Quantity = 1;
        oliCHEM.TotalPrice = 8200;
        olichem.Product_Suite__c = 'MSDS Management';
        
        olisToInsert.add(oliCHEM);
        
        
        opportunitylineitem oliODT = new opportunitylineitem();
        oliODT.OpportunityId = newOpportunity.id;
        oliODT.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'On-Demand Training - Setup/Hosting/Maintenance Fee' limit 1].id;
        oliODT.Product2ID = [select id, name from product2 where name = 'On-Demand Training - Setup/Hosting/Maintenance Fee' limit 1].id;    
        oliODT.Quantity = 1;
        oliODT.TotalPrice = 8200;
        oliodt.Product_Suite__c = 'On-Demand Training';
        olisToInsert.add(oliODT);
        
        
        insert olisToInsert;
        opportunitylineitem[] multisuiteCheck = [Select id,Product_Suite__c from opportunitylineitem where id IN :olisToInsert];
        
        //Pass in controller and opportunity ID
        ApexPages.currentPage().getParameters().put('id', newOpportunity.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(newOpportunity);
        opportunityController myController = new opportunityController(testController);
        
        
        //set result too null and call save method to cover first valadation 
        myController.result = null;
        
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        //set result to won to cover the Won save scenario and hit all valadation 
         mycontroller.CloseOppData();
        myController.result = 'Won';
        
        
        //Fill in all required fields and call save method to allow the page to save succesfully 
        mycontroller.oppty.Existing_System_AUTH__c = '3E';
        mycontroller.oppty.Existing_System_CHEM__c = 'AIC';
        mycontroller.oppty.Existing_System_EHS__c = 'DATS';
        mycontroller.oppty.Existing_System_ERGO__c = 'RCI';
        mycontroller.oppty.Existing_System_ODT__c = 'TRA';
        
        
        mycontroller.oppty.Competition_Encountered_AUTH__c = 'SAP';
        mycontroller.oppty.Competition_Encountered_CHEM__c = 'SAI';
        mycontroller.oppty.Competition_Encountered_EHS__c = 'P2';
        mycontroller.oppty.Competition_Encountered_ERGO__c = 'MSDS School';
        mycontroller.oppty.Competition_Encountered_ODT__c = 'Medgate';
        
        mycontroller.oppty.Future_System_AUTH__c = '3E';
        mycontroller.oppty.Future_System_CHEM__c = 'BLR';
        mycontroller.oppty.Future_System_EHS__c = 'Chemwatch';
        mycontroller.oppty.Future_System_ERGO__c = 'Chemscape';
        mycontroller.oppty.Future_System_ODT__c = 'AON';
        
        myController.CloseOppSave();
        
        // check to see if stage name and close date are set correctly 
        system.assertEquals(date.today(), mycontroller.oppty.closedate);
        system.assertEquals('Closed/Won', mycontroller.oppty.stagename);

 
        
    }
    
    static testMethod void ClosedOppClosed() {
        //Create Data
        Account newAccount = new Account();
        newAccount.Name = 'Travis Matthews';
        newAccount.NAICS_Code__c = 'Test';
        insert newAccount;
        
        Contact nc = new Contact();
        nc.FirstName = 'Matt';
        nc.LastName = 'Naggy';
        nc.AccountId = newaccount.id;
        insert nc;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Close Opp Won';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'System Demo';
        newOpportunity.Opportunity_Type__c = 'New';
        insert newOpportunity;
        
        opportunitylineitem[] olisToInsert = new list<opportunitylineitem>();
        
        
        opportunitylineitem oliEHS = new opportunitylineitem();
        oliEHS.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and product2.name = 'Audit & Inspection' limit 1].id;
        oliEHS.OpportunityId = newOpportunity.id;
        oliEHS.Product2ID = [select id, name from product2 where name = 'Audit & Inspection' limit 1].id;    
        oliEHS.Quantity = 1;
        oliEHS.TotalPrice = 1000;
        oliehs.Product_Suite__c = 'EHS Management';
        olisToInsert.add(oliEHS);
        
        opportunitylineitem oliAUTH = new opportunitylineitem();
        oliAUTH.OpportunityId = newOpportunity.id;
        oliAUTH.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'Authoring - Alias SDS Creation' limit 1].id;
        oliAUTH.Product2Id = [select id, name from product2 where name = 'Authoring - Alias SDS Creation' limit 1].id;    
        oliAUTH.Quantity = 1;
        oliAUTH.TotalPrice = 2500;
        oliauth.Product_Suite__c = 'MSDS Authoring';
        olisToInsert.add(oliAUTH);
        
        opportunitylineitem oliERGO = new opportunitylineitem();
        oliERGO.OpportunityId = newOpportunity.id;
        oliERGO.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'Deployed Consulting' limit 1].id;
        oliERGO.Product2ID = [select id, name from product2 where name = 'Deployed Consulting' limit 1].id;    
        oliERGO.Quantity = 1;
        oliergo.Product_Suite__c = 'Ergonomics';
        oliERGO.TotalPrice = 2500;
        oliErgo.Expected_Delivery_Date__c = date.today() + 90;
        olisToInsert.add(oliERGO);
        
        opportunitylineitem oliCHEM = new opportunitylineitem();
        oliCHEM.OpportunityId = newOpportunity.id;
        oliCHEM.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'HQ Account' limit 1].id;
        oliCHEM.Product2ID = [select id, name from product2 where name = 'HQ Account' limit 1].id;    
        oliCHEM.Quantity = 1;
        oliCHEM.TotalPrice = 8200;
        olichem.Product_Suite__c = 'MSDS Management';
        
        olisToInsert.add(oliCHEM);
        
        
        opportunitylineitem oliODT = new opportunitylineitem();
        oliODT.OpportunityId = newOpportunity.id;
        oliODT.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'On-Demand Training - Setup/Hosting/Maintenance Fee' limit 1].id;
        oliODT.Product2ID = [select id, name from product2 where name = 'On-Demand Training - Setup/Hosting/Maintenance Fee' limit 1].id;    
        oliODT.Quantity = 1;
        oliodt.Product_Suite__c = 'On-Demand Training';
        oliODT.TotalPrice = 8200;
        olisToInsert.add(oliODT);
        
        
        insert olisToInsert;
        opportunitylineitem[] multisuiteCheck = [Select id,Product_Suite__c from opportunitylineitem where id IN :olisToInsert];
        
        //Pass in controller and opportunity ID
        ApexPages.currentPage().getParameters().put('id', newOpportunity.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(newOpportunity);
        opportunityController myController = new opportunityController(testController);
        
        
        //set reult to lost to cover most of the lost save scenario
        myController.result = 'Lost';
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        
        mycontroller.oppty.Lost_Reasons__c = 'Switching cost too high';
        mycontroller.oppty.Lost_Functionality_Reasons__c = null;
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        mycontroller.oppty.Lost_Reasons__c = 'Don’t See ROI';
        mycontroller.oppty.Lost_Functionality_Reasons__c = null;
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        mycontroller.oppty.Lost_Reasons__c = 'Not Ready for Change';
        mycontroller.oppty.Lost_Functionality_Reasons__c = null;
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        mycontroller.oppty.Lost_Reasons__c = 'Project Scope Changed';
        mycontroller.oppty.Lost_Functionality_Reasons__c = null;
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        //set fields to non compeitors to cover section 2 of the modal display method 
        //Fill in all required fields to allow the page to save succesfully 
        mycontroller.oppty.Existing_System_AUTH__c = '3E';
        mycontroller.oppty.Existing_System_CHEM__c = 'AIC';
        mycontroller.oppty.Existing_System_EHS__c = 'DATS';
        mycontroller.oppty.Existing_System_ERGO__c = 'RCI';
        mycontroller.oppty.Existing_System_ODT__c = 'TRA';
        
        
        mycontroller.oppty.Competition_Encountered_AUTH__c = 'SAP';
        mycontroller.oppty.Competition_Encountered_CHEM__c = 'SAI';
        mycontroller.oppty.Competition_Encountered_EHS__c = 'P2';
        mycontroller.oppty.Competition_Encountered_ERGO__c = 'MSDS School';
        mycontroller.oppty.Competition_Encountered_ODT__c = 'Medgate';
        
        mycontroller.oppty.Future_System_AUTH__c = 'VelocityEHS';
        mycontroller.oppty.Future_System_CHEM__c = 'VelocityEHS';
        mycontroller.oppty.Future_System_EHS__c = 'VelocityEHS';
        mycontroller.oppty.Future_System_ERGO__c = 'VelocityEHS';
        mycontroller.oppty.Future_System_ODT__c = 'VelocityEHS';
        
        mycontroller.lostreasons[0].isselected = true;
        mycontroller.lostreasons[0].option = 'Don’t See ROI';
        
        
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.LostReasonValue();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        mycontroller.productSuiteWrappers[0].OLI[0].Lost_Functionality_Reasons__c = 'Asset Managment';
        
        mycontroller.productSuiteWrappers[0].LostFunctionalityReasons[0].option = 'Asset Managment';
        mycontroller.productSuiteWrappers[0].LostFunctionalityReasons[0].isselected = true;
        
        
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.LostReasonValue();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        
        // check to see if stage name, probabillity and close date are set correctly 
        system.assertEquals(date.today(), mycontroller.oppty.closedate);
        system.assertEquals('Closed', mycontroller.oppty.stagename);
        system.assertEquals(0, mycontroller.oppty.Probability);
        
        
    }
    
    
    static testMethod void ClosedOppClosedLost() {
        //Create Data
        Account newAccount = new Account();
        newAccount.Name = 'Travis Matthews';
        newAccount.NAICS_Code__c = 'Test';
        insert newAccount;
        
        Contact nc = new Contact();
        nc.FirstName = 'Matt';
        nc.LastName = 'Naggy';
        nc.AccountId = newaccount.id;
        insert nc;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Close Opp Won';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'System Demo';
        newOpportunity.Opportunity_Type__c = 'New';
        insert newOpportunity;
        
        opportunitylineitem[] olisToInsert = new list<opportunitylineitem>();
        
        
        opportunitylineitem oliEHS = new opportunitylineitem();
        oliEHS.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and product2.name = 'Audit & Inspection' limit 1].id;
        oliEHS.OpportunityId = newOpportunity.id;
        oliEHS.Product2ID = [select id, name from product2 where name = 'Audit & Inspection' limit 1].id;    
        oliEHS.Quantity = 1;
        oliehs.Product_Suite__c = 'EHS Management';
        oliEHS.TotalPrice = 1000;
        olisToInsert.add(oliEHS);
        
        opportunitylineitem oliAUTH = new opportunitylineitem();
        oliAUTH.OpportunityId = newOpportunity.id;
        oliAUTH.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'Authoring - Alias SDS Creation' limit 1].id;
        oliAUTH.Product2Id = [select id, name from product2 where name = 'Authoring - Alias SDS Creation' limit 1].id;    
        oliAUTH.Quantity = 1;
        oliauth.Product_Suite__c = 'MSDS Authoring';
        oliAUTH.TotalPrice = 2500;
        olisToInsert.add(oliAUTH);
        
        opportunitylineitem oliERGO = new opportunitylineitem();
        oliERGO.OpportunityId = newOpportunity.id;
        oliERGO.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'Deployed Consulting' limit 1].id;
        oliERGO.Product2ID = [select id, name from product2 where name = 'Deployed Consulting' limit 1].id;    
        oliERGO.Quantity = 1;
        oliERGO.TotalPrice = 2500;
        oliergo.Product_Suite__c = 'Ergonomics';
        oliErgo.Expected_Delivery_Date__c = date.today() + 90;
        olisToInsert.add(oliERGO);
        
        opportunitylineitem oliCHEM = new opportunitylineitem();
        oliCHEM.OpportunityId = newOpportunity.id;
        oliCHEM.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'HQ Account' limit 1].id;
        oliCHEM.Product2ID = [select id, name from product2 where name = 'HQ Account' limit 1].id;    
        oliCHEM.Quantity = 1;
        oliCHEM.TotalPrice = 8200;
        olichem.Product_Suite__c = 'MSDS Management';
        
        olisToInsert.add(oliCHEM);
        
        
        opportunitylineitem oliODT = new opportunitylineitem();
        oliODT.OpportunityId = newOpportunity.id;
        oliODT.PricebookEntryId = [select id, name from PricebookEntry where  pricebook2.name = 'Standard Price Book' and  product2.name = 'On-Demand Training - Setup/Hosting/Maintenance Fee' limit 1].id;
        oliODT.Product2ID = [select id, name from product2 where name = 'On-Demand Training - Setup/Hosting/Maintenance Fee' limit 1].id;    
        oliODT.Quantity = 1;
        oliodt.Product_Suite__c = 'On-Demand Training';
        oliODT.TotalPrice = 8200;
        olisToInsert.add(oliODT);
        
        
        insert olisToInsert;
        opportunitylineitem[] multisuiteCheck = [Select id,Product_Suite__c from opportunitylineitem where id IN :olisToInsert];
        
        //Pass in controller and opportunity ID
        ApexPages.currentPage().getParameters().put('id', newOpportunity.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(newOpportunity);
        opportunityController myController = new opportunityController(testController);
        
        
        
        
        
        
        //set reult to lost to cover most of the lost save scenario
        myController.result = 'Lost';
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        
        //Covers valadaition if statment on line 881 of controller 
        mycontroller.oppty.Lost_Reasons__c = 'Budget';
        mycontroller.oppty.Lost_Budget_Reasons__c = null;
        mycontroller.oppty.Lost_Potential_Purchase_Price__c = 43353;
        
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        
        //Covers valadaition if statment on line 878 of controller 
        mycontroller.oppty.Lost_Reasons__c = 'Price';
        mycontroller.oppty.Lost_Potential_Purchase_Price__c = null;
        
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        
        mycontroller.oppty.Lost_Reasons__c = 'Functionality';
        mycontroller.oppty.Lost_Functionality_Reasons__c = null;
        
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        
        //set fields to non compeitors to cover section 2 of the modal display method  
        mycontroller.oppty.Existing_System_AUTH__c = '3E';
        mycontroller.oppty.Existing_System_CHEM__c = 'AIC';
        mycontroller.oppty.Existing_System_EHS__c = 'DATS';
        mycontroller.oppty.Existing_System_ERGO__c = 'RCI';
        mycontroller.oppty.Existing_System_ODT__c = 'TRA';
        
        
        mycontroller.oppty.Competition_Encountered_AUTH__c = 'SAP';
        mycontroller.oppty.Competition_Encountered_CHEM__c = 'SAI';
        mycontroller.oppty.Competition_Encountered_EHS__c = 'P2';
        mycontroller.oppty.Competition_Encountered_ERGO__c = 'MSDS School';
        mycontroller.oppty.Competition_Encountered_ODT__c = 'Medgate';
        
        mycontroller.oppty.Future_System_AUTH__c = 'AIC';
        mycontroller.oppty.Future_System_CHEM__c = 'SAP';
        mycontroller.oppty.Future_System_EHS__c = 'Medgate';
        mycontroller.oppty.Future_System_ERGO__c = 'RCI';
        mycontroller.oppty.Future_System_ODT__c = 'TRA';
        
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        mycontroller.productSuiteWrappers[0].OLI[0].Lost_Functionality_Reasons__c = 'Asset Managment';
        mycontroller.productSuiteWrappers[0].LostFunctionalityReasons[0].isSelected = true;
        
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        
        
        mycontroller.oppty.Lost_Potential_Purchase_Price__c = 51649;
        mycontroller.oppty.Lost_Potential_Purchase_Price_Notes__c = 'We lost this deal because.....';
        mycontroller.oppty.Lost_Budget_Reasons__c = 'Budget Cut';
        
        
        mycontroller.lostreasons[0].isselected = true;
        mycontroller.lostreasons[0].option = 'Functionality';
        
        mycontroller.lostreasons[1].option = 'Price';
        mycontroller.lostreasons[1].isselected = true;
        
        mycontroller.FindModalDisplay();
        mycontroller.ProductSuite();
        mycontroller.LostReasonValue();
        
        mycontroller.CloseOppData();
        myController.CloseOppSave();
        myController.xDelete();
        
        // check to see if stage name, probabillity and close date are set correctly 
        system.assertEquals(date.today(), mycontroller.oppty.closedate);
        system.assertEquals('Closed Lost', mycontroller.oppty.stagename);
        system.assertEquals(0, mycontroller.oppty.Probability);
        
        
    }
    
    
}