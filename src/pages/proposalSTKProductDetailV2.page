<apex:page id="proposalHQProductDetail" showHeader="FALSE" sidebar="FALSE" renderAS="PDF" standardController="Quote__c" standardStylesheets="false">
<apex:stylesheet value="{!URLFOR($Resource.proposalPrintResource, 'default.css')}"/>
<body style="page-break-after:always;" id="stk" class="details">
    <div id="page-wrap">
        <div id="page-header">
            <h1 class="left">Product Description</h1>
             <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/logo-msdsonline.png')}" styleClass="msdsonlineLogo right" width="234" height="52" />
        </div>
<div id="page-content">
            <div class="col one left">
                <div class="header">
                    <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/persona-stk.png')}" styleClass="left"/>
                    <p class="quote">
                    “The Safety Tool Kit
                    from MSDSonline
                    gives me a
                    comprehensive
                    solution for staying
                    up-to-date
                    on OSHA regulations.”
                    </p>
                </div>
                <div class="ribbon">
                        <div class="ribbon-left green"></div>
                        <h2 class="green">Benefits</h2>
                        <div class="ribbon-right green"></div>
                    </div>
                <div class="content">
                    <ul class="benefits">
                        <li>
                            <span>
                                Make Training Easier
                            </span><br />
                                The Safety Tool Kit, from MSDSonline®,
                                has everything you need including
                                downloadable and customizable training
                                presentations, checklists, meeting
                                schedules, quizzes and handouts.
                        </li>
                        <li>
                            <span>
                                Reduce Costs
                            </span><br />
                                Minimize workers’ compensation
                                claims through training and effective
                                accident investigation, and lower
                                your legal fees by handling routine
                                compliance questions yourself.
                        </li>
                        <li>
                            <span>
                                Work Smarter
                            </span><br />
                                Find best practices from other
                                companies as well as plain-English
                                regulatory analyses, all from one
                                convenient source on the Web.
                        </li>
                    </ul>
                </div>
                <div class="ribbon">
                    <div class="ribbon-left green"></div>
                    <h2 class="green">Contact Us Now</h2>
                    <div class="ribbon-right green"></div>
                </div>
                <div class="content">
                    <ul class="contact-info">
                        <li>
                        <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/monitor.png')}"/>
                            www.MSDSonline.com
                        </li>
                        <li>
                        <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/phone.png')}"/>
                            Toll Free 1.888.362.2007
                        </li>
                        <li>
                            <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/envelope.png')}"/>
                            sales@ehs.com
                        </li>
                        <li>
                        <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/blog.png')}"/>
                            www.MSDSonline.com/blog
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col two left">
                <div class="header">
                    <h2>
                        Safety Tool Kit
                    </h2>
                    <h3>
                        Comprehensive Safety Resources at Your Fingertips
                    </h3>
                </div>
                <div class="ribbon">
                    <div class="ribbon-left green"></div>
                    <h2 class="green">Overview</h2>
                    <div class="ribbon-right green"></div>
                </div>
                <div class="content">
                    <p>
                        Environmental, health and safety (EH&amp;S) professionals are continuously
                    asked to do more with fewer resources. The demands for staying compliant
                    are getting more difficult no matter what industry you’re in or how big your
                    organization.
                    </p>
                    <p>
                    Managing compliance and training your employees on how to evaluate
                    and avoid risks from hazards is critical to your business.
                    </p>
                    <p>
                    An accident can have serious implications in terms of lost productivity,
                    skyrocketing insurance costs and workers’ compensation claims, not to
                    mention possible litigation costs or fines for non compliance.
                    </p>
                    <p>
                    Safety training is a huge part of OSHA Hazard Communication compliance.
                    Finding the time to develop training materials that pertain to your unique
                    workplace can be costly and time-consuming. Our Safety Tool Kit gives you
                    24/7, on-demand access to hundreds of downloadable and customizable
                    resources:
                    </p>
                    <ul>
                        <li>Hundreds of Training Resources at your Fingertips</li>
                        <li>Compliance Analyses &amp; Advice</li>
                        <li>Checklists, Forms &amp; Guidance Documents</li>
                        <li>Weekly OSHA Regulatory Newsletters</li>
                        <li>Online Access to Safety Compliance Experts</li>
                        <li>And much more!</li>
                    </ul>
                    <p class="bold">
                        Our Safety Tool Kit is ideal for EH&amp;S professionals who want to stay upto-
                        date on global, federal and state-specific environmental regulations and
                        requirements.
                    </p>
                    <p>
                        For pricing information on our Safety Tool Kit, visit www.MSDSonline.com
                        or call us toll free at 1.888.362.2007 (U.S. &amp; Canada Only). Outside the
                        United States, call 1.312.881.2000.
                    </p>
                    <p class="call-to-action">
                        Call Today For A Free Quote<br /> <span>1.888.362.2007</span>
                    </p>
                </div>
            </div>
        </div>
        </div>
</body>
</apex:page>