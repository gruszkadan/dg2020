public with sharing class taskCreateController {
    private Apexpages.StandardController controller; 
    public Task t {get; set;}
    public Account theAccount {get;set;}
    public Case theCase {get;set;}
    public ID what {get; set;}
    public ID user {get; set;}
    public ID who {get; set;}
    public String cType {get; set;}
    public String rURL {get; set;}
    public String close {get; set;}
    public String callResult {get; set;}
    public String editPage {get; set;}
    public String subject {get; set;}
    public User u {get; set;}
    public Contact contactInfo {get; set;}
    public Lead leadInfo {get; set;}
    public String Email {get; set;}
    public String Phone {get; set;}
    public String e {get; set;}
    public String p {get; set;}
    public String activityDate {get; set;}
    public String whatType {get;set;}
    public String whoType {get;set;}
    public static String acctPrefix = Account.sObjectType.getDescribe().getKeyPrefix();
    public static String opptyPrefix = Opportunity.sObjectType.getDescribe().getKeyPrefix();
    public static String leadPrefix = Lead.sObjectType.getDescribe().getKeyPrefix();
    public static String casePrefix = Case.sObjectType.getDescribe().getKeyPrefix();
    public Contact [] theContacts {get;set;}
    public boolean isLogACall {get;set;}
    public string AccountID {get;set;}
    boolean hasError;
    
    public taskCreateController(ApexPages.StandardController stdController) {
        u= [Select UserPreferencesTaskRemindersCheckboxDefault, Name, Group__c, Suite_Of_Interest__c, Department__c From User where id =: UserInfo.getUserId() LIMIT 1];
        this.controller = stdController;
        String retURL = ApexPages.currentPage().getParameters().get('retURL');
        Id whatID = ApexPages.currentPage().getParameters().get('whatid');
        String callType = ApexPages.currentPage().getParameters().get('type');
        if(whatID != null){
            AggregateResult[] completedDemo = new list <AggregateResult>();
            completedDemo = [Select MAX(CreatedDate) maxCDate
                             From Event 
                             where Suite_of_Interest__c =: u.Suite_Of_Interest__c 
                             AND Event_Status__c = 'Completed'
                             AND AccountId =: whatID
                            ];
            if((datetime)completedDemo[0].get('maxCDate') != null){
                DateTime cDate = (datetime)completedDemo[0].get('maxCDate');
                Opportunity[] openOpp = new list <Opportunity>();
                openOpp = [Select id, CreatedDate, Suite_Of_Interest__c 
                           From Opportunity 
                           where Suite_of_Interest__c =: u.Suite_Of_Interest__c 
                           AND isClosed = FALSE
                           AND AccountId =: whatID
                           AND CreatedDate >=: cDate
                           AND CreatedDate <=: cDate.addMinutes(5)
                           LIMIT 1];
                if(openOpp.size() > 0){
                    callType = 'Follow-Up';
                }
            }
        }
        Id userID = ApexPages.currentPage().getParameters().get('user');
        String e = ApexPages.currentPage().getParameters().get('e');
        String p = ApexPages.currentPage().getParameters().get('p');
        String subject = ApexPages.currentPage().getParameters().get('tsk5');
        String whoID = ApexPages.currentPage().getParameters().get('whoID');
        String status = ApexPages.currentPage().getParameters().get('tsk12');
        if(whatID != NULL){
            if(String.valueOf(whatID).startsWith(acctPrefix)){
                whatType = 'Account';
                accountID = whatId;
            }
            if(String.valueOf(whatID).startsWith(opptyPrefix)){
                whatType = 'Opportunity';
            }  
            if(String.valueOf(whatID).startsWith(casePrefix)){
                whatType = 'Case';
            } 
        }
        if(whatType =='Case'){
            theCase = [Select ID, AccountID, ContactID from Case where ID=:whatID];
        }
        if(whoID != NULL){
            if(String.valueOf(whoID).startsWith(leadPrefix)){
                whoType = 'Lead';
            }
        } 
        what = whatID;
        user = userID;
        cType = callType;
        email = e;
        phone = p;
        who = whoID;
        Task t = (Task)controller.getRecord();
        t.WhatId = whatID;
        t.Type__c = callType;
        t.OwnerId = userID;
        t.Subject = subject;
        t.WhoID = whoID;
        t.Status = status;
        if (status == 'Completed') {
            t.ActivityDate = date.today();
            isLogACall = true;
        }
        
        if(u.UserPreferencesTaskRemindersCheckboxDefault == true){
            t.IsReminderSet = true;
        } else {
            t.IsReminderSet = false;
        }
        rURL = retURL;
        if(whatID != null && whatType=='Account'){
            theContacts = [select ID from Contact where AccountID =:whatID];
            checkAccount();
        }
    }        
    
    public PageReference updateTime() {
        Task t = (Task)controller.getRecord();
        if(t.ActivityDate != NULL){
            date myDate = t.ActivityDate;
            Time myTime = Time.newInstance(08, 00, 00, 00);
            DateTime D = DateTime.newInstance(myDate,myTime);
            t.ReminderDateTime = D;
            if(u.UserPreferencesTaskRemindersCheckboxDefault == true){
                t.IsReminderSet = true;
            } else {
                t.IsReminderSet = false;
            }
        } else {
            t.ReminderDateTime = NULL;
            t.IsReminderSet = false;
        }
        return null;
    }
    
    public List<SelectOption> getContactSelectList() {        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--Select--',false));        
        if(whoType != 'Lead' && whatType != 'Case'  ){
            List<Contact> contacts;
            if(whatType != 'Opportunity'){
                contacts = [SELECT Id, Name FROM Contact where AccountID=:What Order By Name ASC limit 500];
            }else{
                id opportunityAccountId = [Select AccountId from Opportunity where id =:what LIMIT 1].AccountId;
                contacts = [SELECT Id, Name FROM Contact where AccountID=:opportunityAccountId Order By Name ASC limit 500];
            }
            for (Contact contact : contacts) {
                options.add(new SelectOption(contact.ID, contact.Name));
            }
        }
        if(whatType =='Case'){
            List<Contact> contacts = [SELECT Id, Name FROM Contact where AccountID=:theCase.AccountID Order By Name ASC limit 500];
            for (Contact contact : contacts) {
                options.add(new SelectOption(contact.ID, contact.Name));
            }
        } 
        if(whoType == 'Lead'){
            List<Lead> leads = [SELECT Id, Name FROM Lead where ID=:Who Order By Name ASC];
            for (Lead lead : leads) {
                options.add(new SelectOption(lead.ID, lead.Name));
            }
        } 
        return options;
        
    }
    
    public void checkAccount (){
        theAccount = [SELECT id, Date_Connected_with_DM__c, Rating, Account_Rating_Reason__c, Ergonomics_Rating__c, Ergonomics_Rating_Reason__c, Online_Training_Rating__c, Online_Training_Rating_Reason__c, 
                      Authoring_Rating__c, Authoring_Rating_Reason__c, EHS_Rating__c, EHS_Rating_Reason__c, Num_of_SDS__c, Num_of_Employees__c, Current_Ergonomics_Solution__c, Ergonomics_Not_a_Target_Reason__c, Ergonomics_Not_Interested_Reason__c,
                      Current_Training_Method__c, Training_Not_a_Target_Reason__c, Training_Not_Interested_Reason__c
                      FROM Account 
                      WHERE id = : accountID LIMIT 1];
    }
    
    public void validation() {
        hasError = false;
        if (accountID != null) {
                Task t = (Task)controller.getRecord();
            if(test.isRunningTest()){
                t.Call_Result__c = 'Confirm Demo';
            }
            if (t.WhoId == null & theContacts.size() > 0) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a contact'));
                hasError = true;
            }
               if (t.Status == 'Completed' && u.Department__c == 'Sales' && t.Call_Result__c == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a call result'));
                hasError = true;
            }
            if (!hasError) {
                try {
                    update theAccount;
                } catch (System.DMLexception e) { 
                    ApexPages.addMessages(e);
                    hasError = true;
                }
            }
        }
    }
    
    public PageReference save() {
        validation();
        if (!hasError) {
            this.controller.save();
            if(rURL != null){
                PageReference pageRef = new PageReference(rURL);
                return pageRef;
            } else {
                PageReference pageRef = new PageReference('/'+ what);
                return pageRef;
            }
        } else {
            return null;
        }
    }   
    
    public PageReference saveNewEvent() {
        validation();
        if (!hasError) {
            this.controller.save();
            PageReference newEvent = new PageReference('/00U/e?');
            newEvent.setRedirect(true);
            if(what != NULL){
                newEvent.getParameters().put('what_id',what); 
            } 
            if(who != NULL){
                newEvent.getParameters().put('who_id', who);    
            }
            newEvent.getParameters().put('user',user);      
            return newEvent;  
        } else {
            return null;
        }
    }
    
    public PageReference saveNew() {
        validation();
        if (!hasError) {
            this.controller.save(); 
            if(what != NULL){
                checkAccount();
            }
            PageReference task_new = Page.task_new;
            task_new.setRedirect(true);
            if(what != NULL){
                task_new.getParameters().put('whatid',what); 
            } 
            if(who != NULL){
                task_new.getParameters().put('whoID', who); 
                task_new.getParameters().put('e', email);
                task_new.getParameters().put('p', phone);   
            }
            task_new.getParameters().put('tsk5',subject);
            task_new.getParameters().put('user',user);
            task_new.getParameters().put('retURL',rURL); 
            if(what != NULL && theAccount.Date_Connected_with_DM__c != NULL){
                task_new.getParameters().put('type','Follow-Up');
            } else {
                task_new.getParameters().put('type','Initial');
            }
            
            return task_new;  
        } else {
            return null;
        }
    }
}