public class userManagement {
    
    public static void checkDeactivation(User[] users) {
        
        //query the default users from the lead custom settings class
        leadCustomSettings lcd = new leadCustomSettings();
        leadCustomSettings.defaultOwners def = lcd.findDefaultOwners();
        
        set<id> userIds = new set<id>();
        
        //loop through our users
        for (User u : users) {
            //if the user is a default owner, do not deactivate
            if (def.allIds.contains(u.id)) {
                if (!test.isRunningTest()) {
                    u.Cannot_Deactivate_User__c = true;
                }
            } else {
                userIds.add(u.id);
            }             
        }
        
        if (userIds.size() > 0) {
            string allIds = '';
            Round_Robin_Assignment__c[] rrs = [SELECT id, UserID__c FROM Round_Robin_Assignment__c];
            for (Round_Robin_Assignment__c r : rrs) {
                allIds += r.UserID__c+';';
            }
            
            Territory__c[] terrs = new list<Territory__c>();
            terrs.addAll([SELECT Id, OwnerId, Territory_Owner__c, Online_Training_Owner__c, Authoring_Owner__c, Ergonomics_Owner__c, EHS_Owner__c, SLED_Owner__c
                          FROM Territory__c
                          WHERE OwnerId IN : userIds
                          OR Territory_Owner__c IN : userIds
                          OR Online_Training_Owner__c IN : userIds
                          OR Authoring_Owner__c IN : userIds
                          OR Ergonomics_Owner__c IN : userIds
                          OR SLED_Owner__c IN : userIds
                          OR EHS_Owner__c IN : userIds]);
            
            if (terrs.size() > 0) {
                for (Territory__c t : terrs) {
                    allIds += t.OwnerId+';'+t.Territory_Owner__c+';'+t.Online_Training_Owner__c+';'+t.Authoring_Owner__c+';'+t.Ergonomics_Owner__c+';'+t.EHS_Owner__c+';'+t.SLED_Owner__c+';';
                }
            }
            
            for (User u : users) {
                boolean cannotDeactivate = false;
                if (allIds.contains(u.id)) {
                    u.Cannot_Deactivate_User__c = true;
                }
            }
            
            
        }        
        
    }
    
    // when a RR user is deactivated/activated, re-order the other users
    public static void enterpriseLeadRRManagement() {
        Round_Robin_Assignment__c[] newAssignments = new list<Round_Robin_Assignment__c>();
        Round_Robin_Assignment__c[] oldAssignments = [SELECT id, Name, UserID__c, Order__c, Used_Last__c, Type__c
                                                      FROM Round_Robin_Assignment__c
                                                      WHERE Type__c = 'Enterprise'
                                                      ORDER BY Order__c ASC ]; 
        User[] users = [SELECT id, FirstName, LastName, isActive,Suite_Of_Interest__c
                        FROM User
                        WHERE EntLeads_RR__c = true AND Suite_Of_Interest__c != 'Ergonomics'
                        AND isActive = true];
        integer ient = 1;
        //integer ierg = 1;
        
        string addedIds = '';
        for (Round_Robin_Assignment__c r : oldAssignments) {
            for (User u : users) {
                if (u.id == r.UserID__c) {
                    Round_Robin_Assignment__c rra = new Round_Robin_Assignment__c();
                    rra.Name = u.FirstName+' '+u.LastName;
                    //if(r.Type__c == 'Ergonomics'){
                    //    rra.Order__c = ierg++;                        
                    //}else{
                    rra.Order__c = ient++;
                    //}
                    rra.UserID__c = u.id;
                    rra.Used_Last__c = r.Used_Last__c;
                    rra.Type__c = r.Type__c;
                    addedIds += u.id+';';
                    newAssignments.add(rra);
                }
            }
        }
        
        //integer i2 = newAssignments.size()+1;
        for (User u : users) {
            if (!addedIds.contains(u.id)) {
                Round_Robin_Assignment__c rra = new Round_Robin_Assignment__c();
                rra.Name = u.FirstName+' '+u.LastName;
                //rra.Order__c = i2++;
                rra.UserID__c = u.id;
                //if(u.Suite_Of_Interest__c == 'Ergonomics'){
                //    rra.Type__c = 'Ergonomics';
                //    rra.Order__c = ierg++;
                //}else{
                    rra.Type__c = 'Enterprise';
                    rra.Order__c = ient++;
                //}
                addedIds += u.id+';';
                newAssignments.add(rra);
            }
        }
        
        try{
            delete oldAssignments;
            insert newAssignments;
        }catch(exception e){
            salesforceLog.createLog('Users', true, 'userManagement', 'enterpriseLeadRRManagement', string.valueOf(e));
        }
    }
}