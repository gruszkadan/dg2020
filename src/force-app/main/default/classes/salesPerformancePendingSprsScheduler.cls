global class salesPerformancePendingSprsScheduler Implements Schedulable {
    //boolean executeEmail;
    
    global void execute(SchedulableContext sc)
    {
        
        integer businessDay = currentBusinessDay();
        if(businessDay < 7 || Test.isRunningTest()){
            sendmail();
        }
    }
    
    public integer currentBusinessDay(){
        date monthFirstDay = Date.Today().toStartofMonth();
        Holiday[] holidays = [SELECT ActivityDate FROM Holiday];
        Set<Date> holidaysSet = new Set<Date> ();
        for (Holiday currHoliday : holidays)
        {
            holidaysSet.add(currHoliday.ActivityDate);
        }
        Integer workingDays = 0;
        for (integer i = 0; i <= monthFirstDay.daysBetween(Date.Today()); i++)
        {
            Date dt = monthFirstDay + i;
            DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());
            String todayDay = currDate.format('EEEE');
            if (todayDay != 'Saturday' && todayDay != 'Sunday' && (!holidaysSet.contains(dt)))
            {
                workingDays = workingDays + 1;
            }
        }
        return workingdays;
    }
    
    public void sendmail() {
        
        Datetime dt = Datetime.now();
        DateTime lastMonthDT = dt.addMonths(-1);
        string financeMonth = lastMonthDT.format('MMMM');
        string financeYear = lastMonthDT.format('YYYY');
        
        Sales_Performance_Request__c[] pendingSpr = new List<Sales_Performance_Request__c>();
        pendingSpr = [SELECT Current_Approver__c,Name,Type__c, Owner.Name, Current_Approval_Step__c, Month_Incentive_Applied__c, Year_Incentive_Applied__c,
                      Assigned_Approver_1__r.Email, Assigned_Approver_2__r.Email, Assigned_Approver_3__r.Email, Assigned_Approver_4__r.Email, Assigned_Approver_5__r.Email, 
                      Assigned_Approver_6__r.Email, Assigned_Approver_7__r.Email, Assigned_Approver_8__r.Email, Assigned_Approver_9__r.Email, Assigned_Approver_10__r.Email
                      FROM Sales_Performance_Request__c WHERE Status__c = 'Pending Approval' AND Month_Incentive_Applied__c = :financeMonth AND Year_Incentive_Applied__c = :financeYear];
        
        if (pendingSpr.size() > 0){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(queryEmails(pendingSpr));
            string[] ccEmails = New List<string>();
            ccEmails.add('salesforceops@ehs.com');
            ccEmails.add( 'hcoppersmith@ehs.com');
            mail.setCcAddresses(ccEmails);
            mail.setSubject('Pending Sales Performance Request Notification');
            mail.sethtmlbody(createEmailBody(pendingSpr));
            Messaging.sendEmail(New Messaging.SingleEmailMessage[]{mail});
        }
    }
    
    public string createEmailBody(Sales_Performance_Request__c[] pendingSpr){
        integer remainingDays = 6 - currentBusinessDay();
        string htmlBody ='<p style="font-size: 75%;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;"><b>Sales Performance Request Notification</b></br></br>'+
            'The following requests are pending incentive approval. ';
        if(remainingDays == 1) {
            htmlBody += 'There is <b>'+ remainingDays +'</b> business day remaining to approve or reject.</br></br>';
        } else{
            htmlBody += 'There are <b>'+ remainingDays  +'</b> business days remaining to approve or reject. </br></br>';      
        }
        
        htmlBody += '<u>Sales Performance Requests</u>:</br>'+
            '<table style="font-size: 75%;width: 100%;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;border-collapse: collapse;border-color: grey;border: 1px solid #dddbda;">';
        if(pendingSpr.size() > 0){
            htmlBody += '<tr style="font-size: 0.75rem;line-height: 1.25;color: #706e6b;text-transform: uppercase;letter-spacing: 0.0625rem;">'+
                '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Name</th>'+
                '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Type</th>'+
                '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Submitted By</th>'+
                '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Current Approver</th>'+
                '</tr>';
            for (Sales_Performance_Request__c spr: pendingSpr){
                htmlBody +='<tr>'+
                    //'<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+spr.Name +'</td>'+
                    '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+'<a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/'+spr.id+'>'+spr.Name+'</a></td>'+
                    '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+spr.Type__c+'</td>'+
                    '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+spr.Owner.Name+'</td>'+
                    '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+spr.Current_Approver__c +'</td>'+
                    '</tr>';
            }
            htmlBody += '</table>';
        } 
        return htmlBody;  
    }
    
    
    public List<String> queryEmails(Sales_Performance_Request__c[] pendingSpr){
        
        set<String> sToAddresses = new set<String>();  
        list<String> toAddresses = new list<String>();
        for(Sales_Performance_Request__c spr: pendingSpr){
            integer stepnum = integer.valueOf(spr.Current_Approval_Step__c);
            
            
            
            if (stepNum == 1 ){
                sToAddresses.add(spr.Assigned_Approver_1__r.Email);
                sToAddresses.add(spr.Assigned_Approver_2__r.Email);
                sToAddresses.add(spr.Assigned_Approver_3__r.Email);
                sToAddresses.add(spr.Assigned_Approver_4__r.Email);
                sToAddresses.add(spr.Assigned_Approver_5__r.Email);
                sToAddresses.add(spr.Assigned_Approver_6__r.Email);
                sToAddresses.add(spr.Assigned_Approver_7__r.Email);
                sToAddresses.add(spr.Assigned_Approver_8__r.Email);
                sToAddresses.add(spr.Assigned_Approver_9__r.Email);
                sToAddresses.add(spr.Assigned_Approver_10__r.Email);

            }
            if (stepNum == 2 ){
                sToAddresses.add(spr.Assigned_Approver_2__r.Email);
                sToAddresses.add(spr.Assigned_Approver_3__r.Email);
                sToAddresses.add(spr.Assigned_Approver_4__r.Email);
                sToAddresses.add(spr.Assigned_Approver_5__r.Email);
                sToAddresses.add(spr.Assigned_Approver_6__r.Email);
                sToAddresses.add(spr.Assigned_Approver_7__r.Email);
                sToAddresses.add(spr.Assigned_Approver_8__r.Email);
                sToAddresses.add(spr.Assigned_Approver_9__r.Email);
                sToAddresses.add(spr.Assigned_Approver_10__r.Email);
            }
            if (stepNum == 3 ){
                sToAddresses.add(spr.Assigned_Approver_3__r.Email);
                sToAddresses.add(spr.Assigned_Approver_4__r.Email);
                sToAddresses.add(spr.Assigned_Approver_5__r.Email);
                sToAddresses.add(spr.Assigned_Approver_6__r.Email);
                sToAddresses.add(spr.Assigned_Approver_7__r.Email);
                sToAddresses.add(spr.Assigned_Approver_8__r.Email);
                sToAddresses.add(spr.Assigned_Approver_9__r.Email);
                sToAddresses.add(spr.Assigned_Approver_10__r.Email);
            }
            if (stepNum == 4 ){
                sToAddresses.add(spr.Assigned_Approver_4__r.Email);
                sToAddresses.add(spr.Assigned_Approver_5__r.Email);
                sToAddresses.add(spr.Assigned_Approver_6__r.Email);
                sToAddresses.add(spr.Assigned_Approver_7__r.Email);
                sToAddresses.add(spr.Assigned_Approver_8__r.Email);
                sToAddresses.add(spr.Assigned_Approver_9__r.Email);
                sToAddresses.add(spr.Assigned_Approver_10__r.Email);
            }
            if (stepNum == 5 ){
                sToAddresses.add(spr.Assigned_Approver_5__r.Email);
                sToAddresses.add(spr.Assigned_Approver_6__r.Email);
                sToAddresses.add(spr.Assigned_Approver_7__r.Email);
                sToAddresses.add(spr.Assigned_Approver_8__r.Email);
                sToAddresses.add(spr.Assigned_Approver_9__r.Email);
                sToAddresses.add(spr.Assigned_Approver_10__r.Email);
            }
            if (stepNum == 6 ){
                sToAddresses.add(spr.Assigned_Approver_6__r.Email);
                sToAddresses.add(spr.Assigned_Approver_7__r.Email);
                sToAddresses.add(spr.Assigned_Approver_8__r.Email);
                sToAddresses.add(spr.Assigned_Approver_9__r.Email);
                sToAddresses.add(spr.Assigned_Approver_10__r.Email);
            }
            if (stepNum == 7 ){
                sToAddresses.add(spr.Assigned_Approver_7__r.Email);
                sToAddresses.add(spr.Assigned_Approver_8__r.Email);
                sToAddresses.add(spr.Assigned_Approver_9__r.Email);
                sToAddresses.add(spr.Assigned_Approver_10__r.Email);
            }
            if (stepNum == 8 ){
                sToAddresses.add(spr.Assigned_Approver_8__r.Email);
                sToAddresses.add(spr.Assigned_Approver_9__r.Email);
                sToAddresses.add(spr.Assigned_Approver_10__r.Email);
            }
            if (stepNum == 9 ){
                sToAddresses.add(spr.Assigned_Approver_9__r.Email);
                sToAddresses.add(spr.Assigned_Approver_10__r.Email);
            }
            if (stepNum == 10 ){
                sToAddresses.add(spr.Assigned_Approver_10__r.Email);
            }
        }   
        
        for(String s:sToAddresses){
            if(s != null){
                toAddresses.add(s);
            }
        }
        return toAddresses;
    }

}