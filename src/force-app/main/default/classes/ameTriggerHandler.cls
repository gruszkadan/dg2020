//Test Class: testAMEUtility,testAMENewController
public class ameTriggerHandler {
    // AFTER UPDATE
    public static void afterUpdate(Account_Management_Event__c[] oldame, Account_Management_Event__c[] newame) {
        Account_Management_Event__c[] ownerChanges = new list<Account_Management_Event__c>();
        Account_Management_Event__c[] activatedRenewals = new list<Account_Management_Event__c>();
        for (integer i=0; i<newame.size(); i++) {
            if (newame[i].ownerId != oldame[i].OwnerId && string.valueOf(newame[i].ownerId).startsWith('005')) {
                ownerChanges.add(newame[i]);
            }
            if (oldame[i].Status__c == 'Not Yet Created' && newame[i].Status__c == 'Active' &&
                newame[i].RecordTypeId == Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId()) 
            {
                activatedRenewals.add(newame[i]);
            }
        }
        if (OwnerChanges.size() > 0) {
            ameUtility.updateRelatedRecordsOwner(OwnerChanges);
        } 
        if (activatedRenewals.size() > 0) {
            quoteUtility.createQuoteFromAME(activatedRenewals);
        } 
    }
}