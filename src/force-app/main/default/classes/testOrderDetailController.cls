@isTest
private class testOrderDetailController {
    
    private static testmethod void test1() {
        
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            AT_Product_Mapping__c atpm = new AT_Product_Mapping__c();
            atpm.Name = '94';
            atpm.Admin_Tool_Product_Name__c = 'HQ';
            atpm.Category__c = 'MSDS Management';
            atpm.Salesforce_Product_Name__c = 'HQ';
            atpm.Display_on_Account__c = true;
            atpm.Product_Platform__c = 'MSDSonline';
            atpm.Subscription_Based__c = true;
            atpm.Single_Year_Incentive_Rate__c = 10;
            atpm.Multi_Year_Incentive_Rate__c = 20;
            insert atpm;
            
            Sales_Performance_Summary__c[] vpSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=0; i<12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, null);
                sps.Month__c = testDataUtility.findMonth(i + 1);
                sps.Manager__c = null;
                sps.Sales_Team_Manager__c = vp.id;
                vpSPSs.add(sps);
            }
            insert vpSPSs;
            
            //Create 2 order items to create on 1 order
            Order_Item__c[] testOIs = new list<Order_Item__c>();
            
            Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
            oi1.Order_ID__c = 'test456';
            oi1.Month__c = 'January';
            oi1.Salesforce_Product_Name__c = 'HQ';
            oi1.Category__c = 'MSDS Management';
            oi1.Product_Platform__c = 'MSDSonline';
            testOIs.add(oi1);
            
            Order_Item__c oi2 = util.createOrderItem('TestVP', 'test2');
            oi2.Order_ID__c = 'test456';
            oi2.Month__c = 'January';
            oi2.Salesforce_Product_Name__c = 'HQ';
            oi2.Category__c = 'MSDS Management';
            oi1.Product_Platform__c = 'MSDSonline';
            testOIs.add(oi2);
            insert testOIs;
            
            Sales_Performance_Order__c spo = [SELECT id FROM Sales_Performance_Order__c WHERE Order_ID__c = 'test456' LIMIT 1];
            ApexPages.currentPage().getParameters().put('id', spo.id);
            orderDetailController con = new orderDetailController();
            
             ApexPages.currentPage().getParameters().put('spItemID', con.items[0].id);
            con.queryIncentives();
        }
    }
}