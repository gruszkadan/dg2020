public class proposalBuilderCoverPage {
    
    //public attachment ourAttachment {get;set;}
    public Id quoteId {get;set;}
    //public List<Attachment> quoteAttachment {get;set;}
    public Quote__c quote {get;set;}
    //public String attachmentName {get;set;}
    public boolean hasNeeds {get;set;}
    
    public proposalBuilderCoverPage(){
        
        //ourAttachment = new attachment();
        
        quoteId = ApexPages.currentPage().getParameters().get('Id');
        
        quote = [SELECT Id, Name, Show_Contact_Name__c, JSON_Proposal_Data__c FROM Quote__c WHERE id = :quoteId];
        hasNeeds = proposalBuilderUtility.containsNeeds(quote.JSON_Proposal_Data__c);
        
        //query logo attachments that already exist for quote
       //quoteAttachment = [SELECT Id FROM Attachment WHERE ParentId = :quote.id AND Name LIKE 'Customer_Logo%' LIMIT 1];
        
    }
    
    
    public PageReference nextPage(){
        
        
        /*if there is already logo, delete for no logo or replacement 
       // if(quoteAttachment.size() > 0){
           // delete quoteAttachment;
       // }
        
        //if there is attachment, link to quote and give name Customer_Logo+extension
       // if(ourAttachment.body != null){
       //     ourAttachment.ParentId = quoteId;
        //    ourAttachment.name = 'Customer_Logo.'+attachmentName.split('\\.').get(1); 
        //    insert ourAttachment; 
        //}
        */
        
        //update quote with show contact field update
        update quote;
        if(hasNeeds){
            return new PageReference('/apex/proposal_builder_needs_requirements?id=' + QuoteId);     
        }else{
            return new PageReference('/apex/proposal_builder_proposed_solution?id=' + QuoteId); 
        }
    }
    
   /* public void deleteLogo(){
        
        //remove logo from page and quote detail
        delete quoteAttachment; 
        
        //requery quoteAttachment so VF page knows size = 0
        quoteAttachment.clear();
        
    }
    */
    
}