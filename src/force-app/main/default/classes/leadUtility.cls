public class leadUtility {
    
    //set the lead created date
    public static void leadCreatedDateAndGroup(Lead[] leads) {
        set<id> createdByIds = new set<id>();
        for(Lead l:leads){
            createdByIds.add(l.CreatedById);
        }
        User[] users = [Select id,Group__c from User where id in:createdByIds];
        for (User u:users){
            for (Lead l : leads) {
                if(l.CreatedById == u.id){
                    if (l.Lead_Created_Date__c == null) {
                        l.Lead_Created_Date__c = l.CreatedDate;
                    }
                    l.Created_By_Group__c = u.Group__c;
                }
            }
        }
    }
    
    // check each lead to see if it is an enterprise company and mark it accordingly
    public static void leadEnterpriseCheck(Lead[] leads) {
        string ecList = [SELECT Body
                         FROM StaticResource
                         WHERE Name = 'enterpriseCompanyList' LIMIT 1].Body.toString();
        if (ecList != null) {
            for (Lead l : leads) {
                if (ecList.containsIgnoreCase(';' + l.Company + ';')) {
                    l.Enterprise_Sales_Lead__c = true; 
                    System.debug(l.Enterprise_Sales_Lead__c);
                }
                
            }
        }
    }
    
    // check each lead to see if it is an enterprise company and mark it accordingly based on email domains
    public static void leadEnterpriseEmailCheck(Lead[] leads, boolean fromBeforeInsert) {
        string ecList = [SELECT Body
                         FROM StaticResource
                         WHERE Name = 'enterpriseEmailDomainList' LIMIT 1].Body.toString();
        if (ecList != null) {
            for (Lead l : leads) {
                //To get email doamin: SUBSTITUTE(Email, LEFT(Email, FIND("@", Email)), NULL); Domain could be null: does not break when tested in execute anon
                if (ecList.containsIgnoreCase(';' + l.Domain__c + ';')) {
                    if (fromBeforeInsert){
                        l.Enterprise_Sales_Lead__c = true;
                        l.ES_Email_Domain__c = true;
                    }else{
                        //BeforeUpdate set checkbox to true but do not set ES Sales Lead to True.
                        l.ES_Email_Domain__c = true;      
                    }
                }else{
                    //default is unchecked/false.  Updating for when change occurs (true --> false)
                    l.ES_Email_Domain__c = false;
                }  
            }
        }
    }
    
    
    // when converting a lead, make sure the right date and source are copied to the contact
    public static void leadCreatedDateAndSource(Lead[] leads) {
        set<id> contactIds = new set<id>();
        for (Lead l : leads) {
            if (l.ConvertedContactId != null) {
                contactIds.add(l.ConvertedContactId);
            }
        }
        if (contactIds.size() > 0) {
            map<Lead, Contact> lcMap = new map<Lead, Contact>();
            Contact[] contacts = [SELECT id, AccountID, Lead_Created_Date__c, Temporary_Lead_Created_Date__c
                                  FROM Contact
                                  WHERE id IN : contactIds];
            for (lead l : leads) {
                for (Contact c : contacts) {
                    if (c.id == l.ConvertedContactId) {
                        lcMap.put(l, c);
                    }
                }
            }
            Contact[] updateList = new list<Contact>();
            for (lead l : lcMap.keySet()) {
                Contact c = lcMap.get(l);
                if (c.Lead_Created_Date__c == null) {
                    c.Lead_Created_Date__c = c.Temporary_Lead_Created_Date__c;
                    c.LeadSource = l.LeadSource;
                    updateList.add(c);
                } else if (l.Lead_Created_Date__c < c.Lead_Created_Date__c) {
                    c.Lead_Created_Date__c = l.Lead_Created_Date__c;
                    c.LeadSource = l.LeadSource;
                    updateList.add(c);
                }
            }
            if (updateList.size() > 0) {
                update updateList;
            }
            if(contacts.size()>0){
                contactUtility.reassignContact(contacts, true);  
            }
        }
        
    }
    
    // When a lead's ownership is changed manually, we need to double check it to make sure the chemical management owner 
    //matches the new owner
    public static void checkChemMgmtOwners(Lead[] leads) {
        for (Lead l : leads) {
            if (l.Chemical_Management_Owner__c != l.OwnerId) {
                l.Chemical_Management_Owner__c = l.OwnerId;
            }
        }
    }
    
}