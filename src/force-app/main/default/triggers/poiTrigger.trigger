trigger poiTrigger on Product_Of_Interest__c 
(after update) {
    if (trigger.isAfter) {
        if (trigger.isUpdate) {
            poiTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
    }
}