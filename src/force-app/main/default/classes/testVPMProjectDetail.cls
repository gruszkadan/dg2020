@isTest
private class testVPMProjectDetail {
    
    static testmethod void VPMProjectDetail() {     
        
        VPM_Project__c proj = new VPM_Project__c();
        
        Account testAccount = new Account(name = 'Ace Hardware');
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.AccountId = testAccount.id;
        testContact.LastName = 'Texas';
        insert testContact;
        
        proj.Account__c = testAccount.id;
        proj.Contact__c = testContact.id;
        insert proj;
        
        //query Users to use for OwnerIds 
        User milestoneOwner = [SELECT Id FROM User WHERE Name = 'Al Powell'];
        User taskOwner = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka'];
        
        VPM_Milestone__c milestone = new VPM_Milestone__c(VPM_Project__c = proj.Id, OwnerId = milestoneOwner.Id);
        insert milestone;
        
        VPM_Task__c task = new VPM_Task__c(VPM_Project__c = proj.Id, OwnerId = taskOwner.Id, Status__c = 'Not Started', Contact__c = testContact.id, Start_Date__c = Date.today(), VPM_Milestone__c = milestone.id);
        VPM_Note__c note = new VPM_Note__c(VPM_Project__c = proj.id);
        
        insert task;
        insert note;
        
        ApexPages.currentPage().getParameters().put('Id', proj.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new VPM_Project__c());
        VPMProjectDetail myController = new VPMProjectDetail(testController);
        
        //Test that the FullPhotoUrls are being stored with OwnerIds of task/milestone; map being populated
        system.assertEquals(myController.ownerPhotos.size(), 2);
        
        //inlineEdit changes to true when method is run
        myController.inputField();
        system.assertEquals(myController.inlineEdit, true);
        
        myController.getContacts();
        myController.primaryContact = testContact.LastName;
        myController.saveProject();
        
        ApexPages.currentPage().getParameters().put('editNoteId', note.Id);
        myController.selectNoteToEdit();
        
        myController.saveEditedNote();
        
        ApexPages.currentPage().getParameters().put('selRecordType', 'VPM_Milestone__c');
        ApexPages.currentPage().getParameters().put('selRecordId', milestone.id);
        
        myController.querySelectedRecord();
        myController.resetRightPanelView();
        
        myController.newNote.Type__c = 'Configuration';
        myController.newNote.Comments__c = 'Hello Sir';
        
        //Check that note was added to the list
        myController.addNote();
        system.assertEquals(myController.notes.size(), 2);
        
        myController.toggleModal();
        myController.cancelButton();
        
    }
    
}