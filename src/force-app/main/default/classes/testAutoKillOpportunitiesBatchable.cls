@isTest 
public class testAutoKillOpportunitiesBatchable {

    public static string CRON_EXP = '0 0 0 15 3 ? 2040';

    static testmethod void test1() { 
        Opportunity[] OppIds = new List<Opportunity>();
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;

        Contact nc = new Contact();
        nc.FirstName = 'John';
        nc.LastName = 'Joe';
        nc.AccountId = newaccount.id;        
        insert nc; 

        user u = [SELECT id FROM User WHERE isActive = TRUE AND Department__c = 'Sales' AND Suite_of_interest__c != NULL AND profile.name = 'Sales Representative - Mid-Market'  LIMIT 1];
        
        System.runAs(u) {
        Opportunity o = new Opportunity();
        o.AccountID = newAccount.Id;
        o.Name = 'O 500';
        o.CloseDate = system.today();
        o.StageName = 'Scoping';
        o.Next_Demo_Scheduled_On__c = date.today().addDays(500);
        o.Opportunity_Type__c = 'New';
        o.CreatedDate = System.today().addDays(-35);
        insert o;
        OppIds.add(o);
            
        Event newDemo= new Event();
        newDemo.WhatId = o.Id;
        newDemo.Event_Status__c = 'Scheduled';
        newDemo.ActivityDate = date.today().addDays(500);
        newDemo.DurationInMinutes = 60;
        newDemo.ActivityDateTime = datetime.now().addDays(500);
        newDemo.new_Department__c = 'Sales';
        insert newDemo;
        
        
        Task newT= new Task();
        newT.WhatId = o.Id;
        newT.WhoId = nc.Id;
        newT.Status = 'In Progress';
        newT.new_Department__c = 'Sales';
        newT.ActivityDate = date.today().addDays(500);
        insert newT;
        

        Opportunity o2 = new Opportunity();
        o2.AccountID = newAccount.Id;
        o2.Name = 'o 100';
        o2.CloseDate = system.today();
        o2.StageName = 'Scoping';
        o2.Next_Demo_Scheduled_On__c = date.today().addDays(-100);    
        o2.Opportunity_Type__c = 'New';
        o2.CreatedDate = System.today().addDays(-35);
        insert o2;
        OppIds.add(o2);
            
        Event newDemo2 = new Event();
        newDemo2.WhatId = o2.Id;
        newDemo2.Event_Status__c = 'Scheduled';
        newDemo2.ActivityDate = date.today().addDays(-100);
        newDemo2.DurationInMinutes = 60;
        newDemo2.ActivityDateTime = datetime.now().addDays(-100);
        newDemo2.Activity_Date_Only__c = date.today().addDays(-100);
        newDemo2.new_Department__c = 'Sales';
        insert newDemo2;

        Opportunity o3 = new Opportunity();
        o3.AccountID = newAccount.Id;
        o3.Name = 'o 2d';
        o3.CloseDate = system.today();
        o3.StageName = 'Scoping';
        o3.Next_Demo_Scheduled_On__c = date.today().addDays(2);
        o3.Opportunity_Type__c = 'New';
        o3.CreatedDate = System.today().addDays(-35);
        insert o3;
        OppIds.add(o3);
            
        Event newDemo3= new Event();
        newDemo3.WhatId = o3.Id;
        newDemo3.Event_Status__c = 'Scheduled';
        newDemo3.ActivityDate = date.today().addDays(2);
        newDemo3.DurationInMinutes = 60;
        newDemo3.ActivityDateTime = datetime.now().addDays(2);
        newDemo3.new_Department__c = 'Sales';
        insert newDemo3;

        Event newDemo4 = new Event();
        newDemo4.WhatId = o3.Id;
        newDemo4.Event_Status__c = 'Scheduled';
        newDemo4.ActivityDate = date.today().addDays(400);
        newDemo4.DurationInMinutes = 60;
        newDemo4.ActivityDateTime = datetime.now().addDays(400);
        newDemo4.new_Department__c = 'Sales';
        insert newDemo4;
        }
                
        //lookup rollup summary spoof
        for(Opportunity opp: OppIds){          
          switch on opp.Name {
                when 'O 500' {
                    opp.Next_Demo_Scheduled_On__c = date.today().addDays(500);
                }
                when 'o 2d' {
                    opp.Next_Demo_Scheduled_On__c = date.today().addDays(2);
                }
                when 'o 100' {
                    opp.Next_Demo_Scheduled_On__c = date.today().addDays(-100);
                }                    
            }
        }
        update OppIds;        

        Test.startTest();
        ScheduleAutoKillBatches s = new ScheduleAutoKillBatches();
        s.execute(null) ;
        Test.stopTest();
		
        Opportunity[] checkOpps = [SELECT id, stageName, Name, 
                                             (SELECT Status FROM Tasks),
                                             (SELECT Event_status__c FROM Events)
                                                FROM Opportunity
                                                WHERE id IN :OppIds];

		for(Opportunity o: checkOpps){
          
            if(o.Name == 'O 500'){
                System.assert(o.stageName == 'Closed');
                for(task t: o.tasks){
                    System.assert(t.Status == 'Auto-Closed');                    
                }
                for(event e: o.events){
                    System.assert(e.Event_Status__c == 'Auto-Closed');    
                }
            }
			
             if(o.Name == 'o 100'){
                System.assert(o.stageName == 'Closed');
            }	
			
            
            if(o.Name == 'o 2d'){
                System.assert(o.stageName != 'Closed');
            }
        }  

    }
}