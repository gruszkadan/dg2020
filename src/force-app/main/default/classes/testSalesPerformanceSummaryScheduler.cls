@isTest
private class testSalesPerformanceSummaryScheduler {
    
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    
    private static testmethod void test_scheduler() {
        test.startTest();
        string jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new salesPerformanceSummaryScheduler());
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger 
                          WHERE id = : jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', string.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
    
    private static testmethod void test_batchable1() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr1 = util.createUser('TestMGR1', 'manager', vp, dir, dir);
            insert mgr1;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr1);
            insert rep;
            
            Sales_Performance_Summary__c vpSPS = util.createSPS(vp, null);
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = util.createSPS(dir, vpSPS);
            insert dirSPS;
            
            Sales_Performance_Summary__c mgr1SPS = util.createSPS(mgr1, dirSPS);
            insert mgr1SPS;
            
            test.startTest();
            
            salesPerformanceSummaryBatchableStep1 step1 = new salesPerformanceSummaryBatchableStep1();
            Database.executeBatch(step1, 1);
            
            test.stopTest();
            
            Sales_Performance_Summary__c[] repSummaries = [SELECT id FROM Sales_Performance_Summary__c WHERE OwnerId = : rep.id];
            System.assert(repSummaries.size() > 0);
            
        }
    }
    
    private static testmethod void test_batchable2() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr1 = util.createUser('TestMGR1', 'manager', vp, dir, dir);
            insert mgr1;
            
            User mgr2 = util.createUser('TestMGR2', 'manager', vp, dir, dir);
            insert mgr2;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr1);
            insert rep;
            
            Sales_Performance_Summary__c[] vpSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=1; i<=12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, null);
                sps.Month__c = testDataUtility.findMonth(i);
                vpSPSs.add(sps);
            }
            insert vpSPSs;
            
            Sales_Performance_Summary__c[] dirSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=1; i<=12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(dir, vpSPSs[i-1]);
                sps.Month__c = testDataUtility.findMonth(i);
                dirSPSs.add(sps);
            }
            insert dirSPSs;
            
            Sales_Performance_Summary__c[] mgr1SPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=1; i<=12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(mgr1, dirSPSs[i-1]);
                sps.Month__c = testDataUtility.findMonth(i);
                mgr1SPSs.add(sps);
            }
            insert mgr1SPSs;
            
            Sales_Performance_Summary__c[] mgr2SPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=1; i<=12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(mgr2, dirSPSs[i-1]);
                sps.Month__c = testDataUtility.findMonth(i);
                mgr2SPSs.add(sps);
            }
            insert mgr2SPSs;
            
            Sales_Performance_Summary__c[] repSPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=1; i<=12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(rep, mgr1SPSs[i-1]);
                sps.Month__c = testDataUtility.findMonth(i);
                repSPSs.add(sps);
            }
            insert repSPSs;
            
            Order_Item__c oi = util.createOrderItem('TestREP', 'test1');
            insert oi; 
            
            rep.ManagerId = mgr2.id;
            rep.Sales_Performance_Manager_ID__c = mgr2.id;
            update rep;
            
            test.startTest();

            salesPerformanceSummaryBatchableStep2 step2 = new salesPerformanceSummaryBatchableStep2();
            Database.executeBatch(step2, 1);
            
            test.stopTest();
            
            // Assert the OI
            oi = [SELECT id, Sales_Manager__c, Order_ID__c, Order_Item_ID__c
                 FROM Order_Item__c
                  WHERE id = : oi.id];
            System.assert(oi.Sales_Manager__c == mgr2.id); 
            
            // Assert the SPI
            Sales_Performance_Item__c spi = [SELECT id, Order_ID__c, Order_Item_ID__c, Sales_Manager__c
                                             FROM Sales_Performance_Item__c
                                             WHERE Order_Item_ID__c = : oi.Order_Item_ID__c];
            System.assert(spi.Sales_Manager__c == mgr2.id);
            
            // Assert the SPO
            Sales_Performance_Order__c spo = [SELECT id, Order_ID__c, Sales_Manager__c
                                              FROM Sales_Performance_Order__c
                                              WHERE Order_ID__c = : oi.Order_ID__c];
            System.assert(spo.Sales_Manager__c == mgr2.id);
            
            // Assert the summaries            
            Sales_Performance_Summary__c[] repSummaries = [SELECT id, Manager__c, Month_Number__c FROM Sales_Performance_Summary__c WHERE OwnerId = : rep.id];
            Sales_Performance_Summary__c[] prevSummaries = new list<Sales_Performance_Summary__c>();
            Sales_Performance_Summary__c[] nextSummaries = new list<Sales_Performance_Summary__c>();
            for (Sales_Performance_Summary__c sps : repSummaries) {
                if (sps.Month_Number__c >= date.today().month()) {
                    nextSummaries.add(sps);
                } else {
                    prevSummaries.add(sps);
                }
            }
            
            // Assert only this month and forward have changed summaries
            for (Sales_Performance_Summary__c sps : nextSummaries) {
                System.assert(sps.Manager__c == mgr2.id);
            }
            
            for (Sales_Performance_Summary__c sps : prevSummaries) {
                System.assert(sps.Manager__c == mgr1.id);
            }

        }
    } 
    
  
}