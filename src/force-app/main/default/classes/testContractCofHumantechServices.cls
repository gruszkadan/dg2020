@isTest(seeAllData = true)
private class testContractCofHumantechServices {
    static testmethod void test1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.A2_SaaS_Start_Date__c = date.today();
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '5 Years';
        insert newContract;  
        
        Product2 newProd =  [ SELECT id, Name, Contract_Print_Group__c FROM Product2 WHERE Name = 'Humantech Industrial Ergonomics - Proof of Concept' AND product_sub_type__c = 'Group' LIMIT 1 ];
        
        Contract_Line_Item__c cli = new Contract_Line_Item__c();
        cli.Contract__c = newContract.Id;
        cli.Product__c = newProd.id;
        cli.Contract_Sort_Order__c = 1;
        insert cli;
        
        ApexPages.currentPage().getParameters().put('id', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
        contractCofHumantechServices myController = new contractCofHumantechServices(testController);
        
    }
}