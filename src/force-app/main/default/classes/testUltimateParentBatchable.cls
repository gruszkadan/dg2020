@isTest
private class testUltimateParentBatchable {
    
    public static testmethod void test1() {
        
        Account[] parents = new list<Account>();
        parents.add(new Account(Name = 'Subway'));
        parents.add(new Account(Name = 'Taco Bell'));
        parents.add(new Account(Name = 'Burger King'));
        //insert one without children
        parents.add(new Account(Name = 'Quiznos')); 
        insert parents;
        
        Account[] children1 = new List<Account>();
        for (integer i=0; i<30; i++) {
            Account child = new Account();
            if (i<10) { child.Name = 'Subway Child 1'; child.ParentId = parents[0].id; }
            if (i>=10 && i<20) { child.Name = 'Taco Bell Child 1'; child.ParentId = parents[1].id; }
            if (i>=20) { child.Name = 'Burger King Child 1'; child.ParentId = parents[2].id; }
            children1.add(child);
        }
        insert children1;
        
        Account[] children2 = new List<Account>();
        for (integer i=0; i<30; i++) {
            Account child = new Account();
            if (i<10) { child.Name = 'Subway Child 2'; child.ParentId = children1[i].id; }
            if (i>=10 && i<20) { child.Name = 'Taco Bell Child 2'; child.ParentId = children1[i].id; }
            if (i>=20) { child.Name = 'Burger King Child 2'; child.ParentId = children1[i].id; }
            children2.add(child);
        }
        insert children2;        
        
        Account[] children3 = new List<Account>();
        for (integer i=0; i<30; i++) {
            Account child = new Account();
            if (i<10) { child.Name = 'Subway Child 3'; child.ParentId = children2[i].id; }
            if (i>=10 && i<20) { child.Name = 'Taco Bell Child 3'; child.ParentId = children2[i].id; }
            if (i>=20) { child.Name = 'Burger King Child 3'; child.ParentId = children2[i].id; }
            children3.add(child);
        }
        insert children3;        
        
        Account[] children4 = new List<Account>();
        for (integer i=0; i<30; i++) {
            Account child = new Account();
            if (i<10) { child.Name = 'Subway Child 4'; child.ParentId = children3[i].id; }
            if (i>=10 && i<20) { child.Name = 'Taco Bell Child 4'; child.ParentId = children3[i].id; }
            if (i>=20) { child.Name = 'Burger King Child 4'; child.ParentId = children3[i].id; }
            children4.add(child);
        }
        insert children4;      
        
        Account[] children5 = new List<Account>();
        for (integer i=0; i<30; i++) {
            Account child = new Account();
            if (i<10) { child.Name = 'Subway Child 5'; child.ParentId = children4[i].id; }
            if (i>=10 && i<20) { child.Name = 'Taco Bell Child 5'; child.ParentId = children4[i].id; }
            if (i>=20) { child.Name = 'Burger King Child 5'; child.ParentId = children4[i].id; }
            children5.add(child);
        }
        insert children5;      
        
        Account[] children6 = new List<Account>();
        for (integer i=0; i<30; i++) {
            Account child = new Account();
            if (i<10) { child.Name = 'Subway Child 6'; child.ParentId = children5[i].id; }
            if (i>=10 && i<20) { child.Name = 'Taco Bell Child 6'; child.ParentId = children5[i].id; } 
            if (i>=20) { child.Name = 'Burger King Child 6'; child.ParentId = children5[i].id; }
            children6.add(child);
        }
        insert children6;      
        
        ultimateParentBatchable upb = new ultimateParentBatchable();
        id batchprocessId = Database.executeBatch(upb);
        
    }
    
}