@isTest
public class testProposalNeedsRequirements {
    public static testmethod void test1() {
       
        List<string> orderedList = new List<string>();
        
        Quote_Proposal_Need__c  qpnA = new Quote_Proposal_Need__c();
        qpnA.Available_For_Use__c = true;
        insert qpnA;
        OrderedList.add(qpnA.Id);
        
        Quote_Proposal_Need__c  qpnB = new Quote_Proposal_Need__c();
        qpnB.Available_For_Use__c = true;
        insert qpnB;
        OrderedList.add(qpnB.Id);
        
        Quote__c newQuote = new Quote__c();
        newQuote.Contract_Length__c = '2 Years';
        newQuote.Needs_JSON__c = JSON.serialize(orderedList);
        insert newQuote;
        
        ApexPages.currentPage().getParameters().put('id', newQuote.id);
        proposalNeedsRequirements con = new proposalNeedsRequirements();
        System.assert(con.qProposalNeeds.size() == 2);
        System.assert(con.qPNMap.size() == 2);
    }
    
        public static testmethod void test2() {
       
        List<string> orderedList = new List<string>();
        
        Quote_Proposal_Need__c  qpnA = new Quote_Proposal_Need__c();
        qpnA.Available_For_Use__c = true;
        insert qpnA;
        OrderedList.add(qpnA.Id);
        
        Quote_Proposal_Need__c  qpnB = new Quote_Proposal_Need__c();
        qpnB.Available_For_Use__c = false;
        insert qpnB;
        OrderedList.add(qpnB.Id);
        
        Quote__c newQuote = new Quote__c();
        newQuote.Contract_Length__c = '2 Years';
        newQuote.Needs_JSON__c = JSON.serialize(orderedList);
        insert newQuote;
        
        ApexPages.currentPage().getParameters().put('id', newQuote.id);
        proposalNeedsRequirements con = new proposalNeedsRequirements();
        System.assert(con.qProposalNeeds.size() == 1);
        System.assert(con.qPNMap.size() == 1);
    }
}