trigger revenueRecognitionTransactionTrigger on Revenue_Recognition_Transaction__c (after insert, after update) {
    
    //AFTER TRIGGER
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            revRecTransactionTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            revRecTransactionTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
    }
    
}