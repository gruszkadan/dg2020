public with sharing class sfRequestTestScenario{
    public Salesforce_Request_Test_Scenario__c[] scenarioList {get;set;}
    public Salesforce_Request_Test_Scenario__c newScenario {get;set;}
    public Salesforce_Request_Test_Scenario__c scenarioDetail {get;set;}
    public Salesforce_Request__c request {get;set;}
    public User u {get;set;}
    id sfrid;
    id scenid;
    
    public sfRequestTestScenario(ApexPages.StandardController stdCon) {
        sfrId = ApexPages.currentPage().getParameters().get('id');
        scenid = ApexPages.currentPage().getParameters().get('scenid');
        queryScenarios();
        request = [SELECT id, Name, CreatedBy.Name, LastModifiedBy.Name, CreatedDate, LastModifiedDate, Summary__c, Status__c
                   FROM Salesforce_Request__c
                   WHERE id = :sfrId];
        u = [Select id, Name, LastName, Role__c, SF_Request_Owner_Change__c, SF_Request_Status_Change__c, Profile.Name from User where id =: UserInfo.getUserId() LIMIT 1];
        linkToDetail();
        if(scenarioList.size() == 0){
            newScenario();
        }
    }
    
    
    public void queryScenarios(){
        String SobjectApiName = 'Salesforce_Request_Test_Scenario__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String scenario = 'select ' + commaSepratedFields + ' from ' + SobjectApiName + ' where Salesforce_Request__c = :sfrId ORDER BY Step__c asc';
        
        // return the scenarios related to the sf request
        scenarioList = Database.query(scenario);
    }
    
    public PageReference backButton() {
        PageReference sf_Request_detail = Page.sf_Request_detail;
        sf_Request_detail.setRedirect(true);
        sf_Request_detail.getParameters().put('id',sfrid);
        return sf_Request_detail;
    } 
    
    //"new" button
    
    public void newScenario() {
        Integer size = scenarioList.size()+1;
        newScenario = new Salesforce_Request_Test_Scenario__c();
        newScenario.Step__c = size;
        newScenario.Salesforce_Request__c = sfrId;
    }
    
    //save button on the add_edit page
    
    public void saveAddEdit() {
        if(newScenario != null) {
            insert newScenario;
            update scenarioList;
            newScenario = null;
            queryScenarios();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Saved successfully!'));
        }else{
            update scenarioList;
            queryScenarios();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Saved successfully!'));
        }
    }
    
    //save button on the detail page
    
    public void saveDetail(){
        update scenarioDetail;
        queryScenarios();
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Saved successfully!'));
    }
    
    //save button on the reorder page
    
    public void saveReorder(){
        update scenarioList;
        queryScenarios();
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Saved successfully!'));
    }
    
    // finds the scenario details to pull back on test scenario detail page    
    
    public PageReference findScenDetail() {
        string scenarioId = ApexPages.currentPage().getParameters().get('scenarioId');
        if (scenarioId != null) { 
            if (scenarioList.size() > 0) {
                for (Salesforce_Request_Test_Scenario__c s : scenarioList) {
                    if (s.id == scenarioId) {
                        scenarioDetail = s;
                    }
                }
            }
        }
        return new PageReference('/apex/sfrequesttestscenario_detail?id='+sfrId+'&scenid='+scenarioDetail.id).setRedirect(true);
    }
    
    //sets scenarioDetail based on the paramater passed in the url from the sfRequest_detail page
    
    public void linkToDetail() {
        if (scenid != null) {
            // check to see if the list has any records in it
            if (scenarioList.size() > 0) {
                // if it does have records in it, loop through the list
                for (Salesforce_Request_Test_Scenario__c s : scenarioList) {
                    // look for the salesforce_Request_Test_Scenario record that has an id matching the scenid
                    if (s.id == scenid) {
                        // once found, this is the scenarioDetail record
                        scenarioDetail = s;
                    }   
                }
            }
        }
    }
}