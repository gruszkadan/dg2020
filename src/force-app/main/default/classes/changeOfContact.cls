public with sharing class changeOfContact {
    public Contact oldContact {get;set;}
    public string oldContactID {get;set;}
    public string AccountID {get;set;}
    public string newContactID {get;set;}
    public Contact newContact {get;set;}
    public Case[] oldContactCases {get;set;}
    public string CustomerAdministratorId{get;set;}
    public string oldContactStatus{get;set;}
    public Set<String> caseOwnerEmails {get;set;}
    public List<String> caseOwnerEmailsList {get;set;}
    public List<String> toAddresses {get;set;}
    public User u {get;set;}
    public string environment {get;set;}
    public Renewal__c[] oldRenewals;
    public Account theAccount {get;set;}
    public VPM_Project__c[] oldProjects {get;set;}
  	public VPM_Milestone__c[] oldMilestones {get;set;}
    public VPM_Task__c[] oldTasks {get;set;}
    public set<String> setToAddresses {get;set;}
    public boolean hasErrors {get;set;}
    
    public changeOfContact() {
      
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D300000001HEfEAM'){
            environment = 'Production';
        } else {
            environment ='Sandbox';
        }
        
        
        toAddresses = new List<String>();
        setToAddresses = new set<String>();
        if(environment == 'Production'){
            setToAddresses.add('billing@ehs.com');
        }else{
            setToAddresses.add('twills@ehs.com');   
        }
        
        caseOwnerEmailsList = new List<String>();
        caseOwnerEmails = new Set<String>();
        Id contactID = ApexPages.currentPage().getParameters().get('contactID');
        oldContactID = contactID;
        u = [Select id, Name, FirstName, Email, Username from User where id =: UserInfo.getUserId() LIMIT 1];
        if(oldContactID != NULL){
            oldContact = [Select ID, Name, Status__c, CustomerAdministratorId__c, Update_Type__c, AccountID, Account.Name from Contact where ID=:oldContactID];  
            accountID = oldContact.AccountId;
            CustomerAdministratorId = oldContact.CustomerAdministratorId__c;
            oldContactCases = [Select ID, Case.Owner.Email from Case where ContactID=:oldContact.Id and Status='Active' and Owner.Type='User'];
            if(oldContactCases.size()>0){
                for(Case c : oldContactCases){
                    caseOwnerEmailsList.add(c.Owner.Email);
                    caseOwnerEmails.add(c.Owner.Email);
                    setToAddresses.add(c.Owner.Email);
                }
            }            
            
            
            oldRenewals = [Select Owner.Email from Renewal__c where Status__c='Active' and Renewal__c.Account__c = :accountID and Owner.Type='User'];
            if(oldRenewals.size()>0){
                for (renewal__c r: oldRenewals){
                    setToAddresses.add(r.Owner.Email);
                }
            }
            
            
            theAccount = [Select Owner.Email, Owner.Opt_In_to_CoC_Notification__c, Customer_Success_Specialist__r.email, EHS_Owner__r.Email, EHS_Owner__r.Opt_In_to_CoC_Notification__c, Ergonomics_Account_Owner__r.Email,
                          Ergonomics_Account_Owner__r.Opt_In_to_CoC_Notification__c, Authoring_Account_Owner__r.Email, Authoring_Account_Owner__r.Opt_In_to_CoC_Notification__c,
                          Online_Training_Account_Owner__r.Email, Online_Training_Account_Owner__r.Opt_In_to_CoC_Notification__c
                          from Account 
                          where ID = :accountID ];
            if (theAccount.Customer_Success_Specialist__r.email != NULL){
                setToAddresses.add(theAccount.Customer_Success_Specialist__r.email);
            }
            if (theAccount.Owner.Email != NULL && theAccount.Owner.Opt_In_to_CoC_Notification__c){
                setToAddresses.add(theAccount.Owner.Email);
            }
            if (theAccount.EHS_Owner__r.Email != NULL  && theAccount.EHS_Owner__r.Opt_In_to_CoC_Notification__c){
                setToAddresses.add(theAccount.EHS_Owner__r.Email);
            }
            if (theAccount.Ergonomics_Account_Owner__r.Email != NULL && theAccount.Ergonomics_Account_Owner__r.Opt_In_to_CoC_Notification__c){
                setToAddresses.add(theAccount.Ergonomics_Account_Owner__r.Email);
            }
            if (theAccount.Authoring_Account_Owner__r.Email != NULL && theAccount.Authoring_Account_Owner__r.Opt_In_to_CoC_Notification__c){
                setToAddresses.add(theAccount.Authoring_Account_Owner__r.Email);
            }
           	if (theAccount.Online_Training_Account_Owner__r.Email != NULL && theAccount.Online_Training_Account_Owner__r.Opt_In_to_CoC_Notification__c){
                setToAddresses.add(theAccount.Online_Training_Account_Owner__r.Email);
            }
                       
            //Add active/open projects, milestones, and tasks owners to the email if the old contact was related to the project/milestone, task
           	oldProjects = [Select Owner.Email from VPM_Project__c where Contact__c = :oldContactID and Project_Status__c = 'Open'];
            if(oldProjects.size() > 0){
                for (VPM_Project__c PM: oldProjects){
                    setToAddresses.add(PM.Owner.Email);
                }
            }
            
            oldMilestones = [Select Owner.Email from VPM_Milestone__c where Contact__c = :oldContactID and Status__c = 'Open'];
            if(oldProjects.size() > 0){
                for (VPM_Milestone__c mile: oldMilestones){
                    setToAddresses.add(mile.Owner.Email);
                }
            }
            
            oldTasks = [Select Owner.Email from VPM_Task__c where Contact__c = :oldContactID and Status__c != 'Complete' ];
            if(oldTasks.size() > 0){
                for (VPM_Task__c tsk: oldTasks){
                    setToAddresses.add(tsk.Owner.Email);
                }
            }
            
        }
        
        if(setToAddresses.size() > 0){
            toAddresses.addAll(setToAddresses);
        }

    }
    
    public List<SelectOption> getStatus(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('-- Select Status --', '-- Select Status --'));  
        Schema.DescribeFieldResult fieldResult =
            Contact.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getContactSelectList() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('', '-- Select Contact --'));        
        Contact[] contacts = [SELECT id, Name FROM Contact WHERE AccountId = :accountID and ID!= :oldContactID ORDER BY Name ASC];
        for (Contact contact : contacts) {
            ops.add(new SelectOption(contact.id, contact.Name));
        }
        return ops;
    }
    
    
    
    public void updateNewContact(){
        newContact = [select ID, Status__c, CustomerAdministratorId__c, Name, Phone, Email from Contact where ID=:newContactID];
        newContact.Status__c = 'Active';
        newContact.CustomerAdministratorId__c = CustomerAdministratorId;
        if(oldContact.Update_Type__c == 'Primary Admin & Billing Contact Update'){
            newContact.Billing_Contact__c = true;
        }
        update newContact;
    }
    
    public void updateOldContact(){
        oldContact.CustomerAdministratorId__c = NULL;
        oldContact.Status__c = oldContactStatus;
        oldContact.Change_of_Contact_Date__c = Date.today();
        //if includes billing contact update and is currently a billing contact, set to false
        if (oldContact.Update_Type__c == 'Primary Admin & Billing Contact Update'){
            oldContact.Billing_Contact__c = false;
        }
        update oldContact;
    }
    
    public void SendEmail(){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setHtmlBody('Please be aware that there has been a change of contact for '+ oldContact.Account.Name +'. Contact '+oldContact.Name+' has been changed to contact '+NewContact.Name+'. Please update your records accordingly.  '+'<br/> '+
                              'Link to Account: https://login.salesforce.com/'+oldContact.AccountId +
                              ' <br/>'+' <br/>'+
                              '<b>Update Details: </b>'+' <br/> ' +
                              'Update Type:  '+oldContact.Update_Type__c+' <br/> '+
                              'New Contact:  '+newContact.Name+' <br/> '+
                              'Email:  '+newContact.Email+' <br/> '+
                              'Phone:  '+newContact.Phone+' <br/> '+
                              'Customer Admin Id:  '+CustomerAdministratorId+' <br/> '+
                              'Previous Contact:  '+oldContact.Name+' <br/>' +
                              'Status of Previous Contact:  '+oldContactStatus
                             ); 
        
        mail.setSubject('Change of Contact Alert for '+oldContact.Account.Name);
        mail.setToAddresses(toAddresses);
        mail.setSaveAsActivity(FALSE);
        mail.setUseSignature(FALSE);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public void processCoC() {
        hasErrors = false;
        if(oldContact.Id != NULL && newContactID != NULL && oldContact.Update_Type__c != NULL &&  oldContactStatus != '-- Select Status --'){
            try{
                updateOldContact();
                updateNewContact();
                SendEmail();
                
                
            }catch(exception e){
                hasErrors = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Could Not Process Change of Contact - Error Processing Updates'));
            }
        }
        else{ 
            hasErrors = true;
            System.debug('missing fields');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Could Not Process Change of Contact - Missing required fields'));
        }
    }
    
    
    
    public PageReference cancel(){
        PageReference pr = Page.contact_detail;
        pr.getParameters().put('id', oldContact.Id);
        pr.setRedirect(true);
        return pr;
    }
    
}