trigger salesPerformanceOrderTrigger on Sales_Performance_Order__c (after update, after insert) {
    if (trigger.isAfter) {
        // AFTER UPDATE
        if (trigger.isUpdate) {
           salesPerformanceOrderTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
         // AFTER INSERT
        if (trigger.isInsert) {
           salesPerformanceOrderTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
    }
}