public with sharing class adminToolUpdateRequest {
    public Case cse {get;set;}
    public Case_Notes__c cseNote {get;set;}
    public Account a {get;set;}
    public Contact mainAdmin {get;set;}
    public Contact c {get;set;}
    public string record {get;set;}
    
    public adminToolUpdateRequest() {
        String objectType = ApexPages.currentPage().getParameters().get('type'); 
        Id accountID = ApexPages.currentPage().getParameters().get('accountID');
        Id contactID = ApexPages.currentPage().getParameters().get('contactID');
        if(objectType != null){
            if(objectType == 'a'){
                record ='Account';  
            } else {
                record ='Contact';
            }
        }    
        if ( accountId != null ) {
            a = [SELECT Id, Name, Phone, BillingStreet, BillingCity, BillingState, BillingPostalCode, NAICS_Code__c 
                FROM Account WHERE Id = :accountID];
            mainAdmin = [select ID from Contact where AccountID =:a.Id limit 1];        
            this.cse = new Case();                        
        }
        if ( contactId != null ) {
            c = [SELECT Id, Name, Email, Phone, AccountID, Account.Name FROM Contact WHERE Id = :contactId ];    
            this.cse = new Case();
        }
        this.cseNote = new Case_Notes__c(); 
    }
    
    public List<SelectOption> getNAICS(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Account.NAICS_Code__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('--None--','--None--'));
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
            }       
    return options;
        }
    
    //*Create the case with the user supplied information*//
    public PageReference createCase() {
        //Fetching the assignment rules on case
        AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;        
        if ( a != null && record =='Account' ) {     
                       
            this.cse.AccountID = a.Id;
            this.cse.ContactID = mainAdmin.Id;
            this.cse.Type = 'CC Support';
            this.cse.Product_Support_Issue_Locations__c = 'Account Update';
            this.cse.Status = 'Active';
            this.cse.Subject = 'Account Change Update';
            this.cse.Description = 
            'Name: ' + cse.Account_Change_Name__c + '\r\n' + 
            'Phone: ' +cse.Account_Change_Phone__c + '\r\n' + 
            'Street: ' + cse.Account_Change_Street__c +'\r\n' + 
            'City: ' + cse.Account_Change_City__c + '\r\n' + 
            'State: ' + cse.Account_Change_State__c +'\r\n' + 
            'Postal Code: ' + cse.Account_Change_Postal_Code__c + '\r\n' + 
            'NAICS Code: ' + cse.Account_Change_NAICS__c;
            this.cse.setOptions(dmlOpts);
            insert this.cse;          
            
            if(cseNote.Issue__c != null){
            this.cseNote.Case__c = this.cse.Id;
            this.cseNote.Notes_Type__c = 'Update';
            this.cseNote.Issue__c = cseNote.Issue__c;
            insert this.cseNote;
            }
           return null;
        }
        if ( c != null && record =='Contact') {     
                       
            this.cse.AccountID = c.AccountId;
            this.cse.ContactID = c.Id;
            this.cse.Type = 'CC Support';
            this.cse.Product_Support_Issue_Locations__c = 'Account Update';
            this.cse.Status = 'Active';
            this.cse.Subject = 'Contact Change Update';
            this.cse.Description = 'Name: ' + cse.Contact_Change_Name__c + '\r\n' + 'Email: ' +cse.Contact_Change_Email__c +'\r\n' + 'Phone: ' + cse.Contact_Change_Phone__c;           
            this.cse.setOptions(dmlOpts);
            insert this.cse;
            
            if(cseNote.Issue__c != null){
            this.cseNote.Case__c = this.cse.Id;
            this.cseNote.Notes_Type__c = 'Update';
            this.cseNote.Issue__c = cseNote.Issue__c;
            insert this.cseNote;
            }
            return null;
        }
        return null;
        }
        
        
    
    //*Cancel Button*//
      public PageReference cancel(){
        if(record =='Account'){
        PageReference account_detail = Page.account_detail;
        account_detail.setRedirect(true);
        account_detail.getParameters().put('ID',a.Id);
        return account_detail;
        }
        else{
        PageReference contact_detail = Page.contact_detail;
        contact_detail.setRedirect(true);
        contact_detail.getParameters().put('ID',c.Id);
        return contact_detail;
        }
    }

}