public class contractLineItemUtility {
    
    public static void formatProductTypes(Contract_Line_Item__c[] contractLineItems){
        for(Contract_Line_Item__c cli:contractLineItems){
            //Create string to hold product types - default to product_types__c
            string productTypes = cli.Product_Types__c;
            
            //Create string to hold formatted product types
            string productTypesFormatted;
            
            //Replace 'Custom' with the value entered during quote creation
            if(productTypes.contains('Custom')){
                productTypes = productTypes.replace('Custom', cli.Product_Type_Custom__c);
            }
            
            integer currentProductType = 0;
            for(String s:productTypes.split(';')){
                currentProductType++;
                if(productTypesFormatted == null){
                    productTypesFormatted = s;
                }else if(currentProductType < productTypes.split(';').size()){
                    productTypesFormatted += ', '+s;
                }else{
                    productTypesFormatted += ' and '+s;
                }
            }
            cli.Product_Types_Formatted__c = productTypesFormatted;
        }
    }
    
}