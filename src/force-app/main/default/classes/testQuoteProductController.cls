@isTest(seeAllData=true)
private class testQuoteProductController {
    
    static testMethod void quoteProductController_test1() {
        //insert test account
        Account a = new Account(Name='Test');
        insert a;
        
        //insert test contact
        Contact c = new Contact(AccountId=a.id, LastName='Test');
        insert c;
        
        //insert test option package
        Quote_Options__c qop = new Quote_Options__c(Type__c='New');
        insert qop;
        
        Quote__c[] options = new List<Quote__c>();
        for (integer i=0; i<2; i++) {
            Quote__c q = new Quote__c();
            q.Account__c = a.id;
            q.Contact__c = c.id;
            q.Option_Name__c = 'Test '+i;
            q.Quote_Options__c = qop.id;
            q.Contract_Length__c = '3 Years';
            options.add(q);
        }
        insert options;
        
        Quote_Product__c[] qps = new List<Quote_Product__c>();
        for (integer i=0; i<2; i++) {
            Quote_Product__c qp = new Quote_Product__c();
            qp.Quantity__c = 1;
            qp.Quote__c = options[i].id;
            if (i == 0) {
                qp.Name__c = 'Authoring - MSDS Authoring';
                qp.Product__c = [SELECT id FROM Product2 WHERE Name = 'Authoring - MSDS Authoring' LIMIT 1].id;
                qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - MSDS Authoring' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
            } else {
                qp.Name__c = 'HQ Account';
                qp.Product__c = [SELECT id FROM Product2 WHERE Name = 'HQ Account' LIMIT 1].id;
                qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'HQ Account' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
                qp.Y1_Quote_Price__c = 2.50;
                qp.Approval_Required_by__c = 'Sales VP';
            }
            qps.add(qp); 
        }
        insert qps;
        
        ApexPages.currentPage().getParameters().put('quote_id', options[0].id);
        ApexPages.currentPage().getParameters().put('product_id', qps[0].id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteProductController con = new quoteProductController(testController);
        
        con.closeEdit();
        con.showEdit();
        con.saveChanges();
        con.cancelChanges();
        con.viewQuote();
    }
    
    static testMethod void quoteProductController_authoringPricing() {
        
        //insert test account
        Account a = new Account(Name='Test');
        insert a;
        
        //insert test contact
        Contact c = new Contact(AccountId=a.id, LastName='Test');
        insert c;
        
        Quote__c q = new Quote__c();
        q.Account__c = a.id;
        q.Contact__c = c.id;
        q.Contract_Length__c = '1 Year';
        insert q;
        
        Quote_Product__c[] qps = new List<Quote_Product__c>();
        for (integer i=0; i<8; i++) {
            Quote_Product__c qp = new Quote_Product__c();
            qp.Quantity__c = 2;
            qp.Quote__c = q.id;
            if (i == 0) {
                qp.Name__c = 'Authoring - MSDS Re-Authoring';
                qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'Authoring - MSDS Re-Authoring' LIMIT 1].id;
                qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - MSDS Re-Authoring' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
                qp.Auth_Regulatory_Format_Docs__c = 'OTHER,100,2,3;MALAY GHS,24,2,4';
                qp.Auth_Start_Date__c = date.today();
            } 
            if (i == 1) {
                qp.Name__c = 'Authoring - Translation Services (Other)';
                qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'Authoring - Translation Services (Other)' LIMIT 1].id;
                qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - Translation Services (Other)' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
                qp.Auth_Language_Docs__c = 'Basa,1;Celtic,1';
            }
            if (i == 2) {
                qp.Name__c = 'Authoring - Translation Services (Other)';
                qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'Authoring - Translation Services (Other)' LIMIT 1].id;
                qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - Translation Services (Other)' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
            }
            if (i == 3) {
                qp.Name__c = 'Authoring - RC - Article Assessments';
                qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'Authoring - RC - Article Assessments' LIMIT 1].id;
                qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - RC - Article Assessments' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
                qp.Authoring_Locations__c = 'North America;Bosnia;Poland';
            }
            if (i == 4) {
                qp.Name__c = 'Authoring - RC - Compliance Review';
                qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'Authoring - RC - Compliance Review' LIMIT 1].id;
                qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - RC - Compliance Review' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
            }
            if (i == 5) {
                qp.Name__c = 'Authoring - Post-Finalization Revisions';
                qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'Authoring - Post-Finalization Revisions' LIMIT 1].id;
                qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - Post-Finalization Revisions' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
            }
            if (i == 6) {
                qp.Name__c = 'Authoring - Custom Template';
                qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'Authoring - Custom Template' LIMIT 1].id;
                qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - Custom Template' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
            }
            if (i == 7) {
                qp.Name__c = 'Authoring - Label Authoring';
                qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'Authoring - Label Authoring' LIMIT 1].id;
                qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - Label Authoring' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
            }
            qps.add(qp); 
        }
        insert qps;
        
        //Product 1
        ApexPages.currentPage().getParameters().put('quote_id', q.id);
        ApexPages.currentPage().getParameters().put('product_id', qps[0].id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteProductController con = new quoteProductController(testController);
        
        con.getAuthRegFormats();
        
        ApexPages.currentPage().getParameters().put('quoteProductId', qps[0].id);
        ApexPages.currentPage().getParameters().put('quoteProductName', qps[0].Name);
        con.addAuthRegFormat();
        con.removeAuthRegFormat();
        
        ApexPages.currentPage().getParameters().put('numDocs','2');
        ApexPages.currentPage().getParameters().put('pName', [Select id,name from Quote_Product__c where id =:qps[0].id].Name);
        con.calculateAuthEndDate();
        
        con.saveChanges();
        
        //Product 2
        ApexPages.currentPage().getParameters().put('quote_id', q.id);
        ApexPages.currentPage().getParameters().put('product_id', qps[1].id);
        testController = new ApexPages.StandardController(new Quote__c());
        con = new quoteProductController(testController);
        
        con.getAuthLanguages();
        
        ApexPages.currentPage().getParameters().put('quoteProductId', qps[1].id);
        ApexPages.currentPage().getParameters().put('quoteProductName', qps[1].Name);
        con.addAuthLanguage();
        con.removeAuthLanguage();
        
        con.saveChanges();
        
        //Product 3
        ApexPages.currentPage().getParameters().put('quote_id', q.id);
        ApexPages.currentPage().getParameters().put('product_id', qps[2].id);
        testController = new ApexPages.StandardController(new Quote__c());
        con = new quoteProductController(testController);
        
        ApexPages.currentPage().getParameters().put('quoteProductId', qps[2].id);
        ApexPages.currentPage().getParameters().put('quoteProductName', qps[2].Name);
        
        con.saveChanges();
        
        //Product 4
        ApexPages.currentPage().getParameters().put('quote_id', q.id);
        ApexPages.currentPage().getParameters().put('product_id', qps[3].id);
        testController = new ApexPages.StandardController(new Quote__c());
        con = new quoteProductController(testController);
        
        ApexPages.currentPage().getParameters().put('quoteProductId', qps[3].id);
        ApexPages.currentPage().getParameters().put('quoteProductName', qps[3].Name);
        
        con.saveChanges();
        
        //Product 5
        ApexPages.currentPage().getParameters().put('quote_id', q.id);
        ApexPages.currentPage().getParameters().put('product_id', qps[4].id);
        testController = new ApexPages.StandardController(new Quote__c());
        con = new quoteProductController(testController);
        
        ApexPages.currentPage().getParameters().put('quoteProductId', qps[4].id);
        ApexPages.currentPage().getParameters().put('quoteProductName', qps[4].Name);
        
        con.saveChanges();
        
        //Product 6
        ApexPages.currentPage().getParameters().put('quote_id', q.id);
        ApexPages.currentPage().getParameters().put('product_id', qps[5].id);
        testController = new ApexPages.StandardController(new Quote__c());
        con = new quoteProductController(testController);
        
        ApexPages.currentPage().getParameters().put('quoteProductId', qps[5].id);
        ApexPages.currentPage().getParameters().put('quoteProductName', qps[5].Name);
        
        con.saveChanges();
        
        //Product 7
        ApexPages.currentPage().getParameters().put('quote_id', q.id);
        ApexPages.currentPage().getParameters().put('product_id', qps[6].id);
        testController = new ApexPages.StandardController(new Quote__c());
        con = new quoteProductController(testController);
        
        ApexPages.currentPage().getParameters().put('quoteProductId', qps[6].id);
        ApexPages.currentPage().getParameters().put('quoteProductName', qps[6].Name);
        
        con.saveChanges();
        
        //Product 8
        ApexPages.currentPage().getParameters().put('quote_id', q.id);
        ApexPages.currentPage().getParameters().put('product_id', qps[7].id);
        testController = new ApexPages.StandardController(new Quote__c());
        con = new quoteProductController(testController);
        
        ApexPages.currentPage().getParameters().put('quoteProductId', qps[7].id);
        ApexPages.currentPage().getParameters().put('quoteProductName', qps[7].Name);
        
        con.saveChanges();
    }
    
    static testMethod void quoteProductController_onSite() {
        
        //insert test account
        Account a = new Account(Name='Test');
        insert a;
        
        //insert test contact
        Contact c = new Contact(AccountId=a.id, LastName='Test');
        insert c;
        
        Quote__c q = new Quote__c();
        q.Account__c = a.id;
        q.Contact__c = c.id;
        q.Contract_Length__c = '1 Year';
        insert q;
        
        Quote_Product__c qp = new Quote_Product__c();
        qp.Quantity__c = 2;
        qp.Quote__c = q.id;
        qp.Name__c = 'On-Site Chemical Inventory';
        qp.Product__c = [SELECT id, Quote_Terms_Language__c FROM Product2 WHERE Name = 'On-Site Chemical Inventory' LIMIT 1].id;
        qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'On-Site Chemical Inventory' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        qp.Onsite_Location_Data__c = '{"locations":[{"sqFootage":3,"rowNumber":1,"physicalAddress":"asd","numOfProducts":3,"nameOfContact":"asd","escortAvailable":true,"contactNumberEmail":"asd","closestAirport":"asd","addlComments":"asd"},{"sqFootage":0,"rowNumber":2,"physicalAddress":"asdasd","numOfProducts":0,"nameOfContact":"","escortAvailable":false,"contactNumberEmail":"","closestAirport":"","addlComments":""}]}';
        qp.Onsite_Protective_Equipment__c = 'a';
        qp.Onsite_Shots_Vaccinations__c = 'a';
        qp.Onsite_Specific_Safety_Training__c = 'a';
        qp.Onsite_Timeline__c = 'a';
        qp.Onsite_Vendormate_Registration__c = 'a';
        qp.Onsite_Hourly_Rate__c = 1;
        qp.Onsite_Maximum_Hours__c = '1';
        qp.Onsite_Maximum_Travel_Expense__c = 1;
        insert qp;
        
        
        ApexPages.currentPage().getParameters().put('quote_id', q.id);
        ApexPages.currentPage().getParameters().put('product_id', qp.id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteProductController con = new quoteProductController(testController);
        
        con.addOnsiteLocation();
        ApexPages.currentPage().getParameters().put('rowNum', string.valueof(3));
        con.removeOnsiteLocation();
        con.saveChanges();
        con.attachOnsite();
        con.showHideOnsiteTable();
    }
    
    static testMethod void quoteProductController_testSplitLocations() {
        //insert test account
        Account a = new Account(Name='Test');
        a.Active_MSDS_Management_Products__c = 'HQ';
        insert a;
        
        //insert test contact
        Contact c = new Contact(AccountId=a.id, LastName='Test');
        insert c;
        
        Quote__c q = new Quote__c();
        q.Account__c = a.id;
        q.Contact__c = c.id;
        q.Contract_Length__c = '3 Years';
        insert q;
        
        Quote_Product__c qp = new Quote_Product__c();
        qp.Quantity__c = 1;
        qp.Quote__c = q.id;
        qp.Name__c = 'Custom Print Job';
        qp.Product__c = [SELECT id FROM Product2 WHERE Name = 'Custom Print Job' LIMIT 1].id;
        qp.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Custom Print Job' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        qp.PB_Location_Names__c = 'TestA,TestB,TestC';
        insert qp;
        
        ApexPages.currentPage().getParameters().put('quote_id', q.id);
        ApexPages.currentPage().getParameters().put('product_id', qp.id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteProductController con = new quoteProductController(testController);
        
        
        con.splitLocations();
        con.theProduct.PB_Locations_SDSs_String__c = 'TestA,5;TestB,6;';
        con.splitLocations();
        con.theProduct.PB_Location_Names__c = '';
        con.splitLocations();
        ApexPages.currentPage().getParameters().put('delIndx', String.ValueOf(1));
        con.delLocation();
        con.addRows();
    }
    
}