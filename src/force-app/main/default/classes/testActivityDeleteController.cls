@isTest
private class testActivityDeleteController {
    
    //Test 1 Default - Permissions set so user cannot delete task.  Check to confirm Task is not deleted. Return Url provided.
    @isTest static void test1(){
        User u =[Select ID from User where LastName='McCauley' and isActive = TRUE limit 1];
        
        Account acct = new Account(name='TestAccount');
        insert acct; 
        
        Task t = new Task(Subject='Task1', Status = 'Complete', Priority='Normal');
        insert t;
        
        MSDS_Custom_Settings__c cSettings = new MSDS_Custom_Settings__c (SetupOwnerId = u.Id);   //cSettings.setupOwnerId = u.Id;
        insert cSettings;
        
        system.runAs(u){
            
            ApexPages.currentPage().getParameters().put('delID', t.Id );
            ApexPages.currentPage().getParameters().put('retURL', acct.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(t);  
            activityDeleteController myController = new activityDeleteController(testController);
            System.debug('user '+u.Id);
            
            //Default - no permissions
            myController.deleteRedirect();
            Task delTaskNo = [SELECT Id, IsDeleted FROM TASK WHERE Id = :t.Id ALL ROWS];
            //System.debug(delTaskNo);
            System.assertEquals(delTaskNo.IsDeleted, false);
            
            //Test BackButton Url Provided
            myController.backButton();
        }
    }        
    
    //Test 2 Default - Permissions set so user can delete task.  Check to confirm Task is deleted. Return Url is provided.
    @isTest static void test2(){
        User u =[Select ID from User where LastName='McCauley' and isActive = TRUE limit 1];
        Account acct = new Account(name='TestAccount');
        insert acct; 
        
        Task t = new Task(Subject='Task1', Status = 'Complete', Priority='Normal');
        insert t;
        
        MSDS_Custom_Settings__c cSettings = new MSDS_Custom_Settings__c (SetupOwnerId = u.Id, Can_delete_tasks__C = true);   //cSettings.setupOwnerId = u.Id;
        insert cSettings;
        system.runAs(u){
            
            ApexPages.currentPage().getParameters().put('delID', t.Id );
            ApexPages.currentPage().getParameters().put('retURL', acct.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(t);  
            activityDeleteController myController = new activityDeleteController(testController);
            System.debug('user '+u.Id);
            
            myController.deleteRedirect();
            Task delTaskYes = [SELECT Id, IsDeleted FROM TASK WHERE Id = :t.Id ALL ROWS];
            System.assertEquals(delTaskYes.IsDeleted, true);
        }
    } 
    
    //Test 3 Default - Permissions set so user cannot delete event.  Check to confirm Event is not deleted. Return Url is NOT provided.
    @isTest static void test3(){
        User u =[Select ID from User where LastName='McCauley' and isActive = TRUE limit 1];
        Account acct = new Account(name='TestAccount');
        insert acct; 
        
        Contact c = new Contact(FirstName ='Tara', LastName='Contact', accountId=acct.id);
        insert c;
        
        Event e = new Event(Subject='Repeat Demo', StartDateTime=datetime.now(), EndDateTime=datetime.now()+1, WhoID=c.id, OwnerId = u.id);  // //contact is the whoId
        insert e;
        
        MSDS_Custom_Settings__c cSettings = new MSDS_Custom_Settings__c (SetupOwnerId = u.Id);   //cSettings.setupOwnerId = u.Id;
        insert cSettings;
        system.runAs(u){
            
            ApexPages.currentPage().getParameters().put('delID', e.Id );
            //ApexPages.currentPage().getParameters().put('retURL', acct.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(e);  
            activityDeleteController myController = new activityDeleteController(testController);
            System.debug('user '+u.Id);
            
            myController.deleteRedirect();
            Event delEventNo = [SELECT Id, IsDeleted FROM EVENT WHERE Id = :e.Id ALL ROWS];
            System.assertEquals(delEventNo.IsDeleted, false);
            
            //Test Button with no URL
            myController.backButton();
        }
    }   
    
    //Test 4 Permissions set so user can delete event.  Check to confirm Event is deleted. Return Url is NOT provided.
    @isTest static void test4(){
        User u =[Select ID from User where LastName='McCauley' and isActive = TRUE limit 1];
        Account acct = new Account(name='TestAccount');
        insert acct; 
        
        Contact c = new Contact(FirstName ='Tara', LastName='Contact', accountId=acct.id);
        insert c;
        
        Event e = new Event(Subject='Repeat Demo', StartDateTime=datetime.now(), EndDateTime=datetime.now()+1, WhoID=c.id, OwnerId = u.id);  // //contact is the whoId
        insert e;
        
        MSDS_Custom_Settings__c cSettings = new MSDS_Custom_Settings__c (SetupOwnerId = u.Id, Can_delete_events__C = true);   //cSettings.setupOwnerId = u.Id;
        insert cSettings;
        system.runAs(u){
            
            ApexPages.currentPage().getParameters().put('delID', e.Id );
            //ApexPages.currentPage().getParameters().put('retURL', acct.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(e);  
            activityDeleteController myController = new activityDeleteController(testController);
            System.debug('user '+u.Id);
            
            myController.deleteRedirect();
            Event delEventYes = [SELECT Id, IsDeleted FROM EVENT WHERE Id = :e.Id ALL ROWS];
            System.assertEquals(delEventYes.IsDeleted, true);
        }
    }
    
}