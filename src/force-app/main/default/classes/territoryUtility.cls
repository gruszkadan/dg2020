public class territoryUtility {
    public static void updateTerritory(list<sObject> records, string sObjectType, boolean updateOwners, string action) {
        sObjectField postalCodeField;
        sObjectField countryField;
        sObjectField territoryField;
        sObjectField countyField;
        sObjectField stateField;
        sObjectField inferredZipField;
        if (sObjectType == 'Account') {
            postalCodeField = Account.BillingPostalCode;
            countryField = Account.BillingCountry;
            territoryField = Account.Territory__c;
            countyField = Account.Billing_County__c;
            stateField = Account.BillingState;
            inferredZipField = null;

            // turn off v-rules
            User u = [SELECT id, Validation_Rules_Active__c, Group__c FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
            u.Validation_Rules_Active__c = false;
            update u;
        } else if (sObjectType == 'Lead') {
            postalCodeField = Lead.PostalCode;
            countryField = Lead.Country;
            territoryField = Lead.Territory__c;
            countyField = Lead.County__c;
            stateField = Lead.State;
            inferredZipField = Lead.mkto71_Inferred_Postal_Code__c;
        }
        set<string> zipCodes = new set<string>();
        for (sObject r : records) {
            string postalCode;
            if ((string)r.get(postalCodeField) != null) {
                postalCode = (string)r.get(postalCodeField);
            } else if (sObjectType == 'Lead' && (string)r.get(inferredZipField) != null) {
                postalCode = (string)r.get(inferredZipField);
            }
            string country = (string)r.get(countryField);
            if (postalCode != null) {
                if (country == 'United States' || country == null) {
                    zipCodes.add(postalCode.left(5));
                } else if (country == 'Canada') {
                    string newZip = '';
                    string oldZip = postalCode.toUpperCase();
                    integer zipLength = oldZip.length();
                    if (zipLength == 3) {
                        newZip = oldZip;
                        postalCode = newZip;
                        zipCodes.add(newZip);
                    } else {
                        newZip = oldZip.left(3)+' '+oldZip.right(3);
                        postalCode = newZip;
                        zipCodes.add(newZip);
                        zipCodes.add(newZip.left(3));
                    } 
                }
                r.put(postalCodeField, postalCode);
            }
        }
        map<string, County_Zip_Code__c> zipCodeMap = new map<string, County_Zip_Code__c>();
        if (zipCodes.size() > 0) {
            for (County_Zip_Code__c zipCode : [SELECT id, Territory__c, Country__c, State__c, Territory__r.Authoring_Owner__c, Territory__r.SLED_Owner__c, Territory__r.Online_Training_Owner__c, Territory__r.Territory_Owner__c, Zip_Code__c, County__c,
                                               Territory__r.EHS_Owner__c, Territory__r.Ergonomics_Owner__c
                                               FROM County_Zip_Code__c 
                                               WHERE Zip_Code__c IN : zipCodes]) {
                                                   zipCodeMap.put(zipCode.Zip_Code__c, zipCode);
                                               }
            for (sObject r : records) {
                string postalCode = (string)r.get(postalCodeField);
                string country = (string)r.get(countryField);
                County_Zip_Code__c zipCode = null;
                if (country == 'Canada' && postalCode != null) {
                    if (zipCodeMap.get(postalCode) != null) {
                        zipCode = zipCodeMap.get(postalCode);
                    } else {
                        zipCode = zipCodeMap.get(postalCode.left(3));
                    }
                } else {
                    zipCode = zipCodeMap.get(postalCode.left(5));
                }
                if (zipCode != null) {
                    r.put(countyField, zipCode.County__c);
                    r.put(territoryField, zipCode.Territory__c);
                    r.put(stateField, zipCode.State__c);
                    r.put(countryField, zipCode.Country__c);
                }  
            }
        }
        if (updateOwners) {
            ownershipUtility.updateOwnership(records, zipCodeMap, sObjectType, action);
        }
    }
}