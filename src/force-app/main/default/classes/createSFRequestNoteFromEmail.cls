global class createSFRequestNoteFromEmail implements Messaging.InboundEmailHandler 
{
    
    global Messaging.InboundEmailResult  handleInboundEmail(Messaging.inboundEmail email, 
                                                            Messaging.InboundEnvelope env)
    {
        
        // Create an InboundEmailResult object for returning the result of the 
        // Apex Email Service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        String myPlainText= '';
        String emailSubject= '';
        String senderEmail='';
        String senderEHSEmail='';
        String senderMSDSEmail='';
        String senderKMIEmail='';
        String senderEHSUsername='';
        String senderMSDSUsername='';
        String senderKMIUsername='';
        string environment;
        string sandboxName;
        if(UserInfo.getUserName().substringAfterLast('.com')!= null){
            sandboxName = UserInfo.getUserName().substringAfterLast('.');
        } else {
            sandboxName = NULL;
        }
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D300000001HEfEAM'){
            environment = 'Production';
        } else {
            environment ='Sandbox';
        }
        // Store the email plain text into the local variable 
        myPlainText = email.plainTextBody;
        senderEmail = email.fromAddress;
        if(email.subject.startsWith('RE:')){
            emailSubject = email.subject.removeStart('RE: New Salesforce Request Note - ');
        } else {
            emailSubject = email.subject.removeStart('FW: New Salesforce Request Note - ');
        }
        if(senderEmail.containsIgnoreCase('@ehs.com')){
            senderEHSEmail = senderEmail;
        }
        senderMSDSEmail = senderEmail.replace('@ehs.com', '@msdsonline.com');
        senderKMIEmail = senderEmail.replace('@ehs.com', '@kminnovations.com');
        senderEHSUsername = senderEHSEmail;
        senderMSDSUsername = senderMSDSEmail;
        senderKMIUsername = senderKMIEmail;
        if(environment =='Sandbox') {
            senderEHSUsername = senderEHSEmail +'.'+ sandboxName;
            senderMSDSUsername = senderMSDSEmail +'.'+ sandboxName;
            senderKMIUsername = senderKMIEmail +'.'+ sandboxName; 
        }
        User[] sender;
        ID createdBy;
        if(senderEmail != NULL){
            sender = [Select ID, Email from User where (
                UserName = :senderEHSUsername OR
                UserName = :senderMSDSUsername OR
                UserName = :senderKMIUsername ) AND (
                    Email = :senderEHSEmail OR
                    Email = :senderMSDSEmail OR
                    Email = :senderKMIEmail) LIMIT 1];
        } 
        if(sender.size() ==1){
            createdBy = sender[0].Id;
        } else {
            createdBy = '00580000003UChI';
        }
        // Create a new Request Note object 
        Salesforce_Request_Note__c[] newNote = new Salesforce_Request_Note__c[0];
        
        // Try to look up any requests based on the subject.
        try 
        {
            Salesforce_Request__c vSFReq = [SELECT Id, Name FROM Salesforce_Request__c WHERE Name =:emailSubject LIMIT 1];   
            // Add a new Note to the request record we just found above.
            newNote.add(new Salesforce_Request_Note__c(
                Comments__c =  myPlainText,
                CreatedbyID = createdBy,
                Salesforce_Update__c =  vSFReq.Id));
            
            // Insert the new Note 
            insert newNote;    
            
            System.debug('New Note Object: ' + vSFReq.name+vSFReq.id);   
        }
        // If an exception occurs when the query accesses 
        // the request record, a QueryException is thrown.
        // The exception is written to the Apex debug log.
        catch (QueryException e) {
            System.debug('Query Issue: ' + e);
        }
        
        // Set the result to true. No need to send an email back to the user 
        // with an error message
        result.success = true;
        
        // Return the result for the Apex Email Service
        return result;
    }
}