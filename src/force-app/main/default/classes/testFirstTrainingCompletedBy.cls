@isTest(SeeAllData=true)
public with sharing class testFirstTrainingCompletedBy { 
    static testMethod void test() { 
        
        Account newAccount = new Account();
    	newAccount.Name = 'Test Account';
    	newAccount.AdminID__c = '123456';
    	newAccount.Customer_Status__c ='Active';
    	insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'IMP: HQ';
        newCase.Subject = 'Test';
        newCase.OwnerID = '00580000003UChI';  
        insert newCase;
        
        Case_Notes__c newNote = new Case_Notes__c();
        newNote.Case__c = newCase.ID;
        newNote.Notes_Type__c = 'Training';
        newNote.Call_Duration__c = 10;
        newNote.Customer_Contacted__c = TRUE;
        newNote.Product__c = 'HQ Account';
        insert newNote;
   
        Case_Notes__c note = [select ID, CreatedDate, CreatedByID from Case_Notes__c where ID = :newNote.Id];
        Case c =[select First_Training_Completed_By__c from Case where ID= :NewCase.Id]; 
        System.assertEquals(note.CreatedByID,c.First_Training_Completed_By__c); 
}
}