@isTest()
public class testVPMTaskTriggerHandler {
    static testMethod void test1() {
        
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        
        VPM_Project__c project = new VPM_Project__c(Account__c = newAcct.Id,	Project_Status__c = 'Open' , Type__c = 'Chemical Management Upgrade' );
        insert project;
        
        VPM_Milestone__c milestone = new VPM_Milestone__c(Account__c = newAcct.Id, Status__c = 'Open', VPM_Project__c = project.Id );
        insert milestone;
        
        VPM_task__c task = new VPM_task__c(Status__c = 'Complete', VPM_Project__c = project.Id, VPM_Milestone__c = milestone.Id, Contact__c = newCon.Id, Start_Date__c = System.today(), Subject__c = 'Training' );
        insert task;
        
        VPM_Project__c checkProject = [Select Project_Status__c from VPM_Project__c where Id = :project.Id ];
        System.assert(checkProject.Project_Status__c == 'Closed');
        
        task.Status__c = 'In Progress';
        update task;
        
        VPM_Milestone__c checkStatus = [Select Status__c, Milestone_End_Date__c, VPM_Project__r.Project_Status__c from VPM_Milestone__c where Id = :milestone.Id ];
        System.assert(checkStatus.Status__c == 'Open');
        System.assert(checkStatus.Milestone_End_Date__c == NULL);
        System.assert(checkStatus.VPM_Project__r.Project_Status__c == 'Open');
        
        //milestone.Status__c = 'Closed';
        //update milestone;
        //VPM_Milestone__c checkEndDate = [Select Milestone_End_Date__c, VPM_Project__r.Project_End_Date__c from VPM_Milestone__c where Id = :milestone.Id ];
        //System.assert(checkEndDate.Milestone_End_Date__c != NULL);
        //System.assert(checkEndDate.VPM_Project__r.Project_End_Date__c != NULL);
        
        delete task;

    }
}