public class productUtility {
    
    //returns semi-colon separated string of product suites based on Product2 Ids that are passed in
    public static String findProductSuites(Id[] prodIds){
        
        String productSuites = '';
        
        Set<String> prodSuites = new Set<String>();
        
        //convert list of Product Ids into set to dedup
        Set<id> productIds = new Set<id>(prodIds);
        
        //create list of product_suite__c fields
        List<Product2> products = [SELECT Product_Suite__c FROM Product2 WHERE id IN :productIds];
        
        //add product_suite__c fields to set to dedup
        for(Product2 p : products){
            if(p.Product_Suite__c == null){
                prodSuites.add('null');
            } else{
                prodSuites.add(p.Product_Suite__c);    
            }
            
        }
        
        //create semi-colon separated string of product_suite__c fields to return
        for(String p : prodSuites){
            productSuites += p + ';';
        }
        
        return productSuites;
        
    }
    
    
    
    
}