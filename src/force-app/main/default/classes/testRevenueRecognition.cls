@isTest(SeeAllData=false)
public class testRevenueRecognition {

    static testMethod void Test1() {

        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;

        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;

        id caseOwner = [SELECT id FROM User WHERE LastName = 'Wills' LIMIT 1].id; 

        Case[] casesToInsert = new List<Case>();
        for(integer i = 0; i<10; i++){
            Case newCase = new Case();
            newCase.OwnerID = caseOwner;
            newCase.AccountId = newAcct.Id;
            newCase.ContactId = newCon.Id;
            newCase.Status = 'Active';
            newCase.Subject = 'Test ' + i;
            newCase.Type = 'Compliance Services';
            newCase.Primary_project_type__c = 'BVA';
            newCase.Total_Project_Amount__c = 9.99 + i;
            casesToInsert.add(newCase);
        }

        //Upon insertion of cases witht he fields above not set to null, a rev rec and transactions will be created.
        insert casesToInsert;
        
        Revenue_Recognition__c[] revRecs = [SELECT id, project_status__c, (SELECT id,Expected_Date__c,Number_of_Days_Forecast__c,Number_of_Days_Forecasted_in_Status__c,Revenue_Recognition__c,Force_Subsequent_Expected_Date_Change__c,Recognition_Date__c,Sequence_Number__c,Recognition_Project_Status__c
                                                                           FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                            FROM Revenue_Recognition__c
                                            WHERE case__c in:casesToInsert];  
        System.assert(revRecs.size() == 10);
        
        for(Revenue_Recognition__c rr: revRecs){
            System.assert(rr.Revenue_Recognition_Transactions__r.size() > 0);
            rr.Project_Status__c = 'CF - Scheduling Initial Call';
        }
        //Upon a change of the project status on the rev rec, the transaction of the new status will be recognized
        update revRecs;
        for(Case newCase :casesToInsert){
            newCase.Project_Status__c = 'CF - Scheduling Initial Call';
        }
        update casesToInsert;
        Revenue_Recognition__c[] updatedRevRecs = [SELECT id, project_status__c, (SELECT id,Expected_Date__c,Number_of_Days_Forecast__c,Number_of_Days_Forecasted_in_Status__c,Revenue_Recognition__c,Force_Subsequent_Expected_Date_Change__c,Recognition_Date__c,Sequence_Number__c,Recognition_Project_Status__c
                                                                                  FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                                   FROM Revenue_Recognition__c
                                                   WHERE case__c in:casesToInsert];
        
        for(Revenue_Recognition__c updatedRR: updatedRevRecs){
            for(Revenue_Recognition_Transaction__c rrt: updatedRR.Revenue_Recognition_Transactions__r){
                if(rrt.Recognition_Project_Status__c == 'CF - Assigned' || rrt.Recognition_Project_Status__c == 'CF - Scheduling Initial Call'){
                    System.assert(rrt.Recognition_Date__c != NULL);
                }else{
                    System.assert(rrt.Recognition_Date__c == NULL);
                }
            }
        }
        
        //Change status of one Rev Rec back to CF - Assigned (step 1): because step 1 has already been recognized, a new transaction will be created
        updatedRevRecs[0].project_status__c = 'CF - Assigned';
        update updatedRevRecs[0];
        
        //Confirm the new transaction
        Revenue_Recognition_Transaction__c[] firstRevRecsTransactions = [SELECT id,Expected_Date__c,Number_of_Days_Forecast__c,Number_of_Days_Forecasted_in_Status__c,Recognition_Date__c,Sequence_Number__c
                                                                           FROM Revenue_Recognition_Transaction__c WHERE Revenue_Recognition__c = :updatedRevRecs[0].ID and Step__c = 1 ORDER BY Sequence_Number__c ASC];
        System.assert(firstRevRecsTransactions.size() == 2);
        
        //Now lets skip step 3 and go straight to step 5
        updatedRevRecs[1].project_status__c = 'CF - Account Review';
        update updatedRevRecs[1];
        
        //check that step 3 and 4 are not recognized and step 5 is recognized
        Revenue_Recognition_Transaction__c[] secondRevRecsTransactions = [SELECT id,Expected_Date__c,Number_of_Days_Forecast__c,Number_of_Days_Forecasted_in_Status__c,Recognition_Date__c,Step__c, skipped__c
                                                                           FROM Revenue_Recognition_Transaction__c WHERE Revenue_Recognition__c = :updatedRevRecs[1].ID and (Step__c = 3 OR Step__c = 4 OR Step__c = 5) ORDER BY Sequence_Number__c ASC];
        
        for(Revenue_Recognition_Transaction__c rrt: secondRevRecsTransactions){
            if(rrt.Step__c < 5){
                system.assert(rrt.Recognition_Date__c == Null);
                System.assert(rrt.skipped__c);
            }else{
                system.assert(rrt.Recognition_Date__c != Null); 
            }  
        }
        
        //add a status
        Id metadataId = [Select id from Revenue_Recognition_Setting__mdt WHERE Project_Status__c = 'Cancelled'  LIMIT 1].Id;
        Id addBeforeId  = [SELECT id,Expected_Date__c,Number_of_Days_Forecast__c,Recognition_Date__c FROM Revenue_Recognition_Transaction__c WHERE Revenue_Recognition__c = :updatedRevRecs[1].ID and Step__c = 8 LIMIT 1].Id;
        RevenueRecognitionUtility.addStatus(addBeforeId, updatedRevRecs[1].Id, metadataId, System.today().addDays(365));
        Revenue_Recognition_Transaction__c[] AfterStatusAddition  = [SELECT id,Expected_Date__c,Number_of_Days_Forecast__c,Recognition_Date__c, Recognition_Project_Status__c 
                                                                     FROM Revenue_Recognition_Transaction__c 
                                                                     WHERE Revenue_Recognition__c = :updatedRevRecs[1].ID and (Step__c >= 8 OR Step__c = NULL )
                                                                     ORDER BY Sequence_Number__c ASC];
        
        for (integer j ; j < AfterStatusAddition.size(); j++){
            if(j == 0){
                System.assert(AfterStatusAddition[j].Recognition_Project_Status__c == 'Cancelled');
                
            }else{
                if(j == 1){
                    System.assert(AfterStatusAddition[j].Expected_Date__c == System.today().addDays(365));
                }else{
                System.assert(AfterStatusAddition[j].Recognition_Project_Status__c != 'Cancelled');
                System.assert(AfterStatusAddition[j].Expected_Date__c > System.today().addDays(365));
                }
            }
        }
    }
   
    
    
    static testMethod void Test2() {
        
         //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //Find a case owner
        id caseOwner = [SELECT id FROM User WHERE LastName = 'Wills' LIMIT 1].id; 

        Case[] casesToInsert = new List<Case>();
        for(integer i = 0; i<2; i++){
            Case newCase = new Case();
            newCase.OwnerID = caseOwner;
            newCase.AccountId = newAcct.Id;
            newCase.ContactId = newCon.Id;
            newCase.Status = 'Active';
            newCase.Subject = 'Test ' + i;
            newCase.Type = 'Compliance Services';
            newCase.Primary_project_type__c = 'BVA';
            newCase.Total_Project_Amount__c = 1000;
            casesToInsert.add(newCase);
        }

        //Upon insertion of cases witht he fields above not set to null, a rev rec and transactions will be created.
        insert casesToInsert;

        //Adjust case amount and check that rev rec and transactions adjust
    	Revenue_Recognition__c[] RevRecBeforeAmountChange = [SELECT id, project_status__c, phase_amount__c, (SELECT id, Recognition_Project_Status__c, 	Recognition_Amount__c
                                            FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                            FROM Revenue_Recognition__c
                                            WHERE case__c = :casesToInsert[0].Id LIMIT 1];
        
		Map<id , decimal> originalTransactionAmounts = new Map<id, decimal>();
        for(Revenue_Recognition_Transaction__c rrt: RevRecBeforeAmountChange[0].Revenue_Recognition_Transactions__r){
            originalTransactionAmounts.put(rrt.id, rrt.Recognition_Amount__c);
        }
        
        
        casesToInsert[0].Total_Project_Amount__c = casesToInsert[0].Total_Project_Amount__c/2;
        update casesToInsert[0];
        
		Revenue_Recognition__c[] RevRecAfterAmountChange = [SELECT id, project_status__c, phase_amount__c, (SELECT id, Recognition_Project_Status__c, Recognition_Amount__c, Step__c
                                                                           FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                            FROM Revenue_Recognition__c
                                            WHERE case__c = :casesToInsert[0].Id LIMIT 1];
        
       //Confirm changes to Rev Rec
        System.assert(RevRecAfterAmountChange[0].phase_amount__c == 500);
        
        //Confirm changes to transactions
        decimal total = 0;
        for(Revenue_Recognition_Transaction__c newrrt: RevRecAfterAmountChange[0].Revenue_Recognition_Transactions__r){
            total = total + newrrt.Recognition_Amount__c;
            if(newrrt.Step__c > 2){
                //System.debug(newrrt.Recognition_Amount__c +  ' equals ' + originalTransactionAmounts.get(newrrt.Id)/2);
				//System.assert(newrrt.Recognition_Amount__c == originalTransactionAmounts.get(newrrt.Id)/2);
            }
            if(newrrt.Step__c == 1){
                System.assert(newrrt.Recognition_Amount__c == originalTransactionAmounts.get(newrrt.Id));
            }
        }
        System.assert(total == 500);
        
        
        //add a phase of 600 and update the old phase to adjust for the change
        revenueRecognitionUtility.addPhase(casesToInsert[0].Id, RevRecAfterAmountChange, 600);
        RevRecAfterAmountChange[0].Phase_Amount__c = 400;
        update RevRecAfterAmountChange[0];
        
        Revenue_Recognition__c[] RevRecAfterPhaseAddition = [SELECT id, project_status__c, phase_amount__c, Phase_Number__c, (SELECT id, Recognition_Project_Status__c, Recognition_Amount__c, Step__c
                                                                           FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                            FROM Revenue_Recognition__c
                                            WHERE case__c = :casesToInsert[0].Id LIMIT 2];
        
        for(Revenue_Recognition__c rr : RevRecAfterPhaseAddition){
            if(rr.Phase_Number__c == '1'){
                System.assert(rr.phase_amount__c == 400);
                total = 0;
                for(Revenue_Recognition_Transaction__c updatedrrt: rr.Revenue_Recognition_Transactions__r){
                    total = total + updatedrrt.Recognition_Amount__c;  
                }
                System.assert(total == 400);
            }else{
                System.assert(rr.phase_amount__c == 600);
                total = 0;
                for(Revenue_Recognition_Transaction__c updatedrrt: rr.Revenue_Recognition_Transactions__r){
                    total = total + updatedrrt.Recognition_Amount__c;  
                }
                System.assert(total == 600);
            }
        }
    }
    
    
    static testMethod void Test3() {
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //Find a case owner
        id caseOwner = [SELECT id FROM User WHERE LastName = 'Wills' LIMIT 1].id; 
        
        Case newCase = new Case();
        newCase.OwnerID = caseOwner;
        newCase.AccountId = newAcct.Id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Subject = 'Test';
            newCase.Type = 'Compliance Services';
        newCase.Primary_project_type__c = 'BVA';
        newCase.Total_Project_Amount__c = 1000;
        insert newCase;
        
        //Upon insertion of cases witht he fields above not set to null, a rev rec and transactions will be created.

        //Adjust case amount and check that rev rec and transactions adjust
    	Revenue_Recognition__c[] RevRec = [SELECT id, project_status__c, phase_amount__c, (SELECT id, Recognition_Project_Status__c, step__c, Recognition_Amount__c, Expected_Date__c
                                            FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                            FROM Revenue_Recognition__c
                                            WHERE case__c = :newCase.Id LIMIT 1];
        
        //Set the date of step 2 to yesterday
        Revenue_Recognition_Transaction__c[] updateRRT = new List<Revenue_Recognition_Transaction__c>();
        for(Revenue_Recognition_Transaction__c rrt: RevRec[0].Revenue_Recognition_Transactions__r){
            if(rrt.step__c <= 2){
                rrt.Expected_Date__c = System.today().addDays(-1);
                updateRRT.add(rrt);
            }
        }

        update updateRRT;
    	autoAdjustRevRecTransactionDate abc = new autoAdjustRevRecTransactionDate(); 
		abc.execute(null);
        Revenue_Recognition__c[] RevRecAFTERscheduledClass = [SELECT id, project_status__c, phase_amount__c, (SELECT id, Recognition_Project_Status__c, step__c, Recognition_Amount__c, Expected_Date__c
                                            FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                            FROM Revenue_Recognition__c
                                            WHERE case__c = :newCase.Id LIMIT 1];
        
        for(integer j = 0; j < RevRec[0].Revenue_Recognition_Transactions__r.size(); j++){
            if(RevRec[0].Revenue_Recognition_Transactions__r[j].step__c == 1){
                System.assert(RevRec[0].Revenue_Recognition_Transactions__r[j].Expected_Date__c == System.today().addDays(-1));
            }
            if(RevRec[0].Revenue_Recognition_Transactions__r[j].step__c == 2){
                //Test here
            }
            // if(RevRec[0].Revenue_Recognition_Transactions__r[j]. > 2){
            //  Test here   
            //}
        }
        
   }
    
    
    static testMethod void Test4(){
        
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //Find a case owner
        id caseOwner = [SELECT id FROM User WHERE LastName = 'Wills' LIMIT 1].id; 
        
        Case newCase = new Case();
        newCase.OwnerID = caseOwner;
        newCase.AccountId = newAcct.Id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Subject = 'Test';
        newCase.Type = 'Compliance Services';
        insert newCase;
        
        newCase.Project_Status__c = 'CF - Assigned';
        newCase.Total_Project_Amount__c = 1000;
        update newCase;
        
        Revenue_Recognition__c rrts = [ Select Id, (SELECT Id FROM Revenue_Recognition_Transactions__r) From Revenue_Recognition__c WHERE Case__c = :newCase.id ];
        List<Revenue_Recognition_Setting__mdt> revRecSettings = [ Select Id FROM Revenue_Recognition_Setting__mdt WHERE Case_Type__c = 'Compliance Services' AND Primary_Project_Type__c = null   ];
        system.assertEquals(rrts.Revenue_Recognition_Transactions__r.size(), revRecSettings.size());
        
        newCase.Primary_Project_Type__c = 'BVA';
        update newCase;
        
        rrts = [ Select Id, (SELECT Id FROM Revenue_Recognition_Transactions__r) From Revenue_Recognition__c WHERE Case__c = :newCase.id ];
        revRecSettings = [ Select Id FROM Revenue_Recognition_Setting__mdt WHERE Case_Type__c = 'Compliance Services' AND Primary_Project_Type__c = 'BVA'   ];
        system.assertEquals(rrts.Revenue_Recognition_Transactions__r.size(), revRecSettings.size());
        
        
    }

}