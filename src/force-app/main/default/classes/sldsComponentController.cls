public class sldsComponentController {
    public string var_title_small {get;set;}
    public string var_title_large {get;set;}
    public string var_title_image {get;set;}
    public string var_title_image_color {get;set;}
    public string var_color_guide {get;set;}
    
    public colorGuide[] getColorGuides() {
        if (var_color_guide != null) {
            colorGuide[] cgs = new List<colorGuide>();
            for (string var : var_color_guide.split(',',0)) {
                colorGuide cg = new colorGuide(var);
                cgs.add(cg);
            }
            return cgs;
        } else {
            return null;
        }
    }
    
    public class colorGuide {
        public string color {get;set;}
        public string label {get;set;}
        
        public colorGuide(string wrVar) {
            if (wrVar != null) {
                string[] cgMap = wrVar.split('#',0);
                label = cgMap[0];
                color = '#'+cgMap[1];
            }
        }
    }
    
    public string getXLinkURL() {
        string xlink = '/resource/1448307219000/slds/assets/icons/action-sprite/svg/symbols.svg#'+var_title_image;
        return xlink;
    }
    
}