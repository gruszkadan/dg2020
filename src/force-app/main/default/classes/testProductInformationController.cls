@isTest(seeAllData=true)
public class testProductInformationController {
    
    static testmethod void buildAwesomeGridOfProducts(){
    	 Account testAccount = new Account(Name = 'testAccount');
        insert testAccount;
        
        Order_Item__c oi = new Order_Item__c();
        oi.Name = 'Test';
        oi.Account__c = testAccount.id;
        oi.Product_Platform__c = 'MSDSonline';
        oi.Category__c = 'MSDS Management';
        oi.Display_on_Account__c = true;
        oi.Invoice_Amount__c = 10;
        oi.Order_Date__c = date.today();
        oi.Order_ID__c = 'test123';
        oi.Order_Item_ID__c = 'test123';
        oi.Month__c = 'January';
        oi.Admin_Tool_Product_Name__c = 'MSDS Management (licenses)';
        oi.Year__c = '2017';
        oi.Contract_Length__c = 3;
        oi.AccountID__c = '123TEST';
        oi.Term_Start_Date__c = date.today();
        oi.Term_End_Date__c = date.today().addYears(1);
        oi.Term_Length__c = 1;
        oi.Admin_Tool_Order_Status__c = 'A';
        oi.Contract_Number__c = 'test123';
        oi.Sales_Location__c = 'Chicago';
        oi.Admin_Tool_Sales_Rep__c = 'Daniel Gruszka';
        oi.Admin_Tool_Order_Type__c = 'New';
        insert oi;
        /*
        Order_Item__c oi2 = new Order_Item__c();
        oi2.Name = 'Test2';
        oi2.Account__c = testAccount.id;
        oi2.Product_Platform__c = 'MSDSonline';
        oi2.Category__c = 'MSDS Management';
        oi2.Display_on_Account__c = true;
        oi2.Invoice_Amount__c = 10;
        oi2.Order_Date__c = date.today();
        oi2.Order_ID__c = 'test123';
        oi2.Order_Item_ID__c = 'test321';
        oi2.Month__c = 'January';
        oi2.Admin_Tool_Product_Name__c = 'HQ';
        oi2.Year__c = '2017';
        oi2.Contract_Length__c = 3;
        oi2.AccountID__c = '123TEST';
        oi2.Term_Start_Date__c = date.today();
        oi2.Term_End_Date__c = date.today().addYears(1);
        oi2.Term_Length__c = 1;
        oi2.Admin_Tool_Order_Status__c = 'A';
        oi2.Contract_Number__c = 'test123';
        oi2.Sales_Location__c = 'Chicago';
        oi2.Admin_Tool_Sales_Rep__c = 'Daniel Gruszka';
        oi2.Admin_Tool_Order_Type__c = 'New';
        insert oi2;
        */
        Order_Item__c oi3 = new Order_Item__c();
        oi3.Name = 'Test';
        oi3.Account__c = testAccount.id;
        oi3.Product_Platform__c = 'Ergo';
        oi3.Category__c = 'Ergo Licensing';
        oi3.Display_on_Account__c = true;
        oi3.Invoice_Amount__c = 10;
        oi3.Order_Date__c = date.today();
        oi3.Order_ID__c = 'test123';
        oi3.Order_Item_ID__c = 'test312';
        oi3.Month__c = 'January';
        oi3.Admin_Tool_Product_Name__c = 'Ergo';
        oi3.Year__c = '2017';
        oi3.Contract_Length__c = 3;
        oi3.AccountID__c = '123TEST';
        oi3.Term_Start_Date__c = date.today();
        oi3.Term_End_Date__c = date.today().addYears(1);
        oi3.Term_Length__c = 1;
        oi3.Admin_Tool_Order_Status__c = 'A';
        oi3.Contract_Number__c = 'test123';
        oi3.Sales_Location__c = 'Chicago';
        oi3.Admin_Tool_Sales_Rep__c = 'Daniel Gruszka';
        oi3.Admin_Tool_Order_Type__c = 'New';
        insert oi3;
        
        Order_Item__c oi4 = new Order_Item__c();
        oi4.Name = 'Test';
        oi4.Account__c = testAccount.id;
        oi4.Product_Platform__c = 'Ergo';
        oi4.Category__c = 'Ergo Licensing';
        oi4.Display_on_Account__c = true;
        oi4.Invoice_Amount__c = 10;
        oi4.Order_Date__c = date.today();
        oi4.Order_ID__c = 'test123';
        oi4.Order_Item_ID__c = 'test231';
        oi4.Month__c = 'January';
        oi4.Admin_Tool_Product_Name__c = 'The Humantech System';
        oi4.Year__c = '2017';
        oi4.Contract_Length__c = 3;
        oi4.AccountID__c = '123TEST';
        oi4.Term_Start_Date__c = date.today();
        oi4.Term_End_Date__c = date.today().addYears(1);
        oi4.Term_Length__c = 1;
        oi4.Admin_Tool_Order_Status__c = 'A';
        oi4.Contract_Number__c = 'test123';
        oi4.Sales_Location__c = 'Chicago';
        oi4.Admin_Tool_Sales_Rep__c = 'Daniel Gruszka';
        oi4.Admin_Tool_Order_Type__c = 'New';
        insert oi4;
        
       Test.startTest();

        productInformationController myController = new productInformationController();
        myController.AcctId = testAccount.id; 
        myController.getProductsByCategory();
        //System.assert( myController.getProductsByCategory()[0].Platform == 'Chemical Management');
        //System.assert( myController.getProductsByCategory()[0].categories[0].orderItemList.size() > 0);
        // System.debug( myController.getProductsByCategory()[2].Platform);
        // System.debug( myController.getProductsByCategory()[2].categories[0].Category);
        //System.debug( myController.getProductsByCategory()[2].categories[0].orderItemList.size());
     Test.stopTest();

        
    }
}