@isTest()
public class testAmeConsoleHome {
    static testMethod void test1(){
        //create test account
        Account testAccount = new account();
        testAccount.name = 'Test 1';
        testAccount.NAICS_Code__c = 'Test';
        testaccount.Customer_Status__c = 'Active';
        insert testAccount;
        
        Contact testContact = new contact();
        testContact.FirstName = 'Jack';
        testContact.LastName = 'James';
        testContact.AccountId = testAccount.id;
        Insert testContact;
        
        
        Account_Management_Event__c ame = new Account_Management_Event__c();
        Account_Management_Event__c ame1 = new Account_Management_Event__c();
        Account_Management_Event__c ame2 = new Account_Management_Event__c();
        Account_Management_Event__c ame3 = new Account_Management_Event__c();      
        Account_Management_Event__c ame4 = new Account_Management_Event__c();  
        Account_Management_Event__c ame5 = new Account_Management_Event__c();       
        Account_Management_Event__c ameCancel = new Account_Management_Event__c();
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1];   
        
        //insert new ame Account
        ame.Account__c = testAccount.id;
        ame.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        ame.Contact__c = testContact.id;
        ame.OwnerId = Dan.Id;
        ame.Batch_Month__c = 'Sept';
        ame.Batch_Year__c = String.valueof(System.today().toStartOfMonth().addMonths(3).year());
        ame.Status__c = 'Active';
        ame.Count_in_Stats__c = true;
        insert ame;
        system.debug(ame.Batch_Year__c);
        
        //insert new Ame Account1
        ame1.Account__c = testAccount.id;
        ame1.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        ame1.Contact__c = testContact.id;
        ame1.OwnerId = Dan.Id;
        ame1.Not_Yet_Processed__c = false;
        ame1.Batch_Month__c = 'Sept';
        ame1.Batch_Year__c = String.valueof(System.today().toStartOfMonth().addMonths(3).year());
        ame1.Status__c = 'Active';
        ame1.Count_in_Stats__c = true;
        
        insert ame1;
        
        //insert new Ame Account1
        ame2.Account__c = testAccount.id;
        ame2.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        ame2.Contact__c = testContact.id;
        ame2.OwnerId = Dan.Id;
        ame2.Not_Yet_Processed__c = false;
        ame2.Batch_Month__c = 'Sept';
        ame2.Batch_Year__c = String.valueof(System.today().toStartOfMonth().addMonths(3).year());
        ame2.Status__c = 'Pending Approval';
        ame2.Count_in_Stats__c = true;
        
        insert ame2;
        
        //insert new Ame Account1
        ame3.Account__c = testAccount.id;
        ame3.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        ame3.Contact__c = testContact.id;
        ame3.OwnerId = Dan.Id;
        ame3.Not_Yet_Processed__c = false;
        ame3.Batch_Month__c = 'Sept';
        ame3.Batch_Year__c = String.valueof(System.today().toStartOfMonth().addMonths(3).year());
        ame3.Status__c = 'Sent to Orders';
        ame3.Count_in_Stats__c = true;
        
        insert ame3;
        
        //insert new Ame Account1
        ame4.Account__c = testAccount.id;
        ame4.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        ame4.Contact__c = testContact.id;
        ame4.OwnerId = Dan.Id;
        ame4.Not_Yet_Processed__c = false;
        ame4.Batch_Month__c = 'Sept';
        ame4.Batch_Year__c = String.valueof(System.today().toStartOfMonth().addMonths(3).year());
        ame4.Status__c = 'Approved';
        ame4.Count_in_Stats__c = true;
        
        insert ame4;
        
        //insert new Ame Account1
        ame5.Account__c = testAccount.id;
        ame5.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        ame5.Contact__c = testContact.id;
        ame5.OwnerId = Dan.Id;
        ame5.Not_Yet_Processed__c = false;
        ame5.Batch_Month__c = 'Sept';
        ame5.Batch_Year__c = String.valueof(System.today().toStartOfMonth().addMonths(3).year());
        ame5.Status__c = 'Not Yet Created';
        ame5.Count_in_Stats__c = true;
        
        insert ame5;
        
        
        //insert cancellation ame
        ameCancel.Account__c = testAccount.id;
        ameCancel.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();  
        ameCancel.Contact__c = testContact.id;
        ameCancel.OwnerId = Dan.Id;
        ameCancel.Status__c = 'Active';
        ameCancel.Related_AME__c = ame.Id;
        ameCancel.Count_in_Stats__c = true;
        
        insert ameCancel;
        
        //insert new opp
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Ford1';
        Opp.StageName= 'Closed/Won';
        Opp.OwnerID= Dan.Id;
        Opp.AccountId = testAccount.id;
        Opp.CloseDate = date.today();
        Opp.Account_Management_Event__c = ame.Id;
        Opp.Save__c = true;
        Opp.Amount = 2000;
        insert Opp;
        system.debug(Opp.Amount);
        system.debug(Opp.Id);
        
        //Insert Product2
        Product2 product = new Product2();
        product.Name = 'testproduct';
        product.isActive = true;
        product.CurrencyIsoCode = 'USD';
        insert product;
        
        //Insert Pricebook2
        Pricebook2 Pricebook = new Pricebook2();
        Pricebook.Name = 'Standard Price Book';
        Pricebook.isActive = true;
        insert Pricebook;
        
        //Insert PriceBookEntry
        PriceBookEntry pbe = new PriceBookEntry();
        pbe.isActive = true;
        pbe.UnitPrice = 1.00;
        pbe.Pricebook2Id = Pricebook.Id;
        pbe.Product2Id = product.Id;
        //insert pbe;
        
        //Insert new OpportunityLineItem(1)
        OpportunityLineItem oppItem = new OpportunityLineItem();
        oppItem.PricebookEntryId = '01u80000006eap9AAA';
        oppItem.Quantity = 1;
        oppItem.TotalPrice = 1.00;
        oppItem.OpportunityId = Opp.Id;
        oppItem.qpID__c = 'Test1';
        oppItem.PI__c = 100;
        oppItem.Product2Id = product.id;
        insert oppItem;
        
        // create task 
        Task T = new task();
        T.OwnerId = ame.OwnerId;
        T.WhatId = ame.id;
        T.WhoId = ame.Contact__c;
        T.Status = 'Not Started';
        T.Subject = 'new task';
        T.ActivityDate = date.today();
        T.Type__c = 'Call';
        Insert T;
        
        //Creating list of order items
        Order_Item__c[] testOIs = new list<Order_Item__c>();
        testDataUtility Util = new testDataUtility(); 
        
        //adding order Item 1
        Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
        oi1.Order_ID__c = 'test456';
        oi1.Month__c = 'January';
        oi1.Salesforce_Product_Name__c = 'HQ';
        oi1.Category__c = 'MSDS Management';
        oi1.Product_Platform__c = 'MSDSonline';
        oi1.Renewal_Amount__c = 1000;
        oi1.AccountID__c = 'Test 1';
        oi1.Term_Start_Date__c = Date.today().addDays(-10);
        oi1.Term_End_Date__c = Date.today().addDays(10);
        oi1.Account_Management_Event__c = ame.Id;
        testOIs.add(oi1);
        
        ameConsoleHomeController ameDashboard = new ameConsoleHomeController();
        ameDashboard.wrapperAME();
        
    }
    
    static testMethod void test2(){
        
        Account testAccount = new account();
        testAccount.name = 'Test 1';
        testAccount.NAICS_Code__c = 'Test';
        testaccount.Customer_Status__c = 'Active';
        insert testAccount;
        
        Contact testContact = new contact();
        testContact.FirstName = 'Jack';
        testContact.LastName = 'James';
        testContact.AccountId = testAccount.id;
        Insert testContact;
        
        
        Account_Management_Event__c ameRenewal = new Account_Management_Event__c();
        Account_Management_Event__c ameCancel = new Account_Management_Event__c();
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1];  
        
        //insert new Account
        ameRenewal.Account__c = testAccount.id;
        ameRenewal.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        ameRenewal.Contact__c = testContact.id;
        ameRenewal.OwnerId = Dan.Id;
        ameRenewal.Not_Yet_Processed__c = False;
        ameRenewal.Batch_Month__c = 'Dec';
        ameRenewal.Batch_Year__c = String.valueof(System.today().toStartOfMonth().addMonths(3).year());
        ameRenewal.Status__c = 'Not Yet Created';
        ameRenewal.Count_in_Stats__c = true;
        
        insert ameRenewal;
        
        //Creating list of order items
        Order_Item__c[] testOIs = new list<Order_Item__c>();
        testDataUtility Util = new testDataUtility(); 
        
        //adding order Item 1
        Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
        oi1.Order_ID__c = 'test456';
        oi1.Month__c = 'January';
        oi1.Cancelled_On_AME__c = ameRenewal.Id;
        oi1.Salesforce_Product_Name__c = 'HQ';
        oi1.Category__c = 'MSDS Management';
        oi1.Product_Platform__c = 'MSDSonline';
        oi1.Renewal_Amount__c = 1000;
        oi1.AccountID__c = testAccount.Id;
        oi1.Term_Start_Date__c = Date.today().addDays(-10);
        oi1.Term_End_Date__c = Date.today().addDays(10);
        oi1.Account_Management_Event__c = ameRenewal.Id;
        testOIs.add(oi1);
        
        ameConsoleHomeController ameDashboardHome = new ameConsoleHomeController();
        ameDashboardHome.upcomingWrapper();
        ameDashboardHome.getRetentionUserSelectList();
        ameDashboardHome.getBatchYears();
        ameDashboardHome.userIdChange();
    }  
}