public class accountReferralTriggerHandler {
    
    public static void afterInsert(Account_Referral__c[] refs) {
        poiActionUtility.createSFPOIActions(refs);
        incentiveTransactionUtility.createReferralIncentive(refs[0].id, refs[0].Account__c, refs[0].Type__c, refs[0].Referrer__c);
    }

}