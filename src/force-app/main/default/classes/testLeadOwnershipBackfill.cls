@isTest(seeAllData=false)
private class testLeadOwnershipBackfill {
    
    static testmethod void test1() {
        //create default owners
        Lead_Custom_Settings__c[] defs = new list<Lead_Custom_Settings__c>();
        Lead_Custom_Settings__c defEntEHS = new Lead_Custom_Settings__c(SetupOwnerId='00534000008R6yS', Enterprise_EHS_Owner__c=true);
        defs.add(defEntEHS);
        Lead_Custom_Settings__c defEHS = new Lead_Custom_Settings__c(SetupOwnerId='0053400000965x6', Default_EHS_Owner__c=true);
        defs.add(defEHS);
        Lead_Custom_Settings__c specialEHS = new Lead_Custom_Settings__c(SetupOwnerId='0053400000965xC', Special_Country_EHS_Owner__c=true);
        defs.add(specialEHS);
        Lead_Custom_Settings__c defErgo = new Lead_Custom_Settings__c(SetupOwnerId='005800000044Lax', Default_Ergo_Owner__c=true);
        defs.add(defErgo);
        Lead_Custom_Settings__c defOT = new Lead_Custom_Settings__c(SetupOwnerId='005300000012aUI', Default_Online_Training_Owner__c=true, Default_Authoring_Owner__c=true);
        defs.add(defOT);
        Lead_Custom_Settings__c defChemMgt = new Lead_Custom_Settings__c(SetupOwnerId='00534000009RHio', Default_Chemical_Management_Owner__c=true);
        defs.add(defChemMgt);
        insert defs;
        
        //create round robin assignments
        Round_Robin_Assignment__c[] rrs = new list<Round_Robin_Assignment__c>();
        for (integer i=0; i<3; i++) {
            Round_Robin_Assignment__c rr = new Round_Robin_Assignment__c();
            if (i==0) {
                rr.Name = 'Joe Odere';
                rr.UserID__c = '00534000008o1S3';
                rr.Order__c = (i+1);
            } else if (i==1) {
                rr.Name = 'Brad Whitaker';
                rr.UserID__c = '00534000008R9mj';
                rr.Order__c = (i+1);
            } else {
                rr.Name = 'Victoria Azzi';
                rr.UserID__c = '00534000008R477';
                rr.Order__c = (i+1);
                rr.Used_Last__c = true;
            }
            rrs.add(rr);
        }
        insert rrs;
        
        //create a territory
        Territory__c t = new Territory__c();
        t.Name = 'TEST1';
        t.Territory_Owner__c = '00534000008R4IZ';
        t.Online_Training_Owner__c = '0058000000565uQ';
        t.Authoring_Owner__c = '00580000007GA2F';
        t.Ergonomics_Owner__c = '00534000008d2ny';
        t.EHS_Owner__c = '00534000008d6bD';
        insert t;
        
        //create county zip code
        County_Zip_Code__c zip = new County_Zip_Code__c();
        zip.City__c = 'Chicago';
        zip.State__c = 'IL';
        zip.Territory__c = t.id;
        zip.Zip_Code__c = '60654';
        zip.County__c = 'Cook';
        zip.Country__c = 'United States';
        insert zip;
        
        Lead[] leads = new list<Lead>();
        
        //LEAD 1 - enterprise, territory
        Lead l1 = new Lead();
        l1.LastName = 'Test';
        l1.Company = 'Test Company';
        l1.Enterprise_Sales_Lead__c = true;
        l1.Territory__c = t.id;
        l1.OwnerId = '00580000007F76q';
        leads.add(l1);
        
        //LEAD 2 - enterprise, territory, ent owner
        Lead l2 = new Lead();
        l2.LastName = 'Test';
        l2.Company = 'Test Company';
        l2.Enterprise_Sales_Lead__c = true;
        l2.Territory__c = t.id;
        l2.OwnerId = '005300000012mUR';
        leads.add(l2);
        
        //LEAD 3 - enterprise, territory, ent ehs owner
        Lead l3 = new Lead();
        l3.LastName = 'Test';
        l3.Company = 'Test Company';
        l3.Enterprise_Sales_Lead__c = true;
        l3.Territory__c = t.id;
        l3.OwnerId = '0053400000965xE';
        leads.add(l3);
        
        //LEAD 4 - midmarket, territory
        Lead l4 = new Lead();
        l4.LastName = 'Test';
        l4.Company = 'Test Company';
        l4.Territory__c = t.id;
        l4.OwnerId = '00580000007F76q';
        leads.add(l4);
        
        //LEAD 5 - special country, ent owner
        Lead l5 = new Lead();
        l5.LastName = 'Test';
        l5.Company = 'Test Company';
        l5.Country = 'UAE';
        l5.OwnerId = '005300000012mUR';
        l5.Enterprise_Sales_Lead__c = true;
        leads.add(l5);
        
        //LEAD 6- special country, enterprise
        Lead l6 = new Lead();
        l6.LastName = 'Test';
        l6.Company = 'Test Company';
        l6.Country = 'UAE';
        l6.OwnerId = '00580000007F76q';
        l6.Enterprise_Sales_Lead__c = true;
        leads.add(l6);
        
        //LEAD 7- special country, ent ehs owner
        Lead l7 = new Lead();
        l7.LastName = 'Test';
        l7.Company = 'Test Company';
        l7.Country = 'UAE';
        l7.OwnerId = '0053400000965xE';
        l7.Enterprise_Sales_Lead__c = true;
        leads.add(l7);
        
        //LEAD 8- special country, midmarket
        Lead l8 = new Lead();
        l8.LastName = 'Test';
        l8.Company = 'Test Company';
        l8.Country = 'UAE';
        l8.OwnerId = '00580000007F76q';
        leads.add(l8);
        
        //LEAD 9 - no terr, midmarket
        Lead l9 = new Lead();
        l9.LastName = 'Test';
        l9.Company = 'Test Company';
        l9.OwnerId = '00580000007F76q';
        leads.add(l9);
        
        //LEAD 10 - no terr, ent, ent owner
        Lead l10 = new Lead();
        l10.LastName = 'Test';
        l10.Company = 'Test Company';
        l10.OwnerId = '005300000012mUR';
        leads.add(l10);
        
        //LEAD 11 - no terr, ent, ent ehs owner
        Lead l11 = new Lead();
        l11.LastName = 'Test';
        l11.Company = 'Test Company';
        l11.OwnerId = '0053400000965xE';
        leads.add(l11);
        
        //LEAD 12 - no terr, ent
        Lead l12 = new Lead();
        l12.LastName = 'Test';
        l12.Company = 'Test Company';
        l12.OwnerId = '00580000007F76q';
        l12.Enterprise_Sales_Lead__c = true;
        leads.add(l12);
        
        //LEAD 13 - assigned to queue, enterprise
        Lead l13 = new Lead();
        l13.LastName = 'Test';
        l13.Company = 'Test Company';
        l13.OwnerId = '00G8000000288tl';
        l13.Enterprise_Sales_Lead__c = true;
        leads.add(l13);
        
        //LEAD 14 - assigned to queue, mm
        Lead l14 = new Lead();
        l14.LastName = 'Test';
        l14.Company = 'Test Company';
        l14.OwnerId = '00G34000003POBr';
        leads.add(l14);
        
        //LEAD 15 - assigned to queue, mm, special country
        Lead l15 = new Lead();
        l15.LastName = 'Test';
        l15.Company = 'Test Company';
        l15.Country = 'UAE';
        l15.OwnerId = '00G34000003POBr';
        leads.add(l15);
        
        insert leads;
        
        test.startTest();
        leadOwnershipBackfill batchJob = new leadOwnershipBackfill();
        Database.executeBatch(batchJob);
        test.stopTest();
        
        //requery and assert
        set<id> leadIds = new set<id>();
        for (Lead l : leads) {
            leadIds.add(l.id);
        }
        
        leads = [SELECT id, OwnerId, Chemical_Management_Owner__c, Online_Training_Owner__c, Authoring_Owner__c, EHS_Owner__c, Ergonomics_Owner__c
                 FROM Lead
                 WHERE id IN : leadIds];
        
        for (Lead l : leads) {
            
            //LEAD 1
            if (l.id == l1.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == rrs[0].UserID__c);
                System.assert(l.Online_Training_Owner__c == t.Online_Training_Owner__c);
                System.assert(l.Authoring_Owner__c == t.Authoring_Owner__c);
                System.assert(l.EHS_Owner__c == defEntEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == t.Ergonomics_Owner__c);
            }
            //LEAD 2
            if (l.id == l2.id) {
                System.assert(l.Chemical_Management_Owner__c == l.OwnerId);
                System.assert(l.Online_Training_Owner__c == t.Online_Training_Owner__c);
                System.assert(l.Authoring_Owner__c == t.Authoring_Owner__c);
                System.assert(l.EHS_Owner__c == defEntEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == t.Ergonomics_Owner__c);
            }
            //LEAD 3
            if (l.id == l3.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == rrs[1].UserID__c);
                System.assert(l.Online_Training_Owner__c == t.Online_Training_Owner__c);
                System.assert(l.Authoring_Owner__c == t.Authoring_Owner__c);
                System.assert(l.EHS_Owner__c == l3.OwnerId);
                System.assert(l.Ergonomics_Owner__c == t.Ergonomics_Owner__c);
            }
            //LEAD 4
            if (l.id == l4.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == t.Territory_Owner__c);
                System.assert(l.Online_Training_Owner__c == t.Online_Training_Owner__c);
                System.assert(l.Authoring_Owner__c == t.Authoring_Owner__c);
                System.assert(l.EHS_Owner__c == t.EHS_Owner__c);
                System.assert(l.Ergonomics_Owner__c == t.Ergonomics_Owner__c);
            }
            //LEAD 5
            if (l.id == l5.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == l5.OwnerId);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == specialEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
            //LEAD 6
            if (l.id == l6.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == rrs[2].UserID__c);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == specialEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
            //LEAD 7
            if (l.id == l7.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == rrs[0].UserID__c);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == l7.OwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
            //LEAD 8
            if (l.id == l8.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == defChemMgt.SetupOwnerId);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == specialEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
            //LEAD 9
            if (l.id == l9.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == defChemMgt.SetupOwnerId);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == defEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
            //LEAD 10
            if (l.id == l10.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == l10.OwnerId);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == defEntEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
            //LEAD 11
            if (l.id == l11.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == rrs[1].UserID__c);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == l11.OwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
            //LEAD 12
            if (l.id == l12.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == rrs[2].UserID__c);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == defEntEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
            //LEAD 13
            if (l.id == l13.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == rrs[0].UserID__c);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == defEntEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
            //LEAD 14
            if (l.id == l14.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == defChemMgt.SetupOwnerId);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == defEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
            //LEAD 15
            if (l.id == l15.id) {
                System.assert(l.OwnerId == l.Chemical_Management_Owner__c);
                System.assert(l.Chemical_Management_Owner__c == defChemMgt.SetupOwnerId);
                System.assert(l.Online_Training_Owner__c == defOT.SetupOwnerId);
                System.assert(l.Authoring_Owner__c == defOT.SetupOwnerId);
                System.assert(l.EHS_Owner__c == specialEHS.SetupOwnerId);
                System.assert(l.Ergonomics_Owner__c == defErgo.SetupOwnerId);
            }
        }
    }
}