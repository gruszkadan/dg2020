//Test Class:testSalesPerformanceRequestIncentives
public class salesPerformanceGhostBooking {
    public String viewAs {get;set;}
    public String viewAsDefaultView {get;set;}
    public string userID {get;set;}
    public User u {get;set;}
    public User currentUser {get;set;}
    public String defaultView {get;set;}
    public Sales_Performance_Request__c newSPR {get;set;}
    public String errorMessage {get;set;}
    public String alertIcon {get;set;}
    public Boolean isSuccess {get;set;}
    public String pageName {get;set;}
    public boolean directorVP {get;set;}
    public list<String> approverNames {get;set;}
    public boolean previousMonthClosed {get;set;}
    public boolean includeIncentive {get;set;}
    public SelectOption[] salesUsers {get;set;}
    
    public salesPerformanceGhostBooking() {
        //Get the Name of the Page
        pageName = ApexPages.currentPage().getUrl().substringAfter('apex/');
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias, FirstName, LastName, Name, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID, Sales_Director__c,
             Sales_Director__r.Name
             FROM User 
             WHERE id = :viewAs LIMIT 1];
        userID = u.id;
        //get defaultView to display when viewing as
        defaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
        newSPR = new Sales_Performance_Request__c(Rollup_to_Rep__c = TRUE,Rollup_to_Manager__c = TRUE,Rollup_to_Director__c = TRUE,Rollup_to_VP__c = TRUE,Incentive_Month__c = 'Current Month');
        previousMonthClosed = salesPerformanceUtility.previousMonthClosed();
        includeIncentive = FALSE;
        salesUsers = salesPerformanceUtility.getSalesUsers(defaultView, userID, u.Name); 
        //Set directorVP Variable (used to determine if approval is required below)
        if(defaultView =='Director' || defaultView =='VP'){
            directorVP = TRUE;
        } else {
            directorVP = FALSE;
        }
        errorMessage = 'Please make sure all required fields are filled out';
    }
    
    //Get List of Incentive Months for Add Incentive
    public List<SelectOption> getIncentiveMonths() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Current Month', 'Current Month'));
        //If its the 6th business day or less in the current month, display Previous Mont
        if(!previousMonthClosed){
            options.add(new SelectOption('Previous Month', 'Previous Month'));
        }
        return options;
    }
    
    public void includeIncentives(){
        if(includeIncentive == true){
            includeIncentive = false;
        }else{
            includeIncentive = true;
        }
    }
    
    public void submit(){
        //Set the Variables
        boolean canSave = TRUE;
        isSuccess = TRUE;
        alertIcon = 'success';
        approverNames = new list<String>();        
        //Create the SPR Record
        newSPR.Type__c = 'Ghost Booking';
        newSPR.Status__c ='Pending Approval';
        newSPR.Requires_Approval__c = TRUE;
        newSPR.OwnerId = u.Id;
        system.debug('defaultview is  '+defaultView);
        if(defaultView =='Rep'){
            newSPR.Approval_Required_By__c = salesPerformanceRequestUtility.approvalRequiredBy(string.valueOf(u.ID), TRUE, TRUE, FALSE);
            newSPR.Assigned_Approver_1__c = u.ManagerID;
            newSPR.Assigned_Approver_2__c = u.Sales_Director__c;
            approverNames.add(u.Manager.Name);
            approverNames.add(u.Sales_Director__r.Name);
            system.debug('approvals '+u.Name+'-'+u.Manager.Name+'-'+u.Sales_Director__r.Name);
            system.debug('actual approvals '+u.Name+'-'+newSPR.Assigned_Approver_1__c+'-'+ newSPR.Assigned_Approver_2__c);
        }else if(defaultView == 'Manager'){
            newSPR.Approval_Required_By__c = salesPerformanceRequestUtility.approvalRequiredBy(string.valueOf(u.ID), TRUE, FALSE, FALSE);
            newSPR.Assigned_Approver_1__c = u.ManagerID;
            approverNames.add(u.Manager.Name);
        }else if(defaultView == 'Director' || defaultView == 'VP'){
            newSPR.Status__c ='Approved';
            newSPR.Requires_Approval__c = FALSE;
        }
        
        //Find the SPS to associate the SPR to
        newSPR.Sales_Performance_Summary__c = salesPerformanceRequestUtility.findSprSps(null, newSPR);
        
        if(newSpr.Sales_Rep__c == NULL || newSPR.Booking_Amount__c == NULL || newSPR.Reason__c == NULL ||
           ((newSPR.Incentive_Amount__c == null || newSPR.Incentive_Amount__c == 0) && includeIncentive)){
               canSave = FALSE;
           }
        if(canSave){
            try {
                insert newSPR;
                system.debug('approvalslist is  '+approverNames);
            } catch(System.DMLexception e) {
                errorMessage = string.valueOf(e);
                system.debug('error is '+errorMessage);
                isSuccess = false;
                alertIcon = 'error';
            }
        } else {
            isSuccess = false;
            alertIcon = 'error';
        }
    }
    
}