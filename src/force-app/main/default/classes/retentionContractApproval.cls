public class retentionContractApproval {
    Approval__c app;
    Contract__c c;
    contractUtility util = new contractUtility();
    dynamicQuery query = new dynamicQuery();
    string[] contractReasons = new List<string>();
    Contract_Line_Item__c[] items = new List<Contract_Line_Item__c>();
    Contract_Line_Item__c[] lineItemChanges = new List<Contract_Line_Item__c>();
    Set<string> approvers = new Set<string>();
    boolean newApp = false;
    boolean lang = false;
    boolean manager = false;
    boolean orders = false; 
    boolean finance = false; 
    boolean operations = false;
    public boolean wasRefreshed = false;
    public boolean wasApproved = false;
    public boolean backToApproved = true;
    public boolean isInitialCheck = false;
    id[] productIds = new list<id>();
    String contractProductSuites;
    
    // main method called when checking the contract for approval criteria
    public void check(id cid, boolean isSubmit) {
        
        //query the contract
        queryContract(cid);	
        
        //query existing app or create a new one
        queryExistingApp();	
        
        //query the line items
        queryLineItems();		
        
        //check the criteria on the contract
        contractCheck();	
        
        //check the criteria of the line items
        lineItemCheck();	
        
        //create reasons for approval
        approvalReasons();	
        
        //create list of approvers
        createApproverList();	
        
        if (!isSubmit) {
            
            //upsert app and update contract
            if (manager || orders || finance || operations) {
                app.Status__c = 'Approval Needed';
                c.Status__c = 'Active';
                upsert app;
                update c;
            } 
            
            // if all approval criteria is removed, set the contract back to approved
            if (backToApproved) {		
                c.Status__c = 'Approved';
                app.Status__c = 'Approved';
                update c;
                upsert app;
                if (!newApp && !wasApproved) {
                    delete app;
                }
            } 
        } else {
            flagContract();												// mark the fields on the contract for the approval process to read
            app.Status__c = 'Pending Approval';
            update app;
            update c;
        }
    }
    
    // query the contract
    public void queryContract(id cid) {
        c = util.queryContract(cid);
    }
    
    // query the approval record or create a new one if there isnt one already
    public void queryExistingApp() {
        if (c.Approvals__r.size() > 0) {
            app = (Approval__c)new dynamicQuery().query('Approval__c', null, 'Contract__c = \''+c.id+'\'');
            if (app.Status__c == 'Refreshed') {				
                wasRefreshed = true;
            } 
            if (app.Previously_Approved__c) {
                wasApproved = true;
            }
        } else {
            newApp = true;
            app = new Approval__c();
            app.Contract__c = c.id;
        }
    }
    
    // query line items
    public void queryLineItems() {
        items = [SELECT id, Name__c, Type__c, Contract_Terms__c, Approval_Required__c, Product__c		
                 FROM Contract_Line_Item__c 
                 WHERE Contract__c = : c.id 
                 ORDER BY Contract_Sort_Order__c];
        
        
    }
    
    //this checks the criteria on the contract itself
    public void contractCheck() {
        
        //split billing
        if (c.Split_Billing__c) {
            if (app.Split_Billing__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Split billing');
                app.Split_Billing__c = 'Approval Needed';
                manager = true; finance = true;
                backToApproved = false;
            }
        } else {
            if (app.Split_Billing__c != 'Approved') {
                app.Split_Billing__c = null;
            }
        }
        
        //deferred invoice
        if (c.Delayed_Billing_Only__c && c.Delayed_Billing_Date__c > (date.today() + 60)) {
            boolean onlyNewItems = true;
            for (Contract_Line_Item__c item : items) {
                if (item.Type__c == 'Renewal') {
                    onlyNewItems = false;
                }
            }
            if (onlyNewItems) {
                if (app.Delayed_Billing__c != 'Approved' || c.Delayed_Billing_Date__c > app.Approved_Delayed_Billing_Date__c) {
                    contractReasons.add('Deferred invoice > 60 days');
                    app.Delayed_Billing__c = 'Approval Needed';
                    manager = true; finance = true;
                    backToApproved = false;
                }
            }
        } else if (app.Delayed_Billing__c != 'Approved' || c.Delayed_Billing_Date__c < (date.today() + 60)) {
            app.Delayed_Billing__c = null;
        }
        
        //installments
        if (c.Installments__c) {
            if (app.Installments__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Installments');
                app.Installments__c = 'Approval Needed';
                manager = true; finance = true;
                backToApproved = false;
            }
        } else if (app.Installments__c != 'Approved') {
            app.Installments__c = null;
        }
        
        //Related Contracts
        /*
        if(c.Related_Contract_Type__c == 'Add-On' && c.Contract_End_Date_new__c == null){
            if (app.Related_Contract__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Related Contract Subscription Dates');
                app.Related_Contract__c = 'Approval Needed';
                 manager = true; orders = true;
                backToApproved = false;
            } else {
                if (app.Related_Contract__c != 'Approved') {
                    app.Related_Contract__c = null;
                }
            }
        }
*/
        
        //nonstandard msa
        if (c.Standard_MSA__c == 'No') {
            if (app.Non_Standard_MSA__c != 'Approved') {
                contractReasons.add('Non-standard MSA');
                app.Non_Standard_MSA__c = 'Approval Needed';
                manager = true; operations = true;
                backToApproved = false;
            }
        } else if (app.Non_Standard_MSA__c != 'Approved') {
            app.Non_Standard_MSA__c = null;
        }
        
        //net 90 payment terms
        if (c.Custom_Payment_Net_Terms__c && c.Payment_Net_Terms__c == 'Net 90') {
            if (app.Custom_Net_Terms__c != 'Approved' || wasRefreshed) {
                contractReasons.add('Net terms > 90');
                app.Custom_Net_Terms__c = 'Approval Needed';
                manager = true; finance = true;
                backToApproved = false;
            }
        } else if (app.Custom_Net_Terms__c != 'Approved') {
            app.Custom_Net_Terms__c = null;
        }
    }
    
    //run through the line items and see if any need language
    public void lineItemCheck() {
        
        boolean needsLang = false;
        
        for (Contract_Line_Item__c item : items) {
            if (item.Name__c == 'Custom Label' || item.Name__c == 'Custom Services Project' || item.Name__c == 'Webpliance' || item.Name__c.contains('PPI')) {
                
                //if the contract terms are null, it needs language
                if (item.Contract_Terms__c == null || item.Approval_Required__c) {
                    contractReasons.add(item.Name__c+': Needs new contract language');
                    item.Approval_Required__c = true;
                    app.Needs_Language__c = 'Approval Needed';
                    manager = true;
                    if(item.Name__c == 'Custom Label' || item.Name__c == 'Custom Services Project'){
                        orders = true; 
                    }
                    operations = true;
                    backToApproved = false;
                } 
            }
        }
    }
    
    // create the reasons and record on app record (probably not the best way to go about this?)
    public void approvalReasons() { 						
        Set<string> reasonSet = new Set<string>();
        if (contractReasons.size() > 0) {															// if there are approval reasons on the contract, add the reasons to the set
            for (string cr : contractReasons) {
                reasonSet.add(cr);
            }
        }
        app.Approval_Reasons__c = '';																// clear the reasons on the app, loop through the list of new reasons, and save the app
        for (string ar : reasonSet) {
            app.Approval_Reasons__c += ar+';';
        }
        app.Approval_Reasons__c = app.Approval_Reasons__c.removeStart(';').removeEnd(';');			// sometimes an extra semicolon is placed at the start / end so this will remove them
        c.Rep_Approval_Reasons__c = app.Approval_Reasons__c.replace(';', ', ');
    }
    
    // find the list of approvers
    public void createApproverList() {	
        string appList = '';
        integer num = 0;
        if (manager) {
            appList += 'Manager;';
            num++;
        }
        if (orders) {
            appList += 'Orders;';
            num++;
        }
        if (finance) {
            appList += 'Finance;';
            num++;
        }
        if (operations) {
            appList += 'Operations;';
            num++;
        }
        //set the list of approvers
        app.Approvers__c = appList;	
        
        //set the # of approval steps
        app.Num_Of_Steps__c = num;		
        
        //set the current step to 1
        app.Current_Step_Num__c = 1;													
    }
    
    // set the approval fields on the contract so the approval process can read it
    public void flagContract() {
        c.Approve_Orders__c = false;
        c.Approve_Finance__c = false;
        c.Approve_Operations__c = false;
        for (string approver : app.Approvers__c.split(';', 0)) {						// check to see what approvers are needed and mark the contract accordingly
            if (approver == 'Manager') {
                c.Status__c = 'Active';
            } 
            if (approver == 'Orders') {
                c.Approve_Orders__c = true;
                c.Status__c = 'Active';
            }
            if (approver == 'Finance') {
                c.Approve_Finance__c = true;
                c.Status__c = 'Active';
            }
            if (approver == 'Operations') {
                c.Approve_Operations__c = true;
                c.Status__c = 'Active';
            }
        }
    }
    
}