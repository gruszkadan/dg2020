trigger orderItemTrigger on Order_Item__c 
(before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            orderItemTriggerHandler.beforeInsert(trigger.new);
        }
        if (trigger.isUpdate) {
            orderItemTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
    }
    
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            orderItemTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        if (trigger.isUpdate) {
            orderItemTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        if (trigger.isDelete) {
            orderItemTriggerHandler.afterDelete(trigger.old, trigger.new);
        }
    }
    dlrs.RollupService.triggerHandler();

}