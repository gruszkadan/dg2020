@isTest
private class testSalesPerformanceSummaryTrigger {
    static testmethod void test1() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            User testUser = util.createUser('test1a', 'vp', null, null, null);
            insert testUser;
            
            Sales_Performance_Summary__c sps = util.createSPS(testUser, null);
            sps.Total_Bookings__c = 500;
            sps.Quota__c = 500;
            insert sps;
            
            test.startTest();
            sps.Total_Bookings__c = 1000;
            sps.Quota__c = 1000;
            update sps;
            test.stopTest();
        }
    }
    
    static testmethod void test2() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
             User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            User assoc = util.createUser('TestAs', 'assoc', vp, dir, mgr);
            insert assoc;
            
            assoc.Group__c = 'Enterprise Sales';
            update assoc;
            
            DateTime d = date.today();
            String monthName = d.format('MMMMM');
            String year = d.format('yyyy');
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = year;
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = year;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = year;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
                        
            Sales_Performance_Summary__c assocSPS = new Sales_Performance_Summary__c();
            assocSPS.OwnerId = assoc.id;
            assocSPS.Manager__c = assoc.Sales_Performance_Manager_ID__c;
            assocSPS.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            assocSPS.Month__c = monthName;
            assocSPS.Year__c = year;
            assocSPS.Role__c = assoc.Role__c;
            assocSPS.Sales_Team_Manager__c = assoc.Sales_Performance_Manager_ID__c;
            assocSPS.Sales_Rep__c = assoc.id; 
            assocSPS.Total_Bookings__c = 500;
            assocSPS.Quota__c = 400;
            assocSPS.Group__c = 'Enterprise Sales';
            assocSPS.Role__c = 'Associate';
            insert assocSPS;
            
            test.startTest();
            assocSPS.Total_Bookings__c = 300;
            update assocSPS;
            test.stopTest();
        }
    }
}