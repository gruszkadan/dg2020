/*
 * Test class: testSalesPerformanceSummaryUtility
 */
public class salesPerformanceSummaryUtility {
    
    public static void updateYTDTotals(Sales_Performance_Summary__c[] SPSs) {
        set<id> ownerIds = new set<id>();
        set<string> years = new set<string>();
        for (Sales_Performance_Summary__c sps : SPSs) {
            ownerIds.add(sps.OwnerId);
            years.add(sps.Year__c);
        }
        Sales_Performance_Summary__c[] allSummaries = [SELECT id, OwnerId, Total_Bookings__c, My_Bookings__c, YTD_Total_Bookings__c, YTD_My_Bookings__c, Month__c, Month_Number__c, Year__c, Quota__c
                                                       FROM Sales_Performance_Summary__c
                                                       WHERE OwnerId IN : ownerIds
                                                       AND Year__c IN : years
                                                       ORDER BY Month_Number__c ASC];
        if (allSummaries.size() > 0) {
            for (Sales_Performance_Summary__c sps : SPSs) {
                decimal ytdTotalBookings = 0.00;
                decimal ytdMyBookings = 0.00;
                decimal ytdQuota = 0.00;
                for (Sales_Performance_Summary__c s : allSummaries) {
                    if (s.OwnerId == sps.ownerId && s.Year__c == sps.Year__c) {
                        ytdTotalBookings = ytdTotalBookings + s.Total_Bookings__c;
                        ytdMyBookings = ytdMyBookings + s.My_Bookings__c;
                        ytdQuota = ytdQuota + s.Quota__c;
                        s.YTD_Total_Bookings__c = ytdTotalBookings;
                        s.YTD_My_Bookings__c = ytdMyBookings;
                        s.YTD_Quota__c = ytdQuota;
                    }
                }
                
            }
            upsertList(allSummaries, 'updateYTDTotals');
        }
    } 
    
    //SF-53877
    // Method to change ES associate eligibility for rep bookings credit
   /* public static void updateEsAssociateCreditEligibility(Sales_Performance_Summary__c[] SPSs) {
        for(Sales_Performance_Summary__c sps:SPSs){
            if(sps.Eligible_for_Associate_Credit__c){
                sps.Eligible_for_Associate_Credit__c = FALSE;
            }else{
                sps.Eligible_for_Associate_Credit__c = TRUE;
            }
        }
    }*/
    
    // Reusable upsert method
    public static void upsertList(sObject[] upsList, string location) {
        if (upsList.size() > 0) {
            try {
                upsert upsList;
            } catch(exception e) {
                salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceSummaryUtility', location, string.valueOf(e));
            }
        }
    }
    
}