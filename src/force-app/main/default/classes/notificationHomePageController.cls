public with sharing class notificationHomePageController 
{
    private Apexpages.StandardController controller; 
    public User u {get; set;}
    public boolean autoOpen {get; set;}
    public Notification_Audience__c na {get; set;}
    public Notification_Audience__c[] nas {get; set;}
    public newNotifWrapper[] wrappers {get; set;}
    public id naId {get; set;}
    public integer notifDisp {get; set;}
    public integer numUnread {get; set;}
    public integer popupSize {get;set;}
    public boolean isNewPopup {get; set;}
    public Notification_Audience__c[] naList {get; set;}
    public string offNum {get; set;}
    public integer num {get; set;}
    public integer totalSize {get; set;}
    public integer selPage {get; set;}
    public string sortBy {get; set;}
    public boolean nextPage {get; set;}
    public boolean prevPage {get; set;}
    public string soql {get;set;}
    public Map<integer, Notification_Audience__c[]> pmap {get; set;}
    public datetime myDate = datetime.now();
    public string selView {get; set;}
    public boolean showAll {get; set;}
    public integer numOfPages {get; set;}
    
    public notificationHomePageController(ApexPages.StandardController stdController) 
    {
        selPage = 1;
        selView = 'Unread Notifications';
        num = 0;
        Schema.DescribeSObjectResult r = Notification_Audience__c.sObjectType.getDescribe();
        if (ApexPages.currentPage().getParameters().get('showall') == 'true') 
        {
            showAll = true;
        }
        if (ApexPages.currentPage().getParameters().get('notif') != null) 
        {
            isNewPopup = false;
            na = [SELECT id, Name, Already_Shown__c, Completed_Date__c, Notification_Due_Date__c, Notification_Read__c, Status__c, Notification__c, Notification__r.Title__c, Notification__r.Owner.Name, Notification__r.Description__c, Notification__r.Publish_Date__c
                  FROM Notification_Audience__c WHERE id = :ApexPages.currentPage().getParameters().get('notif') LIMIT 1];
        } 
        else 
        {
            isNewPopup = true;
        }
        popupSize = 440;
        u = [SELECT id, Name, Email, ManagerId, Department__c, Group__c, Role__c, ProfileId FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
        Notification_Audience__c[] tempList = [SELECT id, Notification_Read__c FROM Notification_Audience__c WHERE User__c = :u.id AND Notification_Due_Date__c > :date.TODAY()-30 AND Notification__r.Publish_Date__c < :datetime.NOW() AND Notification__r.Status__c != 'Draft' AND Notification__r.Status__c != 'Scheduled'];
        numUnread = 0;
        totalSize = tempList.size();
        if (wrappers == null)
        {
            wrappers = new List<newNotifWrapper>();
        }
        find();
        makeWrappers(tempList);
    }
    
    public void makeWrappers(Notification_Audience__c[] tempList2)
    {
        integer i = 0;
        Notification_Audience__c[] tempList = tempList2;
        for (Notification_Audience__c tempNa : tempList)
        {
            if (tempNa.Notification_Read__c == false)
            {
                numUnread++;
            }
        }
        integer numPopups = 0;
        for (Notification_Audience__c tempNa : nas)
        {
            if (tempNa.Already_Shown__c == false || showAll == true)
            {
                i++;
                newNotifWrapper nnw = new newNotifWrapper(tempNa, i);
                wrappers.add(nnw);
                numPopups = numPopups + 1;
            }
        }
        if (numPopups == 1) {popupSize = 400;}
		if (numPopups == 2) {popupSize = 422;}
		if (numPopups == 3) {popupSize = 451;}
        if (numPopups == 4) {popupSize = 480;}
        if (numPopups == 5 || numPopups > 5) {popupSize = 510;}
        autoOpen = false;
        if (wrappers.size() > 0)
        {
            autoOpen = true;
            notifDisp = wrappers.size();
        }
    }
    
    public void find()
    {
        if (offNum != null)
        {
            if (offNum == '-') 
            {
                num = num - 10;
            }
            else
            {
                num = num + 10;
            }
        }
        else
        {
            num = 0;
        }
        if (sortBy == null)
        {
            sortBy = 'Notification__r.Publish_Date__c DESC';
        }
        soql = 'SELECT id, Name, Already_Shown__c, Completed_Date__c, Notification_Due_Date__c, Notification_Read__c, Status__c, Notification__c, Notification__r.Title__c, Notification__r.Owner.Name, '+
            'Notification__r.Description__c, Notification__r.Publish_Date__c FROM Notification_Audience__c WHERE User__c = \''+u.id+'\' AND Notification__r.Publish_Date__c < :myDate AND Notification__r.Status__c = \'Published\' ';
        if (selView == 'Unread Notifications')
        {
            soql += 'AND Notification_Read__c = false ';
        }
        soql += 'ORDER BY '+sortBy+' NULLS LAST OFFSET '+num;
        nas = Database.query(soql);
        mapToPage();
    }
    
    public void mapToPage()
    {
       numOfPages = integer.valueof(((decimal.valueOf(nas.size())) / 10).round(System.RoundingMode.UP));
        pmap = new Map<integer, Notification_Audience__c[]>();
        if (numOfPages == 1)
        {
            pmap.put(1, nas);
        }
        else 
        {
            for (integer iPg = 0; iPg < numOfPages; iPg ++)
            {
                if (iPg == 0)
                {
                    Notification_Audience__c[] tempNas = new List<Notification_Audience__c>();
                    for (integer iVal = 0; iVal < 10; iVal ++)
                    {
                        tempNas.add(nas[iVal]);
                    }
                    pmap.put(iPg + 1, tempNas);
                }
                if (iPg > 0 && iPg != numOfPages - 1)
                {
                    Notification_Audience__c[] tempNas = new List<Notification_Audience__c>();
                    for (integer iVal = iPg * 10; iVal < iPg * 10 + 10; iVal ++)
                    {
                        tempNas.add(nas[iVal]);
                    }
                    pmap.put(iPg + 1, tempNas);
                }
                if (iPg == numOfPages - 1)
                {
                    Notification_Audience__c[] tempNas = new List<Notification_Audience__c>();
                    for (integer iVal = iPg * 10; iVal < nas.size(); iVal ++)
                    {
                        tempNas.add(nas[iVal]);
                    }
                    pmap.put(iPg + 1, tempNas);
                }
            }
        }
        naList = pmap.get(selPage);
    }
    
    public List<SelectOption> getStatusList() 
    {
        List<SelectOption> o = new List<SelectOption>();      
        o.add(new SelectOption('Unread Notifications', 'Unread Notifications'));
        o.add(new SelectOption('All Notifications', 'All Notifications'));
        return o;
    }
    
    public void viewNotif()
    {
        for (Notification_Audience__c na1 : nas)
        {
            if (na1.id == naId)
            {
                na = na1;
            }
        }
    }
    
    public void checkNewNotif()
    {
        for (integer iw = 0; iw < wrappers.size(); iw++)
        {
            if (wrappers[iw].w_na.Notification_Read__c == true)
            {
                wrappers[iw].w_na.Notification_Read__c = true;
                wrappers[iw].w_na.Completed_Date__c = datetime.now();
                update wrappers[iw].w_na;
            }
        }
    }
    
    public void checkOldNotif()
    {
        na.Notification_Read__c = true;
        na.Completed_Date__c = datetime.now();
        update na;
    }
    
    public void onLoad()
    {
        if (showAll != true)
        {
            for (newNotifWrapper wrap : wrappers)
            {
                wrap.w_na.Already_Shown__c = true;
                update wrap.w_na;
            }
        }
    }
    
    public class newNotifWrapper
    {
        public Notification_Audience__c w_na {get; set;}
        public integer w_i {get;set;}
        
        public newNotifWrapper(Notification_Audience__c na, integer i)
        {
            w_na = na;
            w_i = i;
        }
    }
    
}