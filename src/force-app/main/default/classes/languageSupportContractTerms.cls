public class languageSupportContractTerms {
    @InvocableMethod (label='Language Support Terms')
    public static void languageSupportTerms(List<id> contractLineItemId) {
        //The line item that will need to be updated with the new language
        Contract_Line_Item__c lineItemToUpdate = [SELECT id,Y1_Total_Price__c,Name,Contract__r.Account__r.Name,Product__r.Contract_Product_Name__c,
                                                  Contract__c,Contract__r.Billing_Currency__c, Contract__r.Billing_Currency_Rate__c, Language_Support_Languages__c
                                                  FROM Contract_Line_Item__c
                                                  WHERE id =:contractLineItemId[0]
                                                  LIMIT 1];
        
        //Query for the terms
        String contractTerms = [SELECT id, Description__c
                                FROM Description_Dictionary__c
                                WHERE Flow_Referance_Name__c = 'language_support'
                                LIMIT 1].Description__c;
        
        
        //build the list of modules
        integer currentLoop = 0;
        String languagesFormatted;
        integer languagesSize = lineItemToUpdate.Language_Support_Languages__c.split(';').size();
        for (String s:lineItemToUpdate.Language_Support_Languages__c.split(';')) {
            currentLoop++;
            if(languagesFormatted == null){
                languagesFormatted = s;
            }else if(currentLoop != languagesSize){
                languagesFormatted += ', '+s; 
            }else{
                languagesFormatted += ' and '+s; 
            }
        }
        
        if(lineItemToUpdate != null){
            
            //string to hold the main terms that will be appended to the line item
            string itemTerms = contractTerms;
            
            //Update the languages in the terms
            itemTerms = itemTerms.replace('&lt;Language_Support_Languages&gt;', languagesFormatted);
            
            //Update the amount of languages in the terms
            itemTerms = itemTerms.replace('&lt;Num_of_Languages&gt;', string.valueof(languagesSize));
            
            //Update the customer name in the terms
            itemTerms = itemTerms.replace('&lt;CustomerName&gt;', string.valueof(lineItemToUpdate.Contract__r.Account__r.Name));
            
            //Build Muli-Currency total amount string
            string y1PriceConverted = '$';
            if(lineItemToUpdate.Contract__r.Billing_Currency__c != null && lineItemToUpdate.Contract__r.Billing_Currency__c == 'GBP'){
                y1PriceConverted = '&pound;';
            }else if(lineItemToUpdate.Contract__r.Billing_Currency__c != null && lineItemToUpdate.Contract__r.Billing_Currency__c == 'EUR'){
                y1PriceConverted = '&euro;'; 
            }
            decimal rate = 1;
            if(lineItemToUpdate.Contract__r.Billing_Currency_Rate__c != null){
                rate = lineItemToUpdate.Contract__r.Billing_Currency_Rate__c;
            }
            decimal convertedTotal = Math.ceil(lineItemToUpdate.Y1_Total_Price__c * rate);
            y1PriceConverted += string.valueof(convertedTotal.format());
            if(lineItemToUpdate.Contract__r.Billing_Currency__c != null){
                y1PriceConverted += ' '+lineItemToUpdate.Contract__r.Billing_Currency__c;
            }else{
                y1PriceConverted += ' USD';
            }
            itemTerms = itemTerms.replace('&lt;amount&gt;', y1PriceConverted);
            
            //Set the new contract terms on the item;
            lineItemToUpdate.Contract_Terms__c = itemTerms;
            
            update lineItemToUpdate;
        }
    }
}