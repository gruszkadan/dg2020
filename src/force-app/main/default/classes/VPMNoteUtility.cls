public class VPMNoteUtility {

    //newly added was in caseNote Utility
    public static void VPMNoteCreation(Case_Notes__c[] caseNotes){
        
        Id[] relatedCaseIds = new List<Id>();
        for(Case_Notes__c cNote : caseNotes){
            relatedCaseIds.add(cNote.Case__c);
        }
        
        VPM_Milestone__c[] relatedMilestones = new List<VPM_Milestone__c>();
        relatedMilestones = [Select Id, Case__c, VPM_Project__c from VPM_Milestone__c where Case__c IN :relatedCaseIds];
        
        Map<Case_Notes__c, VPM_Milestone__c> getMilestones = new Map<Case_Notes__c, VPM_Milestone__c>();
        for(Case_Notes__c cN : caseNotes){
            for(VPM_Milestone__c mlstone : relatedMilestones){
                if(mlstone.Case__c == cN.Case__c){
                    getMilestones.put(cN, mlstone);
                }
            }
        }
        
        VPM_Note__c[] newNotes = new List<VPM_Note__c>();
        for(Case_Notes__c cN : caseNotes){
            VPM_Note__c n = new VPM_Note__c();
            n.VPM_Project__c = getMilestones.get(cN).VPM_Project__c;
            n.VPM_Milestone__c = getMilestones.get(cN).Id;
            //added fields to note Type__c to match caseNote field
            n.Type__c = cN.Notes_Type__c;
            n.Call_Duration__c = cN.Call_Duration__c;
            n.Comments__c = cN.Issue__c;
            n.Case_Note__c = cN.Id;
            n.Contact__c = cN.Case_Contact_Id__c;
            newNotes.add(n);
        } 
        try{
            insert newNotes;
        }catch(exception e){
            salesforceLog.createLog('Case Note', true, 'caseNoteUtility', 'projectNoteCreation', +string.valueOf(e));
        }       
    }
    
    
        public static void VPMTaskNoteAssociation(VPM_Task__c[] tasks){

        Id[] relatedCaseNoteIds = new List<Id>();
        for(VPM_Task__c tsk : tasks){
            relatedCaseNoteIds.add(tsk.Case_Note__c);
        }
        
        VPM_Note__c[] relatedNotes = new List<VPM_Note__c>();
        relatedNotes = [Select Type__c, Call_Duration__c, Comments__c, Case_Note__c from  VPM_Note__c where Case_Note__c IN :relatedCaseNoteIds];
        
        Map< VPM_Note__c, VPM_Task__c> getNotesTask = new Map< VPM_Note__c, VPM_Task__c>();
        for(VPM_Task__c tsk : tasks){
            for(VPM_note__c note : relatedNotes){
                if(note.Case_Note__c == tsk.Case_Note__c){
                    getNotesTask.put(note, tsk);
                }
            }
        }
       
        for( VPM_Note__c n : relatedNotes){
         n.VPM_Task__c = getNotesTask.get(n).Id;     
        }
        
        try{
            update relatedNotes;
        }catch(exception e){
            salesforceLog.createLog('VPM_Task__c', true, 'projectTaskUtility', 'projectNoteAssociations', +string.valueOf(e));
        } 
       
    }
    
     public static void VPMNoteUpdate(Case_Notes__c[] caseNotes){
        
        VPM_Note__c[] relatedNotes = new List<VPM_Note__c>();
        relatedNotes = [Select Type__c, Call_Duration__c, Comments__c, Case_Note__c from VPM_Note__c where Case_Note__c IN :caseNotes];
        
        VPM_Note__c[] updatedNotes = new List<VPM_Note__c>();
        for(Case_Notes__c cN : caseNotes){
            for(VPM_Note__c note : relatedNotes){
                if(note.Case_Note__c == cN.Id){
                    note.Type__c = cN.Notes_Type__c;
                    note.Call_Duration__c = cN.Call_Duration__c;
                    note.Comments__c = cN.Issue__c;
                    updatedNotes.add(note);
                }
            }
        }
        
        try{
            update updatedNotes;
        }catch(exception e){
            salesforceLog.createLog('Case Note', true, 'caseNoteUtility', 'projectNoteUpdates', +string.valueOf(e));
        }       
    }
}