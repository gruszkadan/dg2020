//Test Class: testSalesPerformanceRequestIncentives
public class salesPerformanceRequestUtility {
    
    public static string approvalRequiredBy (string userID, boolean mgr, boolean dir, boolean vp){
        //Set to Hold User Names that are to be apart of the approval process
        Set<String> userNames = new Set<String>();
        
        User theUser = [Select ID, Name, Manager.Name, Sales_Director__r.Name, Sales_VP__r.Name from User where ID=: userID];
        if(mgr){
            userNames.add(theUser.Manager.Name);
        }
        if(dir){
            userNames.add(theUser.Sales_Director__r.Name);
        }
        if(vp){
            userNames.add(theUser.Sales_VP__r.Name);
        }
        return string.valueOf(userNames);
    }
    
    public static void submitForApproval (Sales_Performance_Request__c[] sfrs){
        for(Sales_Performance_Request__c s : sfrs){
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            //Record to Be Submitted
            req.setObjectId(s.id);
            //Approval Submitted By
            req.setSubmitterId(s.OwnerID);
            //Submit the Record for Approval
            Approval.ProcessResult result = Approval.process(req);
        }
    }
    
    //Method Creates the Data to Send to Incentive Transaction Utility
    //Used for Manual Incentive Creation
    public static void buildIncentiveTransaction (Sales_Performance_Request__c[] sfrs){
        date incentiveDate;
        //If Incentive Month was Previous Month, find the last day of the previous month, else use today's date
        if(((Datetime.Now().format('MMMMM') != sfrs[0].CreatedDate.format('MMMMM')) ||
            (Datetime.Now().format('MMMMM') == sfrs[0].CreatedDate.format('MMMMM') && sfrs[0].Incentive_Month__c == 'Previous Month')) &&
           (!salesPerformanceUtility.previousMonthClosed())
          ){
              incentiveDate = Date.Today().toStartofMonth() -1;           
          } else {
              incentiveDate = Date.Today();
          }
        //Send the Data to Create the Incentive Transaction
        incentiveTransactionUtility.insertSingleIncetive(NULL, 'Other', sfrs[0].Incentive_Amount__c, incentiveDate, sfrs[0].Sales_Rep__c, sfrs[0].Id);
    }
    
    public static void updateCurrentApproverLookup(Sales_Performance_Request__c[] sfrs) {
        //Set to Hold User Names we want to Query
        set<String> userNames = new set<String>();
        //Go through all the Requests and add the User Names to the Set
        for(Sales_Performance_Request__c sprs : sfrs){
            userNames.add(sprs.Current_Approver__c);
        }
        //Query the Users
        User[] users = [Select ID, Name
                        FROM User 
                        WHERE Name in: userNames];
        for(Sales_Performance_Request__c sprs : sfrs){
            if(users.size()>0){
                for(User u : Users){
                    if(u.Name == sprs.Current_Approver__c){
                        sprs.Current_Approver_Lookup__c = u.ID;
                    } else {
                        sprs.Current_Approver_Lookup__c = NULL;
                    }
                }
            } else {
                sprs.Current_Approver_Lookup__c = NULL;
            }
        }
    }
    
    public static void createOrderItemUpdateRequest(Order_Item__c[] oldItems, Order_Item__c[] newItems) {
        boolean previousMonthClosed = salesPerformanceUtility.previousMonthClosed();
        //List to hold newly created SPR's
        Sales_Performance_Request__c[] sprs = new list<Sales_Performance_Request__c>();
        
        //List to hold any split order items that need to be sent to SF ops
        set<id> splitOrderItemIds = new set<id>();
        
        //Create a map of SPI's for each order item that has been changed
        Map<id,Sales_Performance_Item__c[]> spiMap = new Map<id,Sales_Performance_Item__c[]>();
        Sales_Performance_Item__c[] spis = [SELECT id,Sales_Performance_Order__c,Order_Item__c,Booking_Amount__c,OwnerId,Sales_Director__c FROM Sales_Performance_Item__c WHERE Order_Item__c in:newItems AND Type__c = 'Booking'];
        set<id> spoIds = new set<id>();
        for(Order_Item__c oi:newItems){
            Sales_Performance_Item__c[] orderItemSPIs = new list<Sales_Performance_Item__c>();
            for(Sales_Performance_Item__c spi:spis){
                spoIds.add(spi.Sales_Performance_Order__c);
                if(spi.Order_Item__c == oi.id){
                    orderItemSPIs.add(spi);
                }
            }
            spiMap.put(oi.id,orderItemSPIs);
        }
        
        //Reject any currently pending SPR's related to any SPO's being adjusted - excluding automated adjustments for other order items
        Sales_Performance_Request__c[] sprsToReject = [SELECT id FROM Sales_Performance_Request__c where Sales_Performance_Order__c in:spoIds 
                                                       AND ((Status__c LIKE '%Pending%' AND Type__c != 'Automated Adjustment') OR (Status__c LIKE '%Pending%' AND Order_Item__c in:newItems))];
        if(sprsToReject.size() > 0){
            Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId in :sprsToReject])).keySet();
            Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
            if(pInstanceWorkitems.size() > 0){
                Approval.ProcessWorkitemRequest[] allReq = New list<Approval.ProcessWorkitemRequest>();
                for (Id pInstanceWorkitemsId:pInstanceWorkitems){
                    Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                    req2.setComments('Auto-Reject due to Automated Adjustment.');
                    req2.setAction('Reject');
                    req2.setNextApproverIds(null);
                    req2.setWorkitemId(pInstanceWorkitemsId);
                    
                    // Add the request for approval
                    allReq.add(req2);
                }
                Approval.ProcessResult[] result2 = Approval.process(allReq);
            }
        }
        
        for (integer i=0; i<newItems.size(); i++) {
            //Determine the adjustment amount to the bookings
            decimal adjustment = newItems[i].Invoice_Amount__c - oldItems[i].Invoice_Amount__c;
            
            //Determine status change
            string status;
            if(newItems[i].Status__c != oldItems[i].Status__c){
                status = newItems[i].Status__c;
            }
            
            //Find Sales Performance Items related to the Order Item
            Sales_Performance_Item__c spi;
            if(spiMap.get(newItems[i].id).size() == 1){
                spi = spiMap.get(newItems[i].id)[0];
            }else{
                splitOrderItemIds.add(newItems[i].id); 
            }
            
            if(spi != null){
                //Create a new SPR record for the approval
                Sales_Performance_Request__c spr = new Sales_Performance_Request__c();
                spr.Order_Item__c = newItems[i].id;
                spr.Type__c = 'Automated Adjustment';
                spr.Reason__c = 'Admin Tool Adjustment';
                spr.Status__c = 'Pending Approval';
                spr.Requires_Approval__c = true;
                spr.Assigned_Approver_1__c = spi.Sales_Director__c;
                //If bookings were adjusted then do not send for approval immediately
                if(adjustment != 0){
                    spr.Status__c = 'New';
                    spr.Requires_Approval__c = false;
                }
                spr.Sales_Rep__c = spi.OwnerId;
                spr.Sales_Performance_Order__c = spi.Sales_Performance_Order__c;
                Sales_Performance_Order__c spo = [SELECT id,Order_Date__c,OwnerId FROM Sales_Performance_Order__c WHERE id=:spi.Sales_Performance_Order__c LIMIT 1];
                spr.Sales_Performance_Summary__c = findSprSpsInUtility(spo,null,previousMonthClosed);
                string[] updates = new list<string>();
                updates.add(newItems[i].id);
                updates.add(spi.id);
                if(adjustment != 0){
                    updates.add(string.valueof(adjustment));  
                    updates.add(string.valueof(oldItems[i].Invoice_Amount__c));
                    updates.add(string.valueof(newItems[i].Invoice_Amount__c));
                }else{
                    updates.add('N/A');
                    updates.add('N/A');
                    updates.add('N/A');
                }
                if(status != null){
                    updates.add(status);
                }else{
                    updates.add('N/A');
                }
                spr.JSON__c = JSON.serialize(updates);
                sprs.add(spr);
            }
        }
        
        if(splitOrderItemIds.size() > 0){
            string logMessage = 'Automated adjustment was unable to be completed for the following order items due to split: '+splitOrderItemIds;
            salesforceLog.createLog('Sales Performance', true, 'salesPerformanceRequestUtility', 'createOrderItemUpdateRequest', logMessage);
        }
        
        if(sprs.size() > 0){
            try {
                insert sprs;
            } catch(exception e) {
                salesforceLog.createLog('Sales Performance', true, 'salesPerformanceRequestUtility', 'createOrderItemUpdateRequest', string.valueOf(e));
            }
        }
        
    }
    
    //Find Sales Performance Summary to associate to a Sales Performance Request
    public static id findSprSps(Sales_Performance_Order__c spo, Sales_Performance_Request__c spr){
        //id of the sps found to return
        id spsId;
        
        //id of the owner of sps to find
        id rep;
        
        //Month of SPS to find
        //Current month by default
        integer month = date.today().month();
        
        //Year of SPS to find
        //Current year by default
        integer year = date.today().year();
        
        //If a sales performance order was passed in
        if(spo != null && spr == null){
            //Rep is set to the owner of the spo
            rep = spo.OwnerId;
            //If the previous month is still open and the order date is before this month
            if(!salesPerformanceUtility.previousMonthClosed() && spo.Order_Date__c < date.today().toStartOfMonth()){
                //Month is set to the previous month
                month = date.today().addMonths(-1).month();
                //Year is set based on the previous months year
                year = date.today().addMonths(-1).year();
            }
        }
        
        //If incentive only with no sales performance order
        else if(spr != null && spo == null){
            //Rep is set to the spr sales rep
            rep = spr.Sales_Rep__c;
            if(spr.Incentive_Month__c == 'Previous Month'){
                //Month is set to the previous month
                month = date.today().addMonths(-1).month();
                //Year is set based on the previous months year
                year = date.today().addMonths(-1).year();
            }
        }
        
        //Query the SPS id to return
        Sales_Performance_Summary__c[] sps = [SELECT id
                                              FROM Sales_Performance_Summary__c 
                                              WHERE Sales_Rep__c =:rep
                                              and Year__c =:string.valueof(year)
                                              and Month_Number__c =:month
                                              LIMIT 1];
        if(sps.size() > 0){
            spsId = sps[0].id;
        }
        
        return spsId;
    }
    
    //Find Sales Performance Summary to associate to a Sales Performance Request
    public static id findSprSpsInUtility(Sales_Performance_Order__c spo, Sales_Performance_Request__c spr, boolean previousMonthClosed){
        //id of the sps found to return
        id spsId;
        
        //id of the owner of sps to find
        id rep;
        
        //Month of SPS to find
        //Current month by default
        integer month = date.today().month();
        
        //Year of SPS to find
        //Current year by default
        integer year = date.today().year();
        
        //If a sales performance order was passed in
        if(spo != null && spr == null){
            //Rep is set to the owner of the spo
            rep = spo.OwnerId;
            //If the previous month is still open and the order date is before this month
            if(!previousMonthClosed && spo.Order_Date__c < date.today().toStartOfMonth()){
                //Month is set to the previous month
                month = date.today().addMonths(-1).month();
                //Year is set based on the previous months year
                year = date.today().addMonths(-1).year();
            }
        }
        
        //If incentive only with no sales performance order
        else if(spr != null && spo == null){
            //Rep is set to the spr sales rep
            rep = spr.Sales_Rep__c;
            if(spr.Incentive_Month__c == 'Previous Month'){
                //Month is set to the previous month
                month = date.today().addMonths(-1).month();
                //Year is set based on the previous months year
                year = date.today().addMonths(-1).year();
            }
        }
        
        //Query the SPS id to return
        Sales_Performance_Summary__c[] sps = [SELECT id
                                              FROM Sales_Performance_Summary__c 
                                              WHERE Sales_Rep__c =:rep
                                              and Year__c =:string.valueof(year)
                                              and Month_Number__c =:month
                                              LIMIT 1];
        if(sps.size() > 0){
            spsId = sps[0].id;
        }
        
        return spsId;
    }
    
}