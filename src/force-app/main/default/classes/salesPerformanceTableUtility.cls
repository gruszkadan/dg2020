/*
 * Test class: testSalesPeformanceTable
 */
public class salesPerformanceTableUtility {
    
    public salesPerformanceTableUtility() {}
    
    // year picklist on salesPerformanceTable
    public SelectOption[] getyears() {
        SelectOption[] ops = new list<SelectOption>();
        integer firstYear = 2017;
        integer nowYear = date.today().year();
        ops.add(new SelectOption(string.valueOf(firstYear), string.valueOf(firstYear)));
        for (integer i = 0; i < (nowYear - firstYear); i++) {
            integer yearVal = firstYear + (i + 1);
            ops.add(new SelectOption(string.valueOf(yearVal), string.valueOf(yearVal)));
        }
        return ops;
    }   
    
    // view types picklist on salesPerformanceTable
    public SelectOption[] getviewTypes() {
        SelectOption[] ops = new list<SelectOption>();
        ops.add(new SelectOption('My Team', 'My Team'));
        ops.add(new SelectOption('Group', 'Group'));
        ops.add(new SelectOption('Role', 'Role'));
        return ops; 
    }
    
    // months to loop through for salesPerformanceTable controller
    public string[] months() {
        string m = 'January;February;March;April;May;June;July;August;September;October;November;December';
        return m.split(';');
    }
    
    public string findMonth(integer num) {
        string month = '';
        if (num == 1) {
            month = 'January';
        } else if (num == 2) {
            month = 'February';
        } else if (num == 3) {
            month = 'March';
        } else if (num == 4) {
            month = 'April';
        } else if (num == 5) {
            month = 'May';
        } else if (num == 6) {
            month = 'June';
        } else if (num == 7) {
            month = 'July';
        } else if (num == 8) {
            month = 'August';
        } else if (num == 9) {
            month = 'September';
        } else if (num == 10) {
            month = 'October';
        } else if (num == 11) {
            month = 'November';
        } else if (num == 12) {
            month = 'December';
        }
        return month;
    }
}