public class activityHistoryController {
    
    transient Account acct;
    public string filter {get;set;}
    public boolean hasNextPage {get;set;}
    public boolean hasPrevPage {get;set;}
    public integer pgNum {get;set;}
    public integer ahSize {get;set;}
    public string showingNumString {get;set;}
    id acctId;
    string userId;
    //fixes
    transient ActivityHistory[] allFilteredHistories;
    public ActivityHistory[] histories {get;set;}
    public string acctName {get;set;}
    
    public activityHistoryController() {
        acctId = ApexPages.currentPage().getParameters().get('id');
        filter = ApexPages.currentPage().getParameters().get('f');
        userId = UserInfo.getUserId();
        pgNum = 1;
        histories = new List<ActivityHistory>();
        queryAll();
    }
    
    public void queryAll() {
        string q = 'SELECT id, Name, '+
            '(SELECT id, Subject, Type__c, Call_Result__c, Description, Important_Note__c, isTask, ActivityDate, Owner.Name, OwnerId, Who.Name, WhoId, Event_Status__c, EndDateTime, StartDateTime, PrimaryWhoId '+
            'FROM ActivityHistories ';
        if (filter != null) {
            if (filter == 'All Tasks') {
                q += 'WHERE isTask = true ';
            } else if (filter == 'All Events') {
                q += 'WHERE isTask = false ';
            } else if (filter == 'All Important') {
                q += 'WHERE Important_Note__c = true ';
            } else if (filter == 'My Important') {
                q += 'WHERE Important_Note__c = true AND OwnerId = \''+userId+'\' ';
            } else if (filter == 'Account Management') {
                q += 'WHERE Call_Result__c = \'Account Management Communication\'';
            }
        }
        q += 'ORDER BY ActivityDate DESC, LastModifiedDate DESC LIMIT 500) '+
            'FROM Account WHERE id = \''+acctId+'\'';
        acct = Database.query(q);
        acctName = acct.Name;
        allFilteredHistories = acct.ActivityHistories;
        ahSize = allFilteredHistories.size();
        if (ahSize > 0) {
            createHistories();
        } else {
            histories.clear();
            checkShowingNum();
        }
    }
    
    public void createHistories() {
        if (histories.size() > 0) {
            histories.clear();
        }
        if (pgNum == 1) {
            integer i2;
            if (ahSize > 50) {
                i2 = 50;
            } else {
                i2 = ahSize;
            }
            for (integer i=0; i<i2; i++) {
                histories.add(allFilteredHistories[i]);
            }
        }
        if (pgNum > 1) {
            integer i1 = (pgNum - 1) * 50;
            integer i2;
            if (i1 + 50 <= ahSize) {
                i2 = i1 + 50;
            } else {
                i2 = ahSize;
            }
            for (integer i=i1; i<i2; i++) {
                histories.add(allFilteredHistories[i]);
            }
        }
        checkNextPage();
        checkShowingNum();
    }
    
    public void checkNextPage() {
        if (pgNum > 1) {
            hasPrevPage = true;
        } else {
            hasPrevPage = false;
        }
        integer recRemain = ahSize - (pgNum * 50);
        if (recRemain > 0) {
            hasNextPage = true;
        } else {
            hasNextPage = false;
        }
    }
    
    public void gotoPrevPage() {
        pgNum --;
        queryAll();
        checkShowingNum();
    }
    
    public void gotoNextPage() {
        pgNum ++;
        queryAll();
        checkShowingNum();
    }
    
    public void filterList() {
        filter = ApexPages.currentPage().getParameters().get('ahf');
        pgNum = 1;
        queryAll();
    }
    
    public void checkShowingNum() { 
        if (histories.size() > 0) {
            integer num1 = pgNum * 50 - 50 + 1;
            integer num2;
            if ((num1 - 1) + 50 < ahSize) {
                num2 = (num1 - 1) + 50;
            } else { 
                num2 = ahSize;
            }
            showingNumString = 'Showing '+num1+'-'+num2+' of '+ahSize;
        } else {
            showingNumString = 'Showing 0-0 of 0';
        }
    }
    
    public PageReference back() {
        return new PageReference('/'+acctId).setRedirect(true);
    }
    
    
}