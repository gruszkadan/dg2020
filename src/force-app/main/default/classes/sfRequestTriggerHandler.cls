public class sfRequestTriggerHandler {
    
    public static void afterUpdate(Salesforce_Request__c[] oldSfRequests, Salesforce_Request__c[] newSfRequests) {
        
        Salesforce_Request__c[] newRequestNotes = new list<Salesforce_Request__c>();
        
        // loop through saleforce requests that are updated
        for (integer i=0; i<newSfRequests.size(); i++) {
            // if latest comment field is updated, add to SF Request list that will be passed to Utility
            if (oldSfRequests[i].Latest_Comment__c != newSfRequests[i].Latest_Comment__c){
                newRequestNotes.add(newSfRequests[i]);
            }
        }
        
        if(newRequestNotes.size() > 0){
            sfNoteUtility.updateComments(newRequestNotes);
            
        }
        
        
    }
    
}