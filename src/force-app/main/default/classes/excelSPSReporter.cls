public class excelSPSReporter {

    public Sales_Performance_Summary__c[] spsResults {get;set;}
    public Sales_Performance_Request__c[] pendingSprResults {get;set;}
    public string selectedYear {get;set;}
    public string selectedMonth {get;set;} 
    public string xmlheader {get;set;}
    public string monthWord {get;set;}
    public string endfile {get;set;}
    public Sales_Performance_Summary__c[] MDSPSs {get;set;}
    public List<List<Sales_Performance_Summary__c>> masterListdirector {get;set;}
    public Set<String> directorNamesSet {get;set;}
  
    
    public excelSPSReporter(){        

        masterListDirector = new List<List<Sales_Performance_Summary__c>>();
        xmlheader ='<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
        endfile = '</Workbook>';
        selectedYear = ApexPages.currentPage().getParameters().get('year');
        selectedMonth = ApexPages.currentPage().getParameters().get('month'); 
        
        
        if (selectedYear != NULL && selectedMonth != Null)  {
            string midMonth = selectedYear + '-' + selectedMonth + '-15 00:00:00';
            Datetime midMonthDT = Datetime.valueOf(midMonth);
            monthWord = midMonthDT.format('MMMM');
            
            spsResults = [SELECT Employee_ID__c, Sales_Rep__r.Name, My_Bookings__c, Bookings_Incentives__c, Demo_Incentives__c,  
                          Associate_Incentives__c, Referral_Incentives__c, Other_Incentives__c, Group__c, Manager__r.Name, Sales_Director__r.Name
                          FROM Sales_Performance_Summary__c 
                          WHERE Year__c = :selectedYear 
                          AND Month__c = :monthWord ORDER BY Sales_Rep__r.Name];   
            
            if(spsResults.size() > 0){
               directorNamesSet = new Set<String>();
               for(Sales_Performance_Summary__c sps: spsResults){
                   if(sps.Sales_Director__r.Name != NULL){
                   directorNamesSet.add(sps.Sales_Director__r.Name);
                   }
               }
             List<string> directorList = new List<String>();
                directorList.addAll(directorNamesSet);
                
                for(integer i = 0; i < directorList.size(); i++ ){
                    List<Sales_Performance_Summary__c> tempList = new List<Sales_Performance_Summary__c>();
                    for( Sales_Performance_Summary__c sps: spsResults){
                       
                        if(sps.Sales_Director__r.Name ==  directorList[i]){
                            tempList.add(sps);
                        }
                    }
                    MasterListDirector.add(tempList);
                }
            }
        }
    }

}