@isTest(seeAllData = true)
public class testVPMTaskUtility {
    static testMethod void test1(){
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1];  
        Account a = new account();
        a.name = 'test1';
        a.OwnerId = Dan.Id;
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'John';
        c.LastName = 'Parke';
        c.AccountId = a.id;
        c.EHS_Contact__c = false;
        c.Upgrade_Email_1_Sent_Date__c = date.today();
        insert c;
        
        VPM_Project__c vProject1 = new VPM_Project__c();
        vProject1.Account__c = a.Id;
        vProject1.CurrencyIsoCode = 'USD';
        vProject1.Scheduled_Upgrade_Date__c = date.today().addDays(-1);
        insert vProject1;
        
        VPM_Milestone__c vpmMilestone1 = new VPM_Milestone__c();
        vpmMilestone1.Account__c = a.Id;
        vpmMilestone1.Contact__c = c.Id;
        vpmMilestone1.VPM_Project__c = vProject1.Id;
        vpmMilestone1.CurrencyIsoCode = 'USD';
        insert vpmMilestone1;
        
        VPM_Task__c vpmtask1 = new VPM_Task__c();
        vpmtask1.Account__c = a.Id;
        vpmtask1.Contact__c = c.Id;
        vpmtask1.OwnerId = Dan.Id;
        vpmtask1.VPM_Milestone__c = vpmMilestone1.Id;
        vpmtask1.Email_Sequence_Number__c = 1;
        vpmtask1.Start_Date__c = null;
        vpmtask1.End_Date__c = null;
        VPMtask1.VPM_Project__c = vProject1.Id;
        Insert vpmtask1;
        
        VPM_Task__c vpmtask2 = new VPM_Task__c();
        vpmtask2.Account__c = a.Id;
        vpmtask2.Contact__c = c.Id;
        vpmtask2.OwnerId = Dan.Id;
        vpmtask2.Contact__c = c.Id;
        vpmtask2.Status__c = 'Not Started';
        vpmtask2.VPM_Milestone__c = vpmMilestone1.Id;
        vpmtask2.Email_Sequence_Number__c = 2;
        //vpmtask2.Start_Date__c = NULL;
        vpmtask2.End_Date__c = null;
        VPMtask2.VPM_Project__c = vProject1.Id;
        Insert vpmtask2;
        
        VPM_Task__c vpmtask3 = new VPM_Task__c();
        vpmtask3.Account__c = a.Id;
        vpmtask3.Contact__c = c.Id;
        vpmtask3.OwnerId = Dan.Id;
        vpmtask3.VPM_Milestone__c = vpmMilestone1.Id;
        vpmtask3.Email_Sequence_Number__c = 3;
        vpmtask3.Start_Date__c = null;
        vpmtask3.Status__c = 'Not Started';
        vpmtask3.End_Date__c = null;
        VPMtask3.VPM_Project__c = vProject1.Id;
        Insert vpmtask3;
        
        //updates vpm Project related tasks start date 
        vProject1.Scheduled_Upgrade_Date__c = date.today().addDays(-2);
        update vProject1;   
        VPM_Task__c vt = [select Id, Email_Sequence_Number__c, Status__c, Start_Date__c, End_Date__c from VPM_Task__c where Id = :VPMtask3.Id];
        System.assert(vt.Start_Date__c == date.today().addDays(-1));
      
    }
    
    static testMethod void test2(){
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1];  
        Account a = new account();
        a.name = 'test1';
        a.OwnerId = Dan.Id;
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'John';
        c.LastName = 'Parke';
        c.AccountId = a.id;
        c.EHS_Contact__c = false;
        c.Upgrade_Email_1_Sent_Date__c = null;
        c.Upgrade_Email_2_Sent_Date__c = null;
        c.Upgrade_Email_3_Sent_Date__c = null;
        insert c;
        
        VPM_Project__c vProject1 = new VPM_Project__c();
        vProject1.Account__c = a.Id;
        vProject1.CurrencyIsoCode = 'USD';
        vProject1.Scheduled_Upgrade_Date__c = date.today().addDays(-1);
        insert vProject1;
        
        VPM_Milestone__c vpmMilestone1 = new VPM_Milestone__c();
        vpmMilestone1.Account__c = a.Id;
        vpmMilestone1.Contact__c = c.Id;
        vpmMilestone1.VPM_Project__c = vProject1.Id;
        vpmMilestone1.CurrencyIsoCode = 'USD';
        insert vpmMilestone1;
        
        VPM_Task__c vpmtask1 = new VPM_Task__c();
        vpmtask1.Account__c = a.Id;
        vpmtask1.Contact__c = c.Id;
        vpmtask1.OwnerId = Dan.Id;
        vpmtask1.VPM_Milestone__c = vpmMilestone1.Id;
        vpmtask1.Email_Sequence_Number__c = 1;
        vpmtask1.Start_Date__c = null;
        vpmtask1.End_Date__c = null;
        VPMtask1.VPM_Project__c = vProject1.Id;
        Insert vpmtask1;
        
        VPM_Task__c vpmtask2 = new VPM_Task__c();
        vpmtask2.Account__c = a.Id;
        vpmtask2.Contact__c = c.Id;
        vpmtask2.OwnerId = Dan.Id;
        vpmtask2.Contact__c = c.Id;
        vpmtask2.Status__c = 'Not Started';
        vpmtask2.VPM_Milestone__c = vpmMilestone1.Id;
        vpmtask2.Email_Sequence_Number__c = 2;
        vpmtask2.Start_Date__c = NULL;
        vpmtask2.End_Date__c = null;
        VPMtask2.VPM_Project__c = vProject1.Id;
        Insert vpmtask2;
        
        VPM_Task__c vpmtask3 = new VPM_Task__c();
        vpmtask3.Account__c = a.Id;
        vpmtask3.Contact__c = c.Id;
        vpmtask3.OwnerId = Dan.Id;
        vpmtask3.VPM_Milestone__c = vpmMilestone1.Id;
        vpmtask3.Email_Sequence_Number__c = 3;
        vpmtask3.Start_Date__c = null;
        vpmtask3.Status__c = 'Not Started';
        vpmtask3.End_Date__c = null;
        VPMtask3.VPM_Project__c = vProject1.Id;
        Insert vpmtask3;
        
        //upon updating email sent date on contact, end date on tasks updates
        c.Upgrade_Email_1_Sent_Date__c = date.today().addDays(-2);
        c.Upgrade_Email_2_Sent_Date__c = date.today().addDays(-1);
        c.Upgrade_Email_3_Sent_Date__c = date.today();
        update c;     
    }
}