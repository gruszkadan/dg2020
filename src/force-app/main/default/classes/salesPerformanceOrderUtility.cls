/*
* Test class:
* testSalesPerformanceItemOrderUtility
* testSalesPerformanceRequestIncentives
*/
public class salesPerformanceOrderUtility {
    
    //Called from orderItemTriggerHandler.afterInsert()
    // Creates SPO's - 1 SPO per order per rep
    public static void insertSPO(Order_Item__c[] orderItems) {
        Sales_Performance_Order__c[] newSPOs = new list<Sales_Performance_Order__c>();
        set<id> ownerIds = new set<id>();
        set<string> SPOkeys = new set<string>();
        set<string> SPSkeys = new set<string>();
        for (Order_Item__c oi : orderItems) { 
            ownerIds.add(oi.OwnerId);
            SPOkeys.add(oi.Order_ID__c + '-' + oi.OwnerId);
            SPSkeys.add(oi.OwnerId + '-' + oi.Month__c + '-' + oi.Year__c);
        }
        User[] owners = [SELECT id, Role__c, Group__c
                         FROM User
                         WHERE id IN : ownerIds];
        Sales_Performance_Order__c[] existingSPOs = [SELECT id, Key__c
                                                     FROM Sales_Performance_Order__c
                                                     WHERE Key__c IN : SPOkeys
                                                     and Type__c = 'Booking'];
        Sales_Performance_Summary__c[] SPSs = [SELECT id, Key__c
                                               FROM Sales_Performance_Summary__c
                                               WHERE Key__c IN : SPSkeys];
        string oiErrors = '';
        for (string key : SPOkeys) {
            boolean hasSPO = false;
            if (existingSPOs.size() > 0) {
                for (Sales_Performance_Order__c spo : existingSPOs) {
                    if (spo.Key__c == key) {
                        hasSPO = true;
                    }
                }
            }
            if (!hasSPO) {
                Order_Item__c[] oiMatches = new list<Order_Item__c>();
                for (Order_Item__c oi : orderItems) {
                    string oiKey = oi.Order_ID__c + '-' + oi.OwnerId;
                    if (oiKey == key) {
                        oiMatches.add(oi);
                    }
                }
                User oiOwner;
                for (User owner : owners) {
                    if (owner.id == oiMatches[0].OwnerId) {
                        oiOwner = owner;
                    }
                }
                string SPSkey = oiMatches[0].OwnerId + '-' + oiMatches[0].Month__c + '-' + oiMatches[0].Year__c;
                Sales_Performance_Summary__c SPSmatch;
                for (Sales_Performance_Summary__c sps : SPSs) {
                    if (sps.Key__c == SPSkey) {
                        SPSmatch = sps;
                    }
                }
                Sales_Performance_Order__c newSPO = new Sales_Performance_Order__c();
                newSPO.Key__c = key;
                newSPO.Order_ID__c = oiMatches[0].Order_ID__c;
                newSPO.OwnerId = oiMatches[0].OwnerId;
                if (SPSmatch != null) {
                    newSPO.Sales_Rep_Sales_Performance_Summary__c = SPSmatch.id;
                } else {
                    for (Order_Item__c oi : oiMatches) {
                        oiErrors += oi.id + ', ';
                    }
                }
                newSPO.Sales_Manager__c = oiMatches[0].Sales_Manager__c;
                newSPO.Sales_Director__c = oiMatches[0].Sales_Director__c;
                newSPO.Sales_VP__c = oiMatches[0].Sales_VP__c;
                newSPO.Group__c = oiOwner.Group__c;
                newSPO.Role__c = oiOwner.Role__c;
                newSPO.Order_Date__c = oiMatches[0].Order_Date__c;
                newSPO.Month__c = oiMatches[0].Month__c;
                newSPO.Year__c = oiMatches[0].Year__c;
                newSPO.Contract_Length__c = oiMatches[0].Contract_Length__c;
                newSPO.Account__c = oiMatches[0].Account__c;
                newSPO.Contract_Number__c = oiMatches[0].Contract_Number__c;
                newSPO.Type__c = 'Booking';
                newSPO.Opportunity__c = oiMatches[0].Opportunity__c;
                newSPOs.add(newSPO);
            } 
        }
        upsertSPOs(newSPOs, 'insertSPO');
        insertAssociateCreditSPO(newSPOs, false);
        if (oiErrors != '') {
            salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceOrderUtility', 'insertSPO', 'No Sales Performance Summary found for ' + oiErrors.removeEnd(', '));
        }
    }
    
    public static void upsertSPOs(sObject[] upsList, string location) {
        if (upsList.size() > 0) {
            try {
                upsert upsList;
            } catch(exception e) {
                salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceOrderUtility', location, string.valueOf(e));
            }
        }
    }
    
    //Called from accountTriggerHandler.afterUpdate()
    // Creates SPO's for ES associate bookings
    public static void insertESAssociteSPO(Account[] accounts) {
        Sales_Performance_Order__c[] newSPOs = new list<Sales_Performance_Order__c>();
        Sales_Performance_Order__c[] currentSPOs = new list<Sales_Performance_Order__c>();
        Sales_Performance_Order__c[] sposToPass = new list<Sales_Performance_Order__c>();
        set<id> demoSetterIds = new set<id>();
        set<string> SPOkeys = new set<string>();
        set<string> SPSkeys = new set<string>();
        for (Account a : accounts) { 
            demoSetterIds.add(a.Demo_Setter1__c);
            string spoKey = a.id+'-'+datetime.now().format('yyyy')+'-'+a.Demo_Setter1__c;
            SPOkeys.add(spoKey);
            DateTime d = datetime.now();
            String monthName = d.format('MMMMM');
            String year = d.format('yyyy');
            string spsKey = a.Demo_Setter1__c+'-'+monthName+'-'+year;
            SPSkeys.add(spsKey);
        }
        User[] owners = [SELECT id, Role__c, Group__c
                         FROM User
                         WHERE id IN : demoSetterIds and Group__c = 'Enterprise Sales'];
        Sales_Performance_Order__c[] existingSPOs = [SELECT id, Key__c, Account__c, CreatedDate, OwnerId, Sales_Manager__c, Sales_Director__c, Sales_VP__c
                                                     FROM Sales_Performance_Order__c
                                                     WHERE Key__c IN : SPOkeys
                                                     and Type__c = 'Base Licensing'];
        Sales_Performance_Summary__c[] SPSs = [SELECT id, Key__c, Manager__c, Sales_Director__c, Sales_VP__c, Group__c, Role__c
                                               FROM Sales_Performance_Summary__c
                                               WHERE Key__c IN : SPSkeys];
        
        if(owners != null && owners.size() > 0){
            for(Account a:accounts){
                //get all owner information
                User owner;
                for(User u:owners){
                    if(a.Demo_Setter1__c == u.id){
                        owner = u;
                    }
                }
                //check for existing SPO
                string spoKey = a.id+'-'+datetime.now().format('yyyy')+'-'+a.Demo_Setter1__c;
                Sales_Performance_Order__c existingSPO;
                if(existingSPOs != null && existingSPOs.size() > 0){
                    for(Sales_Performance_Order__c spo:existingSPOs){
                        if(spoKey == spo.Key__c){
                            if(spo.CreatedDate >= datetime.now().addDays(-30)){
                                existingSPO = spo;
                            }
                        }
                    }  
                }
                //Find SPS
                DateTime d = datetime.now();
                String monthName = d.format('MMMMM');
                String year = d.format('yyyy');
                string spsKey = a.Demo_Setter1__c+'-'+monthName+'-'+year;
                Sales_Performance_Summary__c SPS;
                if(SPSs != null && SPSs.size() > 0){
                    for(Sales_Performance_Summary__c s:SPSs){
                        if(spsKey == s.Key__c){
                            SPS = s;
                        }
                    }
                }
                
                if(SPS != null && owner != null){
                    if(existingSPO != null){
                        currentSPOs.add(existingSPO);
                    }else{
                        Sales_Performance_Order__c newSPO = new Sales_Performance_Order__c();
                        newSPO.Key__c = spoKey;
                        newSPO.OwnerId = owner.id;
                        newSPO.Sales_Rep_Sales_Performance_Summary__c = SPS.id;
                        newSPO.Sales_Manager__c = SPS.Manager__c;
                        newSPO.Sales_Director__c = SPS.Sales_Director__c;
                        newSPO.Sales_VP__c = SPS.Sales_VP__c;
                        newSPO.Group__c = SPS.Group__c;
                        newSPO.Role__c = SPS.Role__c;
                        newSPO.Order_Date__c = date.today();
                        newSPO.Month__c = monthName;
                        newSPO.Year__c = year;
                        newSPO.Account__c = a.Id;
                        newSPO.Type__c = 'Base Licensing';
                        newSPOs.add(newSPO);
                    }
                }
            }
            if(currentSPOs.size() > 0){
                sposToPass.addAll(currentSPOs);
            }
            if(newSPOs.size() > 0){
                try {
                    insert newSPOs;
                    sposToPass.addAll(newSPOs);
                } catch(exception e) {
                    salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceOrderUtility', 'insertESAssociteSPO', string.valueOf(e));
                }
            }
            if(sposToPass.size() > 0){
                salesPerformanceItemUtility.insertESAssociateSPI(sposToPass,accounts);
            }
        }
    }
    
    public static void updateSPOFromSPI(Sales_Performance_Item__c[] salesPerformanceItems) {
        set<id> spoIds = new set<id>();
        for(Sales_Performance_Item__c spi : salesPerformanceItems){
            spoIds.add(spi.Sales_Performance_Order__c);
        }
        Sales_Performance_Order__c[] SPOs = new list<Sales_Performance_Order__c>();
        //query the SPO's and subquery all related spi's
        SPOs = [SELECT id, Account__c, OwnerId, Sales_Manager__c, Sales_Director__c, Sales_VP__c, Sales_Rep_Sales_Performance_Summary__c,
                Role__c, Group__c, Key__c,
                (SELECT id, Account__c, OwnerId, Sales_Manager__c, Sales_Director__c, Sales_VP__c, Sales_Rep_Sales_Performance_Summary__c,
                 Role__c, Group__c, Order_Id__c FROM Sales_Performance_Items__r WHERE id in :salesPerformanceItems)
                FROM Sales_Performance_Order__c
                WHERE id IN : spoIds];
        
        //loop through the SPO's
        for (Sales_Performance_Order__c spo : SPOs) {
            //loop over any related spi's and set the updated field values
            for(Sales_Performance_Item__c spi:spo.Sales_Performance_Items__r){
                //Account Change
                spo.Account__c = spi.Account__c;
                //For Rep Change
                spo.OwnerId = spi.OwnerId;
                spo.Sales_Manager__c = spi.Sales_Manager__c;
                spo.Sales_Director__c = spi.Sales_Director__c;
                spo.Sales_VP__c = spi.Sales_VP__c;
                spo.Sales_Rep_Sales_Performance_Summary__c = spi.Sales_Rep_Sales_Performance_Summary__c;
                spo.Role__c = spi.Role__c;
                spo.Group__c = spi.Group__c;
                spo.Key__c = spi.Order_ID__c+'-'+spi.OwnerId;
                
            }
        }
        upsertSPOs(SPOs, 'updateSPOFromSPI');
    }
    
    public static Sales_Performance_Item_Split__c[] insertSplitSPO(Sales_Performance_Item_Split__c[] splits, String reason) {
        Sales_Performance_Order__c splitMasterSPO = [SELECT id, Order_ID__c, Owner.Name, OwnerId, Contract_Number__c,
                                                     Account__r.Name, Order_Date__c, Account__r.AdminId__c, Contract_Length__c, Month__c, Year__c, Account__c, isSplit__c, isSplitMaster__c
                                                     FROM Sales_Performance_Order__c
                                                     WHERE id =:splits[0].SPO_Split_From__c
                                                     LIMIT 1];
        Sales_Performance_Order__c[] upsertSpos = new list<Sales_Performance_Order__c>();
        set<id> spsOwnerIds = new set<id>();
        for(Sales_Performance_Item_Split__c s: splits){
            spsOwnerIds.add(s.OwnerId);
        }
        Sales_Performance_Summary__c[] SPSs = [SELECT id,Sales_Rep__c,Key__c,Manager__c,Sales_Director__c,Sales_VP__c,Group__c,Role__c
                                               FROM Sales_Performance_Summary__c 
                                               WHERE Sales_Rep__c in:spsOwnerIds 
                                               and Year__c =:splitMasterSPO.Year__c
                                               and Month__c =:splitMasterSPO.Month__c];
        //set the master order fields and add to upsert list
        splitMasterSPO.isSplit__c = true;
        splitMasterSPO.isSplitMaster__c = true;
        splitMasterSPO.Split_Reason__c = reason;
        upsertSpos.add(splitMasterSPO);
        //loop over the splits, create new orders for the non-master splits, and add to upsert list
        for(id splitOwner:spsOwnerIds){
            if(splitOwner != splitMasterSPO.OwnerId){
                Sales_Performance_Order__c splitSpo = new Sales_Performance_Order__c();
                string SPOkey = splitMasterSPO.Order_ID__c + '-' + splitOwner;
                string SPSkey = splitOwner + '-' + splitMasterSPO.Month__c + '-' + splitMasterSPO.Year__c;
                for (Sales_Performance_Summary__c sps : SPSs) {
                    if (sps.Key__c == SPSkey) {
                        splitSpo.Sales_Rep_Sales_Performance_Summary__c = sps.id;
                        splitSpo.Sales_Manager__c = sps.Manager__c;
                        splitSpo.Sales_Director__c = sps.Sales_Director__c;
                        splitSpo.Sales_VP__c = sps.Sales_VP__c;
                        splitSpo.Group__c = sps.Group__c;
                        splitSpo.Role__c = sps.Role__c;
                        break;
                    }
                }
                splitSpo.Key__c = SPOkey;
                splitSpo.Order_ID__c = splitMasterSPO.Order_ID__c;
                splitSpo.OwnerId = splitOwner;
                splitSpo.Order_Date__c = splitMasterSPO.Order_Date__c;
                splitSpo.Month__c = splitMasterSPO.Month__c;
                splitSpo.Year__c = splitMasterSPO.Year__c;
                splitSpo.Contract_Length__c = splitMasterSPO.Contract_Length__c;
                splitSpo.Account__c = splitMasterSPO.Account__c;
                splitSpo.Contract_Number__c = splitMasterSPO.Contract_Number__c;
                splitSpo.Type__c = 'Booking';
                splitSpo.isSplit__c = true;
                splitSpo.Split_Reason__c = reason;
                upsertSpos.add(splitSpo);
            }
        }
        
        if(upsertSpos.size() > 0){
            try{
                upsert upsertSpos;
                for(Sales_Performance_Order__c spo:upsertSpos){
                    for(Sales_Performance_Item_Split__c s: splits){ 
                        if(spo.OwnerId == s.OwnerId){
                            s.SPO_Split_To__c = spo.id;
                        }
                    }
                }
                return splits; 
            }catch(exception e){
                return null;
            }
        }else{
            return null; 
        }
        
    }
    
    // Creates SPO's for Associate Credits
    public static void insertAssociateCreditSPO(Sales_Performance_Order__c[] repSPOs, boolean createSPIs) {
        //Find Record Type Id for Associates
        RecordType[] associateRecordTypes = [SELECT ID
                                             FROM RecordType
                                             WHERE SobjectType = 'Sales_Incentive_Setting__c' and (Name='Enterprise Associate' or Name='Mid-Market Associate')];
        
        //Query the sales incentive settings to find any associate-rep mappings
        id[] repIds = new list<id>();
        for(Sales_Performance_Order__c rspo:repSPOs){
            repIds.add(rspo.OwnerId);
        }
        Sales_Incentive_Setting__c[] associateRepMapping = [SELECT id, Associate__c, Sales_Representative__c, Percent_of_Bookings__c
                                                            FROM Sales_Incentive_Setting__c where RecordTypeId in:associateRecordTypes and Sales_Representative__c in:repIds];
        
        if(associateRepMapping.size() > 0){
            //create the keys to query the associate SPS's
            set<String> associateSPSKeys = new set<String>();
            for(Sales_Performance_Order__c rspo:repSPOs){
                for(Sales_Incentive_Setting__c m:associateRepMapping){
                    if(rspo.OwnerId == m.Sales_Representative__c){
                        associateSPSKeys.add(m.Associate__c+'-'+rspo.Month__c+'-'+rspo.Year__c);
                    }
                }
            }
            Sales_Performance_Summary__c[] associateSPSs = [SELECT id,Sales_Rep__c,Key__c,Manager__c,Sales_Director__c,Sales_VP__c,Group__c,Role__c
                                                            FROM Sales_Performance_Summary__c 
                                                            WHERE Key__c in:associateSPSKeys];
            
            //Create a list to hold the new associate credit SPO's 
            Sales_Performance_Order__c[] associateSPOs = new list<Sales_Performance_Order__c>();
            
            //loop over the rep SPO's
            for(Sales_Performance_Order__c rspo:repSPOs){
                //loop over associate rep mappings
                for(Sales_Incentive_Setting__c m:associateRepMapping){
                    //find matching mapping
                    if(rspo.OwnerId == m.Sales_Representative__c){
                        //clone the rep SPO and change fields relating to new associate
                        Sales_Performance_Order__c associateCreditSPO = rspo.clone();
                        associateCreditSPO.Key__c = rspo.Order_ID__c + '-' + m.Associate__c;
                        string spsKey = m.Associate__c+'-'+rspo.Month__c+'-'+rspo.Year__c;
                        for(Sales_Performance_Summary__c sps:associateSPSs){
                            if(sps.Key__c == spsKey){
                                associateCreditSPO.Sales_Rep_Sales_Performance_Summary__c = sps.id;
                                associateCreditSPO.Group__c = sps.Group__c;
                                associateCreditSPO.Role__c = sps.Role__c;
                            }
                        }
                        associateCreditSPO.Associate_Credit_Sales_Rep__c = rspo.OwnerId;
                        associateCreditSPO.OwnerId = m.Associate__c;
                        associateCreditSPO.Type__c = 'Associate Credit';
                        associateSPOs.add(associateCreditSPO);
                    }
                }
            }
            if(associateSPOs.size() > 0){
                try {
                    insert associateSPOs;
                    if(createSPIs){
                        salesPerformanceItemUtility.insertAssociateCreditSPIOnOwnerChange(associateSPOs);
                    }
                } catch(exception e) {
                    salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceOrderUtility', 'insertAssociateCreditSPO', string.valueOf(e));
                }
            }
        }
    }
    
    // Creates SPO's for Ghost Bookings
    public static void insertGhostBookingSPO(Sales_Performance_Request__c request) {
        Sales_Performance_Order__c newSpo = new Sales_Performance_Order__c();
        DateTime d = datetime.now();
        String monthName = d.format('MMMMM');
        if(request.Incentive_Month__c == 'Previous Month'){
            date lastMonth = date.today().toStartOfMonth().addDays(-1);
            d = lastMonth;
            monthName = d.format('MMMMM');
        }
        String year = d.format('yyyy');
        string spsKey = request.Sales_Rep__c+'-'+monthName+'-'+year;
        Sales_Performance_Summary__c SPS = [SELECT id, Key__c, Manager__c, Sales_Director__c, Sales_VP__c, Group__c, Role__c
                                            FROM Sales_Performance_Summary__c
                                            WHERE Key__c =:SPSkey
                                            LIMIT 1];
        if(SPS != null){
            newSPO.OwnerId = request.Sales_Rep__c;
            newSPO.Sales_Rep_Sales_Performance_Summary__c = SPS.id;
            newSPO.Sales_Manager__c = SPS.Manager__c;
            newSPO.Sales_Director__c = SPS.Sales_Director__c;
            newSPO.Sales_VP__c = SPS.Sales_VP__c;
            newSPO.Group__c = SPS.Group__c;
            newSPO.Role__c = SPS.Role__c;
            newSPO.Order_Date__c = date.valueof(d);
            newSPO.Month__c = monthName;
            newSPO.Year__c = year;
            newSPO.Type__c = 'Ghost Booking';
            try {
                insert newSPO;
                salesPerformanceItemUtility.insertGhostBookingSpi(newSPO,request);
            } catch(exception e) {
                salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceOrderUtility', 'insertESAssociteSPO', string.valueOf(e));
            }
        }
        
        
    }
    
    //Updates SPO's Opportunity Field
    public static void updateSPOOpportunity(Order_Item__c[] orderItems){
        //Build a map of Order ID's and each ones corresponding opportunity
        map<String,id> orderIdOppMap = new map<string,id>();
        for(Order_Item__c oi:orderItems){
            orderIdOppMap.put(oi.Order_Id__c,oi.Opportunity__c);
        }
        
        //Query any SPO's related to the Order Id's found
        Sales_Performance_Order__c[] spos = [SELECT id, Order_ID__c 
                                             FROM Sales_Performance_Order__c
                                             WHERE Order_ID__c in:orderIdOppMap.keySet()];
        
        //Loop over the spos and set the opportunity based on the order ID
        for(Sales_Performance_Order__c spo:spos){
            spo.Opportunity__c = orderIdOppMap.get(spo.Order_ID__c);
        }
        
        //Update the list of SPO's
        try {
            update spos;
        } catch(exception e) {
            salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceOrderUtility', 'updateSPOOpportunity', string.valueOf(e));
        }
        
    }
    
    // Creates SPO's for Opportunity Credits
    public static void insertOpportunityCreditSPO(Sales_Performance_Order__c[] spos) {
        
        //Build a set of opportunity ids to query for the opportunity creator
        set<id> opportunityIds = new set<id>();
        for(Sales_Performance_Order__c spo:spos){
            opportunityIds.add(spo.Opportunity__c);
        }
        
        //Query a map of opportunities - this will be used to pull back the createdbyid
        map<id,Opportunity> opportunityMap = new map<id,Opportunity>([SELECT id,CreatedById,Created_By_Role__c FROM Opportunity WHERE id in:opportunityIds]);
        
        //Set to hold all opportunity creators that will need to be checked for incentive mappings
        set<id> opportunityCreatedByIds = new set<id>();
        for (ID idKey : opportunityMap.keyset()) {
            opportunityCreatedByIds.add(opportunityMap.get(idKey).CreatedById);
        }
        
        //Query opportunity credit mappings
        Sales_Incentive_Setting__c[] opportunityCreditMapping = [SELECT id, Associate__c, Percent_of_Bookings__c, Incentive_Stop_Date__c
                                                                 FROM Sales_Incentive_Setting__c WHERE RecordType.Name = 'Mid-Market Opportunity' AND Associate__c in:opportunityCreatedByIds];
        
        if(opportunityCreditMapping.size() > 0){
            //create the keys to query the associate SPS's
            set<String> oppCreditSPSKeys = new set<String>();
            for(Sales_Performance_Order__c spo:spos){
                oppCreditSPSKeys.add(opportunityMap.get(spo.Opportunity__c).CreatedById+'-'+spo.Month__c+'-'+spo.Year__c);
            }
            Sales_Performance_Summary__c[] oppCreditSPSs = [SELECT id,Sales_Rep__c,Key__c,Manager__c,Sales_Director__c,Sales_VP__c,Group__c,Role__c
                                                            FROM Sales_Performance_Summary__c 
                                                            WHERE Key__c in:oppCreditSPSKeys];
            
            //Create a list to hold the new associate credit SPO's 
        Sales_Performance_Order__c[] oppCreditSPOs = new list<Sales_Performance_Order__c>();
        
        //loop over opportunity credit mappings
        for(Sales_Incentive_Setting__c m:opportunityCreditMapping){
            //loop over the booking SPO's
            for(Sales_Performance_Order__c spo:spos){
                //find matching spo for the opp mapping associate
                if(opportunityMap.get(spo.Opportunity__c).CreatedById == m.Associate__c &&
                    (opportunityMap.get(spo.Opportunity__c).Created_By_Role__c == 'Associate' || opportunityMap.get(spo.Opportunity__c).Created_By_Role__c == 'Trainee') &&
                    (m.Incentive_Stop_Date__c == NULL ||
                    (m.Incentive_Stop_Date__c != NULL && spo.Order_Date__c <= m.Incentive_Stop_Date__c))){
                        //clone the booking SPO and change fields relating to new opp credit
                        Sales_Performance_Order__c oppCreditSPO = spo.clone();
                        oppCreditSPO.Key__c = spo.Order_ID__c + '-' + m.Associate__c;
                        string spsKey = m.Associate__c+'-'+spo.Month__c+'-'+spo.Year__c;
                        for(Sales_Performance_Summary__c sps:oppCreditSPSs){
                            if(sps.Key__c == spsKey){
                                oppCreditSPO.Sales_Rep_Sales_Performance_Summary__c = sps.id;
                                oppCreditSPO.Group__c = sps.Group__c;
                                    oppCreditSPO.Role__c = sps.Role__c;
                                    oppCreditSPO.Sales_Manager__c = sps.Manager__c;
                                    oppCreditSPO.Sales_Director__c = sps.Sales_Director__c;
                                    oppCreditSPO.Sales_VP__c = sps.Sales_VP__c;
                                }
                            }
                            oppCreditSPO.OwnerId = m.Associate__c;
                            oppCreditSPO.Type__c = 'Opportunity Credit';
                            oppCreditSPOs.add(oppCreditSPO);
                        }
                }
            }
            if(oppCreditSPOs.size() > 0){
                try {
                    insert oppCreditSPOs;
                } catch(exception e) {
                    salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceOrderUtility', 'insertOpportunityCreditSPO', string.valueOf(e));
                }
            }
        }
    }
    
}