public class adminToolSalesforceIDAPISend implements Schedulable{
/**
* This calls the Admin Tool Web Services to send Salesforce ID Data for Customers (AdminID__c Starts with a 4) to the Admin Tool
* This calls the adminToolSalesforceIDQueueable
**/
	 public void execute(SchedulableContext SC) {
        System.enqueueJob(new adminToolSalesforceIDQueueable());
    }
}