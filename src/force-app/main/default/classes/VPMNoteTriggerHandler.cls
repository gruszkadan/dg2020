public class VPMNoteTriggerHandler {
 
 // BEFORE INSERT
    public static void beforeInsert(VPM_Note__c[] notes) {

    }
    
    // BEFORE UPDATE
    public static void beforeUpdate(VPM_Note__c[] oldNotes, VPM_Note__c[] newNotes) {}
    
    // BEFORE DELETE
    public static void beforeDelete(VPM_Note__c[] oldNotes, VPM_Note__c[] newNotes) {}
    
    // AFTER INSERT
    public static void afterInsert(VPM_Note__c[] oldNotes, VPM_Note__c[] newNotes) {
       
        VPM_Note__c[] trainingProjectNotes = new List<VPM_Note__c>();
        
        for (integer i=0; i<newNotes.size(); i++) {
            //only consider notes that are training and webinar - training that are added via triggers (case_note__c != NULL)
            if (newNotes[i].Case_Note__c != NULL && (newNotes[i].Type__c == 'Training' || newNotes[i].Type__c == 'Webinar - Training')) {
                trainingProjectNotes.add(newNotes[i]);
            }
        }
        
        if(trainingProjectNotes.size() > 0){
            VPMTaskUtility.VPMTaskCreation(trainingProjectNotes);
        } 
    }

    
    // AFTER UPDATE
    public static void afterUpdate(VPM_Note__c[] oldNotes, VPM_Note__c[] newNotes) {
        }

    // AFTER DELETE
    public static void afterDelete(VPM_Note__c[] notes) {}
  
}