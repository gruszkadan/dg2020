@isTest(seeAllData=true)
private class testcontractReviewController {
    
    //new contract
    static testmethod void test1() {        
        // create test account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        // create test contact
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        // create test address
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        // create test contract
        Contract__c c = new Contract__c();
        c.Account__c = newAccount.Id;
        c.Contact__c = newContact.Id;
        c.Address__c = address.Id;
        c.Contract_Type__c ='New';
        c.Contract_Length__c = '1 Year';
        //standard MSA
        c.Standard_MSA__c = 'No';
        c.Non_Standard_MSA_Type__c = 'Amended';
        c.Standard_Payment_Terms__c = 'No';
        //net terms
        c.Custom_Payment_Net_Terms__c = true;
        c.Payment_Net_Terms__c = 'Net 90';
        //deferred invoice and sub
        c.Deferred_Invoice__c = true;
        c.Deferred_Invoice_Date__c = date.today().addDays(120);
        //deferred invoice only
        c.Delayed_Billing_Only__c = true;
        c.Delayed_Billing_Date__c = date.today().addDays(120);
        //installments
        c.Installments__c = true;
        c.Installment_Notes__c = 'test notes';
        //modified down payment
        c.Modified_Down_Payment__c = true;
        //other payment terms
        c.Other_Special_Payment_Terms__c = true;
        c.Other_Special_Payment_Term_Notes__c = 'test notes';
        //opt out
        c.Opt_Out__c = true;
        c.Opt_Out_Notes__c = 'test notes';
        //split billing
        c.Split_Billing__c = true;
        c.Split_Billing_Number_of_Splits__c = 4;
        c.Split_Billing_Notes__c = 'test notes';
        c.US_CS_Approval__c = true;
        c.Webpliance_PPI_Approval__c = true;
        c.Rep_Sales_Director__c = [Select ID from User where lastName = 'Jackson' and FirstName='Eric' and isActive = TRUE Limit 1].ID;
        c.Unsigned_Contract__c = true;

        insert c;  
        
        // create a test product
        Product2 np1 = new Product2();
        np1.Name = 'Webpliance';
        np1.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np1.Contract_Print_Group__c = 'MSDS Management';
        insert np1;
        
        Product2 np2 = new Product2();
        np2.Name = 'Custom Label';
        np2.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np2.Contract_Print_Group__c = 'MSDS Management';
        insert np2;
        
        // create a test line item
        Contract_Line_Item__c cli1 = new Contract_Line_Item__c();
        cli1.Name__c = 'Webpliance';
        cli1.Contract__c = c.Id;
        cli1.Product__c = np1.id;
        cli1.Sort_order__c = 1;
        cli1.Quantity__c = 10;
        cli1.Y1_List_Price__c = 5000;
        cli1.Executed_Services__c = 'executed services';
        cli1.Approval_Required__c = true;
        insert cli1;
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Name__c = 'Custom Label';
        cli2.Contract__c = c.Id;
        cli2.Product__c = np2.id;
        cli2.Sort_order__c = 2;
        cli2.Executed_Services__c = 'executed services';
        cli2.Approval_Required__c = true;
        insert cli2;
        
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Name__c = 'Air';
        cli3.Contract__c = c.Id;
        cli3.Product__c = [SELECT id FROM product2 WHERE name = 'Air' AND Product_Package__c = FALSE LIMIT 1].id;
        cli3.Sort_order__c = 3;
        cli3.Approval_Required__c = true;
        insert cli3;
        
        //intially check the contract
        salesContractApproval sca1 = new salesContractApproval();
        sca1.check(c.id, false); 
        
        //run the check again for submission
        salesContractApproval sca2 = new salesContractApproval();
        sca2.check(c.id, true); 
        
        //debug!
        Approval__c app = (Approval__c)new dynamicQuery().query('Approval__c', null, 'Contract__c = \''+c.id+'\'');
        System.debug(app.Approval_Reasons__c);
        System.debug(app.Approvers__c);
        
        //create an approval process
        Approval.processSubmitRequest req1 = new Approval.processSubmitRequest();
        req1.setComments('Test');
        req1.setObjectId(c.Id);
        Id[] testAppList = new List<Id>();
        testAppList.add([SELECT Id FROM User WHERE LastName = 'McCauley' and isActive=TRUE LIMIT 1].Id);
        req1.setNextApproverIds(testAppList);
        Approval.ProcessResult result = Approval.process(req1);
        
        //initialize the controller
        ApexPages.currentPage().getParameters().put('id', c.Id);
        ApexPages.currentPage().getParameters().put('process', '1');
        string managerId = [SELECT id FROM User WHERE isActive = true AND LastName='Morin' LIMIT 1].id;
        ApexPages.currentPage().getParameters().put('usrAppid', managerId);
        contractReviewController con = new contractReviewController();
        
        con.currentApp = 'Manager';
        con.approverWrappers[0].isChecked = true;
        con.reassignApprover();
        con.saveLanguage();
        con.approve();
        con.reject();
        con.previewCOF();
        con.selectApprover();
        
        //create another approval request since we rejected the last one
        Approval.ProcessResult result2 = Approval.process(req1);
        con.currentApp = 'Sales';
        con.checkItemsToShow();
        con.approve();
        con.currentApp = 'Orders';
        con.checkItemsToShow();
        con.approve();
        con.currentApp = 'Finance';
        con.checkItemsToShow();
        con.approve();
        con.currentApp = 'Operations';
        con.checkItemsToShow();
        con.approve();
        con.approve();
        con.approve();
    }
    
    //
    //
     static testmethod void test2() {        
        // create test account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        // create test contact
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        // create test address
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        // create test contract
        Contract__c c = new Contract__c();
        c.Account__c = newAccount.Id;
        c.Contact__c = newContact.Id;
        c.Address__c = address.Id;
        c.Contract_Type__c ='Renewal';
        c.Contract_Length__c = '1 Year';
        c.Unsigned_Contract__c = true;
         
        //standard MSA
        c.Standard_MSA__c = 'No';
        c.Non_Standard_MSA_Type__c = 'Amended';
        c.MSA_Differences__c = 'Test MSA Differences';
        
        //coverage
        c.Num_of_Locations__c = 66;
        c.Coverage_Type__c = 'Location(s)';
        c.EHS_Coverage__c = 55;
        c.EHS_Coverage_Type__c = 'Employees';
         
         //Payment terms
         c.Standard_Payment_Terms__c = 'No';      
       
        //net terms
        c.Custom_Payment_Net_Terms__c = true;
        c.Payment_Net_Terms__c = 'Net 90';
        //deferred invoice and sub
        c.Deferred_Invoice__c = true;
        c.Deferred_Invoice_Date__c = date.today().addDays(120);
       
        //installments
        c.Installments__c = true;
        c.Billing_Frequency__c = 'Monthly';
        c.Installment_Notes__c = 'test notes';
         
        //modified down payment
        c.Modified_Down_Payment__c = true;
        c.Modified_Down_Payment_Percent__c = 10;
        //other payment terms
        c.Other_Special_Payment_Terms__c = true;
        c.Other_Special_Payment_Term_Notes__c = 'test notes';
        //opt out
        c.Opt_Out__c = true;
        c.Opt_Out_Notes__c = 'test notes';
        //split billing
        c.Split_Billing__c = true;
        c.Split_Billing_Number_of_Splits__c = 4;
        c.Split_Billing_Notes__c = 'test notes';
        //c.US_CS_Approval__c = true;
        //c.Webpliance_PPI_Approval__c = true;
        c.Rep_Sales_Director__c = [Select ID from User where lastName = 'Jackson' and FirstName='Eric' and isActive = TRUE Limit 1].ID;
       
         //milestones
        c.Custom_Billing_Milestones__c = true;
        c.Billing_Milestones_JSON__c ='[{"milestonesList":[{"percentage":30,"milestoneNumber":1,"details":null,"description":"Project Kickoff Complete"},{"percentage":60,"milestoneNumber":2,"details":null,"description":"Implementation Workshop Complete"},{"percentage":10,"milestoneNumber":3,"details":null,"description":"System Production Ready"}],"implementationType":"Simple","customizationType":"Standard"}]';

        insert c;  
        
        // create a test product
        Product2 np1 = new Product2();
        np1.Name = 'Webpliance';
        np1.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np1.Contract_Print_Group__c = 'MSDS Management';
        insert np1;
        
        Product2 np2 = new Product2();
        np2.Name = 'Custom Label';
        np2.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np2.Contract_Print_Group__c = 'MSDS Management';
        insert np2;
        
        // create a test line item
        Contract_Line_Item__c cli1 = new Contract_Line_Item__c();
        cli1.Name__c = 'Webpliance';
        cli1.Contract__c = c.Id;
        cli1.Product__c = np1.id;
        cli1.Sort_order__c = 1;
        cli1.Quantity__c = 10;
        cli1.Y1_List_Price__c = 45000;
        cli1.Executed_Services__c = 'executed services';
        cli1.Approval_Required__c = true;
        insert cli1;
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Name__c = 'Custom Label';
        cli2.Contract__c = c.Id;
        cli2.Product__c = np2.id;
        cli2.Sort_order__c = 2;
        cli2.Y1_List_Price__c = 20000;
        cli2.Executed_Services__c = 'executed services';
        cli2.Approval_Required__c = true;
        insert cli2;
        
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Name__c = 'Air';
        cli3.Contract__c = c.Id;
        cli3.Product__c = [SELECT id FROM product2 WHERE name = 'Air' AND Product_Package__c = FALSE LIMIT 1].id;
        cli3.Sort_order__c = 3;
        cli3.Y1_List_Price__c = 20000;
        cli3.Approval_Required__c = true;
        insert cli3;
        
        //intially check the contract
        salesContractApproval sca1 = new salesContractApproval();
        sca1.check(c.id, false); 
        
        //run the check again for submission
        salesContractApproval sca2 = new salesContractApproval();
        sca2.check(c.id, true); 
        
        //debug!
        Approval__c app = (Approval__c)new dynamicQuery().query('Approval__c', null, 'Contract__c = \''+c.id+'\'');
        System.debug(app.Approval_Reasons__c);
        System.debug(app.Approvers__c);
        
        //create an approval process
        Approval.processSubmitRequest req1 = new Approval.processSubmitRequest();
        req1.setComments('Test');
        req1.setObjectId(c.Id);
        Id[] testAppList = new List<Id>();
        testAppList.add([SELECT Id FROM User WHERE LastName = 'Mosca' and isActive=TRUE LIMIT 1].Id);
        req1.setNextApproverIds(testAppList);
        Approval.ProcessResult result = Approval.process(req1);
         
         
        Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
        req.setComments('Test');
        req.setAction('Approve');
        string work = '';
        for (ProcessInstanceWorkItem workItem : [SELECT p.id FROM ProcessInstanceWorkItem p WHERE p.ProcessInstance.TargetObjectid = :c.id]) {
            work = String.ValueOf(workItem.id);            
        }
        req.setWorkItemid(work);
        Approval.Processresult result2 = Approval.process(req);
         
        
        //initialize the controller
        ApexPages.currentPage().getParameters().put('id', c.Id);
        ApexPages.currentPage().getParameters().put('process', '1');
        string managerId = [SELECT id FROM User WHERE isActive = true AND LastName='Morin' LIMIT 1].id;
        ApexPages.currentPage().getParameters().put('usrAppid', managerId);
        contractReviewController con = new contractReviewController();
        
         //manager
       // con.currentApp = 'Manager';
        //con.approverWrappers[0].isChecked = true;
        //con.reassignApprover();
        //con.saveLanguage();
        //con.approve();
         
        //director
        con.currentApp = 'Director'; 
        con.approve();

         
         
       // con2.currentApp = 'Sales';
        //con2.approve();
        
       //con.currentApp = 'Orders';
       //  con.approve();
         
         // con.currentApp = 'EHS';
        // con.approve();
         
       // con.currentApp = 'Finance'; 
       // con.approve();
         
       //  con.currentApp = 'Operations';
       //  con.approve();
         
    
     }
    
         static testmethod void test2b() {        
        // create test account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        // create test contact
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        // create test address
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        // create test contract
        Contract__c c = new Contract__c();
        c.Account__c = newAccount.Id;
        c.Contact__c = newContact.Id;
        c.Address__c = address.Id;
        c.Contract_Type__c ='Renewal';
        c.Contract_Length__c = '1 Year';
        c.Unsigned_Contract__c = true;
         
        //standard MSA
        c.Standard_MSA__c = 'No';
        c.Non_Standard_MSA_Type__c = 'Amended';
        c.MSA_Differences__c = 'Test MSA Differences';
        
        //coverage
        c.Num_of_Locations__c = 66;
        c.Coverage_Type__c = 'Location(s)';
        c.EHS_Coverage__c = 55;
        c.EHS_Coverage_Type__c = 'Employees';
         
         //Payment terms
         c.Standard_Payment_Terms__c = 'No';      
       
        //net terms
        c.Custom_Payment_Net_Terms__c = true;
        c.Payment_Net_Terms__c = 'Net 90';
        //deferred invoice and sub
        c.Deferred_Invoice__c = true;
        c.Deferred_Invoice_Date__c = date.today().addDays(120);
       
        //installments
        c.Installments__c = true;
        c.Billing_Frequency__c = 'Monthly';
        c.Installment_Notes__c = 'test notes';
         
        //modified down payment
        c.Modified_Down_Payment__c = true;
        c.Modified_Down_Payment_Percent__c = 10;
        //other payment terms
        c.Other_Special_Payment_Terms__c = true;
        c.Other_Special_Payment_Term_Notes__c = 'test notes';
        //opt out
        c.Opt_Out__c = true;
        c.Opt_Out_Notes__c = 'test notes';
        //split billing
        c.Split_Billing__c = true;
        c.Split_Billing_Number_of_Splits__c = 4;
        c.Split_Billing_Notes__c = 'test notes';
        //c.US_CS_Approval__c = true;
        //c.Webpliance_PPI_Approval__c = true;
        c.Rep_Sales_Director__c = [Select ID from User where lastName = 'Jackson' and FirstName='Eric' and isActive = TRUE Limit 1].ID;
       
         //milestones
        c.Custom_Billing_Milestones__c = true;
        c.Billing_Milestones_JSON__c ='[{"milestonesList":[{"percentage":30,"milestoneNumber":1,"details":null,"description":"Project Kickoff Complete"},{"percentage":60,"milestoneNumber":2,"details":null,"description":"Implementation Workshop Complete"},{"percentage":10,"milestoneNumber":3,"details":null,"description":"System Production Ready"}],"implementationType":"Simple","customizationType":"Standard"}]';

        insert c;  
        
        // create a test product
        Product2 np1 = new Product2();
        np1.Name = 'Webpliance';
        np1.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np1.Contract_Print_Group__c = 'MSDS Management';
        insert np1;
        
        Product2 np2 = new Product2();
        np2.Name = 'Custom Label';
        np2.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np2.Contract_Print_Group__c = 'MSDS Management';
        insert np2;
        
        // create a test line item
        Contract_Line_Item__c cli1 = new Contract_Line_Item__c();
        cli1.Name__c = 'Webpliance';
        cli1.Contract__c = c.Id;
        cli1.Product__c = np1.id;
        cli1.Sort_order__c = 1;
        cli1.Quantity__c = 10;
        cli1.Y1_List_Price__c = 45000;
        cli1.Executed_Services__c = 'executed services';
        cli1.Approval_Required__c = true;
        insert cli1;
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Name__c = 'Custom Label';
        cli2.Contract__c = c.Id;
        cli2.Product__c = np2.id;
        cli2.Sort_order__c = 2;
        cli2.Y1_List_Price__c = 20000;
        cli2.Executed_Services__c = 'executed services';
        cli2.Approval_Required__c = true;
        insert cli2;
        
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Name__c = 'Air';
        cli3.Contract__c = c.Id;
        cli3.Product__c = [SELECT id FROM product2 WHERE name = 'Air' AND Product_Package__c = FALSE LIMIT 1].id;
        cli3.Sort_order__c = 3;
        cli3.Y1_List_Price__c = 20000;
        cli3.Approval_Required__c = true;
        insert cli3;
        
        //intially check the contract
        salesContractApproval sca1 = new salesContractApproval();
        sca1.check(c.id, false); 
        
        //run the check again for submission
        salesContractApproval sca2 = new salesContractApproval();
        sca2.check(c.id, true); 
        
        //debug!
        Approval__c app = (Approval__c)new dynamicQuery().query('Approval__c', null, 'Contract__c = \''+c.id+'\'');
        System.debug(app.Approval_Reasons__c);
        System.debug(app.Approvers__c);
        
        //create an approval process
        Approval.processSubmitRequest req1 = new Approval.processSubmitRequest();
        req1.setComments('Test');
        req1.setObjectId(c.Id);
        Id[] testAppList = new List<Id>();
        testAppList.add([SELECT Id FROM User WHERE LastName = 'Mosca' and isActive=TRUE LIMIT 1].Id);
        req1.setNextApproverIds(testAppList);
        Approval.ProcessResult result = Approval.process(req1);
         
         
        Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
        req.setComments('Test');
        req.setAction('Approve');
        string work = '';
        for (ProcessInstanceWorkItem workItem : [SELECT p.id FROM ProcessInstanceWorkItem p WHERE p.ProcessInstance.TargetObjectid = :c.id]) {
            work = String.ValueOf(workItem.id);            
        }
        req.setWorkItemid(work);
        Approval.Processresult result2 = Approval.process(req);

        
        //initialize the controller
        ApexPages.currentPage().getParameters().put('id', c.Id);
        ApexPages.currentPage().getParameters().put('process', '1');
        string managerId = [SELECT id FROM User WHERE isActive = true AND LastName='Morin' LIMIT 1].id;
        ApexPages.currentPage().getParameters().put('usrAppid', managerId);
        contractReviewController con = new contractReviewController();
        
         //manager
       // con.currentApp = 'Manager';
        //con.approverWrappers[0].isChecked = true;
        //con.reassignApprover();
        //con.saveLanguage();
        //con.approve();
         
        //director
        //con.currentApp = 'Director'; 
       // con.approve();

         
         
       //con.currentApp = 'Sales';
       //con.approve();
        
       //con.currentApp = 'Orders';
       //  con.approve();
         
         // con.currentApp = 'EHS';
        // con.approve();
         
        con.currentApp = 'Finance'; 
        con.approve();
         
       //  con.currentApp = 'Operations';
       //  con.approve();
         
    
     }
    
        static testmethod void test2c() {        
        // create test account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        // create test contact
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        // create test address
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        // create test contract
        Contract__c c = new Contract__c();
        c.Account__c = newAccount.Id;
        c.Contact__c = newContact.Id;
        c.Address__c = address.Id;
        c.Contract_Type__c ='Renewal';
        c.Contract_Length__c = '1 Year';
        c.Unsigned_Contract__c = true;
         
        //standard MSA
        c.Standard_MSA__c = 'No';
        c.Non_Standard_MSA_Type__c = 'Amended';
        c.MSA_Differences__c = 'Test MSA Differences';
        
        //coverage
        c.Num_of_Locations__c = 66;
        c.Coverage_Type__c = 'Location(s)';
        c.EHS_Coverage__c = 55;
        c.EHS_Coverage_Type__c = 'Employees';
         
         //Payment terms
         c.Standard_Payment_Terms__c = 'No';      
       
        //net terms
        c.Custom_Payment_Net_Terms__c = true;
        c.Payment_Net_Terms__c = 'Net 90';
        //deferred invoice and sub
        c.Deferred_Invoice__c = true;
        c.Deferred_Invoice_Date__c = date.today().addDays(120);
       
        //installments
        c.Installments__c = true;
        c.Billing_Frequency__c = 'Monthly';
        c.Installment_Notes__c = 'test notes';
         
        //modified down payment
        c.Modified_Down_Payment__c = true;
        c.Modified_Down_Payment_Percent__c = 10;
        //other payment terms
        c.Other_Special_Payment_Terms__c = true;
        c.Other_Special_Payment_Term_Notes__c = 'test notes';
        //opt out
        c.Opt_Out__c = true;
        c.Opt_Out_Notes__c = 'test notes';
        //split billing
        c.Split_Billing__c = true;
        c.Split_Billing_Number_of_Splits__c = 4;
        c.Split_Billing_Notes__c = 'test notes';
        //c.US_CS_Approval__c = true;
        //c.Webpliance_PPI_Approval__c = true;
        c.Rep_Sales_Director__c = [Select ID from User where lastName = 'Jackson' and FirstName='Eric' and isActive = TRUE Limit 1].ID;
       
         //milestones
        c.Custom_Billing_Milestones__c = true;
        c.Billing_Milestones_JSON__c ='[{"milestonesList":[{"percentage":30,"milestoneNumber":1,"details":null,"description":"Project Kickoff Complete"},{"percentage":60,"milestoneNumber":2,"details":null,"description":"Implementation Workshop Complete"},{"percentage":10,"milestoneNumber":3,"details":null,"description":"System Production Ready"}],"implementationType":"Simple","customizationType":"Standard"}]';

        insert c;  
        
        // create a test product
        Product2 np1 = new Product2();
        np1.Name = 'Webpliance';
        np1.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np1.Contract_Print_Group__c = 'MSDS Management';
        insert np1;
        
        Product2 np2 = new Product2();
        np2.Name = 'Custom Label';
        np2.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np2.Contract_Print_Group__c = 'MSDS Management';
        insert np2;
        
        // create a test line item
        Contract_Line_Item__c cli1 = new Contract_Line_Item__c();
        cli1.Name__c = 'Webpliance';
        cli1.Contract__c = c.Id;
        cli1.Product__c = np1.id;
        cli1.Sort_order__c = 1;
        cli1.Quantity__c = 10;
        cli1.Y1_List_Price__c = 45000;
        cli1.Executed_Services__c = 'executed services';
        cli1.Approval_Required__c = true;
        insert cli1;
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Name__c = 'Custom Label';
        cli2.Contract__c = c.Id;
        cli2.Product__c = np2.id;
        cli2.Sort_order__c = 2;
        cli2.Y1_List_Price__c = 20000;
        cli2.Executed_Services__c = 'executed services';
        cli2.Approval_Required__c = true;
        insert cli2;
        
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Name__c = 'Air';
        cli3.Contract__c = c.Id;
        cli3.Product__c = [SELECT id FROM product2 WHERE name = 'Air' AND Product_Package__c = FALSE LIMIT 1].id;
        cli3.Sort_order__c = 3;
        cli3.Y1_List_Price__c = 20000;
        cli3.Approval_Required__c = true;
        insert cli3;
        
        //intially check the contract
        salesContractApproval sca1 = new salesContractApproval();
        sca1.check(c.id, false); 
        
        //run the check again for submission
        salesContractApproval sca2 = new salesContractApproval();
        sca2.check(c.id, true); 
        
        //debug!
        Approval__c app = (Approval__c)new dynamicQuery().query('Approval__c', null, 'Contract__c = \''+c.id+'\'');
        System.debug(app.Approval_Reasons__c);
        System.debug(app.Approvers__c);
        
        //create an approval process
        Approval.processSubmitRequest req1 = new Approval.processSubmitRequest();
        req1.setComments('Test');
        req1.setObjectId(c.Id);
        Id[] testAppList = new List<Id>();
        testAppList.add([SELECT Id FROM User WHERE LastName = 'Mosca' and isActive=TRUE LIMIT 1].Id);
        req1.setNextApproverIds(testAppList);
        Approval.ProcessResult result = Approval.process(req1);
         
         
        Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
        req.setComments('Test');
        req.setAction('Approve');
        string work = '';
        for (ProcessInstanceWorkItem workItem : [SELECT p.id FROM ProcessInstanceWorkItem p WHERE p.ProcessInstance.TargetObjectid = :c.id]) {
            work = String.ValueOf(workItem.id);            
        }
        req.setWorkItemid(work);
        Approval.Processresult result2 = Approval.process(req);

        
        //initialize the controller
        ApexPages.currentPage().getParameters().put('id', c.Id);
        ApexPages.currentPage().getParameters().put('process', '1');
        string managerId = [SELECT id FROM User WHERE isActive = true AND LastName='Morin' LIMIT 1].id;
        ApexPages.currentPage().getParameters().put('usrAppid', managerId);
        contractReviewController con = new contractReviewController();
        
         //manager
       // con.currentApp = 'Manager';
        //con.approverWrappers[0].isChecked = true;
        //con.reassignApprover();
        //con.saveLanguage();
        //con.approve();
         
        //director
        //con.currentApp = 'Director'; 
       // con.approve();

         
         
       //con.currentApp = 'Sales';
       //con.approve();
        
       //con.currentApp = 'Orders';
       //  con.approve();
         
          //con.currentApp = 'EHS';
         //con.approve();
         
        //con.currentApp = 'Finance'; 
        //con.approve();
         
        con.currentApp = 'Operations';
        con.approve();
         
    
     }
    
    
}