@isTest(SeeAllData=true)
public with sharing class testCloseCancelledOppty {
    
    static testMethod void test1() { 
        date myDate = date.newInstance(2013, 4, 30);
        date newDate = mydate.addDays(2);
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account 666666666776';
        newAccount.NAICS_Code__c = '12345';
        insert newAccount;
        
        Renewal__c newRenew = new Renewal__c();
        newRenew.Account__c = newAccount.ID;
        newRenew.Status__c = 'Active';
        insert newRenew;
        
        Renewal__c newRenew1 = new Renewal__c();
        newRenew1.Account__c = newAccount.ID;
        newRenew1.Status__c = 'Active';
        insert newRenew1;
        
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'Test Account 666666666776';
        newOppty.AccountID = newAccount.Id;
        newOppty.Type = 'Renewal';
        newOppty.Renewal__c = newRenew.Id;
        newOppty.CloseDate = System.today();
        newOppty.StageName = 'New';
        insert newOppty;
        
        Opportunity newOppty1 = new Opportunity();
        newOppty1.Name = 'Test Account 5';
        newOppty1.AccountID = newAccount.Id;
        newOppty1.Type = 'Renewal';
        newOppty1.Renewal__c = newRenew.Id;
        newOppty1.CloseDate = newDate;
        newOppty1.StageName = 'New';
        insert newOppty1;
        
        Opportunity newOppty2 = new Opportunity();
        newOppty2.Name = 'Test Account 555';
        newOppty2.AccountID = newAccount.Id;
        newOppty2.Type = 'Renewal';
        newOppty2.Renewal__c = newRenew1.Id;
        newOppty2.CloseDate = System.today();
        newOppty2.StageName = 'New';
        insert newOppty2;
        
        
        newOppty.StageName = 'Closed Lost';
        newOppty.Closed_Reasons__c = 'Cancel';
        update newOppty; 
        
        Opportunity oppty1 = [select id, StageName, CloseDate, Closed_Reasons__c from Opportunity where Id = :newOppty1.Id]; 
        System.assertEquals('Closed Lost',oppty1.StageName);
        System.assertEquals('Cancel',oppty1.Closed_Reasons__c);  
        System.assertEquals(system.Today(),oppty1.CloseDate); 
        
        Opportunity oppty2 = [select id, StageName, CloseDate, Closed_Reasons__c from Opportunity where Id = :newOppty2.Id]; 
        System.assertEquals('New',oppty2.StageName); 
        
        
    }
}