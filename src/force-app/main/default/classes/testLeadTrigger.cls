@isTest()
private class testLeadTrigger {
    
    //insert 3 enterprise leads:
    //	one is assigned a territory
    //	one is assigned a special country
    //	one is assigned nothing
    
    @isTest(seeAllData=true)
    public static void test_enterpriseLeads() {
        
        //query owners
        User[] owners = [SELECT id, LastName FROM User WHERE isActive = true AND Department__c = 'Sales' AND Group__c = 'Mid-Market' ORDER BY LastName DESC LIMIT 5];
        
        //create territories
        Territory__c terr = new Territory__c();
        terr.Name = 'XUSA';
        terr.OwnerId = owners[0].id;
        terr.Territory_Owner__c = owners[0].id;
        terr.Online_Training_Owner__c = owners[1].id;
        terr.Authoring_Owner__c = owners[2].id;
        terr.Ergonomics_Owner__c = owners[3].id;
        terr.EHS_Owner__c = owners[4].id;
        insert terr;
        
        //create zip
        County_Zip_Code__c zip = new County_Zip_Code__c(City__c = 'Chicago', State__c = 'IL', Territory__c = terr.id, Zip_Code__c = 'USAXY', County__c = 'Cook', Country__c = 'United States');
        insert zip;
        
        //query default users
        leadCustomSettings lcd = new leadCustomSettings();
        leadCustomSettings.defaultOwners def = lcd.findDefaultOwners();
        
        //query the should-be enterprise RR owner
        Round_Robin_Assignment__c entRROwner1 = [SELECT id, UserID__c FROM Round_Robin_Assignment__c WHERE Order__c = 1 AND Type__c = 'Enterprise' LIMIT 1];
        Round_Robin_Assignment__c entRROwner2 = [SELECT id, UserID__c FROM Round_Robin_Assignment__c WHERE Order__c = 2 AND Type__c = 'Enterprise' LIMIT 1];
        Round_Robin_Assignment__c entRROwner3 = [SELECT id, UserID__c FROM Round_Robin_Assignment__c WHERE Order__c = 3 AND Type__c = 'Enterprise' LIMIT 1];
        
        //insert an enterprise lead that should grab a territory
        Lead l1 = new Lead(LastName = 'Test', PostalCode = 'USAXY', Country = 'United States', Company = 'United Artists');
        insert l1;
        
        //insert an enterprise lead with a sepcial country
        Lead l2 = new Lead(LastName = 'Test', Country = 'UAE', Company = 'United Artists');
        insert l2;
        
        //insert an enterprise lead with no territory and no special country
        Lead l3 = new Lead(LastName = 'Test', Country = 'United States', Company = 'United Artists');
        insert l3;
        
        //assert lead #1
        l1 = [SELECT id, PostalCode, Territory__r.Name, OwnerId, Online_Training_Owner__c, Authoring_Owner__c, EHS_Owner__c, Ergonomics_Owner__c, Default_Owners_Assigned__c, Enterprise_Sales_Lead__c
              FROM Lead
              WHERE id = : l1.id];
        
        //check the enterprise sales check
        System.assert(l1.Enterprise_Sales_Lead__c == true);
        
        //check the territory updater
        System.assert(l1.Territory__r.Name == 'XUSA');
        
        //check the owners
        System.assert(l1.OwnerId == entRROwner1.UserID__c);
        System.assert(l1.Online_Training_Owner__c == terr.Online_Training_Owner__c);
        System.assert(l1.Authoring_Owner__c == terr.Authoring_Owner__c);
        System.assert(l1.EHS_Owner__c == def.entEHSOwner);
        //     System.assert(l1.Ergonomics_Owner__c == terr.Ergonomics_Owner__c);
        
        //assert lead #2
        l2 = [SELECT id, PostalCode, Territory__r.Name, OwnerId, Online_Training_Owner__c, Authoring_Owner__c, EHS_Owner__c, Ergonomics_Owner__c, Default_Owners_Assigned__c, Enterprise_Sales_Lead__c
              FROM Lead
              WHERE id = : l2.id];
        
        //check the enterprise sales check
        System.assert(l2.Enterprise_Sales_Lead__c == true);
        
        //check the territory updater
        System.assert(l2.Territory__c == null);
        
        //check the owners
        //     System.assert(l2.OwnerId == entRROwner2.UserID__c);
        System.assert(l2.Online_Training_Owner__c == def.defOTOwner);
        System.assert(l2.Authoring_Owner__c == def.defAuthOwner);
        System.assert(l2.EHS_Owner__c == def.specialEHSOwner);
        //     System.assert(l2.Ergonomics_Owner__c == def.defErgoOwner);
        
        //assert lead #3
        l3 = [SELECT id, PostalCode, Territory__r.Name, OwnerId, Online_Training_Owner__c, Authoring_Owner__c, EHS_Owner__c, Ergonomics_Owner__c, Default_Owners_Assigned__c, Enterprise_Sales_Lead__c
              FROM Lead
              WHERE id = : l3.id];
        
        //check the enterprise sales check
        System.assert(l3.Enterprise_Sales_Lead__c == true);
        
        //check the territory updater
        System.assert(l3.Territory__c == null);
        
        //check the owners
        //        System.assert(l3.OwnerId == entRROwner3.UserID__c);
        System.assert(l3.Online_Training_Owner__c == def.defOTOwner);
        System.assert(l3.Authoring_Owner__c == def.defAuthOwner);
        System.assert(l3.EHS_Owner__c == def.entEHSOwner);
        //      System.assert(l3.Ergonomics_Owner__c == def.defErgoOwner);
        
        
        // *********************************** TEST FOR CHANGING A TERRITORY *********************************** //
        //query new owners
        User[] owners2 = [SELECT id, LastName FROM User WHERE isActive = true AND Department__c = 'Sales' AND Group__c = 'Mid-Market' ORDER BY LastName ASC LIMIT 5];
        
        //create territories
        Territory__c terr2 = new Territory__c();
        terr2.Name = 'ZUSA';
        terr2.OwnerId = owners2[0].id;
        terr2.Territory_Owner__c = owners2[0].id;
        terr2.Online_Training_Owner__c = owners2[1].id;
        terr2.Authoring_Owner__c = owners2[2].id;
        terr2.Ergonomics_Owner__c = owners2[3].id;
        terr2.EHS_Owner__c = owners2[4].id;
        insert terr2;
        
        //create zip
        County_Zip_Code__c zip2 = new County_Zip_Code__c(City__c = 'Philadelphia', State__c = 'PA', Territory__c = terr2.id, Zip_Code__c = 'USAAB', County__c = 'Philly', Country__c = 'United States');
        insert zip2;
        
        //lead 1 is an enterprise lead assigned to a territory. change the postalcode and asert the territory changed but the owners didnt
        l1.PostalCode = 'USAAB';
        update l1;
        
        l1 = [SELECT id, PostalCode, Territory__r.Name, OwnerId, Online_Training_Owner__c, Authoring_Owner__c, EHS_Owner__c, Ergonomics_Owner__c, Default_Owners_Assigned__c, Enterprise_Sales_Lead__c
              FROM Lead
              WHERE id = : l1.id];
        
        //check the enterprise sales check
        System.assert(l1.Enterprise_Sales_Lead__c == true);
        
        //check the territory updater
        System.assert(l1.Territory__r.Name == 'ZUSA');
        
        //check the owners
        System.assert(l1.OwnerId == entRROwner1.UserID__c);
        System.assert(l1.Online_Training_Owner__c == terr2.Online_Training_Owner__c);
        System.assert(l1.Authoring_Owner__c == terr2.Authoring_Owner__c);
        System.assert(l1.EHS_Owner__c == def.entEHSOwner);
        //    System.assert(l1.Ergonomics_Owner__c == terr2.Ergonomics_Owner__c);
        
        //lead 3 is an enterprise lead assigned to nothing, change the postalcode so it finds a territory and assert the default owners have changed
        l3.PostalCode = 'USAAB';
        update l3;
        
        l3 = [SELECT id, PostalCode, Territory__r.Name, OwnerId, Online_Training_Owner__c, Authoring_Owner__c, EHS_Owner__c, Ergonomics_Owner__c, Default_Owners_Assigned__c, Enterprise_Sales_Lead__c
              FROM Lead
              WHERE id = : l3.id];
        
        //check the enterprise sales check
        System.assert(l3.Enterprise_Sales_Lead__c == true);
        
        //check the territory updater
        System.assert(l3.Territory__r.Name == 'ZUSA');
        
        //check the owners
        //  System.assert(l3.OwnerId == entRROwner3.UserID__c);
        System.assert(l3.Online_Training_Owner__c == terr2.Online_Training_Owner__c);
        System.assert(l3.Authoring_Owner__c == terr2.Authoring_Owner__c);
        System.assert(l3.EHS_Owner__c == def.entEHSOwner);
        //       System.assert(l3.Ergonomics_Owner__c == terr2.Ergonomics_Owner__c);
        
    }
    
    //insert 3 mid-market leads
    //	one is assigned a territory
    //	one is assigned a special country
    //	one is assigned nothing
    @isTest(seeAllData=true)
    public static void test_midMarketLeads() {
        
        //query owners
        User[] owners = [SELECT id, LastName FROM User WHERE isActive = true AND Department__c = 'Sales' AND Group__c = 'Mid-Market' LIMIT 5];
        
        //create territories
        Territory__c terr = new Territory__c();
        terr.Name = 'XCAN';
        terr.OwnerId = owners[0].id;
        terr.Territory_Owner__c = owners[0].id;
        terr.Online_Training_Owner__c = owners[1].id;
        terr.Authoring_Owner__c = owners[2].id;
        terr.Ergonomics_Owner__c = owners[3].id;
        terr.EHS_Owner__c = owners[4].id;
        insert terr;
        
        //create zip
        County_Zip_Code__c zip = new County_Zip_Code__c(City__c = 'Toronto', State__c = 'ON', Territory__c = terr.id, Zip_Code__c = 'CAN XYZ', County__c = 'Test', Country__c = 'Canada');
        insert zip;
        
        //query default users
        leadCustomSettings lcd = new leadCustomSettings();
        leadCustomSettings.defaultOwners def = lcd.findDefaultOwners();
        
        //insert an enterprise lead that should grab a territory
        Lead l1 = new Lead(LastName = 'Test', PostalCode = 'canxyz', Country = 'Canada', Company = 'Test');
        insert l1;
        
        //insert an enterprise lead with a sepcial country
        Lead l2 = new Lead(LastName = 'Test', Country = 'UAE', Company = 'Test');
        insert l2;
        
        //insert an enterprise lead with no territory and no special country
        Lead l3 = new Lead(LastName = 'Test', Country = 'United States', Company = 'Test');
        insert l3;
        
        //assert lead #1
        l1 = [SELECT id, PostalCode, Territory__r.Name, OwnerId, Online_Training_Owner__c, Authoring_Owner__c, EHS_Owner__c, Ergonomics_Owner__c, Default_Owners_Assigned__c, Enterprise_Sales_Lead__c
              FROM Lead
              WHERE id = : l1.id];
        
        //check the enterprise sales check
        System.assert(l1.Enterprise_Sales_Lead__c == false);
        
        //check the territory updater
        System.assert(l1.Territory__r.Name == 'XCAN');
        System.assert(l1.PostalCode == 'CAN XYZ');
        
        //check the owners
        System.assert(l1.OwnerId == terr.Territory_Owner__c);
        System.assert(l1.Online_Training_Owner__c == terr.Online_Training_Owner__c);
        System.assert(l1.Authoring_Owner__c == terr.Authoring_Owner__c);
        System.assert(l1.EHS_Owner__c == terr.EHS_Owner__c);
        //    System.assert(l1.Ergonomics_Owner__c == terr.Ergonomics_Owner__c);
        
        //assert lead #2
        l2 = [SELECT id, PostalCode, Territory__r.Name, OwnerId, Online_Training_Owner__c, Authoring_Owner__c, EHS_Owner__c, Ergonomics_Owner__c, Default_Owners_Assigned__c, Enterprise_Sales_Lead__c
              FROM Lead
              WHERE id = : l2.id];
        
        //check the enterprise sales check
        System.assert(l2.Enterprise_Sales_Lead__c == false);
        
        //check the territory updater
        System.assert(l2.Territory__c == null);
        
        //check the owners
        System.assert(l2.OwnerId == def.defInternationalOwner);
        System.assert(l2.Online_Training_Owner__c == def.defOTOwner);
        System.assert(l2.Authoring_Owner__c == def.defAuthOwner);
        System.assert(l2.EHS_Owner__c == def.specialEHSOwner);
        //      System.assert(l2.Ergonomics_Owner__c == def.defErgoOwner);
        
        //assert lead #3
        l3 = [SELECT id, PostalCode, Territory__r.Name, OwnerId, Online_Training_Owner__c, Authoring_Owner__c, EHS_Owner__c, Ergonomics_Owner__c, Default_Owners_Assigned__c, Enterprise_Sales_Lead__c
              FROM Lead
              WHERE id = : l3.id];
        
        //check the enterprise sales check
        System.assert(l3.Enterprise_Sales_Lead__c == false);
        
        //check the territory updater
        System.assert(l3.Territory__c == null);
        
        //check the owners
        System.assert(l3.OwnerId == def.defChemMgtOwner);
        System.assert(l3.Online_Training_Owner__c == def.defOTOwner);
        System.assert(l3.Authoring_Owner__c == def.defAuthOwner);
        System.assert(l3.EHS_Owner__c == def.defEHSOwner);
        //      System.assert(l3.Ergonomics_Owner__c == def.defErgoOwner);
        
        l1.OwnerId = def.defChemMgtOwner;
        update l1;
        l1 = [SELECT id, OwnerId, Chemical_Management_Owner__c
              FROM Lead
              WHERE id = : l1.id LIMIT 1];
        System.assert(l1.OwnerId == l1.Chemical_Management_Owner__c);
        
    }
    
    //insert 200 enterprise leads to bulk test RR assignments
    @isTest(seeAllData=true)
    public static void test_roundRobin() {
        Round_Robin_Assignment__c[] owners = [SELECT id, Name, UserID__c, Used_Last__c, Order__c
                                              FROM Round_Robin_Assignment__c
                                              ORDER BY Order__c];
        
        Lead[] leads = new list<Lead>();
        for (integer i=0; i<200; i++) {
            leads.add(new Lead(LastName = 'Test', Company = 'United Artists', Country = 'United States', Num_of_Employees__c = (i+1)));
        }
        insert leads;
        
        set<id> leadIds = new set<id>();
        for (Lead l : leads) {
            leadIds.add(l.id);
        }
        
        leads = [SELECT id, Num_of_Employees__c, OwnerId, Owner.LastName FROM Lead WHERE id IN : leadIds ORDER BY Num_of_Employees__c ASC];
        
        //test the first loop
        for (integer i=0; i<owners.size(); i++) {
            //         System.assert(leads[i].OwnerId == owners[i].UserID__c);
        }
        
        //test the second loop
        for (integer i=(owners.size()); i<(owners.size()*2); i++) {
            //           System.assert(leads[i].OwnerId == owners[i-owners.size()].UserID__c);
        }
        
    }
    
    //test converting a lead to a contact
    @isTest(seeAllData=true)
    public static void test_leadCreatedDate() {
        
        //insert lead
        Lead l = new Lead(LastName = 'Test', Company = 'Test', Status = 'Qualified');
        insert l;
        
        //convert a lead
        database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(l.id);
        lc.setConvertedStatus('Qualified');
        lc.setDoNotCreateOpportunity(true);        
        database.LeadConvertResult lcr = database.convertLead(lc);
        System.assert(lcr.isSuccess());
        
        l = [SELECT id, isConverted, Status, Lead_Created_Date__c, ConvertedContactId
             FROM Lead
             WHERE id = : l.id 
             LIMIT 1];
        
        System.assert(l.Lead_Created_Date__c != null);
        //double-check the lead is converted
        System.assert(l.isConverted);
        //check for a contact id
        System.assert(l.ConvertedContactId != null);
        
        Contact c = [SELECT id, Lead_Created_Date__c
                     FROM Contact
                     WHERE id = : l.ConvertedContactId
                     LIMIT 1];
        
        //assert the contact has the correct lead created date
        System.assert(c.Lead_Created_Date__c == l.Lead_Created_Date__c);
    }
    
    //test the lead source utility
    @isTest(seeAllData=true)
    public static void test_leadSourceUtility() {
        //query our settings
        Lead_SourceCategoryType_Table__c lst = [SELECT User_Selected_Lead_Source__c, Lead_Source__c, Lead_Source_Category__c, Lead_Source_Type__c 
                                                FROM Lead_SourceCategoryType_Table__c
                                                WHERE User_Selected_Lead_Source__c = 'Referral' LIMIT 1];
        
        //insert a lead
        Lead l = new Lead(LastName = 'Test', Company = 'Test', Communication_Channel__c = 'Referral');
        insert l;
        
        //query and assert
        l = [SELECT id, Communication_Channel__c, Lead_Source_Category__c, Lead_Source_Type__c, LeadSource
             FROM Lead
             WHERE id = : l.id LIMIT 1];
        
        System.assert(l.LeadSource == lst.Lead_Source__c);
        System.assert(l.Lead_Source_Category__c == lst.Lead_Source_Category__c);
        System.assert(l.Lead_Source_Type__c == lst.Lead_Source_Type__c);
        
        //query new settings
        lst = [SELECT User_Selected_Lead_Source__c, Lead_Source__c, Lead_Source_Category__c, Lead_Source_Type__c 
               FROM Lead_SourceCategoryType_Table__c
               WHERE User_Selected_Lead_Source__c = 'CC - Support' LIMIT 1];
        //update the lead source
        l.LeadSource = 'CC - Support';
        update l;
        
        //re-query and assert
        l = [SELECT id, Communication_Channel__c, Lead_Source_Category__c, Lead_Source_Type__c, LeadSource
             FROM Lead
             WHERE id = : l.id LIMIT 1];
        
        System.assert(l.LeadSource == lst.Lead_Source__c);
        System.assert(l.Lead_Source_Category__c == lst.Lead_Source_Category__c);
        System.assert(l.Lead_Source_Type__c == lst.Lead_Source_Type__c);
        
        //update the lead source
        l.LeadSource = null;
        update l;
        
        //re-query and assert
        l = [SELECT id, Communication_Channel__c, Lead_Source_Category__c, Lead_Source_Type__c, LeadSource
             FROM Lead
             WHERE id = : l.id LIMIT 1];
        
        System.assert(l.LeadSource == null);
        System.assert(l.Lead_Source_Category__c == null);
        System.assert(l.Lead_Source_Type__c == null);
    }
    
    //test the email preferences utility
    @isTest(seeAllData=true)
    public static void test_emailPreferencesUtility() {
        //test a marketo created lead
        Lead mktoLead = new Lead(LastName = 'Test', Company = 'Test', LeadSource = 'Cold Call - Other', Country = 'United States');
        insert mktoLead;
        
        //re-query and assert
        mktoLead = [SELECT id, Opt_In_Sales_Consulting__c FROM Lead WHERE id = : mktoLead.id LIMIT 1];
        System.assert(mktoLead.Opt_In_Sales_Consulting__c == true);
        
        //opt out the lead
        mktoLead.Unsubscribe_All__c = true;
        update mktoLead;
        
        //re-query and assert
        mktoLead = [SELECT id, Opt_In_Blog_Subscription__c, Opt_In_Newsletter__c FROM Lead WHERE id = : mktoLead.id LIMIT 1];
        System.assert(mktoLead.Opt_In_Blog_Subscription__c == false);
        System.assert(mktoLead.Opt_In_Newsletter__c == false);
        
    }
    @isTest(seeAllData=true)
    public static void test_campaignMemberUtility() {
        Lead cmLead = new Lead(LastName = 'Test', Company = 'Test', LeadSource = 'Cold Call - Other', Country = 'United States', Primary_Solution_Product_of_Interest__c = 'MSDS Management');
        insert cmLead;
        
        Campaign c = new Campaign(Name='Test Campaign');
        insert c;
        
        CampaignMember cm = new CampaignMember(LeadId = cmLead.id, Status = 'Sent', CampaignId = c.id);
        insert cm;
        
        cmLead.Primary_Solution_Product_of_Interest__c = 'MSDS Management;MSDS Authoring;Workplace Training';
        update cmLead;
        
        cm = [select id,Primary_solution_Product_of_interest__c from CampaignMember where id =: cm.id];
        system.assert(cm.Primary_Solution_Product_of_Interest__c == cmLead.Primary_Solution_Product_of_Interest__c);
    }
    
    static testmethod void ergonomicsOwnershipAssignment() {
        //Scenario 1 - See if the lead company name matches an account name if it does set the ergo owner from that account to the lead 
        //Scenario 2 - If account name dosent match take the lead email domain and see if there is an contact with that same domian if so look up to the account of the contact and set the ergo owner of account to lead 
        //Scenario 3 - If account name and email domain dont match send the lead to the roundRobinOwnershipAssignment method to set lead owner 
        
        //Test class method to cover Ergonomics Owner Assignment in ownershipUtility   
        //Query for data needed in test class
        user EJ = [Select id, name from user where name = 'Eric Jackson']; 
        User ErgoSales = [Select id, name from user where role__c = 'Ergonomics Sales Executive' and isactive = true limit 1];  
        User ES = [Select id, name from user where userrole.name like 'Enterprise Sales Representative%' and isactive = true limit 1];  
        User ErgoOwner1 = [Select id, name from user where name = 'Nick Lipnisky' and isactive = true limit 1];  
        User ErgoOwner2 = [Select id, name from user where name = 'Daniel Gruszka' and isactive = true limit 1];  
        User ErgoOwner3 = [Select id, name from user where name = 'Tara Wills' and isactive = true limit 1];  
        
        //Create and set lead custom setting for Eric Jackson and make him lead owner of all leads 
        Lead_Custom_Settings__c LC = Lead_Custom_Settings__c.getOrgDefaults();
        lc.SetupOwnerId = EJ.id;
        lc.Can_Change_Authoring_Owner__c = true;
        lc.Can_Change_Chemical_Mgt_Owner__c = true;
        lc.Can_Change_EHS_Owner__c = true;
        lc.Can_Change_Ergonomics_Owner__c = true;
        lc.Can_Change_Online_Training_Owner__c = true;
        lc.Can_Change_Secondary_Lead_Owner__c = true;
        lc.Can_Edit_Ergonomics_Secondary_Lead_Owner__c = true;
        lc.Default_Authoring_Owner__c = true;
        lc.Default_Chemical_Management_Owner__c = true;
        lc.Default_EHS_Owner__c = true;
        lc.Default_Ergo_Owner__c = true;
        lc.Default_International_Lead_Owner__c = true;
        lc.Default_Online_Training_Owner__c = true;
        lc.Enterprise_EHS_Owner__c = true;
        lc.Special_Country_EHS_Owner__c = true;
        
        insert lc;
        
        //Create Round Robin Assigment custom settings for ergonomics 
        Round_Robin_Assignment__c RR1 = new Round_Robin_Assignment__c();  
        RR1.Name = ErgoSales.name;
        RR1.Order__c = 1;
        RR1.UserID__c = ErgoSales.id;
        RR1.Used_Last__c = false;
        RR1.Type__c = 'Ergonomics';
        RR1.CreatedDate = date.today();
        
        insert RR1;
        
        //Create custom setting - Round Robin assigment for enterprise 
        Round_Robin_Assignment__c RR2 = new Round_Robin_Assignment__c();
        RR2.name = ES.Name;
        RR2.Order__c = 1;
        RR2.UserID__c = ES.id;
        RR2.Used_Last__c = false;
        RR2.Type__c = 'Enterprise';
        RR2.CreatedDate = date.today();
        
        Insert RR2;
        
        //Create 3 accounts and set ergonomics account owner on them from the queries abobe
        //Create 1 contact
        //Create 2 leads 
        //
        account a1 = new account();
        a1.Name = 'NLU';
        a1.Ergonomics_Account_Owner__c = ErgoOwner1.id; 
        
        insert a1;
        
        account a2 = new account();
        a2.Name = 'Test Account';
        a2.Ergonomics_Account_Owner__c = ErgoOwner2.id; 
        
        insert a2;
        
        account a3 = new account();
        a3.Name = 'Maciejs Account';
        a3.Ergonomics_Account_Owner__c = ErgoOwner3.id; 
        
        insert a3;
        
        contact c = new contact();
        c.AccountId = a2.id;
        c.FirstName = 'Big';
        c.LastName = 'Randy';
        c.Email = 'test@macccc.com';
        
        insert c;
        
        lead l1 = new lead();
        l1.Company = a1.Name;
        l1.LastName = 'Leader';
        l1.Communication_Channel__c = 'Cold Call';
        l1.OwnerId = ej.id;
        
        insert l1;
        
        lead l2 = new lead();
        l2.Company = 'No Company Name Available';
        l2.LastName = 'Nosek';
        l2.Communication_Channel__c = 'Cold Call';
        l2.Email = 'MacsEmail@macccc.com';
        l2.OwnerId = ej.id;
        
        insert l2;
        
        lead l3 = new lead();
        l3.Company = 'No Company Name Available';
        l3.LastName = 'Thomas';
        l3.Communication_Channel__c = 'Cold Call';
        l3.OwnerId = ej.id;
        
        insert l3;
        
        //requery leads to see what owners where set 
        lead lead1 = [select id, Ergonomics_Owner__c from lead where id =: l1.id];
        lead lead2 = [select id, Ergonomics_Owner__c from lead where id =: l2.id];
        lead lead3 = [select id, Ergonomics_Owner__c from lead where id =: l3.id];

        // system asserts check to see if the code ran and set proper account owners 
        system.assertEquals(a1.Ergonomics_Account_Owner__c, lead1.Ergonomics_Owner__c);
        system.assertEquals(a2.Ergonomics_Account_Owner__c, lead2.Ergonomics_Owner__c);
        system.assertEquals(RR1.UserID__c, lead3.Ergonomics_Owner__c);
       
    }
}