@isTest()
public class testAmeToDoTabsController {
    static testMethod void test1(){
        //Create data for test class        
        Account testAccount = new account();
        testAccount.name = 'Maciejs Test';
        testaccount.Customer_Status__c = 'Active';
        
        Insert testAccount;
        
        Contact TC = new contact();
        TC.FirstName = 'Mountain';
        TC.LastName = 'Annie';
        TC.AccountId = testAccount.id;
        
        Insert TC;
        
        Account_Management_Event__c ame = new Account_Management_Event__c();
        ame.Account__c = testAccount.id;
        ame.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        ame.Contact__c = TC.id;
        ame.OwnerId = userinfo.getUserId();
        ame.Status__c = 'Active';
        ame.CreatedById = userinfo.getUserId();
        
        Insert ame;
        
        Task NT = new task();
        
        NT.OwnerId = ame.OwnerId;
        NT.WhatId = ame.id;
        NT.WhoId = ame.Contact__c;
        NT.Status = 'Not Started';
        NT.Subject = 'This is a new task';
        NT.ActivityDate = date.today();
        NT.Type__c = 'Call';
        
        Insert NT;
        
        Task CT = new task();
        
        CT.OwnerId = ame.OwnerId;
        CT.WhatId = ame.id;
        CT.WhoId = ame.Contact__c;
        CT.Status = 'Completed';
        CT.Subject = 'Tester';
        CT.ActivityDate = date.today().addDays(-5);
        CT.Type__c = 'Send Email';
        
        
        Insert CT;
        
        Attachment A = new Attachment();
        
        A.Name = 'Test Document';
        A.ParentId = ame.Id;
        
        A.Body = blob.valueOf('a');
        
        Insert A;
        // End of data creation         
        
        
        //Pass in controller used in the ameToDoTabsController 
        ameToDoTabsController myC = new ameToDoTabsController(); 
        
        //Give the attributes used in page a value to pass test class coverage 
        myC.selectedAMEId = ame.id;
        myC.currentView = '';
        myC.sortDirection = '';
        myC.sortField = '';
        myC.ViewAsUser = '';
        myC.TaskId = NT.id;
        
        //Check to see if the selected ame id is equivalent
        system.assertEquals(myc.selectedAMEId, ame.id);
        
        //Call intilize method which will call all querys on page and populate the data
        myC.getInitialize();
        
        //Call retention reps methods ro populate the Assigned To picklist 
        myC.getReps();
        
        //Call the new task method which will run when user presses the new button on task page 
        myC.NewTask();
        
        //Check to see if the new task method set appropriate fields 
        system.assertEquals(NT.OwnerId, ame.OwnerId);
        System.assertEquals(NT.WhatId, ame.Id);
        System.assertEquals(NT.WhoId, ame.Contact__c);
        System.assertEquals(NT.Status, 'Not Started');
        system.assertEquals(myc.TaskUpserted, null);
        
        
        //Set new task fields to null/epmty string to fire the valadation method 
        Myc.ViewTask.OwnerId = null;
        Myc.ViewTask.Subject = '';
        myC.ViewTask.Status = null;
        
        // check to see if the the vaibeles are set before method runs
        system.assertEquals(myc.TaskUpserted, null);
        system.assertEquals(myc.errorMsg, null);
        
        //call save method
        myC.SaveTask();
        
        
        //Check varaibles again after method runs to see if they changed to the correct value 
        system.assertEquals(myc.TaskUpserted, false);
        system.assertEquals(myc.errorMsg, 'Please fill in all required fields!');
        
        
        //Call new task method 
        myC.NewTask();
        
        //Check to see task upserted varible is set to correct value on load
        system.assertEquals(myc.TaskUpserted, null);
        
        //set fields to pass save valadation 
        Myc.ViewTask.OwnerId = [select id, name from user where FirstName = 'Sarah' and LastName = 'Goldstein' limit 1 ].id;
        Myc.ViewTask.Subject = 'test';
        myC.ViewTask.Status = 'test';
        
        //Check to see if valadation fired 
        system.assertEquals(myc.TaskUpserted, null);
        system.assertEquals(myc.errorMsg, 'Please fill in all required fields!');
        
        //Call save method 
        myC.SaveTask();
        
        //Check to see if the parameter is holding the correct ID for open task 
        system.currentpagereference().getparameters().put('currentTask', NT.Id);
        
        //Call Edit task method 
        myC.EditTask();
        
        //Check to see if view task holding correct value and variable is set correctly 
        system.assertEquals(myc.ViewTask.id, NT.id);
        system.assertEquals(myc.TaskUpserted, false);
        
        //Call save method again 
        myC.SaveTask();
        
        //Check to see if the parameter is holding the correct ID for completed task 
        system.currentpagereference().getparameters().put('CompTask', CT.Id);
        
        //Call edit completed task method 
        myC.EditCompTask();
        
        //Check to see if view task holding correct value and variable is set correctly 
        system.assertEquals(myc.ViewTask.id, CT.id);
        system.assertEquals(myc.TaskUpserted, false);
        
        //set feilds and save to cover the insert part of our save method 
        Myc.ViewTask.OwnerId = [select id, name from user where FirstName = 'Sarah' and LastName = 'Goldstein' limit 1 ].id;
        Myc.ViewTask.Subject = 'test';
        myc.ViewTask.activitydate = date.today() +15;
        myc.ViewTask.type__c = 'Initial';
        myC.ViewTask.Status = 'test';
        myc.ViewTask.Description = 'Comments';
        
        //Call save task method 
        myC.SaveTask();
        //Check to see if the variables are holding correct value after method runs 
        system.assertEquals(myc.TaskUpserted, true);
        system.assertEquals(myc.errorMsg, null);   
        
        myC.returnToAmeConsole();     
        
    }
    
    static testMethod void TaskTable(){
        //Create data for test class        
        Account testAccount = new account();
        testAccount.name = 'Task Table';
        testaccount.Customer_Status__c = 'Active';
        
        Insert testAccount;
        
        Contact TC = new contact();
        TC.FirstName = 'Billy';
        TC.LastName = 'Goat';
        TC.AccountId = testAccount.id;
        
        Insert TC;
        
        Account_Management_Event__c ame = new Account_Management_Event__c();
        ame.Account__c = testAccount.id;
        ame.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        ame.Contact__c = TC.id;
        ame.OwnerId = userinfo.getUserId();
        ame.Status__c = 'Active';
        ame.CreatedById = userinfo.getUserId();
        
        Insert ame;
        
        Task T1 = new task();
        
        T1.OwnerId = ame.OwnerId;
        T1.WhatId = ame.id;
        T1.WhoId = ame.Contact__c;
        T1.Status = 'Not Started';
        T1.Subject = 'This is a new task';
        T1.ActivityDate = date.today();
        T1.Description = 'Need to Call the client';
        T1.type__c = 'Initial';
        
        Insert T1;
        
        Task T2 = new task();
        
        T2.OwnerId = ame.OwnerId;
        T2.WhatId = ame.id;
        T2.WhoId = ame.Contact__c;
        T2.Status = 'In Progress';
        T2.Subject = 'Update';
        T2.ActivityDate = date.today();
        T2.Description = 'The sunset filled the entire sky with the deep color of rubies, setting the clouds ablaze';
        T2.type__c = 'Initial';
        
        Insert T2;
        
        Task T3 = new task();
        
        T3.OwnerId = ame.OwnerId;
        T3.WhatId = ame.id;
        T3.WhoId = ame.Contact__c;
        T3.Status = 'In Progress';
        T3.Subject = 'Update';
        T3.Description = 'The sunset filled the entire sky with the deep color of rubies, setting the clouds ablaze';
        T3.type__c = 'Initial';
        
        Insert T3;
        
        
        Attachment A = new Attachment();
        
        A.Name = 'Test Document';
        A.ParentId = ame.Id;
        
        A.Body = blob.valueOf('a');
        
        Insert A;
        // End of data creation         
        
        
        //Pass in controller used in the ameToDoTabsController 
        ameToDoTabsController myC = new ameToDoTabsController(); 
        
        //Give the attributes used in page a value to pass test class coverage 
        myC.selectedAMEId = ame.id;
        myC.currentView = '';
        myC.sortDirection = '';
        myC.sortField = '';
        myC.ViewAsUser = '';
        myC.TaskId = T1.id;
        
        //Check to see if the selected ame id is equivalent
        system.assertEquals(myc.selectedAMEId, ame.id);
        
        //Call intilize method which will call all querys on page and populate the data
        myC.getInitialize();
        
        //Call retention reps methods ro populate the Assigned To picklist 
        myC.getReps();
        
        //Pass in task ID to be able to run the delete method 
        system.currentpagereference().getparameters().put('alltask', T1.Id);
        
        myc.DeleteTask();
        
        //Pass in other tasks to beused in edit and save task table method below 
        system.currentpagereference().getparameters().put('alltask', T2.Id);
        system.currentpagereference().getparameters().put('alltask', T3.Id);
        
        // Pass in save table param to be able to save task and cover valadation 
        system.currentpagereference().getparameters().put('SaveTaskTable', T2.Id);
        
        myc.EditAllTasks();
        myc.SaveTaskTable();
        
        // Cover error scenario because task 3 is mssing the activity date 
        system.currentpagereference().getparameters().put('SaveTaskTable', T3.Id);
        
        myc.EditAllTasks();
        myc.SaveTaskTable();
        
        // pass in list index to remove specfied index once task is insterted 
        system.currentpagereference().getparameters().put('index', '0');
        
        //Call inline task method to generate task
        myc.InlineTask();
        
        //save inline task without settinf fields to fire error 
        myc.SaveInlineTask();
        
        //set fields and call save again to cover the insert part of the save method 
        myc.AddTasks[0].ownerid = ame.ownerid;
        myc.AddTasks[0].status = 'In Progress';
        myc.AddTasks[0].subject = 'Call';
        myc.AddTasks[0].ActivityDate = date.today();
        myc.AddTasks[0].Description = 'Comments.....';
        myc.AddTasks[0].type__c = 'test';
        
        
        myc.SaveInlineTask();
        
        
    }
    
}