/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest(SeeAllData=true)
private class testLeadController {
    
    static testmethod void canCreateController() {
        ApexPages.StandardController testController = new ApexPages.StandardController(new Lead());
        leadController myController = new leadController(testController);
        mycontroller.closePopup();
        mycontroller.showPopup();
        mycontroller.getCount();
    } 
    
    
    static testmethod void test1() {
        
        Lead newLead = new Lead();
        newLead.Company = 'Test';
        newLead.LastName = 'McCauley';
        newLead.Lead_Type__c ='Cold Call';
        newLead.State = 'IL';
        newLead.OwnerId = '00580000003Ulfl';
        newLead.Marketo_ID__c ='zz123zz';
        insert newLead;	
        
        ApexPages.currentPage().getParameters().put('id', newLead.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Lead());
        leadController myController = new leadController(testController);
        
        myController.getStatus();
        myController.sendToES();
        myController.ownershipChangeWithNotification();
    }
    
    
    static testmethod void test2() {
        
        Lead newLead = new Lead();
        newLead.Company = 'Test';
        newLead.LastName = 'McCauley';
        newLead.Lead_Type__c ='Cold Call';
        newLead.State = 'OR';
        newLead.OwnerId = '00580000003Ulfl';
        insert newLead;	
        
        ApexPages.currentPage().getParameters().put('id', newLead.Id);
        ApexPages.currentPage().getParameters().put('ownerToChange', 'ehs');
        ApexPages.StandardController testController = new ApexPages.StandardController(new Lead());
        leadController myController = new leadController(testController);
        myController.showOwnerToChange();
    }
    
    static testmethod void test3() {
        
        Lead newLead = new Lead();
        newLead.Company = 'Test';
        newLead.LastName = 'McCauley';
        newLead.Lead_Type__c ='Cold Call';
        newLead.State = 'OR';
        newLead.OwnerId = '00580000003Ulfl';
        newLead.Recycle_Count__c = 0;
        insert newLead;	
        
        ApexPages.currentPage().getParameters().put('id', newLead.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Lead());
        leadController myController = new leadController(testController);
        
        myController.fullDetails();
        myController.incrementCounter();
    }
    
    static testmethod void test4() {
        
        Lead newLead = new Lead();
        newLead.Company = 'Test';
        newLead.LastName = 'McCauley';
        newLead.Lead_Type__c ='Cold Call';
        newLead.State = 'IL';
        newLead.OwnerId = '00580000003Ulfl';
        insert newLead;	
        
        Lead l = [Select Marketo_ID__c from Lead where ID=: newLead.ID];
        l.Marketo_ID__c ='zz123zz';
        update l;
    }
    
        static testmethod void test5() {
        
        Lead newLead = new Lead();
        newLead.Company = 'Test';
        newLead.LastName = 'McCauley';
        newLead.Lead_Type__c ='Cold Call';
        newLead.State = 'IL';
        newLead.OwnerId = '00580000003Ulfl';
        newLead.Marketo_ID__c ='zz123zz';
        insert newLead;	
        
        ApexPages.currentPage().getParameters().put('id', newLead.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Lead());
        leadController myController = new leadController(testController);
        
        myController.sendToES();
        Lead checkESValues = [Select 	Enterprise_Sales__c from Lead where id = :newLead.Id];
        System.assert(checkESValues.Enterprise_Sales__c == true);
        
        myController.sendToMM();
        Lead checkMMValues = [Select 	Enterprise_Sales__c from Lead where id = :newLead.Id];
        System.assert(checkMMValues.Enterprise_Sales__c == false);
        
        myController.redirectToNewPage();
        
    }
    
}