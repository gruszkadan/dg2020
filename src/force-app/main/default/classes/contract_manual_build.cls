public class contract_manual_build {
    private ApexPages.StandardController controller;
    public Contract__c theContract {get;set;}
    public Contact theContact {get;set;}
    public User theUser {get;set;}
    public String sId {get;set;}
    public Integer cl {get;set;}
    public Description_Dictionary__c masterDictionary {get;set;}
    //index bundling vars
    public decimal sIdIndex {get;set;}
    public lineItem[] mWrappers {get;set;}
    public integer numManagementItems {get;set;}
    boolean clearAll = true;
    public String contractId {get;set;}
    public string iso {get;set;}
    public List<Contract_Line_Item__c> contractItems {get;set;}
    public contractUtility util {get;set;}
    public List<Id> product2Ids {get;set;}
    public string productPlatforms {get;set;}
    public string velocityEHSAddress {get;set;}
    public string taxID {get;set;}
    public boolean coverageMatch {get;set;}
    public string contractFees {get;set;}
    
    public contract_manual_build(ApexPages.StandardController stdController) {
        util = new contractUtility();
        contractId = ApexPages.currentPage().getParameters().get('id');
        this.controller = stdController;
        masterDictionary = [ SELECT Id, Description__c FROM Description_Dictionary__c WHERE Flow_Referance_Name__c = 'masterDictionary' LIMIT 1 ];
        if (contractId != null ) {
            queryContract();
            if (theContract.Billing_Currency__c != null) {
                iso = theContract.Billing_Currency__c;
            } else {
                iso = 'USD';
            }
        } else {
            theContract = (Contract__c)stdController.getRecord();
        }
        theContact = [Select ID, Name, AccountId from Contact where ID =:theContract.Contact__c];
        theUser = [Select id, Name, Department__c from User where id =: UserInfo.getUserId() LIMIT 1];        
        String contractlength = theContract.Contract_Length__c;
        cl = Integer.Valueof(contractlength.LEFT(1));
        mWrappers = new List<lineItem>();
        queryContractItems();
        updateSort();
        //Determine the Product Platforms on the Contract
        productPlatforms = productUtility.findProductSuites(product2Ids);
        
        //Get the VelocityEHS Address that Needs to be used on the COF
        velocityEHSAddress = util.companyAddress(productPlatforms);
        
        //Get the Tax ID
        taxID = util.taxID(productPlatforms, theContract.Billing_Currency__c);
        
        //Check to see if the Coverage Matches across all Platforms
        coverageMatch = util.platformsMatch(productPlatforms, theContract);
    }
    
    // query the contract
    public void queryContract() {
        String SobjectApiName = 'Contract__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String additionalFields = ', Sales_Rep__r.Name, Billing_Contact__r.Name, Shipping_Contact__r.Name';
        String query = 'select ' + commaSepratedFields + additionalFields +' from ' + SobjectApiName + ' where ID=\''+contractID+'\' ';
        // return the contract items
        theContract = Database.query(query);
    }
    
    // query the contract items
    public void queryContractItems() {
        String SobjectApiName = 'Contract_Line_Item__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String additionalFields = ', Product__r.ID, Product__r.Name, Product__r.Contract_Print_Group__c, Product__r.ProductCode, Product__r.Contract_Product_Name__c, Product__r.Product_Print_Grouping__c, Product__r.Product_Platform__c, Product__r.Contract_Product_Type__c';
        String OrderBy = ' Order By Contract_Sort_Order__c ASC';
        String query = 'select ' + commaSepratedFields + additionalFields +' from ' + SobjectApiName + ' where Contract__c=\''+theContract.Id+'\' '+ OrderBy;
        // return the contract items
        contractItems = Database.query(query);
        product2Ids = new List<Id>();
        
        for(Contract_Line_Item__c q: contractItems) {
            product2Ids.add(q.Product__r.ID);
        }
    }
    
    public void checkManualEdits() {
        Contract_Line_Item__c[] updList = new List<Contract_Line_Item__c>();
        if (mWrappers.size()>0) {
            for (lineItem li : mWrappers) {
                clearManualFields(li.mainItem, li.isBundleParent);
                updList.add(li.mainItem);
                if (li.subItems.size()>0) {
                    for (Contract_Line_Item__c cli : li.subItems) {
                        clearManualFields(cli, false);
                        updList.add(cli);
                    }
                }
            }
        }
        if (clearAll == true) {
            if (updList.size()>0) {
                for (Contract_Line_Item__c cli : updList) {
                    cli.Year_1_Manual__c = null;
                    cli.Year_2_Manual__c = null;
                    cli.Year_3_Manual__c = null;
                    cli.Year_4_Manual__c = null;
                    cli.Year_5_Manual__c = null;
                    cli.Quantity_Manual__c = null;
                }
            }
        }
        if(updList.size() > 0){
            update updList;
        }
    }
    
    public void clearManualFields(Contract_Line_Item__c cli, boolean x_isBundleParent) {
        if (x_isBundleParent == true) {
            if (cli.Year_1_Manual__c != cli.Y1_Total_with_Bundle__c ||
                cli.Year_2_Manual__c != cli.Y2_Total_with_Bundle__c ||
                cli.Year_3_Manual__c != cli.Y3_Total_with_Bundle__c ||
                cli.Year_4_Manual__c != cli.Y4_Total_with_Bundle__c ||
                cli.Year_5_Manual__c != cli.Y5_Total_with_Bundle__c ||
                cli.Quantity_Manual__c != cli.Quantity__c) {
                    clearAll = false;
                }
        } else {
            if (cli.Bundle__c == false) {
                if (cli.Year_1_Manual__c != cli.Y1_Total_Price__c ||
                    cli.Year_2_Manual__c != cli.Y2_Total_Price__c ||
                    cli.Year_3_Manual__c != cli.Y3_Total_Price__c ||
                    cli.Year_4_Manual__c != cli.Y4_Total_Price__c ||
                    cli.Year_5_Manual__c != cli.Y5_Total_Price__c ||
                    cli.Quantity_Manual__c != cli.Quantity__c) {
                        clearAll = false;
                    }
            }
        }
        
        
    }
    
    public PageReference saveAll() {
        PageReference pr = Page.contract_detail;
        pr.getParameters().put('id',theContract.id);
        update theContract;
        checkManualEdits();
        return pr.setRedirect(true);
    } 
    
    public class lineItem {
        public Contract_Line_Item__c mainItem {get;set;}
        public Contract_Line_Item__c[] subItems {get;set;}
        public integer contractSortOrder {get;set;}
        public boolean isBundleParent {get;set;}
        
        public lineItem(Contract_Line_Item__c x_mainItem, Contract_Line_Item__c[] x_subItems, integer x_sortOrder) {
            mainItem = x_mainItem;
            if (mainItem.Contract_Sort_Order__c == null) {
                mainItem.Sort_Order__c = x_sortOrder;
                if (mainItem.Contract_Sort_Order__c != null) {
                    contractSortOrder = integer.valueOf(mainItem.Contract_Sort_Order__c); 
                }
            }
            subItems = new List<Contract_Line_Item__c>();
            if (x_subItems != null) {
                for (Contract_Line_Item__c cli : x_subItems) {
                    if (cli.Group_ID__c != null && cli.Group_Parent_ID__c == mainItem.Group_Parent_ID__c) {
                        subItems.add(cli);
                    }
                }
            }
            isBundleParent = false;
            if (mainItem.Y1_Bundled_Price__c > 0) {
                isBundleParent = true;
            } 
            if (mainItem.Year_1_Manual__c == null) {
                if (mainItem.Bundle__c == true) {
                    mainItem.Quantity_Manual__c = mainItem.Quantity__c;
                    mainItem.Year_1_Manual__c = 0;
                    mainItem.Year_2_Manual__c = 0;
                    mainItem.Year_3_Manual__c = 0;
                    mainItem.Year_4_Manual__c = 0;
                    mainItem.Year_5_Manual__c = 0;
                } else {
                    mainItem.Quantity_Manual__c = mainItem.Quantity__c;
                    mainItem.Year_1_Manual__c = mainItem.Y1_Total_with_Bundle__c;
                    mainItem.Year_2_Manual__c = mainItem.Y2_Total_with_Bundle__c;
                    mainItem.Year_3_Manual__c = mainItem.Y3_Total_with_Bundle__c;
                    mainItem.Year_4_Manual__c = mainItem.Y4_Total_with_Bundle__c;
                    mainItem.Year_5_Manual__c = mainItem.Y5_Total_with_Bundle__c;
                }
                if (subItems.size()>0) {
                    for (Contract_Line_Item__c cli : subItems) {
                        if (cli.Bundle__c == true) {
                            cli.Quantity_Manual__c = cli.Quantity__c;
                            cli.Year_1_Manual__c = 0;
                            cli.Year_2_Manual__c = 0;
                            cli.Year_3_Manual__c = 0;
                            cli.Year_4_Manual__c = 0;
                            cli.Year_5_Manual__c = 0;
                        } else {
                            cli.Quantity_Manual__c = cli.Quantity__c;
                            cli.Year_1_Manual__c = cli.Y1_Total_with_Bundle__c;
                            cli.Year_2_Manual__c = cli.Y2_Total_with_Bundle__c;
                            cli.Year_3_Manual__c = cli.Y3_Total_with_Bundle__c;
                            cli.Year_4_Manual__c = cli.Y4_Total_with_Bundle__c;
                            cli.Year_5_Manual__c = cli.Y5_Total_with_Bundle__c;
                        }
                    }
                }
            }
        }
    }
    
    public void updateSort() {
        mWrappers.clear(); 
        integer mNum = 1;
        for(Contract_Line_Item__c cli: contractItems) {
            if (cli.Group_ID__c == null) {
                if (cli.Group_Name__c != null) {
                    mWrappers.add(new lineItem(cli, contractItems, mNum++));  
                } else {
                    mWrappers.add(new lineItem(cli, null, mNum++));  
                }
            }
        }
    }
    
    
    
    public String getAddressString( String addressId ) {
        
        if ( addressId == null || addressId == '' ) return '';
        
        Address__c address = [select Id, Name, City__c, State__c, Zip_Postal_Code__c, Street__c, Account__c from Address__c where Id = :addressId];
        String addressStr = '';
        
        if ( address != null ) {
            addressStr += address.Street__c + '\r\n';
            addressStr += address.City__c + ', ';
            addressStr += address.State__c + ' ';
            addressStr += address.Zip_Postal_Code__c;                                                               
        }
        
        return addressStr;
        
        
    }
   
   
    
}