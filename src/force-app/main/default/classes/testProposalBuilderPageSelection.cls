@isTest(SeeAllData=true)
public class testProposalBuilderPageSelection {
    
   public static testmethod void test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        id sellDocsId = [SELECT Id FROM ContentWorkspace WHERE Name = 'Solution Sheets'].Id;
        id mrktDocsId = [SELECT Id FROM ContentWorkspace WHERE Name = 'Proposal Marketing Content'].Id;
        id stndDocsId = [SELECT Id FROM ContentWorkspace WHERE Name = 'Proposal Standard Content'].Id;
        
        Content_Document_Image__c cdi = new Content_Document_Image__c();
        cdi.Name = 'Mark Rules and Tara Drools';
        insert cdi;
        
        ContentVersion contentVersionObj = new ContentVersion();
        contentVersionObj.Title = 'Penguins';
        contentVersionObj.PathOnClient = 'Penguins.pdf';
        contentVersionObj.VersionData = Blob.valueOf('Test Content');
        contentVersionObj.IsMajorVersion = true;
        contentVersionObj.Document_Owner__c = 'Person';
        contentVersionObj.Document_Owner_Email__c = 'testemail@testingvehs.com';
        contentVersionObj.Creation_Date__c = date.today();
        contentVersionObj.Content_Document_Image__c = cdi.id;
        insert contentVersionObj;
        
        id WorkingcontentDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE id =:contentVersionObj.id].ContentDocumentId;
        
        ContentWorkspaceDoc link = new ContentWorkspaceDoc();
        link.ContentDocumentId = WorkingcontentDocId;
        link.ContentWorkspaceId = sellDocsId;
        
        insert link;

        //find the id of the content document that was created
        id contentDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE id =:contentVersionObj.id].ContentDocumentId;
        
        list<ContentWorkspaceDoc> standardCWSD = [SELECT ContentDocumentId
                                                  FROM ContentWorkspaceDoc 
                                                  WHERE ContentWorkspaceId = :stndDocsId Limit 2];
        
        list<ContentWorkspaceDoc> marketCWSD = [SELECT ContentDocumentId
                                                FROM ContentWorkspaceDoc 
                                                WHERE ContentWorkspaceId = :mrktDocsId Limit 1];
        
        list<ContentWorkspaceDoc> sellSheetCWSD = [SELECT ContentDocumentId
                                                   FROM ContentWorkspaceDoc 
                                                   WHERE ContentWorkspaceId = :sellDocsId Limit 1];        
        
        
       ContentDocument SellSheet1 = [SELECT Description,Id,ParentId,Title, (SELECT Content_Document_Image__c, Proposal_Sell_Sheet_Name__c FROM ContentVersions where isLatest = true) 
                                      FROM ContentDocument 
                                      WHERE Description != 'Legal' and Id = :WorkingcontentDocId Limit 1];

        ContentDocument MarketingSheet1 = [SELECT Description,Id,ParentId,Title, (SELECT Content_Document_Image__c, Proposal_Sell_Sheet_Name__c FROM ContentVersions where isLatest = true) 
                                           FROM ContentDocument 
                                           WHERE FileType = 'PDF' and Description != 'Legal' and Id = :marketCWSD[0].ContentDocumentId  Limit 1 ]; 
        
        
        Product2 pro2 = new Product2(Name='testing', isActive=true, sell_sheet__c = WorkingcontentDocId );       
        insert pro2;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Healthcare_Customer__c='No';
        newQuote.Contract_Length__c = '1 Year';
        insert newQuote;
        
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = pro2.Id;
        myItem.PricebookEntryId__c ='01u3400000ArNjLAAV';
        myItem.Quantity__c = 500;
        myItem.Quote__c = newQuote.Id;
        myItem.Year_1__c = TRUE;
        myItem.Year_2__c = TRUE;
        myItem.Year_3__c = TRUE;
        insert myItem;
        
        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        proposalBuilderPageSelection con = new proposalBuilderPageSelection(); 
        
        //on load with the JSON_Proposal_Data__c being null, we expect the Proposed Solution page and the sell sheet of the related product sell sheet to be in the selected wrappers
        System.debug('LOOKIE HERE' +con.selectedWrappers);
        System.assert(con.selectedWrappers.size() == 2);
        
        //select a sheet to add to selected pages
        string name ='';
        if(MarketingSheet1.ContentVersions[0].Proposal_Sell_Sheet_Name__c != NULL){
            name = MarketingSheet1.ContentVersions[0].Proposal_Sell_Sheet_Name__c;
        }else{
            name = MarketingSheet1.Title;
        }
        
        string uniqueID = name + '=' + MarketingSheet1.Id;
        system.currentpagereference().getparameters().put('currPageName', MarketingSheet1.Title);
        system.currentpagereference().getparameters().put('currPage', uniqueID); 
        system.currentpagereference().getparameters().put('selected', 'true');
        con.setSelectValue();
        
        //check that selected list includes the required proprosed solution and the newly selected additional page
        System.assert(con.selectedWrappers.size() == 3);

        //below is faked string data typically producted by javascript on click of "Next"
        //con.orderedString = MarketingSheet1.Title +'='+MarketingSheet1.Id +',' + StandardSheet1.Title +'='+StandardSheet1.Id ;
        con.orderedString = MarketingSheet1.Title +'='+MarketingSheet1.Id +',' + name +'='+MarketingSheet1.Id ;
        con.Next();
        
        //check that the next button saves a Json string to the quote
        Quote__c quote = [Select JSON_Proposal_Data__c from Quote__c where id = :newQuote.Id];
        System.assert(quote.JSON_Proposal_Data__c != NULL);
        
        //check that on load the data is sorted correctly
        con.getJsonData();
        System.assert(con.previouslySelected.contains(uniqueID)); 
        
    }
    
    
    public static testmethod void test2() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        id sellDocsId = [SELECT Id FROM ContentWorkspace WHERE Name = 'Solution Sheets'].Id;
        id mrktDocsId = [SELECT Id FROM ContentWorkspace WHERE Name = 'Proposal Marketing Content'].Id;
        id stndDocsId = [SELECT Id FROM ContentWorkspace WHERE Name = 'Proposal Standard Content'].Id;
        
        list<ContentWorkspaceDoc> standardCWSD = [SELECT ContentDocumentId
                                                  FROM ContentWorkspaceDoc 
                                                  WHERE ContentWorkspaceId = :stndDocsId Limit 2];
        
        list<ContentWorkspaceDoc> marketCWSD = [SELECT ContentDocumentId
                                                FROM ContentWorkspaceDoc 
                                                WHERE ContentWorkspaceId = :mrktDocsId Limit 1];
        
        list<ContentWorkspaceDoc> sellSheetCWSD = [SELECT ContentDocumentId
                                                   FROM ContentWorkspaceDoc 
                                                   WHERE ContentWorkspaceId = :sellDocsId Limit 1];
        
        Content_Document_Image__c cdi = new Content_Document_Image__c();
        cdi.Name = 'Mark Rules and Tara Drools';
        insert cdi;
        
        ContentVersion contentVersionObj = new ContentVersion();
        contentVersionObj.Title = 'Penguins';
        contentVersionObj.PathOnClient = 'Penguins.pdf';
        contentVersionObj.VersionData = Blob.valueOf('Test Content');
        contentVersionObj.IsMajorVersion = true;
        contentVersionObj.Document_Owner__c = 'Person';
        contentVersionObj.Document_Owner_Email__c = 'testemail@testingvehs.com';
        contentVersionObj.Creation_Date__c = date.today();
        contentVersionObj.Content_Document_Image__c = cdi.id;
        insert contentVersionObj;
        
        id WorkingcontentDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE id =:contentVersionObj.id].ContentDocumentId;

        ContentWorkspaceDoc link = new ContentWorkspaceDoc();
        link.ContentDocumentId = WorkingcontentDocId;
        link.ContentWorkspaceId = sellDocsId;
        insert link;
        
        ContentDocument SellSheet1 = [SELECT Description,Id,ParentId,Title, (SELECT Content_Document_Image__c, Proposal_Sell_Sheet_Name__c FROM ContentVersions where isLatest = true) 
                                      FROM ContentDocument 
                                      WHERE FileType = 'PDF' and Description != 'Legal' and Id =:WorkingcontentDocId Limit 1 ];
        
        ContentDocument MarketingSheet1 = [SELECT Description,Id,ParentId,Title, (SELECT Content_Document_Image__c, Proposal_Sell_Sheet_Name__c FROM ContentVersions where isLatest = true) 
                                           FROM ContentDocument 
                                           WHERE FileType = 'PDF' and Description != 'Legal' and Id = :marketCWSD[0].ContentDocumentId  Limit 1 ]; 
        
        Product2 pro2 = new Product2(Name='testing', isActive=true, sell_sheet__c = WorkingcontentDocId );       
        insert pro2;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Healthcare_Customer__c='No';
        newQuote.Contract_Length__c = '1 Year';
        insert newQuote;
        
        //Base Subscription
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = pro2.Id;
        myItem.PricebookEntryId__c ='01u3400000ArNjLAAV';
        myItem.Quantity__c = 500;
        myItem.Quote__c = newQuote.Id;
        myItem.Year_1__c = TRUE;
        myItem.Year_2__c = TRUE;
        myItem.Year_3__c = TRUE;
        insert myItem;
        
        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        proposalBuilderPageSelection con = new proposalBuilderPageSelection(); 
        
        //on load with the JSON_Proposal_Data__c being null, we expect the Proposed Solution page and the sell sheet of the related product sell sheet to be in the selected wrappers
        System.assert(con.selectedWrappers.size() == 2);
        
        //select a sheet to add to selected pages
        string name ='';
        if(MarketingSheet1.ContentVersions[0].Proposal_Sell_Sheet_Name__c != NULL){
            name = MarketingSheet1.ContentVersions[0].Proposal_Sell_Sheet_Name__c;
        }else{
            name = MarketingSheet1.Title;
        }
        
        
        //string uniqueID = MarketingSheet1.Title+ '=' + MarketingSheet1.Id;
        string uniqueID = name+ '=' + MarketingSheet1.Id;
        system.currentpagereference().getparameters().put('currPageName', name);
        system.currentpagereference().getparameters().put('currPage', uniqueID); 
        system.currentpagereference().getparameters().put('selected', 'true');
        con.setSelectValue();
        
        //check that selected list includes the required proprosed solution and the newly selected additional page
        System.assert(con.selectedWrappers.size() == 3);
        
        //remove a sheet
        system.currentpagereference().getparameters().put('currPageName', name);
        system.currentpagereference().getparameters().put('currPage', uniqueID); 
        system.currentpagereference().getparameters().put('selected', 'false');
        con.setSelectValue();
        
        System.assert(con.selectedWrappers.size() == 2);   
    }
    
     public static testmethod void test3() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        id mrktDocsId = [SELECT Id FROM ContentWorkspace WHERE Name = 'Proposal Marketing Content'].Id;
        list<ContentWorkspaceDoc> marketCWSD = [SELECT ContentDocumentId
                                                FROM ContentWorkspaceDoc 
                                                WHERE ContentWorkspaceId = :mrktDocsId];
        List<string> contentIDs = new list<string>();
        for(ContentWorkspaceDoc cwsd : marketCWSD){
            contentIDs.add(cwsd.ContentDocumentId);
        }
         
         System.debug(contentIDs);

        ContentDocument[] ContentDocs = [SELECT Description,Id,ParentId,Title 
                                         FROM ContentDocument 
                                         WHERE FileType = 'PDF' and Title != 'Legal Page' and Id IN :contentIDs Limit 3 ];
        
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Healthcare_Customer__c='No';
        newQuote.Contract_Length__c = '1 Year';
        
        newQuote.JSON_Proposal_Data__c = '{\"jsonWrapper\":[{\"Id\":"'+ContentDocs[0].Id+'\",\"Name\":"'+ContentDocs[0].Title+ '\",\"identifier\":\"' + ContentDocs[0].Title +'-' + ContentDocs[0].Id +'\",\"Order\":1}]}';
        insert newQuote;
        
        //Base Subscription
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = '01t340000041hqS';
        myItem.PricebookEntryId__c ='01u3400000ArNjLAAV';
        myItem.Quantity__c = 500;
        myItem.Quote__c = newQuote.Id;
        myItem.Year_1__c = TRUE;
        myItem.Year_2__c = TRUE;
        myItem.Year_3__c = TRUE;
        insert myItem;
        
        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        proposalBuilderPageSelection con = new proposalBuilderPageSelection(); 
        
        string tempId = ContentDocs[0].Title + '=' + ContentDocs[0].Id;
        System.assert(con.previouslySelected.contains(tempId)); 
    } 
}