@isTest
private class testOrderItemUtility {
    
    private static testmethod void test_findAccount() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            Account acct = util.createAccount('Test');
            insert acct;
            
            test.startTest();
            Order_Item__c oi = util.createOrderItem('TestVP', 'test123');
            insert oi;
            test.stopTest();
            
            oi = [SELECT id, Account__c
                  FROM Order_Item__c
                  WHERE id = : oi.id LIMIT 1];
            
            // assert the account has been found and assigned
            System.assert(oi.Account__c == acct.id);
        }
    } 
    
    @isTest private static void test_findSalesUsers() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User ehsDir = util.createUser('TestEHS', 'director', vp, null, null);
            ehsDir.Group__c = 'EHS Mid-Market Sales';
            insert ehsDir;
            
            User ccDir = util.createUser('TestCC', 'director', vp, null, null);
            ccDir.Department__c = 'Customer Care';
            insert ccDir;
            
            User ehsSales = [SELECT id
                             FROM User 
                             WHERE Name = 'EHS Sales' LIMIT 1]; 
            
            User customerCare = [SELECT id
                                 FROM User 
                                 WHERE Name = 'Customer Care Sales' LIMIT 1]; 
            
            test.startTest();
            
            Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
            insert oi1;
            
            Order_Item__c oi2 = util.createOrderItem('TestEHS', 'test2');
            insert oi2;
            
            Order_Item__c oi3 = util.createOrderItem('TestCC', 'test3');
            insert oi3;
            
            oi1 = [SELECT id, OwnerId FROM Order_Item__c WHERE id = : oi1.id];
            oi2 = [SELECT id, OwnerId FROM Order_Item__c WHERE id = : oi2.id];
            oi3 = [SELECT id, OwnerId FROM Order_Item__c WHERE id = : oi3.id];
            
            System.assert(oi1.OwnerId == vp.id);
            System.assert(oi2.OwnerId == ehsDir.id);
            System.assert(oi3.OwnerId == customerCare.id);       
            
            test.stopTest();
        }
    }
    
    @isTest private static void test_transformAdminToolData() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            Order_Item__c[] ois = new list<Order_Item__c>();
            for (integer i=1; i<5; i++) {
                ois.add(util.createOrderItem('TestVP', 'test'+i));
            }
            ois[1].Admin_Tool_Order_Status__c = 'B';
            ois[2].Admin_Tool_Order_Status__c = 'C';
            ois[3].Admin_Tool_Order_Status__c = 'D';
            insert ois;
            
            set<id> idSet = new set<id>();
            for (Order_Item__c oi : ois) {
                idSet.add(oi.id);
            }
            
            ois = [SELECT id, Name, Order_Item_ID__c, Status__c
                   FROM Order_Item__c
                   WHERE id IN : idSet
                   ORDER BY Admin_Tool_Order_Status__c ASC];
            
            System.assert(ois[0].Status__c == 'Active');
            System.assert(ois[0].Name == ois[0].Order_Item_ID__c);
            
            System.assert(ois[1].Status__c == 'Done');   
            System.assert(ois[1].Name == ois[1].Order_Item_ID__c);
            
            System.assert(ois[2].Status__c == 'Cancelled');  
            System.assert(ois[2].Name == ois[2].Order_Item_ID__c);
            
            System.assert(ois[3].Status__c == 'Deleted');   
            System.assert(ois[3].Name == ois[3].Order_Item_ID__c);
        }
    }
    
    @isTest private static void test_invoiceChangeDate() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            Order_Item__c oi = util.createOrderItem('TestVP', 'test1');
            insert oi;
            
            test.startTest();
            oi.Invoice_Amount__c = 100;
            update oi;
            
            oi = [SELECT id, Invoice_Amount_Change_Date__c
                  FROM Order_Item__c
                  WHERE id = : oi.id LIMIT 1];
            
            System.assert(oi.Invoice_Amount_Change_Date__c == date.today());
            test.stopTest();
        }
    }
    
    @isTest() private static void test_CustomerSinceAnnualRevenue1() {
        
        //insert test account
        Account a = new Account(Name='Test', customer_status__c = 'Active');
        insert a;
        
        Order_Item__c[] oItems = new List<Order_Item__c>();
        integer orderItemIdNum = 1;
        Order_Item__c oi = new Order_Item__c();
        oi.Account__c = a.id;
        oi.Name = 'Test';
        oi.Invoice_Amount__c = 10;
        oi.Renewal_Amount__c = 500;
        oi.Order_Date__c = date.today();
        oi.Order_ID__c = 'test123';
        oi.Order_Item_ID__c = 'test123'+string.valueOf(orderItemIdNum);
        oi.Year__c = string.valueOf(date.today().year()); 
        oi.Month__c = datetime.now().format('MMMMM');
        oi.Contract_Length__c = 3;
        oi.AccountID__c = 'test123';
        oi.Term_Start_Date__c = date.today();
        oi.Term_End_Date__c = date.today().addYears(1);
        oi.Term_Length__c = 1;
        oi.Contract_Number__c = 'test123';
        oi.Sales_Location__c = 'Chicago';
        oi.Admin_Tool_Order_Type__c = 'New';
        oi.Admin_Tool_Product_Name__c = 'HQ';
        oi.Admin_Tool_Order_Status__c = 'A';
        oi.Product_Platform__c = 'MSDSonline';
        insert oi;
        
        oi.Admin_Tool_Order_Status__c = 'B';
        update oi;
        
        oi.Admin_Tool_Order_Status__c = 'A';
        update oi;
        
        //Assert that the Customer Since date on the account got set correctly
        system.assert([Select id,Customer_Since__c from Account where id =: a.id].Customer_Since__c == date.today());
        
    }
    
    
    @isTest(SeeAllData=true) private static void test_CustomerSinceAnnualRevenue2() {
        //insert test account
        Account a = new Account(Name='Test', customer_status__c = 'Active');
        insert a;
        
        Order_Item__c[] oItems = new List<Order_Item__c>();
        integer orderItemIdNum = 1;
        Order_Item__c oi = new Order_Item__c();
        oi.Account__c = a.id;
        oi.Name = 'Test';
        oi.Invoice_Amount__c = 10;
        oi.Renewal_Amount__c = 500;
        oi.Order_Date__c = date.today();
        oi.Order_ID__c = 'test123';
        oi.Order_Item_ID__c = 'test123'+string.valueOf(orderItemIdNum);
        oi.Year__c = string.valueOf(date.today().year()); 
        oi.Month__c = datetime.now().format('MMMMM');
        oi.Contract_Length__c = 3;
        oi.AccountID__c = 'test123';
        oi.Term_Start_Date__c = date.today();
        oi.Term_End_Date__c = date.today().addYears(1);
        oi.Term_Length__c = 1;
        oi.Contract_Number__c = 'test123';
        oi.Sales_Location__c = 'Chicago';
        oi.Admin_Tool_Order_Type__c = 'New';
        oi.Admin_Tool_Product_Name__c = 'Incident Management';
        oi.Admin_Tool_Order_Status__c = 'A';
        oi.Product_Platform__c = 'EHS';
        insert oi;
       
        oi.Admin_Tool_Order_Status__c = 'B';
        update oi;
        
        oi.Admin_Tool_Order_Status__c = 'A';
        update oi;
    }
    
    @isTest(SeeAllData=true) private static void test_CustomerSinceAnnualRevenue3() {
        //insert test account
        Account a = new Account(Name='Test', customer_status__c = 'Active');
        insert a;
        
        Order_Item__c[] oItems = new List<Order_Item__c>();
        integer orderItemIdNum = 1;
        Order_Item__c oi = new Order_Item__c();
        oi.Account__c = a.id;
        oi.Name = 'Test';
        oi.Invoice_Amount__c = 10;
        oi.Renewal_Amount__c = 500;
        oi.Order_Date__c = date.today();
        oi.Order_ID__c = 'test123';
        oi.Order_Item_ID__c = 'test123'+string.valueOf(orderItemIdNum);
        oi.Year__c = string.valueOf(date.today().year()); 
        oi.Month__c = datetime.now().format('MMMMM');
        oi.Contract_Length__c = 3;
        oi.AccountID__c = 'test123';
        oi.Term_Start_Date__c = date.today();
        oi.Term_End_Date__c = date.today().addYears(1);
        oi.Term_Length__c = 1;
        oi.Contract_Number__c = 'test123';
        oi.Sales_Location__c = 'Chicago';
        oi.Admin_Tool_Order_Type__c = 'New';
        oi.Admin_Tool_Product_Name__c = 'Ergo';
        oi.Admin_Tool_Order_Status__c = 'A';
        oi.Product_Platform__c = 'Ergo';
        insert oi;
        
        oi.Admin_Tool_Order_Status__c = 'B';
        update oi;
        
        oi.Admin_Tool_Order_Status__c = 'A';
        update oi;
    }
    
       
    @isTest() private static void test_AssociateOpportunity() {
       
        // create test account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        // create test contact 
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'System Demo';
        newOpportunity.Contract_Length__c = '5 Years';
        newOpportunity.Suite_Of_Interest__c = 'MSDS Management';
        insert newOpportunity;
        
        // create test contract
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Opportunity__c = newOpportunity.Id;
        insert newContract;  
        
        Order_Item__c[] oItems = new List<Order_Item__c>();
        integer orderItemIdNum = 1;
        Order_Item__c oi = new Order_Item__c();
        oi.Account__c = newAccount.id;
        oi.Name = 'Test';
        oi.Invoice_Amount__c = 10;
        oi.Renewal_Amount__c = 500;
        oi.Order_Date__c = date.today();
        oi.Order_ID__c = 'test123';
        oi.Order_Item_ID__c = 'test123'+string.valueOf(orderItemIdNum);
        oi.Year__c = string.valueOf(date.today().year()); 
        oi.Month__c = datetime.now().format('MMMMM');
        oi.Contract_Length__c = 3;
        oi.AccountID__c = 'test123';
        oi.Term_Start_Date__c = date.today();
        oi.Term_End_Date__c = date.today().addYears(1);
        oi.Term_Length__c = 1;
        oi.Contract_Number__c = [Select Name FROM Contract__c WHERE id =:newContract.id].Name;
        oi.Sales_Location__c = 'Chicago';
        oi.Admin_Tool_Order_Type__c = 'New';
        oi.Admin_Tool_Product_Name__c = 'HQ';
        oi.Admin_Tool_Order_Status__c = 'A';
        oi.Product_Platform__c = 'MSDSonline';
        insert oi;
        
        //Assert that the opportuniy and contract got set correctly
        system.assert([Select id,Contract__c from Order_Item__c where id =: oi.id].Contract__c == newContract.id);
        system.assert([Select id,Opportunity__c from Order_Item__c where id =: oi.id].Opportunity__c == newOpportunity.id);
        
        //Clear opportunity/contract fields and retrigger using an update
        oi.Contract_Number__c = null;
        oi.Contract__c = null;
        oi.Opportunity__c = null;
        update oi;
        
        oi.Contract_Number__c = [Select Name FROM Contract__c WHERE id =:newContract.id].Name;
        update oi;
    }
    
}