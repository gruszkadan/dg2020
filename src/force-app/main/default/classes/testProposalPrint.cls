@isTest(seeAllData=true)
private class testProposalPrint {
    
    static testMethod void proposalPrint2_test1() {
        //insert test account
        Account a = new Account(Name='Test');
        insert a;
        
        //insert test contact
        Contact c = new Contact(AccountId=a.id, LastName='Test');
        insert c;
        
        Quote_Promotion__c p = new Quote_Promotion__c(Name='Promo123', Code__c='P123', Start_Date__c=date.today(), End_Date__c = date.today().addDays(10));
        insert p;
        
        //insert test option package
        Quote_Options__c qop = new Quote_Options__c(Type__c='New');
        insert qop;
        
        Quote__c[] options = new List<Quote__c>();
        for (integer i=0; i<2; i++) {
            Quote__c q = new Quote__c();
            q.Account__c = a.id;
            q.Contact__c = c.id;
            q.Option_Name__c = 'Test '+i;
            q.Quote_Options__c = qop.id;
            q.Contract_Length__c = '3 Years';
            q.Approve_Expired_Promo_Pricing_Terms__c = true;
            q.Promotion__c = p.id;
            q.Include_Option_In_Proposal__c = true;
            options.add(q);
        }
        options[0].Main_Quote_Option__c = true;
        insert options;
        
        Product2[] prods = [SELECT id, Name 
                            FROM Product2 
                            WHERE Name = 'HQ Account' 
                            OR Name = 'eBinder Valet' 
                            OR Name = 'Indexing Field - First Aid'];
        Product2 hqProd = new Product2();
        Product2 ebProd = new Product2();
        Product2 indxProd = new Product2();
        for (Product2 prod : prods) {
            if (prod.Name == 'HQ Account') {
                hqProd = prod;
            }
            if (prod.Name == 'eBinder Valet') {
                ebProd = prod;
            }
            if (prod.Name == 'Indexing Field - First Aid') {
                indxProd = prod;
            }
        }
        
        PricebookEntry[] pbes = [SELECT id, Name
                                 FROM PricebookEntry
                                 WHERE (Name = 'HQ Account'
                                 OR Name = 'eBinder Valet'
                                 OR Name = 'Indexing Field - First Aid')
                                 AND Pricebook2.Name = 'Standard Price Book'];
        PricebookEntry hqPbe = new PricebookEntry();
        PricebookEntry ebPbe = new PricebookEntry();
        PricebookEntry indxPbe = new PricebookEntry();
        for (PricebookEntry pbe : pbes) {
            if (pbe.Name == 'HQ Account') {
                hqPbe = pbe;
            }
            if (pbe.Name == 'eBinder Valet') {
                ebPbe = pbe;
            }
            if (pbe.Name == 'Indexing Field - First Aid') {
                indxPbe = pbe;
            }
        }
        
        Quote_Product__c[] qps = new List<Quote_Product__c>();
        
        for (integer i=0; i<2; i++) {
            Quote_Product__c qp = new Quote_Product__c();
            qp.Quantity__c = 1;
            qp.Name__c = hqProd.Name;
            qp.Quote__c = options[i].id;
            qp.Product__c = hqProd.id;
            qp.Product_Print_Name__c = hqProd.Name;
            qp.PricebookEntryId__c = hqPbe.id;
            qp.Y1_Quote_Price__c = 2.50;
            qp.Approval_Required_by__c = 'Sales VP';
            qps.add(qp); 
        }
        qps[0].Print_Group__c = 'Additional Compliance Solutions';
        
        Quote_Product__c ebinder = new Quote_Product__c();
        ebinder.Quantity__c = 1;
        ebinder.Name__c = ebProd.Name;
        ebinder.Quote__c = options[0].id;
        ebinder.Product__c = ebProd.id;
        ebinder.PricebookEntryId__c = ebPbe.id;
        ebinder.Product_Print_Name__c = ebProd.Name;
        insert ebinder;
        
        Quote_Product__c indexing = new Quote_Product__c();
        indexing.Quantity__c = 1;
        indexing.Name__c = indxProd.Name;
        indexing.Quote__c = options[0].id;
        indexing.Product__c = indxProd.id;
        indexing.PricebookEntryId__c = indxPbe.id;
		indexing.Bundle__c = true;
        indexing.Bundled_Product__c = ebinder.id;
        indexing.Product_Print_Name__c = indxProd.Name;
        indexing.Group_ID__c = ebinder.id;
        indexing.Group_Name__c = ebinder.Name__c;
        qps.add(indexing);
        
        
        
        
        insert qps;
        
        Attachment att = new Attachment();
        att.parentId = options[0].Id;
        att.name = 'logo.jpg';
        att.body = NULL ;
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.body=bodyBlob;
        insert att; 
        
        ApexPages.currentPage().getParameters().put('id', options[0].id);
        proposalPrint2 con = new proposalPrint2();
        
        con.getmyfile();
        con.updateOptions();
        con.backToQuote();
        con.addLogo();
        con.setlogoOnAdd();
        con.saveLogo();
        con.setLogo();
        con.needsApp = true;
        con.submitApp();
        con.attachQuote();
        con.checkPricingTerms();
        
        for (integer i=0; i<2; i++) {
            con.options[i].getsiY1Total();
            con.options[i].getsiY2Total();
            con.options[i].getsiY3Total();
            con.options[i].getsiY4Total();
            con.options[i].getsiY5Total();
            
            con.options[i].getoiY1Total();
            con.options[i].getoiY2Total();
            con.options[i].getoiY3Total();
            con.options[i].getoiY4Total();
            con.options[i].getoiY5Total();
            
            con.options[i].getY1Total();
            con.options[i].getY2Total();
            con.options[i].getY3Total();
            con.options[i].getY4Total();
            con.options[i].getY5Total();
        }
    }
    
    static testmethod void proposalPrint_test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
        
        // Lookup an existing item
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = '01t80000002NON4';
        myItem.PricebookEntryId__c ='01u80000006eap9';
        myItem.Quantity__c = 1;
        myItem.Product_Print_Name__c = 'HQ Account';
        myItem.Name__c = 'HQ Account';
        myItem.Y1_Quote_Price__c = 2.50;
        myItem.Approval_Required_by__c = 'Sales VP';
        myItem.Quote__c = newQuote.Id;
        insert myItem;
        
        Attachment att = new Attachment();
        att.parentId = newQuote.Id;
        att.name = 'logo.jpg';
        att.body = NULL ;
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.body=bodyBlob;
        insert att; 
        
        ApexPages.currentPage().getParameters().put('Id', newQuote.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        proposalPrint myController = new proposalPrint(testController);
        
        myController.addLogo();
        myController.setlogoOnAdd();
        myController.saveLogo();
        myController.setLogo();
        myController.getUser();
        myController.queryPrintOptions();
        myController.getPricingFactor();
        myController.getFormatProposalDate();
        myController.getY1AuthTotal();
        myController.getY2AuthTotal();
        myController.getY3AuthTotal();
        myController.getY4AuthTotal();
        myController.getY5AuthTotal();
        myController.getY1Total();
        myController.getY2Total();
        myController.getY3Total();
        myController.getY4Total();
        myController.getY5Total();
        myController.setPricingSections();
        myController.setShowhqDetail();
        myController.setShowhqRegXRDetail();
        myController.setShowgmDetail();
        myController.backToQuote();
        myController.saveChanges();
        
    }
    
}