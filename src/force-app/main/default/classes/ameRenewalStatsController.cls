public class ameRenewalStatsController {

    public string repsforPie {get;set;}
    public string valuesForPie {get;set;}
    public string repsforBar{get;set;}
    public string valuesForActive{get;set;}
    public string valuesForPending{get;set;}   
    public id renewalId {get;set;}
    public id pendingRenewalQueue {get;set;}
    public map<string, rep> repBatchMap {get;set;}
    public map<string, rep> repStatsMap {get;set;}
    public rep[] repActiveWrappers {get;set;}    
    public rep[] repBatchWrappers {get;set;}
    
    public boolean getTheStats(){

        pendingRenewalQueue = [Select queue.id from queueSobject where queue.developerName = 'Pending_Renewals'].queue.id;
        renewalId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        
        AggregateResult[] stats = [Select count(id) numOfRecords, Owner.Name, Status__c, SUM(AME_ARR__c) ARRtotal, Not_Yet_Processed__c FROM account_management_event__c 
        	WHERE RecordTypeId = :renewalId AND OwnerId != :pendingRenewalQueue AND (Status__c = 'Not Yet Created' OR Status__c = 'Active') 
                                     Group By Owner.Name, Status__c, Not_Yet_Processed__c];
        

        repBatchMap = new Map<string, rep>();
        repStatsMap = new Map<string, rep>();
        for(AggregateResult ar: stats){
            //reps.add(string.ValueOf(ar.get('Name')));
            string n = string.ValueOf(ar.get('Name'));
            if(!repStatsMap.containsKey(n)){
                repStatsMap.put(n, new rep(n));
                repBatchMap.put(n, new rep(n));
            }
            
            decimal arr;
            integer ct = integer.valueOf(ar.get('numOfRecords'));
            if(ar.get('ARRtotal') != NULL ){
                arr = decimal.valueOf(string.ValueOf(ar.get('ARRtotal')));            	
            }else{
                arr = 0;
            }
           
            if(string.ValueOf(ar.get('Status__c')) == 'Active'){
                 repStatsMap.get(n).activeArr = arr;
                 repStatsMap.get(n).totalARR += arr;
                 repStatsMap.get(n).activeOrPendingActiveCount += ct;
            }else if(boolean.ValueOf(ar.get('Not_Yet_Processed__c'))){
                 repBatchMap.get(n).batchArr = arr;
                 repBatchMap.get(n).batchCount = ct;
                 repBatchMap.get(n).totalARR += arr;
            }else{
                 repStatsMap.get(n).pendingActivationArr = arr;
                 repStatsMap.get(n).activeOrPendingActiveCount += ct;
                 repStatsMap.get(n).totalARR += arr;
            }

		}  
        
        repActiveWrappers = new List<rep>(repStatsMap.values());
        repActiveWrappers.sort();

        repBatchWrappers = new List<rep>(repBatchMap.values());
        repBatchWrappers.sort();

        string[] activelastNames = new List<string>();
        decimal[] active = new List<decimal>();
        decimal[] pending = new List<decimal>();

        string[] batchlastNames = new List<string>();
        decimal[] batchArrs = new List<decimal>();

        for(rep w:repActiveWrappers ){
            activelastNames.add(w.repLastName);
            active.add(w.ActiveArr);
            pending.add(w.pendingActivationArr);
        }

        for(rep w:repBatchWrappers ){
        batchlastNames.add(w.repLastName);
        batchArrs.add(w.batchARR);
        }

        repsforBar = '';
        valuesForActive = '';
        valuesForPending = '';
        repsforPie = '';
        valuesForPie = '';

        repsforBar = JSON.serialize(activelastNames);
        valuesForActive = JSON.serialize(active);
        valuesForPending = JSON.serialize(pending);
        repsforPie = JSON.serialize(batchlastNames);
        valuesForPie = JSON.serialize(batchArrs);

        return true;  

    }
    
    
    public class rep implements Comparable{
        public string fullName {get;set;}
        public string repLastName {get;set;}
        public decimal batchArr {get;set;}
        public decimal pendingActivationArr {get;set;}
        public decimal ActiveArr {get;set;}
        public integer batchCount {get;set;}       
        public integer activeOrPendingActiveCount {get;set;}
        public decimal totalARR {get;set;}          

        public rep(string name){
            if(name.contains(' ')){
                repLastName = name.split(' ')[1];
            }else{
              repLastName =  name;
            }
            fullname = name;
            batchArr = 0;
            batchCount = 0; 
            pendingActivationArr = 0;
            ActiveArr = 0;
            activeOrPendingActiveCount = 0;
            totalARR = 0;    

        }

        public Integer compareTo(Object compareTo) {
            if(totalARR == NULL){
                return repLastName.compareTo(((rep)compareTo).repLastName); 
            }else{
                Integer returnValue = 0;
                if (totalARR > ((rep)compareTo).totalARR) {
                    returnValue = -1;
                }
                if (totalARR < ((rep)compareTo).totalARR){
                    returnValue = 1;
                }
                return returnValue; 
            }
        }
    }
    
    

}