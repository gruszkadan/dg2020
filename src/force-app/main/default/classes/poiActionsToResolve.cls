public class poiActionsToResolve {
    id sfId;
    string objType;
    public session[] sessions {get;set;}
    
    public poiActionsToResolve() {
        // Find the ID from the params
        sfId = apexPages.currentPage().getParameters().get('id');
        
        // Check the obj type
        string prefix = string.valueOf(sfId).left(3);
        if (prefix == '001') {
            objType = 'Account';
        } else {
            objType = 'Lead';
        }
        sessions = new list<session>();
        findCreateSessions();
    }
    
    public boolean getviewPOI() {
        boolean viewPOI = [SELECT View_POI__c FROM User WHERE id = : UserInfo.getUserId() LIMIT 1].View_POI__c;
        return viewPOI;
    }
    
    public void findCreateSessions() {
        string q = 'SELECT id, Contact__c, AccountID__c, Lead__c, Products_To_Resolve_MSDS__c, Products_To_Resolve_EHS__c, Products_To_Resolve_AUTH__c, Products_To_Resolve_ODT__c, Products_To_Resolve_ERGO__c, '+
            'Product_Actions_To_Resolve_MSDS__c, Product_Actions_To_Resolve_EHS__c, Product_Actions_To_Resolve_AUTH__c, Product_Actions_To_Resolve_ODT__c, Product_Actions_To_Resolve_ERGO__c, '+
            'POI_Status_MSDS__c, POI_Status_EHS__c, POI_Status_AUTH__c, POI_Status_ODT__c, POI_Status_ERGO__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Title, Contact__r.Email, Contact__r.Phone ';
        q += 'FROM Product_Of_Interest__c ';
        if (objType == 'Account') {
            q += 'WHERE AccountID__c = \''+sfId+'\' ';
        } else {
            q += 'WHERE Lead__c = \''+sfId+'\' ';
        }
        q += 'AND ((Products_To_Resolve_MSDS__c != null AND POI_Status_MSDS__c = \'Open\') OR '+
            '(Products_To_Resolve_EHS__c != null AND POI_Status_EHS__c = \'Open\') OR '+
            '(Products_To_Resolve_AUTH__c != null AND POI_Status_AUTH__c = \'Open\') OR '+
            '(Products_To_Resolve_ODT__c != null AND POI_Status_ODT__c = \'Open\') OR '+
            '(Products_To_Resolve_ERGO__c != null AND POI_Status_ERGO__c = \'Open\'))';
        if (objType == 'Account') {
            q += 'ORDER BY Contact__r.LastName ASC';
        }
        Product_Of_Interest__c[] pois = Database.query(q);
        if (pois.size() > 0) {
            createSessions(pois);
        }
    }
    
    public void createSessions(Product_Of_Interest__c[] pois) {
        for (Product_Of_Interest__c poi : pois) {
            if (poi.Products_To_Resolve_MSDS__c != null && poi.POI_Status_MSDS__c == 'Open') {
                sessions.add(new session(poi, 'MSDS'));
            }
            if (poi.Products_To_Resolve_EHS__c != null && poi.POI_Status_EHS__c == 'Open') {
                sessions.add(new session(poi, 'EHS'));
            }
            if (poi.Products_To_Resolve_AUTH__c != null && poi.POI_Status_AUTH__c == 'Open') {
                sessions.add(new session(poi, 'AUTH'));
            }
            if (poi.Products_To_Resolve_ODT__c != null && poi.POI_Status_ODT__c == 'Open') {
                sessions.add(new session(poi, 'ODT'));
            }
            if (poi.Products_To_Resolve_ERGO__c != null && poi.POI_Status_ERGO__c == 'Open') {
                sessions.add(new session(poi, 'ERGO'));
            }
        }
        if (sessions.size() > 0) {
            reorderSessions();
        }
    }
    
    public void reorderSessions() {
        session[] temp = new list<session>();
        temp.addAll(sessions);
        sessions.clear();
        for (session s : temp) {
            if (s.suite == 'MSDS') {
                sessions.add(s);
            }
        }
        for (session s : temp) {
            if (s.suite == 'EHS') {
                sessions.add(s);
            }
        }
        for (session s : temp) {
            if (s.suite == 'AUTH') {
                sessions.add(s);
            }
        }
        for (session s : temp) {
            if (s.suite == 'ODT') {
                sessions.add(s);
            }
        }
        for (session s : temp) {
            if (s.suite == 'ERGO') {
                sessions.add(s);
            }
        }
    }
    
    public PageReference viewInactive() {
        return new PageReference('/apex/poiArchivedActionsToResolve?id='+sfId).setRedirect(true);
    }
    
    public class session {
        Product_Of_Interest__c poi;
        public string suite {get;set;}
        public string contactName {get;set;}
        public string contactId {get;set;}
        public string title {get;set;}
        public string email {get;set;}
        public string phone {get;set;}
        public string products {get;set;}
        public string actions {get;set;}
        public string newStatus {get;set;}
        
        public session(Product_Of_Interest__c xpoi, string xsuite) {
            suite = xsuite;
            poi = xpoi;
            system.debug('POI '+xpoi);
            if (poi.Contact__c != null) {
                contactName = poi.Contact__r.FirstName+' '+xpoi.Contact__r.LastName;
                contactId = poi.Contact__c;
                title = poi.Contact__r.Title;
                email = poi.Contact__r.Email;
                phone = poi.Contact__r.Phone;
            }
            if (suite == 'MSDS') {
                products = poi.Products_To_Resolve_MSDS__c;
                actions = poi.Product_Actions_To_Resolve_MSDS__c;
            } else if (suite == 'EHS') {
                products = poi.Products_To_Resolve_EHS__c;
                actions = poi.Product_Actions_To_Resolve_EHS__c;
            } else if (suite == 'AUTH') {
                products = poi.Products_To_Resolve_AUTH__c;
                actions = poi.Product_Actions_To_Resolve_AUTH__c;
            } else if (suite == 'ODT') {
                products = poi.Products_To_Resolve_ODT__c;
                actions = poi.Product_Actions_To_Resolve_ODT__c;
            } else if (suite == 'ERGO') {
                products = poi.Products_To_Resolve_ERGO__c;
                actions = poi.Product_Actions_To_Resolve_ERGO__c;
            }
        }
        
        public SelectOption[] getStatusVals() {
            SelectOption[] ops = new list<SelectOption>();
            ops.add(new SelectOption('Open', 'Open'));
            ops.add(new SelectOption('Resolved - Good Title, But Not the DM', 'Resolved - Good Title, But Not the DM'));
            ops.add(new SelectOption('Resolved - Poor Title / No Influence', 'Resolved - Poor Title / No Influence'));
            ops.add(new SelectOption('Resolved - DM Not in My Territory', 'Resolved - DM Not in My Territory'));
            ops.add(new SelectOption('Resolved - Too Small to Call', 'Resolved - Too Small to Call'));
            ops.add(new SelectOption('Resolved - Poor Website', 'Resolved - Poor Website'));
            ops.add(new SelectOption('Resolved - Recently Resolved Product', 'Resolved - Recently Resolved Product'));
            ops.add(new SelectOption('Resolved - Active Customer / No Potential Opp', 'Resolved - Active Customer / No Potential Opp'));
            return ops;
        }   
        
        public boolean getCanEdit() {
            if (suite == 'MSDS') {
                return leadCustomSettings.canEditMSDSSuite();
            } else if (suite == 'EHS') {
                return leadCustomSettings.canEditEHSSuite();
            } else if (suite == 'AUTH') {
                return leadCustomSettings.canEditAUTHSuite();
            } else if (suite == 'ODT') {
                return leadCustomSettings.canEditODTSuite();
            } else if (suite == 'ERGO') {
                return leadCustomSettings.canEditERGOSuite();
            } else {
                return false;
            }
        }
        
        public PageReference saveStatus() {
            if (newStatus != 'Open') {
                if (contactId != null) {
                    Contact c = [SELECT id
                                 FROM Contact
                                 WHERE id = : contactId LIMIT 1];
                    if (c != null) {
                        if (suite == 'MSDS') {
                            c.POI_Status_MSDS__c = newStatus;
                            c.POI_Status_Change_Date_MSDS__c = date.today();
                        } else if (suite == 'EHS') {
                            c.POI_Status_EHS__c = newStatus;
                            c.POI_Status_Change_Date_EHS__c = date.today();
                        } else if (suite == 'AUTH') {
                            c.POI_Status_AUTH__c = newStatus;
                            c.POI_Status_Change_Date_AUTH__c = date.today();
                        } else if (suite == 'ODT') {
                            c.POI_Status_ODT__c = newStatus;
                            c.POI_Status_Change_Date_ODT__c = date.today();
                        } else if (suite == 'ERGO') {
                            c.POI_Status_ERGO__c = newStatus;
                            c.POI_Status_Change_Date_ERGO__c = date.today();
                        }
                        update c;
                    }
                    return new PageReference('/'+poi.AccountID__c).setRedirect(true); 
                } else {
                    Lead l = [SELECT id
                              FROM Lead
                              WHERE id = : poi.Lead__c LIMIT 1];
                    if (l != null) {
                        if (suite == 'MSDS') {
                            l.POI_Status_MSDS__c = newStatus;
                            l.POI_Status_Change_Date_MSDS__c = date.today();
                        } else if (suite == 'EHS') {
                            l.POI_Status_EHS__c = newStatus;
                            l.POI_Status_Change_Date_EHS__c = date.today();
                        } else if (suite == 'AUTH') {
                            l.POI_Status_AUTH__c = newStatus;
                            l.POI_Status_Change_Date_AUTH__c = date.today();
                        } else if (suite == 'ODT') {
                            l.POI_Status_ODT__c = newStatus;
                            l.POI_Status_Change_Date_ODT__c = date.today();
                        } else if (suite == 'ERGO') {
                            l.POI_Status_ERGO__c = newStatus;
                            l.POI_Status_Change_Date_ERGO__c = date.today();
                        }
                        update l;
                    }
                    return new PageReference('/'+poi.Lead__c).setRedirect(true); 
                }
            } else {
                return null;
            }
        }
        
    }
    
    
    
}