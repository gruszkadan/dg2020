@isTest
private class testRetractedDemo {
    
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Event newDemo= new Event();
        newDemo.WhatId = newAccount.Id;
        newDemo.Event_Status__c = 'Scheduled';
        newDemo.ActivityDate = date.today().addDays(-32);
        newDemo.DurationInMinutes = 60;
        newDemo.ActivityDateTime = datetime.now().addDays(-32);
        insert newDemo;
        
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new retractedDemoScheduled());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
    
}