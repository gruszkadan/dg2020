public class adminToolDFUDataAPISend implements Schedulable{
/**
* This scheduled class starts the process to send DFU Data to the Admin Tool
* This calls the adminToolDFUDataQueueable
**/
	 public void execute(SchedulableContext SC) {
        System.enqueueJob(new adminToolDFUDataQueueable());
    }
}