@IsTest
public class testCustomerCountDataController {
    
    @IsTest static void Test1(){
        testDataUtility util = new testDataUtility();
        
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1]; 
        
        Account acct1 = new Account(Name = 'Test Account 1', CustomerID__c = '12345', OwnerId = Dan.Id);
        Account acct2 = new Account(Name = 'Test Account 2', CustomerID__c = '54321', OwnerId = Dan.Id);
        insert acct1;
        insert acct2;
        system.debug(acct2);   
        
        /*Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
        oi1.Product_Suite__c = 'MSDS Management';
        oi1.Admin_Tool_Product_Name__c = 'HQ';
        oi1.account__c = acct2.id;
        oi1.Admin_Tool_Order_Status__c ='A';
        insert oi1;*/
        
        Order_Item__c oi2 = util.createOrderItem('TestEHS', 'test2');
        oi2.Product_Suite__c = 'MSDS Management';
        oi2.Admin_Tool_Product_Name__c = 'HQ';
        oi2.account__c = acct2.id;
        oi2.Admin_Tool_Order_Status__c ='A';
        insert oi2;
        
        Order_Item__c oi3 = util.createOrderItem('TestCC', 'test3');
        oi3.Product_Suite__c = 'MSDS Management';
        oi3.Admin_Tool_Product_Name__c = 'HQ RegXR';
        oi3.account__c = acct2.id;
        oi3.Admin_Tool_Order_Status__c ='A';
        insert oi3;
        
        Order_Item__c oi4 = util.createOrderItem('TestODT', 'test4');
        oi4.Product_Suite__c = 'On-Demand Training';
        oi4.Admin_Tool_Product_Name__c = 'Online Training';
        oi4.account__c = acct2.id;
        oi4.Admin_Tool_Order_Status__c ='A';
        insert oi4;
        
        customerCountDataController con = new customerCountDataController(); 
        //System.assert(con.totalWithoutChemTelAuth == 2);
       // System.assert(con.totalHQandRegX == 2);
        System.assert(con.productCounts.get('HQ') == 1);
        System.assert(con.suiteCounts.get('MSDS Management') == 1);
        System.assert(con.suiteCounts.get('EHS Management') == 0);
        System.assert(con.suiteCounts.get('Ergonomics') == 0);
        System.assert(con.productCounts.get('Online Training') == 1);
    }
    
}