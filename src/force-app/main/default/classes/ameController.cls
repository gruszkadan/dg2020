public with sharing class ameController {
    public Id ameId {get;set;}
    public id rtRenew {get;set;}
    public Id rtAtRisk {get;set;}
    public Id rtCancel {get;set;}
    public string ame {get;set;}
    public Id RecordType {get;set;}
    public Order_Item__c[] ameOrderItems {get;set;}
    public Account_Management_Event__c ameRecord {get;set;}
    public Account_Management_Event__c relatedAme {get;set;}
    public Account_Management_Event__c cancellationQueue {get;set;}
    public string approvalcomments {get;set;}
    Public list<Contact> ContactList {get;set;}
    public boolean canEdit {get;set;}
    Public Opportunity[] Opp {get;set;}
    Public Opportunity[] oList {get;set;}
    public boolean showReqCanButton {get;set;}

    
    //Variables used to pull back new ame info
    public Account_Management_Event__c NewAme {get;set;}
    public id NewAmeId {get;set;}
    
    public ameController(ApexPages.StandardController stdcontroller) {
        
        ameId = ApexPages.currentpage().getparameters().get('id');
        ameRecord = (Account_Management_Event__c)stdcontroller.getRecord();
        ContactList = new list <Contact>();
        
        //Set new id to be able to query the new ame name 
        NewAmeId = ApexPages.currentPage().getParameters().get('RollId');
        
        //Disable inline edit when Status is Pending Approval
        if(ameRecord.Status__c=='Pending Approval')   {
            canEdit=False;
        }else{
            canEdit=true;
        }
        
        //Only run query if NewAmeId variabe has a value 
        if(NewAmeId != null){
            //Query ame name when open tasks get rolled over to new ame and to be able to display the name of the ame in the banner     
            NewAme = [Select Id, name
                      From Account_Management_Event__c
                      Where id = :NewAmeId];
        }
        
         //Query fields to replace Opportunity__c lookup on AME 
       Opp = [Select id, Closed_Reasons__c from Opportunity 
              where Account_Management_Event__c =: ameId 
              order by CreatedDate desc limit 1];     
        
        //Query for related cancellation ame (request cancellation) 
         List<Account_Management_Event__c> reqAME = [SELECT Id, Related_AME__c, RecordType.Name FROM Account_Management_Event__c WHERE RecordType.Name = 'Cancellation' AND Related_AME__c =:ameId LIMIT 1];
        
        if(reqAME.size() > 0){
            showReqCanButton = true;
        }else{
            showReqCanButton = false;
        }
    }
    
    //Creating a list that wil store the result of our query 
    //the query will pull back contacts related to the ame account 
    //this will be used to display contacts in a typeable dropdown list on the new ame form
    public string[] getContacts(){
        list <string> Cont = new list <String>();
        for(Contact con : [Select id, name from contact
                           where Accountid =: ameRecord.account__c
                           order by name ASC]) {
                               Cont.add(con.Name);
                               ContactList.add(con);
                           }
        Return cont;
        
    } 
    //save method to update ameRecord/display error msg
    public void save(){
        if(ameUtility.cansave(ameRecord)){
            update ameRecord; 
        }else{
            apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR,'Please fill in all the applicable fields'));         
        }
    } 
    
    //Delete method
   public pageReference xDelete(){
        delete ameRecord;
        pageReference pr = new pageReference('/apex/account_detail_standard?id='+ameRecord.Account__c);
        pr.setRedirect(true);
        return pr;
    }
    
    //Submit for approval button method 
    public pageReference submitforapproval(){
        //RecordType Variables
        Id rtId  = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();
        Id RenewId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
  
        If(ameRecord.RecordTypeId == rtId){
            //Query for Related Renewal AME RecordType
            List<Account_Management_Event__c> relatedAme = [Select Id, RecordType.Name, Related_AME__c
                                                            From Account_Management_Event__c 
                                                            Where RecordType.Name ='Renewal' AND Id =: ameRecord.Related_AME__c ];
            if(relatedAme.size() > 0){
                //Query for open opportunities
                oList = [select Id, StageName from Opportunity Where IsClosed = False AND Account_Management_Event__c =: relatedAme[0].Id];
                If(ameRecord.RecordTypeId == rtId && relatedAme[0].RecordType.Name == 'Renewal'){
                    if(oList.size()>0){
                        ameUtility.pendingOpp(oList); 
                    }
                }
            }
        }
        ameUtility.submitforapproval(ameId, approvalcomments);
        return new PageReference('/'+ameId);
    }
    
    //Convert to at risk button method
    public pageReference convertAME(){    
        string type = system.currentpagereference().getparameters().get('RecordType');
        Id rtId;       
        if(type == 'Cancellation'){
            rtId  = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();            
        }else if(type == 'At Risk'){
            rtId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('At Risk').getRecordTypeId();
        }
        PageReference pr = Page.ame_new;
        pr.getParameters().put('RecordType', rtId);
        pr.getParameters().put('Id', ameId);
        return pr;
    }  
    
    //Sent to order Method
    public pageReference sentToOrders(){
        ameUtility.cancellationQueue(ameRecord);
         return new PageReference('/'+ameId);

    }
}