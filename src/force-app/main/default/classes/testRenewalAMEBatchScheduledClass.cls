@isTest
public class testRenewalAMEBatchScheduledClass {

   public static string CRON_EXP = '0 0 0 15 3 ? 2025';
    
   @isTest static void test_scheduler() {
        test.startTest();
        string jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new renewalAMEBatchCreationScheduledClass());
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger 
                          WHERE id = : jobId];
       
       System.assertEquals(CRON_EXP, ct.CronExpression);
       System.assertEquals(0, ct.TimesTriggered);
       System.assertEquals('2025-03-15 00:00:00', string.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
   
}