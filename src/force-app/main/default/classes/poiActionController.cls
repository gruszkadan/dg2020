public class poiActionController {
    public Product_of_Interest_Action__c poiAction {get;set;}
    public Product_of_Interest_Action__History[] poiActionHistory {get;set;}
    public User u {get;set;}
    public id poiActionId;
    public poiActionController(ApexPages.StandardController controller) {
        poiActionId = ApexPages.currentPage().getParameters().get('id');
        queryPOIAction();
        poiActionHistory = new list<Product_of_Interest_Action__History>();
        poiActionHistory = [SELECT CreatedBy.Name, CreatedDate, Field, NewValue, OldValue, ParentId FROM Product_of_Interest_Action__History WHERE ParentId =:poiActionId ORDER BY CreatedDate DESC];
        u = [Select id, Profile.Name, Name from User where Id =:UserInfo.getUserId()];
    }
    public void queryPOIAction(){
        String SobjectApiName = 'Product_of_Interest_Action__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String addFields = ', CreatedBy.Name, LastModifiedBy.Name';
        String poiQuery = 'select ' + commaSepratedFields + addFields + ' from ' + SobjectApiName + ' where id = :poiActionId LIMIT 1';
        poiAction = Database.query(poiQuery);
    }
}