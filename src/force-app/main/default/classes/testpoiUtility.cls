@isTest(seeAllData=true)
private class testpoiUtility {
    
    static testmethod void test_marketoIdUpdate() {
        Account newAccount = testAccount();
        insert newAccount;
        Contact newContact = testContact(newAccount.id);
        insert newContact;
        //Create a POI record
        Product_Of_Interest__c poi = new Product_Of_Interest__c();
        poi.Contact__c = newContact.id;
        insert poi;
        Product_Of_Interest_Action__c[] actions = new list<Product_Of_Interest_Action__c>();
        Product_Of_Interest_Action__c msds = contactAction(poi.id, newContact.id, 'msds', 'mkto1');
        Product_Of_Interest_Action__c ehs = contactAction(poi.id, newContact.id, 'ehs', 'mkto1');
        Product_Of_Interest_Action__c auth = contactAction(poi.id, newContact.id, 'auth', 'mkto1');
        Product_Of_Interest_Action__c odt = contactAction(poi.id, newContact.id, 'odt', 'mkto1');
        Product_Of_Interest_Action__c ergo = contactAction(poi.id, newContact.id, 'ergo', 'mkto1');
        actions.add(msds);
        actions.add(ehs);
        actions.add(auth);
        actions.add(odt);
        actions.add(ergo);
        insert actions;
        test.startTest();
        newContact.Marketo_ID__c = 'mkto1';
        
        newContact.POI_Status_MSDS__c = 'Working - Pending Response';
        newContact.POI_Status_EHS__c = 'Working - Pending Response';
        newContact.POI_Status_AUTH__c = 'Working - Pending Response';
        newContact.POI_Status_ODT__c = 'Working - Pending Response';
        newContact.POI_Status_ERGO__c = 'Working - Pending Response';
        
        update newContact;
        test.stopTest();
    }
    
    static testmethod void test_createPOI() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        insert c;
        Lead l = testLead();
        insert l;
        set<Lead> leadSet = new set<Lead>();
        leadSet.add(l);
        set<Contact> contactSet = new set<Contact>();
        contactSet.add(c);
        test.startTest();
        poiUtility.createPOI(leadSet, contactSet);
        test.stopTest();
    }
    
    static testmethod void test_updatePOI() {
        Lead l = testLead();
        insert l;
        poiActionUtility.poiAction[] newPoias = new list<poiActionUtility.poiAction>();
		poiActionUtility.poiAction newPoia = new poiActionUtility.poiAction();
		newPoia.primaryAttributeValueId = '18133';
		newPoia.sfId = l.id;
		newPoia.marketoGUid = 'testL1';
		newPoias.add(newPoia);
        test.startTest();
		POIActionInsert.createPOIActions(newPoias);
        test.stopTest();
    }
    
    static testmethod void test_backupPois() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        insert c;
        Product_Of_Interest__c poi = new Product_Of_Interest__c(Contact__c=c.id);
        insert poi;
        
        test.startTest();
        map<id, Product_Of_Interest__c[]> map1 = new map<id, Product_Of_Interest__c[]>();
        map1.put(c.id, new list<Product_Of_Interest__c>{poi});
        poiUtility.backupPois(map1);
        test.stopTest();
    }
    
    static testmethod void test_poiScoreUpdate() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        insert c;
        Product_Of_Interest__c poi = new Product_Of_Interest__c(Contact__c=c.id);
        insert poi;
        Lead l = testLead();
        insert l;
        Product_Of_Interest__c poi2 = new Product_Of_Interest__c(Lead__c=l.id);
        insert poi2;
        test.startTest();
        c.MSDS_Score__c = 5;
        update c;
        l.MSDS_Score__c = 5;
        update l;
        test.stopTest();
    }

    public static Account testAccount() {
        return new Account(Name='Test Account', OwnerId='00580000007F76q');
    }
    
    public static Contact testContact(id accountId) {
        return new Contact(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', AccountId=accountId);
    }
    
    public static Product_Of_Interest_Action__c contactAction(id poiId, id contactId, string suite, string marketoId) {
        Product_Of_Interest_Action__c action = new Product_Of_Interest_Action__c();
        action.Product_Of_Interest__c = poiId;
        action.Contact__c = contactId;
        action.Marketo_ID__c = marketoId;
        action.Action__c = 'Request Demo';
        action.Session_Open__c = true;
        if (suite == 'msds') {
            action.Product__c = 'HQ';
            action.Product_Suite__c = 'MSDS Management';
            action.Action_ID__c = 'msds1';
        } else if (suite == 'ehs') {
            action.Product__c = 'Safety Meetings';
            action.Product_Suite__c = 'EHS Management';
            action.Action_ID__c = 'ehs1';
        } else if (suite == 'auth') {
            action.Product__c = 'MSDS Authoring';
            action.Product_Suite__c = 'MSDS Authoring';
            action.Action_ID__c = 'auth1';
        } else if (suite == 'odt') {
            action.Product__c = 'Environmental Toolkit';
            action.Product_Suite__c = 'On-Demand Training';
            action.Action_ID__c = 'odt1';
        } else if (suite == 'ergo') {
            action.Product__c = 'Ergonomics';
            action.Product_Suite__c = 'Ergonomics';
            action.Action_ID__c = 'ergo1';
        }
        return action;
    }
    
     public static Lead testLead() {
        return new Lead(FirstName='Test', LastName='Test', Company='TestCompany', Communication_Channel__c='Inbound Call');
    }
    
}