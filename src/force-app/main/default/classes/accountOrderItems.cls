public class accountOrderItems {

    public Order_Item__c[] OrderList {get; set;}
    public Account Account{get; set;}
    public List<Order_Item__c> activeList {get;set;}
    public List<Order_Item__c> inactiveList {get;set;}
    public List<Order_Item__c> pendingCancellationList {get;set;}
    public ID accountId {get;set;}
    public boolean hasErgo {get;set;}
    
    public accountOrderItems(){             
	accountId = ApexPages.currentPage().getParameters().get('id');   
    Account = [SELECT Name, Id FROM Account WHERE Id =: accountId LIMIT 1];    
    OrderList = [SELECT Admin_Tool_Product_Name__c, Order_Id__c, Order_Item_ID__c, Sales_Rep__c, Order_Date__c, Product_Suite__c, Cancellation_Date__c, Site_Shutdown_Date__c,Cancellation_Reason__c, Cancellation_Reason_Other__c,
                 Contract_Length__c, Invoice_Amount__c, Status__c,City__c, Term_Start_Date__c FROM Order_Item__c WHERE Account__c =: accountId];   
     fillLists();    
    }
   
    public void fillLists(){
        hasErgo = false;
        activeList = new List<Order_Item__c>();
        inactiveList = new List<Order_Item__c>(); 
        pendingCancellationList = new List<Order_Item__c>(); 
        
        For(Order_Item__c o : OrderList){        
           if(o.Status__c == 'Active'){             
               if(o.Cancellation_Date__c == NULL){
                   activeList.add(o);
               }else{
                   pendingCancellationList.add(o);
               }
            }
            if(o.Status__c != 'Active'){  
                inactiveList.add(o);  
            }
            if(o.Product_Suite__c == 'Ergonomics'){
                hasErgo = true;
            }
        }   
    }   
}