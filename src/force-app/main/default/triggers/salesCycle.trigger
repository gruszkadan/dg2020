trigger salesCycle on Opportunity (before insert) {	
    
    system.debug('salesCycle running');
    // list of opportunities to loop through
    Opportunity[] opps = new List<Opportunity>();		
    
    // set to hold opportunity account ids
    Set<id> acctIds = new Set<id>();
    
    // loop through opportunities in the trigger
    for (Opportunity opp : trigger.new) {
        
        // if the opportunity has no sales cycle start, add it to the list and add the account id to the set
        if (opp.Sales_Cycle_Start__c == null) {
            opps.add(opp);
            acctIds.add(opp.AccountId);
        }
    }
    
    // if there are opportunities in the list
    if (opps.size() > 0) {
        
        // set to hold account owner ids
        Set<id> acctOwnerIds = new Set<id>();
        Account[] accts = [SELECT id, OwnerId
                           FROM Account
                           WHERE id IN :acctIds];
        
        // loop through the accounts and add the owner ids to the set
        if (accts.size() > 0) {
            for (Account acct : accts) {
                acctOwnerIds.add(acct.OwnerId);
            }
            
            // query the demos linked to the account ids and owner ids
            Event[] demos = [SELECT id, ActivityDate, OwnerId, AccountId
                             FROM Event
                             WHERE Event_Status__c = 'Completed'
                             AND Subject = 'HQ - Demo'
                             AND AccountId IN :acctIds
                             AND OwnerId IN :acctOwnerIds
                             ORDER BY ActivityDate DESC];
            
            // if demos were found
            if (demos.size() > 0) {
                
                // loop through opportunities
                for (Opportunity opp : opps) {
                    
                    // set a variable to hold the opportunity's account
                    Account oppAcct;
                    boolean acctFound = false;
                    
                    // find the account linked to the opportunity
                    for (Account acct : accts) {
                        if (opp.AccountId == acct.id) {
                            oppAcct = acct;
                            acctFound = true;
                        }
                    }
                    
                    // if the opportunity's account has been found
                    if (acctFound) {
                        
                        // if the opportunity owner also owns the linked account
                        if (oppAcct.OwnerId == opp.OwnerId) {
                            
                            // set a variable to hold the most recent demo
                            Event acctDemo;
                            boolean demoFound = false;
                            
                            // loop through the demos and find the first (most recent) matching demo
                            for (Event demo : demos) {
                                if (!demoFound) {
                                    if (demo.AccountId == oppAcct.id && demo.OwnerId == oppAcct.OwnerId) {
                                        acctDemo = demo;
                                        demoFound = true;
                                    }
                                }
                            }
                            
                            // if the demo has been found
                            if (demoFound) {
                                
                                // set the opportunity fields accordingly
                                if (opp.CloseDate >= acctDemo.ActivityDate) {
                                    opp.Sales_Cycle_Start__c = acctDemo.ActivityDate;
                                } else {
                                    opp.Sales_Cycle_Start__c = null;
                                }
                            }

                        }
                    }
                }
            }
        }
    }
  
    
}