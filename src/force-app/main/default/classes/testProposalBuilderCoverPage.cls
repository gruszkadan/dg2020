@isTest
private class testProposalBuilderCoverPage {
    
    
   /* static testmethod void deleteOldLogo() {     
        
        
        Quote__c quote = new Quote__c();
        insert quote;
        
        ApexPages.currentPage().getParameters().put('Id', quote.Id);
        
        Attachment attachment = new Attachment();
        attachment.parentId = quote.Id;
        attachment.name = 'Customer_Logo';
        String myString = 'Blob.jpg';
        Blob myBlob = Blob.valueof(myString);
        attachment.body = myBlob;
        insert attachment;
        
        proposalBuilderCoverPage myController = new proposalBuilderCoverPage(); 
        
        myController.nextPage();
        
        myController = new proposalBuilderCoverPage(); 
        
        system.assertEquals(myController.quoteAttachment.size(), 0);
        
        
    }
    
    
    static testmethod void insertLogo() {     
        
        
        Quote__c quote = new Quote__c();
        insert quote;
        
        ApexPages.currentPage().getParameters().put('Id', quote.Id);
        
        Attachment attachment = new Attachment();
        String myString = 'Blob.jpg';
        Blob myBlob = Blob.valueof(myString);
        attachment.body = myBlob;
        
        proposalBuilderCoverPage myController = new proposalBuilderCoverPage(); 
        
        myController.ourAttachment = attachment;
        myController.attachmentName = 'Test_Logo.jpg';
        myController.nextPage();
        
        
        system.assertEquals(myController.ourAttachment.name, 'Customer_Logo.jpg');
        
        
        
    }
    
    
    static testmethod void deleteLogo() {     
        
        
        Quote__c quote = new Quote__c();
        insert quote;
        
        ApexPages.currentPage().getParameters().put('Id', quote.Id);
        
        Attachment attachment = new Attachment();
        attachment.parentId = quote.Id;
        attachment.name = 'Customer_Logo';
        String myString = 'Blob.jpg';
        Blob myBlob = Blob.valueof(myString);
        attachment.body = myBlob;
        insert attachment;
        
        proposalBuilderCoverPage myController = new proposalBuilderCoverPage(); 
        
        
        myController.deleteLogo();
        
        myController = new proposalBuilderCoverPage(); 
        
        system.assertEquals(myController.quoteAttachment.size(), 0);
        
    }
    */
    static testmethod void displayContact() {     
        
        
        Quote__c quote = new Quote__c();
        quote.Show_Contact_Name__c = false;
        insert quote;
        
        
        ApexPages.currentPage().getParameters().put('Id', quote.Id);
        
        proposalBuilderCoverPage myController = new proposalBuilderCoverPage();
        
        myController.quote.Show_Contact_Name__c = true;
        myController.nextPage();
        

       Quote = [SELECT Id, Show_Contact_Name__c FROM Quote__c WHERE Id = :quote.id];
        
        
        system.assertEquals(quote.Show_Contact_Name__c, true);
        
    }
    
    
}