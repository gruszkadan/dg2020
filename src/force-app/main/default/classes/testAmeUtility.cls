@isTest()
public class testAmeUtility {
    Static testmethod void Test1(){
        // We will need a new ame to be able to test the ameUtillity class
        Account_Management_Event__c NewAme = new Account_Management_Event__c();
        
        // We will need new the record types Ids to make sure the valadation in our utillity class will be active  
        id rtRenew = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        id rtAtRisk = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('At Risk').getRecordTypeId();  
        id rtRAM = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();  
        id rtCancel = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();  
        
        // setting the result varable to all of our possible record types 
        //  boolean Result = ameUtillity.canSave(NewAme, rtRenew, rtAtRisk, rtRAM, rtCancel);
        boolean Result = ameUtility.canSave(NewAme); 
        
        // Covers all valadation in our utillity class. First we need a record type to be selected and we are checking if the result variable was set to false 
        newAme.recordTypeId = rtRenew;
        Result = ameUtility.canSave(NewAme);
        
        newAme.RecordTypeId = rtAtRisk;
        newAme.Status__c = 'Closed';
        Result = ameUtility.canSave(NewAme);
        
        
        newAme.RecordTypeId = rtCancel;
        Result = ameUtility.canSave(NewAme);
        
        // Double checking that our results vairable is storing the correct value 
        system.assertEquals(false, Result);
        
    }
    
    //Update Record Owner Change
    Static testmethod void Test2(){
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1]; 
        user Mark = [SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1];
        
        Account_Management_Event__c NewAme = new Account_Management_Event__c();
        NewAme.OwnerId = Dan.Id;
        NewAme.Status__c = 'Active';
        insert NewAme;
        
        Account Acc = new Account();
        Acc.Name = 'Macys';
        Acc.Customer_Status__c = 'Prospect'; 
        Acc.OwnerID= Dan.Id;
        Acc.NAICS_Code__c = '12345';
        insert Acc;
        
        Contact Con = new Contact();
        Con.LastName = 'Lahners';
        Con.AccountId = Acc.Id;
        Con.OwnerID= Dan.Id;
        insert Con;
        
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Macys1';
        Opp.StageName= 'Closed/Won';
        Opp.OwnerID= Dan.Id;
        opp.AccountId = Acc.id;
        Opp.CloseDate = date.today();
        Opp.Account_Management_Event__c = NewAme.Id;
        insert Opp;
        
        Opportunity Opp1 = new Opportunity();
        Opp1.Name= 'Macys2';
        Opp1.StageName= 'System Demo';
        Opp1.OwnerId= Dan.Id; 
        Opp1.AccountId = Acc.id;
        Opp1.CloseDate = date.today()+15;
        Opp1.Account_Management_Event__c = NewAme.Id;
        insert Opp1;
        
        Quote__c Q = new Quote__c();
        Q.OwnerID= Dan.Id; 
        Q.Account__c = Acc.id;
        Q.Account_Management_Event__c = NewAme.Id;
        Q.Contact__c = Con.Id;
        Q.Contract_length__c = '1 Year';
        Q.Currency__c = 'USD';
        Q.Opportunity__c = Opp1.Id;
        insert Q;
        
        Task T1 = new Task();
        T1.OwnerId = Dan.Id;
        T1.Subject = 'Email';
        T1.Status = 'In Progress';
        T1.Priority = 'Low';
        T1.WhatId = NewAme.Id;
        insert T1;
        
        Task T2 = new Task();
        T2.OwnerId = Dan.Id;
        T2.Subject = 'Call';
        T2.Status = 'Completed';
        T2.Priority = 'Low';
        T2.WhatId = NewAme.Id;
        insert T2;
        
        NewAme.OwnerId = Mark.Id;
        
        Test.startTest();
        update NewAme;
        Test.stopTest();
        Account_Management_Event__c ame = [Select Id, OwnerId,  
                                           (SELECT Id, OwnerId FROM Quotes__r),
                                           (SELECT Id, OwnerId FROM Opportunities__r),
                                           (SELECT Id, OwnerId FROM Tasks)
                                           FROM  Account_Management_Event__c WHERE Id =:NewAme.Id];
        
        for(Opportunity O :ame.Opportunities__r){
            if(O.Id == Opp.Id){
                System.assertEquals(O.OwnerId, Dan.Id); 
            }
            if(O.Id == Opp1.Id){
                System.assertEquals(O.OwnerId, Mark.Id); 
            }
        }
        
        for(Task T :ame.Tasks){
            if(T.Id == T1.Id){
                System.assertEquals(T.OwnerId, Mark.Id); 
            }
            if(T.Id == T2.Id){
                System.assertEquals(T.OwnerId, Dan.Id); 
            }
        }
        
        for(Quote__c Q1 :ame.Quotes__r){
            if(Q1.Id == Q.Id){
                System.assertEquals(Q1.OwnerId, Mark.Id); 
            }
        }
    }
    
    @isTest static void test3() {
        Account testAccount = new Account(Name = 'testAccount');
        insert testAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = testAccount.Id;
        insert newContact;    
        
        Contact newAdminContact = new Contact();
        newAdminContact.FirstName = 'Bob';
        newAdminContact.LastName = 'Admin';
        newAdminContact.AccountID = testAccount.Id;
        newAdminContact.CustomerAdministratorId__c = '12345';
        insert newAdminContact;    
        
        Order_Item__c oi = new Order_Item__c();
        oi.Name = 'Test';
        oi.Account__c = testAccount.id;
        oi.Product_Platform__c = 'MSDSonline';
        oi.Category__c = 'MSDS Management';
        oi.Display_on_Account__c = true;
        oi.Invoice_Amount__c = 10;
        oi.Order_Date__c = date.today();
        oi.Order_ID__c = 'test123';
        oi.Order_Item_ID__c = 'test123';
        oi.Month__c = 'January';
        oi.Admin_Tool_Product_Name__c = 'MSDS Management (licenses)';
        oi.Year__c = '2017';
        oi.Contract_Length__c = 3;
        oi.AccountID__c = '123TEST';
        oi.Term_Start_Date__c = date.today();
        oi.Term_End_Date__c = date.today().addYears(1);
        oi.Term_Length__c = 1;
        oi.Admin_Tool_Order_Status__c = 'A';
        oi.Contract_Number__c = 'test123';
        oi.Sales_Location__c = 'Chicago';
        oi.Admin_Tool_Sales_Rep__c = 'Daniel Gruszka';
        oi.Admin_Tool_Order_Type__c = 'New';
        oi.Renewal_Amount__c = 100;
        oi.Do_Not_Renew__c = false;
        oi.Automatic_Renewal_Creation__c = true;
        oi.Contract_Term_End_Date__c = date.today().toStartOfMonth().addMonths(4).addDays(15);
        insert oi;
        
        Order_Item__c oi2 = new Order_Item__c();
        oi2.Name = 'Test2';
        oi2.Account__c = testAccount.id;
        oi2.Product_Platform__c = 'MSDSonline';
        oi2.Category__c = 'MSDS Management';
        oi2.Display_on_Account__c = true;
        oi2.Invoice_Amount__c = 10;
        oi2.Order_Date__c = date.today();
        oi2.Order_ID__c = 'test123';
        oi2.Order_Item_ID__c = 'test321';
        oi2.Month__c = 'January';
        oi2.Admin_Tool_Product_Name__c = 'HQ';
        oi2.Year__c = '2017';
        oi2.Contract_Length__c = 3;
        oi2.AccountID__c = '123TEST';
        oi2.Term_Start_Date__c = date.today();
        oi2.Term_End_Date__c = date.today().addYears(1);
        oi2.Term_Length__c = 1;
        oi2.Admin_Tool_Order_Status__c = 'A';
        oi2.Contract_Number__c = 'test123';
        oi2.Sales_Location__c = 'Chicago';
        oi2.Admin_Tool_Sales_Rep__c = 'Daniel Gruszka';
        oi2.Admin_Tool_Order_Type__c = 'New';
        oi2.Automatic_Renewal_Creation__c = true;
        oi2.Renewal_Amount__c = 100;
        oi2.Contract_Term_End_Date__c = date.today().toStartOfMonth().addMonths(4).addDays(15); 
        insert oi2;
        
        date startDate = date.today().toStartOfMonth().addMonths(4).addDays(14);
        date endDate = date.today().toStartOfMonth().addMonths(4).addDays(16);        
        ameUtility.renewalAMEBatchCreation(startDate, endDate);
        
        //check that ame was created (AmeId exists) and that each record on order is associated with that ame
        order_item__c[] checkOIs = [Select id, Order_ID__c, Admin_Tool_Product_Name__c, Account_management_event__c from Order_item__c where Order_ID__c = 'test123'];
        id ameID;
        for(order_item__c o: checkOIs){
            System.assert(o.Account_management_event__c != NULL);
            if(ameID != NULL){
                System.assert(o.Account_management_event__c == ameId); 
            }
            ameId = o.Account_management_event__c;
        }
        
        if(ameId != NULL){
            string ContactId = [Select contact__c from Account_Management_Event__c where id = :ameId].contact__c;
            System.assert(ContactId == newAdminContact.id);
        }
    }
    @isTest(SeeAllData=true) static void test4() {
        //Create Account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        //Create Contact
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Primary_Admin__c = true;
        insert newContact;
        
        //Renewal AME
        Account_Management_event__c ame = new Account_Management_event__c();
        ame.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        ame.Account__c = newAccount.id;
        ame.Contact__c = newContact.id;
        ame.Status__c = 'Not Yet Created';
        ame.Batch_Month__c	= 'Mar';   
        ame.Batch_Year__c = '2014';
        insert ame;
        
        //Create Order Item
        testDataUtility Util = new testDataUtility(); 
        order_item__c newOrderItem =  Util.createOrderItem('House Sale', '88654321');
        newOrderItem.Product_Platform__c = 'MSDSonline';
        newOrderItem.Renewal_Amount__c = 100;
        newOrderItem.Account__c = newAccount.Id;
        newOrderItem.Account_Management_Event__c = ame.id;
        insert newOrderItem;
        
        ame.Status__c = 'Active';
        update ame;
        
        //Assert that the opp & quote was auto created
        Quote__c[] ameQuotes = [SELECT id FROM Quote__c WHERE Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameQuotes.size());
        
        Quote_Product__c[] ameQuoteProds = [SELECT id FROM Quote_Product__c WHERE Quote__r.Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameQuoteProds.size());
        
        Opportunity[] ameOpps = [SELECT id FROM Opportunity WHERE Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameOpps.size());
        
        OpportunityLineItem[] ameOLIs = [SELECT id FROM OpportunityLineItem WHERE Opportunity.Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameOLIs.size());
    }
    
    @isTest(SeeAllData=true) public static void test_submitforapproval() {
        
        Account_Management_Event__c newAme = new Account_Management_Event__c();
        Account_Management_Event__c newAme1 = new Account_Management_Event__c();
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1]; 
        
        //Insert newAme record
        newAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();
        newAme.OwnerId = Dan.Id;
        newAme.Status__c = 'Active';
        insert newAme;
        
        newAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        newAme.OwnerId = Dan.Id;
        newAme.Status__c = 'Active';
        insert newAme1;
        
        //Insert Account
        Account Acc = new Account();
        Acc.Name = 'Macys';
        Acc.Customer_Status__c = 'Prospect'; 
        Acc.OwnerID= Dan.Id;
        Acc.NAICS_Code__c = '12345';
        insert Acc; 
        
        
        //Insert Contact
        Contact Con = new Contact();
        Con.LastName = 'Lahners';
        Con.AccountId = Acc.Id;
        Con.OwnerID= Dan.Id;
        insert Con;
        
        
        //Insert Opportunity
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Macys1';
        Opp.StageName= 'Closed/Won';
        Opp.OwnerID= Dan.Id;
        opp.AccountId = Acc.id;
        Opp.CloseDate = date.today();
        Opp.Account_Management_Event__c = newAme.Id;
        insert Opp; 
        
        //Insert Opportunity
        Opportunity Opp1 = new Opportunity();
        Opp1.Name = 'Macys2';
        Opp1.StageName= 'System Demo';
        Opp1.OwnerID= Dan.Id;
        opp1.AccountId = Acc.id;
        Opp1.CloseDate = date.today();
        Opp1.Account_Management_Event__c = newAme1.Id;
        insert Opp1; 
        
        //insert order item
        testDataUtility Util = new testDataUtility();
        Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
        oi1.Order_ID__c = 'test456';
        oi1.Month__c = 'January';
        oi1.Salesforce_Product_Name__c = 'HQ';
        oi1.Category__c = 'MSDS Management';
        oi1.Product_Platform__c = 'MSDSonline';
        oi1.Cancelled_On_AME__c = newAme.Id;
        insert oi1;
        
        
        ameUtility.submitforapproval(newAme.Id, 'Approve');
        newAme = [SELECT Id, Status__c FROM Account_Management_Event__c WHERE Id = :newAme.Id];
        System.assert(newAme.Status__c == 'Pending Approval');
        ameUtility.cancellationQueue(newAme);
        Account_Management_Event__c ame = [Select Id, Status__c
                                           FROM Account_Management_Event__c
                                           WHERE Id = :newAme.Id];
        Group orderCancellation = [Select Id From Group where DeveloperName = 'Order_Cancellation'];
        System.assert(ame.Status__c == 'Sent to Orders');
        
        //Pending Opp method
        List<Opportunity> o = [SELECT Id, StageName, Account_Management_Event__c from Opportunity where Account_Management_Event__c = :newAme1.Id];
        ameUtility.pendingOpp(o);
        Opp1 = [SELECT Id, StageName from Opportunity Where Id = :Opp1.Id]; 
        System.assert(Opp1.StageName == 'Pending Cancellation');
        
        //canAme method
        list<Order_Item__c> oi = [SELECT Id, Cancelled_On_AME__c FROM Order_Item__c WHERE Cancelled_On_AME__c = :newAme.Id];
        ameUtility.canAME(newAme.Id, oi);
        oi1 = [SELECT Id, Cancelled_On_AME__c FROM Order_Item__c WHERE Id = :oi1.Id];
        System.assert(oi1.Cancelled_On_AME__c == newAme.Id);

    }
}