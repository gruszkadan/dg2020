@isTest(SeeAllData=true)
private class testAccountProductInfoUpdater {
    
    static testmethod void test1() {
        //insert test account
        Account a = new Account(Name='Test', customer_status__c = 'Active');
        insert a;
        
        Order_Item__c[] oItems = new List<Order_Item__c>();
        integer orderItemIdNum = 1;
        for (integer i=0; i<8; i++) {
            Order_Item__c oi = new Order_Item__c();
            oi.Account__c = a.id;
            oi.Name = 'Test';
            oi.Invoice_Amount__c = 10;
            oi.Order_Date__c = date.today();
            oi.Order_ID__c = 'test123';
            oi.Order_Item_ID__c = 'test123'+string.valueOf(orderItemIdNum);
            oi.Year__c = string.valueOf(date.today().year()); 
            oi.Month__c = datetime.now().format('MMMMM');
            oi.Contract_Length__c = 3;
            oi.AccountID__c = 'test123';
            oi.Term_Start_Date__c = date.today();
            oi.Term_End_Date__c = date.today().addYears(1);
            oi.Term_Length__c = 1;
            oi.Contract_Number__c = 'test123';
            oi.Sales_Location__c = 'Chicago';
            oi.Admin_Tool_Order_Type__c = 'New';
          
            if (i == 0) {
                oi.Admin_Tool_Product_Name__c = 'HQ';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
                oi.Version_Field__c = 'Chemical_Management_Version__c';
                oi.Version__c = '9.0';
            } if (i == 1) {
                oi.Admin_Tool_Product_Name__c = 'Data Management';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Admin_Tool_Product_Type__c = 'Data Import';
                oi.Product_Platform__c = 'EHS';
            } if (i == 2) {
                oi.Admin_Tool_Product_Name__c = 'Audit and Inspection';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'EHS';
            } if (i == 3) {
                oi.Admin_Tool_Product_Name__c = 'CS - Other';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
            } if (i == 4) {
                oi.Admin_Tool_Product_Name__c = 'Desktop Backup';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
            } if (i == 5) {
                oi.Admin_Tool_Product_Name__c = 'Ergo';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'Ergo';
            } if (i == 6) {
                oi.Admin_Tool_Product_Name__c = 'Ergonomics Consulting Services';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'Ergo';
            } if (i == 7) {
                oi.Admin_Tool_Product_Name__c = 'Internationalization';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Admin_Tool_Product_Type__c = 'Language Translation (per word)';
                oi.Product_Platform__c = 'EHS';
            } 
            oItems.add(oi); 
            orderItemIdNum++;
        }
        insert oItems;
        
        test.startTest();
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'B';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'C';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'B';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'A';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'C';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'A';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'V';
        }
        update oItems;
        test.stopTest();
        
        //Assert that the chemical management version got updated on the account
        system.assert([SELECT Chemical_Management_Version__c FROM Account WHERE id =:a.id LIMIT 1].Chemical_Management_Version__c == '9.0');
    }
    
    static testmethod void test2() {
        //insert test account
        Account a = new Account(Name='Test', customer_status__c = 'Active');
        insert a;
        
        Order_Item__c[] oItems = new List<Order_Item__c>();
        integer orderItemIdNum = 1;
        for (integer i=0; i<18; i++) {
            Order_Item__c oi = new Order_Item__c();
            oi.Account__c = a.id;
            oi.Name = 'Test';
            oi.Invoice_Amount__c = 10;
            oi.Order_Date__c = date.today();
            oi.Order_ID__c = 'test123';
            oi.Order_Item_ID__c = 'test123'+string.valueOf(orderItemIdNum);
            oi.Year__c = string.valueOf(date.today().year()); 
            oi.Month__c = datetime.now().format('MMMMM');
            oi.Contract_Length__c = 3;
            oi.AccountID__c = 'test123';
            oi.Term_Start_Date__c = date.today();
            oi.Term_End_Date__c = date.today().addYears(1);
            oi.Term_Length__c = 1;
            oi.Contract_Number__c = 'test123';
            oi.Sales_Location__c = 'Chicago';
            oi.Admin_Tool_Order_Type__c = 'New';

            if (i == 0 || i == 8) {
                oi.Admin_Tool_Product_Name__c = 'HQ';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
                oi.Version_Field__c = 'Chemical_Management_Version__c';
                oi.Version__c = '9.0';
            } if (i == 1 || i == 9) {
                oi.Admin_Tool_Product_Name__c = 'Data Management';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Admin_Tool_Product_Type__c = 'Data Import';
                oi.Product_Platform__c = 'EHS';
            } if (i == 2 || i == 10) {
                oi.Admin_Tool_Product_Name__c = 'Audit and Inspection';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'EHS';
            } if (i == 3 || i == 11) {
                oi.Admin_Tool_Product_Name__c = 'CS - Other';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
            } if (i == 4 || i == 12) {
                oi.Admin_Tool_Product_Name__c = 'Desktop Backup';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
            } if (i == 5 || i == 13) {
                oi.Admin_Tool_Product_Name__c = 'Ergo';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'Ergo';
            } if (i == 6 || i == 14) {
                oi.Admin_Tool_Product_Name__c = 'Ergonomics Consulting Services';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'Ergo';
            } if (i == 7 || i == 15) {
                oi.Admin_Tool_Product_Name__c = 'Internationalization';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Admin_Tool_Product_Type__c = 'Language Translation (per word)';
                oi.Product_Platform__c = 'EHS';
            }
            if(i == 16){
                oi.Admin_Tool_Product_Name__c = 'Safety Tool Kit';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
            }
            if(i == 17){
                oi.Admin_Tool_Product_Name__c = 'MSDS Management (licenses)';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
            }
            oItems.add(oi); 
            orderItemIdNum++;
        }
        
        
        insert oItems;
        
        
        Order_Item__c o = [Select id, OwnerId from Order_Item__c where id = :oItems[0].id];
        System.debug(o);
        
        test.startTest();
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'B';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'C';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'B';
        }
        update oItems;
        
        for (integer i=0; i<8; i++) {
            oItems[i].Admin_Tool_Order_Status__c = 'V';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'A';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'C';
        }
        update oItems;
        
        for (integer i=0; i<8; i++) {
            oItems[i].Admin_Tool_Order_Status__c = 'V';
        }
        update oItems;
        
       for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'V';
        }
        update oItems;
        
        for(Order_Item__c oi:oItems){
            oi.Admin_Tool_Order_Status__c = 'A';
        }
        update oItems;
        
        test.stopTest();
        
        //Assert that the chemical management version got updated on the account
        system.assert([SELECT Chemical_Management_Version__c FROM Account WHERE id =:a.id LIMIT 1].Chemical_Management_Version__c == '9.0');
    }
    
    static testmethod void testPriorityLogic() {
        //insert test account
        Account a = new Account(Name='Test', customer_status__c = 'Active');
        insert a;
        
        Order_Item__c[] oItems = new List<Order_Item__c>();
        integer orderItemIdNum = 1;
        for (integer i=0; i<2; i++) {
            Order_Item__c oi = new Order_Item__c();
            oi.Account__c = a.id;
            oi.Name = 'Test';
            oi.Invoice_Amount__c = 10;
            oi.Order_Date__c = date.today();
            oi.Order_ID__c = 'test123';
            oi.Order_Item_ID__c = 'test123'+string.valueOf(orderItemIdNum);
            oi.Year__c = string.valueOf(date.today().year()); 
            oi.Month__c = datetime.now().format('MMMMM');
            oi.Contract_Length__c = 3;
            oi.AccountID__c = 'test123';
            oi.Term_Start_Date__c = date.today();
            oi.Term_End_Date__c = date.today().addYears(1);
            oi.Term_Length__c = 1;
            oi.Contract_Number__c = 'test123';
            oi.Sales_Location__c = 'Chicago';
            oi.Admin_Tool_Order_Type__c = 'New';
              if (i == 0) {
                oi.Admin_Tool_Product_Name__c = 'HQ';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
                oi.Order_Date__c = date.today().addDays(1);
            } if (i == 1) {
                oi.Admin_Tool_Product_Name__c = 'HQ';
                oi.Admin_Tool_Order_Status__c = 'C';
                oi.Product_Platform__c = 'MSDSonline';
            }
            oItems.add(oi); 
            orderItemIdNum++;
        }
        insert oItems;
        a = [select id,Active_Products__c,Done_Products__c,Cancelled_Products__c from account where id =: a.id];
        system.assert(a.Active_Products__c.contains('HQ'));
        system.assert(a.Cancelled_Products__c == null);
        system.assert(a.Done_Products__c == null);
        
        //change newest product to Done
        oItems[0].Admin_Tool_Order_Status__c = 'B';
        update oItems;
        a = [select id,Active_Products__c,Done_Products__c,Cancelled_Products__c from account where id =: a.id];
        system.assert(a.Active_Products__c == null);
        system.assert(a.Cancelled_Products__c == null);
        system.assert(a.Done_Products__c.contains('HQ'));
        
        //change older product to Active
        oItems[1].Admin_Tool_Order_Status__c = 'A';
        update oItems;
        a = [select id,Active_Products__c,Done_Products__c,Cancelled_Products__c from account where id =: a.id];
        system.assert(a.Active_Products__c.contains('HQ'));
        system.assert(a.Cancelled_Products__c == null);
        system.assert(a.Done_Products__c == null);
        
        //change older product to Cancelled
        oItems[1].Admin_Tool_Order_Status__c = 'C';
        update oItems;
        a = [select id,Active_Products__c,Done_Products__c,Cancelled_Products__c from account where id =: a.id];
        system.assert(a.Active_Products__c == null);
        system.assert(a.Cancelled_Products__c == null);
        system.assert(a.Done_Products__c.contains('HQ'));
        
        //change newest product to Cancelled
        oItems[0].Admin_Tool_Order_Status__c = 'C';
        update oItems;
        a = [select id,Active_Products__c,Done_Products__c,Cancelled_Products__c from account where id =: a.id];
        system.assert(a.Active_Products__c == null);
        system.assert(a.Cancelled_Products__c.contains('HQ'));
        system.assert(a.Done_Products__c == null);
        
        oItems[0].Admin_Tool_Order_Status__c = 'V';
        update oItems;
        a = [select id,Active_Products__c,Done_Products__c,Cancelled_Products__c from account where id =: a.id];
        system.assert(a.Active_Products__c == null);
        system.assert(a.Cancelled_Products__c.contains('HQ'));
        //System.debug(a.Cancelled_Products__c);
        system.assert(a.Done_Products__c == null);
        
        oItems[1].Admin_Tool_Order_Status__c = 'V';
        update oItems;
        a = [select id,Active_Products__c,Done_Products__c,Cancelled_Products__c from account where id =: a.id];
        system.assert(a.Active_Products__c == null);
        system.assert(a.Cancelled_Products__c == null);
        system.assert(a.Done_Products__c == null);
        
    }
    
}