public class eventTriggerHandler {
    
    // BEFORE INSERT
    public static void beforeInsert(Event[] events) {
        activityUtility.eventCreated(events);
        activityUtility.activityDateEventUpdate(events);
    }
    
    // BEFORE UPDATE
    public static void beforeUpdate(Event[] oldEvents, Event[] newEvents) {
        //Check to See if the Event Owner has Changed
        Event[] ownerChanged = new list<Event>();
        Event[] eventActivityUpdate = new list<Event>();
        for (integer i=0; i<newEvents.size(); i++) {
            if(newEvents[i].OwnerID != oldEvents[i].ownerID){
                ownerChanged.add(newEvents[i]);
            }
            if(newEvents[i].StartDateTime != oldEvents[i].StartDateTime && newEvents[i].StartDateTime != null){
                eventActivityUpdate.add(newEvents[i]);
            }
        }
        if(ownerChanged.size()>0){
            activityUtility.eventOwnerUpdate(ownerChanged);
        }
        if(eventActivityUpdate.size()>0){
            activityUtility.activityDateEventUpdate(eventActivityUpdate);
        }
    }
    
    // BEFORE DELETE
    public static void beforeDelete(Event[] oldEvents, Event[] newEvents) {}
    
    // AFTER INSERT
    public static void afterInsert(Event[] oldEvents, Event[] newEvents) {
        //Check to see if the Event is an HQ or HQRegXR Completed Demo
        Event[] completedHQDemos = new list<Event>();
        for (integer i=0; i<newEvents.size(); i++) {
            //Checking to See if the Event Status is a Completed HQ Demo Created by a Mid-Market or ES Associate
            if(newEvents[i].Event_Status__c =='Completed' && newEvents[i].Subject=='HQ - Demo' &&
               (newEvents[i].Created_By_Current_Role__c.Contains('Associate') && (newEvents[i].Created_By_Group__c =='Mid-Market' || newEvents[i].Created_By_Group__c =='Enterprise Sales'))){
                   //Add the Event to the List
                   completedHQDemos.add(newEvents[i]);
               }
        }
        if(completedHQDemos.size() >0){
            incentiveTransactionUtility.createDemoIncentives(completedHQDemos); 
        }
    }
    
    // AFTER UPDATE
    public static void afterUpdate(Event[] oldEvents, Event[] newEvents) {
        //Check to see if the Event is an HQ or HQRegXR Completed Demo
        Event[] completedHQDemos = new list<Event>();
        
        for (integer i=0; i<newEvents.size(); i++) {
            if(
                //Checking to See if the Event Status changed to Completed
                (oldEvents[i].Event_Status__c !='Completed' && newEvents[i].Event_Status__c =='Completed') && 
                //Checking to See if the Event is an HQ Demo
                newEvents[i].Subject=='HQ - Demo' &&
                //Checking to see if the Event was Created by a Mid-Market or ES Associate
                (newEvents[i].Created_By_Current_Role__c.Contains('Associate') && (newEvents[i].Created_By_Group__c =='Mid-Market' || newEvents[i].Created_By_Group__c =='Enterprise Sales'))                  
            ){
                //Add the Event to the List
                completedHQDemos.add(newEvents[i]);
            }
        }
        
        //Check to see if the Event Status on an HQ Demo was PREVIOUSLY marked Completed and now is NOT
        Event[] unCompletedHQDemos = new list<Event>();
        for (integer i=0; i<newEvents.size(); i++) {
            if(
                //Checking to See if the Event Status changed from Completed to Something Else
                (oldEvents[i].Event_Status__c =='Completed' && newEvents[i].Event_Status__c !='Completed') &&
                //Checking to See if the Event is an HQ Demo
                newEvents[i].Subject=='HQ - Demo'
            ){
                //Add the Event to the List
                unCompletedHQDemos.add(newEvents[i]);
            }
        }
        
        //If there are any events in the list, pass the events to the Incentive Transaction Utlilty so that Incentive Trasactions are Created
        if(completedHQDemos.size() >0){
            incentiveTransactionUtility.createDemoIncentives(completedHQDemos); 
        }
        //If there are any events in the list, pass the events to the Incentive Transaction Utlilty so that Incentive Transactions are Deleted
        if(unCompletedHQDemos.size() >0){
            incentiveTransactionUtility.deleteDemoIncentives(unCompletedHQDemos); 
        }
    }
    
    // AFTER DELETE
    public static void afterDelete(Event[] oldEvents, Event[] newEvents) {
        //Check to see if the Event getting deleted was an HQ Demo where the incentive transaction should be deleted.
        Event[] deletedHQDemos = new list<Event>();
        for (integer i=0; i<oldEvents.size(); i++) {
            //Check to See if the Event was Completed and an HQ Demo
            if(oldEvents[i].Event_Status__c =='Completed' && oldEvents[i].Subject=='HQ - Demo'){
                //Add the Event to the List
                deletedHQDemos.add(oldEvents[i]);     
            }
        }
        //If there are any events in the list, pass the events to the Incentive Transaction Utlilty so that Incentive Transactions are Deleted
        if(deletedHQDemos.size() >0){
            incentiveTransactionUtility.deleteDemoIncentives(deletedHQDemos); 
        }
    }
    
    // AFTER UNDELETE
    public static void afterUndelete(Event[] oldEvents, Event[] newEvents) {}
    
}