/*
 * Test class: testSalesPerformanceRequestDetail
 */
public class salesPerformanceRequestDetailController {
    public id reqId {get;set;}
    public Sales_Performance_Request__c request {get;set;}
    public String viewAs {get;set;}
    public String viewAsDefaultView {get;set;}
    public string userID {get;set;}
    public string userName {get;set;}
    public User u {get;set;}
    public User approver {get;set;}
    public String defaultView {get;set;}
    public ProcessInstance[] approvals {get;set;}
    public ProcessInstanceWorkItem[] piwi {get;set;}
    public String approvalComments {get;set;}
    
    public salesPerformanceRequestDetailController(ApexPages.StandardController stdController){
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias,FirstName, Profile.Name, LastName, Name, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID 
             FROM User 
             WHERE id = :viewAs LIMIT 1];
        userID = u.id;
        userName = u.Name;
        
        //get defaultView to display when viewing as
        defaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
        reqId = apexPages.currentPage().getParameters().get('id');
        request = [SELECT id,Type__c,Automated_Adjustment_Incentive__c,Approval_Required_By__c,Status__c FROM Sales_Performance_Request__c WHERE id =:reqId LIMIT 1];
        approvals = [SELECT CompletedDate, ElapsedTimeInDays, ElapsedTimeInHours, ElapsedTimeInMinutes, Id, ProcessDefinitionId, Status, SubmittedById, TargetObjectId,
                     (SELECT Id, StepStatus, ActorId, Actor.Name, OriginalActorId, OriginalActor.Name, Comments FROM StepsAndWorkitems ORDER BY CreatedDate DESC, Id DESC)
                     FROM ProcessInstance 
                     WHERE TargetObjectId =:reqId 
                     ORDER BY CompletedDate DESC
                     NULLS FIRST
                     LIMIT 1];
        if(approvals!= null && approvals.size() > 0){
            if(request.Type__c == 'Automated Adjustment'){
                request.Automated_Adjustment_Incentive__c = true;
            }
            piwi = [SELECT id,ProcessInstanceId,ActorId
                    FROM ProcessInstanceWorkItem
                    WHERE ProcessInstanceId =:approvals[0].id];
        }
        if(piwi != null && piwi.size() > 0){
            approver = [SELECT id,DelegatedApproverId 
                        FROM User
                        WHERE id =:piwi[0].ActorId];
        }
    }
    
    public void approveReject(){
        String action = ApexPages.currentPage().getParameters().get('approvalAction');
        if(request.Type__c == 'Automated Adjustment' && action == 'Approve'){
            update request;
        }
        Approval.ProcessWorkitemRequest preq = new Approval.ProcessWorkitemRequest();
        preq.setComments(approvalComments);
        preq.setAction(action);
        preq.setWorkitemId(piwi[0].id);
        Approval.ProcessResult result = Approval.process(preq);
    }
}