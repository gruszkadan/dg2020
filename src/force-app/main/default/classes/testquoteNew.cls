@isTest(seeAllData=true)
private class testquoteNew {
    
    static testmethod void test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        insert newOpportunity;
        
        Campaign newCampaign1 = new Campaign();
        newCampaign1.Name = 'Web 2014';
        newCampaign1.Status = 'In Progress';
        newCampaign1.IsActive = TRUE;
        insert newCampaign1;
        
        CampaignMember newMember = new CampaignMember();
        newMember.CampaignId = newCampaign1.Id;
        newMember.ContactId = newContact.Id;
        newMember.Status = 'Responded';
        insert newMember;
        
        Campaign newCampaign = new Campaign();
        newCampaign.Name = 'Unknown';
        newCampaign.Status = 'In Progress';
        newCampaign.IsActive = TRUE;
        insert newCampaign;
        
        //insert a promo
        Quote_Promotion__c p1 = new Quote_Promotion__c(Name = 'p1', Start_Date__c = date.today(), End_Date__c = date.today().addDays(10), Code__c = 'p1', Fees_Language_Changes__c = true, Fees_Language_Changes_Text__c = 'test text');
        p1.Name = 'p1';
        p1.Start_Date__c = date.today();
        p1.End_Date__c = date.today().addDays(10);
        p1.Code__c = 'p1';
        p1.Fees_Language_Changes__c = true;
        p1.Fees_Language_Changes_Text__c = 'p1 text';
        insert p1;
        
        //hq account
        Quote_Promotion_Product__c qpp1 = new Quote_Promotion_Product__c();
        qpp1.Quote_Promotion__c = p1.id;
        qpp1.Name__c = 'HQ Account';
        qpp1.Product__c = '01t80000002NON4';
        qpp1.Discount__c = 1;
        qpp1.Discount_Method__c = 'Amount Off';
        qpp1.Is_Main_Bundled_Product__c = true;
        qpp1.Year_1__c = true;
        qpp1.Auto_Add__c = true;
        insert qpp1;
        
        //hq imp fee
        Quote_Promotion_Product__c qpp2 = new Quote_Promotion_Product__c();
        qpp2.Quote_Promotion__c = p1.id;
        qpp2.Name__c = 'HQ Implementation Fee';
        qpp2.Product__c = '01t80000003Q4sL';
        qpp2.Discount__c = 1;
        qpp2.Discount_Method__c = 'Amount Off';
        qpp2.Bundled_Into__c = qpp1.id;
        qpp2.Year_1__c = true;
        qpp2.Auto_Add__c = true;
        insert qpp2;
        
        //scan pdf
        Quote_Promotion_Product__c qpp3 = new Quote_Promotion_Product__c();
        qpp3.Quote_Promotion__c = p1.id;
        qpp3.Name__c = 'Scan/PDF Process';
        qpp3.Product__c = '01t80000002NOOM';
        qpp3.Discount__c = 1;
        qpp3.Discount_Method__c = 'Amount Off';
        qpp3.Is_Main_Group_Product__c = true;
        qpp3.Year_1__c = true;
        qpp3.Auto_Add__c = true;
        qpp3.group__c = '01t80000002NOOM';
        insert qpp3;
        
        //indexing
        Quote_Promotion_Product__c qpp4 = new Quote_Promotion_Product__c();
        qpp4.Quote_Promotion__c = p1.id;
        qpp4.Name__c = 'Indexing Field - First Aid';
        qpp4.Product__c = '01t80000003kKvz';
        qpp4.Discount__c = 1;
        qpp4.Discount_Method__c = 'Amount Off';
        qpp4.Group__c = qpp3.Product__c;
        qpp4.Year_1__c = true;
        qpp4.Auto_Add__c = true;
        insert qpp4;
        
        ApexPages.currentPage().getParameters().put('CF00N800000055Rbg_lkid', newAccount.Id);
        ApexPages.currentPage().getParameters().put('CF00N800000055Rbj_lkid', newContact.Id);
        ApexPages.currentPage().getParameters().put('CF00N800000055Rcb_lkid', newOpportunity.Id);
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteNew con = new quoteNew(testController);
        
        con.quote.Account__c = newAccount.Id;
        con.quote.Contact__c = newContact.Id;
        con.quote.Contract_Length__c = '3 Years';
        
        con.getQuote();
        System.assert(con.Quote.id == null);
        
        con.getContactSelectList();
        con.getContact();
        con.getCurrencyTypes();        
        con.setName();
        System.assert(con.Quote.Contact__c == newContact.id);
        
        con.selCurrency = 'USD';
        con.selPromo = null;
        con.getContractLengthVals();
        con.selPromo = p1.id;
        con.promo = p1;
        con.quote.Promotion__c = p1.id;
        con.getContractLengthVals();
        for (integer i=1; i<6; i++) {
            if (i == 1) {
                con.promo.Minimum_Contract_Length__c = i+' Year';
            } else {
                con.promo.Minimum_Contract_Length__c = i+' Years';
            }
            con.getContractLengthVals();
        }
        con.getPromoSelectList();
        con.findPromoDetails();
        con.productEntry();
        
        Quote__c testQuote = [SELECT Currency__c, Currency_Rate__c, Currency_Rate_Date__c
                              FROM Quote__c 
                              WHERE id = :con.Quote.id LIMIT 1];
        System.assert(testQuote.Currency__c == 'USD');
        System.assert(testQuote.Currency_Rate__c == 1);
        System.assert(testQuote.Currency_Rate_Date__c == date.today());
        
        
        
        
    } 
    
    
}