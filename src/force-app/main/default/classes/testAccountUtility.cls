@isTest(seeAllData=true)
private class testAccountUtility {
    
    static testmethod void test1() {
        Account parent = new Account(Name = 'Test Account', Customer_Status__c = 'Active', BillingPostalCode = '97031', OwnerID=[SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1].Id, Enterprise_Sales__c = true);
        insert parent;
        Account child1 = new Account(Name = 'Test Account 2', OwnerID=[SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1].Id);
        insert child1;
        Account child2 = new Account(Name = 'Test Account 3', OwnerID=[SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1].Id);
        insert child2;
        Account child3 = new Account(Name = 'Test Account 4', OwnerID=[SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1].Id);
        insert child3;
        Account child4 = new Account(Name = 'Test Account 5', OwnerID=[SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1].Id);
        insert child4;
        Account child5 = new Account(Name = 'Test Account 6', OwnerID=[SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1].Id);
        insert child5;
        Account child6 = new Account(Name = 'Test Account 7', OwnerID=[SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1].Id);
        insert child6;
        
        test.startTest();
        Account[] accts = new list<Account>();
        child1.ParentId = parent.id;
        accts.add(child1);
        
        child2.ParentId = child1.id;
        accts.add(child2);
        
        child3.ParentId = child2.id;
        accts.add(child3);
        
        child4.ParentId = child3.id;
        accts.add(child4);
        
        child5.ParentId = child4.id;
        accts.add(child5);
        
        child6.ParentId = child5.id;
        accts.add(child6);   
        
        update accts;
        
        parent.Customer_Status__c = 'Inactive';
        update parent;
        
        test.stopTest();
    }
    
    
    //checks SAM changes
     static testmethod void test2() {
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1]; 
        user Mark = [SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1];
        
        System.debug('Dan ' + Dan.Id); 
        System.debug('Mark ' + Mark.Id); 
         
        Account parent = new Account(Name = 'Test Account', Customer_Status__c = 'Active', BillingPostalCode = '97031', OwnerID= Dan.Id, Enterprise_Sales__c = true, Platform_Sales_Rep__c= Dan.Id );
        insert parent;
         
        Account child1 = new Account(Name = 'Test Account 2', OwnerID=Mark.Id, ParentId = parent.id, Ultimate_Parent__c = parent.id);
        insert child1;
         
        Account child2 = new Account(Name = 'Test Account 3', OwnerID=Mark.Id, ParentId = child1.id, Ultimate_Parent__c = parent.id);
        insert child2;
        
        System.debug('Child1 info' + child1); 
        System.debug('Child2 info' + child2); 
         
        test.startTest();

        Account newChild = new Account(Name = 'Test Account 3', OwnerID=[SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1].Id, ParentId = parent.id, Ultimate_Parent__c = parent.id);
        insert newChild;
         
        account findnewChild = [Select Platform_Sales_Rep__c from Account where Id = :newchild.Id];
        System.assert(findnewChild.Platform_Sales_Rep__c == Dan.id);
        
        parent.Platform_Sales_Rep__c = Mark.Id;
        parent.Is_Ultimate_Parent__c = true; 
        update Parent; 
         
        account findChild1 = [Select Platform_Sales_Rep__c from Account where Id = :child1.Id];
        account findChild2 = [Select Platform_Sales_Rep__c from Account where Id = :child2.Id];
        
        System.debug(findChild1.Platform_Sales_Rep__c == mark.id); 
        System.debug(findChild2.Platform_Sales_Rep__c == mark.id);

        test.stopTest();
    }
    
    static testMethod void test3(){
        
        Account testAccount1 = new Account(Name = 'Test Account', Customer_Annual_Revenue__c = 15000);
        insert testAccount1;
        
        Account testAccount2 = new Account(Name = 'Test Account', Customer_Annual_Revenue__c = 25000);
        insert testAccount2;
        
        testAccount1.Customer_Annual_Revenue__c = 25000;
        update testAccount1;
        
        testAccount2.Customer_Annual_Revenue__c = 15000;
        update testAccount2;
        
        
        
    }
    
    
}