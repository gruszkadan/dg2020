public class sfNoteUtility {
    
    public static void updateComments(Salesforce_Request__c[] newRequestNotes)
    {
        
        Salesforce_Request_Note__c[] reqNotes = new List<Salesforce_Request_Note__c>();
        
        
        //loop through the Salesforce Requests where Latest_Comment__c has changed
        for (Salesforce_Request__c x : newRequestNotes){
            
            //create note for each request, pair comment for note with latest comment
            Salesforce_Request_Note__c reqNote = new Salesforce_Request_Note__c();
            reqNote.Comments__c = x.Latest_Comment__c;    
            //pair Salesforce Request ID with Salesforce Update ID
            reqNote.Salesforce_Update__c = x.ID;
            reqNote.Jira_Comment__c = true;
            reqNotes.add(reqNote);
        }
        
        try {
            insert reqNotes;
        } catch(exception e) {
            salesforceLog.createLog('Salesforce Request Note', true, 'sfNoteUtility', 'latestComment', string.valueOf(e));
        }
        
        
        
    }
    
}