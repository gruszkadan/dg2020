public class dynamicQuery {
    
    // query a single record
    //pass the object API name, the record id, and any additional relationship fields
    public sObject query(string xsObj, string xaddFields, string xcriteria) {
        map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        map<string, Schema.SObjectField> fieldMap = schemaMap.get(xsObj).getDescribe().fields.getMap();
        string csv = ''; 
        for (string fieldName : fieldMap.keyset()) {
            if (csv == null || csv == '') {
                csv = fieldName;
            } else {
                csv = csv + ', ' + fieldName; 
            }
        }
        if (xaddFields != null && xaddFields != '' && xaddFields != 'none' && xaddFields != 'None') {
            if (xaddFields.left(1) == ',') {
                csv = csv + xaddFields;
            } else {
                csv = csv + ', ' + xaddFields;
            }
        }
        string query = 'SELECT '+csv+' FROM '+xsObj;
        if (xcriteria != null && xcriteria != '' && xcriteria != 'none' && xcriteria != 'None') {
        	query += ' WHERE '+xcriteria;
        }
        query += ' LIMIT 1';
        sObject sObj = Database.query(query);
        return sObj;
    }
    
    // query a list of records
    //pass the object API name, any additional relationship fields, the WHERE criteria, the order, and the limit
    public sObject[] queryList(string xsObj, string xaddFields, string xcriteria, string xorderBy, string xlimit) {
        map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        map<string, Schema.SObjectField> fieldMap = schemaMap.get(xsObj).getDescribe().fields.getMap();
        string csv = ''; 
        for (string fieldName : fieldMap.keyset()) {
            if (csv == null || csv == '') {
                csv = fieldName;
            } else {
                csv = csv + ', ' + fieldName; 
            }
        }
        if (xaddFields != null && xaddFields != '' && xaddFields != 'none' && xaddFields != 'None') {
            if (xaddFields.left(1) == ',') {
                csv = csv + xaddFields;
            } else {
                csv = csv + ', ' + xaddFields;
            }
        }
        string query = 'SELECT ' + csv + ' FROM ' + xsObj;
        if (xcriteria != null && xcriteria != '') {
            query += ' WHERE '+xcriteria;
        }
        if (xorderBy != null && xorderBy != '') {
            query += ' ORDER BY ' + xorderBy;
        }
        if (xlimit != null) {
            query += ' LIMIT '+xlimit;
        }
        sObject[] sObjs = Database.query(query);
        return sObjs;
    }
    
}