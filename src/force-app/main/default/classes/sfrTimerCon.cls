public class sfrTimerCon {
    public User u {get;set;}
    public request[] wrappers {get;set;}
    public integer numOpen {get;set;}
    public string selReqId {get;set;}
    
    Salesforce_Request__c[] requests;
    Set<id> sfrIds;
    
    public sfrTimerCon() {
        u = [SELECT id, Name, Role__c, LastName
             FROM User 
             WHERE id = :UserInfo.getUserId() LIMIT 1];
        createRequestWrappers();
    }
    
    public void createRequestWrappers() {
        wrappers = new List<request>();
        Salesforce_Request_Task__c[] timers = [SELECT id, Start_Date__c, End_Date__c, Owner__c, Work_Request__c, Work_Request__r.Name
                                               FROM Salesforce_Request_Task__c
                                               WHERE Owner__c = :u.id
                                               AND Start_Date__c != null
                                               AND End_Date__c = null
                                               ORDER BY CreatedDate DESC];
        sfrIds = new Set<id>();
        numOpen = timers.size();
        
        for (Salesforce_Request_Task__c timer : timers) {
            sfrIds.add(timer.Work_Request__c);
        }
        
        requests = [SELECT id, Name, Request_Type__c, Status__c, Summary__c, Sidebar_Pin__c, Requested_By__r.Name
                    FROM Salesforce_Request__c
                    WHERE id IN :sfrIds];
        
        for (Salesforce_Request__c request : requests) {
            wrappers.add(new request(request, timers));
        }
    }
    
    public PageReference stopTimer() {
        string reqId = ApexPages.currentPage().getParameters().get('reqid');
        for (request r : wrappers) {
            if (r.request.id == reqId) {
                r.timer.End_Date__c = dateTime.now();
                update r.timer;
            }
        }
        return new PageReference('/apex/sfrTimer').setRedirect(true);
    }
    
    public class request {
        public Salesforce_Request__c request {get;set;}
        public Salesforce_Request_Task__c timer {get;set;}
        public string truncDate {get;set;}
        public string truncSum {get;set;}
        
        public request(Salesforce_Request__c xrequest, Salesforce_Request_Task__c[] xtimers) {
            request = xrequest;
            truncSum = '';
            if (request.Summary__c != null) {
                truncSum = request.Summary__c.abbreviate(30);  
            }
            for (Salesforce_Request_Task__c xtimer : xtimers) {
                if (xtimer.Work_Request__c == request.id) {
                    timer = xtimer;
                    truncDate = xtimer.Start_Date__c.format().remove('/2016').remove('/2017');
                }
            }
        }
    }
    
}