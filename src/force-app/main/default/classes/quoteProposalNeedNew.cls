public class quoteProposalNeedNew {
    
    public descriptionWrapper[] descriptionWrappers {get;set;}
    public Quote_Proposal_Need__c need {get;set;}
    public productWrapper[] productWrappers {get;set;}
    public List<Product2> products {get;set;}
    public List<String> errorMessages {get;set;}
    public string productFamily {get;set;}
    public string query {get;set;}
    public string prod {get;set;}
    public string descr {get;set;}
    public string productFamilyList {get;set;}
    public string productList {get;set;}
    public string pageName {get;set;}
    public boolean error {get;set;}
    
    public quoteProposalNeedNew(ApexPages.StandardController stdController){
        
        pageName = ApexPages.currentPage().getUrl().substringAfter('apex/');
        
        descriptionWrappers = new list<descriptionWrapper>();
        descriptionWrappers.add(new descriptionWrapper(0));
        descriptionWrappers.add(new descriptionWrapper(1));
        descriptionWrappers.add(new descriptionWrapper(2));
        
        need = new Quote_Proposal_Need__c();
        
        queryProducts();
        
        
    }
    
    
    
    public void queryProducts(){
        
        query = 'Select Name, Id, Family FROM Product2 WHERE IsActive = true AND Can_Have_Needs__c = true ';
        
        if(productFamily != null){
            query += 'AND Family = \''+productFamily+'\'';
        }
        
        products = database.query(query);
        products.sort();
        
        productWrappers = new List<productWrapper>();
        
        //loop through list of Products; wrap and put in list of Product Wrappers
        for(product2 p : products){
            if(p.Family != null){
                productWrappers.add(new productWrapper(p));
                
            }
        }
        
        
    }
    
    public List<SelectOption> getFamilyOptions() {
        
        Set<SelectOption> options = new Set<SelectOption>();  
        
        options.add(new selectOption('', '- All -'));
        
        for(product2 p : products){
            if(p.Family != null){
                options.add(new SelectOption(p.family, p.family));
            }
        }
        
        List<SelectOption> optionList = new List<SelectOption>(options);
       
        optionList.sort();
        
        return optionList;
    }
    
    
    public void moveToSelected(){
        
        for(productWrapper p : productWrappers){
            if(p.Action == true){
                p.productStatus = 'Selected'; 
                p.Action = false;
            } 
        }
    }
    
    public void moveToAvailable(){
        
        for(productWrapper p : productWrappers){
            if(p.Action == true){
                p.productStatus = 'Available'; 
                p.Action = false;
            } 
        }
    }
    
    public class descriptionWrapper{
        public integer order {get;set;}
        public string bullet {get;set;}
        
        public descriptionWrapper(integer xOrder){
            order = xOrder; 
        }
    }
    
    public class productWrapper{
        public product2 prod {get;set;}
        public boolean action {get;set;}
        public string productStatus {get;set;}
        
        public productWrapper(product2 xProd){
            productStatus = 'Available';
            prod = xProd;
        }
    }
    
    public void addWrapper(){
        
        integer order = descriptionWrappers.size();
        
        descriptionWrappers.add(new descriptionWrapper(order));
        
    }
    
    
    public void removeBullet() {
        
        Integer bullet = Integer.valueOf(ApexPages.currentPage().getParameters().get('bullet'));
        
        descriptionWrappers.remove(bullet);  
        
        
        //loop through list and reorder 
        for(integer x = 0; x < descriptionWrappers.size(); x++){
            descriptionWrappers[x].order = x;
        }
        
    }
    
    
    public PageReference needsManagementHome(){
        
            
            return new PageReference('/apex/quote_proposal_need_management'); 
        
    }
    
    
    public void createNeed(){
        
        productFamilyList = '';
        productList = null;
        descr = '';
        
        
        //convert description bullets into string
        for(descriptionWrapper d : descriptionWrappers){
            if(d.bullet != null && d.bullet != '' && !d.bullet.isWhiteSpace()){
                if(String.isBlank(descr)){
                    descr = d.bullet;
                } else{
                    descr += ' </li><li> ' + d.bullet;
                }
            }
        }
        
        //create set for products selected and their respective family; Use set to dedup
        Set<String> fam = new Set<String>();
        Set<String> pro = new Set<String>();
        
        
        for(productWrapper p : productWrappers){
            if(p.productStatus == 'Selected'){
                fam.add(p.prod.family);
                pro.add(p.prod.name);
            }
        }
        
        
        //convert family set into comma seperated string
        for(string s: fam){
            
            if(String.isBlank(productFamilyList)){
                productFamilyList = s;
            } else{
                productFamilyList += ', ' +s; 
            }
        }
        
        //convert product set into comma seperated string
        for(string p: pro){
            if(String.isBlank(productList)){
                productList = p;
            } else{
                productList += ', ' +p; 
            }
        }
       
        //list string of missing fields to display on VF page
       errorMessages = new List<String>();        
        
        if(productList == null && need.Display_Need_For_All_Products__c == false){
            errorMessages.add('Selected Products');
        } 
        if(need.Need__c == null){
            errorMessages.add('Need');
        } 
        if(need.Need_Heading__c == null){
            errorMessages.add('Need Heading');
        } 
        
        if(String.isBlank(descr)){
            errorMessages.add('Description');
        } 
        if(errorMessages.size() == 0){
            need.Description__c = descr;
            need.Product__c = productList;
            need.Product_Family__c = productFamilyList;
            
            if(pageName == 'quote_proposal_need_new_user'){
                need.Global__c = false;
                need.Available_For_Use__c = true;
            } 
            
            try{
                
                insert need;
                
            } catch(DMLException e) {
                
                errorMessages.add(String.valueOf(e));
                
            }
            
            
        }
        
        
        
        List<Product_Need__c> prodNeeds = new List<Product_Need__c>();
        
        //loop through selected productWrappers. Create Product Need for each and link to respective Quote Proposal Need
        for(productWrapper p : productWrappers){
            if(p.productStatus == 'Selected'){
                Product_Need__c prodNeed = new Product_Need__c();
                prodNeed.Product__c = p.prod.id;
                prodNeed.Quote_Proposal_Need__c = need.id; 
                prodNeeds.add(prodNeed);
            } 
        }
        
        try{
       
            insert prodNeeds;    
            
        }catch(DMLException e) {
            
            errorMessages.add(String.valueOf(e));
            
        }
        
        
        if(errormessages.size() > 0){
            error = false;
        }else{
            error = true;
        }
        
    
    
    
        
        
    }
    
}