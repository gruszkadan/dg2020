public class ameConsoleHomeController {
    public Account_Management_Event__c ame {get;set;}
    public Account_Management_Event__c[] ameList {get;set;}
    public batchWrapper[] bWrapper {get;set;}
    public upcomingBatchWrapper[] upBatchWrapper {get;set;}
    public waterfallBatchWrapper[] wBatchWrap {get;set;}
    public string userId {get;set;}
    public string piData {get;set;}
    public string percentagePiData {get;set;}
    public string cancelsData {get;set;}
    public string cancelsPercentageData {get;set;}
    public string savesCount {get;set;}
    public string savesPercentage {get;set;}
    public string resignRatePercentage {get;set;}
    public string renewalsNotYetWorked {get;set;}
    public string renewalsWorked {get;set;}
    public string monthSort {get;set;}
    public string recordTypeSort {get;set;}
    public string statusActive {get;set;}
    public string statusPendingApproval {get;set;}
    public string statusApproved {get;set;}
    public string statusSentToOrders {get;set;}
    public string statusNotYetCreated {get;set;}
    public decimal waterfallRenewal {get;set;}
    public decimal waterfallNew {get;set;}
    public decimal waterfallPI {get;set;}
    public decimal waterfallCancel {get;set;}
    public decimal waterfallTotal {get;set;}
    public id rtRenew {get;set;} 
    public id rtCancel {get;set;}
    public string batchMonthLabel {get;set;}
    public string batchYearLabel {get;set;}
    public string rep {get;set;}
    public string userIdChange {get;set;}
    public boolean disablePicklistValues {get;set;}
    public User User {get;set;}
    public User isSelectedUser {get;set;}
    public boolean canViewAs{get;set;}
    public string viewingUserId {get;set;}
    
    public ameConsoleHomeController(){
        //variable for recordtype id
        rtRenew = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId(); 
        rtCancel = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();
        
        //Look for view as parameter
        canViewAs = System.FeatureManagement.checkPermission('AME_Console_View_As');
        if(canViewAs){
            viewingUserId = ApexPages.currentPage().getParameters().get('va');
        }
        if(viewingUserId == null || viewingUserId == ''){
            viewingUserId = userInfo.getUserId();
        }
        
        // current user id info
        userId = viewingUserId;
        
        //Query for current user role
        isSelectedUser = [Select id, Name, UserRole.Name from User where id = :userId];
        
        //if user is manager of retention clear all users
        if(isSelectedUser.UserRole.Name != 'Retention Specialist'){
            userId = '';
        }
        
        //Disable Retention specialist field values for Retention users
        if(isSelectedUser.UserRole.Name == 'Retention Specialist'){
           disablePicklistValues = true;
        }else{
            disablePicklistValues = false;
        }
        // Batch Year condition on load
        // using string.valueof to change condition from integer to string
        batchYearLabel = String.valueof(System.today().toStartOfMonth().addMonths(3).year());
        system.debug(batchYearLabel);
        //calling all wrapper methods
        wrapperAME();
        upcomingWrapper();
        waterfallChart();
        
    }
   //method to call all wrappers in vf page action support 
    public void userIdChange(){
        wrapperAME(); 
        upcomingWrapper();
        waterfallChart();
    }
    
    //to display select drop-down list from batch year field
    //making new list for batch years
    //to get meta data of sobject we are using schema.describe 
    //getting all the picklist values from the field 
    public List<SelectOption> getBatchYears() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult batchYear = Account_Management_Event__c.Batch_Year__c.getDescribe();
        List<Schema.PicklistEntry> ple = batchYear.getPicklistValues();
        //options.add(new SelectOption('', 'All'));
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
        
    //to display select drop-down list from Retention Specialist field
    //adding options to picklist Retention Specialist
   public List<SelectOption> getRetentionUserSelectList() {
        List<SelectOption> options = new List<SelectOption>();
        AggregateResult[] Agr = [SELECT Count(Id)ct, OwnerId, Owner.Name from Account_Management_Event__c where RecordType.Name = 'Renewal' AND Batch_Year__c = :batchYearLabel AND Owner.Type != 'Queue' GROUP BY OwnerId, Owner.Name ORDER BY Owner.Name];
        options.add(new SelectOption('', 'All'));
        for (AggregateResult ar : Agr) {
            options.add(new SelectOption(string.valueof(ar.get('OwnerId')), string.valueof(ar.get('Name'))));
        }
        return options;
    }
    
    public void wrapperAME(){
        
        List<AggregateResult> ameData2 = new List<AggregateResult>();
        List<Account_Management_Event__c> ameData3 = new List<Account_Management_Event__c>();
    
        //<----------***Open AME's Chart Logic***------------->
        
        //making new aggreagte list to get open ames
        List<AggregateResult> ag = new List<AggregateResult>();
        
        //Query for Open AME 
        string openAME = 'Select Count(Id)ct, RecordType.Name, Status__c FROM Account_Management_Event__c WHERE Status__c != \'Closed\' ';
        
        //if userId is not Manager of retention then filter data by ownerId
        if(userId != '' && userId != null){ 
            openAME += ' AND OwnerId = ' + '\''+userId +'\'' ;
        }
            openAME +=  ' GROUP BY RecordType.Name, Status__c ';
        
        //returning list of sobjects 
        ameData2 = database.query(openAME);
        
        //set to return unique(dedupe) RecordType Names  
        Set<string> Rec = new Set<string>(); 
        //looping through aggreagte result ameData2
        //adding recordtype name to the set 
        for(AggregateResult ameRec : ameData2){
            Rec.add(string.valueOf(ameRec.get('Name')));
        }
        //Clearing the strings
        statusActive = '';
        statusPendingApproval = '';
        statusApproved = '';
        statusSentToOrders = '';
        statusNotYetCreated = '';
        recordTypeSort = '';
        
        list<string> tempAME = new list<string>();
        //converting set to list
        tempAME.addAll(Rec);
        
        //Count for Grouping
        integer groupingCount = 0;
        for(string R: Rec){
            groupingCount++;
            for(AggregateResult ameRec : ameData2){
                if(R == ameRec.get('Name')){
                    if(ameRec.get('Status__c') == 'Active'){
                         statusActive += ameRec.get('ct') + ',';
                    }else if(ameRec.get('Status__c') == 'Pending Approval'){
                         statusPendingApproval += ameRec.get('ct') + ',';
                    }else if(ameRec.get('Status__c') == 'Approved'){
                         statusApproved += ameRec.get('ct') + ',';
                    }else if(ameRec.get('Status__c') == 'Sent to Orders'){
                         statusSentToOrders += ameRec.get('ct') + ',';
                    }else if(ameRec.get('Status__c') == 'Not Yet Created'){
                         statusNotYetCreated += ameRec.get('ct') + ',';
                    }
                }
            }
            if(statusActive == '' || (statusActive != '' && statusActive.split(',').size() < groupingCount)){
                statusActive += 0 + ',';
            }
            if(statusPendingApproval == '' || (statusPendingApproval != '' && statusPendingApproval.split(',').size() < groupingCount)){
                statusPendingApproval += 0 + ',';
            }
            if(statusApproved == '' || (statusApproved != '' && statusApproved.split(',').size() < groupingCount)){
                statusApproved += 0 + ',';
            }
            if(statusSentToOrders == '' || (statusSentToOrders != '' && statusSentToOrders.split(',').size() < groupingCount)){
                statusSentToOrders += 0 + ',';
            }
            if(statusNotYetCreated == '' || (statusNotYetCreated != '' && statusNotYetCreated.split(',').size() < groupingCount)){
                statusNotYetCreated += 0 + ',';
            }
        }
        //Serializing the tempAME and setting it to the recordTypeSort
        recordTypeSort = JSON.serialize(tempAME);
        
        //Query for Closed/won opportunities related to Renewal AME
        string queryameOppList = 'SELECT Id, OwnerId, RecordType.Name, Status__c, Num_of_Tasks__c, Batch_Month__c, Batch_Year__c, MonthsOrder__c,'
            +'(SELECT Id, RecordType.Name, Related_AME__c, OwnerId FROM Account_Management_Events__r WHERE RecordType.Name = \'Cancellation\'),'
            +'(SELECT Id, PI__c, Amount, Save__c, StageName FROM Opportunities__r WHERE StageName = \'Closed/Won\')' 
            +'FROM Account_Management_Event__c WHERE RecordType.Name = \'Renewal\' AND Count_in_Stats__c = TRUE';
        if(userId != '' && userId != null){
            queryameOppList += ' AND OwnerId = ' + '\''+userId+'\'';
        }
        if(batchYearLabel != null && batchYearLabel != ''){
            queryameOppList += ' AND Batch_Year__c = ' + '\''+batchYearLabel+'\'';
        }
        if(batchMonthLabel != null && batchMonthLabel != ''){
            queryameOppList += ' AND Batch_Month__c = ' + '\''+batchMonthLabel+'\'';
        }
        /*if(rep != null && rep != ''){
            queryameOppList += ' AND Owner.Name = ' + '\''+rep+'\'';
        }*/
        ameData3 = database.query(queryameOppList);
        
        //set to return unique month numbers
        //adding MonthsOrder field to the set s1
        Set<Decimal> s1 = new Set<Decimal>();
            s1.add(1);
            s1.add(2);
            s1.add(3);
            s1.add(4);
            s1.add(5);
            s1.add(6);
            s1.add(7);
            s1.add(8);
            s1.add(9);
            s1.add(10);
            s1.add(11);
            s1.add(12);
       
        
        //looping through the set
        //initializing new ame
        //looping through the ame query
        //adding ame to the templist
        //creating new batchwrapper with list of ame's
        bWrapper = new List<batchWrapper>();
        for(Decimal numMonth : s1){
            list<Account_Management_Event__c> tempList = new list<Account_Management_Event__c>();
            for(Account_Management_Event__c ameMonth : ameData3){
                if(ameMonth.MonthsOrder__c == numMonth){
                    tempList.add(ameMonth);
                }
            }
            bWrapper.add(new batchWrapper(tempList, numMonth));
        }
        //clearing the strings 
        piData = '';
        percentagePiData = '';
        cancelsData = '';
        cancelsPercentageData = '';
        resignRatePercentage = '';
        savesCount = '';
        savesPercentage = '';
        renewalsNotYetWorked = '';
        renewalsWorked = '';
        monthSort = '';
        
        //Creating list of strings for months
        //making new list to sort months in order
        //adding the set to the list
        //sorting the list
        List<string> ml = new List<string>();
        List<decimal> monthNumList = new List<decimal>();
        monthNumList.addAll(s1);
        monthNumList.sort();
        
        //loop through monthNumList 
        
        for (decimal s: monthNumList) {
            //looping through bwrapper(ame data)
            for(batchWrapper bw :bWrapper){
                //if monthNum(MonthsOrder__c) = s(sorted months) 
                if(s == bw.monthNum){
                    //add month(Batch_Month__c) to list<string> ml
                    ml.add(bw.Month);
                    //Price Increases
                    piData += bw.sumPI + ',';
                    percentagePiData += bw.percentage + ',';
                    system.debug(percentagePiData);
                    //Cancels
                    cancelsData += bw.numOfAMEsWithCancellations + ',';
                    cancelsPercentageData += bw.cancellationCount + ',';
                    //Saves
                    savesCount += bw.opportunityCountSaveIsTrue + ',';
                    savesPercentage += bw.opportunityCount + ',';
                    //Re-sign Rate
                    resignRatePercentage += bw.resignRate + ',';
                    //Renewals Worked/Not Worked
                    renewalsNotYetWorked += bw.openAmeWithNullTasks + ',';
                    renewalsWorked += bw.openAmeWithTasks + ',';
                }
            }
        }
        monthSort = JSON.serialize(ml);
    }
    
    //batch wrapper
    public class batchWrapper {
        //public Account_Management_Event__c[] ameList {get;set;}
        public string Month {get;set;}
        public integer monthNum {get;set;}
        public string Year {get;set;}
        public Decimal sumAmount {get;set;}
        public Decimal sumPI {get;set;}
        public Decimal percentage {get;set;}
        public decimal noOfAmeClosedwonOpps {get;set;}
        public integer totalNoOfAME {get;set;}
        public Decimal resignRate {get;set;}
        public decimal cancellationCount {get;set;}
        public decimal numOfAMEsWithCancellations {get;set;}
        public decimal opportunityCountSaveIsTrue {get;set;}
        public decimal opportunityTotalCount {get;set;}
        public decimal opportunityCount {get;set;}
        public integer openAmeWithNullTasks {get;set;}
        public integer openAmeWithTasks {get;set;}
        
        //ameList = tempList
        public batchWrapper(List<Account_Management_Event__c> ameList, decimal xmonthLabels){  
            // setting xmonthLabels(s1-12 months) to variables
            monthNum = integer.valueof(xmonthLabels);
            //setting the month numbers to string 
            if(monthNum == 1){
                Month = 'Jan';
            }else if(monthNum == 2){
                Month = 'Feb';
            }else if(monthNum == 3){
                Month = 'Mar';
            }else if(monthNum == 4){
                Month = 'Apr';
            }else if(monthNum == 5){
                Month = 'May';
            }else if(monthNum == 6){
                Month = 'Jun';
            }else if(monthNum == 7){
                Month = 'Jul';
            }else if(monthNum == 8){
                Month = 'Aug';
            }else if(monthNum == 9){
                Month = 'Sept';
            }else if(monthNum == 10){
                Month = 'Oct';
            }else if(monthNum == 11){
                Month = 'Nov';
            }else if(monthNum == 12){
                Month = 'Dec';
            }
            //setting all variables to 0 
            noOfAmeClosedwonOpps =0;
            numOfAMEsWithCancellations = 0;
            opportunityCountSaveIsTrue = 0;
            opportunityCount = 0;
            opportunityTotalCount = 0;
            openAmeWithNullTasks = 0;
            openAmeWithTasks = 0;
            sumAmount = 0;
            percentage = 0;
            cancellationCount = 0;
            resignRate = 0;
            sumPI = 0;    
            for(Account_Management_Event__c ame:ameList)   {
                //monthNum = Integer.valueOf(ame.MonthsOrder__c);
                //Month = ame.Batch_Month__c;
                if(ame.Num_of_Tasks__c == null || ame.Num_of_Tasks__c == 0 ){
                    openAmeWithNullTasks++;
                }else{
                    openAmeWithTasks++;
                }
                if(ame.Account_Management_Events__r.size() > 0){
                    numOfAMEsWithCancellations++;
                }
                if(ame.Opportunities__r.size()>0){
                    noOfAmeClosedwonOpps++;
                }
                for(Opportunity Opp :ame.Opportunities__r){
                    if(Opp.Save__c == true){
                        opportunityCountSaveIsTrue++;
                    }
                    sumPI += Opp.PI__c;
                    sumAmount += Opp.Amount;
                    opportunityTotalCount++;
                }
            } 
            if(sumAmount > 0){
                percentage = ((1-((sumAmount - sumPI)/sumAmount))*100).setScale(2);
            }
            if(ameList.size() > 0 && ameList.size() != null){
            resignRate =  ((noOfAmeClosedwonOpps/ameList.size())*100).setScale(2);
            }else{
                resignRate = 0;
            }
            if(ameList.size() > 0 && ameList.size() != null){
                cancellationCount = ((numOfAMEsWithCancellations/ameList.size())*100).setScale(2);
            }else{
                cancellationCount = 0;
            }
            if(opportunityTotalCount > 0){
                opportunityCount = ((opportunityCountSaveIsTrue/opportunityTotalCount)*100).setScale(2); 
            }
        }
    }
    
     //wrapper method for upcoming batch table
    
    public void upcomingWrapper(){
        
        List<Account_Management_Event__c> ameData1 = new List<Account_Management_Event__c>();
        
        //Query for Upcoming Batch
        string queryames = 'SELECT Id, RecordType.Name, OwnerId, Batch_Month__c, Batch_Year__c, Not_Yet_Processed__c, MonthsOrder__c, '
            + 'Status__c, (SELECT Id, Renewal_Amount__c FROM Order_Items__r) FROM Account_Management_Event__c WHERE Status__c = \'Not Yet Created\' AND Not_Yet_Processed__c = False AND RecordType.Name = \'Renewal\' AND Count_in_Stats__c = TRUE';
        
        //condition to render charts based on OwnerId
        if(userId != '' && userId != null){
            queryames += ' AND OwnerId = ' + '\''+userId+'\'' ;
        }
        //condition to render charts based on Year
        /* if(batchYearLabel != null && batchYearLabel != ''){
            queryames += ' AND Batch_Year__c = ' + '\''+batchYearLabel+'\'';
        }
        //condition to render charts based on Month        
        if(batchMonthLabel != null && batchMonthLabel != ''){
            queryames += ' AND Batch_Month__c = ' + '\''+batchMonthLabel+'\'';
        }*/
        ameData1 = database.query(queryames);
        
        //set to return unique month numbers 
        Set<Decimal> s1 = new Set<Decimal>();
        for(Account_Management_Event__c ame : ameData1){
            s1.add(ame.MonthsOrder__c);
        } 
        
         //Creating list of strings for months
        //making new list to sort months in order
        //adding the set to the list
        //sorting the list
        List<string> ml = new List<string>();
        List<decimal> monthNumList = new List<decimal>();
        monthNumList.addAll(s1);
        monthNumList.sort(); 

        //looping through the set
        //initializing new ame
        //looping through the ame query
        //adding ame to the templist
        //creating new batchwrapper with list of ame's
        upBatchWrapper = new List<upcomingBatchWrapper>();
        for(Decimal numMonth : monthNumList){
            list<Account_Management_Event__c> tempList = new list<Account_Management_Event__c>();
            for(Account_Management_Event__c ameMonth : ameData1){
                if(ameMonth.MonthsOrder__c == numMonth){
                    tempList.add(ameMonth);
                }
            }
            upBatchWrapper.add(new upcomingBatchWrapper(tempList));
        }
        
    }
    
    // Wrapper class for upcoming batch table
    public class upcomingBatchWrapper { 
        
        public Account_Management_Event__c[] ameRenewalBatchList {get;set;}
        public string monthYear {get;set;}
        public integer renewalAmountTotalCount {get;set;}
        public decimal renewalAmountTotal {get;set;}
        public integer openAMECount {get;set;}
        
        public upcomingBatchWrapper(List<Account_Management_Event__c> ameRenewalBatchList){
            renewalAmountTotalCount = 0;
            renewalAmountTotal = 0;
            openAMECount = 0;
            //looping through AME related order items Renewal Amount for table
            for(Account_Management_Event__c ame1:ameRenewalBatchList)   {
                for(Order_Item__c OI :ame1.Order_Items__r){
                    if(OI.Renewal_Amount__c != 0 && OI.Renewal_Amount__c != null){
                        renewalAmountTotalCount++;
                        renewalAmountTotal += OI.Renewal_Amount__c;
                    }
                }
                //setting batch month and year column in table
                if(ame1.Batch_Month__c != null){
                    monthYear = ame1.Batch_Month__c + ' ' + ame1.Batch_Year__c;
                }
            }
            openAMECount = ameRenewalBatchList.size();
        }
    }
    
    public void waterfallChart(){
        
        List<Account_Management_Event__c> ameData4 = new List<Account_Management_Event__c>();
        //Query for waterfall chart
        String waterfallAMES = 'SELECT Id, RecordType.Name, OwnerId, Batch_Month__c, Batch_Year__c, Not_Yet_Processed__c, MonthsOrder__c, '
            + 'Status__c, (SELECT Id, Renewal_Amount__c, Cancelled_On_AME__c FROM Order_Items__r), (SELECT Id, PI__c, Amount, New_Revenue__c FROM Opportunities__r ) FROM Account_Management_Event__c WHERE RecordType.Name = \'Renewal\' AND Count_in_Stats__c = TRUE';
        
        //condition to render charts based on OwnerId
        if(userId != '' && userId != null){
            waterfallAMES += ' AND OwnerId = ' + '\''+userId+'\'' ;
        }
        //condition to render charts based on Year
        if(batchYearLabel != null && batchYearLabel != ''){
            waterfallAMES += ' AND Batch_Year__c = ' + '\''+batchYearLabel+'\'';
        }
        //condition to render charts based on Month        
        if(batchMonthLabel != null && batchMonthLabel != ''){
            waterfallAMES += ' AND Batch_Month__c = ' + '\''+batchMonthLabel+'\'';
        }
        ameData4 = database.query(waterfallAMES);
        
        //set to return unique month numbers 
        Set<Decimal> s1 = new Set<Decimal>();
        for(Account_Management_Event__c ame : ameData4){
            s1.add(ame.MonthsOrder__c);
        } 
        
         //Creating list of strings for months
        //making new list to sort months in order
        //adding the set to the list
        //sorting the list
        List<string> ml = new List<string>();
        List<decimal> monthNumList = new List<decimal>();
        monthNumList.addAll(s1);
        monthNumList.sort(); 

        //looping through the set
        //initializing new ame list
        //looping through the ame query
        //adding ame to the templist
        //creating new batchwrapper with list of ame's
        wBatchWrap = new List<waterfallBatchWrapper>();
        for(Decimal numMonth : monthNumList){
            list<Account_Management_Event__c> tempList = new list<Account_Management_Event__c>();
            for(Account_Management_Event__c ameMonth : ameData4){
                if(ameMonth.MonthsOrder__c == numMonth){
                    tempList.add(ameMonth);
                }
            }
            wBatchWrap.add(new waterfallBatchWrapper(tempList));
        }
        
        waterfallRenewal = 0;
        waterfallNew = 0;
        waterfallPI = 0;
        waterfallCancel = 0;
        waterfallTotal = 0;
        
        for(waterfallBatchWrapper wbw :wBatchWrap){
                //Renewal
                waterfallRenewal += wbw.sumRenewalAmount;
                //Cancel
                waterfallCancel += wbw.negativeSumOfRenewalAmount;
                //New
                waterfallNew += wbw.sumNewRevenue;
                //PI
                waterfallPI += wbw.sumPIWaterfall;
                //Total
                waterfallTotal += wbw.sumAll;
            }
    }

    public class waterfallBatchWrapper{
        public Account_Management_Event__c[] ameWaterfallList {get;set;}
        public decimal sumRenewalAmount {get;set;}
        public decimal sumNewRevenue {get;set;}
        public decimal sumPIWaterfall {get;set;}
        public decimal sumAll {get;set;}
        public decimal negativeSumOfRenewalAmount {get;set;}
        public string Month {get;set;}
        public integer monthNum {get;set;}
        
        public waterfallBatchWrapper(List<Account_Management_Event__c> ameWaterfallList){
        sumRenewalAmount = 0;
        sumNewRevenue = 0;
        sumPIWaterfall = 0;
        sumAll =0;
        negativeSumOfRenewalAmount = 0;
        //looping through ameData4
        for(Account_Management_Event__c ameWaterfall : ameWaterfallList){
             monthNum = Integer.valueOf(ameWaterfall.MonthsOrder__c);
             Month = ameWaterfall.Batch_Month__c;
            for(Order_Item__c oi : ameWaterfall.Order_Items__r){
                if(oi.Renewal_Amount__c != null){
                    sumRenewalAmount += oi.Renewal_Amount__c;
                }
                if(oi.Cancelled_On_AME__c != null && oi.Renewal_Amount__c != null){
                    negativeSumOfRenewalAmount += oi.Renewal_Amount__c;
                }
            }
            for(Opportunity oppWaterfall : ameWaterfall.Opportunities__r){
                sumNewRevenue += oppWaterfall.New_Revenue__c;
                sumPIWaterfall += oppWaterfall.PI__c;
            }
        }
        sumAll = sumRenewalAmount - negativeSumOfRenewalAmount + sumNewRevenue + sumPIWaterfall;
    }
  }
}