public with sharing class quoteProductBundle {
    private Apexpages.StandardController controller; 
    public List<QuoteProductWrapper> quoteProductList{get;set;}
    public Quote__c theQuote {get;set;}
    public Quote_Product__c theMainProduct {get;set;}
    public Quote_Product_Bundle__c theQuoteProductBundle {get;set;}
    public String qpMainId {get; set;}
    public String bundleID {get; set;}
    public Quote_Product_Bundle__c quoteProductBundle {get;set;}
    public Quote_Product_Bundle__c[] theBundles {get;set;}
    public Quote_Product__c [] theBundledProducts {get;set;}
    public Quote_Product__c [] theShoppingCart {get;set;}
    public String dID{get;set;}
    public Quote_Product__c bp {get;set;}
    public Quote_Product__c theVerification {get;set;}
    // ryans additions
    boolean isOption = false;
    public User u {get;set;}
    
    public quoteProductBundle(ApexPages.StandardController stdController){
        this.quoteProductList  = new List<quoteProductWrapper>();
        Id quoteId = ApexPages.currentPage().getParameters().get('id');
        Id qpMainId = ApexPages.currentPage().getParameters().get('qpMainId');
        Id mainProductID = ApexPages.currentPage().getParameters().get('mainProductID');
        u = [Select id, Name, LastName, Email, ManagerID, Department__c, Group__c from User where id =: UserInfo.getUserId() LIMIT 1];
        theQuote = [SELECT Id, Name, Department__c, Contract_Length__c, Y1_Total__c, Y2_Total__c, Y3_Total__c, Y4_Total__c, Y5_Total__c, Total_Value__c, Travelers_Customer__c, Count_of_Employees__c, Quote_Options__c 
                    FROM Quote__c 
                    WHERE Id = :quoteId 
                    LIMIT 1];
        if (theQuote.Quote_Options__c != null) {
            isOption = true;
        }
        SYSTEM.DEBUG('isOption ====== '+isOption);
        theShoppingCart = [select Id, Product__r.Proposal_Print_Quantity_Name__c, Year_1__c, Year_2__c, Year_3__c, Year_4__c, Year_5__c, Product_Print_Name__c, Standard_MSDS_Inclusion_1__c, Standard_MSDS_Inclusion_2__c,
                           Standard_MSDS_Inclusion_3__c, Bundled_Product__c, Print_Group__c,
                           Product__r.Description, Quantity__c, Name__c, type__c,
                           Y1_Total_Price__c, Y2_Total_Price__c, Y3_Total_Price__c, Y4_Total_Price__c, Y5_Total_Price__c,
                           Y1_Bundled_Price__c, Y2_Bundled_Price__c, Y3_Bundled_Price__c, Y4_Bundled_Price__c, Y5_Bundled_Price__c,
                           Quantity_Field_Name__c, Y1_Total_with_Bundle__c, Y2_Total_with_Bundle__c, Y3_Total_with_Bundle__c, 
                           Y4_Total_with_Bundle__c, Y5_Total_with_Bundle__c, Total_Bundle_Amount__c, Quote__r.Y1_Total__c,
                           Quote__r.Y2_Total__c, Quote__r.Y3_Total__c, Quote__r.Y4_Total__c,
                           Quote__r.Y5_Total__c from Quote_Product__c WHERE Quote__c=:theQuote.Id AND Product__r.Parent_Product__c = NULL];
        theBundles = [select ID, Name, Main_Quote_Product__c, Main_Quote_Product__r.Name__c, (Select ID, Name__c from Quote_Products__r) from Quote_Product_Bundle__c where Quote__c =:theQuote.ID];
        
        if(mainProductID == NULL){
            loadQuoteProducts();
        }
        quoteProductBundle = new Quote_Product_Bundle__c();
        if(mainProductID != NULL){
            theMainProduct = [select ID, Name__c, Type__c, Main_Bundled_Product__c, Y1_Bundled_Price__c, Y1_Total_with_Bundle__c, Y2_Total_with_Bundle__c, Y3_Total_with_Bundle__c, Y4_Total_with_Bundle__c, Y5_Total_with_Bundle__c, Quote__c from Quote_Product__c where ID=:mainProductID];
            theQuoteProductBundle = [select ID, Delete__c, Verification_Bundle__c, Year_2_Differance__c, Year_3_Differance__c from Quote_Product_Bundle__c where Quote__c =:theQuote.ID and Main_Quote_Product__c =:mainProductID ];
            theBundledProducts = [select ID, Name__c, Type__c, Y1_Total_Price__c, Y2_Total_Price__c, Y3_Total_Price__c, Y4_Total_Price__c, Y5_Total_Price__c, Total_Value__c, Quote__c from Quote_Product__c where Quote__c=:theQuote.Id and Quote_Product_Bundle__c=:theQuoteProductBundle.ID];
            loadQuoteSecondaryProducts();
            
        }
    }
    
    public void processBundle(){
        for(quoteProductWrapper qp : this.quoteProductList){
            if(qp.selected==true){
                qp.tQuoteProduct.Quote_Product_Bundle__c = theQuoteProductBundle.ID;
                qp.tQuoteProduct.Bundle__c = TRUE;
                qp.tQuoteProduct.Bundled_Product__c = theMainProduct.Id;
                update qp.tQuoteProduct;
                if(qp.tQuoteProduct.Name__c == 'Verification Services'){
                    verificationBundle();
                }
            }
        }
    }
    
    public void removeItemFromBundle(){
        bp = [select ID, Quote_Product_Bundle__c, Bundle__c, Bundled_Product__c from Quote_Product__c where ID=:dID];
        bp.Quote_Product_Bundle__c = NULL;
        bp.Bundled_Product__c = NULL;
        bp.Bundle__c = FALSE;
        update bp;
    }
    
    public void verificationBundle(){
        if(theQuoteProductBundle.ID != NULL){
            theQuoteProductBundle.Verification_Bundle__c = TRUE;
            update theQuoteProductBundle;
        }
    }
    
    public void markBundleDelete(){
        if(bundleID !=NULL){
            theQuoteProductBundle = [select ID from Quote_Product_Bundle__c where ID=:bundleID];
        }
        theQuoteProductBundle.Delete__c = TRUE;
        update theQuoteProductBundle;
    }
    
    public void updateCanadaOffset(){
        if(theQuoteProductBundle.Verification_Bundle__c == TRUE){
            theVerification = [select ID, Name__c, Y2_Canada_Bundle_Offset__c, Y3_Canada_Bundle_Offset__c
                               from Quote_Product__c where Quote__c=:theQuote.Id and 
                               Quote_Product_Bundle__c=:theQuoteProductBundle.ID and Name__c= 'Verification Services'];
            
            theVerification.Y2_Canada_Bundle_Offset__c = theQuoteProductBundle.Year_2_Differance__c;
            theVerification.Y3_Canada_Bundle_Offset__c = theQuoteProductBundle.Year_3_Differance__c;
            update theVerification;
        }
    }
    
    private void loadQuoteProducts() {
        for(Quote_Product__c qp : [select Id, Name__c, Type__c, Product__r.Has_Questions__c, Discount_Reason__c, Other_Discount_Reason__c, Quote_Product__c, 
                                   Y1_Total_Price__c, Y2_Total_Price__c, Y3_Total_Price__c, Y4_Total_Price__c, Y5_Total_Price__c, Total_Value__c, Approval_Status__c  
                                   from Quote_Product__c where Quote__c=:theQuote.Id and Bundle__c = FALSE and Main_Bundled_Product__c = FALSE AND Product__r.Parent_Product__c = NULL Order By Name__c]) {
                                       this.quoteProductList.add(new quoteProductWrapper(qp)); 
                                   }
    }
    
    public void loadQuoteSecondaryProducts() {
        //select 50 contacts for sample display
        for(Quote_Product__c qp : [select Id, Name__c, Type__c, Product__r.Has_Questions__c, Discount_Reason__c, Other_Discount_Reason__c, Quote_Product__c, Y1_Total_Price__c, 
                                   Y2_Total_Price__c, Y3_Total_Price__c, Y4_Total_Price__c, Y5_Total_Price__c, Total_Value__c, Approval_Status__c  
                                   from Quote_Product__c where Quote__c=:theQuote.Id and Bundle__c = FALSE and Main_Bundled_Product__c = FALSE AND Product__r.Parent_Product__c = NULL and ID!=:theMainProduct.ID Order By Name__c]) {
                                       this.quoteProductList.add( new quoteProductWrapper(qp) ); 
                                   }
    }
    
    // ryan edited this
    public PageReference bundleProducts() {
        processBundle();
         if (theQuoteProductBundle.Verification_Bundle__c == true) {
            updateCanadaOffset();
        }
        if (isOption) {
            return new PageReference('/apex/quoteProductEntryStep3?id='+theQuote.id).setRedirect(true);
        } else {
            return new PageReference('/apex/quoteProductEntryStep4?id='+theQuote.id).setRedirect(true);
        }
    }
    
    public PageReference removeFromBundle(){
        removeItemFromBundle();
        PageReference quoteBundle = Page.quoteProductBundle;
        quoteBundle.setRedirect(true);
        quoteBundle.getParameters().put('id',theQuote.id);
        quoteBundle.getParameters().put('mainProductID',theMainProduct.Id);  
        return quoteBundle; 
    }
    
    public PageReference mainProductSelection(){
        Id  qpMainId = ApexPages.currentPage().getParameters().get('qpMainId');
        createProductBundle();
        PageReference quoteBundle = Page.quoteProductBundle;
        quoteBundle.setRedirect(true);
        quoteBundle.getParameters().put('id',theQuote.id);
        quoteBundle.getParameters().put('mainProductID',qpMainId);  
        return quoteBundle; 
    }
    
    public PageReference editMainProductSelection(){
        Id  qpMainId = ApexPages.currentPage().getParameters().get('qpMainId');
        PageReference quoteBundle = Page.quoteProductBundle;
        quoteBundle.setRedirect(true);
        quoteBundle.getParameters().put('id',theQuote.id);
        quoteBundle.getParameters().put('mainProductID',qpMainId);  
        return quoteBundle; 
    }
    
    // ryan edited this
    public PageReference step4() {
        if (theQuoteProductBundle != null) {
            if (theQuoteProductBundle.Verification_Bundle__c == true) {
                updateCanadaOffset();
            }
        }
        if (isOption) {
            return new PageReference('/apex/quoteProductEntryStep3?id='+theQuote.Id).setRedirect(true);
        } else {
            return new PageReference('/apex/quoteProductEntryStep4?id='+theQuote.Id).setRedirect(true);
        }
    }
    
    public PageReference returnToQuote(){
        PageReference returnToQuote = Page.quoteProductEntryStep4;
        returnToQuote.setRedirect(true);
        returnToQuote.getParameters().put('id',theQuote.id); 
        return returnToQuote; 
    }
    
    
    public PageReference saveAddAnother(){
        processBundle();
        PageReference quoteProductBundle = Page.quoteProductBundle;
        quoteProductBundle.setRedirect(true);
        quoteProductBundle.getParameters().put('id',theQuote.id);
        return quoteProductBundle; 
    }
    
    public PageReference deleteBundle() {
        Id  bundleID = ApexPages.currentPage().getParameters().get('bundleID');
        markBundleDelete();
        PageReference quoteProductBundle = Page.quoteProductBundle;
        quoteProductBundle.setRedirect(true);
        quoteProductBundle.getParameters().put('id',theQuote.id);
        return quoteProductBundle; 
    }
    
    public void createProductBundle(){
        Id  qpMainId = ApexPages.currentPage().getParameters().get('qpMainId');
        if(qpMainID != NULL){
            quoteProductBundle.Main_Quote_Product__c = qpMainID;
            quoteProductBundle.Quote__c = theQuote.Id;
            insert quoteProductBundle;
            addMainBundleCheck();
        }
    }
    public void addMainBundleCheck(){
        Id  qpMainId = ApexPages.currentPage().getParameters().get('qpMainId');
        theMainProduct = [Select ID, Main_Bundled_Product__c from Quote_Product__c where ID=:qpMainId];
        if(theMainProduct.ID != NULL){	
            theMainProduct.Main_Bundled_Product__c = TRUE;
            update theMainProduct;
        }
    }
    /*
*	This wrapper class encapsulates a contact record.
*	An additional property 'selected' tracks whether 
*	the record has been selected.
*/
    public class quoteProductWrapper{
        
        //the contact record
        public Quote_Product__c tQuoteProduct{get;set;}
        
        //whether the record is selected
        public Boolean selected{get;set;}
        
        /*
*	Constructor
*	initializes the Contact reference
*/
        public quoteProductWrapper(Quote_Product__c qp){
            this.tQuoteProduct = qp;
            this.selected = false;
        }
        
    }
}