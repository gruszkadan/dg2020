trigger salesPerformanceRequestTrigger on Sales_Performance_Request__c 
    (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
        if (trigger.isBefore) {
            if (trigger.isInsert) {
                salesPerformanceRequestTriggerHandler.beforeInsert(trigger.old, trigger.new);
            }
            if (trigger.isUpdate) {
                salesPerformanceRequestTriggerHandler.beforeUpdate(trigger.old, trigger.new);
            }
        }
        if (trigger.isAfter) {
            if (trigger.isInsert) {
                salesPerformanceRequestTriggerHandler.afterInsert(trigger.old, trigger.new);
            }
            if (trigger.isUpdate) {
                salesPerformanceRequestTriggerHandler.afterUpdate(trigger.old, trigger.new);
            }
        }
        
    }