<apex:page controller="orderDetailController" showHeader="false" sidebar="false" standardStylesheets="false" >
    <c:salesPerformanceMaintenance rendered="{!$Setup.Sales_Performance_Settings__c.Maintenance_Mode__c}"/>
    <div style="{!if(!$Setup.Sales_Performance_Settings__c.Maintenance_Mode__c,'','display:none;')}">
        <apex:includeScript value="{!URLFOR($Resource.salesperformance_resources, 'jQuery/jquery-3.2.1.min.js')}"/>
        <style>
            .scroll::-webkit-scrollbar {
            width: 10px;
            }
            
            .scroll::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
            }
            
            .scroll::-webkit-scrollbar-thumb {
            background: #d8dde6; 
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
            }
        </style>
        <body style="background-color:#fafaf9;">
            <apex:form >
                <apex:slds />
                <div class="slds-scope">
                    <div class="slds-spinner_container slds-hide" id="uploadSpinner" style="position:fixed;">
                        <div class="slds-spinner slds-spinner_brand slds-spinner_medium" aria-hidden="false" role="alert">
                            <div class="slds-spinner__dot-a"></div>
                            <div class="slds-spinner__dot-b"></div>
                        </div>
                    </div>                
                    <div class="slds-page-header slds-m-around_x-small">
                        <div class="slds-grid">
                            <div class="slds-col slds-has-flexi-truncate">
                                <div class="slds-media slds-no-space slds-grow">
                                    <div class="slds-media__figure">
                                        <span class="slds-icon_container slds-icon-custom-custom93" title="Description of icon when needed">
                                            <svg class="slds-icon" aria-hidden="true">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/custom-sprite/svg/symbols.svg#custom93')}"></use>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="slds-media__body">
                                        <nav>
                                            <ol class="slds-breadcrumb slds-line-height_reset">
                                                <li class="slds-breadcrumb__item">
                                                    <span>Orders</span>
                                                </li>
                                            </ol>
                                        </nav>
                                        <h1 class="slds-page-header__title slds-m-right_small slds-align-middle slds-truncate" title="{!Order.Account__r.Name} - {!Order.Order_ID__c}">{!Order.Account__r.Name} - {!Order.Order_ID__c}</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="slds-col slds-no-flex slds-grid slds-align-top">
                                <apex:commandLink styleClass="slds-button slds-button_neutral" id="AdminTool" rendered="{!Order.Account__r.AID__c != '' }" onclick="adminTool()" value="View In Admin Tool"/>
                            </div>
                        </div>
                        <ul class="slds-grid slds-page-header__detail-row">
                            <li class="slds-page-header__detail-block">
                                <p class="slds-text-title slds-truncate slds-m-bottom_xx-small" title="Account">
                                    Account
                                </p>
                                <a href="/{!Order.Account__c}">{!Order.Account__r.Name}</a>
                            </li>
                            <li class="slds-page-header__detail-block">
                                <p class="slds-text-title slds-truncate slds-m-bottom_xx-small" title="Order Date">Order Date</p>
                                <p title="{!Order.Order_Date__c}">
                                    <apex:outputField value="{!Order.Order_Date__c}"/>
                                </p>
                            </li>
                            <li class="slds-page-header__detail-block">
                                <p class="slds-text-title slds-truncate slds-m-bottom_xx-small" title="Contract Length">Contract Length</p>
                                <div style="{!IF(Order.Contract_Length__c != null,'','display:none;')}">
                                    <p title="{!Order.Contract_Length__c} Year(s)">{!Order.Contract_Length__c} Year(s)</p>
                                </div>
                                <div style="{!IF(Order.Contract_Length__c == null,'','display:none;')}">
                                    <p title="{!Order.Contract_Length__c}">{!Order.Contract_Length__c}</p>
                                </div>
                            </li>
                            <li class="slds-page-header__detail-block">
                                <p class="slds-text-title slds-truncate slds-m-bottom_xx-small" title="Type">Type</p>
                                <p title="{!Order.Type__c}">{!Order.Type__c}</p>
                            </li>
                            <li class="slds-page-header__detail-block">
                                <p class="slds-text-title slds-truncate slds-m-bottom_xx-small" title="Total Bookings">Total Bookings</p>
                                <p title="${!Order.Booking_Amount__c}">${!Order.Booking_Amount__c}</p>
                            </li>
                            <li class="slds-page-header__detail-block">
                                <p class="slds-text-title slds-truncate slds-m-bottom_xx-small" title="Incentive Amount">Incentive Amount</p>
                                <p title="${!Order.Incentive_Amount__c}">${!Order.Incentive_Amount__c}</p>
                            </li>
                            <li class="slds-page-header__detail-block">
                                <p class="slds-text-title slds-truncate slds-m-bottom_xx-small" title="Owner">Owner</p>
                                <a href="/{!Order.OwnerID}">
                                    {!Order.Owner.Name}
                                </a>
                            </li>
                        </ul>
                    </div>    
                    <div class="slds-grid slds-m-around_x-small"> 
                        <div class="slds-col slds-p-right_x-small slds-size--8-of-12">
                            <div class="slds-tabs_card slds-tabs_default">
                                <ul class="slds-tabs_default__nav" role="tablist">
                                    <li class="slds-tabs_default__item slds-is-active" title="Order Items" role="presentation">
                                        <a class="slds-tabs_default__link" href="javascript:void(0);" role="tab" tabindex="0" aria-selected="true" aria-controls="tab-default-1" id="tab-default-1__item">
                                            Related
                                        </a>
                                    </li>
                                </ul>
                                <div id="tab-default-1" class="slds-tabs_default__content slds-show" role="tabpanel" aria-labelledby="tab-default-1__item">
                                    <article class="slds-card slds-card_boundary">
                                        <div class="slds-card__header slds-grid">
                                            <header class="slds-media slds-media_center slds-has-flexi-truncate">
                                                <div class="slds-media__figure">
                                                    <span class="slds-icon_container slds-icon-custom-custom20" title="description of icon when needed">
                                                        <svg class="slds-icon slds-icon_small" aria-hidden="true">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/standard-sprite/svg/symbols.svg#orders')}"></use>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div class="slds-media__body">
                                                    <h2>
                                                        <a href="javascript:void(0);" class="slds-card__header-link slds-truncate" title="[object Object]">
                                                            <span class="slds-text-heading_small">
                                                                Items ({!items.size})
                                                            </span>
                                                        </a>
                                                    </h2>
                                                </div>
                                            </header>
                                        </div>
                                        <div class="slds-card__body slds-card__body_inner">
                                            <table class="slds-table slds-table_bordered slds-table_cell-buffer">
                                                <thead>
                                                    <tr class="slds-text-title_caps">
                                                        <th scope="col">
                                                            <div class="slds-truncate" title="Product">
                                                                Product
                                                            </div>
                                                        </th>
                                                        <th scope="col">
                                                            <div class="slds-truncate" title="Booking Amount">
                                                                Booking Amount
                                                            </div>
                                                        </th>
                                                        <th scope="col">
                                                            <div class="slds-truncate" title="Incentive Amount">
                                                                Incentive Amount
                                                            </div>
                                                        </th>
                                                        <th scope="col">
                                                            <div class="slds-truncate" title="Status">
                                                                Status
                                                            </div>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <apex:repeat value="{!items}" var="i">
                                                        <tr>
                                                            <th scope="row" data-label="Product">
                                                                <div class="slds-truncate" title="{!i.Product_Name__c}">
                                                                    {!i.Product_Name__c}
                                                                </div>
                                                            </th>
                                                            <td data-label="Booking Amount">
                                                                <div class="slds-truncate" title="{!i.Booking_Amount__c}">
                                                                    {!i.Booking_Amount__c}
                                                                </div>
                                                            </td>
                                                            <td data-label="Incentive Amount">
                                                                <div class="slds-truncate" title="{!i.Total_Incentive_Amount__c}">
                                                                    <apex:actionStatus onstart="showSpinner()" onstop="itModal()" id="blockElement"/>
                                                                    <apex:commandLink styleClass="slds-m-left_medium" status="blockElement" value="{!i.Total_Incentive_Amount__c}" action="{!queryIncentives}" reRender="itHeader,incentiveData">
                                                                        <apex:param name="spItemID" value="{!i.id}"/>
                                                                    </apex:commandLink>
                                                                </div>
                                                            </td>
                                                            <td data-label="Status">
                                                                <div class="slds-truncate" title="{!i.Status__c}">
                                                                    {!i.Status__c}
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </apex:repeat>
                                                </tbody>
                                            </table>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                        <div class="slds-col">
                            <div class="slds-tabs_card slds-tabs_default">
                                <ul class="slds-tabs_default__nav" role="tablist">
                                    <li class="slds-tabs_default__item slds-is-active" title="Order Items" role="presentation">
                                        <a class="slds-tabs_default__link" href="javascript:void(0);" role="tab" tabindex="0" aria-selected="true" aria-controls="tab-default-1" id="tab-default-1__item">
                                            Actions
                                        </a>
                                    </li>
                                </ul>
                                <div id="tab-default-1" class="slds-grid slds-wrap slds-show">
                                    <div style="{!if(Order.Type__c != 'Booking','display:none','')}">
                                        <!--Split Order Card -->
                                        <div class="slds-col slds-size_1-of-1" style="{!IF(defaultView != 'Rep','','display:none;')}">
                                            <a class="slds-box slds-box_link slds-box_x-small slds-media slds-m-top_medium" target="_parent" onclick="parentSpinner()" href="/apex/sales_performance_split?spoID={!Order.ID}{!if(viewAs != null && viewAs != $User.Id,'&va='+viewAs,'')}">
                                                <div class="slds-media__figure slds-media__figure_fixed-width slds-align_absolute-center slds-m-left_xx-small">
                                                    <span class="slds-icon_container slds-icon-utility-groups" title="Split order">
                                                        <svg class="slds-icon slds-icon_medium slds-icon-text-default">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#groups')}" ></use>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div class="slds-media__body slds-border_left slds-p-around_small">
                                                    <h2 style="text-align: left;" class="slds-truncate slds-text-heading_small" title="Split Order">Split Order</h2>
                                                    <p style="text-align: left;" class="slds-m-top_small">Split bookings and/or incentive between two or more employees.</p>
                                                </div>
                                            </a>
                                        </div>
                                        <!--Adjust Dispute Card -->
                                        <div class="slds-col slds-size_1-of-1">
                                            <a class="slds-box slds-box_link slds-box_x-small slds-media slds-m-top_small" target="_parent" onclick="parentSpinner()" href="/apex/sales_performance_adjust_dispute?id={!Order.Id}{!if(viewAs != null && viewAs != $User.Id,'&va='+viewAs,'')}">
                                                <div class="slds-media__figure slds-media__figure_fixed-width slds-align_absolute-center slds-m-left_xx-small">
                                                    <span class="slds-icon_container slds-icon-utility-groups" title="{!IF(defaultView ='VP' || defaultView='Director','Adjust Incentive','Dispute Incentive')}">
                                                        <svg class="slds-icon slds-icon_medium slds-icon-text-default">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#change_record_type')}" ></use>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div class="slds-media__body slds-border_left slds-p-around_small" style="{!IF(defaultView ='VP' || defaultView='Director','','display:none;')}">
                                                    <h2 style="text-align: left;" class="slds-truncate slds-text-heading_small" title="Adjust Incentive">
                                                        Adjust Incentive
                                                    </h2>
                                                    <p style="text-align: left;" class="slds-m-top_small">Adjust the incentive for an existing booking for an employee.</p>
                                                </div>
                                                <div class="slds-media__body slds-border_left slds-p-around_small" style="{!IF(defaultView ='Rep' || defaultView='Manager','','display:none;')}">
                                                    <h2 style="text-align: left;" class="slds-truncate slds-text-heading_small" title="Dispute Incentive">
                                                        Dispute Incentive
                                                    </h2>
                                                    <p style="text-align: left;" class="slds-m-top_small">Request an adjustment to the incentive of an existing booking.</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--INCENTIVES MODAL -->
                    <section id="itModal" role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-modal="true" aria-describedby="modal-content-id-1" class="slds-modal">
                        <div class="slds-modal__container">
                            <header class="slds-modal__header">
                                <button type="button" class="close slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" title="Close">
                                    <svg class="slds-button__icon slds-button__icon_large" aria-hidden="true">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#close')}"></use>
                                    </svg>
                                    <span class="slds-assistive-text">Close</span>
                                </button>
                                <apex:outputPanel id="itHeader">
                                    <h2 id="modal-heading-01" class="slds-text-heading_medium slds-hyphenate">Incentive Transaction History</h2>
                                </apex:outputPanel>
                            </header>
                            <div class="slds-modal__content" id="modal-content-id-1">
                                <apex:dataTable id="incentiveData" value="{!it}" var="itt" styleClass="slds-table slds-table_bordered slds-no-row-hover" style="border-bottom:none;border-top:none;">
                                    <apex:column value="{!itt.Incentive_Amount__c}" headerValue="Incentive Amount" headerClass="slds-text-title_caps slds-cell-buffer_left" styleClass="slds-cell-buffer_left"> 
                                        
                                    </apex:column>
                                    <apex:column value="{!itt.Incentive_Date__c}" headerValue="Transaction Date" headerClass="slds-text-title_caps"/>
                                </apex:dataTable>
                            </div>
                            <footer class="slds-modal__footer">
                                <button type="button" class="close slds-button slds-button_neutral">Close</button>
                            </footer>
                        </div>
                    </section>
                    <div id="backdrop" class="slds-backdrop">
                    </div>
                </div>
            </apex:form>
        </body>
        <script type="text/javascript">
        $j = jQuery.noConflict();
        function showSpinner(){
            $j('#uploadSpinner').show();
        }
        function hideSpinner(){
            $j('#uploadSpinner').hide();
        }
        function parentSpinner(){
            window.top.showSpinner();
        }
        function itModal() {
            $j('#uploadSpinner').hide();
            $j('#itModal').addClass('slds-fade-in-open');
            $j('#backdrop').addClass('slds-backdrop_open');
        }
        $j('.close').click(function(){
            $j('#itModal').removeClass('slds-fade-in-open');
            $j('#backdrop').removeClass('slds-backdrop_open');
        });
        
        function adminTool() {
            myWindow=window.open('http://intranet.msdsonline.com/Cambridge/Hanover/Summary.aspx?AID={!Order.Account__r.AID__c}','','menubar=yes,titlebar=yes,toolbar=yes,scrollbars=yes,fullscreen=yes,location=no,width=1024,height=768');
            myWindow.focus();
        }
        </script>
    </div>
</apex:page>