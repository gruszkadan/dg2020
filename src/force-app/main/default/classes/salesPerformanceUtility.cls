public class salesPerformanceUtility {
    //Cacluate whether the previous month is finalized 
    public static Boolean previousMonthClosed() {
        date monthFirstDay = Date.Today().toStartofMonth();
        Holiday[] holidays = [SELECT ActivityDate FROM Holiday];
        Set<Date> holidaysSet = new Set<Date> ();
        for (Holiday currHoliday : holidays)
        {
            holidaysSet.add(currHoliday.ActivityDate);
        }
        Integer workingDays = 0;
        for (integer i = 0; i <= monthFirstDay.daysBetween(Date.Today()); i++)
        {
            Date dt = monthFirstDay + i;
            DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());
            String todayDay = currDate.format('EEEE');
            if (todayDay != 'Saturday' && todayDay != 'Sunday' && (!holidaysSet.contains(dt)))
            {
                workingDays = workingDays + 1;
            }
        }
        integer businessDay = workingDays;
        string defaultView = Sales_Performance_Settings__c.getInstance(userInfo.getUserId()).Default_View__c;
        Sales_Incentive_Setting__c[] sisList = [SELECT id FROM Sales_Incentive_Setting__c WHERE RecordType.Name = 'Incentive Finance Submission' AND Incentive_Date__c = LAST_MONTH];
        if(businessDay > 6){
            if(defaultView == 'VP'){
                if(sisList.size() > 0){
                    return true;
                }else{
                    return false;
                }
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
    
    //Get Listing of Sales Users
    public static List<SelectOption> getSalesUsers(string defaultView, string UserID, string UserName) {
        List<SelectOption> options = new List<SelectOption>();
        //Only Display the Reps Name if a Sales Rep is viewing the page
        if(defaultView == 'Rep'){
            options.add(new SelectOption(UserID, UserName)); 
        } else {
            options.add(new SelectOption('-None-', '--Select Sales Rep--'));
            //Get users      
            for (User usr : [Select Id, Name From User where isActive = TRUE and Department__c = 'Sales' and 
                             Group__c !='EHS Enterprise Sales' and Group__c !='EHS Mid-Market Sales'  
                             ORDER BY Name ASC]) {
                                 options.add(new SelectOption(usr.ID, usr.Name));
                             }
        }
        return options;
    }
    
    //Send Notification to all involved when SPR is approved or rejected
    public static void sendSprApprovedRejectedNotification (Sales_Performance_Request__c[] sprs){
        Sales_Performance_Request__c[] requests = [SELECT id,Name,Booking_Amount__c,Incentive_Amount__c,Current_Approver_Email__c,Status__c,Sales_Performance_Order__r.Account__r.Name,Owner.Name,Owner.Email,Reason__c,Type__c,JSON__c,
                                                   Sales_Performance_Order__r.Contract_Number__c,Sales_Rep__r.Name,Sales_Rep__r.Email,Assigned_Approver_1__r.Email,Assigned_Approver_2__r.Email, Assigned_Approver_3__r.Email, Assigned_Approver_4__r.Email,
                                                   Assigned_Approver_5__r.Email, Assigned_Approver_6__r.Email,Assigned_Approver_7__r.Email,Assigned_Approver_8__r.Email,Assigned_Approver_9__r.Email,Assigned_Approver_10__r.Email,
                                                   (SELECT id,Owner.Email FROM Sales_Performance_Item_Splits__r)
                                                   FROM Sales_Performance_Request__c
                                                   WHERE id in:sprs];
        for(Sales_Performance_Request__c spr:requests){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            set<String> sToAddresses = new set<String>();
            sToAddresses.add(spr.Assigned_Approver_1__r.Email);
            sToAddresses.add(spr.Assigned_Approver_2__r.Email);
            sToAddresses.add(spr.Assigned_Approver_3__r.Email);
            sToAddresses.add(spr.Assigned_Approver_4__r.Email);
            sToAddresses.add(spr.Assigned_Approver_5__r.Email);
            sToAddresses.add(spr.Assigned_Approver_6__r.Email);
            sToAddresses.add(spr.Assigned_Approver_7__r.Email);
            sToAddresses.add(spr.Assigned_Approver_8__r.Email);
            sToAddresses.add(spr.Assigned_Approver_9__r.Email);
            sToAddresses.add(spr.Assigned_Approver_10__r.Email);
            sToAddresses.add(spr.Sales_Rep__r.Email);
            sToAddresses.add(spr.Owner.Email);
            if(spr.Sales_Performance_Item_Splits__r.size() > 0){
                for(Sales_Performance_Item_Split__c split:spr.Sales_Performance_Item_Splits__r){
                    sToAddresses.add(split.Owner.Email);
                }
            }
            list<String> sToAddressesList = new list<String>();
            for(String s:sToAddresses){
                if(s != null){
                    sToAddressesList.add(s);
                }
            }
            mail.setToAddresses(sToAddressesList);
            mail.setSubject('Sales Performance Request '+spr.Status__c);
            mail.setHtmlBody(createSprEmailBody(spr));
            Messaging.sendEmail( new Messaging.SingleEmailMessage[]{mail});
        }
    }
    
    //Re-Usable method to create SPR approval process email body
    public static String createSprEmailBody(Sales_Performance_Request__c spr){
        string htmlBody ='<p style="font-size: 75%;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;"><b>Sales Performance Request '+spr.Status__c+'</b></br></br>'+
            'Request: '+spr.Name+'</br>'+
            'Request Type: '+spr.Type__c+'</br>';
        if(spr.Sales_Performance_Order__r.Account__r.Name != null){
            htmlBody += 'Account: '+spr.Sales_Performance_Order__r.Account__r.Name+'</br>';
        }
        if(spr.Sales_Performance_Order__r.Contract_Number__c != null){
            htmlBody += 'Contract #: '+spr.Sales_Performance_Order__r.Contract_Number__c+'</br>';
        }
        if(spr.Sales_Rep__r.Name != null){
            htmlBody += 'Sales Rep: '+spr.Sales_Rep__r.Name+'</br>';
        }
        htmlBody += 'Submitted By: '+spr.Owner.Name+'</br>'+
            'Reason: '+spr.Reason__c+'</br></br>';
        if(spr.Type__c == 'Split'||spr.Type__c == 'Adjustment/Dispute'||spr.Type__c == 'Automated Adjustment'||(spr.Booking_Amount__c != null || spr.Incentive_Amount__c != null)){
            htmlBody += '<u>Summary</u>:</br>'+
                '<table style="font-size: 75%;width: 100%;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;border-collapse: collapse;border-color: grey;border: 1px solid #dddbda;">';
            
            //If split then create columns and rows for splits
            if(spr.Type__c == 'Split'){
                Sales_Performance_Item_Split__c [] splits = [Select id, Product_Name__c, Split_Group__c, SPI_Split_From__c, Booking_Amount__c, Incentive_Amount__c, Owner.Name 
                                                             From Sales_Performance_Item_Split__c
                                                             WHERE Sales_Performance_Request__c =:spr.id
                                                             ORDER BY Split_Group__c ASC NULLS LAST];
                map<id,String> productMap = new map<id,String>();
                set<String> owners = new set<String>();
                map<String,Sales_Performance_Item_Split__c> ownerMap = new map<String,Sales_Performance_Item_Split__c>();
                if(splits.size() > 0){
                    for(Sales_Performance_Item_Split__c split:splits){
                        owners.add(split.Owner.Name);
                        ownerMap.put(split.Owner.Name+'|'+split.Product_Name__c+'|'+split.SPI_Split_From__c,split);
                        productMap.put(split.SPI_Split_From__c,split.Product_Name__c);
                    }
                    htmlBody += '<tr style="font-size: 0.75rem;line-height: 1.25;color: #706e6b;text-transform: uppercase;letter-spacing: 0.0625rem;">'+
                        '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Product</th>';
                    
                    for(String owner:owners){
                        htmlBody += '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">'+
                            owner+'</th>';
                    }
                    htmlBody += '</tr>';
                    for(id prod:productMap.keyset()){
                        htmlBody +='<tr>';
                        htmlBody += '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+
                            productMap.get(prod)+'</td>';
                        for(String owner:owners){
                            Sales_Performance_Item_Split__c ownerSplit = ownerMap.get(owner+'|'+productMap.get(prod)+'|'+prod);
                            htmlBody +='<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">$'+
                                ownerSplit.Booking_Amount__c.format()+'</td>';
                        }
                        htmlBody +='</tr>';
                    }
                }
            }
            
            //If adjustment/dispute then create columns and rows for adjustments
            else if(spr.Type__c == 'Adjustment/Dispute'){
                String[] adjustments = (List<String>)System.JSON.deserialize(spr.JSON__c, List<String>.class);
                set<id> spiIds = new set<id>();
                map<id,decimal> spiAmountMap = new map<id,decimal>();
                map<id,decimal> spiOriginalAmountMap = new map<id,decimal>();
                for(String s:adjustments){
                    String[] sList = s.split('\\|');
                    spiIds.add(sList[0]);
                    spiAmountMap.put(sList[0],decimal.valueof(sList[1]));
                    spiOriginalAmountMap.put(sList[0],decimal.valueof(sList[2]));
                }
                Sales_Performance_Item__c[] items = [SELECT id,Product_Name__c,Total_Incentive_Amount__c
                                                     FROM Sales_Performance_Item__c
                                                     WHERE id in:spiIds];
                if(items.size() > 0){
                    htmlBody += '<tr style="font-size: 0.75rem;line-height: 1.25;color: #706e6b;text-transform: uppercase;letter-spacing: 0.0625rem;">'+
                        '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Product</th>'+
                        '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Original Incentive</th>'+
                        '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">New Incentive</th>'+
                        '</tr>';
                    for(Sales_Performance_Item__c item:items){
                        htmlBody +='<tr>'+
                            '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+item.Product_Name__c+'</td>'+
                            '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">$'+spiOriginalAmountMap.get(item.id).format()+'</td>'+
                            '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">$'+spiAmountMap.get(item.id).format()+'</td>'+
                            '</tr>';
                    }
                }
            }
            
            //If adjustment/dispute then create columns and rows for adjustments
            else if(spr.Type__c == 'Automated Adjustment'){
                String[] adjustments = (List<String>)System.JSON.deserialize(spr.JSON__c, List<String>.class);
                set<id> spiIds = new set<id>();
                map<id,String> spiAmountMap = new map<id,String>();
                map<id,String> spiOriginalAmountMap = new map<id,String>();
                map<id,String> spiStatusMap = new map<id,String>();
                spiIds.add(adjustments[1]);
                spiAmountMap.put(adjustments[1],adjustments[4]);
                spiOriginalAmountMap.put(adjustments[1],adjustments[3]);
                spiStatusMap.put(adjustments[1],adjustments[5]);
                Sales_Performance_Item__c[] items = [SELECT id,Product_Name__c,Total_Incentive_Amount__c, Owner.Name
                                                     FROM Sales_Performance_Item__c
                                                     WHERE id in:spiIds];
                
                if(items.size() > 0){
                    htmlBody += '<tr style="font-size: 0.75rem;line-height: 1.25;color: #706e6b;text-transform: uppercase;letter-spacing: 0.0625rem;">'+
                        '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Product</th>'+
                        '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Rep</th>'+
                        '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Original Bookings</th>'+
                        '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">New Bookings</th>'+
                        '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">New Status</th>'+
                        '</tr>';
                    for(Sales_Performance_Item__c item:items){
                        htmlBody +='<tr>'+
                            '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+item.Product_Name__c+'</td>'+
                            '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+item.Owner.Name+'</td>'+
                            '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">$'+spiOriginalAmountMap.get(item.id)+'</td>'+
                            '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">$'+spiAmountMap.get(item.id)+'</td>'+
                            '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">'+spiStatusMap.get(item.id)+'</td>'+
                            '</tr>';
                    }
                }
            }
            
            //For any other type of SPR Create a table with fixed columns and rows
            else if(spr.Booking_Amount__c != null || spr.Incentive_Amount__c != null){
                htmlBody += '<tr style="font-size: 0.75rem;color: #706e6b;text-transform: uppercase;letter-spacing: 0.0625rem;">';
                if(spr.Booking_Amount__c != null){
                    htmlBody +='<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Booking Amount</th>';
                }
                if(spr.Incentive_Amount__c != null){
                    htmlBody += '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 0.25rem 0.5rem;font-weight: 400;white-space: nowrap;">Incentive Amount</th>';
                }
                htmlBody +='</tr>';
                htmlBody +='<tr>';
                if(spr.Booking_Amount__c != null){
                    htmlBody += '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">$'+spr.Booking_Amount__c.format()+'</td>';
                }
                if(spr.Incentive_Amount__c != null){
                    htmlBody += '<td style="border-top: 1px solid #dddbda;padding: 0.25rem 0.5rem;">$'+spr.Incentive_Amount__c.format()+'</td>';
                }
                htmlBody +='</tr>';  
            }
            htmlBody += '</table>';
        }
        htmlBody += '</br></br><center><a style="font-size: 75%;border-radius: 10%;border: 1px solid #dddbda;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;'+
            'border-color: #dddbda;background-color: white;padding: 12px;cursor: pointer;text-decoration: none;color: #0070d2;" '+
            'href="'+URL.getSalesforceBaseUrl().toExternalForm() + '/'+spr.id+'">Go To Request</a></center></p>';
        return htmlBody;
    }
    
    public static boolean canViewAs(){
        //Cannot view as by default
        boolean canViewAs = false;
        //Look for a view as id in the url parameters
        string viewAsId = ApexPages.currentPage().getParameters().get('va');
        //If found a view as id then check for security
        if(viewAsId != null && viewAsId != ''){
            if(Sales_Performance_Settings__c.getInstance().View_As_Enabled__c){
                canViewAs = true;
            }else if(Sales_Performance_Settings__c.getInstance().View_As_Multiple_Sales_Teams__c){
                User u = [SELECT id, EmployeeNumber FROM User WHERE id =:UserInfo.getUserId() LIMIT 1];
                User[] salesTeamUsers = [Select Id, Name From User where EmployeeNumber =:u.EmployeeNumber and id !=:u.id  
                                         ORDER BY Name ASC]; 
                for(User usr: salesTeamUsers){
                    if(usr.id == viewAsId){
                        canViewAs = true;  
                    }
                }
            } 
        }
        return canViewAs;
    }
}