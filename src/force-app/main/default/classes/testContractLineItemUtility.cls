@isTest(SeeAllData=true)
public class testContractLineItemUtility {

    public static testmethod void test1() {
		Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Quote_Promotion__c promo = new Quote_Promotion__c();
        promo.Name = 'Test Promo';
        promo.Start_Date__c = date.today();
        promo.End_Date__c = date.today().addDays(90);
        promo.Code__c = 'Test';
        insert promo;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Promotion__c = promo.id;
        insert newQuote;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Quote__c = newQuote.Id; 
        insert newOpportunity;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.First_Approver__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        newContract.Initial_Approval_Check__c = FALSE;
        newContract.Opportunity__c = newOpportunity.id;
        newContract.Approve_Sales__c = true;
        newContract.OwnerId = [Select id from User where Sales_Director__c != Null AND Managerid != NULL LIMIT 1].id;
        newContract.EHS_Off_The_Shelf_Modules__c = 'Air';
        newContract.EHS_Modules__c = 'Air';
        insert newContract; 
        
        System.debug(newContract.OwnerId );
        
        Contract_Line_Item__c cli = new Contract_Line_Item__c();
        cli.Contract__c = newContract.Id;
        cli.Product__c = [Select id from Product2 where name = 'Data Export' Limit 1].id;
        cli.Quantity__c = 100;
        cli.Product_Types__c = 'All Incident Management;All Audits & Inspections;All Management of Change';
        cli.Y1_List_Price__c = 70;
        cli.Sort_order__c = 1;
        cli.Year_1_Manual__c = 35;
        cli.Year_2_Manual__c = 0;
        cli.Year_3_Manual__c = 0;
        cli.Year_4_Manual__c = 0;
        cli.Year_5_Manual__c = 0;
        insert cli;
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Contract__c = newContract.Id;
        cli2.Product__c = [Select id from Product2 where name = 'Data Import' Limit 1].id;
        cli2.Quantity__c = 100;
        cli2.Product_Types__c = 'Custom';
        cli2.Product_Type_Custom__c = 'Testing Custom Product Type';
        cli2.Y1_List_Price__c = 50;
        cli2.Sort_order__c = 1;
        cli2.Year_1_Manual__c = 25;
        cli2.Year_2_Manual__c = 0;
        cli2.Year_3_Manual__c = 0;
        cli2.Year_4_Manual__c = 0;
        cli2.Year_5_Manual__c = 0;
        insert cli2;


        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Contract__c = newContract.Id;
        cli3.Product__c = [Select id from Product2 where name = 'Air' Limit 1].id;
        cli3.Quantity__c = 100;
        cli3.Y1_List_Price__c = 50;
        cli3.Sort_order__c = 1;
        cli3.Year_1_Manual__c = 25;
        cli3.Year_2_Manual__c = 25;
        cli3.Year_3_Manual__c = 25;
        cli3.Year_4_Manual__c = 0;
        cli3.Year_5_Manual__c = 0;
        cli3.Implementation_Model__c = 'Off the Shelf';
        cli3.Implementation_Model__c = 'Business';
        insert cli3;
        
  

        Contract_Line_Item__c[] clis = [Select id, Product_Types_Formatted__c from Contract_Line_Item__c where id = :cli.id OR id = :cli2.id OR id = :cli3.id];
        for(Contract_Line_Item__c c: clis){
           if(c.id == cli.id){
               System.assert('All Incident Management, All Audits & Inspections and All Management of Change' == c.Product_Types_Formatted__c);
            }
            if(c.id == cli2.id){
                 System.assert(cli2.Product_Type_Custom__c == c.Product_Types_Formatted__c);
            }
            if(c.id == cli3.id){
                 System.assert(c.Product_Types_Formatted__c == NULL);
            }
        }
        
        System.assert(newContract.EHS_Off_The_Shelf_Modules__c == 'Air');
        delete cli3;
        contract__c checkContract = [Select EHS_Modules__c, EHS_Off_The_Shelf_Modules__c from Contract__c where id = :newContract.id];
        System.assert(checkContract.EHS_Off_The_Shelf_Modules__c == NULL);

    }
}