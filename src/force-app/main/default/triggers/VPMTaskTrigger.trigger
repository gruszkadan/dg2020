trigger VPMTaskTrigger on VPM_Task__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
      if (trigger.isBefore) {
        
        // BEFORE INSERT
        if (trigger.isInsert) {
            VPMTaskTriggerHandler.beforeInsert(trigger.new);
        }
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            VPMTaskTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
        
        // BEFORE DELETE
        if (trigger.isDelete) {
            VPMTaskTriggerHandler.beforeDelete(trigger.old, trigger.new);
        }
        
    }
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            VPMTaskTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            VPMTaskTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
        // AFTER DELETE
        if (trigger.isDelete) {
            VPMTaskTriggerHandler.afterDelete(trigger.old);
        }
        
    }


}