@isTest()
private class testOrderPendingCancellationController {
    
    
    static testMethod void test1(){
        //Create all data for test
        //1 Account + 1 Contact + 2 AMEs + 2 Opportunitys  
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.AdminID__c = '11111';
        insert testAccount;
        
        Contact testContact = new contact();
        testContact.FirstName = 'Bob';
        testContact.LastName = 'Weir';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Account_Management_Event__c ame = new Account_Management_Event__c();
        ame.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        ame.Account__c = testAccount.Id;
        ame.Contact__c = testContact.Id;
        ame.OwnerId = [Select Id, Name From User where isActive = TRUE and UserRole.Name LIKE '%Retention%' and firstname = 'sarah' limit 1].id;
        ame.Status__c = 'Active';
        ame.Cancellation_Reason__c = 'Bankruptcy';
        ame.New_System__c = 'Other';
        ame.Sent_to_Orders_Date__c = date.today();  
        ame.Sent_to_Orders_By__c = [Select Id, Name From User where isActive = TRUE and UserRole.Name LIKE '%Retention%' and firstname = 'sarah' limit 1].id;
        ame.CreatedById = ame.Sent_to_Orders_By__c;
        insert ame;
        
        Account_Management_Event__c relatedAME = new Account_Management_Event__c();
        
        relatedAME.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();  
        relatedAME.Account__c = testAccount.Id;
        relatedAME.Contact__c = testContact.Id;
        relatedAME.OwnerId = [Select Id from Group where Type = 'Queue' and DeveloperNAME = 'Order_Cancellation' limit 1].id;
        relatedAME.Status__c = 'Active';
        relatedAME.Related_AME__c = ame.id;
        relatedAME.Cancellation_Reason__c = 'Unknown';
        relatedAME.New_System__c = 'Going Without';
        relatedAME.Sent_to_Orders_Date__c = date.today();  
        relatedAME.Sent_to_Orders_By__c = ame.Sent_to_Orders_By__c;
        relatedAME.CreatedById = ame.Sent_to_Orders_By__c;
        insert relatedAME;
        
        Opportunity ameOpp = new opportunity();
        ameOpp.Name = 'Opp for ame';
        ameOpp.AccountId = testAccount.Id;
        ameOpp.StageName = 'New';
        ameOpp.CloseDate = date.today();
        ameopp.CreatedById = ame.Sent_to_Orders_By__c;
        ameOpp.Account_Management_Event__c = ame.Id;
        insert ameOpp;
        
        Opportunity relatedOpp = new opportunity();
        relatedOpp.Name = 'Opp for Relatedame';
        relatedOpp.AccountId = testAccount.Id;
        relatedOpp.StageName = 'System Demo';
        relatedOpp.CloseDate = date.today();
        relatedOpp.CreatedById = ame.Sent_to_Orders_By__c;
        relatedOpp.Account_Management_Event__c = relatedAME.Id;
        insert RelatedOpp;
        
        // Pass in controller used in page 
        orderPendingCancellationController myController = new orderPendingCancellationController(); 
        // Select AME
        mycontroller.selectAll = true;
        // Check if AME was selected 
        system.assert(mycontroller.selectAll, true);
        
        mycontroller.CheckboxSelectAll();
        
        mycontroller.ProcessCancelation();
        
        //set date on fields after process cancellation method runs 
        mycontroller.ameWrappers[0].ame.Cancellation_Processed_Date__c = date.today();
        // check if date was set correctly 
        system.assertEquals(date.today(), mycontroller.amewrappers[0].ame.Cancellation_Processed_Date__c);
                
        mycontroller.ProcessCancelation();
        

        relatedame = [select id, name, status__c, OwnerId, Cancellation_Processed_By__c, Sent_to_Orders_By__c from Account_Management_Event__c where id =: relatedAME.id limit 1];
        ame = [select id, name, status__c, OwnerId, Cancellation_Processed_By__c, Sent_to_Orders_By__c from Account_Management_Event__c where id =: ame.id limit 1]; 
        ameOpp = [select id, stagename, reason_lost__c, closedate from opportunity where id =: ameOpp.id];
        RelatedOpp = [select id, stagename, reason_lost__c, closedate from opportunity where id =: ameOpp.id];
        
        //Check to see if the fields were set on the cancellation (related) ame 
        system.assertEquals(relatedame.OwnerId, relatedame.Sent_to_Orders_By__c);
        system.assertEquals('Closed', relatedame.Status__c);
        system.assertEquals(userinfo.getUserId(), relatedame.Cancellation_Processed_By__c);
        
        //check to see if the ame status was set to closed 
        system.assertequals('Closed', ame.Status__c);
        
        //check to see if the fields were set on the opportunity related to ame  
        system.assertEquals('Closed Lost', ameOpp.StageName);
        system.assertEquals('Cancel', ameOpp.Reason_Lost__c);
        system.assertEquals(date.today(), ameOpp.CloseDate);
       
        //check to see if the fields were set on the opportunity related to the related ame  
        system.assertEquals('Closed Lost', RelatedOpp.StageName);
        system.assertEquals('Cancel', relatedopp.Reason_Lost__c);
        system.assertEquals(date.today(), relatedopp.CloseDate);
        
        
        
    }
    
}