public class TerritorySearchController {
    public String countyInput{get;set;}
    public String zipInput{get;set;}
    public String stateInput{get;set;}
    
    public String noResultsMessage { get; set; }
    public List <County_Zip_Code__c> zips {get;set;}
    
    public TerritorySearchController() { 
        noResultsMessage = 'No records to display';
    }
    
    public void find(){
        if(stateInput !='Select State' && countyInput !=NULL){	
            countyInput = String.escapeSingleQuotes(countyInput);
            stateInput = String.escapeSingleQuotes(stateInput);
            noResultsMessage = NULL;
            zips = [SELECT City__c, County__c, State__c, Zip_Code__c, Territory__r.Online_Training_Owner__c, Territory__r.Name, Territory__r.Authoring_Owner__c, Territory__r.Territory_Owner__c, Territory__r.Territory_Owner_Email__c, Territory__r.Territory_Owner_Phone__c, Territory__c from County_Zip_Code__c WHERE County__c LIKE :countyInput and State__c = :stateInput limit 1];
        }
        if(zipInput != NULL && stateInput =='Select State'){
            zipInput = String.escapeSingleQuotes(zipInput);
            zipInput = zipInput.trim();
            noResultsMessage = NULL;
            zips = [SELECT City__c, County__c, State__c, Zip_Code__c, Territory__r.Online_Training_Owner__c, Territory__r.Name, Territory__r.Authoring_Owner__c, Territory__r.Territory_Owner__c, Territory__r.Territory_Owner_Email__c, Territory__r.Territory_Owner_Phone__c, Territory__c from County_Zip_Code__c WHERE Zip_Code__c LIKE :zipInput limit 1];
        }
        countyInput = NULL;
        zipInput = NULL;
        stateInput = 'Select State';        	
    }
    
    public List<SelectOption> getStateOptions(){ 
        List<SelectOption> options = new List<SelectOption>();      
        options.add(new SelectOption('Select State', 'Select State',false)); 
        // Reading picklist values and labels
        Schema.DescribeFieldResult fieldResult = County_Zip_Code__c.State__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        // Adding apicklist values to the select list
        for(Schema.PicklistEntry entry : picklistEntries){
            
            options.add(new SelectOption(entry.getValue(), entry.getLabel())); 
        }
        return options;
    }
}