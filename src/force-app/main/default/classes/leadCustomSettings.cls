public with sharing class leadCustomSettings {
    
    public boolean canChangeAuthoringOwner() {
        return Lead_Custom_Settings__c.getInstance().Can_Change_Authoring_Owner__c;
    }
    
    public boolean canChangeChemicalMgtOwner() {
        return Lead_Custom_Settings__c.getInstance().Can_Change_Chemical_Mgt_Owner__c;
    }
    
    public boolean canChangeEHSOwner() {
        return Lead_Custom_Settings__c.getInstance().Can_Change_EHS_Owner__c;
    }
    
    public boolean canChangeOnlineTrainingOwner() {
        return Lead_Custom_Settings__c.getInstance().Can_Change_Online_Training_Owner__c;
    }
    
    public boolean canChangeErgonomicsOwner() {
        return Lead_Custom_Settings__c.getInstance().Can_Change_Ergonomics_Owner__c;
    }
    
    public boolean canChangeSecondaryLeadOwner() {
        return Lead_Custom_Settings__c.getInstance().Can_Change_Secondary_Lead_Owner__c;
    }
        
   public boolean CanEditErgonomicsSecondaryLeadOwner() {
       return  Lead_Custom_Settings__c.getInstance().Can_Edit_Ergonomics_Secondary_Lead_Owner__c;
    }
        
    public static boolean canEditMSDSSuite() {
        return POI_Security__c.getInstance().Can_Edit_MSDS_Suite__c;
    }
    
    public static boolean canEditEHSSuite() {
        return POI_Security__c.getInstance().Can_Edit_EHS_Suite__c;
    }
    
    public static boolean canEditAUTHSuite() {
        return POI_Security__c.getInstance().Can_Edit_AUTH_Suite__c;
    }
    
    public static boolean canEditODTSuite() {
        return POI_Security__c.getInstance().Can_Edit_ODT_Suite__c;
    }
    
    public static boolean canEditERGOSuite() {
        return POI_Security__c.getInstance().Can_Edit_ERGO_Suite__c;
    }

    
    public defaultOwners findDefaultOwners() {
        defaultOwners d = new defaultOwners();
        return d;
    }
    
    public class defaultOwners {
        public id defChemMgtOwner;
        public id defAuthOwner;
        public id defErgoOwner;
        public id defOTOwner;
        public id defEHSOwner;
        public id defInternationalOwner;
        public id entEHSOwner;
        public id specialEHSOwner;
        public string allIds;
        public id defESOwner;
        public defaultOwners() {
            allIds = '';
            for (Lead_Custom_Settings__c lcs : [SELECT SetupOwnerId, Default_Authoring_Owner__c, Default_Ergo_Owner__c, Default_Online_Training_Owner__c, Default_EHS_Owner__c, Default_Chemical_Management_Owner__c, Default_International_Lead_Owner__c,
                                                Special_Country_EHS_Owner__c, Enterprise_EHS_Owner__c, Default_ES_Owner__c
                                                FROM Lead_Custom_Settings__c
                                                WHERE Default_Chemical_Management_Owner__c = true
                                                OR Default_Authoring_Owner__c = true
                                                OR Default_Ergo_Owner__c = true
                                                OR Default_Online_Training_Owner__c = true
                                                OR Default_EHS_Owner__c = true
                                                OR Special_Country_EHS_Owner__c = true
                                                OR Enterprise_EHS_Owner__c = true
                                                OR Default_ES_Owner__c = true
                                                OR Default_International_Lead_Owner__c = true]) { 
                                                    if (lcs.Default_Chemical_Management_Owner__c) {
                                                        defChemMgtOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Default_Authoring_Owner__c) {
                                                        defAuthOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Default_Ergo_Owner__c) {
                                                        defErgoOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Default_Online_Training_Owner__c) {
                                                        defOTOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Default_EHS_Owner__c) {
                                                        defEHSOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Enterprise_EHS_Owner__c) {
                                                        entEHSOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Special_Country_EHS_Owner__c) {
                                                        specialEHSOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if (lcs.Default_International_Lead_Owner__c) {
                                                        defInternationalOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    if(lcs.Default_ES_Owner__c){
                                                        defESOwner = lcs.SetupOwnerId;
                                                        allIds += lcs.SetupOwnerId+';';
                                                    }
                                                    
                                                }
            
            
            
            
        }
    }
    
}