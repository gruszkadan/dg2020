public class salesPerformanceRequestController {
    public String o {get;set;}
    public String viewAs {get;set;}
    public String viewAsDefaultView {get;set;}
    public string userID {get;set;}
    public string userName {get;set;}
    public User u {get;set;}
    public String defaultView {get;set;}
    public String spoID {get;set;}
    public Sales_Performance_Request__c[] allRequests {get;set;}
    public Sales_Performance_Request__c[] requests {get;set;}
    public Sales_Performance_Request__c[] pendingApprovalRequests {get;set;}
    public string theQuery {get;set;}
    public boolean pendingFilter {get;set;}
    public boolean approvedFilter {get;set;}
    public boolean rejectedFilter {get;set;}
    public Integer paginationSize {get; set;}
    public Integer listNumber {get;set;}
    List<List<Sales_Performance_Request__c>> listofListRequests;
    
    public salesPerformanceRequestController() {
        
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias, FirstName, LastName, Name, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID 
             FROM User 
             WHERE id = :viewAs LIMIT 1];
        userID = u.id;
        userName = u.Name;
        if (defaultView == 'VP' && viewAs == userInfo.getUserId()) {
            u = [SELECT id, Alias, FirstName, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID
                 FROM User
                 WHERE UserRole.Name = 'VP Sales' AND isActive = true and LastName != 'Haling' LIMIT 1];
        }
        
        //get defaultView to display when viewing as
        defaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
        
        if(defaultView !='Rep'){
            pendingApprovalRequests = [SELECT ID, Name, Type__c, CreatedBy.Name, CreatedDate, CreatedBy.FullPhotoURL
                                       FROM Sales_Performance_Request__c 
                                       WHERE Current_Approver__c =: userName and Status__c = 'Pending Approval'
                                       ORDER BY CreatedDate DESC ];
        }
        pendingFilter = TRUE;
        approvedFilter = FALSE;
        rejectedFilter = FALSE;
        paginationSize = 10;
        queryRequests();
    }
    
    
    
    public void queryRequests(){
        integer i = 0;
        listNumber = 0;
        string statusFilter;
        if(pendingFilter == TRUE){
            if(statusFilter == NULL){
                statusFilter ='\'Pending Approval\'';
            } else {
                statusFilter +=',\'Pending Approval\'';
            }
        }
        if(approvedFilter == TRUE){
            if(statusFilter == NULL){
                statusFilter ='\'Approved\'';
            } else {
                statusFilter +=',\'Approved\'';
            }
        }
        if(rejectedFilter == TRUE){
            if(statusFilter == NULL){
                statusFilter ='\'Rejected\'';
            } else {
                statusFilter +=',\'Rejected\'';
            }
        }
        
        theQuery = 'SELECT ID, Name, Type__c, Status__c, Current_Approver__c, Current_Approver_Lookup__r.FullPhotoURL '; 
        theQuery+= 'FROM Sales_Performance_Request__c ';
        if(defaultView != 'Director' && defaultView != 'VP'){
            theQuery+= 'WHERE OwnerID = \''+userID+'\' ';
        }else{
            theQuery+= 'WHERE (OwnerID = \''+userID+'\' OR (Type__c = \'Automated Adjustment\' AND Assigned_Approver_1__c = \''+userId+'\')) ';
        }
        if(statusFilter != NULL){
            theQuery+= 'AND Status__c IN ('+statusFilter+') ';
        }
        theQuery+= 'AND Status__c != \'New\' ';
        theQuery+= 'ORDER BY CreatedDate DESC';
        allRequests = database.query(theQuery);
        listofListRequests = new List<List<Sales_Performance_Request__c>>();
        for(Sales_Performance_Request__c sr : allRequests){
            if(math.mod(i, paginationSize) == 0){
                List<Sales_Performance_Request__c> tempList = new List<Sales_Performance_Request__c>();
                tempList.add(sr);
                listofListRequests.add(tempList);
            }else{
                listofListRequests.get(listofListRequests.Size() - 1).add(sr);
            }
            i++;
            
        }
        if(allRequests.size()>0){
            requests = listofListRequests.get(listNumber);
        } else {
            requests = allRequests;
        }
    }
    
    public void next(){
        if(listofListRequests.Size()-1 > listNumber)
            requests = listofListRequests.get(++listNumber);        
    }
    
    public void previous(){
        if(0 < listNumber)
            requests = listofListRequests.get(--listNumber);        
    }
    
    public boolean gethasPrevious(){
        if(listNumber > 0)
            return true;    
        return false;
    }
    
    public boolean gethasNext(){
        if(listNumber < listofListRequests.Size() -1)
            return true;
        return false;
    }
    
    //Get List of List Size Options
    public List<SelectOption> getListSizeOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('10', '10'));
        options.add(new SelectOption('20', '20'));
        options.add(new SelectOption('30', '30'));
        return options;
    }
}