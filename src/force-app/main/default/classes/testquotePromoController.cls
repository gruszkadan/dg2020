@isTest(seeAllData=true)
private class testquotePromoController {
    
    //for new promos
    private static testmethod void test1() {
        //insert a promo
        Quote_Promotion__c p1 = new Quote_Promotion__c(Name = 'p1', Start_Date__c = date.today(), End_Date__c = date.today().addDays(10), Code__c = 'p1', Fees_Language_Changes__c = true, Fees_Language_Changes_Text__c = 'test text');
        p1.Name = 'p1';
        p1.Start_Date__c = date.today();
        p1.End_Date__c = date.today().addDays(10);
        p1.Code__c = 'p1';
        p1.Fees_Language_Changes__c = true;
        p1.Fees_Language_Changes_Text__c = 'p1 text';
        insert p1;
        
        //hq account
        Quote_Promotion_Product__c qpp1 = new Quote_Promotion_Product__c();
        qpp1.Quote_Promotion__c = p1.id;
        qpp1.Name__c = 'HQ Account';
        qpp1.Product__c = '01t80000002NON4';
        qpp1.Discount__c = 1;
        qpp1.Discount_Method__c = 'Amount Off';
        qpp1.Is_Main_Bundled_Product__c = true;
        qpp1.Year_1__c = true;
        insert qpp1;
        
        //hq imp fee
        Quote_Promotion_Product__c qpp2 = new Quote_Promotion_Product__c();
        qpp2.Quote_Promotion__c = p1.id;
        qpp2.Name__c = 'HQ Implementation Fee';
        qpp2.Product__c = '01t80000003Q4sL';
        qpp2.Discount__c = 1;
        qpp2.Discount_Method__c = 'Amount Off';
        qpp2.Bundled_Into__c = qpp1.id;
        qpp2.Year_1__c = true;
        insert qpp2;
        
        //scan pdf
        Quote_Promotion_Product__c qpp3 = new Quote_Promotion_Product__c();
        qpp3.Quote_Promotion__c = p1.id;
        qpp3.Name__c = 'Scan/PDF Process';
        qpp3.Product__c = '01t80000002NOOM';
        qpp3.Discount__c = 1;
        qpp3.Discount_Method__c = 'Amount Off';
        qpp3.Is_Main_Group_Product__c = true;
        qpp3.Year_1__c = true;
        insert qpp3;
        
        //indexing
        Quote_Promotion_Product__c qpp4 = new Quote_Promotion_Product__c();
        qpp4.Quote_Promotion__c = p1.id;
        qpp4.Name__c = 'Indexing Field - First Aid';
        qpp4.Product__c = '01t80000003kKvz';
        qpp4.Discount__c = 1;
        qpp4.Discount_Method__c = 'Amount Off';
        qpp4.Group__c = qpp3.Product__c;
        qpp4.Year_1__c = true;
        insert qpp4;
        
        //init controller and pass the new promo to the standard controller
        ApexPages.currentPage().getParameters().put('id', p1.id);
        quotePromoController con = new quotePromoController(new ApexPages.StandardController(p1));
        
        con.getsubmitMsg();
        
        ApexPages.currentPage().getParameters().put('cloneId', p1.id);
        con.findClonedPromo();
        con.clonePromo();
        
        for (integer i=0; i<con.products.size(); i++) {
            if (con.products[i].entry.Product2.Name == 'eBinder Valet') {
                con.selProd = con.products[i];
                con.selProd.promoProd.Subscription_Model__c = 'Business';
                con.selProd.promoProd.Implementation_Model__c = 'Simple';
            }
        }
        con.findNewPricebook();
        
        ApexPages.currentPage().getParameters().put('prodOpId', '01t80000003Q4rL');
        ApexPages.currentPage().getParameters().put('opType', 'option');
        con.findProductOptions();
        
        for (integer i=0; i<con.products.size(); i++) {
            string n = con.products[i].entry.Product2.Name;
            if (n == 'GM Account' || n == 'eBinder Valet' || n == 'HQ Account Package') {
                con.products[i].selected = true;
            }
        }
        con.productOptions[0].selected = true;
        con.saveSelectedOptions();
        con.addSelectedProducts();
        
        ApexPages.currentPage().getParameters().put('prodOpId', '01t340000041hq0');
        ApexPages.currentPage().getParameters().put('opType', 'option');
        for (integer i=0; i<con.products.size(); i++) {
            if (con.products[i].entry.Product2.Name == 'EH&S Management') {
                con.selProd = con.products[i];
                con.selProd.promoProd.Subscription_Model__c = 'Business';
                con.selProd.promoProd.Implementation_Model__c = 'Simple';
            }
        }
        con.findProductOptions();
        for (integer i=0; i<con.productOptions.size(); i++) {
            if (con.productOptions[i].promoProd.Name__c == 'Audit & Inspection') {
                con.productOptions[i].selected = true;
            }
        }
        con.saveSelectedOptions();
        con.addSelectedProducts();
        for (integer i=0; i<con.selectedProducts.size(); i++) {
            con.selectedProducts[i].promoProd.Discount__c = 1;
            con.selectedProducts[i].promoProd.Discount_Method__c = 'Percent Off';
            con.selectedProducts[i].promoProd.Year_1__c = true;
        }
        con.step2_saveAndContinue();
        
        con.editPromo();
        con.cancelPromo();
        for (integer i=0; i<con.selectedProducts.size(); i++) {
            con.selectedProducts[i].selected = true;
        }
        con.delSelectedProducts();
        
        ApexPages.currentPage().getParameters().remove('id');
        con = new quotePromoController(new ApexPages.StandardController(new Quote_Promotion__c()));
        con.saveAddProducts();
        con.save();
        con.cancel();
        
        ApexPages.currentPage().getParameters().put('id', p1.id);
        con = new quotePromoController(new ApexPages.StandardController(p1));
        con.submitPromo();
        con.recallPromo();
        
        ApexPages.currentPage().getParameters().put('id', p1.id);
        con = new quotePromoController(new ApexPages.StandardController(p1));
        con.submitPromo();
        con.reject();
        
        ApexPages.currentPage().getParameters().put('id', p1.id);
        con = new quotePromoController(new ApexPages.StandardController(p1));
        con.submitPromo();
        con.approve();
        con.finalReview_back();
        con.getconfirmApp();
        con.getrejectApp();
        
        p1.Fees_Language_Changes__c = false;
        update p1;
        ApexPages.currentPage().getParameters().put('id', p1.id);
        con = new quotePromoController(new ApexPages.StandardController(p1));
        con.getsubmitMsg();
        
        p1.Start_Date__c = date.today().addDays(30);
        update p1;
        ApexPages.currentPage().getParameters().put('id', p1.id);
        con = new quotePromoController(new ApexPages.StandardController(p1));
        con.getsubmitMsg();
    }
    
    
    //for new promos
    private static testmethod void test2() {
        //insert a promo
        Quote_Promotion__c p1 = new Quote_Promotion__c(Name = 'p1', Start_Date__c = date.today(), End_Date__c = date.today().addDays(10), Code__c = 'p1', Fees_Language_Changes__c = true, Fees_Language_Changes_Text__c = 'test text');
        p1.Name = 'p1';
        p1.Start_Date__c = date.today();
        p1.End_Date__c = date.today().addDays(10);
        p1.Code__c = 'p1';
        p1.Fees_Language_Changes__c = true;
        p1.Fees_Language_Changes_Text__c = 'p1 text';
        insert p1;
        
        //hq account
        Quote_Promotion_Product__c qpp1 = new Quote_Promotion_Product__c();
        qpp1.Quote_Promotion__c = p1.id;
        qpp1.Name__c = 'HQ Account';
        qpp1.Product__c = '01t80000002NON4';
        qpp1.Discount__c = 1;
        qpp1.Discount_Method__c = 'Amount Off';
        qpp1.Is_Main_Bundled_Product__c = true;
        insert qpp1;
        
        //hq imp fee
        Quote_Promotion_Product__c qpp2 = new Quote_Promotion_Product__c();
        qpp2.Quote_Promotion__c = p1.id;
        qpp2.Name__c = 'HQ Implementation Fee';
        qpp2.Product__c = '01t80000003Q4sL';
        qpp2.Discount__c = 1;
        qpp2.Discount_Method__c = 'Amount Off';
        qpp2.Bundled_Into__c = qpp1.id;
        insert qpp2;
        
        //scan pdf
        Quote_Promotion_Product__c qpp3 = new Quote_Promotion_Product__c();
        qpp3.Quote_Promotion__c = p1.id;
        qpp3.Name__c = 'Scan/PDF Process';
        qpp3.Product__c = '01t80000002NOOM';
        qpp3.Discount__c = 1;
        qpp3.Discount_Method__c = 'Amount Off';
        qpp3.Is_Main_Group_Product__c = true;
        insert qpp3;
        
        //indexing
        Quote_Promotion_Product__c qpp4 = new Quote_Promotion_Product__c();
        qpp4.Quote_Promotion__c = p1.id;
        qpp4.Name__c = 'Indexing Field - First Aid';
        qpp4.Product__c = '01t80000003kKvz';
        qpp4.Discount__c = 1;
        qpp4.Discount_Method__c = 'Amount Off';
        qpp4.Group__c = qpp3.Product__c;
        insert qpp4;
        
        ApexPages.currentPage().getParameters().put('cloneId', p1.id);
        quotePromoController con = new quotePromoController(new ApexPages.StandardController(new Quote_Promotion__c()));
        con.saveAddProducts();
        con.backNewPromo();
        con.cancelNewPromo();
        
        ApexPages.currentPage().getParameters().put('id', p1.id);
        con = new quotePromoController(new ApexPages.StandardController(new Quote_Promotion__c()));
        
        ApexPages.currentPage().getParameters().put('prodId', qpp1.id);
        con.createBundle();
        qpp2.Bundled_Into__c = qpp1.id;
        con.saveBundle();
        con.editBundle();
        ApexPages.currentPage().getParameters().put('prodId', qpp2.id);
        con.removeFromBundle();
        con.deleteBundle();
        
    }
    
}