@isTest
private class testSalesPerformanceSetup {
    private static testmethod void test1() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            User rep1 = util.createUser('rep1', 'rep', vp, dir, mgr);
            insert rep1;
            
            User rep2 = util.createUser('rep2', 'rep', vp, dir, mgr);
            insert rep2;
            
            User assoc = util.createUser('TestAs', 'assoc', vp, dir, mgr);
            insert assoc;
            
            User assoc1 = util.createUser('TeAs1', 'assoc', vp, dir, mgr);
            insert assoc1;
            
            User assoc2 = util.createUser('TeAs2', 'assoc', vp, dir, mgr);
            insert assoc2;
            
            User assoc3 = util.createUser('TeAs3', 'assoc', vp, dir, mgr);
            insert assoc3;
            
            rep.Group__c = 'Enterprise Sales';
            update rep;
            rep1.Group__c = 'Enterprise Sales';
            update rep1;
            rep2.Group__c = 'Enterprise Sales';
            update rep2;
            assoc.Group__c = 'Enterprise Sales';
            update assoc;
            assoc1.Group__c = 'Enterprise Sales';
            update assoc1;
            assoc2.Group__c = 'Enterprise Sales';
            update assoc2;
            assoc3.Group__c = 'Enterprise Sales';
            update assoc3;
            
            test.startTest();
            
            Sales_Incentive_Setting__c si = util.createIncentiveSettings('Enterprise Associate', rep.id, assoc.id);
            insert si;
            Sales_Incentive_Setting__c si1 = util.createIncentiveSettings('Enterprise Associate', rep1.id, assoc2.id);
            insert si1;
            Sales_Incentive_Setting__c si2 = util.createIncentiveSettings('Enterprise Associate', rep2.id, assoc2.id);
            insert si2;
            Sales_Incentive_Setting__c si3 = util.createIncentiveSettings('Enterprise Associate', rep2.id, assoc3.id);
            
            PageReference pageRef = Page.sales_performance_es_associate_settings;
            test.setCurrentPage(pageRef);
            salesPerformanceSetup con = new salesPerformanceSetup();
            system.assertEquals('Enterprise Sales', con.groupName);
            system.assertEquals(3, con.allSettings.size());
            
            //Insert New Setting
            con.allSettings.add(util.createIncentiveSettings('Enterprise Associate', rep2.id, assoc3.id));
            con.saveChanges();
            
            //Query to get All Settings
            Sales_Incentive_Setting__c[] allSettings = [SELECT ID, Associate__c, Sales_Representative__c, Percent_Of_Bookings__c, Licensing_Goal_Kicker__c 
                                                        FROM Sales_Incentive_Setting__c 
                                                        WHERE RecordType.Name='Enterprise Associate'];
            system.assertEquals(4, allsettings.size());
            test.stopTest();
        }
    }
    
    private static testmethod void test2() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            User rep1 = util.createUser('rep1', 'rep', vp, dir, mgr);
            insert rep1;
            
            User assoc = util.createUser('TestAs', 'assoc', vp, dir, mgr);
            insert assoc;
            
            User assoc1 = util.createUser('TeAs1', 'assoc', vp, dir, mgr);
            insert assoc1;
            
            rep.Group__c = 'Mid-Market';
            update rep;
            rep1.Group__c = 'Mid-Market';
            update rep1;
            assoc.Group__c = 'Mid-Market';
            update assoc;
            assoc1.Group__c = 'Mid-Market';
            update assoc1;
            
            test.startTest();
            
            Sales_Incentive_Setting__c si = util.createIncentiveSettings('Mid-Market Associate', rep.id, assoc.id);
            insert si;
            Sales_Incentive_Setting__c si1 = util.createIncentiveSettings('Mid-Market Associate', rep1.id, assoc1.id);
            insert si1;
            
            PageReference pageRef = Page.sales_performance_mm_associate_settings;
            test.setCurrentPage(pageRef);
            salesPerformanceSetup con = new salesPerformanceSetup();
            con.getAssociates();
            system.assertEquals('Mid-Market', con.groupName);
            
            //Delete Setting
            con.deleteSettings.add(si);
            con.saveChanges();
            
            //Query to get All Settings
            Sales_Incentive_Setting__c[] allSettings = [SELECT ID, Associate__c, Sales_Representative__c, Percent_Of_Bookings__c, Licensing_Goal_Kicker__c 
                                                        FROM Sales_Incentive_Setting__c 
                                                        WHERE RecordType.Name='Mid-Market Associate'];
            system.assertEquals(1, allsettings.size());
            test.stopTest();
        }
    }
    
        private static testmethod void test3() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            User rep1 = util.createUser('rep1', 'rep', vp, dir, mgr);
            insert rep1;
            
            User assoc = util.createUser('TestAs', 'assoc', vp, dir, mgr);
            insert assoc;
            
            User assoc1 = util.createUser('TeAs1', 'assoc', vp, dir, mgr);
            insert assoc1;
            
            rep.Group__c = 'Mid-Market';
            update rep;
            rep1.Group__c = 'Mid-Market';
            update rep1;
            assoc.Group__c = 'Mid-Market';
            update assoc;
            assoc1.Group__c = 'Mid-Market';
            update assoc1;
            
            test.startTest();
            
            PageReference pageRef = Page.sales_performance_mm_associate_settings;
            test.setCurrentPage(pageRef);
            salesPerformanceSetup con = new salesPerformanceSetup();
            con.getReps();
            //Add Blank Setting
            con.addSetting();
            system.assertEquals(1, con.getSettings().size());
            //Insert Setting with Missing Information
            con.allSettings.add(util.createIncentiveSettings('Mid-Market Associate', rep1.id, NULL));
            con.saveChanges();
            system.assertEquals(FALSE, con.isSuccess); 

            test.stopTest();
        }
    }
    
       private static testmethod void test4() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User assoc = util.createUser('TestAs', 'assoc', vp, dir, mgr);
            insert assoc;
            
            User assoc1 = util.createUser('TeAs1', 'assoc', vp, dir, mgr);
            insert assoc1;
            
            assoc.Group__c = 'Mid-Market';
            update assoc;
            assoc1.Group__c = 'Mid-Market';
            update assoc1;
            
            test.startTest();
            
            PageReference pageRef = Page.sales_performance_mm_opportunity_rates;
            test.setCurrentPage(pageRef);
            salesPerformanceSetup con = new salesPerformanceSetup();
            con.getReps();
            con.allSettings.add(util.createOpportunityIncentiveSettings(assoc.id));
            con.allSettings.add(util.createOpportunityIncentiveSettings(assoc1.id));
            con.saveChanges();
            system.assertEquals(true, con.isSuccess); 

            test.stopTest();
        }
    }
    
}