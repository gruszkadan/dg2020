public class sfUsage 
{
    public User u {get;set;}
    public String[] appList  {get;set;}
    public SF_Usage__c[] uses {get;set;}
    public String selApp {get;set;}
    public String[] fieldList {get;set;}
    public String soql {get;set;}
    public String useTotals {get;set;}
    public String[] varList {get;set;}
    public Map<String,Integer> map1 {get;set;}
    public UseWrap[] wrappers {get;set;}
    
    public sfUsage()
    {
        wrappers = new List<UseWrap>();
        selApp = 'AccountLocator';
        u = [ SELECT Id, Name, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, Role__c, ProfileID, Profile.Name 
             FROM User 
             WHERE Id =: UserInfo.getUserId() LIMIT 1 ];
        
        appList = [ SELECT Id, Project_List__c
                   FROM SF_Usage__c 
                   WHERE Name = 'MASTER' LIMIT 1 ].Project_List__c.split( ';', 0 );
        
        map1 = new Map<String,Integer>();
        varList = new List<String>();
        soql = 'SELECT Name, ';
        
        for( Schema.SObjectField val : Schema.SObjectType.SF_Usage__c.fields.getMap().values() )
        {
            if( String.valueOf( val ).StartsWithIgnoreCase( selApp ) == TRUE )
            {
                soql += val+', ';
                if( String.valueOf( val.getDescribe().getType() ) == 'DOUBLE' )
                {
                    varList.add( String.valueOf( val ) );
                }
            }
        }
        
        soql += 'Id FROM SF_Usage__c WHERE Name != \'MASTER\'';
    }
    
    public List<SelectOption> getApps()
    {
        List<SelectOption> o = new List<SelectOption>();
        o.add( new SelectOption( 'All', '- All -' ) );
        for( String app : appList )
        {
            o.add( new SelectOption( app, app ) );
        }
        return o;
    }
    
    public void find()
    {
        wrappers.clear();
        String soql2 = 'SELECT Id, Name, ';
        soql2 += selApp+'_Views__c, ';
        soql2 += selApp+'_Searches__c, ';
        soql2 += selApp+'_Last_Use__c ';
        soql2 += 'FROM SF_Usage__c WHERE Name != \'MASTER\'';
        uses = Database.query( soql2 );
        for( SF_Usage__c use : uses )
        {
            Integer iViews = Integer.valueOf( use.get( selApp+'_Views__c' ) );
            Integer iSearches = Integer.valueOf( use.get( selApp+'_Searches__c' ) );
            Date iDate = Date.valueOf( use.get( selApp+'_Last_Use__c' ) );
            useWrap newWrap = new useWrap( use.Name, iViews, iSearches, iDate );
            wrappers.add( newWrap );
        }
    }
    
    public void addView()
    {
        sfUsage_add add = new sfUsage_add();
        add.addView( selApp );
    }
    
     public void addSearch()
    {
        sfUsage_add add = new sfUsage_add();
        add.addSearch( selApp );
    }

    
    public Class useWrap
    {
        public String wrapName {get;set;}
        public Integer wrapViews {get;set;}
        public Integer wrapSearches {get;set;}
        public Date wrapLastUse {get;set;}
        
        public useWrap( String userName, Integer views, Integer searches, Date lastUse )
        {
            wrapName = userName;
            wrapViews = views;
            wrapSearches = searches;
            wrapLastUse = lastUse;
        }
    }
    
}