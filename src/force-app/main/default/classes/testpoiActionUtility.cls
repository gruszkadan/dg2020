@isTest
private class testpoiActionUtility {
    
    static testmethod void test_createSFPOIActions() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        //c.LDR_Contact__c = true;
        insert c;
        id referrerId = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        Account_Referral__c[] refs = new list<Account_Referral__c>();
        for (integer i=0; i<5; i++) {
            Account_Referral__c newRef = new Account_Referral__c();
            newRef.Account__c = a.id;
            newRef.Contact__c = c.id;
            newRef.Referrer__c = referrerId;
            if (i==0) {newRef.Type__c = 'Chemical Management';}
            if (i==1) {newRef.Type__c = 'EHS';}
            if (i==2) {newRef.Type__c = 'Authoring';}
            if (i==3) {newRef.Type__c = 'ODT';}
            if (i==4) {newRef.Type__c = 'Ergonomics';}
            refs.add(newRef);
        }
         
        test.startTest();
        //insert refs;
        //a.LDR_Status__c = 'Done - Completed';
        //update a;
        test.stopTest();
    }
    
    static testmethod void test_checkPOIActionQueues() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        c.Marketo_ID__c = 'mkto1';
        //Create a queue record
        Product_Of_Interest_Action_Queue__c q = new Product_Of_Interest_Action_Queue__c();
        q.Product__c = 'HQ';
        q.Action__c = 'Request Demo';
        q.Action_ID__c = 'test1';
        q.Product_Suite__c = 'MSDS Management';
        q.List_ID__c = '18133';
        q.Marketo_ID__c = 'mkto1';
        insert q;
        
        test.startTest();
        insert c;
        test.stopTest();
    }
    
    static testmethod void test_convertQueueToAction() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        c.Marketo_ID__c = 'mkto1';
        Product_Of_Interest_Action_Queue__c[] qs = new list<Product_Of_Interest_Action_Queue__c>();
        for (integer i=0; i<5; i++) {
            Product_Of_Interest_Action_Queue__c q = new Product_Of_Interest_Action_Queue__c();
            q.Action__c = 'Request Demo';
            q.Marketo_ID__c = 'mkto1';
            if (i == 0) {
                q.Product__c = 'HQ';
                q.Action_ID__c = 'msds1';
                q.Product_Suite__c = 'MSDS Management';
                q.List_ID__c = '18133';
            } else if (i == 1) {
                q.Product__c = 'Air Emissions';
                q.Action_ID__c = 'ehs1';
                q.Product_Suite__c = 'EHS Management';
                q.List_ID__c = '18012';
            } else if (i == 2) {
                q.Product__c = 'Compliance Review';
                q.Action_ID__c = 'auth1';
                q.Product_Suite__c = 'MSDS Authoring';
                q.List_ID__c = '18226';
            } else if (i == 3) {
                q.Product__c = 'Environmental Toolkit';
                q.Action_ID__c = 'odt1';
                q.Product_Suite__c = 'On-Demand Training';
                q.List_ID__c = '18273';
            } else if (i == 4) {
                q.Product__c = 'Ergonomics';
                q.Action_ID__c = 'ergo1';
                q.Product_Suite__c = 'Ergonomics';
                q.List_ID__c = '18202';
            }
            qs.add(q);
        }
        insert qs;
        
        test.startTest();
        insert c;
        test.stopTest();

    }
    
    static testmethod void test_findCreatePois() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        insert c;
        Product_Of_Interest__c poi = new Product_Of_Interest__c(Contact__c=c.id);
        insert poi;
        test.startTest();
        poiActionUtility.findCreatePois(new list<sObject>{c}, 'Contact');
        test.stopTest();
    }
    
    static testmethod void test_backupPoiActions() {
        Product_Of_Interest__c poi = new Product_Of_Interest__c();
        insert poi;
        Product_Of_Interest_Action__c poia = new Product_Of_Interest_Action__c();
        poia.Product_Of_Interest__c = poi.id;
        poia.Action_ID__c = 'poia1';
        poia.Product__c = 'HQ';
        poia.Action__c = 'Request Demo';
        poia.Product_Suite__c = 'MSDS Management';
        insert poia;
        map<id, Product_Of_Interest_Action__c[]> map1 = new map<id, Product_Of_Interest_Action__c[]>();
        map1.put(poi.id, new list<Product_Of_Interest_Action__c>{poia});
        test.startTest();
        poiActionUtility.backupPoiActions(map1);
        test.stopTest();
    }
    
    static testmethod void test_updatePoiaOwnership() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        insert c;
        Product_Of_Interest__c poi = new Product_Of_Interest__c(Contact__c = c.id);
        insert poi;
        Product_Of_Interest_Action__c poia = new Product_Of_Interest_Action__c();
        poia.Contact__c = c.id;
        poia.Product_Of_Interest__c = poi.id;
        poia.Action_ID__c = 'poia1';
        poia.Product__c = 'HQ';
        poia.Action__c = 'Request Demo';
        poia.Product_Suite__c = 'MSDS Management';
        insert poia;
        
        test.startTest();
        a.OwnerId = [SELECT id FROM User WHERE isActive = true AND Role__c = 'Sales Executive' AND Group__c = 'Mid-Market' LIMIT 1].id;
        update a; 
        poiActionUtility.mergePoiaOwnership(new set<id>{a.id});
        test.stopTest();
    }
    
    public static Account testAccount() {
        return new Account(Name='Test', NAICS_Code__c='Test');
    }
    
    public static Contact testContact(id accountId) {
        return new Contact(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', AccountId=accountId);
    }
    
   
}