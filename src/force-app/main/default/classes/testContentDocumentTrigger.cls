@isTest(SeeAllData = true)
private class testContentDocumentTrigger {
    private static testmethod void runTest(){

        ContentVersion contentVersionObj = new ContentVersion();
        contentVersionObj.ContentURL='http://www.google.com/';
        contentVersionObj.Title = 'Google.com'; 
        contentVersionObj.Document_Owner__c = 'Person';
        contentVersionObj.Document_Owner_Email__c = 'testemail@testingvehs.com';
        contentVersionObj.Creation_Date__c = date.today();
        insert contentVersionObj;
        
        
        //find the id of the content document that was created
        id contentDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE id =:contentVersionObj.id].ContentDocumentId;
        
        //find the id of the library to use initially
        id contentLibraryId = [SELECT id FROM ContentWorkspace WHERE Name = 'Proposal Standard Content'].id;
        
        //Create the junction that publishes the version to the library
        ContentWorkspaceDoc link = new ContentWorkspaceDoc();
        link.ContentDocumentId = contentDocId;
        link.ContentWorkspaceId = contentLibraryId;
        insert link;
        
        //find the id of a library to change it to
        id contentMarketingPageLibraryId = [SELECT id FROM ContentWorkspace WHERE Name = 'Proposal Marketing Content'].id;
        //Update the library on the ContentDocument         
        ContentDocument document = [SELECT id,ParentId FROM ContentDocument WHERE id =:contentDocId];
        document.ParentId = contentMarketingPageLibraryId;
        update document;
        //Query salesforce request that the mothod created 
        Salesforce_Request__c[] SFR = new list<Salesforce_Request__c>();
        SFR = [Select id, name, Summary__c, Description__c, Requested_By__r.name, Owner.name from Salesforce_Request__c where Summary__c = 'Create Content Document Image' limit 1];
        
        //Check to see if the summary requested by and owner fields were set corretley 
        system.assertEquals('Create Content Document Image', SFR[0].Summary__c);        
        system.assertEquals('Mark McCauley', SFR[0].Requested_By__r.name);        
        system.assertEquals('SF Request Queue', SFR[0].Owner.name);        


    }
}