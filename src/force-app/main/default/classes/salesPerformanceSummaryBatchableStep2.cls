/*
* This will handle the changing of quotas, roles, and teams
* Test class: testSalesPerformanceSummaryScheduler
*/ 
global class salesPerformanceSummaryBatchableStep2 implements Database.Batchable<SObject>, Database.Stateful {
    
    integer yearOffset = 0;
    string stringYear = string.valueOf((date.today().year()) + yearOffset);
    integer monthNum = date.today().month();
    integer numRepsUpdated = 0;
    string repsUpdated = '';
    integer numOI = 0;
    integer numSPI = 0;
    integer numSPO = 0;
    integer numSPS = 0;
    string htmlBody = '';
    boolean updateRep;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (!test.isRunningTest()) {
            return Database.getQueryLocator([SELECT id, Name, Role__c, Sales_Performance_Manager_ID__c, Sales_VP__c, Sales_Director__c, Group__c, 
                                             SIC_Group__c
                                             FROM User
                                             WHERE ForecastEnabled = true
                                             ORDER BY Role_Order__c ASC]);
        } else {
            return Database.getQueryLocator([SELECT id, Name, Role__c, Sales_Performance_Manager_ID__c, Sales_VP__c, Sales_Director__c, Group__c,
                                             SIC_Group__c
                                             FROM User
                                             WHERE ForecastEnabled = true
                                             AND Alias = 'TestREP' LIMIT 1]);
        }
    }
    
    global void execute(Database.BatchableContext bc, list<sObject> batch) {
        User rep = (User)batch[0];   
        updateRep = false;
        orderItem(rep);
        salesPerformanceItem(rep);
        salesPerformanceOrder(rep);
        salesPerformanceSummary(rep);
        if (updateRep) {
            repsUpdated += rep.Name+', ';
        }
    }
    
    // ORDER ITEMS - team info
    global void orderItem(User rep) {
        Order_Item__c[] OIsUpd = new list<Order_Item__c>();
        Order_Item__c[] OIs = [SELECT id, Sales_VP__c, Sales_Director__c, Sales_Manager__c
                               FROM Order_Item__c
                               WHERE OwnerId = : rep.id
                               AND Year__c = : stringYear
                               AND Month_Number__c = : monthNum];
        for (Order_Item__c oi : OIs) {
            if (oi.Sales_VP__c != rep.Sales_VP__c || oi.Sales_Director__c != rep.Sales_Director__c || oi.Sales_Manager__c != rep.Sales_Performance_Manager_ID__c) {
                oi.Sales_VP__c = rep.Sales_VP__c;
                oi.Sales_Director__c = rep.Sales_Director__c;
                oi.Sales_Manager__c = rep.Sales_Performance_Manager_ID__c;
                OIsUpd.add(oi);
            }
        }
        if (OIsUpd.size() > 0) {
            update OIsUpd;
            numOI += OIsUpd.size();
            numRepsUpdated++;
            updateRep = true;
        } 
    }
    
    // SALES PERFORMANCE ITEMS - team info, group, role
    global void salesPerformanceItem(User rep) {
        Sales_Performance_Item__c[] SPIsUpd = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] SPIs = [SELECT id, Sales_VP__c, Sales_Director__c, Sales_Manager__c, Role__c, Group__c, Manager_Sales_Performance_Summary__c,
                                            Director_Sales_Performance_Summary__c, VP_Sales_Performance_Summary__c
                                            FROM Sales_Performance_Item__c
                                            WHERE OwnerId = : rep.id
                                            AND Year__c = : stringYear
                                            AND Month_Number__c = : monthNum];
        Sales_Performance_Summary__c[] mgrSPSs = [SELECT id, Month_Number__c, OwnerId
                                                  FROM Sales_Performance_Summary__c
                                                  WHERE (OwnerId = : rep.Sales_Performance_Manager_ID__c 
                                                         OR OwnerId = : rep.Sales_Director__c 
                                                         OR OwnerId = : rep.Sales_VP__c)
                                                  AND Year__c = : stringYear
                                                  AND Month_Number__c = : monthNum];
        for (Sales_Performance_Item__c spi : SPIs) {
            boolean needsUpd = false;
            if (spi.Group__c != rep.Group__c) {
                spi.Group__c = rep.Group__c;
                needsUpd = true;
            } 
            if (spi.Role__c != rep.Role__c) {
                spi.Role__c = rep.Role__c;
                needsUpd = true;
            }
            if (spi.Sales_VP__c != rep.Sales_VP__c || spi.Sales_Director__c != rep.Sales_Director__c || spi.Sales_Manager__c != rep.Sales_Performance_Manager_ID__c) {
                spi.Sales_VP__c = rep.Sales_VP__c;
                spi.Sales_Director__c = rep.Sales_Director__c;
                spi.Sales_Manager__c = rep.Sales_Performance_Manager_ID__c;
                if (mgrSPSs.size() > 0) {
                    for (Sales_Performance_Summary__c mgrSps : mgrSPSs) {
                        if (mgrSps.OwnerId == rep.Sales_Performance_Manager_ID__c) {
                            spi.Manager_Sales_Performance_Summary__c = mgrSps.id;
                        } else if (mgrSps.OwnerId == rep.Sales_Director__c) {
                            spi.Director_Sales_Performance_Summary__c = mgrSps.id;
                        } else if (mgrSps.OwnerId == rep.Sales_VP__c) {
                            spi.VP_Sales_Performance_Summary__c = mgrSps.id;
                        }
                    }
                }
                needsUpd = true;
            }
            if (needsUpd) {
                SPIsUpd.add(spi);
            }
        }
        if (SPIsUpd.size() > 0) {
            update SPIsUpd;
            numSPI += SPIsUpd.size();
            updateRep = true;
        } 
    }
    
    // SALES PERFORMANCE ORDERS - team info, group, role
    global void salesPerformanceOrder(User rep) {
        Sales_Performance_Order__c[] SPOsUpd = new list<Sales_Performance_Order__c>();
        Sales_Performance_Order__c[] SPOs = [SELECT id, Group__c, Role__c, Sales_VP__c, Sales_Director__c, Sales_Manager__c
                                             FROM Sales_Performance_Order__c
                                             WHERE OwnerId = : rep.id
                                             AND Year__c = : stringYear
                                             AND Month_Number__c = : monthNum];
        for (Sales_Performance_Order__c spo : SPOs) {
            boolean needsUpd = false;
            if (spo.Group__c != rep.Group__c) {
                spo.Group__c = rep.Group__c;
                needsUpd = true;
            } 
            if (spo.Role__c != rep.Role__c) {
                spo.Role__c = rep.Role__c;
                needsUpd = true;
            }
            if (spo.Sales_VP__c != rep.Sales_VP__c || spo.Sales_Director__c != rep.Sales_Director__c || spo.Sales_Manager__c != rep.Sales_Performance_Manager_ID__c) {
                spo.Sales_VP__c = rep.Sales_VP__c;
                spo.Sales_Director__c = rep.Sales_Director__c;
                spo.Sales_Manager__c = rep.Sales_Performance_Manager_ID__c;
                needsUpd = true;
            }
            if (needsUpd) {
                SPOsUpd.add(spo);
            }
        }
        if (SPOsUpd.size() > 0) {
            update SPOsUpd;
            numSPO += SPOsUpd.size();
            updateRep = true;
        } 
    }
    
    // SALES PERFORMANCE SUMMARIES - team info, group, role, quota
    global void salesPerformanceSummary(User rep) {
        Sales_Performance_Summary__c[] SPSs = [SELECT id, OwnerId, Group__c, Role__c, Manager__c, Manager_Sales_Performance_Summary__c, Quota__c, Sales_Director__c, Sales_Team_Manager__c, 
                                               SIC_Group__c, Sales_VP__c, YTD_Quota__c, Month_Number__c
                                               FROM Sales_Performance_Summary__c
                                               WHERE OwnerId = : rep.id
                                               AND Year__c = : stringYear
                                               AND Month_Number__c >= : monthNum
                                               ORDER BY Month_Number__c ASC];
        Sales_Performance_Summary__c[] mgrSPSs = [SELECT id, OwnerId, Month_Number__c, Year__c, SIC_Group__c
                                                  FROM Sales_Performance_Summary__c
                                                  WHERE OwnerId = : rep.Sales_Performance_Manager_ID__c
                                                  AND Year__c = : stringYear
                                                  AND Month_Number__c >= : monthNum
                                                  ORDER BY Month_Number__c ASC];
        ForecastingQuota[] quotas = [SELECT id, QuotaOwnerId, StartDate, QuotaAmount
                                     FROM ForecastingQuota
                                     WHERE QuotaOwnerId = : rep.id
                                     AND CALENDAR_YEAR(StartDate) = : integer.valueOf(stringYear)
                                     AND CALENDAR_MONTH(StartDate) >= : monthNum
                                     ORDER BY StartDate ASC];
        boolean needsQuotaUpdate = false;
        boolean needsUpd = false;
        for (Sales_Performance_Summary__c sps : SPSs) {
            if (sps.Group__c != rep.Group__c) {
                sps.Group__c = rep.Group__c;
                needsUpd = true;
            } 
            if (sps.Role__c != rep.Role__c) {
                sps.Role__c = rep.Role__c;
                needsUpd = true;
            }
            if(sps.SIC_Group__c != integer.valueOf(rep.SIC_Group__c)){
                sps.SIC_Group__c = integer.valueOf(rep.SIC_Group__c);
                needsUpd = true;
            }
            if (sps.Sales_VP__c != rep.Sales_VP__c || sps.Sales_Director__c != rep.Sales_Director__c || sps.Manager__c != rep.Sales_Performance_Manager_ID__c) {
                sps.Sales_VP__c = rep.Sales_VP__c;
                sps.Sales_Director__c = rep.Sales_Director__c;
                sps.Manager__c = rep.Sales_Performance_Manager_ID__c;
                if (mgrSPSs.size() > 0) {
                    for (Sales_Performance_Summary__c mgrSps : mgrSPSs) {
                        if (mgrSps.OwnerId == sps.Manager__c) {
                            if (mgrSps.Month_Number__c == sps.Month_Number__c) {
                                sps.Manager_Sales_Performance_Summary__c = mgrSps.id;
                            }
                        }
                    }
                }
                sps.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
                needsUpd = true;
            }
            for (ForecastingQuota q : quotas) {
                if (q.QuotaOwnerId == sps.OwnerId) {
                    if (q.StartDate.month() == sps.Month_Number__c) {
                        if (q.QuotaAmount != sps.Quota__c) {
                            sps.Quota__c = q.QuotaAmount;
                            needsUpd = true;
                            needsQuotaUpdate = true;
                        }
                    }
                } 
            }
        }
        if (needsQuotaUpdate) {
            decimal ytdQuota = 0.00;
            for (Sales_Performance_Summary__c sps : SPSs) {
                ytdQuota = ytdQuota + sps.Quota__c;
                sps.YTD_Quota__c = ytdQuota;
            }
        }
        if (needsUpd) {
            update SPSs;
            numSPS += SPSs.size();
            updateRep = true;
        } 
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
    
    
    
}