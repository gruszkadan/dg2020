@isTest(seeAllData=true)
private class testSurveys {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test_surveyTrigger() {
        Account acct = new Account(Name = 'Test', Customer_Status__c = 'Active', AdminID__c = 'test123');
        insert acct;
        
        Contact con = new Contact(FirstName = 'Bob', LastName = 'Test', AccountId = acct.id, CustomerAdministratorId__c = 'test123');
        insert con;
        
        test.startTest();
        Survey_Feedback__c surv = new Survey_Feedback__c(Name = 'Test1', Account__c = acct.id, Contact__c = con.id);
        insert surv;
        
        surv.Name = 'Test2';
        update surv;
        test.stopTest();
    } 
    
    static testmethod void test1a() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'IMP: HQ';
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        update newCase;
    }
    static testmethod void test2a() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'CC Support';
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        Case_Issue__c ci = new Case_Issue__c();
        ci.Associated_Case__c = newCase.id;
        ci.Product_In_Use__c = 'HQ';
        ci.Product_Support_Issue__c = 'Test issue';
        ci.Product_Support_Issue_Locations__c = 'Test location';
        insert ci;
        
        
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        newCase.Category__c = 'Service Request';
        newCase.Priority__c = '2';
        update newCase;
    }
    static testmethod void test3a() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'Compliance Services';
        newCase.Project_Status__c ='Delivered';
        newCase.Standalone__c = true;
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        newCase.Project_Work_Submitted__c = 'Yes';
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        update newCase;
    }
    static testmethod void test1b() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'IMP: HQ';
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        update newCase;
    }
    static testmethod void test2b() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'CC Support';
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        Case_Issue__c ci = new Case_Issue__c();
        ci.Associated_Case__c = newCase.id;
        ci.Product_In_Use__c = 'HQ';
        ci.Product_Support_Issue__c = 'Test issue';
        ci.Product_Support_Issue_Locations__c = 'Test location';
        insert ci;
        
        
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        newCase.Category__c = 'Service Request';
        newCase.Priority__c = '2';
        update newCase;
    }
    static testmethod void test3b() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'Compliance Services';
        newCase.Project_Status__c ='Delivered';
        newCase.Standalone__c = true;
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        newCase.Project_Work_Submitted__c = 'Yes';
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        update newCase;
    }
    static testmethod void test1c() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'IMP: HQ';
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        update newCase;
    }
    static testmethod void test2c() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'CC Support';
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        Case_Issue__c ci = new Case_Issue__c();
        ci.Associated_Case__c = newCase.id;
        ci.Product_In_Use__c = 'HQ';
        ci.Product_Support_Issue__c = 'Test issue';
        ci.Product_Support_Issue_Locations__c = 'Test location';
        insert ci;
        
        
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        newCase.Category__c = 'Service Request';
        newCase.Priority__c = '2';
        update newCase;
    }
    static testmethod void test3c() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'Compliance Services';
        newCase.Project_Status__c ='Delivered';
        newCase.Standalone__c = true;
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        newCase.Project_Work_Submitted__c = 'Yes';
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        update newCase;
    }
    static testmethod void test1d() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.Opt_In_Surveys__c = false;
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'IMP: HQ';
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        update newCase;
    }
    static testmethod void test2d() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Opt_In_Surveys__c = false;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'CC Support';
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        Case_Issue__c ci = new Case_Issue__c();
        ci.Associated_Case__c = newCase.id;
        ci.Product_In_Use__c = 'HQ';
        ci.Product_Support_Issue__c = 'Test issue';
        ci.Product_Support_Issue_Locations__c = 'Test location';
        insert ci;
        
        
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        newCase.Category__c = 'Service Request';
        newCase.Priority__c = '2';
        update newCase;
    }
    static testmethod void test3d() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Opt_In_Surveys__c = false;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'Compliance Services';
        newCase.Project_Status__c ='Delivered';
        newCase.Standalone__c = true;
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        newCase.Project_Work_Submitted__c = 'Yes';
        newCase.Case_Resolution__c = 'sdfsdf';
        newCase.Status = 'Closed';
        update newCase;
    }
    static testmethod void test4a() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Opt_In_Surveys__c = false;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Survey_Feedback__c surv = new Survey_Feedback__c();
        surv.Name = 'testsurv';
        surv.Account__c = newAccount.id;
        surv.Contact__c = newContact.id;
        surv.ETS_Send_Reminder__c = false;
        insert surv;
        
        surv.ETS_Send_Reminder__c = true;
        update surv;
    }
    static testmethod void test5a() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Survey_Feedback__c surv = new Survey_Feedback__c();
        surv.Name = 'testsurv';
        surv.Account__c = newAccount.id;
        surv.Contact__c = newContact.id;
        surv.ETS_Send_Reminder__c = false;
        insert surv;
        
        Survey_Queue__c q = new Survey_Queue__c();
        q.Contact__c = newContact.id;
        q.Send_Immediately__c = false;
        q.Survey__c = surv.id;
        insert q;
        
        q.Send_Immediately__c = true;
        update q;        
        
    }
    static testmethod void test6a() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Survey_Feedback__c surv = new Survey_Feedback__c();
        surv.Name = 'testsurv';
        surv.Account__c = newAccount.id;
        surv.Contact__c = newContact.id;
        surv.Was_Forwarded__c = false;
        insert surv;
        
        Survey_Queue__c q = new Survey_Queue__c();
        q.Contact__c = newContact.id;
        q.Survey__c = surv.id;
        insert q;
        
        surv.Was_Forwarded__c = true;
        update surv;
    }
    static testmethod void test6b() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Survey_Feedback__c surv = new Survey_Feedback__c();
        surv.Name = 'testsurv';
        surv.Account__c = newAccount.id;
        surv.Contact__c = newContact.id; 
        surv.Was_Forwarded__c = true;
        insert surv;
        
        Survey_Queue__c q = new Survey_Queue__c();
        q.Contact__c = newContact.id;
        q.Survey__c = surv.id;
        q.Send_Date__c = date.today();
        insert q;
        
        Contact newContact2 = new Contact();
        newContact2.FirstName = 'Bobby';
        newContact2.LastName = 'Bobby';
        newContact2.AccountID = newAccount.Id;
        newContact2.CustomerAdministratorId__c = 'z1z2z3z5';
        insert newContact2;
        
        surv.Contact__c = newContact2.id;
        update surv;
    }
    static testmethod void test6c() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'IMP: HQ';
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        Survey_Feedback__c surv = new Survey_Feedback__c();
        surv.Name = 'testsurv';
        surv.Case__c = newCase.id;
        surv.Account__c = newAccount.id;
        surv.Contact__c = newContact.id;
        surv.Was_Forwarded__c = true;
        insert surv;
        
        newCase.Survey_Feedback__c = surv.id;
        update newCase;
        
        Survey_Queue__c q = new Survey_Queue__c();
        q.Contact__c = newContact.id;
        q.Survey__c = surv.id;
        q.Send_Date__c = date.today();
        insert q;
        
        Contact newContact2 = new Contact();
        newContact2.FirstName = 'Bobby';
        newContact2.LastName = 'Bobby';
        newContact2.Opt_In_Surveys__c = false;
        newContact2.AccountID = newAccount.Id;
        newContact2.CustomerAdministratorId__c = 'z1z2z3z5';
        insert newContact2;
        
        surv.Contact__c = newContact2.id;
        update surv;
    }
    static testmethod void test6d() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'IMP: HQ';
        newCase.Subject = 'Test';
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        insert newCase;
        
        Survey_Feedback__c surv = new Survey_Feedback__c();
        surv.RecordTypeid = [SELECT id, Name FROM RecordType WHERE Name = 'ETS - Onboarding' LIMIT 1].id;
        surv.Name = 'testsurv';
        surv.Case__c = newCase.id;
        surv.Account__c = newAccount.id;
        surv.Contact__c = newContact.id;
        surv.Was_Forwarded__c = true;
        insert surv;
        
        newCase.Survey_Feedback__c = surv.id;
        update newCase;
        
        Survey_Queue__c q = new Survey_Queue__c();
        q.Contact__c = newContact.id;
        q.Survey__c = surv.id;
        insert q;
        
        Contact newContact2 = new Contact();
        newContact2.FirstName = 'Bobby';
        newContact2.LastName = 'Bobby';
        newContact2.Opt_In_Surveys__c = false;
        newContact2.AccountID = newAccount.Id;
        newContact2.CustomerAdministratorId__c = 'z1z2z3z5';
        insert newContact2;
        
        surv.Contact__c = newContact2.id;
        update surv;
    }
    static testmethod void testSurveyScheduler() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Survey_Feedback__c surv = new Survey_Feedback__c();
        surv.Name = 'testsurv';
        surv.Account__c = newAccount.id;
        surv.Contact__c = newContact.id;
        surv.Start_Date__c = date.today();
        insert surv;
        
        Survey_Queue__c q = new Survey_Queue__c();
        q.Contact__c = newContact.id;
        q.Survey__c = surv.id;
        q.Send_Date__c = date.today();
        insert q;
        
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new ETS_surveyScheduler());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, 
                            ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', 
                            String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
    static testmethod void testSurveySummaryEmails() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Survey_Feedback__c surv = new Survey_Feedback__c();
        surv.Name = 'testsurv';
        surv.Account__c = newAccount.id;
        surv.Contact__c = newContact.id;
        surv.Start_Date__c = date.today();
        surv.RecordTypeid = [SELECT id, Name FROM RecordType WHERE Name = 'ETS - Onboarding' LIMIT 1].id;
        surv.Onboarding_Experience_Satisfaction__c = 'Very Dissatisfied';
        surv.Survey_Completed_Date__c = date.today()-7;
        insert surv;
        
        Survey_Feedback__c surv2 = new Survey_Feedback__c();
        surv2.Name = 'testsurv';
        surv2.Account__c = newAccount.id;
        surv2.Contact__c = newContact.id;
        surv2.Start_Date__c = date.today();
        surv2.RecordTypeid = [SELECT id, Name FROM RecordType WHERE Name = 'ETS - Services' LIMIT 1].id;
        surv2.Services_Project_Experience_Satisfaction__c = 'Very Dissatisfied';
        surv2.Survey_Completed_Date__c = date.today()-7;
        surv2.Survey_Completed__c = TRUE;
        insert surv2;
        
        Survey_Feedback__c surv3 = new Survey_Feedback__c();
        surv3.Name = 'testsurv';
        surv3.Account__c = newAccount.id;
        surv3.Contact__c = newContact.id;
        surv3.Start_Date__c = date.today();
        surv3.RecordTypeid = [SELECT id, Name FROM RecordType WHERE Name = 'ETS - Support' LIMIT 1].id;
        surv3.Support_Satisfaction__c = 'Very Dissatisfied';
        surv3.Survey_Completed_Date__c = date.today()-7;
        insert surv3;
        
        
        
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new ETS_summaryEmail());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, 
                            ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', 
                            String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }  
    
    static testmethod void testSurveyCompleted() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Active_Products__c = 'HQ';
        newAccount.AdminID__c = '123456789abcdef';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Opt_In_Surveys__c = false;
        newContact.CustomerAdministratorId__c = 'z1z2z3z4';
        insert newContact;
        
        Survey_Feedback__c surv = new Survey_Feedback__c();
        surv.Name = 'testsurv';
        surv.Account__c = newAccount.id;
        surv.Contact__c = newContact.id;
        surv.ETS_Send_Reminder__c = false;
        insert surv;
        
        surv.Survey_Completed__c = TRUE;
        surv.Recommendation_1_10__c = '8';
        surv.Additional_Product_Interest__c = 'Chemical Management';
        update surv;
    }
    
}