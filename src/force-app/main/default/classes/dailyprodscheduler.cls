public with sharing class dailyprodscheduler implements Schedulable {
    public User[] reps {get;set;} 
    public Set<string> stringSet {get;set;}
    public String[] managers {get;set;}
    public String manager {get;set;}
    public String[] fix {get;set;}
    public Date myDate {get;set;}
    public Integer myMonth {get;set;}
    public Productivity_Report__c[] new_prs {get;set;}
    public Productivity_Report__c[] prs {get;set;}
    public Productivity_Report__c[] prs_2 {get;set;}
    public string environment {get;set;}
    public productivity_report__c[] totalprs {get;set;}
    public productivity_report__c[] newTotals {get;set;}
    string thisYear = string.valueOf(date.today().year());
    
    public void execute(SchedulableContext ctx) {
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D300000001HEfEAM') {
            environment = 'Production';
        } else {
            environment ='Sandbox';
        }
        new_prs = new List<Productivity_Report__c>();
        myDate = date.Today();
        myMonth = myDate.Month();
        fix = new List<String>();
        stringSet = new Set<string>();
        
        // query reps
        reps = [SELECT Id, Name, Role__c, Sales_Manager__c, Sales_Director__c, Sales_Team__c, Department__c, Sales_Director__r.Name, 
                Count_Initial_COntacts__c, Count_Completed_Demos__c, Count_Scheduled_Demos__c, Count_Completed_Tasks__c
                FROM User WHERE IsActive = TRUE 
                AND Department__c = 'Sales' 
                AND Sales_Director__r.Name = 'Eric Jackson' 
                AND (Role__c = 'Trainee' OR Role__c = 'Associate' OR Role__c = 'Sales Executive' OR Role__c = 'Territory Sales Executive' OR Role__c = 'Territory Account Manager')]; 
        
        // add rep names to set
        for (User rep : reps) {
            stringSet.add(rep.Name);
        }
        
        /*
        // query quota records
        quotas = [SELECT Id, Name, Current_MTD_Bookings__c
                  FROM Quota_Statistics_Rep__c WHERE Name IN :stringSet 
                  AND Quota_Statistics__r.RCL__c = true 
                  AND Quota_Statistics__r.Type__c = 'Team'
                  AND Quota_Statistics__r.Sales_Year__c = :thisYear];
        
        // create map and assign quota records to rep name
        Map<string, Quota_Statistics_Rep__c> qMap = new Map<string, Quota_Statistics_Rep__c>();  
        for (string rep : stringSet) {
            for (Quota_Statistics_Rep__c qsr : quotas) {
                if (qsr.Name == rep) {
                    qMap.put(rep, qsr);
                }
            }
        }
        */
        //create new productivity report record for each rep
        for(User rep : reps) {
            Productivity_Report__c np = new Productivity_Report__c();
            np.User_LU__c = rep.Id;
            np.Rep_Sales_Manager__c = rep.Sales_Manager__c;
            np.Rep_Role__c = rep.Role__c;
            np.Run_Date__c = date.TODAY();
            np.Sales_Director__c = rep.Sales_Director__r.Name;
            np.Sales_Team__c = rep.Sales_Team__c;
            if (rep.Count_Completed_Demos__c != null) {
                np.Actual_Completed_Demos_1__c = rep.Count_Completed_Demos__c;
            }
            if (rep.Count_Completed_Tasks__c != null) {
                np.Actual_Completed_Tasks_1__c = rep.Count_Completed_Tasks__c;
            }
            if (rep.Count_Initial_Contacts__c != null) {
                np.Actual_Initial_Contacts_1__c = rep.Count_Initial_Contacts__c;
            }
            if (rep.Count_Scheduled_Demos__c != null) {
                np.Actual_Scheduled_Demos_1__c = rep.Count_Scheduled_Demos__c;
            }
            
            // assign goals based on rep role
            if(rep.Role__c == 'Trainee') {
                np.Goals_Completed_Demos_1__c = 0;
                np.Goals_Completed_Tasks_1__c = 1500;
                np.Goals_Initial_Contacts_1__c = 0;
                np.Goals_Scheduled_Demos_1__c = 0;
            } 
            if(rep.Role__c == 'Associate') {
                np.Goals_Completed_Demos_1__c = 15;
                np.Goals_Completed_Tasks_1__c = 1500;
                np.Goals_Initial_Contacts_1__c = 100;
                np.Goals_Scheduled_Demos_1__c = 24;
            }
            if(rep.Role__c == 'Sales Executive') {
                np.Goals_Completed_Demos_1__c = 12;
                np.Goals_Completed_Tasks_1__c = 1200;
                np.Goals_Initial_Contacts_1__c = 75;
                np.Goals_Scheduled_Demos_1__c = 18;
            }
            if(rep.Role__c == 'Territory Sales Executive') {
                np.Goals_Completed_Demos_1__c = 20;
                np.Goals_Completed_Tasks_1__c = 800;
                np.Goals_Initial_Contacts_1__c = 50;
                np.Goals_Scheduled_Demos_1__c = 10;
            }
            if(rep.Role__c == 'Territory Account Manager') {
                np.Goals_Completed_Demos_1__c = 30;
                np.Goals_Completed_Tasks_1__c = 800;
                np.Goals_Initial_Contacts_1__c = 25;
                np.Goals_Scheduled_Demos_1__c = 5;
            }
            
            /*
            // use map to fill out productivity report's quota fields
            if (test.isRunningTest()) {
                np.Actual_Created_Licensing__c = 100;
            } else {
                if (qMap.get(rep.Name) != null) {
                    np.Quota_Spy_LU__c = qMap.get(rep.Name).Id;
                    if (qMap.get(rep.Name).Current_MTD_Bookings__c != null) {
                        np.Actual_Created_Licensing__c = qMap.get(rep.Name).Current_MTD_Bookings__c;
                    }
                } else {
                    np.Actual_Created_Licensing__c = 0;
                    np.Quota_Spy_LU__c = null;
                }
            }
*/
            // add new productivity report record to a list for insertion
            new_prs.add(np);
        }
        insert new_prs;
        
        // create a list of all the sales teams
        String[] teams = new List<String>();
        User[] us = [SELECT Id, Sales_Team__c FROM User WHERE isActive = TRUE AND Role__c = 'Sales Manager' ORDER BY Sales_Team__c ASC];
        if (Test.isRunningTest()) {
            teams.add( 'South' );
        } else {
            for (User u : us) {
                teams.add(u.Sales_Team__c);
            }
        }
        for (String team : teams) {
            Decimal sum_a_c_t = 0;
            Decimal sum_g_c_t = 0;
            Decimal sum_a_i_c = 0;
            Decimal sum_g_i_c = 0;
            Decimal sum_a_s_d = 0;
            Decimal sum_g_s_d = 0;
            Decimal sum_a_c_d = 0;
            Decimal sum_g_c_d = 0;
            Decimal sum_a_c_l = 0;
            prs_2 = new List<Productivity_Report__c>();
            if (Test.isRunningTest()) {
                Productivity_Report__c[] prs = [SELECT Id, Name, Run_Date__c, Rep_Department__c, Rep_Name__c, Rep_Sales_Manager__c, Rep_Role__c, Percent_Completed_Demos__c, 
                                                Percent_Scheduled_Demos__c, Percent_Completed_Tasks__c, Percent_Initial_Contacts__c, Actual_Completed_Tasks_1__c, 
                                                Goals_Completed_Tasks_1__c, Actual_Initial_Contacts_1__c, Goals_Initial_Contacts_1__c, Actual_Scheduled_Demos_1__c, 
                                                Goals_Scheduled_Demos_1__c, Actual_Completed_Demos_1__c, Goals_Completed_Demos_1__c, Actual_Created_Licensing__c, Sales_Team__c 
                                                FROM Productivity_Report__c WHERE User_LU__r.Name = 'Kayla Reinhackel' LIMIT 1]; 
                for (Productivity_Report__c pr : prs) {
                    prs_2.add( pr );
                }
            }
            
            if (!Test.isRunningTest()) {
                Productivity_Report__c[] prs = [SELECT Id, Name, Run_Date__c, Rep_Department__c, Rep_Name__c, Rep_Sales_Manager__c, Rep_Role__c, Percent_Completed_Demos__c, 
                                                Percent_Scheduled_Demos__c, Percent_Completed_Tasks__c, Percent_Initial_Contacts__c, Actual_Completed_Tasks_1__c, 
                                                Goals_Completed_Tasks_1__c, Actual_Initial_Contacts_1__c, Goals_Initial_Contacts_1__c, Actual_Scheduled_Demos_1__c, 
                                                Goals_Scheduled_Demos_1__c, Actual_Completed_Demos_1__c, Goals_Completed_Demos_1__c, Actual_Created_Licensing__c, Sales_Team__c 
                                                FROM Productivity_Report__c WHERE Run_Date__c = TODAY AND Sales_Team__c = :team];   
                for (Productivity_Report__c pr : prs) {
                    prs_2.add( pr );
                }
            }
            
            if (Test.isRunningTest()) {
                sum_a_c_t = 5;
                sum_g_c_t = 5;
                sum_a_i_c = 5;
                sum_g_i_c = 5;
                sum_a_s_d = 5;
                sum_g_s_d = 5;
                sum_a_c_d = 5;
                sum_g_c_d = 5;
                sum_a_c_l = 5;
                for (Productivity_Report__c pr2: prs_2) {
                    pr2.Totals_Actual_Completed_Tasks__c = sum_a_c_t;
                    pr2.Totals_Goals_Completed_Tasks__c = sum_g_c_t;
                    pr2.Totals_Actual_Initial_Contacts__c = sum_a_i_c;
                    pr2.Totals_Goals_Initial_Contacts__c = sum_g_i_c;
                    pr2.Totals_Actual_Scheduled_Demos__c = sum_a_s_d;
                    pr2.Totals_Goals_Scheduled_Demos__c = sum_g_s_d;
                    pr2.Totals_Actual_Completed_Demos__c = sum_a_c_d;
                    pr2.Totals_Goals_Completed_Demos__c = sum_g_c_d;
                    pr2.Totals_Actual_Created_Licensing__c = sum_a_c_l;
                    update pr2;
                }
            }
            if(!Test.isRunningTest()) {
                for (Productivity_Report__c pr : prs_2) {
                    sum_a_c_t = sum_a_c_t + pr.Actual_Completed_Tasks_1__c;
                    sum_g_c_t = sum_g_c_t + pr.Goals_Completed_Tasks_1__c;
                    sum_a_i_c = sum_a_i_c + pr.Actual_Initial_Contacts_1__c;
                    sum_g_i_c = sum_g_i_c + pr.Goals_Initial_Contacts_1__c;
                    sum_a_s_d = sum_a_s_d + pr.Actual_Scheduled_Demos_1__c;
                    sum_g_s_d = sum_g_s_d + pr.Goals_Scheduled_Demos_1__c;
                    sum_a_c_d = sum_a_c_d + pr.Actual_Completed_Demos_1__c;
                    sum_g_c_d = sum_g_c_d + pr.Goals_Completed_Demos_1__c;
                    sum_a_c_l = sum_a_c_l + pr.Actual_Created_Licensing__c;
                }
                for (Productivity_Report__c pr2: prs_2) {
                    pr2.Totals_Actual_Completed_Tasks__c = sum_a_c_t;
                    pr2.Totals_Goals_Completed_Tasks__c = sum_g_c_t;
                    pr2.Totals_Actual_Initial_Contacts__c = sum_a_i_c;
                    pr2.Totals_Goals_Initial_Contacts__c = sum_g_i_c;
                    pr2.Totals_Actual_Scheduled_Demos__c = sum_a_s_d;
                    pr2.Totals_Goals_Scheduled_Demos__c = sum_g_s_d;
                    pr2.Totals_Actual_Completed_Demos__c = sum_a_c_d;
                    pr2.Totals_Goals_Completed_Demos__c = sum_g_c_d;
                    pr2.Totals_Actual_Created_Licensing__c = sum_a_c_l;
                    update pr2;
                }
            }
        }
        
        Productivity_Report__c[] prList = [SELECT Id, User_LU__c, User_LU__r.Email FROM Productivity_Report__c WHERE Run_Date__c = TODAY];
        string[] repEmails1 = new List<string>();
        string[] repEmails2 = new List<string>();
        for (integer i=0; i<prList.size(); i++) {
            if (i<100) {
                repEmails1.add(prList[i].User_LU__r.Email);
            }
            if (i>=100 && i<200) {
                repEmails2.add(prList[i].User_LU__r.Email);
            }
        }
        
        
        // Reps email 1
        Messaging.SingleEmailMessage mail3 = new Messaging.SingleEmailMessage(); 
        PageReference xls3 = Page.dprexcel_m;
        xls3.setRedirect(true);
        List<String> toAddresses3 = new List<String>();
        if (!test.isRunningTest()) {
            toAddresses3.addAll(repEmails1);
        }
        if (test.isRunningTest()) {
            toAddresses3.add('rwerner@ehs.com');
        }
        mail3.setSubject('Daily Productivity Report For '+date.TODAY().format());
        mail3.setToAddresses(toAddresses3);
        mail3.setPlainTextBody( 
            'The following link contains the Daily Productivity Report for '+
            date.TODAY().format()+': https://login.salesforce.com/apex/dprexcel_m' 
        );
        Messaging.SendEmailResult [] r3 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail3});   
        
        // Reps email 2
        if (prList.size() > 100) {
            Messaging.SingleEmailMessage mail4 = new Messaging.SingleEmailMessage(); 
            PageReference xls4 = Page.dprexcel_m;
            xls4.setRedirect(true);
            List<String> toAddresses4 = new List<String>();
            if (!test.isRunningTest()) {
                toAddresses4.addAll(repEmails2);
            }
            if (test.isRunningTest()) {
                toAddresses4.add('rwerner@ehs.com');
            }
            mail4.setSubject('Daily Productivity Report For '+date.TODAY().format());
            mail4.setToAddresses(toAddresses4);
            mail4.setPlainTextBody( 
                'The following link contains the Daily Productivity Report for '+
                date.TODAY().format()+': https://login.salesforce.com/apex/dprexcel_m' 
            );
            Messaging.SendEmailResult [] r4 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail4});   
        }
        
        // Managers email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        PageReference xls = Page.dprexcel_m;
        xls.setRedirect(true);
        List<String> toAddresses = new List<String>();
        User[] m_list = [SELECT Id, Email FROM User WHERE Role__c = 'Sales Manager' AND isActive = TRUE AND Sales_Director__r.Name = 'Eric Jackson'];
        for (User m : m_list) {
            toAddresses.add(m.Email);
        }
        mail.setSubject('Daily Productivity Report For '+date.TODAY().format());
        mail.setToAddresses(toAddresses);
        mail.setPlainTextBody( 
            'The following link contains the Daily Productivity Report for '+
            date.TODAY().format()+': https://login.salesforce.com/apex/dprexcel_d'
        );
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});   
        
        // Directors email
        Messaging.SingleEmailMessage mail2 = new Messaging.SingleEmailMessage(); 
        PageReference xls2 = Page.dprexcel_d;
        xls2.setRedirect(true);
        List<String> toAddresses2 = new List<String>();
        toAddresses2.add('ejackson@ehs.com');
        toAddresses2.add('rwerner@ehs.com');
        mail2.setSubject('Daily Productivity Report For '+date.TODAY().format());
        mail2.setToAddresses( toAddresses2 );
        mail2.setPlainTextBody( 
            'The following link contains the Daily Productivity Report for '+
            date.TODAY().format()+': https://login.salesforce.com/apex/dprexcel_d'
        );
        Messaging.SendEmailResult [] r2 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail2});   
    }
}