/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Revenue_Recognition_TransactionTest
{
    private static testmethod void testTrigger()
    {
        // Code to cover the one line trigger, the method called has been tested separately by the packaged tests.
        try { insert new Revenue_Recognition_Transaction__c(); } catch(Exception e) { }
    }
}