@isTest()
Private class testAmeConsoleHeaderController {
    static testMethod void test1(){
        //Query the rentention user who will be viewing the page
        User viewingUser = [SELECT id,Name from User WHERE isActive = true and UserRole.Name LIKE '%Retention%' LIMIT 1];
        
        //Pass in the url parameter that will display when the user is viewing the page
        ApexPages.currentPage().getParameters().put('va', viewingUser.Id);
        
        //Pass in the controller user in the AME header controller
        ameConsoleHeaderController myController = new ameConsoleHeaderController(); 
        
        //Assert to check that view as name variable matches what we pull back in the query
        system.assertEquals(mycontroller.ViewAsName,viewinguser.name);
        
		//Call methods being used in the page        
        mycontroller.getRetentionReps();
        mycontroller.resetView();
        
        //Create test user to fire the error statment 
        mycontroller.viewAsName = 'test';
        
        //Call redirect method
        mycontroller.viewAsRedirect();
        
        //Checking to see if the error message fired
        system.assert(mycontroller.errorMsg != null);
        
        //Pull rentention user from query to activate the viewing as feature 
        mycontroller.viewAsName = viewingUser.name;
        
        //Call view as redirect method
        mycontroller.viewAsRedirect();
    }
}