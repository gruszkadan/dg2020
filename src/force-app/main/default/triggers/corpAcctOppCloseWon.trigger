trigger corpAcctOppCloseWon on Opportunity (before update) 
{
    for (Opportunity oldOpp : trigger.old)
    {
        if (oldOpp.StageName != 'Closed/Won')
        {
            for( Opportunity opp : trigger.new )
            {
                if( opp.Opportunity_Type__c == 'New' && opp.StageName == 'Closed/Won' && opp.Corporate_Account__c == TRUE && opp.Corporate_Account_Close_Won_Email__c == false)
                {
                    opp.Corporate_Account_Close_Won_Email__c = true;
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    String[] toAddresses = new List<String>();
                    
                    String[] ccAddresses = new List<String>();
                    User[] emails = [ SELECT Id, Email FROM User WHERE isActive = TRUE AND (Role__c = 'Sales Manager' OR Role__c = 'Director')  ];
                    Account a = [ SELECT Id, OwnerId FROM Account WHERE Id = :opp.AccountId ];
                    for( User e : emails )
                    {
                        toAddresses.add(e.Email);
                    }
                    if( opp.Account.Corporate_Account_Approved_Referral_Reps__c != null )
                    {
                        String[] reps = opp.Account.Corporate_Account_Approved_Referral_Reps__c.Split(',', 0);
                        for( String r : reps )
                        {
                            toAddresses.add( [ SELECT Email FROM User WHERE Id = :r LIMIT 1 ].Email );
                        }
                    }
                    if (opp.Ownerid != '00580000007F76q')
                    {
                    mail.setToAddresses(toAddresses);
                    mail.SetCcAddresses(ccAddresses);                        
                    }
                    mail.setSaveAsActivity(false); 
                    mail.setReplyTo([SELECT id, Email FROM User WHERE id = :opp.OwnerId LIMIT 1].email);
                    mail.setWhatId(opp.Id);
                    mail.setTemplateId( [ SELECT Id FROM EmailTemplate WHERE Name = 'Corporate Account Closed Won' LIMIT 1 ].Id );
                    mail.settargetobjectid(a.OwnerId);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            }  
        }
    }
}