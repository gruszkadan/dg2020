public class contentUtility {
    
    //Find latest ContentVersion records from ContentDocuments
    public static ContentVersion[] findContentVersions(ContentDocumentLink[] documentLinks){
        set<id> contentDocumentIds = new set<id>();
        for(ContentDocumentLink cdl:documentLinks){
            contentDocumentIds.add(cdl.ContentDocumentId);
        }
        ContentVersion[] versions = [SELECT id,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId in:contentDocumentIds AND isLatest = true AND Content_Document_Image__c = null];
        return versions;
    }
    
    //Sends notification about new Content Document/Version updates
    public static void sendContentNotification(ContentVersion[] versions){
        //Get ContentDocument ids of the ContentVersions passed in
        set<id> documentIds = new set<id>();
        for(ContentVersion v:versions){
            documentIds.add(v.ContentDocumentId);
        }
        
        //Query ContentWorkspaceDocs to pull back all library associations for each doc
        ContentWorkspaceDoc[] workspaceDocs = [SELECT id, ContentDocumentId, ContentWorkspaceId FROM ContentWorkspaceDoc Where ContentDocumentId in:documentIds];
        set<id> libraryIds = new set<id>();
        for(ContentWorkspaceDoc wd:workspaceDocs){
            libraryIds.add(wd.ContentWorkspaceId);
        }
        
        //Query to find the Library names for each ContentDocument
        ContentWorkspace[] libraries = [Select id,Name from ContentWorkspace where id in:libraryIds AND (Name = 'Proposal Standard Content' OR Name = 'Proposal Marketing Content' OR Name = 'Solution Sheets')];
        
      
        //Query for Salesforce Request Owner and Salesforce Request Queue
        id RequestedBy = [Select id, name from user where Name = 'Mark McCauley' limit 1].id;
        id SfrQ =  [Select id, name from Group where DeveloperName = 'SF_Request_Queue' limit 1].id;
        // Intialize a list to add the salesforce request too then we will update this list
        list<Salesforce_Request__c> Request = new list<Salesforce_Request__c>();
        
        
        //Loop over each content version
        for(ContentVersion v:versions){
            //Loop over workspace docs
            for(ContentWorkspaceDoc d:workspaceDocs){
                //If the workspace doc is associated with the current content document
                if(v.ContentDocumentId == d.ContentDocumentId){
                    //Loop over the libraries
                    for(ContentWorkspace l:libraries){
                        //If Library matches
                        if(d.ContentWorkspaceId == l.id){
                            // Create new Salesforce request
                            Salesforce_Request__c SFR = new Salesforce_Request__c();
                            //Set fields on the newly created SF request
                            SFR.Summary__c = 'Create Content Document Image';
                            SFR.Description__c = 'A new content document has been added. Please create a new content document image record for this new version. Document URL: ' + '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + v.ContentDocumentId + '" target="_blank">Click Here</a>';
                            SFR.Requested_By__c = RequestedBy;
                            SFR.OwnerId = SfrQ;
                            
                            Request.add(SFR);
                            
                        }
                    }
                    //Break out of loop so that it only sends one email per contentDoc 
                    break;
                }
            }
        }
        
        insert Request;
        
    }
}