public class ScheduleAutoKillBatches implements Schedulable{
    public void execute(SchedulableContext ctx) {
        autoKillOpportunitiesBatchable killOpps = new autoKillOpportunitiesBatchable();
        database.executebatch(killOpps, 100);
    }
}