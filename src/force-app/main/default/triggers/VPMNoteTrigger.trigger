trigger VPMNoteTrigger on VPM_Note__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (trigger.isBefore) {
        
        // BEFORE INSERT
        if (trigger.isInsert) {
            VPMNoteTriggerHandler.beforeInsert(trigger.new);
        }
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            VPMNoteTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
        
        // BEFORE DELETE
        if (trigger.isDelete) {
            VPMNoteTriggerHandler.beforeDelete(trigger.old, trigger.new);
        }
        
    }
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            VPMNoteTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            VPMNoteTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
        // AFTER DELETE
        if (trigger.isDelete) {
            VPMNoteTriggerHandler.afterDelete(trigger.old);
        }
        
    }
    
}