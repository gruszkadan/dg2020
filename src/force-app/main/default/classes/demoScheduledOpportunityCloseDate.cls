global class demoScheduledOpportunityCloseDate implements Schedulable {
    global void execute(SchedulableContext ctx) {
        demoOpportunityCloseDateBatchable ocd = new demoOpportunityCloseDateBatchable();
        database.executebatch(ocd,200);
    }
}