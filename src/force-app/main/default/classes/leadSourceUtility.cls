public class leadSourceUtility {
    public static void updateLeadSourceValues(sObject[] people, string action, string sObjectType) {
        sObjectField leadSource;
        sObjectField leadSourceType;
        sObjectField leadSourceCategory;
        sObjectField communicationChannel;
        
        //set fields based on sObjectType
        if(sObjectType == 'Contact'){
            leadSource = Contact.LeadSource;
            leadSourceType = Contact.Lead_Source_Type__c;
            leadSourceCategory = Contact.Lead_Source_Category__c;
            communicationChannel = Contact.Communication_Channel__c;
        }else if (sObjectType == 'Lead'){
            leadSource = Lead.LeadSource;
            leadSourceType = Lead.Lead_Source_Type__c;
            leadSourceCategory = Lead.Lead_Source_Category__c;
            communicationChannel = Lead.Communication_Channel__c;
        }
        
        //query the lead source custom setting
        Lead_SourceCategoryType_Table__c[] leadSourceTable = [Select User_Selected_Lead_Source__c, Lead_Source__c, Lead_Source_Category__c, Lead_Source_Type__c from Lead_SourceCategoryType_Table__c];
        //loop through the leads
        for (sObject p : people){
            boolean sourceFound = false;
            //for each lead loop trough the lead source custom settings list 
            for (Lead_SourceCategoryType_Table__c t : leadSourceTable){
                if(action == 'insert'){
                    //if inserted by a user
                    if((string)p.get(communicationChannel) != null && (string)p.get(leadSource) == null){
                        //match the lead's communiction channel with the user selected lead source in the custom setting list
                        if((string)p.get(communicationChannel) == t.User_Selected_Lead_Source__c){
                            p.put(leadSource,t.Lead_Source__c);
                            p.put(leadSourceCategory,t.Lead_Source_Category__c);
                            p.put(leadSourceType,t.Lead_Source_Type__c);
                        }
                    }
                    //if inserted by Marketo
                    if((string)p.get(communicationChannel) == null && (string)p.get(leadSource) != null){
                        if((string)p.get(leadSource) == t.Lead_Source__c){
                            p.put(leadSourceCategory,t.Lead_Source_Category__c);
                            p.put(leadSourceType,t.Lead_Source_Type__c);
                        }
                    }
                }else{
                    if(action == 'update'){
                        //match the lead source of the lead with the lead source in the custom setting list
                        if((string)p.get(leadSource) == t.Lead_Source__c){
                            sourceFound = true;
                            p.put(leadSourceCategory,t.Lead_Source_Category__c);
                            p.put(leadSourceType,t.Lead_Source_Type__c);
                        }
                    }
                }
            }
            if (!sourceFound && action == 'update') {
                p.put(leadSourceCategory,null);
                p.put(leadSourceType,null);
            }
        }
    }
}