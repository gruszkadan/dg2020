public class productInformationController {    
    public Id AcctId {get;set;}
    public platformWrapper[] productsSortedIntoWrappers {get;set;}
    public integer rowNumber {get;set;}
    public String[] platformNameList {get;set;}
    public string accountGUID {get;set;}
    //public String AccountVersion {get;set;}
    public boolean styleHorizontally {get;set;}
    public string categorySizeStyle {get;set;}
    
    
    public productInformationController(){}
    
  
    public list<platformWrapper> getProductsByCategory(){
        
        rowNumber = 0;
        Set<String> finalList = new Set<String>();
        AT_Product_Mapping__c[] categories = [Select Product_Platform_Display_Name__c from AT_Product_Mapping__c WHERE Product_Platform_Display_Name__c != NULL];
        for(AT_Product_Mapping__c pm: categories){
           finalList.add(pm.Product_Platform_Display_Name__c);
        }   
        platformNameList = new List<String>(finalList);
        
        Set<String> oiName = new Set<String>();
        
        Map<string, platformWrapper> platformMap = new Map<string, platformWrapper>();
        
        Order_Item__c[] orderItems = [Select id, Salesforce_Product_Name__c, Status__c, Product_Platform_Display_Name__c, Category__c, Category_Display_Name__c, Version__c, Product_Platform__c, Order_Date__c, Admin_Tool_Product_Name__c,
                                      Account__r.Customer_Annual_Revenue__c, Account__r.Customer_Annual_Revenue_EHS__c, Account__r.Customer_Annual_Revenue_Ergo__c, Account__r.EHS_Subscription_Model__c, Account__r.GUID__c
                                      FROM Order_Item__c 
                                      where Account__c = :AcctId AND (Term_Start_Date__c = NULL OR Term_Start_Date__c <= Today) AND Display_on_Account__c = TRUE AND Product_Platform_Display_Name__c != NULL
                                      ORDER BY Order_Date__c DESC];

        
        
        for(Order_Item__c oi: orderItems){ 
           
            if(accountGUID == NULL){
                accountGUID = oi.account__r.GUID__c; 
            }
           // if(accountVersion == NULL){
           //     accountVersion = oi.Version__c;  
           //}
            
            if(platformMap.containsKey(oi.Product_Platform_Display_Name__c)){
                boolean found = false;
                System.debug(platformMap.get(oi.Product_Platform_Display_Name__c).categories);
                for(categoryWrapper cw: platformMap.get(oi.Product_Platform_Display_Name__c).categories){  
                    System.debug(oi.Category_Display_Name__c);
                    System.debug(cw.Category);
                    if(oi.Category_Display_Name__c == cw.Category){
                        
                        if(!platformMap.get(oi.Product_Platform_Display_Name__c).productsIncluded.contains(oi.Salesforce_Product_Name__c)){
                             cw.orderItemList.add(oi);
                            oiName.add(oi.Salesforce_Product_Name__c);
                            platformMap.get(oi.Product_Platform_Display_Name__c).productsIncluded.add(oi.Salesforce_Product_Name__c);
                        }
                        found = true;
                        break;
                    }
                }
                if(!found){
                    if(!platformMap.get(oi.Product_Platform_Display_Name__c).productsIncluded.contains(oi.Salesforce_Product_Name__c)){
                    platformMap.get(oi.Product_Platform_Display_Name__c).categories.add(new categoryWrapper(oi));
                    oiName.add(oi.Salesforce_Product_Name__c);
                    platformMap.get(oi.Product_Platform_Display_Name__c).productsIncluded.add(oi.Salesforce_Product_Name__c);
                      
                   }
                }
            }else{
                platformMap.put(oi.Product_Platform_Display_Name__c, new platformWrapper(oi, null));
                oiName.add(oi.Salesforce_Product_Name__c);
                platformMap.get(oi.Product_Platform_Display_Name__c).productsIncluded.add(oi.Salesforce_Product_Name__c);
            } 
        }
        //add any empty containers:
        for(string s: platformNameList){
             if(!platformMap.containsKey(s)){
                  platformMap.put(s, new platformWrapper(null, s));
            }
        }
        
        //convert map to list and sort lists     
       platformWrapper[] results = new List<platformWrapper>(); 
        for(platformWrapper p: platformMap.values()){
            p.categories.sort();
            results.add(p);
        }
        rowNumber = results.size();
        categorySizeStyle = 'slds-size--1-of-' + rowNumber;
        results.sort();  
        Return results;    
    } 
  
    
    //platform wrappers -- each new platform creates the first category automatically based on the first OI added
    public class platformWrapper implements comparable{
        public string Platform {get;set;}
        public categoryWrapper[] categories {get;set;}
        public decimal ARR {get;set;} 
        public String version {get;set;}
        public String accountGUID {get;set;}
        public string subscription {get;set;}
        public boolean empty {get;set;}
        Set<String> productsIncluded {get;set;}
        
        
        public platformWrapper(Order_Item__c initialOrderItem, string platformName){
            if(platformName == NULL){
                platform = initialOrderItem.Product_Platform_Display_Name__c;
                version = initialOrderItem.Version__c;
                productsIncluded = new Set<String>();
                productsIncluded.add(initialOrderItem.Product_Platform_Display_Name__c);
                categories = new List<categoryWrapper>{new categoryWrapper(initialOrderItem)}; 
                empty = false; 
                Switch on initialOrderItem.Product_Platform__c {
                    when 'EHS'{
                        ARR = initialOrderItem.Account__r.Customer_Annual_Revenue_EHS__c;
                        subscription = initialOrderItem.Account__r.EHS_Subscription_Model__c;
                    }when 'Ergo'{
                        ARR = initialOrderItem.Account__r.Customer_Annual_Revenue_Ergo__c;
                    }when 'MSDSonline'{
                        ARR = initialOrderItem.Account__r.Customer_Annual_Revenue__c;
                        accountGUID = initialOrderItem.account__r.GUID__c; 
                    }
                }
                if (ARR == NULL){
                    ARR = 0;
                }
            }else{
                platform =  platformName;
                categories = new List<categoryWrapper>();
                ARR = 0;
                empty = true;
            }
        }
        
        public Integer compareTo(Object compareTo) {
                return platform.compareTo(((platformWrapper)compareTo).platform); 
        } 
    }
    
    //category wrappers
    public class categoryWrapper implements comparable{
        public string Category {get;set;}
        public Order_Item__c[] orderItemList {get;set;}  
        public integer forceOrder {get;set;}
        
        public categoryWrapper(Order_Item__c creationOrderItem) {
            Category = creationOrderItem.Category_Display_Name__c;
            orderItemList = new  List<Order_Item__c>{creationOrderItem};
                forceOrder = 101;
            if(creationOrderItem.Category_Display_Name__c == 'Licensing'){
                forceOrder = 1;
            } 
            if(creationOrderItem.Category_Display_Name__c == 'Services'){
                forceOrder = 2;
            }
        }
        
        public Integer compareTo(Object compareTo) {
            
            Integer returnValue = 0;
            if (forceOrder > ((categoryWrapper)compareTo).forceOrder) {
                returnValue = 1;
            }
            if (forceOrder < ((categoryWrapper)compareTo).forceOrder){
                returnValue = -1;
            }
            if(returnValue != 0){
                return returnValue; 
            }else{
                return category.compareTo(((categoryWrapper)compareTo).category); 
            }
        } 
        
    }
}