public class ameConsoleToDoController {
    public Account_Management_Event__c[] ames {get;set;}
    public id selectedAMEId {get;set;}
    public Account_Management_Event__c selectedAME {get;set;}
    public string whereStatement {get;set;}
    public string queryScope {get;set;}
    public string selectedViewId {get;set;}
    public string sortField {get;set;}
    public string sortDirection {get;set;}
    public string viewingUserId {get;set;}
    public SelectOption[] viewOptions {get;set;}
    string authToken;
    boolean canViewAs;
    public ameConsoleToDoController(){
        //Look for view as parameter
        canViewAs = System.FeatureManagement.checkPermission('AME_Console_View_As');
        if(canViewAs){
            viewingUserId = ApexPages.currentPage().getParameters().get('va');
        }
        if(viewingUserId == null || viewingUserId == ''){
            viewingUserId = userInfo.getUserId();
        }
        //Find available list views
        queryListViews();
        //Callout to get ListView informaion and query AME records
        loadView();
        //Check to see if an AME is seleced
        selectAME();
    }
    
    //Callout to ListView API to return our filters for the AME Query.
    public void loadView(){
        string sessionId = UserInfo.getSessionId();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer '+sessionId);
        string ep = System.URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v45.0/sobjects/Account_Management_Event__c/listviews/'+selectedViewId+'/describe';
        req.setEndpoint(ep);
        if(!test.isRunningTest()){
            HttpResponse res = new Http().send(req);
            queryWrapper qw = (queryWrapper)json.deserialize(res.getBody(), queryWrapper.class);
            whereStatement = (qw.query.containsIgnoreCase('WHERE')?qw.query.substringBetween('WHERE','ORDER BY'):'No filter criteria for this list view');
            queryScope = qw.scope;
        }else{
            whereStatement = 'id != null';
            queryScope = 'Everything';
        }
        //Run the queryAMEs() method to load the AME TO Do List based on the where statement returned
        queryAMEs();
    }
    
    //Query list views associated to the Account_Management_Event__c object. These will be the options displayed for selection in the To Do list sidebar
    public void queryListViews(){
        viewOptions = new list<SelectOption>();
        ListView[] views = [SELECT id,Name,DeveloperName FROM ListView WHERE SobjectType='Account_Management_Event__c' ORDER BY CreatedDate ASC];
        for(ListView v:views){
            viewOptions.add(new SelectOption(v.Id,v.Name));
        }
        //Check for selected list view parameter in URL. If existing then default to that view.
        if(selectedViewId == null && ApexPages.currentPage().getParameters().get('currentView') != null){
            selectedViewId = ApexPages.currentPage().getParameters().get('currentView');
        }else{
            selectedViewId = views[0].id;
        }
    }
    
    //Query AME's displayed in the to-do list
    public void queryAMEs(){
        Map<String, Schema.SObjectField> fieldMap = Account_Management_Event__c.sObjectType.getDescribe().fields.getMap();
        Set<String> fieldNames = fieldMap.keySet();
        
        //Set scope/ownership filters based on user permissions
        boolean canViewAs = System.FeatureManagement.checkPermission('AME_Console_View_As');
        if(queryScope == 'mine' || (!canViewAs) || (canViewAs && viewingUserId != userInfo.getUserId())){
            //If user has no acces to view as OR if they are viewing as another user then filter the results to be based on that user
            whereStatement += ' AND OwnerId =\''+viewingUserId+'\'';
        }
       
        //Additional fields to query
        string addlFields = ',RecordType.Name,RecordType.DeveloperName,Account__r.Name,Contact__r.Name,Owner.Name';
        
        //Set sort by field
        if(sortField == null && ApexPages.currentPage().getParameters().get('sortField') != null){
            sortField = ApexPages.currentPage().getParameters().get('sortField');
        }else if(sortField == null){
            sortField = 'Next_Open_Task__c';    
        }
        
        //Set sort direction
        if(sortDirection == null && ApexPages.currentPage().getParameters().get('sortDirection') != null){
            sortDirection = ApexPages.currentPage().getParameters().get('sortDirection');
        }else if(sortDirection == null){
            sortDirection = 'ASC';    
        }
        
        //If we have a selected ame then add it to the query
        if(selectedAMEId != null){
            whereStatement = '('+whereStatement+')';
            whereStatement += ' OR (id = \''+selectedAMEId+'\')';            
        }else if(ApexPages.currentPage().getParameters().get('id') != null){
            whereStatement = '('+whereStatement+')';
            whereStatement += ' OR (id = \''+ApexPages.currentPage().getParameters().get('id')+'\')';     
        }
        
        // Build the query string and run query
        ames = Database.query('select ' + string.join((Iterable<String>)fieldNames, ',') +addlFields+
                              +' from Account_Management_Event__c'//using scope '+queryScope+
                              +' where '+whereStatement+
                              +' ORDER BY '+sortField+
                              +' '+sortDirection+' NULLS FIRST');
    }
    
    //Used for the sort button to switch sort direction and re-query the AME list
    public void updateSort(){
        if(sortDirection == 'ASC'){
            sortDirection = 'DESC';
        }else{
            sortDirection = 'ASC';
        }
        queryAMEs();
    }
    
    //Used to set the AME being viewed. This drives the included components on the page
    public void selectAME(){
        if(selectedAMEId == null && ApexPages.currentPage().getParameters().get('id') != null){
            selectedAMEId = ApexPages.currentPage().getParameters().get('id');
        }else{
            selectedAMEId = ApexPages.currentPage().getParameters().get('selectedAMEId'); 
        }
        for(Account_Management_Event__c ame:ames){
            if(ame.id == selectedAMEId){
                selectedAME = ame;
            }
        }
    }
    
    //Used to update the "Last Updated" datetime when a user clicks the refresh button on the to-do list
    public String getCurrentDateTime(){
        return string.valueof(DateTime.now().format());
    }
    
    //Wrapper to hold/format information returned from the ListView API
    private class queryWrapper{
        String query {get;set;}
        String scope {get;set;}
    }
    
    
}