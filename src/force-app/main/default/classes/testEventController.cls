@isTest(SeeAllData=true)
public class testEventController {
    
    static testmethod void test1() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        //Insert our first task
        Event newEvent = new Event(
            subject='GM - Demo',
            whatId = a.id,
            whoId = newContact.Id,
            startDateTime = datetime.newInstance(2013, 4, 25, 11, 30, 2),
            endDateTime = datetime.newInstance(2013, 4, 25, 12, 30, 2)
        );
        insert newEvent;
        
        ApexPages.currentPage().getParameters().put('Id', newEvent.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Event());
        EventController myController = new eventController(testController);
        myController.rURL = '/a.Id';
        myController.who = newContact.Id;
        myController.save();
        myController.getSubjects();
        myController.getContactSelectList();
        User mark = [Select id from user where LastName = 'McCauley' LIMIT 1];
        newEvent.OwnerId = mark.id;
        update newEvent;
    }
    
    static testmethod void test2() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        
        ApexPages.currentPage().getParameters().put('what_id', a.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Event());
        EventController myController = new eventController(testController);
        myController.rURL = '/a.Id';
        myController.new_edit='New';
        myController.newEvent.WhatId = a.Id;
        myController.newEvent.StartDateTime = datetime.newInstance(2013, 4, 25, 11, 30, 2);
        myController.newEvent.EndDateTime = datetime.newInstance(2013, 4, 25, 12, 30, 2);
        myController.newEvent.WhoId = newContact.Id;
        myController.newEvent.DurationInMinutes = 60;
        myController.newEvent.Subject = 'HQ - Demo';
        myController.newEventSave();
        myController.setName();
        myController.changeEndDate();   
    }
    
    static testmethod void test3() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        
        ApexPages.currentPage().getParameters().put('what_id', a.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Event());
        EventController myController = new eventController(testController);
        myController.rURL = '/a.Id';
        myController.new_edit='New';
        myController.newEvent.WhatId = a.Id;
        myController.newEvent.StartDateTime = datetime.newInstance(2013, 4, 25, 11, 30, 2);
        myController.newEvent.EndDateTime = datetime.newInstance(2013, 4, 25, 12, 30, 2);
        myController.newEvent.WhoId = newContact.Id;
        myController.newEvent.DurationInMinutes = 60;
        myController.newEvent.Subject = 'HQ - Demo';
        myController.setName();
        myController.changeEndDate();   
    }
    
    static testmethod void test4() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        //Insert our first task
        Event newEvent = new Event(
            subject='GM - Demo',
            whatId = a.id,
            whoId = newContact.Id,
            startDateTime = datetime.newInstance(2013, 4, 25, 11, 30, 2),
            endDateTime = datetime.newInstance(2013, 4, 25, 12, 30, 2)
        );
        insert newEvent;
        
        ApexPages.currentPage().getParameters().put('Id', newEvent.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Event());
        EventController myController = new eventController(testController);
        myController.rURL = '/a.Id';
        myController.who = newContact.Id;
        myController.new_edit='Edit';
        myController.setName();
        myController.changeEndDate();   
        myController.saveNewEvent();
    }
    
    static testmethod void test5() {
        User u = [SELECT id FROM User WHERE Role__c = 'Associate' and Group__c = 'Mid-Market' and isActive = true LIMIT 1];
        System.runAs(u) {
            //Setup
            Account a = new Account(name='Test');
            insert a;
            
            Contact newContact = new Contact();
            newContact.FirstName = 'Bob';
            newContact.LastName = 'Bob';
            newContact.AccountID = a.Id;
            insert newContact;
            
            //Insert our first task
            Event newEvent = new Event(
                subject='HQ - Demo',
                whatId = a.id,
                whoId = newContact.Id,
                startDateTime = datetime.newInstance(2013, 4, 25, 11, 30, 2),
                endDateTime = datetime.newInstance(2013, 4, 25, 12, 30, 2),
                event_status__c = 'Completed'
            );
            insert newEvent;
            
            newEvent.event_status__c = 'Scheduled';
            update newEvent;
            
            newEvent.event_status__c = 'Completed';
            update newEvent;
            
            ApexPages.currentPage().getParameters().put('Id', newEvent.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(new Event());
            EventController myController = new eventController(testController);
            myController.rURL = '/a.Id';
            myController.who = newContact.Id;
            myController.new_edit='Edit';
            myController.setName();
            myController.changeEndDate();   
            myController.saveNew();
            
            delete newEvent;
        }
    }
    
    static testmethod void test6() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        insert newOpportunity;
        
        ApexPages.currentPage().getParameters().put('what_id', newOpportunity.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Event());
        EventController myController = new eventController(testController);
        myController.rURL = '/newOpportunity.Id';
        myController.new_edit='New';
        myController.newEvent.WhatId = newOpportunity.Id;
        myController.newEvent.StartDateTime = datetime.newInstance(2013, 4, 25, 11, 30, 2);
        myController.newEvent.EndDateTime = datetime.newInstance(2013, 4, 25, 12, 30, 2);
        myController.newEvent.WhoId = newContact.Id;
        myController.newEvent.DurationInMinutes = 60;
        myController.newEvent.Subject = 'HQ - Demo';
        myController.newEventSave();
        myController.setName();
        myController.changeEndDate();   
    } 
}