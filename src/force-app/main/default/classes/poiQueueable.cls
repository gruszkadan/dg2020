public class poiQueueable implements Queueable, Database.AllowsCallouts {
    public void execute(QueueableContext context) {
        string marketoData = marketoWebServices.marketoData('activities','25');
        jsonUtility j = new jsonUtility();
        poiActionUtility.poiAction[] poiActions = j.parseJSON(marketoData);
        poiActionInsert.createPOIActions(poiActions);
        system.debug('poiActionsSize '+poiActions.size());
        system.debug('marketoData '+marketoData);
    }
}