public class corpAcctRefController {
    private Apexpages.StandardController controller; 
    public User u {get;set;}
    public Corporate_Account_Referral__c ref {get;set;}
    public Account a {get;set;}
    public Id refId {get;set;}
    public Id acctId {get;set;}
    datetime myDateTime = datetime.now();
    public user director {get;set;}
    public EmailTemplate corpAccountsReferralEmail {get;set;}
    
    public corpAcctRefController(ApexPages.StandardController stdController) {
        this.controller = stdController;
        refId = ApexPages.currentPage().getParameters().get('id');
        acctId = ApexPages.currentPage().getParameters().get('acctId');
        if (acctId != null) {
            a = [ SELECT Id, Name, Corporate_Account_Approved_Referral_Reps__c FROM Account WHERE Id = :acctId LIMIT 1 ];
        }
        u = [ SELECT Id, Name, FirstName, LastName, Role__c, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, ProfileID from User where id =: UserInfo.getUserId() LIMIT 1 ];
        if (refId != null) {
            ref = [ SELECT Id, Account__c, Rep__c, Critical_Issues__c, CS_Amount_Quoted__c, HQ_Amount_Quoted__c, Date_Reviewed__c, Decision_Makers_Name__c, Demo_Date__c, Email__c, 
                   Phone__c, Referral_Type__c, Reviewer__c, Status__c, Account__r.Name, Rep__r.Name, Current_SDS_Method__c FROM Corporate_Account_Referral__c WHERE Id = :refId LIMIT 1 ];
        } else {
            ref = new Corporate_Account_Referral__c();
        }
        corpAccountsReferralEmail = [Select Id From EmailTemplate where Name='Corporate Accounts Referral Request'];
    }
    
    public PageReference submitCorpAccountReferral() {
        corpAccountReferralRequest();
        SendCorpAccountReferralEmail();
        PageReference account_detail = Page.account_detail;
        account_detail.getParameters().put('id',a.ID); 
        account_detail.getParameters().put('subCorp', 'true');
        account_detail.setRedirect(true);
        return account_detail; 
    }
    
    public void corpAccountReferralRequest() {
        ref.Rep__c = u.Id;
        ref.Status__c = 'Pending';
        ref.Account__c = a.Id;
        insert ref;
    }
    
    public void SendCorpAccountReferralEmail() {
        if (!test.isRunningTest()) {
            director = [SELECT Id, Email FROM User WHERE isActive = TRUE AND Role__c = 'Director' AND LastName = 'Jackson' LIMIT 1];
        }
        if (test.isRunningTest()) {
            director = [SELECT Id, Email FROM User WHERE Name = 'Ryan Werner' LIMIT 1];
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.settargetObjectId( director.Id );
        mail.setWhatId(ref.Id);
        mail.setTemplateId(corpAccountsReferralEmail.Id);
        mail.setSaveAsActivity(FALSE);
        mail.setUseSignature(FALSE);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public PageReference referralCancel() {
        PageReference account_detail = Page.account_detail;
        account_detail.setRedirect(true);
        account_detail.getParameters().put('id',a.Id); 
        return account_detail;          
    }
    
    public PageReference reload() {
        PageReference reload = Page.account_corp_approval;
        reload.setRedirect(true);
        reload.getParameters().put('ID',a.Id);
        return reload;
    }
    public PageReference approveCorpAccountReferral() {
        this.controller.save();
        corpAccountReferralApproval();
        PageReference account_detail = Page.account_detail;
        account_detail.setRedirect(true);
        account_detail.getParameters().put('id',a.ID); 
        return null; 
    }
    
    public void corpAccountReferralApproval() {
        try {
            ref.Status__c = 'Approved';
            ref.Reviewer__c = u.Id;
            ref.Date_Reviewed__c = myDateTime;
            
            if (a.Corporate_Account_Approved_Referral_Reps__c != null) {
                a.Corporate_Account_Approved_Referral_Reps__c = a.Corporate_Account_Approved_Referral_Reps__c+', '+ref.Rep__r.Name;
                update a;
            }
            if (a.Corporate_Account_Approved_Referral_Reps__c == null) {
                a.Corporate_Account_Approved_Referral_Reps__c = ref.Rep__r.Name;
                update a;
            }
            update ref;
        }
        catch(Exception e) {
            ApexPages.addMessages(e);
        }
    } 
    
    public PageReference rejectCorpAccountReferral() {
        this.controller.save();
        corpAccountReferralRejection();
        PageReference account_detail = Page.account_detail;
        account_detail.setRedirect(true);
        account_detail.getParameters().put('id',a.ID); 
        return null; 
    }
    
    public void corpAccountReferralRejection() {
        try {
            ref.Status__c = 'Rejected';
            ref.Reviewer__c = u.Id;
            ref.Date_Reviewed__c = myDateTime;
            update ref;
        }
        catch(Exception e) {
            ApexPages.addMessages(e);
        }
    } 
}