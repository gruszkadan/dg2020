public class QuoteProductApprovalHistory {
    public String productId {get;set;}
    public List<ProcessInstanceHistory> getApprovalSteps() {
      if (productId != null) {
        Quote_Product__c p = [Select Id, (Select TargetObjectId, SystemModstamp, StepStatus, RemindersSent, ProcessInstanceId, OriginalActorId, IsPending, IsDeleted, Id, CreatedDate, CreatedById, Comments, ActorId From ProcessSteps order by SystemModstamp desc) from Quote_Product__c where Id = :productId];
        return p.ProcessSteps;
      }
      return new List<ProcessInstanceHistory> ();
    }
}