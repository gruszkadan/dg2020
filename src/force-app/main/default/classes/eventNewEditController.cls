public class eventNewEditController {
    public eventNewEditController(ApexPages.StandardController stdController){
    }
    
    public PageReference redirect(){
        if(System.FeatureManagement.checkPermission('Custom_Activity_Page')){
            return null;
        }else{
            String rURL = ApexPages.currentPage().getParameters().get('retURL');
            String whatID = ApexPages.currentPage().getParameters().get('whatid');
            if(whatId == null){
                whatID = ApexPages.currentPage().getParameters().get('what_id');
            }
            String recordId = ApexPages.currentPage().getParameters().get('id');
            String whoID = ApexPages.currentPage().getParameters().get('who_id');
            String urlString = '';
            urlString += '/00U/e?nooverride=1';
            if(recordId != null){
                urlString += '&id='+recordId; 
            }
            if(whatId != null){
                urlString += '&what_id='+whatId; 
            }
            if(whoId != null){
                urlString += '&who_id='+whoId; 
            }
            if(rURL != null){
                urlString += '&saveURL='+ rURL + '&cancelURL=' + rURL;
            }
            return new PageReference(urlString).setRedirect(true);
        }
    }
    
}