public class quoteApproval {
    Quote_Product__c[] qps;
    public User u {get;set;}
    
    /*************** SALES ***************/
    
    public Quote_Product__c[] salesProductCheck(Quote_Product__c[] xqps, boolean isStep4) {
        qps = xqps;
        
        u = [SELECT id, Department__c, Product_Group__c, Suite_of_Interest__c FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
        
        //find the approval limits
        salesApprovalLimits();
        
        //if theres any special approval criteria, put it here
        specialApprovalCriteria(isStep4);
        return qps;
        
        
    }
    
    public void salesApprovalLimits() {
        for (Quote_Product__c qp : qps) {
            
            //initially set all limits to 0
            decimal appLimit = 0.00;
            decimal dirAppLimit = 0.00;
            decimal vpAppLimit = 0.00;
            
            //find the approval limit on the product - this is copied straight from the workflows
            if (qp.CreatedBy.Department__c == 'Customer Care' && qp.CreatedBy.Group__c != 'Retention') {
                appLimit = qp.Product__r.Sales_Executive_Approval__c;
            } else {
                if (qp.CreatedBy.Role__c == 'Online Training Sales Executive' || qp.CreatedBy.Role__c == 'IM Account Manager') {
                    appLimit = qp.Product__r.Online_Training_Approval__c;
                }
                if (qp.CreatedBy.Role__c == 'Associate' || qp.CreatedBy.Role__c == 'Trainee') {
                    appLimit = qp.Product__r.Associate_Approval__c;
                }
                if (qp.CreatedBy.Role__c == 'Authoring Sales Executive') {
                    appLimit = qp.Product__r.Authoring_Sales_Executive_Approval__c;
                }
                if (qp.CreatedBy.Role__c == 'Enterprise Sales Representative' || qp.CreatedBy.Role__c == 'EHS Enterprise Sales') {
                    appLimit = qp.Product__r.Enterprise_Sales_Rep_Approval__c;
                }
                if (qp.CreatedBy.Role__c == 'Sales Executive' || qp.CreatedBy.Role__c == 'EHS Sales Executive') {
                    appLimit = qp.Product__r.Sales_Executive_Approval__c;
                }
                if (qp.CreatedBy.Role__c == 'Territory Sales Executive') {
                    appLimit = qp.Product__r.Senior_Sales_Executive_Approval__c;
                }
                if (qp.CreatedBy.Role__c == 'Territory Account Manager') {
                    appLimit = qp.Product__r.Team_Leader_Approval__c;
                }
                //if it didnt fall into a filter use sales executive approval
                if (appLimit == 0.00) {
                    appLimit = qp.Product__r.Sales_Executive_Approval__c;
                }
            }
            
            //find the director approval limit
            dirAppLimit = qp.Product__r.Director_Approval__c;
            
            //find the vp approval limit
            vpAppLimit = qp.Product__r.VP_Approval__c;
            
            //set the limits
            qp.Approval_Limit__c = appLimit;
            qp.Director_Approval_Limit__c = dirAppLimit;
            qp.VP_Approval_Limit__c = vpAppLimit;
        }
    }
    
    public void specialApprovalCriteria(boolean isStep4) {
        boolean activeGM = findActiveGM();
        for (Quote_Product__c qp : qps) {
            if (activeGM) {
                if (qp.Product__r.Sales_Product_Grouping__c == 'Compliance Services') {
                    qp.Approval_Limit__c = 0;
                    qp.Director_Approval_Limit__c = 0;
                    qp.Special_Approval_Criteria__c = 'A GM Account has been found - any discount on Compliance Services products will need director approval';
                }
            } 
            else {
                if (qp.Product__r.Sales_Product_Grouping__c == 'Compliance Services') {
                    qp.Special_Approval_Criteria__c = null;   
                }
            }
            //Indexing Languages Special Approval
            if(qp.Indexing_Language__c != null &&
               qp.Product__r.Languages_Requiring_Approval__c != null)
            {
                set<String> languages = new set<String>();
                languages.addAll(qp.Product__r.Languages_Requiring_Approval__c.split(';'));
                for(string l:languages){
                    if(qp.Indexing_Language__c == l){
                        qp.Approval_Required_By__c = 'Compliance Services';
                    }
                }
            }
            
            
            if (isStep4) {
                
                
                //Customer Success Special Approval
                if(u.Department__c == 'Customer Care' && u.Suite_of_Interest__c != 'EHS Management' &&  u.Product_Group__c != '' && u.Product_Group__c != null 
                   && qp.Product__r.Product_Group__c != null && qp.Product__r.Product_Group__c.Contains(u.Product_Group__c)){
                       
                       qp.Approval_Required_By__c = 'N/A';
                       
                       if (qp.Year_1__c){
                           if(qp.Name__c.startsWith('Additional Site Administrator') && qp.Y1_Quote_Price__c != null && qp.Y1_Quote_Price__c < 100){
                               qp.Approval_Required_By__c = 'Customer Success Director';
                           } 
                           else if(!qp.Name__c.startsWith('Additional Site Administrator') && qp.Y1_Discount__c > 0){
                               qp.Approval_Required_By__c = 'Customer Success Director';
                           }
                           
                       }
                       
                       if (qp.Year_2__c){
                           if(qp.Name__c.startsWith('Additional Site Administrator') && qp.Y2_Quote_Price__c != null && qp.Y2_Quote_Price__c < 100){
                               qp.Approval_Required_By__c = 'Customer Success Director';
                           } 
                           else  if(!qp.Name__c.startsWith('Additional Site Administrator') && qp.Y2_Discount__c > 0){
                               qp.Approval_Required_By__c = 'Customer Success Director';
                           }
                           
                       }
                       
                       if (qp.Year_3__c){
                           if(qp.Name__c.startsWith('Additional Site Administrator') && qp.Y3_Quote_Price__c != null && qp.Y3_Quote_Price__c < 100){
                               qp.Approval_Required_By__c = 'Customer Success Director';
                           } 
                           else if(!qp.Name__c.startsWith('Additional Site Administrator') && qp.Y3_Discount__c > 0){
                               qp.Approval_Required_By__c = 'Customer Success Director';
                           }
                           
                       }
                       
                       if (qp.Year_4__c){
                           if(qp.Name__c.startsWith('Additional Site Administrator') && qp.Y4_Quote_Price__c != null && qp.Y4_Quote_Price__c < 100){
                               qp.Approval_Required_By__c = 'Customer Success Director';
                           } 
                           else if((!qp.Name__c.startsWith('Additional Site Administrator')) && qp.Y4_Discount__c > 0){
                               qp.Approval_Required_By__c = 'Customer Success Director';
                           }
                           
                       }
                       
                       if (qp.Year_5__c){
                           if(qp.Name__c.startsWith('Additional Site Administrator') && qp.Y5_Quote_Price__c != null && qp.Y5_Quote_Price__c < 100){
                               qp.Approval_Required_By__c = 'Customer Success Director';
                           }
                           else if(!qp.Name__c.startsWith('Additional Site Administrator') && qp.Y5_Discount__c > 0){
                               qp.Approval_Required_By__c = 'Customer Success Director';
                           }
                           
                       }
                       
                   }
                
                //AUTHORING SPECIAL APPROVAL
                if (qp.Name__c.startsWith('Authoring')) { 
                    if (qp.Y1_List_Price__c == null) {
                        qp.Approval_Required_By__c = 'Ivy Mattio';
                    } else {
                        if (qp.Approval_Required_By__c == 'Ivy Mattio') {
                            qp.Approval_Required_By__c = 'N/A';
                        }
                    }
                }
                //EHS Products Special Approval - Director & Ops Director
                if(qp.Name__c == 'Custom Integration'||
                   qp.Name__c == 'Implementation Consulting'||
                   qp.Name__c == 'Administration (System Admin, Data Entry)'||
                   qp.Name__c == 'Infrastructure (IT)'||
                   qp.Name__c == 'Technical Services'){
                       qp.Approval_Required_By__c = 'EHS Director, EHS Operations Director';
                   }
                //Product Types Approvals 
                if(qp.Name__c == 'Data Import' || qp.Name__c == 'Data Export'){
                    for(String s:qp.Product__r.Product_Types_Requiring_Approval__c.split(';')){
                        if(qp.Product_Types__c.contains(s)){
                            qp.Approval_Required_By__c = 'Sales Manager';
                        }
                    }
                }
                
                //EHS Products Special Approval - Ops Director Only
                if(qp.Name__c == 'Customer Care Services' ||
                   qp.Name__c == 'Resource Expenses'){
                       qp.Approval_Required_By__c = 'EHS Operations Director';
                   }
                //Rush Projects Special Approval
                if(qp.Rush__c == 'Yes'){
                    qp.Approval_Required_By__c = 'Compliance Services';
                }
                
                
                //ERS Discount Approvals
                if(qp.Product__r.ERS_Discount_Approval__c != null && qp.Y1_Discount__c > qp.Product__r.ERS_Discount_Approval__c){
                    qp.Approval_Required_By__c = 'ERS Discount';
                }
                
                //ERS APPROVALS HERE
                if(qp.Product__r.Emergency_Response_Approval__c ||
                   ((qp.Name__c.contains('Emergency Response Services') || qp.Name__c.contains('SDS Rapid Access')) && (qp.ERS_Dedicated_800_or_Country_Phone__c == 'Yes' || qp.ERS_Custom_Script__c == 'Yes'))){
                       qp.Approval_Required_By__c = 'Emergency Response';
                   }
                
                //Minimum Price Approvals
                /*if(qp.Minimum_Price_Applied__c && qp.Y1_Discount__c != null && qp.Y1_Discount__c > 0){
qp.Approval_Required_By__c = 'Sales Director';
qp.Additional_Approval_Reasons__c = 'Discount below minimum price of $'+qp.Y1_List_Price__c;
}*/

            }
        }
        
        
        
        
    }
    
    public Quote_Product__c findSalesApprovers(Quote_Product__c qp) {
        
        if(qp.CreatedBy.Product_Group__c == null){
            
            
            //init vars to hold the max discount, the promo discount, and the discounts for each year
            decimal maxDisc = 0.00;
            decimal y1Disc = 0.00;
            decimal y2Disc = 0.00;
            decimal y3Disc = 0.00;
            decimal y4Disc = 0.00;
            decimal y5Disc = 0.00;
            decimal promoDisc = 0.00;
            
            //set the promodisc to the promo discount percent
            if (qp.Promo_Discount_Percent__c != null) {
                promoDisc = qp.Promo_Discount_Percent__c.setScale(2);
            }
            
            //for year 1
            if (qp.Year_1__c) {
                
                //if theres a promo and the discount doesnt equal the promo discount, set y1 discount to the actual user discount
                if (qp.Promo_Y1__c) {
                    if (qp.Y1_Discount__c.setScale(2) != promoDisc) {
                        y1Disc = qp.Y1_Discount__c.setScale(2);
                    }
                } else {
                    //otherwise just set the discount normally
                    y1Disc = qp.Y1_Discount__c;
                }
            }
            //year 2
            if (qp.Year_2__c) {
                if (qp.Promo_Y2__c) {
                    if (qp.Y2_Discount__c.setScale(2) != promoDisc) {
                        y2Disc = qp.Y2_Discount__c.setScale(2);
                    }
                } else {
                    y2Disc = qp.Y2_Discount__c;
                }
            }
            //year 3
            if (qp.Year_3__c) {
                if (qp.Promo_Y3__c) {
                    if (qp.Y3_Discount__c.setScale(2) != promoDisc) {
                        y3Disc = qp.Y3_Discount__c.setScale(2);
                    }
                } else {
                    y3Disc = qp.Y3_Discount__c;
                }
            }
            //year 4
            if (qp.Year_4__c) {
                if (qp.Promo_Y4__c) {
                    if (qp.Y4_Discount__c.setScale(2) != promoDisc) {
                        y4Disc = qp.Y4_Discount__c.setScale(2);
                    }
                } else {
                    y4Disc = qp.Y4_Discount__c;
                }
            }
            //year 5
            if (qp.Year_5__c) {
                if (qp.Promo_Y5__c) {
                    if (qp.Y5_Discount__c.setScale(2) != promoDisc) {
                        y5Disc = qp.Y5_Discount__c.setScale(2);
                    }
                } else {
                    y5Disc = qp.Y5_Discount__c;
                }
            }
            
            //initially well set the max disc to the year 1 disc
            maxDisc = y1Disc;
            
            //if y2 disc > max disc, set the max disc to y2disc
            if (y2Disc > maxDisc) {
                maxDisc = y2Disc;
            }
            if (y3Disc > maxDisc) {
                maxDisc = y3Disc;
            }
            if (y4Disc > maxDisc) {
                maxDisc = y4Disc;
            }
            if (y5Disc > maxDisc) {
                maxDisc = y5Disc;
            }
            
            //if there is a max discount, round and remove the extra digits
            if (maxDisc != null) {
                maxDisc = maxDisc.setScale(2);
            }
            
            //this is replacing the workflow rules to determine who needs to approve the product
            if (qp.Quote__r.Department__c == 'Customer Care') {
                if (maxDisc > qp.Approval_Limit__c) {
                    qp.Approval_Required_by__c = 'Sales Manager';
                }

            } 
            else {
                if (qp.Product__r.ODT__c) {
                    if (qp.Product__r.Online_Training_Approval__c == 10 && maxDisc > 10) {
                        qp.Approval_Required_by__c = 'Nick Lipnisky';
                    }
                    if (qp.Product__r.Online_Training_Approval__c == 0 && maxDisc > 0.00) {
                        qp.Approval_Required_by__c = 'Nick Lipnisky';
                    }
                } 
                else {
                    if ((qp.Name__c.startsWith('Enviro Tool Kit') || qp.Name__c.startsWith('Safety Tool Kit')) && maxDisc > 0.00) {
                        qp.Approval_Required_by__c = 'Nick Lipnisky';
                    } 
                    else {
                        
                        // look at product lines and look for a family
                        string family = '';
                        if (qp.Product__r.Family != null) {
                            if (qp.Product__r.Family.startsWith('EH&S')) {
                                family = 'EH&S';
                            }
                        }
                        
                        // ------------ EHS
                        
                        if (family == 'EH&S') {
                            if (maxDisc > qp.Approval_Limit__c) {
                                if (maxDisc > qp.Director_Approval_Limit__c) {
                                    if (maxDisc > qp.VP_Approval_Limit__c) {
                                        // If the discount is more than the director and VP limit, assign to director and president
                                        qp.Approval_Required_By__c = 'EHS Director, EHS President';
                                    } else {
                                        qp.Approval_Required_By__c = 'EHS Director';
                                        if (qp.Product__r.EHS_Operations_Director_Approval__c) {
                                            qp.Approval_Required_by__c = qp.Approval_Required_by__c+', EHS Operations Director';
                                        }
                                    }
                                }
                            } else {
                                qp.Approval_Required_by__c = 'N/A';
                            }
                        } 
                        
                        // ------------ /EHS 
                        
                        // ------------ Standard
                        else {
                            if (maxDisc > qp.Approval_Limit__c) {
                                if (maxDisc >= qp.Director_Approval_Limit__c || qp.Director_Approval_Limit__c == null) {
                                    if (maxDisc >= qp.VP_Approval_Limit__c) {
                                        qp.Approval_Required_by__c = 'Sales VP';
                                    } else if (maxDisc >= qp.Director_Approval_Limit__c) {
                                        qp.Approval_Required_by__c = 'Sales Director';
                                    } else {
                                        qp.Approval_Required_by__c = 'Sales Manager';
                                    }
                                } else {
                                    qp.Approval_Required_by__c = 'Sales Manager';
                                }
                            } else {
                                qp.Approval_Required_by__c = 'N/A';
                            }
                        }
                        // ------------ /Standard
                    }
                    
                }
            }
        }
        return qp;
    }
    
    
    public boolean findActiveGM() {
        string activeMSDSprods = qps[0].Quote__r.Account__r.Active_MSDS_Management_Products__c;
        boolean activeGM = false;
        if (activeMSDSprods != null) {
            for (string s : activeMSDSprods.split(';')) {
                if (s == 'GM' || s == 'GM Pro' || s == 'GM Upgrade') {
                    activeGM = true;
                }
            }
        }
        //if non is found on the account, loop and find a GM product
        if (!activeGM) {
            for (Quote_Product__c qp : qps) {
                if (qp.Name__c == 'GM Account' || qp.Name__c == 'GM Pro') {
                    activeGM = true;
                }
            }
        }
        return activeGM;
    }
    
    
    /*************** /SALES ***************/
    
    
    
    /*************** RETENTION ***************/
    
    // step 1 - first we look at the product itself and see if that flags approval
    public Quote_Product__c[] retentionProductApproval(Quote_Product__c[] xqps) {
        
        // set the list of products so we can use it elsewhere in this class
        qps = xqps;
        
        // loop through our products
        for (Quote_Product__c qp : qps) {
            
            //if the product is pending approval, we dont want to make any changes
            if (qp.Approval_Status__c != 'Pending') {
                
                // if there is an approved price, we know the product already has been approved. we don't need to ask for product based approval again
                if (qp.Approved_Price__c == null) {
                    
                    // sets to hold the approvers and reasons
                    set<string> approvers = new set<string>();
                    set<string> appReasons = new set<string>();
                    
                    // look for approval criteria
                    if (qp.Name__c.startsWith('Update Services')) {
                        approvers.add('Services');
                        appReasons.add('Update Services added to retention quote');
                    }
                    
                    if (qp.Name__c == 'Custom Services Project') {
                        approvers.add('Services');  
                        appReasons.add('Custom Services Project added to retention quote');
                    }
                    
                    if (qp.Name__c == 'Addl Licensing Fees' && qp.Type__c == 'New') {
                        approvers.add('Manager');    
                        appReasons.add('New Additional Licensing added to retention quote');
                    }
                    
                    if (qp.Name__c == 'Custom Label') {
                        approvers.add('Manager');
                        appReasons.add('Custom Label added to retention quote');
                    }
                    
                    if (qp.Product__r.ODT__c == true) {
                        approvers.add('Manager');
                        appReasons.add('ODT added to retention quote');
                    }
                    
                    if (qp.Name__c == 'Webpliance') {
                        approvers.add('Retention');
                        appReasons.add('Webpliance added to retention quote');
                    }
                    
                    if (qp.Name__c.contains('PPI')) {
                        approvers.add('Retention');
                        appReasons.add('PPI added to retention quote');
                    }
                    
                    if (qp.Name__c == 'Spill Center') {
                        approvers.add('Cassandra Minutillo');
                        appReasons.add('Spill Center added to retention quote');
                    }
                    
                    // set approval required by
                    qp.Approval_Required_by__c = '';
                    
                    // if approvers are found, mark the product as "not submitted" and set the approval required by field
                    if (approvers.size() > 0) {
                        qp.Approval_Status__c = 'Not Submitted';
                        for (string approver : approvers) {
                            qp.Approval_Required_by__c += approver+', ';
                        }
                    } else {
                        // if there are no approvers, set approval required by to "n/a"
                        qp.Approval_Required_by__c = 'N/A';
                        
                        // if there is an approved price, this product has already been approved, so set the status back to approved
                        if (qp.Approved_Price__c != null) {
                            qp.Approval_Status__c = 'Approved';
                            
                        } else {
                            //otherwise the status should be "not required"
                            qp.Approval_Status__c = 'Not Required';
                        }
                    }
                    
                    // set the approval required by on the prod
                    qp.Approval_Required_by__c = qp.Approval_Required_by__c.removeEnd(', ');
                    
                    // set approval reasons
                    qp.Additional_Approval_Reasons__c = '';
                    
                    // if reasons are found, set the additional approval reasons field to the reasons for approval
                    if (appReasons.size() > 0) {
                        for (string reason : appReasons) {
                            qp.Additional_Approval_Reasons__c += reason+', ';
                        }
                        qp.Additional_Approval_Reasons__c = qp.Additional_Approval_Reasons__c.removeEnd(', ');
                        
                    } else {
                        // otherwise null out the field as there are no approval reasons
                        qp.Additional_Approval_Reasons__c = null;
                    }
                    
                } else {
                    
                    // if the approved price isnt null, make sure to reset the fields
                    qp.Approval_Required_By__c = 'N/A';
                    qp.Additional_Approval_Reasons__c = null;
                }
                
                // step 1 is complete, now move to step 2 to look at rep discounts
                retentionDiscountApproval(qp);
            }
        }
        // return the list of qps with the approval info
        return qps;
    }
    
    // step 2 - look at the product's quoted price and see if there is a discount and whether or not it needs approval
    public Quote_Product__c retentionDiscountApproval(Quote_Product__c qp) {
        
        // when a quote product is approved, the price it was approved at is saved. if the product hasnt gone for approval, Approved_Price__c will be null and the discount check will run. if its not null, we know 
        //the product has already been approved, so if the total value doesn't equal the approved price the rep must have changed the pricing after it's been approved. we need to run the approval check again
        if (qp.Total_Value__c != qp.Approved_Price__c) {
            
            // find the annual quoted price - this is the price we will use to compare to the list price. if year 1 is selected, we will use that price. if not, check year 2. if not year 2, check year 3, etc...
            //NEW products may not have any quoted price
            decimal quotedPrice = 0.00;
            if (qp.Year_1__c) {
                quotedPrice = qp.Y1_Quote_Price__c;
            } else if (qp.Year_2__c) {
                quotedPrice = qp.Y2_Quote_Price__c;
            } else if (qp.Year_3__c) {
                quotedPrice = qp.Y3_Quote_Price__c;
            } else if (qp.Year_4__c) {
                quotedPrice = qp.Y4_Quote_Price__c;
            } else if (qp.Year_5__c) {
                quotedPrice = qp.Y5_Quote_Price__c;
            }
            
            // if the quoted price isnt null, we want to check the discount. if it is null, the rep hasnt entered in the amounts and there will be nothing to discount OR
            //its a NEW product
            if (quotedPrice != null || qp.Type__c == 'New') {
                
                // the quoted price is per unit, so if there is quantity calculation we need to find the total quoted price w/ quantity
                if (qp.Quantity_Calculation__c && quotedPrice != null) {
                    quotedPrice = quotedPrice * qp.Quantity__c;
                }
                
                // create the set for approvers. if there already is an approver needed from step 1, add it to the list
                set<string> approvers = new set<string>();
                if (qp.Approval_Required_by__c != null) {
                    for (string app : qp.Approval_Required_by__c.split(', ')) {
                        if (app != 'N/A') {
                            approvers.add(app);
                        }
                    } 
                }
                
                // create a set of reasons. if there already are reasons from step 1, add them to the list
                set<string> appReasons = new set<string>();
                if (qp.Additional_Approval_Reasons__c != null && qp.Additional_Approval_Reasons__c != '' ) {
                    for (string ar : qp.Additional_Approval_Reasons__c.split(', ')) {
                        appReasons.add(ar);
                    }
                }
                
                // look for approval criteria
                if (qp.Name__c.startsWith('Update Services') && qp.Max_Discount_Amount__c > 0) {
                    approvers.add('Manager');
                    appReasons.add('Update Services discounted > 0% on retention quote');
                }
                
                if (qp.Max_Discount_Amount__c > 50 && !qp.Name__c.contains('Site Administrators')) {
                    approvers.add('Manager');
                    appReasons.add('Product discounted > 50% on retention quote');
                }
                
                if (quotedPrice < qp.Renewal_Amount__c && qp.Type__c == 'Renewal') {
                    decimal discPct = (((qp.Renewal_Amount__c - quotedPrice) / qp.Renewal_Amount__c).setScale(2)) * 100;
                    decimal discAmt = qp.Renewal_Amount__c - quotedPrice;
                    if (discPct > 10.00) {
                        approvers.add('Manager');
                        appReasons.add('Product discounted > 10% from previous year\'s renewal amount');
                        
                    }
                    if (discAmt > 2000.00) {
                        approvers.add('Manager');
                        appReasons.add('Product discounted > $2000 from previous year\'s renewal amount');
                    }
                }
                
                if (qp.Product__r.Sales_Product_Grouping__c == 'Compliance Services' && qp.Type__c == 'New' && qp.Total_Value__c > 2000.00) {
                    approvers.add('Manager');
                    appReasons.add('New Compliance Services product > $2000');
                }
                
                if ((qp.Name__c.startsWith('HQ Account') || qp.Name__c.startsWith('HQ RegXR Account')) && qp.Type__c == 'Renewal') {
                    decimal hqBracketPrice = [SELECT Price_Break_1__c FROM PricebookEntry WHERE Name = : qp.Name__c AND Pricebook2Id = : qp.Quote__r.Price_Book__c LIMIT 1].Price_Break_1__c;
                    if (hqBracketPrice != null) {
                        if (quotedPrice < hqBracketPrice && (quotedPrice > (qp.Renewal_Amount__c * 1.35))) {
                            approvers.add('Manager');
                            appReasons.add('HQ product annual price is < lowest HQ bracket and is 35% greater than the previous year\'s price');
                        }                       
                        if (quotedPrice > hqBracketPrice && (quotedPrice > (qp.Renewal_Amount__c * 1.25))){
                            approvers.add('Manager');
                            appReasons.add('HQ product annual price is > lowest HQ bracket and is 25% greater than the previous year\'s price');
                        }
                    }
                }
                
                // set the approvers
                qp.Approval_Required_by__c = '';
                if (approvers.size() > 0) {
                    qp.Approval_Status__c = 'Not Submitted';
                    for (string approver : approvers) {
                        qp.Approval_Required_by__c += approver+', ';
                    }
                } else {
                    qp.Approval_Required_by__c = 'N/A';
                    qp.Additional_Approval_Reasons__c = null;
                    if (qp.Approved_Price__c != null) {
                        qp.Approval_Status__c = 'Approved';
                    } else {
                        qp.Approval_Status__c = 'Not Required';
                    }
                }
                qp.Approval_Required_by__c = qp.Approval_Required_by__c.removeEnd(', ');
                
                // set approval reasons
                qp.Additional_Approval_Reasons__c = '';
                if (appReasons.size() > 0) {
                    for (string ar : appReasons) {
                        qp.Additional_Approval_Reasons__c += ar+', ';
                    }
                }
                qp.Additional_Approval_Reasons__c = qp.Additional_Approval_Reasons__c.removeEnd(', ');
            }
        } else {
            qp.Approval_Required_by__c = 'N/A';
            qp.Additional_Approval_Reasons__c = null;
            if (qp.Approved_Price__c != null) {
                qp.Approval_Status__c = 'Approved';
            } else {
                qp.Approval_Status__c = 'Not Required';
            }
        }
        
        return qp;
    }
    
    /*************** /RETENTION ***************/
    
    
    
    
    
    
}