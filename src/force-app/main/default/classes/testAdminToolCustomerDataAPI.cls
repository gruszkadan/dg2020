@isTest(seeAllData=false)
private class testAdminToolCustomerDataAPI {
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';

    static testmethod void testAdminToolCustomerData() {
        //This test covers adminToolCustomerApiQueueable
        Test.startTest();
        Round_Robin_Assignment__c ergorr = new Round_Robin_Assignment__c();
        ergorr.Order__c = 1;
        ergorr.Type__c = 'Ergonomics';
        ergorr.UserID__c = UserInfo.getUserId();
        ergorr.Name = UserInfo.getUserName();
        insert ergorr;
        
        //Insert Custom Setting
        Admin_Tool_API_Settings__c at = new Admin_Tool_API_Settings__c();
        at.Name = 'Production';
        at.Username__c = 'testtest';
        at.Password__c = 'lalala';
        at.Environment__c = 'Production';
        at.Endpoint_Bookings_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/booking_report';
        at.Endpoint_Customer_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report';
        at.Endpoint_Customer_Data_Report_8_5__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report_85';
        at.Endpoint_DFU_Export__c = 'https://intranet.msdsonline.com/api/v1/reports/dfu_data_report';
        at.Endpoint_First_Responders_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/first_responder_report';
        at.Endpoint_SalesforceID__c = 'https://intranet.msdsonline.com/api/v1/reports/salesforce_id_report';
        at.Endpoint_User_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/user_report';
        insert at;
        
        Admin_Tool_API_Settings__c at1 = new Admin_Tool_API_Settings__c();
        at1.Name = 'Staging';
        at1.Username__c = 'testtest';
        at1.Password__c = 'lalala';
        at1.Environment__c = 'Staging';
        at1.Endpoint_Bookings_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/booking_report';
        at1.Endpoint_Customer_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report';
        at1.Endpoint_Customer_Data_Report_8_5__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report_85';
        at1.Endpoint_DFU_Export__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/dfu_data_report';
        at1.Endpoint_First_Responders_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/first_responder_report';
        at1.Endpoint_SalesforceID__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/salesforce_id_report';
        at1.Endpoint_User_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/user_report';
        insert at1;        

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new adminToolWebServicesMock());
        System.enqueueJob(new adminToolCustomerApiQueueable());
        Test.stopTest();
        System.enqueueJob(new adminToolCustomerApiQueueable85());
        List<Account> a = [Select ID from Account where AdminID__c in ('410602','410316')];
        System.assertEquals(2,a.size());

        VPM_Project__c v = new VPM_Project__c();
        v.Type__c ='Chemical Management Upgrade';
        v.account__c = [Select ID from Account where AdminID__c = '410602' LIMIT 1].id;
        insert v;
        Test.setMock(HttpCalloutMock.class, new adminToolWebServicesMock());
        System.enqueueJob(new adminToolCustomerApiQueueable());
        
        System.enqueueJob(new adminToolCustomerApiQueueable85());


                             
    }
    
    
    static testmethod void testAdminToolNewUserData() {
        //This test covers adminToolUserApiQueueable
        Test.startTest();
        //Insert Custom Setting
        Admin_Tool_API_Settings__c at = new Admin_Tool_API_Settings__c();
        at.Name = 'Production';
        at.Username__c = 'testtest';
        at.Password__c = 'lalala';
        at.Environment__c = 'Production';
        at.Endpoint_Bookings_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/booking_report';
        at.Endpoint_Customer_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report';
        at.Endpoint_Customer_Data_Report_8_5__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report_85';
        at.Endpoint_DFU_Export__c = 'https://intranet.msdsonline.com/api/v1/reports/dfu_data_report';
        at.Endpoint_First_Responders_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/first_responder_report';
        at.Endpoint_SalesforceID__c = 'https://intranet.msdsonline.com/api/v1/reports/salesforce_id_report';
        at.Endpoint_User_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/user_report';
        insert at;
        
        Admin_Tool_API_Settings__c at1 = new Admin_Tool_API_Settings__c();
        at1.Name = 'Staging';
        at1.Username__c = 'testtest';
        at1.Password__c = 'lalala';
        at1.Environment__c = 'Staging';
        at1.Endpoint_Bookings_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/booking_report';
        at1.Endpoint_Customer_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report';
        at1.Endpoint_Customer_Data_Report_8_5__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report_85';
        at1.Endpoint_DFU_Export__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/dfu_data_report';
        at1.Endpoint_First_Responders_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/first_responder_report';
        at1.Endpoint_SalesforceID__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/salesforce_id_report';
        at1.Endpoint_User_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/user_report';
        insert at1;
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.AdminID__c ='410602';
        insert newAccount;
        
        Account newAccount1 = new Account();
        newAccount1.Name = 'Test Account';
        newAccount1.Customer_Status__c ='Active';
        newAccount1.AdminID__c ='410316';
        insert newAccount1;
        
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new adminToolWebServicesMock());
        System.enqueueJob(new adminToolUserApiQueueable());
        Test.stopTest();
        List<Contact> pa = [Select ID from Contact where CustomerAdministratorId__c in ('410602','410316')];
        List<Contact> all = [Select ID, Name from Contact where Account.AdminID__C in ('410602','410316')];
        System.assertEquals(2,pa.size());
        System.assertEquals(3,all.size());
    }
    
    static testmethod void testAdminToolFoundUserData() {
        //This test covers adminToolUserApiQueueable
        Test.startTest();
        Admin_Tool_API_Settings__c at = new Admin_Tool_API_Settings__c();
        at.Name = 'Production';
        at.Username__c = 'testtest';
        at.Password__c = 'lalala';
        at.Environment__c = 'Production';
        at.Endpoint_Bookings_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/booking_report';
        at.Endpoint_Customer_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report';
        at.Endpoint_Customer_Data_Report_8_5__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report_85';
        at.Endpoint_DFU_Export__c = 'https://intranet.msdsonline.com/api/v1/reports/dfu_data_report';
        at.Endpoint_First_Responders_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/first_responder_report';
        at.Endpoint_SalesforceID__c = 'https://intranet.msdsonline.com/api/v1/reports/salesforce_id_report';
        at.Endpoint_User_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/user_report';
        insert at;
        
        Admin_Tool_API_Settings__c at1 = new Admin_Tool_API_Settings__c();
        at1.Name = 'Staging';
        at1.Username__c = 'testtest';
        at1.Password__c = 'lalala';
        at1.Environment__c = 'Staging';
        at1.Endpoint_Bookings_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/booking_report';
        at1.Endpoint_Customer_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report';
        at1.Endpoint_Customer_Data_Report_8_5__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report_85';
        at1.Endpoint_DFU_Export__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/dfu_data_report';
        at1.Endpoint_First_Responders_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/first_responder_report';
        at1.Endpoint_SalesforceID__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/salesforce_id_report';
        at1.Endpoint_User_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/user_report';
        insert at1;
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.AdminID__c ='410602';
        insert newAccount;
        
        Account newAccount1 = new Account();
        newAccount1.Name = 'Test Account';
        newAccount1.Customer_Status__c ='Active';
        newAccount1.AdminID__c ='410316';
        insert newAccount1;
        
        Contact newC = new Contact();
        newC.FirstName = 'tt';
        newC.LastName = 'dd';
        newC.AccountId = newAccount1.Id;
        newC.CustomerAdministratorId__c = '410316';
        newC.CustomerUserId__c = '235407';
        insert newC;
        
        Contact newC1 = new Contact();
        newC1.FirstName = 'tt';
        newC1.LastName = 'dd';
        newC1.AccountId = newAccount1.Id;
        newC1.CustomerUserId__c = '235408';
        insert newC1;
        
        Contact newC2 = new Contact();
        newC2.FirstName = 'tt';
        newC2.LastName = 'dd';
        newC2.AccountId = newAccount.Id;
        newC2.CustomerAdministratorId__c = '410602';
        newC2.CustomerUserId__c = '235409';
        insert newC2;
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new adminToolWebServicesMock());
        System.enqueueJob(new adminToolUserApiQueueable());
        Test.stopTest();
        List<Contact> all = [Select ID, Name from Contact where Account.AdminID__C in ('410602','410316') and Admin_Tool_Status__c != NULL];
        System.assertEquals(3,all.size());
    }
    
    static testmethod void testAdminToolBookingsData() {
        //This test covers adminToolBookingsDataQueueable
        Test.startTest();
        Admin_Tool_API_Settings__c at = new Admin_Tool_API_Settings__c();
        at.Name = 'Production';
        at.Username__c = 'testtest';
        at.Password__c = 'lalala';
        at.Environment__c = 'Production';
        at.Endpoint_Bookings_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/booking_report';
        at.Endpoint_Customer_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report';
        at.Endpoint_Customer_Data_Report_8_5__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report_85';
        at.Endpoint_DFU_Export__c = 'https://intranet.msdsonline.com/api/v1/reports/dfu_data_report';
        at.Endpoint_First_Responders_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/first_responder_report';
        at.Endpoint_SalesforceID__c = 'https://intranet.msdsonline.com/api/v1/reports/salesforce_id_report';
        at.Endpoint_User_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/user_report';
        insert at;
        
        Admin_Tool_API_Settings__c at1 = new Admin_Tool_API_Settings__c();
        at1.Name = 'Staging';
        at1.Username__c = 'testtest';
        at1.Password__c = 'lalala';
        at1.Environment__c = 'Staging';
        at1.Endpoint_Bookings_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/booking_report';
        at1.Endpoint_Customer_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report';
        at1.Endpoint_Customer_Data_Report_8_5__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report_85';
        at1.Endpoint_DFU_Export__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/dfu_data_report';
        at1.Endpoint_First_Responders_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/first_responder_report';
        at1.Endpoint_SalesforceID__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/salesforce_id_report';
        at1.Endpoint_User_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/user_report';
        insert at1;
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new adminToolWebServicesMock());
        System.enqueueJob(new adminToolBookingsDataQueueable());
        
        Test.stopTest();
        List<Order_Item__c> o = [Select ID from Order_Item__c where SystemModstamp = TODAY];
        System.assertEquals(3,o.size());
    }
    
    static testmethod void testDFUData() {
        //This test covers adminToolDFUDataQueueable
        Admin_Tool_API_Settings__c at = new Admin_Tool_API_Settings__c();
        at.Name = 'Production';
        at.Username__c = 'testtest';
        at.Password__c = 'lalala';
        at.Environment__c = 'Production';
        at.Endpoint_Bookings_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/booking_report';
        at.Endpoint_Customer_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report';
        at.Endpoint_Customer_Data_Report_8_5__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report_85';
        at.Endpoint_DFU_Export__c = 'https://intranet.msdsonline.com/api/v1/reports/dfu_data_report';
        at.Endpoint_First_Responders_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/first_responder_report';
        at.Endpoint_SalesforceID__c = 'https://intranet.msdsonline.com/api/v1/reports/salesforce_id_report';
        at.Endpoint_User_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/user_report';
        insert at;
        
        Admin_Tool_API_Settings__c at1 = new Admin_Tool_API_Settings__c();
        at1.Name = 'Staging';
        at1.Username__c = 'testtest';
        at1.Password__c = 'lalala';
        at1.Environment__c = 'Staging';
        at1.Endpoint_Bookings_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/booking_report';
        at1.Endpoint_Customer_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report';
        at1.Endpoint_Customer_Data_Report_8_5__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report_85';
        at1.Endpoint_DFU_Export__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/dfu_data_report';
        at1.Endpoint_First_Responders_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/first_responder_report';
        at1.Endpoint_SalesforceID__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/salesforce_id_report';
        at1.Endpoint_User_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/user_report';
        insert at1;
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.AdminID__c ='410602';
        insert newAccount;
        
        Contact newC = new Contact();
        newC.FirstName = 'tt';
        newC.LastName = 'dd';
        newC.AccountId = newAccount.Id;
        newC.CustomerAdministratorId__c = '410316';
        insert newC;
        
        Case newCase = new Case();
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Process' LIMIT 1].id;  
        newCase.AccountId = newAccount.id;
        newCase.ContactId = newC.Id;
        newCase.Status = 'Closed';
        newCase.Subject = 'Test';
        newCase.Type = 'Delivery Follow Up';
        newCase.Case_Resolution__c ='dsadsadsadsada';
        newCase.DFU_Status__c ='Incomplete';
        newCase.DFU_Incomplete_Reason__c ='Unresponsive';
        newCase.Priority ='3 - Medium';
        insert newCase;
        
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new adminToolWebServicesMock());
        System.enqueueJob(new adminToolDFUDataQueueable());
        Test.stopTest();
    }
    
    static testmethod void testSalesforceIDData() {
        //This test covers adminToolSalesforceIDQueueable
        Test.startTest();
        Admin_Tool_API_Settings__c at = new Admin_Tool_API_Settings__c();
        at.Name = 'Production';
        at.Username__c = 'testtest';
        at.Password__c = 'lalala';
        at.Environment__c = 'Production';
        at.Endpoint_Bookings_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/booking_report';
        at.Endpoint_Customer_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report';
        at.Endpoint_Customer_Data_Report_8_5__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report_85';
        at.Endpoint_DFU_Export__c = 'https://intranet.msdsonline.com/api/v1/reports/dfu_data_report';
        at.Endpoint_First_Responders_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/first_responder_report';
        at.Endpoint_SalesforceID__c = 'https://intranet.msdsonline.com/api/v1/reports/salesforce_id_report';
        at.Endpoint_User_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/user_report';
        insert at;
        
        Admin_Tool_API_Settings__c at1 = new Admin_Tool_API_Settings__c();
        at1.Name = 'Staging';
        at1.Username__c = 'testtest';
        at1.Password__c = 'lalala';
        at1.Environment__c = 'Staging';
        at1.Endpoint_Bookings_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/booking_report';
        at1.Endpoint_Customer_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report';
        at1.Endpoint_DFU_Export__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/dfu_data_report';
        at1.Endpoint_First_Responders_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/first_responder_report';
        at1.Endpoint_SalesforceID__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/salesforce_id_report';
        at1.Endpoint_User_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/user_report';
        insert at1;
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.AdminID__c ='410602';
        insert newAccount;
        
        Account newAccount1 = new Account();
        newAccount1.Name = 'Test Account';
        newAccount1.Customer_Status__c ='Active';
        newAccount1.AdminID__c ='410316';
        insert newAccount1;
        
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new adminToolWebServicesMock());
        System.enqueueJob(new adminToolSalesforceIDQueueable());
        Test.stopTest();
    }
    
    static testmethod void testAdminToolAPIManualRun() {
        //This test covers adminToolBookingsDataQueueable
        Test.startTest();
        Admin_Tool_API_Settings__c at = new Admin_Tool_API_Settings__c();
        at.Name = 'Production';
        at.Username__c = 'testtest';
        at.Password__c = 'lalala';
        at.Environment__c = 'Production';
        at.Endpoint_Bookings_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/booking_report';
        at.Endpoint_Customer_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report';
        at.Endpoint_Customer_Data_Report_8_5__c = 'https://intranet.msdsonline.com/api/v1/reports/customer_data_report_85';
        at.Endpoint_DFU_Export__c = 'https://intranet.msdsonline.com/api/v1/reports/dfu_data_report';
        at.Endpoint_First_Responders_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/first_responder_report';
        at.Endpoint_SalesforceID__c = 'https://intranet.msdsonline.com/api/v1/reports/salesforce_id_report';
        at.Endpoint_User_Data_Report__c = 'https://intranet.msdsonline.com/api/v1/reports/user_report';
        insert at;
        
        Admin_Tool_API_Settings__c at1 = new Admin_Tool_API_Settings__c();
        at1.Name = 'Staging';
        at1.Username__c = 'testtest';
        at1.Password__c = 'lalala';
        at1.Environment__c = 'Staging';
        at1.Endpoint_Bookings_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/booking_report';
        at1.Endpoint_Customer_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/customer_data_report';
        at1.Endpoint_DFU_Export__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/dfu_data_report';
        at1.Endpoint_First_Responders_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/first_responder_report';
        at1.Endpoint_SalesforceID__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/salesforce_id_report';
        at1.Endpoint_User_Data_Report__c = 'https://testsalesforceapi.msdsonline.com/api/v1/reports/user_report';
        insert at1;
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new adminToolWebServicesMock());
        adminToolAPIManualRun updateJob = new adminToolAPIManualRun('BookingsReport','11/1/2017','Staging');
        System.enqueueJob(updateJob);
        
        Test.stopTest();
        List<Order_Item__c> o = [Select ID from Order_Item__c where SystemModstamp = TODAY];
        System.assertEquals(3,o.size());
    }
    
    static testmethod void testCustomerUserScheduledClass() {
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new adminToolCustomerUserAPIFetch());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
    }
    
    static testmethod void testBookingsScheduledClass() {
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new adminToolBookingsAPIFetch());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
    }
    
    static testmethod void testDFUScheduledClass() {
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new adminToolDFUDataAPISend());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
    }
    
    static testmethod void testSFIDScheduledClass() {
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new adminToolSalesforceIDAPISend ());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
    }
}