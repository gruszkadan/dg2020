public class competitorContractEndDateNotification implements Schedulable{   
    public void execute(SchedulableContext SC) {
        boolean isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        Messaging.SingleEmailMessage[] notificationsToSend = new list<Messaging.SingleEmailMessage>();
        //create a map of the date values and corresponding month number to use in matching/email
        Map<Date,Integer> dateMap = new Map<Date,Integer>();
        dateMap.put(date.today().addMonths(6), 6);
        dateMap.put(date.today().addMonths(5), 5);
        dateMap.put(date.today().addMonths(4), 4);
        dateMap.put(date.today().addMonths(3), 3);
        //query all accounts where any of the competitor contract end dates fall within the next 6 months and subquery open opportunities
        Account[] accounts = [SELECT id, Name, Owner.Email, Secondary_Account_Owner__r.Email, EHS_Owner__r.Email, Ergonomics_Account_Owner__r.Email, Online_Training_Account_Owner__r.Email, Authoring_Account_Owner__r.Email, Platform_Sales_Rep__r.Email,
                              Current_System_Contract_End_Date__c, Current_System_Contract_End_Date_ODT__c, Current_System_Contract_End_Date_Ergo__c,
                              Current_System_Contract_End_Date_EHS__c, Current_System_Contract_End_Date_Auth__c,
                              Owner.Manager.Email, EHS_Owner__r.Manager.Email, Ergonomics_Account_Owner__r.Manager.Email, Online_Training_Account_Owner__r.Manager.Email, Authoring_Account_Owner__r.Manager.Email,
                              Owner.Sales_Director__r.Email, EHS_Owner__r.Sales_Director__r.Email, Ergonomics_Account_Owner__r.Sales_Director__r.Email, Online_Training_Account_Owner__r.Sales_Director__r.Email, Authoring_Account_Owner__r.Sales_Director__r.Email,
                              (SELECT id,Suite_of_Interest__c FROM Opportunities WHERE isClosed = false)
                              FROM Account
                              WHERE  
                              (Current_System_Contract_End_Date__c > :date.today() AND Current_System_Contract_End_Date__c <= :date.today().addMonths(6))
                              OR(Current_System_Contract_End_Date_ODT__c > :date.today() AND Current_System_Contract_End_Date_ODT__c <= :date.today().addMonths(6))
                              OR(Current_System_Contract_End_Date_Ergo__c > :date.today() AND Current_System_Contract_End_Date_Ergo__c <= :date.today().addMonths(6))
                              OR(Current_System_Contract_End_Date_EHS__c > :date.today() AND Current_System_Contract_End_Date_EHS__c <= :date.today().addMonths(6))
                              OR(Current_System_Contract_End_Date_Auth__c > :date.today() AND Current_System_Contract_End_Date_Auth__c <= :date.today().addMonths(6))
                             ];
        if(accounts.size() > 0){
            //loop through the accounts
            for(Account a:accounts){
                //create an integer variable to hold the number of open opportunities per product suite
                integer openMSDSOpps = 0;
                integer openEHSOpps = 0;
                integer openErgoOpps = 0;
                integer openODTOpps = 0;
                integer openAuthOpps = 0;
                //loop through the accounts open opportunities and set the open opportunity variables per suite
                for(Opportunity o:a.Opportunities){
                    if(o.Suite_of_Interest__c == 'MSDS Management'){
                        openMSDSOpps++;
                    }else if(o.Suite_of_Interest__c == 'EHS Management'){
                        openEHSOpps++;
                    }else if(o.Suite_of_Interest__c == 'Ergonomics'){
                        openErgoOpps++;
                    }else if(o.Suite_of_Interest__c == 'On-Demand Training'){
                        openODTOpps++;
                    }else if(o.Suite_of_Interest__c == 'MSDS Authoring'){
                        openAuthOpps++;
                    }
                }
                //for each suite check to see if either the end date is in 6 months OR if has both open opportunities AND if it maps to the other dates
                //if match found then create an email notification and add to send list
                if((dateMap.get(a.Current_System_Contract_End_Date__c) == 6) ||
                   (openMSDSOpps == 0 && (dateMap.get(a.Current_System_Contract_End_Date__c) != null && dateMap.get(a.Current_System_Contract_End_Date__c) != 6))){
                       String[] ccEmails = new list<String>();
                       if((!isSandbox) || test.isRunningTest()){
                           ccEmails.add(a.Owner.Manager.Email);
                           ccEmails.add(a.Owner.Sales_Director__r.Email);
                           if(a.Platform_Sales_Rep__r.Email != null){
                               ccEmails.add(a.Platform_Sales_Rep__r.Email);
                           }
                           if(a.Secondary_Account_Owner__r.Email != null){
                               ccEmails.add(a.Secondary_Account_Owner__r.Email);
                           }
                       }
                       notificationsToSend.add(createEmailMessage(a.id,a.Name,dateMap.get(a.Current_System_Contract_End_Date__c),a.Owner.Email,ccEmails));
                   }
                if((dateMap.get(a.Current_System_Contract_End_Date_EHS__c) == 6) ||
                   (openEHSOpps == 0 && (dateMap.get(a.Current_System_Contract_End_Date_EHS__c) != null && dateMap.get(a.Current_System_Contract_End_Date_EHS__c) != 6))){
                       String[] ccEmails = new list<String>();
                       if((!isSandbox) || test.isRunningTest()){
                           ccEmails.add(a.EHS_Owner__r.Manager.Email);
                           ccEmails.add(a.EHS_Owner__r.Sales_Director__r.Email);
                           if(a.Platform_Sales_Rep__r.Email != null){
                               ccEmails.add(a.Platform_Sales_Rep__r.Email);
                           }
                       }
                       notificationsToSend.add(createEmailMessage(a.id,a.Name,dateMap.get(a.Current_System_Contract_End_Date_EHS__c),a.EHS_Owner__r.Email,ccEmails));
                   }
                if((dateMap.get(a.Current_System_Contract_End_Date_Ergo__c) == 6) ||
                   (openErgoOpps == 0 && (dateMap.get(a.Current_System_Contract_End_Date_Ergo__c) != null && dateMap.get(a.Current_System_Contract_End_Date_Ergo__c) != 6))){
                       String[] ccEmails = new list<String>();
                       if((!isSandbox) || test.isRunningTest()){
                           ccEmails.add(a.Ergonomics_Account_Owner__r.Manager.Email);
                           ccEmails.add(a.Ergonomics_Account_Owner__r.Sales_Director__r.Email);
                           if(a.Platform_Sales_Rep__r.Email != null){
                               ccEmails.add(a.Platform_Sales_Rep__r.Email);
                           }
                       }
                       notificationsToSend.add(createEmailMessage(a.id,a.Name,dateMap.get(a.Current_System_Contract_End_Date_Ergo__c),a.Ergonomics_Account_Owner__r.Email,ccEmails));
                   }
                if((dateMap.get(a.Current_System_Contract_End_Date_ODT__c) == 6) ||
                   (openODTOpps == 0 && (dateMap.get(a.Current_System_Contract_End_Date_ODT__c) != null && dateMap.get(a.Current_System_Contract_End_Date_ODT__c) != 6))){
                       String[] ccEmails = new list<String>();
                       if((!isSandbox) || test.isRunningTest()){
                           ccEmails.add(a.Online_Training_Account_Owner__r.Manager.Email);
                           ccEmails.add(a.Online_Training_Account_Owner__r.Sales_Director__r.Email);
                           if(a.Platform_Sales_Rep__r.Email != null){
                               ccEmails.add(a.Platform_Sales_Rep__r.Email);
                           }
                       }
                       notificationsToSend.add(createEmailMessage(a.id,a.Name,dateMap.get(a.Current_System_Contract_End_Date_ODT__c),a.Online_Training_Account_Owner__r.Email,ccEmails));
                   }
                if((dateMap.get(a.Current_System_Contract_End_Date_Auth__c) == 6) ||
                   (openAuthOpps == 0 && (dateMap.get(a.Current_System_Contract_End_Date_Auth__c) != null && dateMap.get(a.Current_System_Contract_End_Date_Auth__c) != 6))){
                       String[] ccEmails = new list<String>();
                       if((!isSandbox) || test.isRunningTest()){
                           ccEmails.add(a.Authoring_Account_Owner__r.Manager.Email);
                           ccEmails.add(a.Authoring_Account_Owner__r.Sales_Director__r.Email);
                           if(a.Platform_Sales_Rep__r.Email != null){
                               ccEmails.add(a.Platform_Sales_Rep__r.Email);
                           }
                       }
                       notificationsToSend.add(createEmailMessage(a.id,a.Name,dateMap.get(a.Current_System_Contract_End_Date_Auth__c),a.Authoring_Account_Owner__r.Email,ccEmails));
                   }
            }
            if(notificationsToSend.size() > 0){
                Messaging.sendEmail(notificationsToSend);
            }
        }
    }
    
    //creates an email notification to send to the account product suite owner
    public Messaging.SingleEmailMessage createEmailMessage(String accountId, String accountName, Integer numOfMonths, String toAddress, string[] ccAddresses){
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setToAddresses(new string[]{toAddress});
        if(ccAddresses.size() > 0){
            message.setCcAddresses(ccAddresses);
        }
        string messageBody = 'Your Account, <a href="'+System.URL.getSalesforceBaseURL().toExternalForm()+'/'+accountId+'">'+accountName+'</a>, has a contract with a competitor that is ending in '+numOfMonths+' months. Please review and plan your selling strategy for this Account. If you have any questions or need any help please contact your manager.';
        message.setSubject(accountName+' - Current Competitor Contract Expiration - '+numOfMonths+' Months');
        message.setHTMLBody(messageBody);
        return message;
    }
}