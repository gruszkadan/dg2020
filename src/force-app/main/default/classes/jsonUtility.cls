public class jsonUtility {
    
    public poiActionUtility.poiAction[] parseJSON(string jsonStr) {
        poiActionUtility.poiAction[] poias = new list<poiActionUtility.poiAction>();
        JSONParser parser = JSON.createParser(jsonStr);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        poiActionUtility.poiAction poia = (poiActionUtility.poiAction)parser.readValueAs(poiActionUtility.poiAction.class);
                        poias.add(poia);
                        parser.skipChildren();
                    }
                }
            }
        } 
        return poias;
    }
    
    public static string tokenParser(string jsonString, string tokenName){
        string token;
        JSONParser parser = JSON.createParser(jsonString);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == tokenName)) {
                // Get the value.
                parser.nextToken();
                // Token Value
                token = parser.getText();                
            }
        }
        return token;
    }
    
    public static boolean marketoErrors(string jsonStr, string logClass, string logMethod) {
        boolean success;
        marketoErrors[] err = new list<marketoErrors>();
        JSONParser parser = JSON.createParser(jsonStr);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'success')) {
                //Get the Value
                parser.nextToken();
                //Success Value
                success = parser.getBooleanValue();
                if(success == FALSE){
                    while (parser.nextToken() != JSONToken.END_OBJECT) {
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            marketoErrors me = (marketoErrors)parser.readValueAs(marketoErrors.class);
                            err.add(me);
                        }
                    }
                    salesforceLog.createLog('POI',TRUE,logClass,logMethod,string.valueOf(err));
                }
            }
        }
        return success;
    }
    
    public static string marketoMoreResults(string jsonStr, string logClass, string logMethod) {
        string nextPageToken;
        System.JSONToken token;
        string text;
        boolean moreResults;
        JSONParser parser = JSON.createParser(jsonStr);
        while (parser.nextToken() != null) {
            if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() ==  'nextPageToken')) {
                //Get the Value
                parser.nextToken();
                nextPageToken = parser.getText();
            }
        }
        return nextPageToken;
    }
    
    public class marketoErrors {
        public String code;
        public String message;
    }    
}