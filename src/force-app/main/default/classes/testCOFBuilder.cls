@isTest(seeAllData = true)
public class testCOFBuilder {
    
    @isTest static void cofTest1(){
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA'; 
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '4 Years';
        insert newContract;  
        
        string orderedString = '';
        Contract_Line_Item__c[] cliToInsert = new List<Contract_Line_Item__c>();
        Product2[] newProds =  [ SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name = 'GM Account' LIMIT 1];
        
        for(Product2 p : newProds){
            Contract_Line_Item__c cli = new Contract_Line_Item__c();
            cli.Contract__c = newContract.Id;
            cli.Product__c = p.id;
            cli.Sort_order__c = 2;
            cliToInsert.add(cli);
        }
        
        
        Product2 parentProd =  [ SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name = 'eBinder Valet' LIMIT 1];
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Contract__c = newContract.Id;
        cli2.Product__c = parentProd.id;
        cli2.Group_ID__c = '100100100';
        cli2.Sort_order__c = 2;
        cliToInsert.add(cli2);
        
        Product2 childProd =  [SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name ='Indexing Field - Custom Field 1' LIMIT 1];
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Contract__c = newContract.Id;
        cli3.Product__c = childProd.id;
        cli3.Group_ID__c = '100100100';
        cli3.Group_Parent_ID__c  = '100100100';
        cliToInsert.add(cli3);
        
        Product2 childProd2 =  [SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name ='Indexing Field - Custom Field 2' LIMIT 1];
        Contract_Line_Item__c cli4 = new Contract_Line_Item__c();
        cli4.Contract__c = newContract.Id;
        cli4.Product__c = childProd2.id;
        cli4.Group_ID__c = '100100100';
        cli4.Group_Parent_ID__c  = '100100100';
        cliToInsert.add(cli4);
        
        System.debug(cliToInsert);
        insert cliToInsert;
        for(Contract_Line_Item__c c: cliToInsert){
            orderedString = orderedString + c.id + ',';
        }
        orderedString.removeEnd(',');
        
        ApexPages.currentPage().getParameters().put('Id', newContract.Id);
        cofBuilder myController = new cofBuilder();
        
        mycontroller.displayOpts = 'Type';
        myController.restructureWrappersByType();
        myController.productOrderString = orderedString;
        myController.next();
    }
    
    @isTest static void cofTest2(){
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
               
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA'; 
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Product_Print_Grouping__c[] PPGs = [Select id, Name__c from Product_Print_Grouping__c];
        
        
        //convertCurrency(calcListPrice(product.Quantity_Calculation__c, product.Y1_List_Price__c, product.Quantity__c, product.Y1_Quote_Price__c, product.Y1_Bundled_Price__c), 
        
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '5 Years';
        newContract.Quoted_Currency__c = 'USD';
        newContract.Quoted_Currency_Rate__c = 1;
        newContract.COF_Group_by__c = 'Type';
        insert newContract;  
        
        string orderedString = '';
        Contract_Line_Item__c[] cliToInsert = new List<Contract_Line_Item__c>();
        Product2[] newProds =  [ SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name = 'HQ' LIMIT 1];
        
        for(Product2 p : newProds){
            Contract_Line_Item__c cli = new Contract_Line_Item__c();
            cli.Contract__c = newContract.Id;
            cli.Product__c = p.id;
            cli.Sort_order__c = 2;
            cli.Year_1__c = true;
            cli.Year_2__c = true;
            cli.Year_3__c = true;
            cli.Year_4__c = true;
            cli.Year_5__c = true;
            cli.Name__c = 'ABC';
            cli.Quantity_Calculation__c = false;
            cli.Y1_List_Price__c = 12500;
            cli.Y1_Quote_Price__c = 12000;
            cli.Y2_List_Price__c = 10000;   
            cli.Y3_List_Price__c = 10000;   
            cli.Y4_List_Price__c = 10000;   
            cliToInsert.add(cli);
            
        }
        

        Product2 parentProd =  [ SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name = 'Air' LIMIT 1];
        Quote_Product__c quoteProductA = new Quote_Product__c();
        quoteProductA.Quote__c = newQuote.id;
        quoteProductA.Product__c = parentProd.id;
        insert quoteProductA;
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Contract__c = newContract.Id;
        cli2.Product__c = parentProd.id;
        cli2.Sort_order__c = 2;
        cli2.Name__c = 'BCD';
                                cli2.Year_1__c = true;
        cli2.Year_2__c = true;
        cli2.Year_3__c = true;
        cli2.Year_4__c = true;
        cli2.Quantity_Calculation__c = TRUE;
        cli2.Quantity__c = 20;
        cli2.Y1_List_Price__c = 5;
        cli2.Y2_List_Price__c = 5;   
        cli2.Y3_List_Price__c = 5;   
        cli2.Y4_List_Price__c = 5;
        cli2.Y1_Bundled_Price__c = 150;
        cli2.Y2_Bundled_Price__c = 150;
        cli2.Y3_Bundled_Price__c = 150;
        cli2.Y4_Bundled_Price__c = 150;
        cli2.qpID__c = quoteProductA.id;
        cliToInsert.add(cli2);
        
        
        
        Product2 childProd =  [SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name ='Air - Implementation Fee' LIMIT 1];
        
        Quote_Product__c quoteProductB = new Quote_Product__c();
        quoteProductB.Quote__c = newQuote.id;
        quoteProductB.Bundled_Product__c = quoteProductA.id;
        quoteProductA.Product__c = childProd.id;
        insert quoteProductB;
        
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Contract__c = newContract.Id;
        cli3.Product__c = childProd.id;
        cli3.Sort_order__c = 2;
                        cli3.Year_1__c = true;
        cli3.Year_2__c = true;
        cli3.Year_3__c = true;
        cli3.Year_4__c = true;
        cli3.Name__c = 'CDE';
        cli3.Quantity_Calculation__c = false;
        cli3.Y1_List_Price__c = 50;
        cli3.Y2_List_Price__c = 50;   
        cli3.Y3_List_Price__c = 50;   
        cli3.Y4_List_Price__c = 50;   
        cli3.Bundle__c = TRUE;
        cli3.Bundled_Product_Name__c = quoteProductA.Name;
        cliToInsert.add(cli3);
        
        
        Product2 parentProd2 =  [ SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name = 'Base Subscription' LIMIT 1];
        Contract_Line_Item__c cli4 = new Contract_Line_Item__c();
        cli4.Contract__c = newContract.Id; 
        cli4.Product__c = parentProd.id;
        cli4.Sort_order__c = 2;
                cli4.Year_1__c = true;
        cli4.Year_2__c = true;
        cli4.Year_3__c = true;
        cli4.Year_4__c = true;
        cli4.Name__c = 'DEF';
        cli4.Quantity_Calculation__c = TRUE;
        cli4.Quantity__c = 20;
        cli4.Y1_List_Price__c = 20;
        cli4.Y2_List_Price__c = 20;   
        cli4.Y3_List_Price__c = 20;   
        cli4.Y4_List_Price__c = 20; 
        cliToInsert.add(cli4);
        
        Product2 childProd2 =  [SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name ='Base Subscription - Implementation Fee' LIMIT 1];
        
        
        Contract_Line_Item__c cli5 = new Contract_Line_Item__c();
        cli5.Contract__c = newContract.Id;
        cli5.Product__c = childProd.id;
        cli5.Sort_order__c = 2;
        cli5.Name__c = 'EFG';
        cli5.Quantity_Calculation__c = False;
        cli5.Quantity__c = 20;
        cli5.Year_1__c = true;
        cli5.Year_2__c = true;
        cli5.Year_3__c = true;
        cli5.Year_4__c = true;
        cli5.Year_5__c = true;
        cli5.Y1_List_Price__c = 100;
        cli5.Y2_List_Price__c = 0;   
        cli5.Y3_List_Price__c = 0;   
        cli5.Y4_List_Price__c = 0; 
        cliToInsert.add(cli5);
        
        
        
        System.debug(cliToInsert);
        insert cliToInsert;
        for(Contract_Line_Item__c c: cliToInsert){
            orderedString = orderedString + c.id + ',';
        }
        orderedString.removeEnd(',');
         
        ApexPages.currentPage().getParameters().put('Id', newContract.Id);
        cofBuilder myController = new cofBuilder();
        myController.productOrderString = orderedString;
        System.debug(myController.Buttons.size());
        for(Integer i = 0; i < myController.Buttons.size(); i++){
            System.debug(myController.Buttons[i].PdtCount);
            if(myController.Buttons[i].PdtCount == 2){
                //myController.Buttons[i].isSelected = true;
                mycontroller.buttonType = myController.Buttons[i].ppg.Name__c;
                myController.currentGroupParent = myController.Buttons[i].ppg.Id;
                System.assert(mycontroller.buttonType == 'Implementation Fee');
            }
        }
        //there are two implementation fees; check by confirming button type 
        mycontroller.clickButton();

        //System.currentPageReference().getParameters().put('year', 2);
        //ApexPages.currentPage().getParameters().put('input', 99); 
       // ApexPages.currentPage().getParameters().put('year', 2);  
           // PageReference reference = Page.cof_builder_step1;
			//reference.getParameters().put('input', 99);    
        
        myController.updateGroupPrices();
        myController.updateGroupedChildren();
        //myController.showInput();
        myController.next();
        
        
    }
    
    //Product_Print_Grouping_Name__c
    @isTest static void cofTest3(){
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
               
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA'; 
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        id impleFeePPGId;
        Product_Print_Grouping__c[] PPGs = [Select id, Name__c from Product_Print_Grouping__c];
        for(Product_Print_Grouping__c g : PPGs){
            if(g.Name__c == 'Implementation Fee'){
                impleFeePPGId = g.id;
            }
            
        }
        
        //convertCurrency(calcListPrice(product.Quantity_Calculation__c, product.Y1_List_Price__c, product.Quantity__c, product.Y1_Quote_Price__c, product.Y1_Bundled_Price__c), 
        
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '5 Years';
        newContract.Quoted_Currency__c = 'USD';
        newContract.Quoted_Currency_Rate__c = 1;
        newContract.COF_Group_by__c = 'Type';
        insert newContract;  
        
        string orderedString = '';
        Contract_Line_Item__c[] cliToInsert = new List<Contract_Line_Item__c>();
        
        Product2 parentProd =  [ SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name = 'Air' LIMIT 1];
        Quote_Product__c quoteProductA = new Quote_Product__c();
        quoteProductA.Quote__c = newQuote.id;
        quoteProductA.Product__c = parentProd.id;
        insert quoteProductA;
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Contract__c = newContract.Id;
        cli2.Product__c = parentProd.id;
        cli2.Sort_order__c = 2;
        cli2.Name__c = 'BCD';
         cli2.Year_1__c = true;
        cli2.Year_2__c = true;
        cli2.Year_3__c = true;
        cli2.Year_4__c = true;
        cli2.Quantity_Calculation__c = TRUE;
        cli2.Quantity__c = 20;
        cli2.Y1_List_Price__c = 5;
        cli2.Y2_List_Price__c = 5;   
        cli2.Y3_List_Price__c = 5;   
        cli2.Y4_List_Price__c = 5;
        cli2.Y1_Bundled_Price__c = 150;
        cli2.Y2_Bundled_Price__c = 150;
        cli2.Y3_Bundled_Price__c = 150;
        cli2.Y4_Bundled_Price__c = 150;
        cli2.qpID__c = quoteProductA.id;
        cliToInsert.add(cli2);
        
        
        
        Product2 childProd =  [SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name ='Air - Implementation Fee' LIMIT 1];
        
        Quote_Product__c quoteProductB = new Quote_Product__c();
        quoteProductB.Quote__c = newQuote.id;
        quoteProductB.Bundled_Product__c = quoteProductA.id;
        quoteProductA.Product__c = childProd.id;
        insert quoteProductB;
        
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Contract__c = newContract.Id;
        cli3.Product__c = childProd.id;
        cli3.Sort_order__c = 2;
             cli3.Product_Print_Grouping_Name__c  = impleFeePPGId;
                 cli3.Year_1__c = true;
        cli3.Year_2__c = true;
        cli3.Year_3__c = true;
        cli3.Year_4__c = true;
        cli3.Name__c = 'CDE';
        cli3.Quantity_Calculation__c = false;
        cli3.Y1_List_Price__c = 50;
        cli3.Y2_List_Price__c = 50;   
        cli3.Y3_List_Price__c = 50;   
        cli3.Y4_List_Price__c = 50;   
        cli3.Bundle__c = TRUE;
        cli3.Bundled_Product_Name__c = quoteProductA.Name;
        cliToInsert.add(cli3);
        
        
        Product2 parentProd2 =  [ SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name = 'Base Subscription' LIMIT 1];
        Contract_Line_Item__c cli4 = new Contract_Line_Item__c();
        cli4.Contract__c = newContract.Id; 
        cli4.Product__c = parentProd.id;
        cli4.Sort_order__c = 2;
                cli4.Year_1__c = true;
        cli4.Year_2__c = true;
        cli4.Year_3__c = true;
        cli4.Year_4__c = true;
        cli4.Name__c = 'DEF';
        cli4.Quantity_Calculation__c = TRUE;
        cli4.Quantity__c = 20;
        cli4.Y1_List_Price__c = 20;
        cli4.Y2_List_Price__c = 20;   
        cli4.Y3_List_Price__c = 20;   
        cli4.Y4_List_Price__c = 20; 
        cliToInsert.add(cli4);
        
        Product2 childProd2 =  [SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name ='Base Subscription - Implementation Fee' LIMIT 1];
        
        
        Contract_Line_Item__c cli5 = new Contract_Line_Item__c();
        cli5.Contract__c = newContract.Id;
        cli5.Product__c = childProd.id;
        cli5.Product_Print_Grouping_Name__c  = impleFeePPGId;
        cli5.Sort_order__c = 2;
        cli5.Name__c = 'EFG';
        cli5.Quantity_Calculation__c = False;
        cli5.Quantity__c = 20;
        cli5.Year_1__c = true;
        cli5.Year_2__c = true;
        cli5.Year_3__c = true;
        cli5.Year_4__c = true;
        cli5.Year_5__c = true;
        cli5.Y1_List_Price__c = 100;
        cli5.Y2_List_Price__c = 0;   
        cli5.Y3_List_Price__c = 0;   
        cli5.Y4_List_Price__c = 0; 
        cliToInsert.add(cli5);
        
        
        
        System.debug(cliToInsert);
        insert cliToInsert;
        for(Contract_Line_Item__c c: cliToInsert){
            orderedString = orderedString + c.id + ',';
        }
        orderedString.removeEnd(',');
         
        ApexPages.currentPage().getParameters().put('Id', newContract.Id);
        cofBuilder myController = new cofBuilder();
        myController.productOrderString = orderedString;
        System.debug(myController.Buttons.size());
        for(Integer i = 0; i < myController.Buttons.size(); i++){
            System.debug(myController.Buttons[i].PdtCount);
            if(myController.Buttons[i].PdtCount == 2){
                //myController.Buttons[i].isSelected = true;
                mycontroller.buttonType = myController.Buttons[i].ppg.Name__c;
                myController.currentGroupParent = myController.Buttons[i].ppg.Id;
                System.assert(mycontroller.buttonType == 'Implementation Fee');
            }
        }
        //there are two implementation fees; check by confirming button type 
        mycontroller.clickButton();

        //System.currentPageReference().getParameters().put('year', 2);
        //ApexPages.currentPage().getParameters().put('input', 99); 
       // ApexPages.currentPage().getParameters().put('year', 2);  
           // PageReference reference = Page.cof_builder_step1;
			//reference.getParameters().put('input', 99);    
        
        myController.updateGroupPrices();
        myController.updateGroupedChildren();
        //myController.showInput();
        myController.next();
        
        
    }
    
    //Product_Print_Grouping_Name__c
    @isTest static void cofPreviewTest1(){
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
               
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA'; 
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
       
        
        id impleFeePPGId;
        Product_Print_Grouping__c[] PPGs = [Select id, Name__c from Product_Print_Grouping__c];
        for(Product_Print_Grouping__c g : PPGs){
            if(g.Name__c == 'Implementation Fee'){
                impleFeePPGId = g.id;
            }
            
        }
        
 
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '5 Years';
        newContract.Quoted_Currency__c = 'USD';
        newContract.Quoted_Currency_Rate__c = 1;
        newContract.COF_Group_by__c = 'Type';
        insert newContract;  
        
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Contract__c = newContract.Id;
        cli2.Sort_order__c = 2;
        cli2.Name__c = 'BCD';
         cli2.Year_1__c = true;
        cli2.Year_2__c = true;
        cli2.Year_3__c = true;
        cli2.Year_4__c = true;
        cli2.Quantity_Calculation__c = TRUE;
        cli2.Quantity__c = 20;
        cli2.Y1_List_Price__c = 5;
        cli2.Y2_List_Price__c = 5;   
        cli2.Y3_List_Price__c = 5;   
        cli2.Y4_List_Price__c = 5;
        cli2.Y1_Bundled_Price__c = 150;
        cli2.Y2_Bundled_Price__c = 150;
        cli2.Y3_Bundled_Price__c = 150;
        cli2.Y4_Bundled_Price__c = 150;
        insert cli2;

        Product2 childProd =  [SELECT id, Name, Product_Platform__c, Contract_Product_Type__c FROM Product2 WHERE Name ='Air - Implementation Fee' LIMIT 1];
                
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Contract__c = newContract.Id;
        cli3.Sort_order__c = 2;
         cli3.Year_1__c = true;
        cli3.Year_2__c = true;
        cli3.Year_3__c = true;
        cli3.Year_4__c = true;
        cli3.Name__c = 'CDE';
        cli3.Quantity_Calculation__c = false;
        cli3.Y1_List_Price__c = 50;
        cli3.Y2_List_Price__c = 50;   
        cli3.Y3_List_Price__c = 50;   
        cli3.Y4_List_Price__c = 50;   
        insert cli3;

        ApexPages.currentPage().getParameters().put('Id', newContract.Id);
        cofBuilderPreview myController = new cofBuilderPreview();
		myController.createAttachments();
        Attachment[] a = [Select id, ParentId from Attachment where ParentId =: newContract.Id];
		System.assert(a.size() == 1);
    }
    
}