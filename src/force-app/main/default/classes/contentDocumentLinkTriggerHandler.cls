public class contentDocumentLinkTriggerHandler {
    
    // AFTER INSERT
    public static void afterInsert(ContentDocumentLink[] oldContentDocumentLinks, ContentDocumentLink[] newContentDocumentLinks) {
        contentUtility.sendContentNotification(contentUtility.findContentVersions(newContentDocumentLinks));
    }
    
}