@isTest(SeeAllData = true)
private class testProposalPrintPDF {
    
    
    
    static testMethod void test1(){
        
        
        User u = [Select id, Email from User where id =: UserInfo.getUserId() LIMIT 1];
        
        Content_Document_Image__c image = new Content_Document_Image__c();
        insert image;
        
        contentVersion testContent = new contentVersion();
        testContent.Document_Owner__c = u.id;
        testContent.Document_Owner_Email__c = u.Email;
        testContent.Creation_Date__c = Date.today();
        testContent.Content_Document_Image__c = image.id; 
        testContent.PathOnClient = 'test.com';
        
        String testString = 'yes';
        Blob testBlob = Blob.valueOf(testString);
        
        testContent.VersionData = testBlob;
        insert testContent;
        
        Id docId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :testContent.id].ContentDocumentId;
        
        
        Quote__c testQuote = new Quote__c();
        testQuote.JSON_Proposal_Data__c = '{"jsonWrapper": [{"Id":"'+docId+'","vfPage":"","Name":"Proposal Document Multiple Pages","identifier":"Proposal Document Multiple Pages=069M0000000Bq9bIAC","Order":1},{"Id":"","vfPage":"proposal_cover_page","Name":"About Us","identifier":"About Us=069M0000000BpvPIAS","Order":2}]}';
        insert testQuote;
        
        
        Attachment testAttachment1 = new attachment();
        testAttachment1.name = 'A Attachment';
        testAttachment1.body = testBlob;
        testAttachment1.ParentId = testContent.Content_Document_Image__c;
        insert testAttachment1;
        
        
        Attachment testAttachment2 = new attachment();
        testAttachment2.name = 'B Attachment';
        testAttachment2.body = testBlob;
        testAttachment2.ParentId = testContent.Content_Document_Image__c;
        insert testAttachment2;
        
        ApexPages.currentPage().getParameters().put('id', testQuote.id);
        proposalPrintPDF con = new proposalPrintPDF();
        
        
        //test Static Wrappers, JSON parses correctly and matches with contentDocumentId
        system.assertEquals(con.pageWrappers[0].type, 'Static');
        system.assertEquals(con.pageWrappers[0].id, docId);
        system.assertEquals(con.pageWrappers[0].contentDocumentImageFound, true);
        
        //test Dynamic Wrappers, JSON parses correctly and contentDocument field remains false
        system.assertEquals(con.pageWrappers[1].type, 'Dynamic');
        system.assertEquals(con.pageWrappers[1].id, 'proposal_cover_page');
        system.assertEquals(con.pageWrappers[1].contentDocumentImageFound, false);
        
        
        //test Attachments match with ContentDocumentId and in correct order
        system.assertEquals(con.contentMap.get(docId)[0].id, testAttachment1.id);
        system.assertEquals(con.contentMap.get(docId)[1].id, testAttachment2.id);
        
        
        
    }
    
}