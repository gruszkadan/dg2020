@isTest(SeeAllData=true)
    private class testConvertToContract {
        
        public static testmethod void test_ConvertToContract() {
            Account newAccount = new Account();
            newAccount.Name = 'Test Account';
            newAccount.Subscription_End_Date__c = '10/10';
            insert newAccount;
            
            Account relatedAcct = new Account();
            relatedAcct.Name = 'test';
            relatedAcct.Subscription_End_Date__c = '12/12';
            insert relatedAcct;
            
            Contact newContact = new Contact();
            newContact.AccountID = newAccount.Id;
            newContact.LastName = 'Test';
            insert newContact;
            
            Quote__c newQuote = new Quote__c();
            newQuote.Account__c = newAccount.Id;
            newQuote.Contact__c = newContact.Id;
            insert newQuote;
            
            Opportunity newOpportunity = new Opportunity();
            newOpportunity.AccountID = newAccount.Id;
            newOpportunity.Name = 'Test';
            newOpportunity.CloseDate = system.today(); 
            newOpportunity.StageName = 'System Demo';
            newOpportunity.Quote__c = newQuote.Id; 
            newOpportunity.Contract_Length__c = '5 Years';
            newOpportunity.Currency__c = 'EUR';
            newOpportunity.Currency_Rate__c = 1;
            newOpportunity.Currency_Rate_Date__c = date.today().addDays(-1);
            newOpportunity.Implementation_Model__c = 'Simple';
            insert newOpportunity;
            
            Opportunity newOpportunityB = new Opportunity();
            newOpportunityB.AccountID = newAccount.Id;
            newOpportunityB.Name = 'Test';
            newOpportunityB.CloseDate = system.today(); 
            newOpportunityB.StageName = 'System Demo';
            newOpportunityB.Quote__c = newQuote.Id; 
            newOpportunityB.Contract_Length__c = '5 Years';
            newOpportunityB.Currency__c = 'EUR';
            newOpportunityB.Currency_Rate__c = 1;
            newOpportunityB.Currency_Rate_Date__c = date.today().addDays(-1);
            insert newOpportunityB;
            
            
            OpportunityLineitem myItemB = new OpportunityLineitem();
            myItemB.PricebookEntryId = [SELECT id FROM PriceBookEntry WHERE isActive = true LIMIT 1].id;
            myItemB.Quantity = 1;
            myItemB.TotalPrice = 1.00;
            myItemB.OpportunityID = newOpportunityB.Id;
            insert myItemB;        
            
            OpportunityContactRole opptyC = new OpportunityContactRole();
            opptyC.OpportunityId = newOpportunity.Id;
            opptyC.ContactID = newContact.Id;
            opptyC.IsPrimary = TRUE;
            insert opptyC;
            
            OpportunityLineitem myItem = new OpportunityLineitem();
            myItem.PricebookEntryId = [SELECT id FROM PriceBookEntry WHERE isActive = true LIMIT 1].id;
            myItem.Quantity = 1;
            myItem.TotalPrice = 1.00;
            myItem.OpportunityID = newOpportunity.Id;
            insert myItem;
            
            Address__c newAddress = new Address__c();
            newAddress.Account__c = newAccount.Id;
            newAddress.City__c = 'test city';
            newAddress.Country__c = 'United States';
            newAddress.IsPrimary__c = true;
            newAddress.State__c = 'IL';
            newAddress.Street__c = '222 Test Road';
            newAddress.Zip_Postal_Code__c = '612365';   
            
            Contract__c newContract = new Contract__c();
            newContract.Contract_Start_Date__c = date.today();
            newContract.Account__c = newAccount.Id;
            newContract.Contact__c = newContact.Id;
            newContract.Address__c = newaddress.Id;
            newContract.Contract_Type__c ='New';
            newContract.Contract_Length__c = '1 Year';
            newContract.Contract_Start_Date__c = date.TODAY();
            newContract.Contract_Start_Type__c = 'Specific Date';
            newContract.Contract_End_Date__c = date.today()+90;
            newContract.Related_Contract__c = 'T1000';
            newContract.Related_Contract_Account__c = relatedAcct.id;
            newContract.Related_Contract_Type__c = 'Add-On';
            newContract.Deferred_Invoice__c = true;
            newContract.Deferred_Invoice_Date__c = date.today();
            newContract.Delayed_Billing_Only__c = true;
            newContract.Delayed_Billing_Date__c = date.today();
            insert newContract;  
            
            Product2 newProd =  [ SELECT id, Name, Contract_Print_Group__c FROM Product2 WHERE Name = 'Custom Print Job' LIMIT 1 ];
            newProd.Contract_End_Date_Type__c = 'Subscription Anniversary';
            update newProd;
            
            Product2 newProd2 =  [ SELECT id, Name, Contract_Print_Group__c FROM Product2 WHERE Name = 'GM Account' LIMIT 1 ];
            newProd2.Contract_End_Date_Type__c = 'Expected End Date';
            update newProd2;
            
            Contract_Line_Item__c cli1 = new Contract_Line_Item__c();
            cli1.Contract__c = newContract.Id;
            cli1.Product__c = newProd.id;
            cli1.Sort_order__c = 1;
            cli1.Year_1_Manual__c = 0;
            cli1.Year_2_Manual__c = 0;
            cli1.Year_3_Manual__c = 0;
            cli1.Year_4_Manual__c = 0;
            cli1.Year_5_Manual__c = 0;
            insert cli1;
            
            Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
            cli2.Contract__c = newContract.Id;
            cli2.Product__c = newProd.id;
            cli2.Sort_order__c = 2;
            insert cli2;
            
            test.startTest();
            PageReference myVfPage = Page.create_contract;
            myVfPage.getParameters().put('id', newOpportunity.Id);
            Test.setCurrentPage(myVfPage); 
            
            //ApexPages.currentPage().getParameters().put('id', newOpportunity.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
            convertToContract myController = new convertToContract(testController);
            
            myController.accountList[0].selectedforErgo = true;
            myController.accountList[0].selectedforEHS = true;
            myController.accountList[0].selected = true;
            myController.saveSelectedAccounts();
            myController.saveSelectedErgoAccounts();
            myController.saveSelectedEHSAccounts();
            
            myController.newAddress = newAddress;
            myController.contractLength = 3;
            myController.cont.Num_of_Locations__c = 1;
            myController.cont.Coverage_Type__c = 'Divisions';
            myController.cont.Ergo_Coverage__c = 1;
            myController.cont.Ergo_Coverage_Type__c = 'Divisions';
            myController.cont.EHS_Coverage__c = 2;
            myController.cont.EHS_Coverage_Type__c = 'Employees';
            
            
            
            myController.theContract = newContract;
            myController.management = new List<Contract_Line_Item__c>();
            myController.services = new List<Contract_Line_Item__c>();
            myController.compliance = new List<Contract_Line_Item__c>();
            
            
            myController.management.add(cli1);
            myController.management.add(cli2);
            myController.services.add(cli1);
            myController.services.add(cli2);        
            myController.compliance.add(cli1);
            myController.compliance.add(cli2);   
            myController.selTerritory = [SELECT id FROM Territory__c LIMIT 1].id;
            myController.selNAICS = '12345';
            
            myController.addAddress();
            myController.specialIndexingUpdate();
            myController.updateContractNumber();
            myController.getContactSelectList();
            myController.setName();
            myController.getContact();
            myController.getAddresses();
            myController.getAddressString( newAddress.Id );
            myController.selectBillingAddress();
            myController.selectShippingAddress();
            myController.updateBillTo(); 
            myController.updateShipTo();
            myController.getOpptyItems( newOpportunity.Id );
            myController.getContractLineItems();
            myController.renewalContractEndDate();
            myController.renewalContractUpdate();
            myController.getrelatedContractYesNo();
            
            myController.findOpportunities();
            System.assert( myController.relatedOppList.size() > 0);
            myController.relatedOppList[0].selected=true;
            myController.SelectedOppId = myController.relatedOppList[0].relatedOp.id;
            myController.selectRelatedOpp();
            
            myController.cont.Contract_Type__c = 'New';
            myController.cont.Related_Contract_Type__c = 'Add-On';
            myController.cont.Related_Contract_Account__c = relatedAcct.id;
            myController.relatedContractEndDate(5);
            
            myController.cont.Contract_Type__c = 'Renewal';
            myController.relatedContractEndDate(5);
            
            myController.contractExecute();
            myController.cont.Contract_Length__c = '3 Years';
            myController.contractUpdate();
            myController.createCont();
            myController.newAddress.Street__c = null;
            myController.newAddress.City__c = null;
            myController.newAddress.Zip_Postal_Code__c = null;
            myController.newAddress.Country__c = null;
            myController.addAddress();
            
            myController.cont.Billing_Currency__c = 'EUR';
            myController.rates = new list<DatedConversionRate>();
            myController.findFXRates();
            myController.multicurrencyDeferralDates();
            myController.getPaymentNetTerms();
            
            myController.accountList[0].selected = TRUE;
            myController.processSelectedAccounts();
            //myController.ergoMSADocumentInsert();
            myController.clearRelatedOps();
            test.stopTest();
        }
        
        
        public static testmethod void test_ConvertToContract_Validation() {
            Account newAccount = new Account();
            newAccount.Name = 'Test Account';
            date d = date.today().addMonths(-1);               
            newAccount.Subscription_End_Date__c = d.month() + '/' + d.day();
            //newAccount.Subscription_End_Date__c = '10/10';
            insert newAccount;
            
            Account relatedAcct = new Account();
            relatedAcct.Name = 'test';
            relatedAcct.Subscription_End_Date__c = '12/12';
            insert relatedAcct;
            
            
            Contact newContact = new Contact();
            newContact.AccountID = newAccount.Id;
            newContact.LastName = 'Test';
            insert newContact;
            
            
            Contact anotherContact = new Contact();
            anotherContact.AccountID = newAccount.Id;
            anotherContact.LastName = 'Test';
            insert anotherContact;
            
            
            Quote__c newQuote = new Quote__c();
            newQuote.Account__c = newAccount.Id;
            newQuote.Contact__c = newContact.Id;
            insert newQuote;
            
            Opportunity newOpportunity = new Opportunity();
            newOpportunity.AccountID = newAccount.Id;
            newOpportunity.Name = 'Test';
            newOpportunity.CloseDate = system.today(); 
            newOpportunity.StageName = 'System Demo';
            newOpportunity.Quote__c = newQuote.Id; 
            newOpportunity.Contract_Length__c = '5 Years';
            newOpportunity.Currency__c = 'EUR';
            newOpportunity.Currency_Rate__c = 1;
            newOpportunity.Subscription_Model__c = 'Enterprise';
            newOpportunity.Implementation_Model__c = 'Flexible';
            newOpportunity.Currency_Rate_Date__c = date.today().addDays(-1);
            insert newOpportunity;
            
            id pricebook = [SELECT id FROM PriceBookEntry WHERE isActive = true AND NAME = 'Air' LIMIT 1].id;
            
            OpportunityLineitem myItem = new OpportunityLineitem();
            myItem.PricebookEntryId = pricebook;
            myItem.Quantity = 1;
            myItem.TotalPrice = 900.00;
            myItem.OpportunityID = newOpportunity.Id;
            myItem.Product2Id = [Select id from Product2 where Name = 'Air' Limit 1].id;
            
            OpportunityLineitem myItem2 = new OpportunityLineitem();
            myItem2.PricebookEntryId = pricebook;
            myItem2.Quantity = 1;
            myItem2.TotalPrice = 1000.00;
            myItem2.OpportunityID = newOpportunity.Id;
            myItem2.Product2Id = [Select id from Product2 where Name = 'HQ Account' Limit 1].id;
            insert myItem2;        
            
            OpportunityContactRole opptyC = new OpportunityContactRole();
            opptyC.OpportunityId = newOpportunity.Id;
            opptyC.ContactID = newContact.Id;
            opptyC.IsPrimary = TRUE;
            insert opptyC;
            
            OpportunityLineitem[] check = [Select id, Product2Id, Product2.Name, product2.product_suite__c, product2.product_platform__c from OpportunityLineitem where OpportunityId = :newOpportunity.id ];
            System.debug('order items' + check);
            
            //Ergo Op
            Opportunity newOpportunityB = new Opportunity();
            newOpportunityB.AccountID = newAccount.Id;
            newOpportunityB.Name = 'Test';
            newOpportunityB.CloseDate = system.today(); 
            newOpportunityB.StageName = 'System Demo';
            newOpportunityB.Quote__c = newQuote.Id; 
            newOpportunityB.Contract_Length__c = '1 Years';
            newOpportunityB.Currency__c = 'EUR';
            newOpportunityB.Currency_Rate__c = 1;
            newOpportunityB.Currency_Rate_Date__c = date.today().addDays(-1);
            insert newOpportunityB;
            
            OpportunityLineitem myItemb = new OpportunityLineitem();
            myItemb.PricebookEntryId = [SELECT id FROM PriceBookEntry WHERE isActive = true LIMIT 1].id;
            myItemb.Quantity = 1;
            myItemb.TotalPrice = 90.00;
            myItemb.OpportunityID = newOpportunityB.Id;
            myItemb.Product2Id = [Select id from Product2 where Name = 'Ergonomics Level 2 Support - Remote' Limit 1].id;
            insert myItemb;        
            
            
            Address__c newAddress = new Address__c();
            newAddress.Account__c = newAccount.Id;
            newAddress.City__c = 'test city';
            newAddress.Country__c = 'United States';
            newAddress.IsPrimary__c = true;
            newAddress.State__c = 'IL';
            newAddress.Street__c = '222 Test Road';
            newAddress.Zip_Postal_Code__c = '612365';     
            
            user u = [Select id from User where Group__c = 'Retention' AND isActive = TRUE Limit 1];
            
            test.startTest();
            
            System.runAs(u) {
                PageReference myVfPage = Page.create_contract;
                myVfPage.getParameters().put('id', newOpportunity.Id);
                Test.setCurrentPage(myVfPage);
                
                //ApexPages.currentPage().getParameters().put('id', newOpportunity.Id);
                ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
                convertToContract myController = new convertToContract(testController);
                System.debug(myController.cont.Billing_Contact__c + ' ' + myController.cont.Shipping_Contact__c );    
                myController.cont.Contact__c = anotherContact.id;
                myController.cont.Billing_Contact__c = null;
                myController.cont.Shipping_Contact__c = null;
                myController.setDefaultContact();
                System.assert(myController.cont.Billing_Contact__c == anotherContact.id && myController.cont.Shipping_Contact__c == anotherContact.id);
                
                myController.newAddress = newAddress;
                myController.cont.Contact__c = null;     
                myController.contractLength = 3;
                myController.cont.Num_of_Locations__c = 0;
                myController.cont.Ergo_Coverage__c = 1;
                myController.cont.EHS_Coverage__c = 2;
                myController.cont.EHS_Coverage_Type__c = null;
                myController.cont.Execute_By__c = 'Specific Date';
                myController.cont.Execute_By_Date__c = null;
                myController.cont.Standard_MSA__c = 'No';
                
                myController.cont.Billing_Contact__c = null;
                myController.cont.Shipping_Contact__c = null;
                myController.cont.Contract_Start_Type__c ='Specific Date';
                
                
                
                //System.assert(myController.contractValid == true);
                //myController.createCont();
                //System.assert(myController.contractValid == false);
                
                myController.cont.Non_Standard_MSA_Type__c = 'Amended';
                myController.cont.Include_Additional_Opportunities__c = true;
                myController.cont.Split_Billing__c = TRUE;
                myController.cont.Split_Billing_Agreement__c = null;
                
                // myController.createCont();
                //  System.assert(myController.contractValid == false);
                
                myController.cont.Num_of_Locations__c = 2;    
                myController.cont.Coverage_Type__c = 'Divisions';
                myController.cont.Ergo_Coverage_Type__c = 'Divisions';    
                myController.cont.EHS_Coverage_Type__c = 'Employees';
                myController.cont.Execute_By_Date__c = System.today().addDays(90);
                myController.cont.Include_Additional_Opportunities__c = false;
                myController.cont.Split_Billing_Agreement__c = 'Test';
                myController.cont.Execute_By_Date__c = System.today().addDays(90);
                myController.cont.Standard_MSA__c = 'Yes';
                myController.setDefaultContact();
                myController.createCont();
                System.debug(myController.contractValid);
                
                
            }
            test.stopTest();
        }
        
        public static testmethod void humantech_ConvertToContract() {
            Account newAccount = new Account();
            newAccount.Name = 'Test Account';
            date d = date.today().addMonths(-1);               
            newAccount.Subscription_End_Date__c = d.month() + '/' + d.day();
            //newAccount.Subscription_End_Date__c = '10/10';
            insert newAccount;
            
            Account relatedAcct = new Account();
            relatedAcct.Name = 'test';
            relatedAcct.Subscription_End_Date__c = '12/12';
            insert relatedAcct;
            
            
            Contact newContact = new Contact();
            newContact.AccountID = newAccount.Id;
            newContact.LastName = 'Test';
            insert newContact;
            
            
            Contact anotherContact = new Contact();
            anotherContact.AccountID = newAccount.Id;
            anotherContact.LastName = 'Test';
            insert anotherContact;
            
            
            Quote__c newQuote = new Quote__c();
            newQuote.Account__c = newAccount.Id;
            newQuote.Contact__c = newContact.Id;
            insert newQuote;
            
            Opportunity newOpportunity = new Opportunity();
            newOpportunity.AccountID = newAccount.Id;
            newOpportunity.Name = 'Test';
            newOpportunity.CloseDate = system.today(); 
            newOpportunity.StageName = 'System Demo';
            newOpportunity.Quote__c = newQuote.Id; 
            newOpportunity.Contract_Length__c = '2 Years';
            newOpportunity.Currency__c = 'EUR';
            newOpportunity.Currency_Rate__c = 1;
            newOpportunity.Suite_Of_Interest__c = 'Ergonomics';
            newOpportunity.Currency_Rate_Date__c = date.today().addDays(-1);
            insert newOpportunity;
            
            id pricebook = [SELECT id FROM PriceBookEntry WHERE isActive = true AND NAME Like 'Humantech%' LIMIT 1].id;
            
            OpportunityLineitem myItem2 = new OpportunityLineitem();
            myItem2.PricebookEntryId = pricebook;
            myItem2.Quantity = 1;
            myItem2.TotalPrice = 1000.00;
            myItem2.OpportunityID = newOpportunity.Id;
            myItem2.Product2Id = [Select id from Product2 where Name = 'Humantech Industrial Ergonomics - Single Sign On (SSO)' Limit 1].id;
            insert myItem2;        
            
            OpportunityContactRole opptyC = new OpportunityContactRole();
            opptyC.OpportunityId = newOpportunity.Id;
            opptyC.ContactID = newContact.Id;
            opptyC.IsPrimary = TRUE;
            insert opptyC;
            
            OpportunityLineitem[] check = [Select id, Product2Id, Product2.Name, product2.product_suite__c, product2.product_platform__c from OpportunityLineitem where OpportunityId = :newOpportunity.id ];
            
            //Ergo Op
            Opportunity newOpportunityB = new Opportunity();
            newOpportunityB.AccountID = newAccount.Id;
            newOpportunityB.Name = 'Test';
            newOpportunityB.CloseDate = system.today(); 
            newOpportunityB.StageName = 'System Demo';
            newOpportunityB.Quote__c = newQuote.Id; 
            newOpportunityB.Contract_Length__c = '4 Years';
            newOpportunityB.Currency__c = 'EUR';
            newOpportunityB.Currency_Rate__c = 1;
            newOpportunityB.Suite_Of_Interest__c = 'Ergonomics';
            newOpportunityB.Currency_Rate_Date__c = date.today().addDays(-1);
            insert newOpportunityB;
            
            OpportunityLineitem myItemb = new OpportunityLineitem();
            myItemb.PricebookEntryId = [SELECT id FROM PriceBookEntry WHERE isActive = true AND NAME Like 'Humantech%'  LIMIT 1].id;
            myItemb.Quantity = 1;
            myItemb.TotalPrice = 90.00;
            myItemb.OpportunityID = newOpportunityB.Id;
            myItemb.Product2Id = [Select id from Product2 where Name = 'Ergonomics Level 2 Support - Remote' Limit 1].id;
            insert myItemb;        
            
            
            Address__c newAddress = new Address__c();
            newAddress.Account__c = newAccount.Id;
            newAddress.City__c = 'test city';
            newAddress.Country__c = 'United States';
            newAddress.IsPrimary__c = true;
            newAddress.State__c = 'IL';
            newAddress.Street__c = '222 Test Road';
            newAddress.Zip_Postal_Code__c = '612365';      
            insert newAddress;
            
            test.startTest();
            
            PageReference myVfPage = Page.create_contract_humantech;
            myVfPage.getParameters().put('id', newOpportunity.Id);
            Test.setCurrentPage(myVfPage); 
            
            //ApexPages.currentPage().getParameters().put('id', newOpportunity.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
            convertToContract myController = new convertToContract(testController);        
            
            myController.clearErgoRelatedOps();
            //myController.ErgoRelatedOppList[0].selected=true;
            myController.ErgoRelatedOppList[1].selected=true;
            myController.SelectedOppId = myController.ErgoRelatedOppList[0].relatedOp.id;
            myController.selectRelatedErgoOpp();
            
            
            myController.selectedBillingAddressId = newAddress.Id;
            myController.selectedShippingAddressId = newAddress.Id;
            myController.selectBillingAddress();
            myController.selectShippingAddress();
            //myController.updateBillTo(); 
            //myController.updateShipTo();
            
            MyController.createCont();      
            
            test.stopTest();
        }
        
        static testmethod void HumanTechContract() {
            Account Acc = new Account();
            Acc.Name = 'Humantech Account';
            insert Acc;
            
            Contact NC = new Contact();
            NC.FirstName = 'Human';
            NC.LastName = 'Tech';
            NC.AccountID = Acc.Id;
            insert NC; 
            
            Quote__c newQuote = new Quote__c();
            newQuote.Account__c = Acc.Id;
            newQuote.Contact__c = NC.Id;
            insert newQuote;
            
            
            Opportunity opp = new Opportunity();
            opp.AccountID = Acc.Id;
            opp.Name = 'Test';
            opp.CloseDate = system.today(); 
            opp.StageName = 'Scoping';
            opp.Quote__c = newQuote.Id; 
            opp.Contract_Length__c = '1 Year';
            opp.Currency__c = 'USD';
            opp.Implementation_Model__c = 'Simple';
            opp.Suite_Of_Interest__c = 'Ergonomics';
            opp.ContactId = NC.id;
            insert opp;
            
            
            
            
            OpportunityLineitem oli = new OpportunityLineitem();
            oli.ProductID__c = [SELECT id, Name FROM Product2 WHERE NAME = 'Humantech Industrial Ergonomics - Single Sign On (SSO)' AND Reoccurring__c = true and Product_Platform__c = 'Ergonomics' AND Product_Suite__c = 'Ergonomics' LIMIT 1].id;
            oli.Product2Id = [SELECT id, Name FROM Product2 WHERE NAME = 'Humantech Industrial Ergonomics - Single Sign On (SSO)' AND Reoccurring__c = true and Product_Platform__c = 'Ergonomics' AND Product_Suite__c = 'Ergonomics'  LIMIT 1].id;
            oli.PricebookEntryId = [SELECT id FROM PriceBookEntry WHERE isActive = true AND NAME = 'Humantech Industrial Ergonomics - Single Sign On (SSO)' and ProductCode = 'Humantech Industrial Ergonomics - Single Sign On (SSO)' and UseStandardPrice = false  LIMIT 1].id;
            oli.Quantity = 1;
            oli.TotalPrice = 1.00;
            oli.OpportunityID = opp.Id;
            insert oli;
    
            
            OpportunityContactRole opptyC = new OpportunityContactRole();
            opptyC.OpportunityId = opp.Id;
            opptyC.ContactID = NC.Id;
            opptyC.IsPrimary = TRUE;
            insert opptyC;
            
            PageReference myPage = Page.create_contract_humantech;
            myPage.getParameters().put('id', opp.Id);
            Test.setCurrentPage(myPage); 
            
            ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
            convertToContract myController = new convertToContract(testController);
            
            myController.SaaSDateCheck();
    
           system.assertEquals(true, mycontroller.Reoccurring);
            
        }
    }