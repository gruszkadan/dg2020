public class campaignMemberUtility {
    
    public static void primarySolutionPOI(sObject[] people, string sObjectType){
        set<id> ids = new set<id>();
        for(sObject p:people){
            ids.add(p.id);
        }
        CampaignMember[] cMembers = new list<CampaignMember>();
        sObjectField primarySolutionPOI;
        
        if(sObjectType == 'Lead'){
            primarySolutionPOI = Lead.Primary_Solution_Product_of_Interest__c;
            cMembers = [SELECT id, Primary_Solution_Product_of_Interest__c, LeadId
                        FROM CampaignMember
                        WHERE LeadId in: ids AND ContactId = null];
        }else if(sObjectType == 'Contact'){
            primarySolutionPOI = Contact.Primary_Solution_Product_of_Interest__c;
            cMembers = [SELECT id, Primary_Solution_Product_of_Interest__c, ContactId
                        FROM CampaignMember
                        WHERE ContactId in: ids];
        }
        
        if(cMembers.size() > 0){
            for(CampaignMember c:cMembers){
                for(sObject p:people){
                    if(sObjectType == 'Lead'){
                        if(c.LeadId == p.id){
                            c.Primary_Solution_Product_of_Interest__c = (string)p.get(primarySolutionPOI);
                        }
                    }else if(sObjectType == 'Contact'){
                        if(c.ContactId == p.id){
                            c.Primary_Solution_Product_of_Interest__c = (string)p.get(primarySolutionPOI);
                        }
                    } 
                }
            }
            try {
                update cMembers;
            } catch(exception e) {
                salesforceLog.createLog(sObjectType, true, 'campaignMemberUtility', 'primarySolutionPOI', string.valueOf(e));
            }
        }
    }
    
    public static void primarySolutionPOIInsert(CampaignMember[] cMembers){
        Contact[] contacts = new list<Contact>();
        Lead[] leads = new list<Lead>();
        set<id> leadIds = new set<id>();
        set<id> contactIds = new set<id>();
        for(CampaignMember cm:cMembers){
            if(cm.ContactId != null){
                contactIds.add(cm.ContactId);
            }else{
                leadIds.add(cm.LeadId);
            }
        }
        if(contactIds.size() > 0){
            contacts = [SELECT id, Primary_Solution_Product_of_Interest__c FROM Contact WHERE id in:contactIds AND Primary_Solution_Product_of_Interest__c != null];
        }
        if(leadIds.size() > 0){
            leads = [SELECT id, Primary_Solution_Product_of_Interest__c FROM Lead WHERE id in:leadIds AND Primary_Solution_Product_of_Interest__c != null];
        }
        if(contacts.size() > 0){
            for(Contact c:contacts){
                for(CampaignMember cm:cMembers){
                    if(c.Primary_Solution_Product_of_Interest__c != null && c.id == cm.ContactId){
                        cm.Primary_Solution_Product_of_Interest__c = c.Primary_Solution_Product_of_Interest__c;
                    }
                }
            }
        }
        if(leads.size() > 0){
            for(Lead l:leads){
                for(CampaignMember cm:cMembers){
                    if(l.Primary_Solution_Product_of_Interest__c != null && l.id == cm.LeadId){
                        cm.Primary_Solution_Product_of_Interest__c = l.Primary_Solution_Product_of_Interest__c;
                    }
                }
            }
        }
    }
    
}