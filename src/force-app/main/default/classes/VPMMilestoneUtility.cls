public class VPMMilestoneUtility {
        public static void milestoneCreation(Case[] cases){
        
        Id[] relatedAccountIds = new List<Id>();
        for(Case c : cases){
            relatedAccountIds.add(c.AccountId);
        } 
        
        VPM_Project__c[] relatedOpenProjects = new List<VPM_Project__c>();
        relatedOpenProjects = [Select Name, Account__c from VPM_Project__c where Account__c IN :relatedAccountIds];       
        
        //new map before milestone creation
        Map<Case, VPM_Project__c> getProject = new Map<Case, VPM_Project__c>();
        for(Case c: Cases){
            for(VPM_Project__c pM: relatedOpenProjects){
                if(pM.Account__c == c.AccountId){
                    getProject.put(c, pM);
                }
            }
        }
        
        //Make a new milestone and map the fields 
        VPM_Milestone__c[] caseMilestones = new List<VPM_Milestone__c>();
        for(Case c: cases){
            VPM_Milestone__c ms = new VPM_Milestone__c();          
            ms.VPM_Project__c = getProject.get(c).id;
            ms.Type__c = 'Training';
            ms.Account__c = c.AccountId;
            ms.OwnerId = c.OwnerId;
            ms.Contact__c = c.ContactId;
            ms.Case__c = c.id;
            if(c.status == 'Active'){
                ms.Status__c ='Open';
            }else {
                ms.Status__c ='Closed';   
            }
            ms.Milestone_Start_Date__c = c.CreatedDate.date();
            caseMilestones.add(ms);           
        }
        try{
            insert caseMilestones;
        }catch(exception e){
            salesforceLog.createLog('Case', true, 'caseUtility', 'milestoneCreation', +string.valueOf(e));
        }  
    }
    
    
    public static void milestoneUpdates(Case[] cases){
        
        Case[] caseMilestones = new List<Case>();
        caseMilestones = [Select id, ContactId, Status, OwnerId, (Select Id, Name, OwnerId, Status__c, Contact__c from VPM_Milestones__r)  from Case where id IN :cases];        
        
        List<VPM_Milestone__c> milestonesToUpdate = new List<VPM_Milestone__c>();
        for(Case c: caseMilestones){
            for (VPM_Milestone__c mlstone: c.VPM_Milestones__r){
                mlstone.OwnerId = c.OwnerId;
                mlstone.Status__c = c.Status;
                mlstone.Contact__c = c.ContactId;
                milestonesToUpdate.add(mlstone);
            }
        }

        if(milestonesToUpdate.size() > 0){
            try{
                update milestonesToUpdate;
            }catch(exception e){
                salesforceLog.createLog('Case', true, 'milestoneUtility', 'milestoneUpdates', +string.valueOf(e));
            }        
        }        
    }
    
    
    public static void milestoneStatusEvaluation(VPM_Task__c[] tasks){
        
        Set<Id> milestoneIds = new Set<Id>();
        for(VPM_Task__c t : tasks){
            milestoneIds.add(t.VPM_Milestone__c);
        }    
        
        List<VPM_Milestone__c> relatedMilestones = new List<VPM_Milestone__c>();    
        relatedMilestones = [Select id, (Select id, Status__c from VPM_Tasks__r) from VPM_Milestone__c where id IN :milestoneIds];   
        
        for(VPM_Milestone__c m: relatedMilestones){
            
            boolean hasOpenTasks = false;
            for(VPM_Task__c t: m.VPM_Tasks__r){
                if(t.Status__c != 'Complete'){
                    hasOpenTasks = true;
                }
            }
            if(hasOpenTasks == true){
                m.Milestone_End_Date__c = NULL;
                m.Status__c = 'Open';
            }else{
                m.Milestone_End_Date__c = System.today();
                m.Status__c = 'Closed';
            }
        }

        try{
            update relatedMilestones;
        }catch(exception e){
            salesforceLog.createLog('VPM_Task__c', true, 'milestoneUtility', 'milestoneStatusEvaluation', +string.valueOf(e));
        }        
    }
    
    
    public static void updateMilestoneEndDate(VPM_Milestone__c[] milestones){
        for(VPM_Milestone__c m: milestones){
            if(m.Status__c != 'Closed'){
                m.Milestone_End_Date__c = NULL;
            }
            if(m.Status__c == 'Closed'){
                m.Milestone_End_Date__c = System.today();
            }
        }
    }
}