/*
* This is the controller for the order items table used in the sales performance table
* Test class: testSalesPerformanceTable
*/
public with sharing class salesPerformanceOrderSetController {
    public string searchName {get;set;}
    public string recPerPage {get;set;}
    public string groupRoleRepName {get;set;}
    public string spoType {get;set;}
    public string sortFieldSave;
    string saveSearchName;
    boolean managerOnly;
    string query;
    string viewType;
    string groupRoleRepId;
    public string month {get;set;}
    string year; 
    public Sales_Performance_Order__c spo {get;set;}
    
    public salesPerformanceOrderSetController(string xviewType, string xgroupRoleRepId, string xgroupRoleRepName, string xmanagerOnly, string xmonth, string xyear) {
        spoType = 'Bookings';
        recPerPage = '25';
        viewType = xviewType; 
        groupRoleRepId = xgroupRoleRepId;
        groupRoleRepName = xgroupRoleRepName;
        if (xmanagerOnly == 'true') {
            managerOnly = true;
        } else {
            managerOnly = false;
        }
        month = xmonth;
        year = xyear;
        buildQuery();  
    }
    
    public ApexPages.StandardSetController con {
        get {
            if (con == null) {
                con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
                con.setPageSize(integer.valueOf(recPerPage));
            }
            return con;
        }
        set;
    }
    
    public SelectOption[] getrecPerPageOptions() {
        SelectOption[] ops = new list<SelectOption>();
        ops.add(new SelectOption('10', '10'));
        ops.add(new SelectOption('25', '25'));
        ops.add(new SelectOption('50', '50'));
        ops.add(new SelectOption('100', '100'));
        ops.add(new SelectOption('200', '200'));
        return ops;
    }
    
    public Sales_Performance_Order__c[] getorders() {
        return (Sales_Performance_Order__c[])con.getRecords();
    }
    
    public void searchItems() {
        saveSearchName = searchName;
        buildQuery();
    }
    
    public void buildQuery() {
        con = null;
        string groupRoleRepIdType = '';
        if (groupRoleRepId.left(3) == '005') {
            groupRoleRepIdType = 'Rep';
        } else {
            if (viewType == 'Group') {
                groupRoleRepIdType = 'Group';
            } else if (viewType == 'Role') { 
                groupRoleRepIdType = 'Role';
            }
        }
        query = 'SELECT id, OwnerId, Owner.Name, Account__c, Account__r.Name, Account__r.AdminID__c, Order_ID__c, Order_Date__c, Contract_Length__c, Role__c, Month__c, Booking_Amount__c, Group__c, Incentive_Amount__c, Type__c, ' +
            '(SELECT id, Booking_Amount__c, Product_Name__c, Order_Item_ID__c, Status__c FROM Sales_Performance_Items__r) ' +
            'FROM Sales_Performance_Order__c ' +
            'WHERE Year__c = \'' + year + '\' ';
        if(month != null){
            query += 'AND Month__c = \'' + month + '\' ';
        }
        if (groupRoleRepIdType == 'Rep') {
            if (!managerOnly) {
                if (viewType != 'My Team') {
                    query += 'AND OwnerId = \'' + groupRoleRepId + '\' ';
                } else {
                    query += 'AND (OwnerId = \'' + groupRoleRepId + '\' OR Sales_Manager__c = \'' + groupRoleRepId + '\' OR Sales_Director__c = \'' + groupRoleRepId + '\' OR Sales_VP__c = \'' + groupRoleRepId + '\') ';
                }
            } else {
                query += 'AND OwnerId = \'' + groupRoleRepId + '\' ';
            }
            if (spoType == 'Bookings') {
                //query += 'AND Type__c =\'Booking\' ';
            } else {
                query += 'AND Incentive_Amount__c > 0 ';
            }
        } else if (groupRoleRepIdType == 'Group') {
            if (groupRoleRepId != 'Group') {
                query += 'AND Group__c = \'' + groupRoleRepId + '\' ';
            } else {
                query += 'AND Group__c != null ';
            }
        } else if (groupRoleRepIdType == 'Role') {
            if (groupRoleRepId != 'Role') {
                query += 'AND Role__c = \'' + groupRoleRepId + '\' ';
            } else {
                query += 'AND Role__c != null ';
            }
        }
        if (saveSearchName != null) {
            query += 'AND Account__r.Name LIKE \'%' + string.escapeSingleQuotes(SaveSearchName) + '%\' ';
        }
        query += 'ORDER BY ' + string.escapeSingleQuotes(sortField) + ' ' + string.escapeSingleQuotes(SortDirection) + ' ';
        System.debug('spoSetConQuery = '+query);
    }
    
    public string sortDirection {
        get { 
            if (sortDirection == null) { 
                sortDirection = 'asc'; 
            } 
            return SortDirection;  
        }
        set; 
    }
    
    public string sortField {
        get { 
            if (sortField == null) {
                sortField = 'Order_Date__c'; 
            } 
            return sortField; 
        }
        set; 
    }
    
    public void sortToggle() {
        SortDirection = SortDirection.equals('asc') ? 'desc NULLS LAST' : 'asc';
        if (SortFieldSave != SortField) {
            SortDirection = 'asc';
            SortFieldSave = SortField;
        }
        buildQuery();
    }
    
    public string nowDisplaying {
        get {
            if (con != null) {
                string s = '';
                integer pageNum = con.getPageNumber();
                integer disp1 = ((pageNum - 1) * integer.valueOf(recPerPage)) + 1;
                integer disp2 = (pageNum * integer.valueOf(recPerPage));
                if (disp2 > con.getResultSize()) {
                    disp2 = con.getResultSize();
                }
                s = 'Displaying ' + disp1 + '-' + disp2 + ' of ' + con.getResultSize();
                return s;
            } else {
                return null;
            }
        }
        set;
    }
    
    public boolean hasNext {  
        get {  
            if (con != null) {
                return con.getHasNext();  
            } else {
                return null;
            } 
        }
        set;  
    }  
    
    public boolean hasPrev {  
        get {  
            if (con != null) {
                return con.getHasPrevious();  
            } else {
                return null;
            } 
        }
        set;  
    }  
    
    public void next() { 
        con.next();
    }
    
    public void prev() { 
        con.previous();
    }
    /*
    public PageReference disputeView() {
        PageReference pr = page.order_detail_incentive;
        pr.getParameters().put('id', ApexPages.currentPage().getParameters().get('orderId'));
        return pr.setRedirect(true);
    }
    */
    public void viewOrder() {
        string spoId = Apexpages.currentPage().getParameters().get('spoId');
        if (spoId != null) {
            for (Sales_Performance_Order__c o : getorders()) {
                if (o.id == spoId) {
                    spo = o;
                }
            }
        }
    }
    
    
}