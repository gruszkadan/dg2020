@isTest(SeeAllData = true)
private class testProposalBuilderPreview {
    
    static testMethod void QuoteOptions(){
        
        
        //insert test option package
        Quote_Options__c qop = new Quote_Options__c(Type__c='New');
        insert qop;
        
        Quote__c[] options = new List<Quote__c>();
       
        for (integer i=0; i<2; i++) {
            Quote__c q = new Quote__c();
            q.Quote_Options__c = qop.id;
            q.Include_Option_In_Proposal__c = true;
            options.add(q);
        }
        options[0].Main_Quote_Option__c = true;
        options[0].Approve_Expired_Promo_Pricing_Terms__c = false;
        options[1].Approve_Expired_Promo_Pricing_Terms__c = true;
        insert options;
        
        ApexPages.currentPage().getParameters().put('id', options[0].id);
        proposalBuilderPreview con = new proposalBuilderPreview();
        
        con.createAttachments();
        
        Quote__c quote = [Select Id, Name, Status__c, (Select Id, Name From Attachments) From Quote__c WHERE Id = :options[0].id];
        
        //test that quote is sent through approval process & that attachment with PDF was inserted and named correctly
        system.assertEquals(quote.Status__c, 'Pending Approval');
        system.assertEquals(quote.Attachments[0].Name, quote.Name + '-v' + quote.Attachments.size() + '.pdf');
        
    }
    
    static testMethod void Quote(){
        
        Quote__c testQuote = new Quote__c();
        insert testQuote;
        
        ApexPages.currentPage().getParameters().put('id', testQuote.id);
        proposalBuilderPreview con = new proposalBuilderPreview();
        
        con.createAttachments();
        
		Quote__c quote = [Select Id, Name, Status__c, (Select Id, Name From Attachments) FROM Quote__c WHERE Id = :testQuote.id];

       //test that quote doesn't go through approval process but attachment is created for quote and named correctly
        system.assertEquals(quote.Status__c, 'Active');
        system.assertEquals(quote.Attachments[0].Name, quote.Name + '-v' + quote.Attachments.size() + '.pdf');
		        
        
        testQuote.Approve_Expired_Promo_Pricing_Terms__c = true;
        update testQuote;
        
        con = new proposalBuilderPreview();
        con.createAttachments();
        
        Quote__c quote2 = [Select Id, Name, Status__c, (Select Id, Name From Attachments) FROM Quote__c WHERE Id = :testQuote.id];
        
        //test that quote does get through approval process and attachment is created for quote and named correctly
        system.assertEquals(quote2.Status__c, 'Pending Approval');
        system.assertEquals(quote2.Attachments[1].Name, quote2.Name + '-v' + quote2.Attachments.size() + '.pdf');
        
        
    }
    
    
}