@isTest

private class testVPMMilestoneDetail {
    
    static testmethod void VPMMilestoneDetail() {     
        
        VPM_Project__c proj = new VPM_Project__c();
        
        Account testAccount = new Account(name = 'Ace Hardware');
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.AccountId = testAccount.id;
        testContact.LastName = 'Texas';
        insert testContact;
        
        proj.Account__c = testAccount.id;
        proj.Contact__c = testContact.id;
        insert proj;
        
        VPM_Milestone__c milestone = new VPM_Milestone__c(VPM_Project__c = proj.Id, Account__c = testAccount.id);
        insert milestone;
        
        VPM_Task__c task = new VPM_Task__c(VPM_Project__c = proj.Id, Status__c = 'Not Started', Contact__c = testContact.id, Start_Date__c = Date.today(), VPM_Milestone__c = milestone.id);
        VPM_Note__c note = new VPM_Note__c(VPM_Milestone__c = milestone.id);
        
        insert task;
        insert note;
        
        ApexPages.currentPage().getParameters().put('Id', milestone.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new VPM_Milestone__c());
        VPMMilestoneDetail myController = new VPMMilestoneDetail(testController);
        
        //Test that the FullPhotoUrls are being stored with OwnerIds of task; map being populated
        system.assertEquals(myController.taskPhotos.Size(), 1);
        
        //inlineEdit changes to true when method is run
        myController.inputField();
        system.assertEquals(myController.inlineEdit, true);
        
        myController.getContacts();
        myController.primaryContact = testContact.LastName;
        myController.saveMilestone();
        
        ApexPages.currentPage().getParameters().put('editNoteId', note.Id);
        myController.selectNoteToEdit();
        
        myController.saveEditedNote();
        
        ApexPages.currentPage().getParameters().put('selRecordType', 'VPM_Task__c');
        ApexPages.currentPage().getParameters().put('selRecordId', task.id);
        
        myController.querySelectedRecord();
        myController.resetRightPanelView();
        
        myController.newNote.type__c = 'Configuration';
        myController.newNote.comments__c = 'Hello Sir';
        
        //Check that note was added to the list
        myController.addNote();
        system.assertEquals(myController.notes.size(), 2);
        
        myController.toggleModal();
        
    }
    
}