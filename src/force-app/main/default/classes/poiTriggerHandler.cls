public class poiTriggerHandler {
    
    // AFTER UPDATE
    public static void afterUpdate(Product_Of_Interest__c[] oldPois, Product_Of_Interest__c[] newPois) {
        Product_Of_Interest__c[] suitesToResolve = new list<Product_Of_Interest__c>();
        for (integer i=0; i<newPois.size(); i++) {
            if (oldPois[i].Open_Suites_Needing_Resolution__c != newPois[i].Open_Suites_Needing_Resolution__c
                || oldPois[i].Contact__c == null && newPois[i].Contact__c != null)  {
                    suitesToResolve.add(newPois[i]);
                }
        } 
        // If there are any open suites, set the suite on the account or lead
        if (suitesToResolve.size() > 0) {
            poiUtility.suitesToResolve(suitesToResolve);
        }
    }
}