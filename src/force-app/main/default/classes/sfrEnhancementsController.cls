public class sfrEnhancementsController {
    public User u {get;set;}
    public request[] dept_reqs {get;set;}
    public request[] reqs {get;set;}
    public id enhancementsQueue {get;set;}
    public id enhancementsDevQueue {get;set;}
    public string selId {get;set;}
    public string colorGuide {get;set;}
    // buttons
    public boolean viewRequestsTBA {get;set;}
    public boolean viewEnhancements {get;set;}
    public boolean viewProjects {get;set;}
    public boolean viewDepartmentManagement {get;set;}
    public boolean viewStatusWall {get;set;}
    public boolean viewAll {get;set;}
    // list of statuses that are considered closed
    string[] closedList = 'Completed;Cannot Add;Chosen Not to Add;Duplicate Request;Unable to Complete'.split(';', 0);
    
    public sfrEnhancementsController() {
        u = [SELECT id, Name, FirstName, LastName, Email, Department__c, Role__c, ProfileId, Profile.Name 
             FROM User 
             WHERE id = :UserInfo.getUserId() LIMIT 1];
        viewButtons();
        enhancementsQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.DeveloperName = 'SF_Requests_Enhancements' LIMIT 1].Queueid;
        enhancementsDevQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.DeveloperName = 'Enhancements_Dev_Work_Queue' LIMIT 1].Queueid;
        colorGuide = 'Sales#F88962,Marketing#4BC076,CC - Chicago#7F8DE1,CC - Oakville#e3d067,Product Management#EF7EAD,VelocityEHS#0C8EFF';
        find(); 
    } 
    
    public void viewButtons() {
        viewRequestsTBA = Salesforce_Request_Settings__c.getInstance().View_Requests_TBA__c;
        viewEnhancements = Salesforce_Request_Settings__c.getInstance().View_Enhancements__c;
        viewProjects = Salesforce_Request_Settings__c.getInstance().View_Projects__c;
        viewDepartmentManagement = Salesforce_Request_Settings__c.getInstance().View_Department_Management__c;
        viewStatusWall = Salesforce_Request_Settings__c.getInstance().View_Status_Wall__c;
        if (viewRequestsTBA && viewEnhancements && viewProjects && viewDepartmentManagement && viewStatusWall) {
            viewAll = true;
        }
    }
    
    public void find() {
        dept_reqs = new List<request>();
        reqs = new List<request>();
        for (Salesforce_Request__c sfr : [SELECT id, Name, OwnerId, Status__c, Summary__c, Request_Type__c, Sort_Order__c, Requested_Date__c, Requested_By__c, Requested_By__r.Name, Priority__c, Department__c, Department_Sort_Order__c
                                          FROM Salesforce_Request__c 
                                          WHERE Request_Type__c = 'Enhancement' 
                                          AND OwnerId = :enhancementsQueue
                                          AND Status__c NOT IN :closedList
                                          ORDER BY Department_Sort_Order__c NULLS LAST]) {
                                              request req = new request(sfr);
                                              dept_reqs.add(req);
                                          }
        for (Salesforce_Request__c sfr : [SELECT id, Name, OwnerId, Status__c, Summary__c, Request_Type__c, Sort_Order__c, Requested_Date__c, Requested_By__c, Requested_By__r.Name, Priority__c, Department__c, Department_Sort_Order__c
                                          FROM Salesforce_Request__c 
                                          WHERE Request_Type__c = 'Enhancement' 
                                          AND OwnerId = :enhancementsDevQueue
                                          AND Status__c NOT IN :closedList
                                          ORDER BY Sort_Order__c NULLS LAST]) {
                                              request req = new request(sfr);
                                              reqs.add(req);
                                          }
    }
    
    public PageReference gotoHome() {
        PageReference pr = new PageReference('/apex/sf_request_tab');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoTBA() {
        PageReference pr = new PageReference('/apex/sfrTBA');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoProjects() {
        PageReference pr = new PageReference('/apex/sfrProjects');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoDepartmentManagement() {
        string url = '/apex/sfrDepartmentManagement';
        string url_var = ApexPages.currentPage().getParameters().get('myParam');
        if (url_var != 'User') {
            url += '?dept='+url_var;
        }
        PageReference pr = new PageReference(url);
        return pr.setRedirect(true);
    }
    
    public PageReference gotoStatusWall() {
        PageReference pr = new PageReference('/apex/sfrStatusWall');
        return pr.setRedirect(true);
    }
    
    public PageReference view() {
        PageReference pr = new PageReference('/'+selId);
        return pr.setRedirect(true);
    }
    
    public void acceptOwnership() {
        id selReq = ApexPages.currentPage().getParameters().get('selreqid');
        for (request req : reqs) {
            if (req.sfr.id == selReq) {
                req.sfr.OwnerId = u.id;
                update req.sfr;
            }
        }
    }
    
    public class request {
        public Salesforce_Request__c sfr {get;set;}
        public string truncSummary {get;set;}
        public string color {get;set;}
        public string dept {get;set;}
        
        public request(Salesforce_Request__c req) {
            sfr = req;
            dept = req.Department__c;
            truncSummary = '';
            if (sfr.Summary__c != null) {
                truncSummary = sfr.Summary__c.abbreviate(100);
            }
            if (dept == 'Sales') {
                color = '#F88962';
            } 
            if (dept == 'Marketing') {
                color = '#4BC076';
            } 
            if (dept == 'Customer Care') {
                color = '#7F8DE1';
            } 
            if (dept == 'Customer Care - Oakville') {
                color = '#e3d067';
            } 
            if (dept == 'Product Management') {
                color = '#EF7EAD';
            } 
            if (dept == 'VelocityEHS') {  // MSDSonline
                color = '#0C8EFF';
            } 
            
        }
    }
    
    
    
}