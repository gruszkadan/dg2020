trigger closeOtherOpportunities on Opportunity (before update) {
    
    Set<id> triggerOppIds = new Set<id>();  // set to hold ids of opportunities in trigger
    Set<id> acctIds = new Set<id>();		// set to hold account ids			
    Set<id> renewalIds = new Set<id>();		// set to hold renewal ids
    
    for (Opportunity opp : trigger.New) {		// loop through trigger list and create sets
        triggerOppIds.add(opp.id);
        acctIds.add(opp.AccountId);
        if (opp.Renewal__c != null) {
            renewalIds.add(opp.Renewal__c);
        }
    }
    
    Opportunity[] openOpps = [SELECT id, Renewal__c, AccountId		// query all open opportunities that have an account id and renewal id in the sets, but aren't one of the trigger opportunities
                              FROM Opportunity
                              WHERE isClosed = false
                              AND id NOT IN :triggerOppIds
                              AND AccountId IN :acctIds
                              AND Renewal__c IN :renewalIds];
    
    Set<Opportunity> oppSet = new Set<Opportunity>();		// set to add opportunities in order to prevent duplicate updates
    Opportunity[] updOpps = new List<Opportunity>();		// final list of opportunities to update
    
    for (Opportunity triggerOpp : trigger.new) {		// loop through opportunities in trigger
        if (triggerOpp.StageName == 'Closed/Won') {		// if the opportunity is closed/won
            for (Opportunity openOpp : openOpps) {		// loop through all the open opportunities
                if (openOpp.id != triggerOpp.id) {		// make sure the open opportunity is not opportunity in the trigger
                    if (openOpp.AccountId == triggerOpp.AccountId && openOpp.Renewal__c == triggerOpp.Renewal__c) {		// find the open opportunities that have an account id and renewal id matching the opportunity in the trigger
                        openOpp.StageName = 'Closed Lost';
                        openOpp.Closed_Reasons__c = 'Term';
                        openOpp.CloseDate = System.today();
                        oppSet.add(openOpp);		// set the values and add to the update set
                    }
                }
            }
        }
    }
    
    if (oppSet.size() > 0) {	// loop through opportunity set and add to update list
        for (Opportunity opp : oppSet) {
            updOpps.add(opp);
        }
    }
    if (updOpps.size() > 0) {	// update the other opportunities
        update updOpps;
    }
}