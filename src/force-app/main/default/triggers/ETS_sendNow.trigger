trigger ETS_sendNow on Survey_Queue__c (after update) {
    //*** if a support survey is made, a workflow will fire 1 hour after creation and check Send_Immediately__c. this trigger will run when that box is checked and will update the contact
    
    Set<id> conSet = new Set<id>();
    Set<id> survSet = new Set<id>();
    Set<id> survSentSet = new Set<id>();
    
    // if send_immediately__c is marked true, add the contact and surveys to sets
    for (integer i=0; i<trigger.new.size(); i++) {
        if (trigger.old[i].Send_Immediately__c == false && trigger.new[i].Send_Immediately__c == true) {
            conSet.add(trigger.new[i].Contact__c);
            survSet.add(trigger.new[i].id);
            survSentSet.add(trigger.new[i].Survey__c);
        }
    }
    
    if (conSet.size() > 0) {
        // query the contacts from the set and set the fields for marketo to read
        Contact[] cons = [SELECT id, ETS_Forward_Survey_URL__c, Last_Transactional_Survey_Sent_Date__c, ETS_Survey_Number__c, ETS_Survey_URL__c, ETS_Survey_Step__c FROM Contact WHERE id IN :conSet];
        for (Survey_Queue__c sq : trigger.new) {
            for (Contact c : cons) {
                if (sq.Contact__c == c.id) {
                    c.ETS_Forward_Survey_URL__c = sq.Survey_Forward_URL__c.substringAfter('://');
                    c.Last_Transactional_Survey_Sent_Date__c = date.today();
                    c.ETS_Survey_Number__c = sq.Survey_Num__c;
                    c.ETS_Survey_URL__c = sq.Survey_URL__c.substringAfter('://');
                    c.ETS_Survey_Step__c = 'Survey';
                }
            }
        }
        update cons;
        
        // delete the queues for the surveys just sent
        delete [SELECT id FROM Survey_Queue__c WHERE id IN :survSet];
        
        // query the survey to update that it has been sent
        Survey_Feedback__c[] sentSurvs = [SELECT id, Survey_Sent__c, Survey_Sent_Date__c FROM Survey_Feedback__c WHERE id IN :survSentSet];
        for (Survey_Feedback__c sf : sentSurvs) {
            sf.Survey_Sent__c = true;
            sf.Survey_Sent_Date__c = date.today();
        }
        update sentSurvs;
    }
}