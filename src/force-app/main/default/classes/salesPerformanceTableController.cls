/*
* Test class: testSalesPerformanceTable
*/
public class salesPerformanceTableController {
    public string viewType {get;set;}
    public string view {get;set;}
    public salesPerformanceTableUtility util {get;set;}
    public User u {get;set;}
    public string selYear {get;set;}
    public User viewUser {get;set;}
    public boolean viewRepPage {get;set;}
    public boolean viewRepList {get;set;}
    public monthWrapper[] monthWrappers {get;set;}
    public trail[] trails {get;set;}
    public decimal ytdTotalBookings {get;set;}
    public decimal ytdTotalQuota {get;set;}
    public decimal ytdTotalPercent {get;set;}
    public salesPerformanceOrderSetController spoSetCon {get;set;}
    public salesPerformanceIncentiveData itDataCon {get;set;}
    public boolean hasPermission {get;set;}
    public string selMonth {get;set;}
    transient Sales_Performance_Summary__c[] summaries; 
    public monthWrapper monthWrapper {get;set;}
    public boolean canViewSideBar {get;set;}
    public boolean canViewTypePicklist {get;set;}
    public id userHeaderId {get;set;}
    string monthRole;
    public String defaultView { get; set; }
    //v1.1 Additions
    public String viewAs { get; set; }
    public String breadcrumbsDefaultView {get;set;}
    public id viewOrderId {get;set;}
    
    
    public salesPerformanceTableController() {
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        
        //Check for sidebar permissions
        canViewSideBar = Sales_Performance_Settings__c.getInstance(viewAs).Bookings_Overview_Sidebar__c;
        canViewTypePicklist = Sales_Performance_Settings__c.getInstance(viewAs).View_Type_Picklist__c;
        
        // init the utility
        util = new salesPerformanceTableUtility();
        
        //Get the Default View to display
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        
        // query the current user's info
        string uAlias = [SELECT Alias FROM User WHERE id = : userInfo.getUserId() LIMIT 1].Alias;
        if (defaultView == 'VP' && viewAs == userInfo.getUserId()) {
            u = [SELECT id, Name, FirstName, LastName, ManagerID, Manager.Name, Sales_Director__c, Sales_Director__r.Name, Sales_VP__c, Sales_VP__r.Name, Department__c, Group__c, 
                 Role__c, Sales_Team__c, ProfileId, Profile.Name, FullPhotoURL
                 FROM User 
                 WHERE UserRole.Name = 'VP Sales' AND isActive = true and LastName != 'Haling' LIMIT 1];
        } else {
            u = [SELECT id, Name, FirstName, LastName, ManagerID, Manager.Name, Sales_Director__c, Sales_Director__r.Name, Sales_VP__c, Sales_VP__r.Name, Department__c, Group__c, 
                 Role__c, Sales_Team__c, ProfileId, Profile.Name, FullPhotoURL
                 FROM User 
                 WHERE id = : viewAs LIMIT 1];
        }
        breadcrumbsDefaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
        userHeaderId = u.id;
        
        
        
        
        
        
        // query and organize the data
        createView();
    } 
    
    public void createView() {
        // Find the year to display and the month to open to by default
        findMonthYear();
        // Find the current viewer and the trail of viewers
        findView();
        // Query for the order summary records
        checkPermissions(); 
        querySummaries();
        // Create wrappers for the orders for each month
        createMonthWrappers();
        // Calculate the YTD totals
        calculateYtdTotals();
        // Initial query to show orders
        initialOrdersQuery();
        // Find which month to default to
        findMonthWrapper();
    }
    
    // Change the view type
    public PageReference newView() {
        PageReference pr = page.sales_performance_overview;
        if(viewAs != userInfo.getUserId()){
            pr.getParameters().put('va', viewAs);
        }else if(canViewTypePicklist){
            pr.getParameters().put('viewtype', viewType);
        }
        pr.getParameters().put('y', selYear);
        return pr.setRedirect(true);
    }
    
    public void findMonthYear() {
        // If a year is provided in the params, use that year
        selYear = ApexPages.currentPage().getParameters().get('y');
        // Otherwise default to this year
        if (selYear == null) {
            selYear = string.valueOf(date.today().year());
        }
        // If a month is provided in the params, use that month to default open to
        selMonth = ApexPages.currentPage().getParameters().get('m');
    }
    
    // Gets the view details - we will use this to determine:
    // 1) Who's performance is being viewed
    // 2) The trail of views that lead up to the current view (trails aka breadcrumbs)
    // 3) Are we viewing the performance of an individual rep? (viewRepPage)
    // 4) Are we viewing a list of sales reps' performances to display incentives column? (viewRepList)
    public void findView() {
        
        // Boolean to determine whether or not a breadcrumb should be linkable (default to false)
        boolean isCurrentView = false;
        
        // Boolean to signal whether or not a list of reps is being displayed (to show incentives column)
        viewRepList = false;
        
        // Boolean to signal whether or not a single rep is being viewed
        viewRepPage = false;
        
        // List of ids for the trail
        id[] repViewIds = new list<id>();
        
        // If viewing the rep page is passed through params
        if (ApexPages.currentPage().getParameters().get('rv') == 'true') {
            viewRepPage = true;
        } 
        
        // Init a list of trails (breadcrumbs)
        trails = new list<trail>();
        
        // Get current view type: "My Team", "Group", "Role"
        viewType = ApexPages.currentPage().getParameters().get('viewtype'); 
        
        // If no viewtype is passed, default to "My Team"
        if (viewType == null) {
            viewType = 'My Team';
        }
        
        // Get the trails passed through the params
        string t1 = ApexPages.currentPage().getParameters().get('t1'); // trail 1
        string t2 = ApexPages.currentPage().getParameters().get('t2'); // trail 2
        string t3 = ApexPages.currentPage().getParameters().get('t3'); // trail 3
        string t4 = ApexPages.currentPage().getParameters().get('t4'); // trail 4
        
        // Start our query for the reps from the trails
        string repQuery = 'SELECT id, Name, Role__c FROM User WHERE id IN : repViewIds';
        
        // If no view is passed through params, the user must be viewing the default table landing page
        if (ApexPages.currentPage().getParameters().get('view') == null) {
            
            System.debug('view params are null');
            
            // It's the first view, so there should be no links in the breadcrumb trail
            isCurrentView = true;
            
            // For the "My Team" viewtype
            if (viewType == 'My Team') {
                
                // Set the view and viewUser to the current user 
                view = u.id;
                viewUser = u;
                
                // If the person being viewed isnt a VP, Director, or Sales Manager, it must be a rep
                if (viewUser.Role__c != 'VP' && viewUser.Role__c != 'Director' && viewUser.Role__c != 'Sales Manager') {
                    viewRepPage = true;
                }
                
                // If the person being viewed is a Sales Manager and we aren't viewing their individual orders, we are viewing a list of reps. Use this to
                //display the incentives column
                if (viewUser.Role__c == 'Sales Manager' && !viewRepPage) {
                    viewRepList = true;
                } 
                //if a Director or Manager then create the VP breadcrumb trail
                if(breadcrumbsDefaultView == 'Director' || breadcrumbsDefaultView == 'Manager'){
                    trails.add(new trail(u.Sales_VP__r.Name, u.Sales_VP__r.id, false));
                }
                //if a Manager then create Director breadcrumb trail
                if(breadcrumbsDefaultView == 'Manager'){
                    trails.add(new trail(u.Sales_Director__r.Name, u.Sales_Director__r.id, false));
                }
                // Add the view user to the trails. This will be the only trail
                trails.add(new trail(viewUser.Name, view, isCurrentView));
            } else {
                // For the "Group" and "Role" viewtypes
                // Create a trail with the name of the viewtype. So the first trail for "Group" will just say "Group"
                view = viewType;
                trails.add(new trail(view, view, isCurrentView));
            }
            
            // Otherwise if a view is supplied
        } else {
            
            view = ApexPages.currentPage().getParameters().get('view'); 
            
            // For the "My Team" viewtype
            if (viewType == 'My Team') { 
                
                // check to see what the rep's role was for that month in time
                if (selMonth != null) {
                    if (!test.isRunningTest() && defaultView != 'Manager' && defaultView != 'Director' && defaultView != 'VP') {
                        monthRole = [SELECT Role__c 
                                     FROM Sales_Performance_Summary__c 
                                     WHERE OwnerId = : view
                                     AND Month__c = : selMonth 
                                     //AND Year__c = : selYear
                                     LIMIT 1].Role__c;
                    } else {
                        monthRole = 'VP';
                    }
                }
                
                System.debug('monthRole is ' + monthRole);
                
                // For each trail, add the trail (the user id) to the list of ids
                if (t1 != null) { 
                    repViewIds.add(t1); 
                }
                if (t2 != null) {
                    repViewIds.add(t2);
                }
                if (t3 != null) { 
                    repViewIds.add(t3); 
                }
                if (t4 != null) { 
                    repViewIds.add(t4); 
                }
                
                // Also add the current view to the list of ids
                repViewIds.add(view); 
                
                // Query the trail of reps
                User[] repViews = Database.query(repQuery);
                
                // Loop through the list of repViewIds
                for (integer i=0; i<repViewIds.size(); i++) {
                    
                    // Loop through the list of reps we queried
                    for (User v : repViews) {
                        
                        // If the user's id equals the repViewId
                        if (v.id == repViewIds[i]) {
                            
                            // If the user's id equals the current view we are viewing that user's view
                            if (v.id == view) {
                                viewUser = v;
                                
                                // If the person being viewed isnt a VP, Director, or Sales Manager, we must be viewing the rep page
                                //if (viewUser.Role__c != 'VP' && viewUser.Role__c != 'Director' && viewUser.Role__c != 'Sales Manager') {
                                if (monthRole != 'VP' && monthRole != 'Director' && monthRole != 'Sales Manager') {    
                                    viewRepPage = true;
                                }
                                
                                // If the person being viewed is a Sales Manager and we aren't viewing that Sales Manager's individual orders, we are viewing a list of reps
                                //if (viewUser.Role__c == 'Sales Manager' && !viewRepPage) {
                                if (monthRole == 'Sales Manager' && !viewRepPage) {
                                    viewRepList = true;
                                }
                            }
                            
                            // The last breadcrumb in the trail is the current view. This should not be linkable, but the ones before it will be
                            if (i == repViewIds.size() - 1) {
                                isCurrentView = true;
                            }
                            
                            // Finally add the trail to the list of trails
                            trails.add(new trail(v.Name, v.id, isCurrentView));
                        }
                    }
                }
                
                // Otherwise the viewtype must be "Group" or "Role"
            } else {
                
                // Grab the trails from the params. In this case, the trails being passed might not be ids, rather the names of groups or roles
                if (t1 != null) {
                    trails.add(new trail(t1, t1, isCurrentView));
                }
                if (t2 != null) {
                    trails.add(new trail(t2, t2, isCurrentView));
                }
                
                // If the view is an id, we know we are viewing an individual rep's performance. Add that rep to the trails
                if (view.left(3) == '005') {
                    repViewIds.add(view);
                    User[] repViews = Database.query(repQuery);
                    viewUser = repViews[0];
                    isCurrentView = true;
                    viewRepPage = true;
                    viewRepList = false;
                    trails.add(new trail(repViews[0].Name, repViews[0].id, isCurrentView));
                    
                    //System.debug('GROUP/ROLE - WITH view - view user is ' + viewUser.Name);
                    
                    // Otherwise the view is just going to be a role or group, add that to the trails
                } else {
                    isCurrentView = true;
                    trails.add(new trail(view, view, isCurrentView));
                    if (view != 'Group' && view != 'Role') {
                        viewRepList = true;
                    }
                }
            }
        }
        
        // *** The trails have now been made and the view set
        
        // Loop through our trails
        if (trails.size() > 0) {
            for (integer i=0; i<trails.size(); i++) {
                // If it's the last trail in the list, there should be no little ">>" next to the name
                if (i == (trails.size() - 1)) {
                    trails[i].hasNext = false;
                }
            }
        }
    }
    
    public void checkPermissions() {
        // Set the boolean permission to FALSE by default
        hasPermission = true;
        // Make sure the user isn't navigating to a page using URL params
        map<string, string> params = ApexPages.currentPage().getParameters();
        if (params.size() > 0) {
            if (params.size() > 0 && params.size() <= 3) {
                for (string key : params.keySet()) {
                    if (key == 'viewType') {
                        if (!canViewTypePicklist) {
                            hasPermission = false;
                        }
                    } else if(key != 'va' && key != 'y'){
                        hasPermission = false;
                    }
                }
            }
            else {
                hasPermission = false;
            } 
        } 
    }
    
    public void initialOrdersQuery() {
        //string xviewType, string xgroupRoleRepId, string xgroupRoleRepName, string xmanagerOnly, string xmonth, string xyear
        //If viewing a month OR if on the rep page
        if(selMonth != null || viewRepPage){
            if (viewUser != null) {
                if (viewRepPage && (viewUser.Role__c == 'VP' || viewUser.Role__c == 'Director' || viewUser.Role__c == 'Sales Manager')) {
                    spoSetCon = new salesPerformanceOrderSetController(viewType, view, viewUser.Name, 'true', selMonth, selYear);
                    itDataCon = new salesPerformanceIncentiveData(viewType, view, viewUser.Name, 'true', selMonth, selYear);
                } else {
                    spoSetCon = new salesPerformanceOrderSetController(viewType, view, viewUser.Name, null, selMonth, selYear);
                    if(viewUser.Role__c != 'VP' && viewUser.Role__c != 'Director' && ((viewType != 'Group' && viewType != 'Role') || viewRepPage == true)){
                        itDataCon = new salesPerformanceIncentiveData(viewType, view, viewUser.Name, null, selMonth, selYear);
                    }
                }
            } else {
                spoSetCon = new salesPerformanceOrderSetController(viewType, view, view, null, selMonth, selYear);
                //itDataCon = new salesPerformanceIncentiveData(viewType, view, view, null, selMonth, selYear);
            }
        }
    }
    
    // query the sps records
    public void querySummaries() {
        
        // Start the SPS query
        string q = 'SELECT id, OwnerId, Manager__c, Month__c, Month_Number__c, My_Bookings__c, Group__c, Quota__c, Quota_Percent__c, Role__c, Sales_Team_Manager__c, Year__c, '+
            'Total_Bookings__c, Owner.Name, YTD_My_Bookings__c, YTD_Total_Bookings__c, YTD_Quota__c, YTD_Quota_Percent__c, MTD_Total_Incentives__c, Sales_Rep__r.Start_Date__c, SIC_Group__c '+
            'FROM Sales_Performance_Summary__c '+
            'WHERE Year__c = \'' + selYear + '\' ';  
        //q += 'AND Month__c = \'June\' ';  
        // For "My Team"
        if (viewType == 'My Team') {
            
            string vu;
            if (monthRole != null) {
                vu = monthRole;
            } else {
                vu = viewUser.Role__c;
            }
            
            // If the person being viewed isnt a VP, Director, or Sales Manager, just query their SPS's
            if (vu != 'VP' && vu != 'Director' && vu != 'Sales Manager') {
                q += 'AND OwnerId = \'' + view + '\' ';
                // Otherwise if a VP, Director, or Sales Manager
            } else {
                // If we are viewing their individual performance page, just query their SPS's
                if (viewRepPage) {
                    q += 'AND OwnerId = \'' + view + '\' ';
                } else {
                    // Otherwise query all SPS's that look up to them
                    q += 'AND (Sales_Team_Manager__c = \'' + view + '\' OR Manager__c = \'' + view + '\') ';
                }
            }
            q += 'ORDER BY Month__c, Owner.FirstName';
            
            // For "Group"
        } else if (viewType == 'Group') {
            // The default view for the group view is just "Group". In this case we would query every SPS for the year
            // If the view is not "Group"
            if (view != 'Group') {
                // If the view is a rep, query the SPS's owned by that rep
                if (view.left(3) == '005') {
                    q += 'AND OwnerId = \'' + view + '\' ';
                } else {
                    // Otherwise query the SPS's associated with that group
                    q += 'AND Group__c = \'' + view + '\' ';
                }
            } 
            q += 'ORDER BY Month__c, Group__c, Owner.FirstName';
            
            // For "Role", the logic is the same as "Group"
        } else if (viewType == 'Role') {
            if (view != 'Role') {
                if (view.left(3) == '005') {
                    q += 'AND OwnerId = \'' + view + '\' ';
                } else {
                    q += 'AND Role__c = \'' + view + '\' ';
                }
            } 
            q += 'ORDER BY Month__c, Role__c, Owner.FirstName';
        }
        // Finally, query our summaries
        summaries = Database.query(q);
        System.debug(q);
        System.debug('# summaries queried = ' + summaries.size());
    } 
    
    // for every month, create a wrapper to hold all that month's data
    public void createMonthWrappers() {
        monthWrappers = new list<monthWrapper>();
        if (summaries.size() > 0) {
            // call the month utility and loop through the months
            for (string m : util.months()) {
                Sales_Performance_Summary__c[] monthlySummaries = new list<Sales_Performance_Summary__c>();
                for (Sales_Performance_Summary__c summary : summaries) {
                    if (summary.Month__c == m) {
                        monthlySummaries.add(summary);
                    }
                }
                // Add a month wrapper to the list passing the month, the view type, all the summaries for that month, and whether or not we are viewing the rep page
                // We are passing viewRepPage to determine when to display a VP/Director/Manager's team numbers or individual numbers
                monthWrappers.add(new monthWrapper(m, viewType, view, monthlySummaries, viewRepPage));
            }
        }
    }
    
    // Month wrapper class - there will always be 12 of these in the monthWrappers list
    public class monthWrapper {
        public string month {get;set;}
        public decimal mtdTotal {get;set;}
        public decimal mtdQuota {get;set;}
        public decimal mtdPercent {get;set;}
        public decimal ytdTotal {get;set;}
        public decimal ytdQuota {get;set;}
        public decimal ytdPercent {get;set;}
        public orderWrapper[] orderWrappers {get;set;}
        public boolean isManager {get;set;}
        transient string viewType;
        transient string view;
        transient Sales_Performance_Summary__c[] summaries;
        transient boolean viewRepPage;
        
        public monthWrapper(string xmonth, string xviewType, string xview, Sales_Performance_Summary__c[] xsummaries, boolean xviewRepPage) {
            // set the passed data
            month = xmonth;
            viewType = xviewType;
            view = xview;
            summaries = xsummaries;
            viewRepPage = xviewRepPage;
            
            // Default the totals to 0. These are the totals for EVERYONE in the month
            mtdTotal = 0;
            mtdQuota = 0;
            mtdPercent = 0.0;
            
            // create the order wrappers based on the viewtype and view
            orderWrappers = new list<orderWrapper>();
            if (viewType == 'My Team') {
                myTeamOrderWrappers();
            } else { 
                groupRoleOrderWrappers();
            }
        }
        
        // create a list of order wrappers for the "My Team" viewtype
        public void myTeamOrderWrappers() {
            // Loop through our summaries
            for (Sales_Performance_Summary__c s : summaries) {
                
                // Add an order wrapper for each summary passing the viewtype, the view, a list of summaries (in this case, just 1), and the month being worked
                orderWrappers.add(new orderWrapper(viewType, view, new list<Sales_Performance_Summary__c>{s}, month));
                
                // If the summary is the current view's summary (i.e. Eric is viewing his and his team's summaries)
                if (s.OwnerId == view) {
                    // If we are viewing the rep page, only use "My" bookings so we don't see the team's total bookings (i.e. Chuck would only want to see his bookings when he clicks his name)
                    if (viewRepPage) {
                        mtdTotal = s.My_Bookings__c.setScale(0);
                        ytdTotal = s.YTD_My_Bookings__c.setScale(0);
                    } else {
                        // Otherwise use the total bookings
                        mtdTotal = s.Total_Bookings__c.setScale(0);
                        ytdTotal = s.YTD_Total_Bookings__c.setScale(0);
                    }
                    mtdQuota = s.Quota__c.setScale(0);
                    mtdPercent = s.Quota_Percent__c.setScale(1);
                    ytdQuota = s.YTD_Quota__c.setScale(0);
                    ytdPercent = s.YTD_Quota_Percent__c.setScale(1);
                }
            }
        }
        
        // create a list of order wrappers for the "Group" or "Role" viewtypes
        public void groupRoleOrderWrappers() {
            
            // Set an sObjectField for either "Group" or "Role"
            sObjectField groupRoleField;
            if (viewType == 'Group') {
                groupRoleField = Sales_Performance_Summary__c.Group__c;
            } else if (viewType == 'Role') {
                groupRoleField = Sales_Performance_Summary__c.Role__c;
            }
            if (view == 'Group' || view == 'Role') {
                
                // Create a set of strings to hold each group/role
                set<string> groupRoles = new set<string>(); 
                for (Sales_Performance_Summary__c s : summaries) {
                    groupRoles.add((string)s.get(groupRoleField));
                }
                // Loop through our set of groups/roles and for each group/role, add the appropriate summaries to a list and create an order wrapper
                for (string gr : groupRoles) {
                    Sales_Performance_Summary__c[] groupRoleSummaries = new list<Sales_Performance_Summary__c>();
                    for (Sales_Performance_Summary__c s : summaries) {
                        if ((string)s.get(groupRoleField) == gr) {
                            groupRoleSummaries.add(s);
                        }
                    }
                    orderWrappers.add(new orderWrapper(viewType, view, groupRoleSummaries, month));
                }
                
            } else {
                for (Sales_Performance_Summary__c s : summaries) {
                    orderWrappers.add(new orderWrapper(viewType, view, new list<Sales_Performance_Summary__c>{s}, month));
                }
            }
            for (orderWrapper ow : orderWrappers) {
                for(Sales_Performance_Summary__c sps:ow.summaries){
                    //if not an ES associate or a VP then add the bookings to MTD total
                    if(!(sps.Role__c == 'Associate' && sps.Group__c == 'Enterprise Sales')){
                        mtdTotal = mtdTotal + sps.My_Bookings__c;
                    }
                    //if on the main view page show VP quota
                    if(view == 'Group' || view == 'Role'){
                        if(sps.Role__c == 'VP'){
                            mtdQuota = sps.Quota__c;
                        }
                    }
                    //if on the drilled down to rep show rep quota
                    else if (view.left(3) == '005'){
                        mtdQuota = sps.Quota__c;
                    }
                    //if viewing by group then add up all rep quotas except ES Associates
                    else if(viewType == 'Group'){
                        if (sps.Role__c != 'Director' && sps.Role__c != 'Sales Manager' && sps.Role__c != 'VP') {
                            if(!(sps.Role__c == 'Associate' && sps.Group__c == 'Enterprise Sales')){
                                mtdQuota = mtdQuota+sps.Quota__c;
                            }
                        }
                    }
                    // use add the current Role/rep quotas
                    else {
                        mtdQuota = mtdQuota+sps.Quota__c;
                    }
                }
                if (mtdTotal > 0 && mtdQuota > 0) {
                    mtdPercent = (mtdTotal / mtdQuota) * 100;
                } 
            }
            mtdTotal = mtdTotal.setScale(0);
            mtdQuota = mtdQuota.setScale(0);
            mtdPercent = mtdPercent.setScale(1);
        }
        
    }
    
    public class orderWrapper {
        public string linkName {get;set;}
        public string labelName {get;set;}
        public string roleName {get;set;}
        public string groupName {get;set;}
        public boolean isManager {get;set;}
        public decimal mtdTotal {get;set;}
        public decimal mtdTotalNoRound {get;set;}
        public decimal mtdIncentives {get;set;}
        public decimal mtdQuota {get;set;}
        public decimal mtdPercent {get;set;}
        public decimal ytdTotal {get;set;}
        public decimal ytdQuota {get;set;}
        public decimal ytdPercent {get;set;}
        public date startDate {get;set;}
        public string sicGroup {get;set;}
        public boolean nextPageRep {get;set;}
        public string month {get;set;}
        public string viewType {get;set;}
        public string view {get;set;}
        transient Sales_Performance_Summary__c[] summaries;
        
        public orderWrapper(string xviewType, string xview, Sales_Performance_Summary__c[] xsummaries, string xmonth) {
            // set the passed data
            viewType = xviewType;
            view = xview;
            summaries = xsummaries;
            month = xmonth;
            nextPageRep = false;
            isManager = false;
            
            mtdTotal = 0;
            mtdTotalNoRound = 0;
            mtdIncentives = 0;
            mtdQuota = 0;
            mtdPercent = 0.0;
            ytdTotal = 0;
            ytdQuota = 0;
            ytdPercent = 0.0;
            
            if (viewType == 'My Team') {
                myTeamInfo();
            } else {
                groupRoleInfo();
            }
        }
        
        public void myTeamInfo() {
            labelName = summaries[0].Owner.Name;
            linkName = summaries[0].OwnerId;
            roleName = summaries[0].Role__c;
            groupName = summaries[0].Group__c;
            
            if (summaries[0].SIC_Group__c != null) {
                sicGroup = string.valueOf(summaries[0].SIC_Group__c);
            } else {
                sicGroup = 'N/A';
            }
            
            startDate = summaries[0].Sales_Rep__r.Start_Date__c;
            
            // find out if the order wrapper is for a manager
            // if the wrapper's ownerId = the view user's id, display that as the manager's sales
            if (id.valueOf(linkName) == view) {
                if (roleName == 'VP' || roleName == 'Director' || roleName == 'Sales Manager') {
                    isManager = true;
                    nextPageRep = true;
                }
            }
            
            // if the role isnt Sales Manager, Director, or VP, the next page should direct to the rep's orders
            if ((roleName != 'Sales Manager' && roleName != 'Director' && roleName != 'VP') || isManager) {
                nextPageRep = true;
            }
            
            if (!isManager) {
                mtdTotal = summaries[0].Total_Bookings__c.setScale(0);
                mtdIncentives = summaries[0].MTD_Total_Incentives__c.setScale(0);
                ytdTotal = summaries[0].YTD_Total_Bookings__c.setScale(0);
            } else {
                mtdTotal = summaries[0].My_Bookings__c.setScale(0);
                ytdTotal = summaries[0].YTD_My_Bookings__c.setScale(0);
            }
            mtdQuota = summaries[0].Quota__c.setScale(0);
            ytdQuota = summaries[0].YTD_Quota__c.setScale(0);
            if (summaries[0].Quota_Percent__c != null) {
                mtdPercent = summaries[0].Quota_Percent__c.setScale(1);         
            }
            if (summaries[0].YTD_Quota_Percent__c != null) {
                ytdPercent = summaries[0].YTD_Quota_Percent__c.setScale(0);
            }
            
        }
        
        public void groupRoleInfo() {
            if (view == 'Group' || view == 'Role') {
                if (viewType == 'Group') {
                    labelName = summaries[0].Group__c;
                } else if (viewType == 'Role') {
                    labelName = summaries[0].Role__c;
                }
                linkName = labelName;
            } else {
                labelName = summaries[0].Owner.Name;
                linkName = summaries[0].OwnerId;
                if (summaries[0].SIC_Group__c != null) {
                    sicGroup = string.valueOf(summaries[0].SIC_Group__c);
                } else {
                    sicGroup = 'N/A';
                }
                startDate = summaries[0].Sales_Rep__r.Start_Date__c;
                nextPageRep = true;
            }
            for (Sales_Performance_Summary__c s : summaries) {
                if(!(view == 'Group' && s.Role__c == 'Associate' && s.Group__c == 'Enterprise Sales')){
                    mtdTotal = mtdTotal + s.My_Bookings__c;
                    mtdIncentives = mtdIncentives + s.MTD_Total_Incentives__c;
                    // if viewing the main group page then use all reps quotas
                    if(view == 'Group'){
                        if (s.Role__c != 'Director' && s.Role__c != 'Sales Manager' && s.Role__c != 'VP') {
                            mtdQuota = mtdQuota+s.Quota__c;
                        }
                    }
                    // if not viewing the main group page then use the current Role/rep quota
                    else{
                        mtdQuota = mtdQuota+s.Quota__c;
                    }
                    ytdTotal = ytdTotal + s.YTD_My_Bookings__c;
                    ytdQuota = ytdQuota + s.YTD_Quota__c;
                }
            }
            
            if (mtdTotal > 0 && mtdQuota > 0) {
                mtdPercent = (mtdTotal / mtdQuota) * 100;
            }
            if (ytdTotal > 0 && ytdQuota > 0) {
                ytdPercent = (ytdTotal / ytdQuota) * 100;
            }
            mtdTotalNoRound = mtdTotal;
            mtdTotal = mtdTotal.setScale(0);
            mtdIncentives = mtdIncentives.setScale(0);
            mtdQuota = mtdQuota.setScale(0);
            mtdPercent = mtdPercent.setScale(1);
            ytdTotal = ytdTotal.setScale(0);
            ytdQuota = ytdQuota.setScale(0);
            ytdPercent = ytdPercent.setScale(1);
        }
        
        
        
    }
    
    public void next() {
        string nextView = apexPages.currentPage().getParameters().get('nextView');
        string nextPageRep = apexPages.currentPage().getParameters().get('nextPageRep');
        string nextMonth = apexPages.currentPage().getParameters().get('nm');
        for (integer i=0; i<trails.size(); i++) {
            apexPages.currentPage().getParameters().put('t'+(i+1), trails[i].viewId);
        }
        apexPages.currentPage().getParameters().put('viewtype', viewType);
        apexPages.currentPage().getParameters().put('view', nextView);
        apexPages.currentPage().getParameters().put('rv', nextPageRep);
        apexPages.currentPage().getParameters().put('m', nextMonth);
        apexPages.currentPage().getParameters().put('y', selYear);
        createView();
    }
    
    public void prev() {
        string prevView = apexPages.currentPage().getParameters().get('prevView');
        string prevMonth = apexPages.currentPage().getParameters().get('m');
        boolean viewFound = false;
        for (integer i=0; i<trails.size(); i++) {
            if (trails[i].viewId != prevView) {
                if (!viewFound) {
                    apexPages.currentPage().getParameters().put('t'+(i+1), trails[i].viewId);
                } 
            } else {
                viewFound = true;
            }
        }
        trails.clear();
        apexPages.currentPage().getParameters().put('viewtype', viewType);
        apexPages.currentPage().getParameters().put('view', prevView);
        apexPages.currentPage().getParameters().put('m', prevMonth);
        apexPages.currentPage().getParameters().put('y', selYear);
        createView();
    }
    
    public class trail {
        public string viewName {get;set;}
        public string viewId {get;set;}
        public boolean hasNext {get;set;}
        public boolean isCurrentView {get;set;}
        
        public trail(string xview, string xviewId, boolean xisCurrentView) {
            viewName = xview;
            viewId = xviewId;
            hasNext = true;
            if (xview == 'My Performance') {
                hasNext = false;
            }
            isCurrentView = xisCurrentView;
        }
    }
    
    public void calculateYtdTotals() {
        // OVERALL MONTH TOTALS
        decimal ytdTotal = 0;
        decimal ytdQuota = 0;
        decimal ytdPercent = 0.0;
        for (monthWrapper mw : monthWrappers) {
            //ytd total
            ytdTotal = ytdTotal + mw.mtdTotal;
            
            //ytd quota
            ytdQuota = ytdQuota + mw.mtdQuota;
            
            //ytd percent
            if (ytdTotal > 0 && ytdQuota > 0) {
                ytdPercent = ((ytdTotal / ytdQuota) * 100).setScale(1);
            }
            
            if (viewType != 'My Team') {
                mw.ytdTotal = ytdTotal;
                mw.ytdQuota = ytdQuota;
                mw.ytdPercent = ytdPercent;
            }
        }
        ytdTotalBookings = ytdTotal.setScale(0);
        ytdTotalQuota = ytdQuota.setScale(0);
        ytdTotalPercent = ytdPercent;
    }
    
    public void queryOrderItems() {
        viewOrderId = null;
        string groupRoleRepId = ApexPages.currentPage().getParameters().get('groupRoleRepId');
        string groupRoleRepName = ApexPages.currentPage().getParameters().get('groupRoleRepName');
        string month = ApexPages.currentPage().getParameters().get('month');
        string managerOnly = ApexPages.currentPage().getParameters().get('managerOnly');
        System.debug('passing to spoSetCon: viewType = '+viewType+' | groupRoleRepId = '+groupRoleRepId+' | groupRoleRepName = '+groupRoleRepName+' | managerOnly = '+managerOnly+' | month = '+month+' | selYear = '+selYear);
        spoSetCon = new salesPerformanceOrderSetController(viewType, groupRoleRepId, groupRoleRepName, managerOnly, month, selYear);
        itDataCon = new salesPerformanceIncentiveData(viewType, groupRoleRepId, groupRoleRepName, managerOnly, month, selYear);
    }
    
    public void findMonthWrapper() {
        monthWrapper = null;
        if (ApexPages.currentPage().getParameters().get('selMonth') != null) {
            selMonth = ApexPages.currentPage().getParameters().get('selMonth');
        }
        if (selMonth != null) {
            if (monthWrappers.size() > 0) {
                initialOrdersQuery();
                for (monthWrapper mw : monthWrappers) {
                    if (mw.month == selMonth) {
                        monthWrapper = mw;
                    }
                }
            }
        }
        viewOrderId = null;
    }
    
    public void viewOrder() {
        id selOrder = ApexPages.currentPage().getParameters().get('selOrder');
        if(selOrder != null){
            viewOrderId = selOrder; 
        }
    }
    
    
}