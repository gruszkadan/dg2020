global class sfRequestTab {
    public User u {get;set;}
    public String SFRFilterId {get; set;}
    private Integer pageSize = 10;
    public string pagename {get;set;}
    // buttons
    public boolean viewRequestsTBA {get;set;}
    public boolean viewEnhancements {get;set;}
    public boolean viewProjects {get;set;}
    public boolean viewDepartmentManagement {get;set;}
    public boolean viewStatusWall {get;set;}
    public boolean viewAll {get;set;}
    
    public sfRequestTab() {
        u = [SELECT id, Name, FirstName, LastName, Email, Department__c, Role__c, ProfileId, Profile.Name 
             FROM User 
             WHERE id = :UserInfo.getUserId() LIMIT 1];
        viewButtons();
    }
    
    public void viewButtons() {
        viewRequestsTBA = Salesforce_Request_Settings__c.getInstance().View_Requests_TBA__c;
        viewEnhancements = Salesforce_Request_Settings__c.getInstance().View_Enhancements__c;
        viewProjects = Salesforce_Request_Settings__c.getInstance().View_Projects__c;
        viewDepartmentManagement = Salesforce_Request_Settings__c.getInstance().View_Department_Management__c;
        viewStatusWall = Salesforce_Request_Settings__c.getInstance().View_Status_Wall__c;
        if (viewRequestsTBA && viewEnhancements && viewProjects && viewDepartmentManagement && viewStatusWall) {
            viewAll = true;
        }
    }
    
     public PageReference gotoTBA() {
        PageReference pr = new PageReference('/apex/sfrTBA');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoEnhancements() {
        PageReference pr = new PageReference('/apex/sfrEnhancements');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoProjects() {
        PageReference pr = new PageReference('/apex/sfrProjects');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoDepartmentManagement() {
        string url = '/apex/sfrDepartmentManagement';
        string url_var = ApexPages.currentPage().getParameters().get('myParam');
        if (url_var != 'User') {
            url += '?dept='+url_var;
        }
        PageReference pr = new PageReference(url);
        return pr.setRedirect(true);
    }
    
    public PageReference gotoStatusWall() {
        PageReference pr = new PageReference('/apex/sfrStatusWall');
        return pr.setRedirect(true);
    }
    
    
}