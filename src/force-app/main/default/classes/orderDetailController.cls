//** Test Class: testOrderDetailController ** 
public class orderDetailController {
    public Sales_Performance_Order__c order {get;set;}
    public Sales_Performance_Item__c[] items {get;set;}
    public Incentive_Transaction__c[] it {get;set;}
    public String viewAs {get;set;}
    public String viewAsDefaultView {get;set;}
    public string userID {get;set;}
    public User u {get;set;}
    public String defaultView {get;set;}
    
    public orderDetailController(){
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(viewAs == null || viewAs == '' ||!Sales_Performance_Settings__c.getInstance().View_As_Enabled__c){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias, FirstName, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID 
             FROM User 
             WHERE id = :viewAs LIMIT 1];
        if (defaultView == 'VP' && viewAs == userInfo.getUserId()) {
            u = [SELECT id, Alias, FirstName, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID
                 FROM User
                 WHERE UserRole.Name = 'VP Sales' AND isActive = true and LastName != 'Haling' LIMIT 1];
        }
        userID = u.id;
        //get defaultView to display when viewing as
        defaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
        
        string spoID = ApexPages.currentPage().getParameters().get('id');
        
        order = [SELECT id, Account__c, Account__r.Name, Account__r.AID__c, Booking_Amount__c, Contract_Length__c, Incentive_Amount__c, Order_Date__c, 
                 Order_ID__c, OwnerID, Owner.Name, Type__c
                 FROM Sales_Performance_Order__c
                 WHERE id = :spoID LIMIT 1];
        items = [SELECT ID, Sales_Performance_Order__c, Product_Name__c, Booking_Amount__c, Total_Incentive_Amount__c, Status__c
                 FROM Sales_Performance_Item__c
                 WHERE Sales_Performance_Order__c =: spoID];
    }
    
    
    public void queryIncentives(){
        string spiID = ApexPages.currentPage().getParameters().get('spItemID');
        it = [SELECT ID, Incentive_Amount__c, Incentive_Date__c
              FROM Incentive_Transaction__c
              WHERE Sales_Performance_Item__c =: spiID
              ORDER BY Incentive_Date__c DESC];
    }
    
}