@isTest(SeeAllData=true)
private class testCustomerEndorsementSearch {
    
    // -------------------------------------------------------------------------------- TEST 1 -------------------------------------------------------------------------------- //    
    static testmethod void test1() {
        
        Territory__c t = new Territory__c();
        t.Name = 'T1';
        insert t;
        
        
        County_Zip_Code__c zipcounty = new County_Zip_Code__c();
        zipcounty.Territory__c = t.Id;
        zipcounty.County__c = 'Cook1';
        zipcounty.State__c = 'IL1';
        zipcounty.Zip_Code__c = '600621';
        insert zipcounty;
        
        Account a = new Account();
        a.Name = 'TestA';
        a.Territory__c = t.id;
        a.NAICS_Code__c = '11 - Agriculture, Forestry, Fishing & Hunting';
        a.BillingState = 'IL1';
        insert a;
        
        customer_endorsement__c cs = new customer_endorsement__c();
        cs.Account__c = a.Id; 
        cs.Story_Name__c = 'Test1';
        cs.Story_Type__c = 'Testimonial';
        cs.Testimonial_Type__c = 'Anonymous';
        insert cs;
        
        
        ApexPages.currentPage().getParameters().put('id', cs.Id);
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new customer_endorsement__c());
        customerEndorsementSearch myController = new  customerEndorsementSearch(testController);
        
        myController.Stateinput = 'IL1';
        
        myController.storyTypeInput = 'Testimonial';
        myController.naicsInput = '11 - Agriculture, Forestry, Fishing & Hunting';
        myController.sid = cs.Id;
        myController.story = cs.Id;
        
        
        myController.getStateOptions();
        myController.getNAICSoptions();
        myController.getStoryTypeOptions();
        mycontroller.getTestimonialTypeOptions();    
    }
    
    // -------------------------------------------------------------------------------- TEST 2 -------------------------------------------------------------------------------- //     
    static testmethod void test2() {
        
        Territory__c t = new Territory__c();
        t.Name = 'T1';
        insert t;
        
        
        County_Zip_Code__c zipcounty = new County_Zip_Code__c();
        zipcounty.Territory__c = t.Id;
        zipcounty.County__c = 'Cook1';
        zipcounty.State__c = 'IL1';
        zipcounty.Zip_Code__c = '600621';
        insert zipcounty;
        
        Account a = new Account();
        a.Name = 'TestA';
        a.Territory__c = t.id;
        a.NAICS_Code__c = '11 - Agriculture, Forestry, Fishing & Hunting';
        a.BillingState = 'IL1';
        insert a;
        
        
        
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new customer_endorsement__c());
        customerEndorsementSearch myController = new  customerEndorsementSearch(testController);
        
        customer_endorsement__c cs = new customer_endorsement__c();
        cs.Account__c = a.Id; 
        cs.Story_Name__c = 'Test1';
        cs.Story_Type__c = 'Case Study';
        cs.Status__c = 'Active';
        cs.case_study_link__c = 'http://www.google.com'; 
        cs.focus_authoring__c = true;
        cs.Focus_Compliance_Education__c = false;
        cs.Focus_HQ__c = false;
        cs.Focus_HQ_Reg_XR__c = false;
        cs.Focus_Incident_Management__c = false;
        cs.Focus_Plan1__c = false;
        cs.Focus_Workplace_Training__c = false;
        insert cs;
        
        
        myController.Stateinput = 'IL1';
        
        myController.storyTypeInput = 'Case Study';
        myController.naicsInput = '11 - Agriculture, Forestry, Fishing & Hunting';
        myController.statusInput = 'Active';
        myController.input_authoring = true;
        myController.sid = cs.Id;
        myController.story = cs.Id;
        myController.selectedstory = cs;
        mycontroller.CaseURL = 'http://www.google.com';
        myController.CaseURL2 = 'www.google.com';
        
        myController.getStateOptions();
        myController.getNAICSoptions();
        myController.getStoryTypeOptions();
        myController.getfocuslist();
        mycontroller.getFocusoptions();
        myController.getstatuslist();
        myController.getproducttypes();
        myController.getMidMarketEnterpriseSales();
        
    }
    
    
    // -------------------------------------------------------------------------------- TEST 3 -------------------------------------------------------------------------------- //     
    static testmethod void test3() {
        
        
        Territory__c t = new Territory__c();
        t.Name = 'T1';
        insert t;
        
        
        County_Zip_Code__c zipcounty = new County_Zip_Code__c();
        zipcounty.Territory__c = t.Id;
        zipcounty.County__c = 'Cook1';
        zipcounty.State__c = 'IL1';
        zipcounty.Zip_Code__c = '600621';
        insert zipcounty;
        
        Account a = new Account();
        a.Name = 'TestA';
        a.Territory__c = t.id;
        a.NAICS_Code__c = '11 - Agriculture, Forestry, Fishing & Hunting';
        a.BillingState = 'IL1';
        a.adminid__c = '101';
        a.billing_county__c = '600621';
        insert a;
        
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new customer_endorsement__c());
        customerEndorsementSearch myController = new  customerEndorsementSearch(testController);
        
        
        customer_endorsement__c cs = new customer_endorsement__c();
        cs.Account__c = a.Id; 
        cs.Story_Name__c = 'Test1';
        cs.Story_Type__c = 'Case Study';
        cs.Status__c = 'Active';
        cs.case_study_link__c = 'http://www.google.com'; 
        cs.focus_authoring__c = true;
        cs.Focus_Compliance_Education__c = false;
        cs.Focus_HQ__c = false;
        cs.Focus_HQ_Reg_XR__c = false;
        cs.Focus_Incident_Management__c = false;
        cs.Focus_Plan1__c = false;
        cs.Focus_Workplace_Training__c = false;
        insert cs;
        
        
        myController.Stateinput = 'IL1';
        mycontroller.zipinput = '600621';
        myController.storyTypeInput = 'Case Study';
        myController.naicsInput = '11 - Agriculture, Forestry, Fishing & Hunting';
        myController.statusInput = 'Active';
        mycontroller.input_hq = true;
        mycontroller.input_hqregxr = true;
        mycontroller.input_authoring = true;
        mycontroller.input_incidentmanagement = true;
        mycontroller.input_complianceeducation = true;
        mycontroller.input_workplacetraining = true; 
        mycontroller.input_plan1 = true;
        
        
        
        mycontroller.find();
        
    }
    
    
    
    
    // -------------------------------------------------------------------------------- TEST 4 -------------------------------------------------------------------------------- //     
    static testmethod void test4() {
        
        
        
        Territory__c t = new Territory__c();
        t.Name = 'T1';
        insert t;
        
        
        County_Zip_Code__c zipcounty = new County_Zip_Code__c();
        zipcounty.Territory__c = t.Id;
        zipcounty.County__c = 'Cook1';
        zipcounty.State__c = 'IL1';
        zipcounty.Zip_Code__c = '600621';
        insert zipcounty;
        
        Account a = new Account();
        a.Name = 'testAccount';
        a.Territory__c = t.id;
        a.NAICS_Code__c = '11 - Agriculture, Forestry, Fishing & Hunting';
        a.BillingState = 'IL1';
        a.adminid__c = '101';
        a.billing_county__c = '600621';
        a.customer_status__c = 'Active';
        insert a;
        
        customer_endorsement__c cs = new customer_endorsement__c();
        cs.Account__c = a.Id; 
        cs.Story_Name__c = 'Test1';
        cs.Story_Type__c = 'Case Study';
        cs.Status__c = 'Active';
        cs.case_study_link__c = 'http://www.google.com'; 
        cs.focus_authoring__c = false;
        cs.Focus_Compliance_Education__c = false;
        cs.Focus_HQ__c = true;
        cs.Focus_HQ_Reg_XR__c = false;
        cs.Focus_Incident_Management__c = false;
        cs.Focus_Plan1__c = false;
        cs.Focus_Workplace_Training__c = false;
        insert cs;
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new customer_endorsement__c());
        customerEndorsementSearch myController = new  customerEndorsementSearch(testController);
        
        myController.Stateinput = 'IL1';
        mycontroller.zipinput = '600621';
        myController.storyTypeInput = 'Case Study';
        myController.naicsInput = '11 - Agriculture, Forestry, Fishing & Hunting';
        myController.statusInput = 'Active';
        mycontroller.input_hq = true;
        mycontroller.input_hqregxr = false;
        mycontroller.input_authoring = false;
        mycontroller.input_incidentmanagement = false;
        mycontroller.input_complianceeducation =false;
        mycontroller.input_workplacetraining = false;
        mycontroller.input_plan1 = false;
        
        
        myController.acctNameSearch = 'testAccount,TestAs';
        mycontroller.find();
        myController.FirstPage();
        myController.previous();
        myController.next();
        myController.getnxt();
        myController.getprev();
    }        
    
    
    
    // -------------------------------------------------------------------------------- TEST 5 -------------------------------------------------------------------------------- //     
    static testmethod void test5() {
        
        
        
        Territory__c t = new Territory__c();
        t.Name = 'T1';
        insert t;
        
        
        County_Zip_Code__c zipcounty = new County_Zip_Code__c();
        zipcounty.Territory__c = t.Id;
        zipcounty.County__c = 'Cook1';
        zipcounty.State__c = 'IL1';
        zipcounty.Zip_Code__c = '600621';
        insert zipcounty;
        
        Account a = new Account();
        a.Name = 'TestA';
        a.Territory__c = t.id;
        a.NAICS_Code__c = '11 - Agriculture, Forestry, Fishing & Hunting';
        a.BillingState = 'IL1';
        insert a;
        
        Contact c = new Contact();
        c.firstName = 'Ryan';
        c.lastName = 'W';
        c.email = 'rwerner88@gmail.com';
        c.AccountId = a.id;
        c.communication_channel__c = 'Inbound Call';
        insert c;
        
        
        
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new customer_endorsement__c());
        customerEndorsementSearch myController = new  customerEndorsementSearch(testController);
        
        
        customer_endorsement__c cs = new customer_endorsement__c();
        cs.Account__c = a.Id; 
        cs.Contact__c = c.id;
        cs.Story_Name__c = 'Test1';
        cs.Story_Type__c = 'Reference';
        cs.Status__c = 'Active';
        
        insert cs;
        
        mycontroller.story = cs.Id;
        mycontroller.selectedstory = cs;        
        
        
        mycontroller.emailinput = 'rwerner88@gmail.com';
        mycontroller.getemailinput();
        
        
        
        mycontroller.updatereferenceused2();
        mycontroller.selectedstory.Reference_Date_Used__c = Date.TODAY(); 
        customer_Story_Settings__c settings = new Customer_Story_Settings__c();
        settings.Name = 'TEST-'+DateTime.now();
        settings.Date_Used__c = Date.Today();
        settings.Reference_Name__c = 'Test1';
        settings.Reference_Id__c = mycontroller.selectedstory.Id;
        insert settings;
        mycontroller.selectedstory.Reference_Times_Used_6_Months__c = 1 ;
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        string body = 'hello';
        
    }
    
    // -------------------------------------------------------------------------------- TEST 6 -------------------------------------------------------------------------------- //     
    static testmethod void test6() {
        
        
        
        Territory__c t = new Territory__c();
        t.Name = 'T1';
        insert t;
        
        
        County_Zip_Code__c zipcounty = new County_Zip_Code__c();
        zipcounty.Territory__c = t.Id;
        zipcounty.County__c = 'Cook1';
        zipcounty.State__c = 'IL1';
        zipcounty.Zip_Code__c = '600621';
        insert zipcounty;
        
        Account a = new Account();
        a.Name = 'TestA';
        a.Territory__c = t.id;
        a.NAICS_Code__c = '11 - Agriculture, Forestry, Fishing & Hunting';
        a.BillingState = 'IL1';
        insert a;
        
        Contact c = new Contact();
        c.firstName = 'Ryan';
        c.lastName = 'W';
        c.email = 'rwerner88@gmail.com';
        c.AccountId = a.id;
        c.communication_channel__c = 'Inbound Call';
        insert c;
        
        
        customer_endorsement__c cs = new customer_endorsement__c();
        cs.Account__c = a.Id; 
        cs.Contact__c = c.id;
        cs.Story_Name__c = 'Test1';
        cs.Story_Type__c = 'Reference';
        cs.Status__c = 'Active';
        
        insert cs;
        
        
        ApexPages.currentPage().getParameters().put('id', cs.Id);
        ApexPages.currentPage().getParameters().put('sid', cs.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new customer_endorsement__c());
        customerEndorsementSearch myController = new  customerEndorsementSearch(testController);
        
        boolean selectall = true;
        myController.alls();
        myController.gotoPopup();
        
        
    }
    
    static testmethod void test7() {
        
        
        
        Territory__c t = new Territory__c();
        t.Name = 'T1';
        insert t;
        
        
        County_Zip_Code__c zipcounty = new County_Zip_Code__c();
        zipcounty.Territory__c = t.Id;
        zipcounty.County__c = 'Cook1';
        zipcounty.State__c = 'IL1';
        zipcounty.Zip_Code__c = '600621';
        insert zipcounty;
        
        Account a = new Account();
        a.Name = 'TestA';
        a.Territory__c = t.id;
        a.NAICS_Code__c = '11 - Agriculture, Forestry, Fishing & Hunting';
        a.BillingState = 'IL1';
        insert a;
        
        Contact c = new Contact();
        c.firstName = 'Ryan';
        c.lastName = 'W';
        c.email = 'rwerner88@gmail.com';
        c.AccountId = a.id;
        c.communication_channel__c = 'Inbound Call';
        insert c;
        
        
        customer_endorsement__c cs = new customer_endorsement__c();
        cs.Account__c = a.Id; 
        cs.Contact__c = c.id;
        cs.Story_Name__c = 'Test1';
        cs.Story_Type__c = 'Testimonial';
        cs.Status__c = 'Active';
        
        insert cs;
        
        
        
        ApexPages.currentPage().getParameters().put('id', cs.Id);
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new customer_endorsement__c());
        customerEndorsementSearch myController = new  customerEndorsementSearch(testController);
        
        myController.selectall = false;
        myController.alls();
        
        
        
    }
}