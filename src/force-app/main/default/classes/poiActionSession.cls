public with sharing class poiActionSession {
    
    public static void insertSession(Product_of_Interest_Action__c[] actions){
        
        Set<Product_of_Interest_Action__c> actionsToUpdate = new set <Product_of_Interest_Action__c>();
        Product_Of_Interest_Action__c[] actionsToUpdateList = new list<Product_Of_Interest_Action__c>();
        Set <Id> pois = new Set<Id>();
        Set <Id> personIds = new Set<Id>();
        
        // Reset contact status to open
        Product_Of_Interest_Action__c[] resetStatuses = new list<Product_Of_Interest_Action__c>();
        
        for(Product_of_Interest_Action__c poia : actions){
            pois.add(poia.Product_of_Interest__c);
            if(poia.Contact__c != null){
                personIds.add(poia.Contact__c);
            }else{
                personIds.add(poia.Lead__c);
            }
        }
        
        //query any related leads/contacts for the last activity dates
        Lead[] leadList = new list<Lead>();
        Contact[] contactList = new list<Contact>();
        if(personIds.size() > 0){
            leadList = [SELECT id, Last_Activity_Date_Authoring__c, Last_Activity_Date_EHS__c, Last_Activity_Date_Ergonomics__c, Last_Activity_Date_Chemical_Management__c, Last_Activity_Date_Online_Training__c
                        FROM Lead
                        WHERE id in :personIds];  
            
            contactList = [SELECT id, Account.Last_Activity_Date_Authoring__c, Account.Last_Activity_Date_EHS__c, Account.Last_Activity_Date_Ergo_Owner__c, Account.Last_Activity_Date_Chemical_Management__c, Account.Last_Activity_Date_Online_Training_Owner__c, Last_Activity_Date_Authoring__c, Last_Activity_Date_EHS__c, Last_Activity_Date_Ergonomics__c, Last_Activity_Date_Chemical_Management__c, Last_Activity_Date_Online_Training__c
                           FROM Contact
                           WHERE id in :personIds];
        }
        
        //query any/all poi actions in open sessions related to the passed list of new actions
        Product_of_Interest_Action__c[] openSessionActions = [SELECT id, Product_Suite__c, Session_Unique_ID__c, Product_Action_Session_Number__c, Product_of_Interest__c, Session_Open_Date__c, Session_Last_Action_Date__c, Session_Confirmed_Date__c, Session_Close_Date__C, Last_Activity_Date_Person__c, Last_Activity_Date_Account__c
                                                              FROM Product_of_Interest_Action__c
                                                              WHERE Product_of_Interest__c in: pois and Session_Close_Date__c =null
                                                              ORDER BY CreatedDate ASC];
        
        //query any/all poi actions in closed sessions related to the passed list of new actions
        Product_of_Interest_Action__c[] closedProductActionSessions = [SELECT id, Product_Suite__c, Session_Unique_ID__c, Product_Action_Session_Number__c, Product_of_Interest__c, Session_Open_Date__c, Session_Last_Action_Date__c, Session_Confirmed_Date__c, Session_Close_Date__C
                                                                       FROM Product_of_Interest_Action__c
                                                                       WHERE Product_of_Interest__c in: pois and Session_Close_Date__c !=null
                                                                       ORDER BY CreatedDate ASC];
        
        
        //LOOP THROUGH EACH NEW ACTION
        for(Product_of_Interest_Action__c a:actions){
            Map<String,Product_of_Interest_Action__c> actionMap = new Map<String,Product_of_Interest_Action__c>();
            //loop through and add them to map with the product suite being used as the key
            for(Product_of_Interest_Action__c cpas : closedProductActionSessions){
                if(cpas.Product_Of_Interest__c == a.Product_Of_Interest__c){
                    actionMap.put(string.valueOf(cpas.Product_Suite__c), cpas);
                }
            }
            //create variable for the session number
            decimal actionSessionNumber = 1;
            //create a list to hold any actions from the open session related to the new action
            Product_of_Interest_Action__c[] actionsInSession = new List<Product_of_Interest_Action__c>();
            //loop through the actions from open sessions
            for(Product_of_Interest_Action__c sa:openSessionActions){
                //find any actions that match the POI of the new action
                if(sa.Product_Of_Interest__c == a.Product_Of_Interest__c && sa.Product_Suite__c == a.Product_Suite__c){
                    //add the matched actions to the list
                    actionsInSession.add(sa);
                }
            }
            
            // IF HAS AN OPEN SESSION
            if(actionsInSession.size() > 0){
                //loop through each related action
                for(Product_of_Interest_Action__c ais : actionsInSession){
                    //set last action date  of related actions
                    ais.Session_Last_Action_Date__c = date.today();
                    //set the session number of new action
                    actionSessionNumber = ais.Product_Action_Session_Number__c;
                    //set the last activity dates of the new action
                    a.Last_Activity_Date_Account__c = ais.Last_Activity_Date_Account__c;
                    a.Last_Activity_Date_Person__c = ais.Last_Activity_Date_Person__c;
                    //set the session confirmed date if it exists
                    a.Session_Confirmed_Date__c = ais.Session_Confirmed_Date__c;
                    a.Session_Unique_ID__c = ais.Session_Unique_ID__c;
                }
                // add looped actions to update list
                actionsToUpdate.addAll(actionsInSession);
                //set session open date for the new action to the MIN open date from related actions 
                a.Session_Open_Date__c = actionsInSession[0].Session_Open_Date__c;
                
                
                // IF NO OPEN SESSION    
            }else{
                // set session open date to today
                a.Session_Open_Date__c = date.today();
                // set action as the first action in session
                a.First_Action_in_Session__c = true;
                
                // If it's the first action in the session, we need to set the POI, person status to 'Open' for that suite
                resetStatuses.add(a);
                
                
                // set product of interest action session number
                Product_of_Interest_Action__c[] relatedClosedActions = new list<Product_of_Interest_Action__c>();
                for(String am : actionMap.keyset()){
                    if(am == string.valueOf(a.Product_Suite__c)){
                        relatedClosedActions.add(actionmap.get(am));
                    }
                }
                if(relatedClosedActions.size() > 0){
                    for(Product_of_Interest_Action__c rca : relatedClosedActions){
                        if(a.Product_Of_Interest__c == rca.Product_Of_Interest__c){
                            if(rca.Product_Action_Session_Number__c >= actionSessionNumber){
                                actionSessionNumber = rca.Product_Action_Session_Number__c + 1;
                            }
                        }
                    }
                }
                
                //set session unique id
                string suite;
                if(a.Product_Suite__c == 'MSDS Management'){
                    suite = 'MSDS';
                }else if(a.Product_Suite__c == 'MSDS Authoring'){
                    suite = 'AUTH';
                }else if(a.Product_Suite__c == 'On-Demand Training'){
                    suite = 'ODT';
                }else if(a.Product_Suite__c == 'EHS Management'){
                    suite = 'EHS';
                }else if(a.Product_Suite__c == 'Ergonomics'){
                    suite = 'ERGO';
                }
                if(a.Contact__c != null){
                    a.Session_Unique_ID__c = suite+'-'+actionSessionNumber+'-'+a.Contact__c;
                }else if(a.Lead__c != null){
                    a.Session_Unique_ID__c = suite+'-'+actionSessionNumber+'-'+a.Lead__c;
                }
                
                //set the last activity dates by suite
                if(a.Contact__c != null){
                    for(Contact c:ContactList){
                        if(c.id == a.Contact__c){
                            if(a.Product_Suite__c == 'MSDS Authoring'){
                                a.Last_Activity_Date_Account__c = c.Account.Last_Activity_Date_Authoring__c;
                                a.Last_Activity_Date_Person__c = c.Last_Activity_Date_Authoring__c;
                            }else if(a.Product_Suite__c == 'EHS Management'){
                                a.Last_Activity_Date_Account__c = c.Account.Last_Activity_Date_EHS__c;
                                a.Last_Activity_Date_Person__c = c.Last_Activity_Date_EHS__c;
                            }else if(a.Product_Suite__c == 'MSDS Management'){
                                a.Last_Activity_Date_Account__c = c.Account.Last_Activity_Date_Chemical_Management__c;
                                a.Last_Activity_Date_Person__c = c.Last_Activity_Date_Chemical_Management__c;
                            }else if(a.Product_Suite__c == 'Ergonomics'){
                                a.Last_Activity_Date_Account__c = c.Account.Last_Activity_Date_Ergo_Owner__c;
                                a.Last_Activity_Date_Person__c = c.Last_Activity_Date_Ergonomics__c;
                            }else if(a.Product_Suite__c == 'On-Demand Training'){
                                a.Last_Activity_Date_Account__c = c.Account.Last_Activity_Date_Online_Training_Owner__c;
                                a.Last_Activity_Date_Person__c = c.Last_Activity_Date_Online_Training__c;
                            }
                        }
                    }
                }else if(a.Lead__c != null){
                    for(Lead l:leadList){
                        if(l.id == a.Lead__c){
                            if(a.Product_Suite__c == 'MSDS Authoring'){
                                a.Last_Activity_Date_Person__c = l.Last_Activity_Date_Authoring__c;
                            }else if(a.Product_Suite__c == 'EHS Management'){
                                a.Last_Activity_Date_Person__c = l.Last_Activity_Date_EHS__c;
                            }else if(a.Product_Suite__c == 'MSDS Management'){
                                a.Last_Activity_Date_Person__c = l.Last_Activity_Date_Chemical_Management__c;
                            }else if(a.Product_Suite__c == 'Ergonomics'){
                                a.Last_Activity_Date_Person__c = l.Last_Activity_Date_Ergonomics__c;
                            }else if(a.Product_Suite__c == 'On-Demand Training'){
                                a.Last_Activity_Date_Person__c = l.Last_Activity_Date_Online_Training__c;
                            }
                        }
                    }
                }
            }
            // EITHER WAY
            // set session last action date for new action to today
            a.Session_Last_Action_Date__c = date.today();
            a.Product_Action_Session_Number__c = actionSessionNumber;
            
        }
        // If there are actions that need to reset the person's status to 'Open'
        if (resetStatuses.size() > 0) {
            poiStageStatusUtility.resetStatus(resetStatuses);
        }
        //update the related actions and error check
        for(Product_of_Interest_Action__c au:actionsToUpdate){
            actionsToUpdateList.add(au);
        }
        
        try{
            update actionsToUpdateList;
        } catch (exception e) {
            salesforceLog.createLog('POI', true, 'poiActionSession', 'insertSession', string.valueOf(e));
        }
    }
    
    public static void sessionConfirmedDate(SObject[] triggerRecords, String sObjectType){
        //CALLED FROM OPPORTUNITY TRIGGER
        if(sObjectType == 'Opportunity'){
            
            //create a set of the opportunity ids
            set<id> oppIds = new set<id>();
            for(sObject tr:triggerRecords){
                oppIds.add(tr.id);
            }
            
            //create an sobjectfield for the opportunity owner group
            sObjectField ownerGroup = Opportunity.Owner_Group__c;	
            
            //query the opportunity contact roles and create a set of contact ids
            set<id> contactIds = new set<id>();
            OpportunityContactRole[] contactRoles = [SELECT id, ContactId, OpportunityId
                                                     FROM OpportunityContactRole
                                                     WHERE OpportunityId in :oppIds and IsPrimary = TRUE];
            for(OpportunityContactRole ocr:contactRoles){
                contactIds.add(ocr.ContactId);
            }
            
            //query any POI actions related to the contacts and set the session confirmed date 
            if(contactIds.size() > 0){
                Product_of_Interest_Action__c[] poiActions = [SELECT id, Session_Confirmed_Date__c, Product_Suite__c
                                                              FROM Product_of_Interest_Action__c
                                                              WHERE Contact__c in :contactIds and Session_Open__c = true];
                for(Product_of_Interest_Action__c poia:poiActions){
                    for(SObject op:triggerRecords){
                        if(poia.Session_Confirmed_Date__c == null){
                            if((poia.Product_Suite__c == 'MSDS Authoring' && String.valueOf(op.get(ownerGroup)) == 'Authoring Sales')||
                               (poia.Product_Suite__c == 'EHS Management' && String.valueOf(op.get(ownerGroup)).contains('EHS'))||
                               (poia.Product_Suite__c == 'Ergonomics' && String.valueOf(op.get(ownerGroup)) == 'Ergo')||
                               (poia.Product_Suite__c == 'MSDS Management' && (String.valueOf(op.get(ownerGroup)) == 'Mid-Market'||String.valueOf(op.get(ownerGroup)) =='Enterprise Sales'))||
                               (poia.Product_Suite__c == 'On-Demand Training' && String.valueOf(op.get(ownerGroup)) == 'Online Training')){
                                   poia.Session_Confirmed_Date__c = date.today();
                               }
                        }
                    }
                }
                update poiActions;
            }
        }
        
        //CALLED FROM POI ACTION TRIGGER
        if(sObjectType == 'poiAction'){
            
            //create a set of the opportunity ids
            set<id> poiaIds = new set<id>();
            for(sObject tr:triggerRecords){
                poiaIds.add(tr.id);
            }
            
            //query the POI Actions and set the session confirmed date
            if(poiaIds.size() > 0){
                Product_of_Interest_Action__c[] poiActions = [SELECT id, Session_Confirmed_Date__c
                                                              FROM Product_of_Interest_Action__c
                                                              WHERE id in :poiaIds];
                for(Product_of_Interest_Action__c poia:poiActions){
                    poia.Session_Confirmed_Date__c = date.today();
                }
                update poiActions;
            }
        } 
    }
    
    public static void sessionCloseDate(Product_of_Interest_Action__c[] actions){
        // Set a string of inactive statuses. Since the session is being closed before the contact can query them, we will
        //skip that and mark the stage as 'Sales Inactive'
        //string inactiveStatuses = 'Resolved - Closed Opp;Resolved - Closed/Lost Opp;Resolved - Not a Target;Resolved - Connected Not Progressing;Resolved - Good Title, But Not the DM;'+
        //'Resolved - Poor Title / No Influence;Resolved - DM Not in My Territory;Resolved - Too Small to Call;Resolved - Poor Website;Resolved - Recently Resolved Product;Resolved - Active Customer / No Potential Opp';
        for(Product_of_Interest_Action__c poia:actions){
            poia.Session_Close_Date__c = date.today();
            poia.Session_Open__c = false;
            poia.Closed_By__c = UserInfo.getUserId();
            //if (inactiveStatuses.contains(poia.Action_Status__c)) {
            //poia.Stage__c = 'Sales Inactive';
            //}
        }
    }
    
    
}