@isTest
private class testUserTrigger {
    
    static testmethod void test_deactivationCheck() {
        User u = [SELECT id, isActive, LastName, Title FROM User WHERE Department__c = 'Customer Care' AND Group__c = 'Retention' AND isActive = true LIMIT 1];
        string title = u.Title;
        //try to deactivate 
        u.isActive = false;
        update u;
       
    } 
    
    public static testmethod void test_enterpriseRRManagement() {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'SF Operations']; 
        User[] users = new list<User>();
        for (integer i=0; i<5; i++) {
            User u = new User(Alias = 'du'+i, Email='standarduser@testorg.com', FirstName = 'Test',
                              EmailEncodingKey='UTF-8', LastName='Testing'+i, LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id, EntLeads_RR__c = false,
                              TimeZoneSidKey='America/Los_Angeles', UserName='test'+i+'@test.com.'+i+'test');
            if(i < 3){
                u.Suite_Of_Interest__c = 'Ergonomics';
            }else{
                 u.Suite_Of_Interest__c = 'MSDS Management';
            }
            users.add(u);
        }
        insert users;
        set<id> userIds = new set<id>();
        for (User u : users) {
            u.EntLeads_RR__c = true;
            userIds.add(u.id);
        }
        update users;
        
        Round_Robin_Assignment__c[] rrs = [SELECT id, Name FROM Round_Robin_Assignment__c WHERE UserID__c IN : userIds];
        System.assert(rrs.size() == 2);
        
        User newUser = new User(Alias = 'newusr', Email='standarduser@testorg.com', FirstName = 'Test',
                                EmailEncodingKey='UTF-8', LastName='NewUserTest', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, EntLeads_RR__c = true, Suite_Of_Interest__c = 'MSDS Management',
                                TimeZoneSidKey='America/Los_Angeles', UserName='newusertest@newusertest.com.test');
        insert newUser;
        
		Round_Robin_Assignment__c[] rr = [SELECT id, Name FROM Round_Robin_Assignment__c WHERE UserID__c = : newUser.id];        
        System.assert(rr.size() == 1);
        
        
    }
}