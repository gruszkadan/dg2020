@isTest(seeAllData=true)
public class testCompetitorContractEndDate {
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void test1() {
        User owner = [SELECT id from User where LastName = 'Gruszka' Limit 1];
        Account testAccount = new Account(Name = 'testAccount', OwnerId = owner.id, Authoring_Account_Owner__c = owner.id, Ergonomics_Account_Owner__c = owner.id, EHS_Owner__c = owner.id, Online_Training_Account_Owner__c = owner.Id,
                                         Current_System_Contract_End_Date__c = date.today().addMonths(6),Current_System_Contract_End_Date_EHS__c = date.today().addMonths(6),Current_System_Contract_End_Date_Ergo__c = date.today().addMonths(6),
                                         Current_System_Contract_End_Date_Auth__c = date.today().addMonths(6),Current_System_Contract_End_Date_ODT__c = date.today().addMonths(6));
        insert testAccount;
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new competitorContractEndDateNotification());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
    }
}