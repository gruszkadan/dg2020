<apex:page id="proposalHQProductDetail" showHeader="FALSE" sidebar="FALSE" renderAS="PDF" standardController="Quote__c" standardStylesheets="false">
    <apex:stylesheet value="{!URLFOR($Resource.proposalPrintResource, 'default.css')}"/>
    <body style="page-break-after:always;" class="customerCare">
        <div class="header">
            <h1>HQ RegXR Account</h1>
            <p>
                Our HQ RegXR Account is an easy and affordable cloud-based solution that simplifies hazardous chemical management and streamlines regulatory reporting. The path to sustainability starts here.  
            </p>
        </div>
        <div class="sidebar">
            <div class="content">
                <div class="logo">
                    <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/logo-msdsonline.png')}" styleClass="msdsonlineLogo" width="234" height="59" />        
                </div>
                <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/HQRegXR-sidebar.jpg')}" styleClass="customerCarePhoto" width="319" height="179" />        
                <h2>Benefits</h2>
                <p>
                    <span class="careFeature">Simplify Compliance</span><br/>
                    Easy-to-use tools help you meet hazard communication requirements (OSHA / WHMIS / GHS), including hazard determination, right-to-know access, labeling, safe chemical handling, PPE, and more. 
                </p>
                <p>
                    <span class="careFeature">Improve Chemical Management</span><br/>
                    Robust chemical management tools provide greater container-level control over the location, status and risk associated with the chemicals you use. Our free mobile Chemical Inventory Scanner app lets you scan barcode/QR code labels for in-the-field container management. 
                </p>
                <p>
                    <span class="careFeature">Streamline Regulatory Reporting</span><br/>
                    Our powerful regulatory cross-referencing engine identifies products, as well as ingredients that are regulated on various state, federal and international hazardous substance lists. 
                </p>
                <p>
                    <span class="careFeature">Mobile-Enabled Functionality</span><br/>
                    Intelligent UI design delivers remote access to your chemical inventory information whenever and wherever you need it, via your mobile device.   
                </p>
                <p>
                    <span class="careFeature">Facilitate Sustainability</span><br/>
                    Better understand the composition of chemical products, simplify environmental and regulatory compliance requirements and move toward using more sustainable and renewable raw materials.   
                </p>
                <p>
                    <span class="careFeature">Quick ROI &amp; Low Cost of Ownership </span><br/>
                    The HQ RegXR Account pays for itself by eliminating time-consuming, manual administrative tasks.   
                </p>
            </div>
        </div>    
        <div class="pageContent">
            <span class="careFeature">Streamline Environmental Management &amp; HazCom Compliance</span><br/> 
            MSDSonline understands that properly maintaining regulated materials in your inventory starts with knowing exactly what chemicals you have on-site, where they are used and stored, the precise quantity of each, and having immediate access to their corresponding up-to-date safety data sheets. 
            <p>
                Our HQ RegXR Account is ideal for single- and multi-facility businesses that need a more efficient, cost-effective way to manage chemical and environmental hazards in the workplace. It’s an easy-to-use system that enables you to account for, report on and limit chemical hazards in your organization. 
            </p>
            <span class="careFeature">
                Simple Safety Data Sheet Compliance
            </span>
            <br/>
            To prepare for the impending document churn resulting from OSHA and Health Canada’s adoption of GHS, it is imperative that employers update their MSDS libraries and we can help.
            <p>
                Our HQ RegXR Account combines unlimited access to the industry-leading database of manufacturer-original safety data sheets with easy-to-use deployment and chemical inventory management tools to help you more cost-effectively manage your company’s on-site chemicals and comply with global hazard communication requirements.
            </p>
            <p>
                Our cloud-based HQ RegXR Account streamlines safety data sheet obtainment, management and chemical reporting, and works seamlessly across multiple PC, laptop and mobile device platforms, giving your employees right-to-know access to vital safety information, such as your company-specific SDSs and secondary container labels, now available in a variety of formats, including GHS, and compatible with Brady Label Printers BBP31 &amp; 33.
            </p>
            <p>
                <span class="careFeature">Powerful Chemical Inventory Management</span><br/> 
                With our HQ RegXR Account, you get on-demand container-level chemical management, tracking, mapping and compliance reporting tools to help you maintain a complete and accurate picture of the chemical hazards in your work environment. As an added benefit, you can easily share this potentially lifesaving information with emergency responders, using our free Plan1 First Responder Share Service. At the same time, you can utilize advanced chemical approval and notification workflow tools to gain greater control over what chemicals to accept or ban from your work environment.
            </p> 
            <p>
                <span class="careFeature">Robust Regulatory Reporting</span><br/> 
                Our sophisticated regulatory cross-referencing (RegXR) engine automatically identifies and flags products and ingredients that are regulated on various state, federal and international hazardous substance lists, making it easier for you to comply with regulatory reporting requirements. It’s a powerful, all-encompassing solution for helping you better understand the material composition of your chemical products, simplify compliance reporting, and facilitate the use of more sustainable raw materials.
            </p> 
            <p>
                <strong>
                    <em>“It is a great tracking tool of what chemicals we are using and in which locations. Great way to find SDS sheets.”</em> — David Buck</strong>
            </p>
        </div>
        <div class="footer">
            Proprietary &amp; Confidential. &copy; Copyright MSDSonline Inc., DBA <em>VelocityEHS</em>. Do not distribute without authorized consent.
            <div class="pageNumber">
                Page <span class="pagenumber"/> of <span class="pagecount"/>
            </div>
        </div>
    </body>
</apex:page>