public class sfrDepartmentManagementCon {
    public User u {get;set;}
    public string dept {get;set;}
    public status[] statuses {get;set;}
    public statusRequest[] statusReqs {get;set;}
    public string selDept {get;set;}
    public integer selSortNum {get;set;}
    // requests lists
    public request[] enhancements {get;set;}
    public request[] newEnhancements {get;set;}
    public request[] projects {get;set;}
    public request[] newProjects {get;set;}
    public Salesforce_Request__c[] allReqs = new List<Salesforce_Request__c>();
    public string colorGuide {get;set;}
    public string[] statusVals {get;set;}
    // buttons
    public boolean viewRequestsTBA {get;set;}
    public boolean viewEnhancements {get;set;}
    public boolean viewProjects {get;set;}
    public boolean viewDepartmentManagement {get;set;}
    public boolean viewStatusWall {get;set;}
    public boolean viewAll {get;set;}
    public id enhancementsQueue {get;set;}
    public id projectsQueue {get;set;}
    
    public sfrDepartmentManagementCon() {
        enhancementsQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.DeveloperName = 'SF_Requests_Enhancements' LIMIT 1].Queueid;
        projectsQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.DeveloperName = 'SF_Requests_Projects' LIMIT 1].Queueid;
        
        selDept = 'All';
        colorGuide = 'Sales#F88962,Marketing#4BC076,CC - Chicago#7F8DE1,CC - Oakville#e3d067,Product Management#EF7EAD,VelocityEHS#0C8EFF';
        statusVals = 'Requested;Pending Department Approval;Scoping;Requirements Review;Scheduling;Development Work Queue;In Development;Testing;Pending Push to Production'.split(';', 0);
        // query the current user and find the department
        u = [SELECT id, Name, FirstName, LastName, Email, Department__c, Role__c, ProfileId, Profile.Name, Division
             FROM User 
             WHERE id = :UserInfo.getUserId() LIMIT 1];
        viewButtons();
        if (ApexPages.currentPage().getParameters().get('dept') == null) {
            dept = u.Department__c;
             if(dept == 'Customer Care' && u.Division == 'Oakville'){
                dept = 'Customer Care - Oakville';
            }
        } else {
            dept = ApexPages.currentPage().getParameters().get('dept');
        }
        find();
    }
    
    public void viewButtons() {
        viewRequestsTBA = Salesforce_Request_Settings__c.getInstance().View_Requests_TBA__c;
        viewEnhancements = Salesforce_Request_Settings__c.getInstance().View_Enhancements__c;
        viewProjects = Salesforce_Request_Settings__c.getInstance().View_Projects__c;
        viewDepartmentManagement = Salesforce_Request_Settings__c.getInstance().View_Department_Management__c;
        viewStatusWall = Salesforce_Request_Settings__c.getInstance().View_Status_Wall__c;
        if (viewRequestsTBA && viewEnhancements && viewProjects && viewDepartmentManagement && viewStatusWall) {
            viewAll = true;
        }
    }
    
    public void selectDepartment() {
        selDept = ApexPages.currentPage().getParameters().get('selDeptParam');
        find();
    }
    
    public PageReference gotoHome() {
        PageReference pr = new PageReference('/apex/sf_request_tab');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoDepartmentManagement() {
        string url = '/apex/sfrDepartmentManagement';
        string url_var = ApexPages.currentPage().getParameters().get('myParam');
        if (url_var != 'User') {
            url += '?dept='+url_var;
        }
        PageReference pr = new PageReference(url);
        return pr.setRedirect(true);
    }
    
    public PageReference gotoEnhancements() {
        PageReference pr = new PageReference('/apex/sfrEnhancements');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoRequestsTBA() {
        PageReference pr = new PageReference('/apex/sfrTBA');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoProjects() {
        PageReference pr = new PageReference('/apex/sfrProjects');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoStatusWall() {
        PageReference pr = new PageReference('/apex/sfrStatusWall');
        return pr.setRedirect(true);
    }
    
    public void makeStatusWrappers() {
        statuses = new List<status>();
        statusReqs = new List<statusRequest>();
        string[] vals = 'Requested;Pending Department Approval;Scoping;Requirements Review;Scheduling;Development Work Queue;In Development;Testing;Pending Push to Production'.split(';', 0);
        string[] icons = 'solution;approval;today;task;event;orders;custom;feed;goals'.split(';', 0);
        for (integer i=0; i<icons.size(); i++) {
            status s = new status(vals[i], icons[i]);
            statuses.add(s);
        }
        for (Salesforce_Request__c req : allReqs) {
            statusRequest sr = new statusRequest(req);
            statusReqs.add(sr);
        }
    }
    
    public void find() {
        enhancements = new List<request>();
        newEnhancements = new List<request>();
        projects = new List<request>();
        newProjects = new List<request>();
        string reqs_soql = 'SELECT id, Name, OwnerId, Owner.Name, Request_Type__c, Summary__c, Status__c, Requested_Date__c, Requested_By__c, Priority__c, Department_Sort_Order__c, Sort_Order__c, Requested_By__r.Name, Department__c, Actual_Minutes__c '+
            'FROM Salesforce_Request__c WHERE '+
            'Status__c !=  \'Completed\' AND Status__c !=  \'Cannot Add\' AND Status__c !=  \'Chosen Not to Add\' AND Status__c !=  \'Duplicate Request\' AND Status__c !=  \'Unable to Complete\' '+
            'AND (Request_Type__c =  \'Enhancement\' OR Request_Type__c =  \'Project\')';
        if (selDept == 'All') {
            allReqs = Database.query(reqs_soql+' ORDER BY Sort_Order__c');
        } else {
            allReqs = Database.query(reqs_soql+' AND Department__c = \''+selDept+'\' ORDER BY Sort_Order__c');
        }
        reqs_soql += 'AND Department__c = \''+dept+'\' ORDER BY Department_Sort_Order__c NULLS FIRST';
        integer eSort = 0;
        integer pSort = 0;
        for (Salesforce_Request__c sfr : Database.query(reqs_soql)) {
            if (sfr.Request_Type__c == 'Enhancement' && sfr.Ownerid == enhancementsQueue) {
                if (sfr.Department_Sort_Order__c != null) {
                    eSort++;
                    request req = new request(sfr, eSort);
                    enhancements.add(req);
                } else {
                    request req = new request(sfr, 0);
                    newEnhancements.add(req);
                }
                
            } 
            if (sfr.Request_Type__c == 'Project' && sfr.Ownerid == projectsQueue) {
                if (sfr.Department_Sort_Order__c != null) {
                    pSort++;
                    request req = new request(sfr, pSort);
                    projects.add(req);
                } else {
                    request req = new request(sfr, 0);
                    newProjects.add(req);
                }
            }
        }
        makeStatusWrappers();
    }
    
    public PageReference sortList() {
        // cycle through enhancements
        string selSortReqId = ApexPages.currentPage().getParameters().get('sortReqId');
        string selSortReqType = ApexPages.currentPage().getParameters().get('sortlist');
        Salesforce_Request__c[] upList = new list<Salesforce_Request__c>();
        if (selSortNum == null) {
            if (selSortReqType == 'Enhancement') {
                for (request r : enhancements) {
                    if (r.sfr.id == selSortReqId) {
                        r.sfr.Department_Sort_Order__c = 1;
                        upList.add(r.sfr);
                    } else {
                        r.sfr.Department_Sort_Order__c = r.sfr.Department_Sort_Order__c + 1;
                        upList.add(r.sfr);
                    }
                }
            }
            if (selSortReqType == 'Project') {
                for (request r : projects) {
                    if (r.sfr.id == selSortReqId) {
                        r.sfr.Department_Sort_Order__c = 1;
                        upList.add(r.sfr);
                    } else {
                        r.sfr.Department_Sort_Order__c = r.sfr.Department_Sort_Order__c + 1;
                        upList.add(r.sfr);
                    }
                }
            }
        } else {
            string dir = '';
            integer oldSortNum;
            if (selSortReqType == 'Enhancement') {
                for (request r : enhancements) {
                    if (r.sfr.id == selSortReqId) {
                        oldSortNum = integer.valueOf(r.sfr.Department_Sort_Order__c);
                        if (selSortNum < r.sfr.Department_Sort_Order__c) {
                            dir = 'up';
                        }
                        if (selSortNum > r.sfr.Department_Sort_Order__c) {
                            dir = 'down';
                        }
                    }
                }
                for (request r : enhancements) {
                    upList.add(r.sfr);
                    if (r.sfr.id != selSortReqId) {
                        if (dir == 'up') {
                            if (r.sfr.Department_Sort_Order__c == selSortNum || (r.sfr.Department_Sort_Order__c > selSortNum && r.sfr.Department_Sort_Order__c < oldSortNum)) {
                                r.sfr.Department_Sort_Order__c = r.sfr.Department_Sort_Order__c + 1;
                            }
                        }
                        if (dir == 'down') {
                            if (r.sfr.Department_Sort_Order__c == selSortNum || (r.sfr.Department_Sort_Order__c < selSortNum && r.sfr.Department_Sort_Order__c > oldSortNum)) {
                                r.sfr.Department_Sort_Order__c = r.sfr.Department_Sort_Order__c - 1;
                            }
                        }
                    } else {
                        r.sfr.Department_Sort_Order__c = selSortNum;
                    }
                }
            }
        }
        update upList;
        return new PageReference('/apex/sfrDepartmentManagement').setRedirect(true);
    }
    
    
    
    public class request {
        public Salesforce_Request__c sfr {get;set;}
        public string truncSum {get;set;}
        public string color {get;set;}
        public string dept {get;set;}
        public boolean isNew {get;set;}
        
        public request(Salesforce_Request__c req, integer num) {
            sfr = req;
            if (sfr.Department_Sort_Order__c == null) {
                sfr.Department_Sort_Order__c = 0;
            }
            dept = sfr.Department__c;
            if (sfr.Summary__c != null) {
                truncSum = sfr.Summary__c.abbreviate(35);
            } else {
                truncSum = '';
            }
            if (num == 0) {
                isNew = true;
            } else { 
                isNew = false;
            }
            if (isNew) {
                color = '#4BC076';
            } else {
                 color = '#CFD7E6';
            }
        }
    }
    
    public class status {
        public string name {get;set;}
        public string iconURL {get;set;}
        
        public status(string deptVal, string deptIcon) {
            name = deptVal;
            iconURL = '/resource/1451490667000/slds/assets/icons/standard-sprite/svg/symbols.svg#'+deptIcon;
        }
    }
    
    public class statusRequest {
        public string status {get;set;}
        public string truncSum {get;set;}
        public Salesforce_Request__c sfr {get;set;}
        
        public statusRequest(Salesforce_Request__c req) {
            sfr = req;
            status = sfr.Status__c;
            truncSum = sfr.Summary__c.abbreviate(24);
        }
    }
    
}