public class contractLineItemTriggerHandler {
    
    public static void beforeInsert(Contract_Line_Item__c[] oldContractLineItems, Contract_Line_Item__c[] newContractLineItems){
        Contract_Line_Item__c[] lineItemsWithProductTypes = new list<Contract_Line_Item__c>();
        
        for (integer i=0; i<newContractLineItems.size(); i++) {
            if (newContractLineItems[i].Product_Types__c != null){
                lineItemsWithProductTypes.add(newContractLineItems[i]);
            }
        }
        
        if(lineItemsWithProductTypes.size() > 0){
            contractLineItemUtility.formatProductTypes(lineItemsWithProductTypes);
        }
    }
    
    public static void afterInsert(Contract_Line_Item__c[] oldContractLineItems, Contract_Line_Item__c[] newContractLineItems){
        Contract_Line_Item__c[] ehsModules = new list<Contract_Line_Item__c>();
        for (integer i=0; i<newContractLineItems.size(); i++) {
            if (newContractLineItems[i].Is_EHS_Module__c){
                ehsModules.add(newContractLineItems[i]);
            }
        }
                
        if(ehsModules.size() > 0){
            contractUtility.updateEHSModules(ehsModules);
        }     
        
        
    }
    
    public static void afterDelete(Contract_Line_Item__c[] oldContractLineItems, Contract_Line_Item__c[] newContractLineItems){
        Contract_Line_Item__c[] ehsModules = new list<Contract_Line_Item__c>();
        
        for (integer i=0; i<oldContractLineItems.size(); i++) {
            if (oldContractLineItems[i].Is_EHS_Module__c){
                ehsModules.add(oldContractLineItems[i]);
            }
        }
        
        if(ehsModules.size() > 0){
            contractUtility.updateEHSModules(ehsModules);
        }
    }
    
}