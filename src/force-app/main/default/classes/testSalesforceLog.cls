@isTest
private class testSalesforceLog {

    static testmethod void test1(){
        String logmessage = 'Test';
        while(logmessage.length() < 131000){
            logmessage += 'TESTESTESTTESTESTESTESTEST';
        }
        salesforceLog.createLog('POI', true, 'testClass', 'testMethod', logmessage);
    }
    static testmethod void test2(){
        String logmessage = 'Test';
        while(logmessage.length() < 131000){
            logmessage += 'TESTESTESTTESTESTESTESTEST';
        }
        salesforceLog.createLog('POI', false, 'testClass', 'testMethod', logmessage);
    }
    
}