@isTest(SeeAllData = true)
private class testQuoteProposalNeedEdit {

    static testMethod void test1(){
        
        Quote_Proposal_Need__c need = new Quote_Proposal_Need__c();
        need.Need__c = 'Test Need';
        need.Description__c = 'Hello </li><li> There </li><li> Friend';
        insert need;
        
        ApexPages.currentPage().getParameters().put('id', need.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote_Proposal_Need__c());
        quoteProposalNeedEdit myController = new QuoteProposalNeedEdit(testController);
        
        myController.getFamilyOptions();
        
        //test that move to selected picklist method working
        myController.productWrappers[0].action = true;
        myController.moveToSelected();
        system.assertEquals(myController.productWrappers[0].action, false);
        system.assertEquals(myController.productWrappers[0].productStatus, 'Selected');
        
        //test that move to available picklist method working
        myController.productWrappers[0].action = true;
        myController.moveToAvailable();
        system.assertEquals(myController.productWrappers[0].action, false);
        system.assertEquals(myController.productWrappers[0].productStatus, 'Available');
        
        //test AddWrapper function works
        integer listSize =  myController.descriptionWrappers.size();
        myController.addWrapper();
        system.assertEquals(myController.descriptionWrappers.size(), listSize + 1);
        
        //test bullet is removed and list is reordered
        integer descrSize = myController.descriptionWrappers.size();
        ApexPages.currentPage().getParameters().put('bullet', '1');
        myController.removeBullet();
        system.assertEquals(myController.descriptionWrappers.size(), descrSize - 1);
        system.assertEquals(myController.descriptionWrappers[1].order, 1);
        
        
        myController.productFamily = 'Services - Core';
        myController.queryProducts();
        myController.getFamilyOptions();
        
        myController.updateNeed();
        
        myController = new quoteProposalNeedEdit(testController);
        
        
        myController.productWrappers[1].productStatus = 'Selected';   
        myController.need.Need__c = 'Hey';
        myController.descriptionWrappers[0].bullet = 'Hello';
        
        
        myController.updateNeed();
        
        myController.queryProducts();
        
        System.assertEquals(myController.descriptionWrappers[2].bullet, 'Friend');
        
        
        
        
        
    }
    
    static testMethod void deleteExistingProducts(){
        
        
        Quote_Proposal_Need__c need = new Quote_Proposal_Need__c();
        need.Need__c = 'Test Need';
        need.Description__c = 'Hello </li><li> There </li><li> Friend';
        insert need;
        
        Product2 product1 = new Product2();
        product1.Name = 'Test Product 1';
        product1.IsActive = true;
        product1.Can_Have_Needs__c = true;
        insert product1;
        
        
        Product2 product2 = new Product2();
        product2.Name = 'Test Product 2';
        product2.IsActive = true;
        product2.Can_Have_Needs__c = true;
        insert product2;
        
        
        Product2 product3 = new Product2();
        product3.Name = 'Test Product 3';
        product3.IsActive = true;
        product3.Can_Have_Needs__c = true;
        insert product3;
        
        Product_Need__c prodNeed1 = new Product_Need__c(Quote_Proposal_Need__c = need.id, product__c = product1.id);
        insert prodNeed1;
        
        Product_Need__c prodNeed2 = new Product_Need__c(Quote_Proposal_Need__c = need.id, product__c = product2.id);
        insert prodNeed2;
        
        Product_Need__c prodNeed3 = new Product_Need__c(Quote_Proposal_Need__c = need.id, product__c = product3.id);
        insert prodNeed3;
        
        ApexPages.currentPage().getParameters().put('id', need.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote_Proposal_Need__c());
        quoteProposalNeedEdit myController = new quoteProposalNeedEdit(testController);
        
        for (integer i=0; i < myController.productWrappers.size(); i++) {
            if (myController.productWrappers[i].productStatus == 'Selected' && myController.productWrappers[i].existing == true) {
                myController.productWrappers[i].action = true;
            }
        }
       
        myController.moveToAvailable();
        myController.productWrappers[0].action = true;
        myController.moveToSelected();
        myController.updateNeed();
        myController.needsManagementHome();
        
        List<Product_Need__c> needList = [select Name FROM Product_Need__c WHERE Id = :myController.productWrappers[0].prod.id];
        system.assertEquals(needList.size(), 0);
        
        
        
        
}
}