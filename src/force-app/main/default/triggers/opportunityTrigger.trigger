trigger opportunityTrigger on Opportunity (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    if (trigger.isBefore) {
        
        // BEFORE INSERT
        if (trigger.isInsert) {
            opportunityTriggerHandler.beforeInsert(trigger.new);
        }
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            opportunityTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
        
        // BEFORE DELETE
        if (trigger.isDelete) {
            opportunityTriggerHandler.beforeDelete(trigger.old, trigger.new);
        }
        
    }
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            opportunityTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            opportunityTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
        // AFTER DELETE
        if (trigger.isDelete) {
            opportunityTriggerHandler.afterDelete(trigger.old, trigger.new);
        }
        
        // AFTER UNDELETE
        if (trigger.isUndelete) {
            opportunityTriggerHandler.afterUndelete(trigger.old, trigger.new);
        }
        
    }
    
    if(!test.isRunningTest()){
        dlrs.RollupService.triggerHandler(); 
    }
}