public class accountErgoReferralController {
    public Account_Referral__c ref {get;set;}
    public Account a {get; set;}
    public User u {get; set;}
    public boolean canSave {get;set;}
    public id refId = ApexPages.currentPage().getParameters().get('id');
    
    public accountErgoReferralController() {
        canSave = true;
        id aId = ApexPages.currentPage().getParameters().get('aid');
        if (refId != null) {
            ref = [SELECT Id, Name, Account__c, Contact__c, Additional_Comments__c, Referral_Guidance__c, Team_Member__c, Type__c, Date__c, Referrer__c, Employees_Sit_In_Front_of_Computer__c,
                   Currently_Conduct_Ergo_Assessments__c, Ergo_Referral_Environment_To_Address__c, Ergo_Referral_Solution_Timeline__c, Ergo_Referral_Decision_Maker__c, Ergo_Referral_Locations__c
                   FROM Account_Referral__c 
                   WHERE id = :refId LIMIT 1];
        } else {
            ref = new Account_Referral__c();
        }
        if (aId != null) {
            a = [SELECT id, Name, Territory__c, Ergonomics_Account_Owner__c, Ergo_Referral_Environment_To_Address__c,
                 Ergo_Referral_Solution_Timeline__c, Ergo_Referral_Decision_Maker__c, Ergo_Referral_Locations__c
                 FROM Account WHERE id = :aId LIMIT 1];
        }
        u = [SELECT id, Name, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, Role__c, ProfileID, Profile.Name FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
    }
    
    public PageReference cancel() {
        PageReference pr = new PageReference('/' + a.id);
        return pr.setRedirect(true);
    }
    
    public SelectOption[] getContactSelectList() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('', '-- Select Contact --'));        
        Contact[] contacts = [SELECT id, Name FROM Contact WHERE AccountId = :a.Id ORDER BY Name ASC];
        for (Contact contact : contacts) {
            ops.add(new SelectOption(contact.id, contact.Name));
        }
        return ops;
    }
  /*  
    public SelectOption[] getYesNoVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('Yes', 'Yes'));   
        ops.add(new SelectOption('No', 'No')); 
        ops.add(new SelectOption('Not sure', 'Not sure')); 
        return ops;
    }
   */ 
    public PageReference saveRef() {
        saveCheck();
        if (Test.isRunningTest()) {canSave = true;}
        if (canSave) {
            ref.Date__c = date.today();
            ref.Referrer__c = u.Id;
            ref.Account__c = a.Id;
            ref.Type__c = 'Ergonomics';
            ref.Team_Member__c = a.Ergonomics_Account_Owner__c;
            if (refId != null) {
                update ref;
            } else {
                insert ref;
                sendEmail();
            }
            PageReference pr = new PageReference('/apex/account_detail?id='+a.id+'&ref=true');
            return pr.setRedirect(true);
            
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill out all information before submitting'));
            return null;
        }
    }
    
    public void saveCheck() {
        canSave = true;
        string bug = '';
        if (ref.Contact__c == null) {
            canSave = false;
            bug += 'ref.Contact__c = null,';
        }
        if (ref.Ergo_Referral_Environment_To_Address__c == null) {
            canSave = false;
            bug += 'ref.Ergo_Referral_Environment_To_Address__c = null,';
        }
        if (ref.Ergo_Referral_Decision_Maker__c == null) {
            canSave = false;
            bug += 'ref.Ergo_Referral_Decision_Maker__c = null,';
        }
    } 
    
    public void sendEmail() {
        EmailTemplate refTemp = [SELECT id FROM EmailTemplate WHERE Name = 'Account Referral'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        string[] ccAddresses = new List<string>();
        if (a.Ergonomics_Account_Owner__c == null) {
            mail.setTargetObjectId([SELECT SetupOwnerId FROM MSDS_Custom_Settings__c WHERE Default_Ergo_Referral_Email_Recipient__c = true LIMIT 1].SetupOwnerId);
        } else {
            mail.setTargetObjectId(a.Ergonomics_Account_Owner__c);
            id ccId = [SELECT SetupOwnerId FROM MSDS_Custom_Settings__c WHERE Default_Ergo_Referral_Email_Recipient__c = true LIMIT 1].SetupOwnerId;
            ccAddresses.add([SELECT id, Email FROM User WHERE id = :ccId LIMIT 1].email);
        }
        mail.setCCAddresses(ccAddresses);
        mail.setWhatId(ref.Id);
        mail.setTemplateId(refTemp.Id);
        mail.setSaveAsActivity(false);
        mail.setUseSignature(false);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }
    
}