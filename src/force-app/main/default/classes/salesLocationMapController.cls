public with sharing class salesLocationMapController {
    private Apexpages.StandardController controller; 
    public User u {get;set;}
    public Account a {get;set;}
    public Account[] aList {get;set;}
    public SDSMethod[] SDSwrappers {get;set;}
    public activeProduct[] MSDSwrappers {get;set;}
    public activeProduct[] SERVwrappers {get;set;}
    public activeProduct[] SOLwrappers {get;set;}
    public activeProduct[] PRODwrappers {get;set;}
    public SelectOption[] countySelectList {get;set;}
    public Id aId {get;set;}
    public String selCustomerStatus {get;set;}
    public String selNAICSCode {get;set;}
    public String selTerritory {get;set;}
    public String selCounty {get;set;}
    public String selNumEmployee {get;set;}
    public String selCloseDateMo {get;set;}
    public String selRegion {get;set;}
    public String primaryNAICS {get;set;}
    public activeProduct[] selProducts {get;set;}
    public String selRating {get;set;}
    public Boolean vc_SDSMethods {get;set;}
    public Boolean showMap {get;set;}
    public Account[] SOSLaccts {get;set;}
    public String aString {get;set;}
    public String aLabel {get;set;}
    public String aListId {get;set;}
    public acctWrapper[] a_wrappers {get;set;}
    public acctWrapper a_wrap {get;set;}
    public Boolean displayMap {get;set;}
    public Boolean vc_Products {get;set;}
    public String sortBy {get;set;}
    public String selBeds {get;set;}
    public Boolean keyCompAdded {get;set;}
    public String startDate {get;set;}
    public String endDate {get;set;}
    public Date closeDate {get;set;}
    public Integer pages {get;set;}
    public Boolean newPage {get;set;}
    public Integer offNum {get;set;}
    public Integer totalSize {get;set;}
    public Integer pageNum {get;set;}
    public Integer pageOffset {get;set;}
    public Integer[] pageList {get;set;}
    public Map<Integer,String> pageMap {get;set;}
    public boolean maxCounties {get;set;}
    public String SelectedState {get;set;}
    public string SearchType {get;set;}
    public string AccountType {get;set;}
    string keyCompId;
    
    public salesLocationMapController(ApexPages.StandardController stdController){
        newPage = TRUE;
        offnum = 0;
        pageNum = 1;
        if( startDate == NULL ){
            startDate = 'MM/DD/YYYY';
        }
        
        if( endDate == NULL ) {
            endDate = 'MM/DD/YYYY';
        }
        
        this.controller = stdController;
        aId = ApexPages.currentPage().getParameters().get('id');
        String dMap = ApexPages.currentPage().getParameters().get('map');
        
        // Parameter to show Google Maps
        if( dMap == 'true' ) {
            displayMap = TRUE;
        }
        
        u = [ SELECT Id, Name, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, Role__c, ProfileID, Profile.Name
             FROM User 
             WHERE Id = :UserInfo.getUserId() LIMIT 1 ];
        
        a = [ SELECT Id, Name, Customer_Status__c, NAICS_Code__c, Key_Competitors__c, BillingCity, BillingStreet, BillingPostalCode, BillingState, Territory__c, Territory__r.Name,
             Billing_County__c, BillingCountry, Num_of_Employees__c, Key_Authoring_Competitors__c, Key_Training_Competitors__c, Key_Ergo_Competitors__c, Key_EHS_Competitors__c, 
             Territory__r.Territory_Owner__c, Territory__r.Online_Training_Owner__c, Territory__r.Authoring_Owner__c, Territory__r.Ergonomics_Owner__c, Territory__r.EHS_Owner__c, Territory__r.SLED_Owner__c,
             OwnerId, Online_Training_Account_Owner__c, Authoring_Account_Owner__c, EHS_Owner__c, Ergonomics_Account_Owner__c
             FROM Account
             WHERE Id = :aId LIMIT 1 ];
        
        // Create the address string for the main account to show in Google Maps
        aString = a.BillingStreet+', '+a.BillingCity+', '+a.BillingPostalCode;
        
        
        // Set the on-load filter values
        if( a.Customer_Status__c == 'Prospect' ) {
            selCustomerStatus = 'Active';
        }
        
        if( a.Customer_Status__c == 'Active' ) {
            selCustomerStatus = 'Prospect';
        }
        
        selNAICSCode = a.NAICS_Code__c;
        //selTerritory = a.Territory__c;
        String co = a.Billing_County__c;
        selCounty = co;
        
        
        aList = new List<Account>();
        a_wrappers = new List<acctWrapper>();
        
        SDSwrappers = new List<SDSMethod>();
        Schema.DescribeFieldResult SDSMethod = Account.Current_Chemical_Management_System__c.getDescribe();
        List<Schema.PicklistEntry> SDSvals = SDSMethod.getPicklistValues();
        for( Schema.PicklistEntry SDSval : SDSvals ){
            String SDSstring = String.valueOf( SDSval.getValue() );
            SDSMethod SDSm = new SDSMethod( SDSstring );
            SDSwrappers.add( SDSm );
        }
        
        PRODwrappers = new List<activeProduct>();
        MSDSwrappers = new List<activeProduct>();
        SERVwrappers = new List<activeProduct>();
        SOLwrappers = new List<activeProduct>();
        
        Schema.DescribeFieldResult aSol = Account.Active_Additional_Compliance_Solutions__c.getDescribe();
        List<Schema.PicklistEntry> aSolVals = aSol.getPicklistValues();
        for( Schema.PicklistEntry aSolVal : aSolVals ){
            String PRODstring = String.valueOf( aSolVal.getValue() );
            String CATstring = 'solutions';
            activeProduct aP = new activeProduct( PRODstring, CATstring );
            SOLwrappers.add( aP );
            PRODwrappers.add( aP );
        }
        
        Schema.DescribeFieldResult aServ = Account.Active_Compliance_Services_Products__c.getDescribe();
        List<Schema.PicklistEntry> aServVals = aServ.getPicklistValues();
        for( Schema.PicklistEntry aServVal : aServVals ){
            String PRODstring = String.valueOf( aServVal.getValue() );
            String CATstring = 'services';
            activeProduct aP = new activeProduct( PRODstring, CATstring );
            SERVwrappers.add( aP );
            PRODwrappers.add( aP );
        }
        
        Schema.DescribeFieldResult aMSDS = Account.Active_MSDS_Management_Products__c.getDescribe();
        List<Schema.PicklistEntry> aMSDSVals = aMSDS.getPicklistValues();
        for( Schema.PicklistEntry aMSDSVal : aMSDSVals ){
            String PRODstring = String.valueOf( aMSDSVal.getValue() );
            String CATstring = 'msds';
            activeProduct aP = new activeProduct( PRODstring, CATstring );
            MSDSwrappers.add( aP );
            PRODwrappers.add( aP );
        }
        vc_Products = FALSE;
        vc_SDSMethods = FALSE;
        find();
        newPage = FALSE;
        queryCounties();
        
        SearchType = 'All Accounts';
        selRegion = 'United States/Canada';
        
        
    }
    
    public void find() {
        if (newPage == false) {
            addUse();
        }
        a_wrappers.clear();
        aList.clear();
        String soql = 'SELECT Id, Name, Num_of_Employees__c, Oldest_Closed_Won_Opportunity__c, Customer_Status__c,Num_of_Locations__c, Num_Of_Completed_Events__c, '+
            'NAICS_Code__c, Territory__c, Territory__r.Name, Billing_County__c, BillingStreet, BillingState, BillingCity, BillingCountry, Key_Competitors__c, Rating, Num_of_Completed_Activities__c, '+
            'Key_Authoring_Competitors__c, Key_Training_Competitors__c, Key_Ergo_Competitors__c, Key_EHS_Competitors__c '+
            'FROM Account WHERE';
        
        if( selCustomerStatus != 'All' ) {
            soql += ' Customer_Status__c = \''+selCustomerStatus+'\''; 
        }
        
        if( selCustomerStatus == 'All' ) {
            soql += ' Customer_Status__c != NULL ';
        }
        
        if( selNAICSCode != NULL && selNAICSCode != 'All' ){
            soql += ' AND NAICS_Code__c = \''+selNAICSCode+'\'';
        }
        
        if( primaryNAICS != NULL && primaryNAICS != '' ) {
            soql += ' AND Primary_NAICS_Code__c = \''+primaryNAICS+'\'';
        }
        
        if( selRegion == 'International') {
            
            soql += ' AND BillingCountry != \'United States\'';
            soql += ' AND BillingCountry != \'Canada\'';
            soql += ' AND BillingCountry != NULL ';
            
        }
        
        
        if( selRegion == 'United States/Canada')  {
            
            soql += ' AND (BillingCountry = \'United States\' OR BillingCountry = \'Canada\')';
            
            if( selTerritory != NULL && selTerritory != 'All' ){
                soql += ' AND Territory__c = \''+selTerritory+'\'';
            }
            
            if(maxCounties != true) {
                if( selCounty != NULL && selCounty != 'All' ) {	
                    string countyOnly = selCounty.substringAfter(' - ');
                    soql += ' AND Billing_County__c = \''+string.escapeSingleQuotes(countyOnly)+'\'';
                }
            }
            
            
            if(SelectedState != null){
                soql +=' AND BillingState = \''+string.valueof(SelectedState)+'\'';
                
            }
        }
        
        if(SearchType == 'My Accounts'){
                 soql +=' AND (EHS_Owner__c = \''+string.valueof(u.id)+'\' OR Ergonomics_Account_Owner__c = \''+string.valueof(u.id)+'\' OR Authoring_Account_Owner__c = \''+string.valueof(u.id)+'\' OR OwnerId = \''+string.valueof(u.id)+'\' OR Online_Training_Account_Owner__c = \''+string.valueof(u.id)+'\') ';
            }

        if(SearchType == 'Accounts in My Territories'){
            
            soql +=' AND (Territory__r.EHS_Owner__c = \''+string.valueof(u.id)+'\' OR Territory__r.Ergonomics_Owner__c = \''+string.valueof(u.id)+'\' OR Territory__r.Authoring_Owner__c = \''+string.valueof(u.id)+'\' OR Territory__r.Territory_Owner__c = \''+string.valueof(u.id)+'\' OR Territory__r.SLED_Owner__c = \''+string.valueof(u.id)+'\' OR Territory__r.Online_Training_Owner__c = \''+string.valueof(u.id)+'\') ';
        }
        

     if (AccountType == 'Enterprise'){

            soql += ' AND Enterprise_Sales__c = TRUE';
        }

     if(AccountType == 'Mid-Market'){

         soql += ' AND Enterprise_Sales__c = FALSE';
     }
        



        if( startDate != 'MM/DD/YYYY' && startDate != NULL && endDate != 'MM/DD/YYYY' && endDate != NULL && startDate != '' && endDate != '' ) {
            String dateStart = String.valueOf( Date.parse( startDate ) );
            String dateEnd = String.valueOf( Date.parse( endDate ) );
            soql += ' AND Oldest_Closed_Won_Opportunity__c > '+dateStart+' AND Oldest_Closed_Won_Opportunity__c < '+dateEnd+'';            
        }
        
        if( selBeds != 'All' && selBeds != NULL ){
            if( selBeds == '0' ) {
                soql += ' AND Num_Of_Beds__c = 0';
            }
            if( selBeds == '1001+' )  {
                soql += ' AND Num_Of_Beds__c > 1000';
            }
            if(selBeds != '0' && selBeds != '1001+')  {
                String[] beds = selBeds.split( ' - ', 0 );
                Integer bed0 = Integer.valueOf( beds[0] );
                Integer bed1 = Integer.valueOf( beds[1] ) + 1;
                soql += ' AND Num_of_Beds__c > '+bed0+' AND Num_of_Beds__c < '+bed1;
            }
        }
        
        if( selNumEmployee != 'All' && selNumEmployee != NULL ) {
            if( selNumEmployee != '5001+' ) {
                String[] emps = selNumEmployee.split( ' - ', 0 );
                Integer emp0 = Integer.valueOf( emps[0] );
                Integer emp1 = Integer.valueOf( emps[1] ) + 1;
                soql += ' AND Num_of_Employees__c > '+emp0+' AND Num_of_Employees__c < '+emp1;
            }
            else{
                soql += ' AND Num_Of_Employees__c > 5000';
            }
        }
        
        activeProduct[] checkedAPs = new List<activeProduct>();
        checkedAPs.clear();
        for( activeProduct p : PRODwrappers ) {
            if( p.isChecked == TRUE )
            {
                checkedAPs.add( p );
            }
        }
        
        if( checkedAPs.size() != PRODwrappers.size() ){
            for( Integer iC = 0; iC < checkedAPs.size(); iC++ )
            {
                if( iC == 0 ) {
                    soql += ' AND ( Active_Products__c INCLUDES(\''+checkedAPs[0].prodVal+'\') OR';
                }
                
                if( iC > 0 && iC < checkedAPs.size() - 1 ) {
                    soql += ' Active_Products__c INCLUDES(\''+checkedAPs[iC].prodVal+'\') OR';
                }
                
                if( iC == checkedAPs.size() - 1 ) {
                    soql += ' Active_Products__c INCLUDES(\''+checkedAPs[iC].prodVal+'\') )';
                }
            }
        }
        
        SDSMethod[] checkedSDSs = new List<SDSMethod>();
        checkedSDSs.clear();
        for( SDSMethod s : SDSwrappers ) {
            if( s.isChecked == TRUE ) {
                checkedSDSs.add( s );
            }
        }
        
        if( checkedSDSs.size() != SDSwrappers.size() ){
            for( Integer iC = 0; iC < checkedSDSs.size(); iC++ ){
                if( iC == 0 ){
                    soql += ' AND ( Current_Chemical_Management_System__c = \''+checkedSDSs[0].SDSmVal+'\' OR';
                }
                
                if( iC > 0 && iC < checkedSDSs.size() - 1 ){
                    soql += ' Current_Chemical_Management_System__c = \''+checkedSDSs[iC].SDSmVal+'\' OR';
                }
                
                if( iC == checkedSDSs.size() - 1 ){
                    soql += ' Current_Chemical_Management_System__c = \''+checkedSDSs[iC].SDSmVal+'\' )';
                }
            }
        }
        
        if( selRating != 'All' && selRating != NULL ){
            soql += ' AND Rating = \''+selRating+'\'';
        }
        
        if( sortBy != NULL ){
            soql += ' ORDER BY '+sortBy+' NULLS LAST';
        }
        
        String soqlSize = soql+' LIMIT 500';
        soql += ' LIMIT 50 OFFSET '+offNum;
        aList = Database.query(soql);
        totalSize = Database.query( soqlSize ).size();
        
        
        if( totalSize <= 50 ){
            pages = 1;
        }
        
        else{
            pages = totalSize / 50 + 1;
        }
        
        pageList = new List<Integer>();
        pageMap = new Map<Integer,String>();
        for( Integer xP = 1; xP < pages + 1; xP++ ){
            pageList.add( xP );
            Integer numStart = ( xP * 50 ) - 49;
            Integer numEnd = 0;
            if( xP == pages ){
                numEnd = ( xP * 50 ) + (totalSize - ( xP * 50 ) );
            }
            
            else{
                numEnd = xP * 50;
            }
            String mapVal = numStart+'-'+numEnd+' of '+totalSize;
            pageMap.put( xP, mapVal );
            
        }
        //key comps
        for (Account keyA : aList) {
            acctWrapper newWrap = new acctWrapper(keyA);
            checkKeyComps(newWrap);
            a_wrappers.add(newWrap);
        }
          
    }
    
    public List<SelectOption> getCustomerStatuses(){ 
        List<SelectOption> o = new List<SelectOption>();      
        o.add( new SelectOption( 'All', '- All -' ) );
        o.add( new SelectOption( 'Active', 'Active' ) );
        o.add( new SelectOption( 'Inactive', 'Inactive' ) );
        o.add( new SelectOption( 'Prospect', 'Prospect' ) );
        
        return o;
    }
    
    public List<SelectOption> getRegions(){ 
        List<SelectOption> r = new List<SelectOption>();      
        r.add( new SelectOption( 'United States/Canada', 'United States/Canada' ) );
        r.add( new SelectOption( 'International', 'International' ) );
        
        
        return r;
    }
    
    public List<SelectOption> getNAICSCodes(){ 
        List<SelectOption> o = new List<SelectOption>();      
        o.add( new SelectOption( 'All', '- All -' ) ); 
        Schema.DescribeFieldResult fieldResult = Account.NAICS_Code__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry entry : picklistEntries ){
            o.add( new SelectOption( entry.getValue() , entry.getLabel() ) ); 
        }
        
        return o;
    }
   /* 
    public List<SelectOption> getTerritories(){
        List<SelectOption> o = new List<SelectOption>();
        o.add( new SelectOption( 'All', '- All -' ) );
        List<Territory__c> terrs = [ SELECT Id, Name
                                    FROM Territory__c 
                                    ORDER BY Name ASC ];
        
        for( Territory__c terr : terrs ){
            o.add( new SelectOption( terr.Id, terr.Name ) );
        }
        return o;
    }
   */ 
    public void queryCounties(){
        County_Zip_Code__c[] countiesList = [SELECT id, County__c, State__c FROM County_Zip_Code__c WHERE State__c = :SelectedState  ORDER BY County__c ASC LIMIT 40000];
        Set<string> zipSet = new Set<string>();
        for (County_Zip_Code__c zip : countiesList){
            zipSet.add(zip.State__c+' - '+zip.County__c);
        }
        if(countiesList.size() == 40000) {maxCounties = true;}
        else {maxCounties = false;}
        countySelectList = new List<SelectOption>();
        countySelectList.add(new SelectOption('All', '- All -'));
        for (string zip : zipSet){
            countySelectList.add(new SelectOption(zip, zip));
        }
        zipSet.clear();
    }
    
    public List<SelectOption> getNumEmployees(){
        List<SelectOption> o = new List<SelectOption>();      
        o.add( new SelectOption( 'All', '- All -' ) ); 
        String[] selOps = '0 - 49;50 - 200;201 - 500;501 - 1000;1001 - 2500;2501 - 5000;5001+'.split( ';', 0 );
        for( String sO : selOps ){
            o.add( new SelectOption( sO, sO ) );
        }
        
        return o;
    }
    
    public List<SelectOption> getNumBeds(){
        List<SelectOption> o = new List<SelectOption>();      
        o.add( new SelectOption( 'All', '- All -' ) ); 
        String[] selOps = '0;1 - 20;21 - 50;51 - 250;251 - 500;501 - 1000;1001+'.split( ';', 0 );
        for( String sO : selOps ){
            o.add( new SelectOption( sO, sO ) );
        }
        
        return o;
    }
    
    public List<SelectOption> getCloseDateMo(){
        List<SelectOption> o = new List<SelectOption>();
        o.add( new SelectOption( 'All', 'Month' ) ); 
        String ds = 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec';
        for( String d : ds.split( ',', 0 ) )
        {
            o.add( new SelectOption( d, d ) );
        }
        
        return o;
    }
    
    public List<SelectOption> getRatings(){
        List<SelectOption> o = new List<SelectOption>();      
        o.add( new SelectOption( 'All', '- All -' ) ); 
        Schema.DescribeFieldResult fieldResult = Account.Rating.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry entry : picklistEntries ){
            o.add( new SelectOption( entry.getValue(), entry.getLabel() ) ); 
        }
        
        return o;
    }
    
    public void hideSDSMethods(){
        vc_SDSMethods = FALSE;
    }
    
    public void grabKeyCompId() {
        keyCompId = ApexPages.currentPage().getParameters().get('aListId');
        if (a_Wrappers.size() > 0) {
            for (acctWrapper aw : a_Wrappers) {
                if (aw.aWrap.id == keyCompId) {
                    a_wrap = aw; 
                }
            }
        }
    }
    
    public void checkKeyComps(acctWrapper xAcctWrap) {
        if (xAcctWrap.aWrap.id != null) {
            //key chemical mgmt competitors
            if (a.Key_EHS_Competitors__c != null) {
                for (string key : a.Key_EHS_Competitors__c.deleteWhiteSpace().split(',')) {
                    if (key == xAcctWrap.aWrap.Name.deleteWhiteSpace()) {
                        xAcctWrap.key_ehs_added = true;
                    }
                }
            }
            //key chemical mgmt competitors
            if (a.Key_Competitors__c != null) {
                for (string key : a.Key_Competitors__c.deleteWhiteSpace().split(',')) {
                    if (key == xAcctWrap.aWrap.Name.deleteWhiteSpace()) {
                        xAcctWrap.key_chemMgmt_added = true;
                    }
                }
            }
            //key authoring competitors
            if (a.Key_Authoring_Competitors__c != null) {
                for (string key : a.Key_Authoring_Competitors__c.deleteWhiteSpace().split(',')) {
                    if (key == xAcctWrap.aWrap.Name.deleteWhiteSpace()) {
                        xAcctWrap.key_auth_added = true;
                    }
                }
            }
            //key training competitors
            if (a.Key_Training_Competitors__c != null) {
                for (string key : a.Key_Training_Competitors__c.deleteWhiteSpace().split(',')) {
                    if (key == xAcctWrap.aWrap.Name.deleteWhiteSpace()) {
                        xAcctWrap.key_train_added = true;
                    }
                }
            }
            //key ergo competitors
            if (a.Key_Ergo_Competitors__c != null) {
                for (string key : a.Key_Ergo_Competitors__c.deleteWhiteSpace().split(',')) {
                    if (key == xAcctWrap.aWrap.Name.deleteWhiteSpace()) {
                        xAcctWrap.key_ergo_added = true;
                    }
                }
            }
            
            //check all
            if (xAcctWrap.key_ehs_added == true && xAcctWrap.key_chemMgmt_added == true && xAcctWrap.key_auth_added == true && xAcctWrap.key_train_added == true && xAcctWrap.key_ergo_added == true) {
                xAcctWrap.allKeysAdded = true;
            } else {
                xAcctWrap.allKeysAdded = false;
            }
        }
    }
    
    public void saveKeyComp() {
        if (keyCompId != null) {
            boolean updAcct = false;
            string keyName = a_wrap.aWrap.Name;
            if (a_wrap.key_ehs == true) {
                updAcct = true;
                if (a.Key_EHS_Competitors__c != null) {
                    a.Key_EHS_Competitors__c = a.Key_EHS_Competitors__c+', '+keyName;
                } else {
                    a.Key_EHS_Competitors__c = keyName;
                }
            }
            if (a_wrap.key_chemMgmt == true) {
                updAcct = true;
                if (a.Key_Competitors__c != null) {
                    a.Key_Competitors__c = a.Key_Competitors__c+', '+keyName;
                } else {
                    a.Key_Competitors__c = keyName;
                }
            }
            if (a_wrap.key_auth == true) {
                updAcct = true;
                if (a.Key_Authoring_Competitors__c != null) {
                    a.Key_Authoring_Competitors__c = a.Key_Authoring_Competitors__c+', '+keyName;
                } else {
                    a.Key_Authoring_Competitors__c = keyName;
                }
            }
            if (a_wrap.key_train == true) {
                updAcct = true;
                if (a.Key_Training_Competitors__c != null) {
                    a.Key_Training_Competitors__c = a.Key_Training_Competitors__c+', '+keyName;
                } else {
                    a.Key_Training_Competitors__c = keyName;
                }
            }
            if (a_wrap.key_ergo == true) {
                updAcct = true;
                if (a.Key_Ergo_Competitors__c != null) {
                    a.Key_Ergo_Competitors__c = a.Key_Ergo_Competitors__c+', '+keyName;
                } else {
                    a.Key_Ergo_Competitors__c = keyName;
                }
            }
            if (updAcct == true) {
                update a;
            }
        }
        sortOrder();
    }
    
    public Class acctWrapper {
        public Account aWrap {get;set;}
        public String aWrap_img {get;set;}
        //key comp added booleans
        public boolean key_ehs_added {get;set;}
        public boolean key_chemMgmt_added {get;set;}
        public boolean key_auth_added {get;set;}
        public boolean key_train_added {get;set;}
        public boolean key_ergo_added {get;set;}
        public boolean allKeysAdded {get;set;}
        //key comp checkboxes
        public boolean key_ehs {get;set;}
        public boolean key_chemMgmt {get;set;}
        public boolean key_auth {get;set;}
        public boolean key_train {get;set;}
        public boolean key_ergo {get;set;}
        
        public acctWrapper(Account aL) {
            aWrap = aL;
            
        } 
    }
    
    public Class SDSMethod{
        public Boolean isChecked {get;set;}
        public String SDSmVal {get;set;}
        
        public SDSMethod( String SDSstring )
        {
            SDSmVal = SDSstring;
            isChecked = TRUE;
        }
    }
    
    public Class activeProduct{
        public Boolean isChecked {get;set;}
        public String prodVal {get;set;}
        public String cat {get;set;}
        
        public activeProduct( String PRODstring, String CATstring  )
        {
            isChecked = TRUE;
            prodVal = PRODstring;
            cat = CATstring;
        }
    }
    
    public void sortOrder(){
        find();  
    }
    
    public void showOnMap(){
        Account aLoc = [ SELECT Id, Name, Customer_Status__c, NAICS_Code__c, BillingStreet, BillingCity, BillingPostalCode
                        FROM Account
                        WHERE Id = :aListId LIMIT 1 ];
        
        aString = a.BillingStreet+', '+a.BillingCity+', '+a.BillingPostalCode
            +'+'+
            aLoc.BillingStreet+', '+aLoc.BillingCity+', '+aLoc.BillingPostalCode;
        
        aLabel = '<b>Account Name:</b> '+aLoc.Name+'<br/>'+
            '<b>Customer Status:</b> '+aLoc.Customer_Status__c;
    }
    
    public void addUse(){
        Integer useSize = [ SELECT Id FROM SF_Usage__c WHERE Name = :u.Name ].size();
        if( useSize == 0 )
        {
            SF_Usage__c nu = new SF_Usage__c();
            nu.Name = u.Name;
            nu.AccountLocator_Searches__c = 1;
            nu.AccountLocator_Last_Use__c = date.TODAY();
            insert nu;
        }
        
        else{
            SF_Usage__c use = [ SELECT Id, Name, AccountLocator_Last_Use__c, AccountLocator_Searches__c, AccountLocator_Views__c 
                               FROM SF_Usage__c
                               WHERE Name = :u.Name LIMIT 1 ];
            use.AccountLocator_Searches__c = use.AccountLocator_Searches__c + 1;
            use.AccountLocator_Last_Use__c = date.TODAY();
            update use;
        }
        
    }
    
    public void nextPage(){
        offNum = ( pageNum * 50 ) - 50;
        find();
    }
    
    
    Public List<SelectOption> getStates(){
        
        List<SelectOption> stateOptions = new List<SelectOption>();
        
        schema.DescribeFieldResult StateFields = County_Zip_Code__c.state__c.getDescribe();
        
        stateOptions.add(new SelectOption('', '-Select State-'));
        
        List<Schema.PicklistEntry> ple = StateFields.getPicklistValues();
        
        for(Schema.PicklistEntry x : ple){
            stateOptions.add(new SelectOption(x.getLabel(), x.getValue()));
        }    
        return stateoptions;
        
        
        
    }
    
}