/*
this class runs once a day and will set the contact fields for marketo to read (just for onboarding and services surveys)
*/
public with sharing class ETS_surveyScheduler implements Schedulable {
    string[] testList = new List<string>();
    
    public void execute(SchedulableContext ctx) {
        testList.add('running');
        Set<id> conSet = new Set<id>();
        Set<id> survSentSet = new Set<id>();
        Survey_Queue__c[] queuedSurveys = new List<Survey_Queue__c>();
        Survey_Queue__c[] delQueues = new List<Survey_Queue__c>();
        integer numSent = 0;
        
        // clear the lists (not sure if needed)
        if (queuedSurveys.size() > 0) {
            queuedSurveys.clear();
        }
        
        if (delQueues.size() > 0) {
            delQueues.clear();
        }
        
        // 1) query all the queue records
        
        for (Survey_Queue__c sq : [SELECT id, Contact__c, Send_Date__c, Survey__c, Survey_Type__c, Survey_Num__c, Survey_Forward_URL__c, Survey_URL__c 
                                   FROM Survey_Queue__c 
                                   WHERE Survey_Type__c != 'ETS - Support' 
                                   AND isDeleted = false
                                   ORDER BY Send_Date__c ASC]) {
                                       queuedSurveys.add(sq);
                                   }
        
        
        // 2) loop through the queue records and add the contact ids to a set
        for (Survey_Queue__c qs : queuedSurveys) {
            conSet.add(qs.Contact__c);  
        }
        
        // 3) query the contacts from the contact id set that dont have a survey currently being sent out
        Contact[] openContacts = [SELECT id, Survey_Number__c FROM Contact WHERE id IN :conSet AND ETS_Survey_Number__c = null];      
        
        // 4) loop through the contacts
        for (Contact oc : openContacts) {
            
            // 5) create a list of queues for the contact
            Survey_Queue__c[] surveys = new List<Survey_Queue__c>();    
            for (Survey_Queue__c qs : queuedSurveys) {
                if (oc.id == qs.Contact__c) {
                    surveys.add(qs);
                }
            } 
            
            // 6) loop through the queues and determine whether any surveys should have been sent or should be sent today
            boolean sendNow = false;
            for (Survey_Queue__c s : surveys) {
                if (s.Send_Date__c == date.today() || s.Send_Date__c < date.today()) {
                    sendNow = true;
                }
            }
            
            // 7) if there is a survey to send, update the contact with the first survey found in the list (the earliest triggered survey)
            if (sendNow == true) {
                oc.ETS_Survey_URL__c = surveys[0].Survey_URL__c.substringAfter('://');
                oc.ETS_Forward_Survey_URL__c = surveys[0].Survey_Forward_URL__c.substringAfter('://');
                oc.ETS_Survey_Number__c = surveys[0].Survey_Num__c;
                oc.Last_Transactional_Survey_Sent_Date__c = date.today();
                oc.ETS_Survey_Step__c = 'Survey';
                
                // 8) add queues for surveys being sent to a list to delete
                delQueues.add(surveys[0]);
                
                // 9) add surveys to a set to update sent/sent date
                survSentSet.add(surveys[0].Survey__c);
            }
        }
        
        // update contact, delete queues, update survey with send dates
        if (delQueues.size() > 0) {
            update openContacts; 
            Survey_Feedback__c[] sentSurveys = new List<Survey_Feedback__c>();
            for (Survey_Feedback__c sf : [SELECT id, Survey_Sent__c FROM Survey_Feedback__c WHERE id IN :survSentSet]) {
                sf.Survey_Sent__c = true;
                sf.Survey_Sent_Date__c = date.today();
                sentSurveys.add(sf);
            }
            numSent = sentSurveys.size();
            update sentSurveys;
            delete delQueues;
        }
        
        // send the email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        string[] toAddresses = new List<string>();
        toAddresses.add('rwerner@ehs.com');
        toAddresses.add('mmccauley@ehs.com');
        mail.setToAddresses(toAddresses);
        mail.setReplyTo('rwerner@ehs.com');
        mail.setSubject('ETS Survey Scheduler - '+string.valueOf(date.today()));
        mail.setHtmlBody('<b># OF ETS SURVEYS SENT: '+numSent+'</b><br/><br/>testList size = '+testList.size());
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}