@isTest(SeeAllData=true)
private class test_salesCycle {
	
	public static testmethod void RegularTests() {        
        Test.startTest();
        
        runTestOne();          //test 1 - create a reservation
        runTestTwo();          //test 2 - create a reservation not in New Status        
        Test.stopTest();
    }
	
	
 public static void runTestOne() {
    Account Acct = new Account(Name='Account Test', OwnerID='00580000003UChI');
    insert Acct;
    
    // insert contact
    Event[] demos = new Event[]
    {
       new Event(OwnerID= Acct.OwnerID, Event_Status__c = 'Completed', subject='HQ - Demo', whatId = Acct.Id, startDateTime = System.Now(), endDateTime = System.now()),
       new Event(OwnerID= Acct.OwnerID, Event_Status__c = 'Scheduled', subject='GM - Demo', whatId = Acct.Id, startDateTime = System.Now(), endDateTime = System.now())
    };    
    insert demos;    
    
    Opportunity Oppty = new Opportunity(Name='test1',StageName='Pending Approval', Approval_Process__c='TEST', AccountID= Acct.Id, OwnerID='00580000003UChI', CloseDate=System.Today());
    insert Oppty;
    
    Oppty =[SELECT Sales_Cycle_Start__c FROM Opportunity WHERE Id = :Oppty.Id];
    System.assertEquals(system.today(),Oppty.Sales_Cycle_Start__c);
   
} 
public static void runTestTwo() {
    Account Acct = new Account(Name='Account Test', OwnerID='00580000003UChI');
    insert Acct;
    
    // insert contact
    Event[] demos = new Event[]
    {
       new Event(OwnerID= Acct.OwnerID, Event_Status__c = 'Scheduled', subject='HQ - Demo', whatId = Acct.Id, startDateTime = System.Now(), endDateTime = System.now()),
       new Event(OwnerID= Acct.OwnerID, Event_Status__c = 'Scheduled', subject='GM - Demo', whatId = Acct.Id, startDateTime = System.Now(), endDateTime = System.now())
    };    
    insert demos;    
    
    Opportunity Oppty = new Opportunity(Name='test1',StageName='Pending Approval', Approval_Process__c='TEST', AccountID= Acct.Id, OwnerID='00580000003UChI', CloseDate=System.Today());
    insert Oppty;
    
    Oppty =[SELECT Sales_Cycle_Start__c FROM Opportunity WHERE Id = :Oppty.Id];
    System.assertEquals(NULL,Oppty.Sales_Cycle_Start__c);
   
} 


}