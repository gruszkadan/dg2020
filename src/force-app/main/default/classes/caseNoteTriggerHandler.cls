public class caseNoteTriggerHandler {
   
    
    // BEFORE INSERT
    public static void beforeInsert(Case_Notes__c[] caseNotes) {}
    
    // BEFORE UPDATE
    public static void beforeUpdate(Case_Notes__c[] oldCaseNotes, Case_Notes__c[] newCaseNotes) {}
    
    // BEFORE DELETE
    public static void beforeDelete(Case_Notes__c[] oldCaseNotes, Case_Notes__c[] newCaseNotes) {}
    
    // AFTER INSERT
    public static void afterInsert(Case_Notes__c[] oldCaseNotes, Case_Notes__c[] newCaseNotes) {
        Case_Notes__c[] UpgradeCaseNotes = new List<Case_Notes__c>();
        
        for (integer i=0; i<newCaseNotes.size(); i++) {
            if (newCaseNotes[i].Case_Type__c == '9.0 Upgrade Training') {
                UpgradeCaseNotes.add(newCaseNotes[i]);
            }
        }
        
        if(UpgradeCaseNotes.size() > 0){
            VPMNoteUtility.VPMNoteCreation(UpgradeCaseNotes);
        }
    }

    
    // AFTER UPDATE
    public static void afterUpdate(Case_Notes__c[] oldCaseNotes, Case_Notes__c[] newCaseNotes) {
        
        Case_Notes__c[] changedUpgradeCaseNotes = new List<Case_Notes__c>();
        Case_Notes__c[] changedToTrainingCaseNotes = new List<Case_Notes__c>();
        Case_Notes__c[] changedFromTrainingCaseNotes = new List<Case_Notes__c>();
        Case_Notes__c[] trainingCaseNotes = new List<Case_Notes__c>();
        Case_Notes__c[] trainingTypeSwitch = new List<Case_Notes__c>();
        
        for (integer i=0; i<newCaseNotes.size(); i++) {
            if (newCaseNotes[i].Case_Type__c == '9.0 Upgrade Training'){

                if(newCaseNotes[i].Notes_Type__c != oldCaseNotes[i].Notes_Type__c 
                   || newCaseNotes[i].Call_Duration__c != oldCaseNotes[i].Call_Duration__c 
                   || newCaseNotes[i].Issue__c != oldCaseNotes[i].Issue__c) {
                       changedUpgradeCaseNotes.add(newCaseNotes[i]);
                   }
                
                //if training or training webinar - create task with new info and update current project note
                if(newCaseNotes[i].Notes_Type__c != oldCaseNotes[i].Notes_Type__c 
                   && ((newCaseNotes[i].Notes_Type__c == 'Training' && oldCaseNotes[i].Notes_Type__c != 'Webinar - Training')
                       || (newCaseNotes[i].Notes_Type__c == 'Webinar - Training' && oldCaseNotes[i].Notes_Type__c != 'Training')
                   	)
                  ){
                      changedToTrainingCaseNotes.add(newCaseNotes[i]);
                  }
                
                if(newCaseNotes[i].Notes_Type__c != oldCaseNotes[i].Notes_Type__c 
                   && ((oldCaseNotes[i].Notes_Type__c == 'Training' && newCaseNotes[i].Notes_Type__c != 'Webinar - Training')  
                       || (oldCaseNotes[i].Notes_Type__c == 'Webinar - Training' && newCaseNotes[i].Notes_Type__c != 'Training')
                       )
                  ){
                      changedFromTrainingCaseNotes.add(newCaseNotes[i]);
                  }
                
                if((newCaseNotes[i].Notes_Type__c == 'Training' && oldCaseNotes[i].Notes_Type__c  == 'Webinar - Training')
                   || (newCaseNotes[i].Notes_Type__c == 'Webinar - Training' && oldCaseNotes[i].Notes_Type__c  == 'Training')
                   || (newCaseNotes[i].Notes_Type__c == oldCaseNotes[i].Notes_Type__c && 
                       (newCaseNotes[i].Notes_Type__c == 'Training' || newCaseNotes[i].Notes_Type__c == 'Webinar - Training')  &&
                       (newCaseNotes[i].Call_Duration__c != oldCaseNotes[i].Call_Duration__c  || newCaseNotes[i].Issue__c != oldCaseNotes[i].Issue__c)
                      )
                  ){
                      trainingTypeSwitch.add(newCaseNotes[i]);
                  }
            }
        }
        
        if(changedUpgradeCaseNotes.size() > 0){
            VPMNoteUtility.VPMNoteUpdate(changedUpgradeCaseNotes);
        }
        
        if(changedToTrainingCaseNotes.size() > 0){
			VPMTaskUtility.afterUpdateVPMTaskCreation(changedToTrainingCaseNotes);
            
        }
        
        if(changedFromTrainingCaseNotes.size() > 0){
            VPMTaskUtility.afterUpdateVPMTaskRemoval(changedFromTrainingCaseNotes);
        }

       if(trainingTypeSwitch.size() > 0){
            VPMTaskUtility.afterUpdateVPMTaskUpdate(trainingTypeSwitch);
        }
        
    }
    
    // AFTER DELETE
    public static void afterDelete(Case_Notes__c[] caseNotes) {}
  
}