@isTest
private class testActivityNewEditController {
    
    static testmethod void activityController_task_edit(){
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Campaign unknownCampaign = new Campaign(name='Unknown');
        insert unknownCampaign;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = a.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Opportunity_Type__c = 'New';
        insert newOpportunity;  
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        //Insert task
        Task t = new Task(
            subject='Testing',
            whatId = newOpportunity.id,
            whoID = newContact.id,
            ActivityDate = date.today()
        );
        insert t;
        
        PageReference pageRef = Page.task_new_edit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', t.Id);
        activityNewEditController myController = new activityNewEditController();
        myController.changeWhatSelection();
        myController.updateTime();
        myController.save();
    }
    
    static testmethod void activityController_task_new(){
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Campaign unknownCampaign = new Campaign(name='Unknown');
        insert unknownCampaign;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = a.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Opportunity_Type__c = 'New';
        insert newOpportunity;  
        
        PageReference pageRef = Page.task_new_edit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('what_id', a.Id);
        activityNewEditController myController = new activityNewEditController();
        
        myController.changeWhatSelection();
        myController.getWhoSelectList();
        myController.updateTime();
        myController.save();
        myController.task.WhoId = newContact.id;
        myController.findWhoInfo();
        myController.task.Status = 'Scheduled';
        myController.saveNewEvent();
        myController.saveNew();
    }
    
    
    static testmethod void activityController_event_edit(){
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Campaign unknownCampaign = new Campaign(name='Unknown');
        insert unknownCampaign;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = a.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Opportunity_Type__c = 'New';
        newOpportunity.CampaignId = unknownCampaign.id;
        insert newOpportunity;  
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        //Insert event
        Event newEvent = new Event(
            subject='HQ - Demo',
            whatId = a.id,
            whoId = newContact.Id,
            startDateTime = datetime.now(),
            endDateTime = datetime.now().addHours(1),
            event_status__c = 'Scheduled'
        );
        insert newEvent;
        
        PageReference pageRef = Page.event_new_edit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', newEvent.Id);
        activityNewEditController myController = new activityNewEditController();
        myController.getCampaigns();
        myController.getSubjects();
        myCOntroller.opportunitySelection = 'NewOpportunity';
        myController.saveNew();
        myCOntroller.changeWhatSelection();
        myController.changeEndDate();
        myController.newOpportunity.CampaignId = unknownCampaign.id;
        myController.saveNew();
    }
    
    static testmethod void activityController_event_new(){
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        
        Campaign unknownCampaign = new Campaign(name='Unknown');
        insert unknownCampaign;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = a.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Opportunity_Type__c = 'New';
        insert newOpportunity;  
        
        PageReference pageRef = Page.event_new_edit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('what_id', a.Id);
        activityNewEditController myController = new activityNewEditController();
        myController.save();
        myController.theEvent.StartDateTIme = datetime.now();
        myController.changeEndDate();
        myCOntroller.theEvent.WhoId = newContact.Id;
        myController.findWhoInfo();
        myCOntroller.theEvent.Subject = 'HQ - Demo';
        myCOntroller.theEvent.StartDateTime = datetime.now();
        myController.save();
        
        
    }
    
    static testmethod void testEventNewEditController() {
        ApexPages.currentPage().getParameters().put('retURL','test');
        ApexPages.currentPage().getParameters().put('whatid','test');
        ApexPages.currentPage().getParameters().put('id','test');
        ApexPages.currentPage().getParameters().put('who_id','test');
        ApexPages.StandardController testController = new ApexPages.StandardController(new Event());
        eventNewEditController myController = new eventNewEditController(testController);
        myController.redirect();
    }
    
    static testmethod void testTaskNewEditController() {
        ApexPages.currentPage().getParameters().put('retURL','test');
        ApexPages.currentPage().getParameters().put('whatid','test');
        ApexPages.currentPage().getParameters().put('id','test');
        ApexPages.currentPage().getParameters().put('who_id','test');
        ApexPages.StandardController testController = new ApexPages.StandardController(new Task());
        taskNewEditController myController = new taskNewEditController(testController);
        myController.redirect();
    }
    
    
}