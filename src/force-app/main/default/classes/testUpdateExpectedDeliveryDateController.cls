@isTest(seealldata = true)
public class testUpdateExpectedDeliveryDateController {
    static testMethod void test1(){
        
        //query for products and price books 
        product2 p1;
        product2 p2;
        PricebookEntry pb1;
        PricebookEntry pb2;
        
        p1 = [Select id, name from product2 where name = 'HQ Account' limit 1];
        p1.Needs_Expected_Delivery_Date__c = true;
        update p1;
        
        p2 = [select id, name from product2 where name = 'eBinder Valet' limit 1];
        p2.Needs_Expected_Delivery_Date__c = true;
        update p2;
        
        pb1 = [select id, name, pricebook2.id from PricebookEntry where pricebook2.name = 'Standard Price Book' AND product2.id =: p1.id];
        update pb1;
        
        pb2 = [select id, name, pricebook2.id from PricebookEntry where pricebook2.name = 'Standard Price Book' AND product2.id =: p2.id];
        update pb2;
        
        //Create all data for test
        //1 Account + 1 Contact + 1 Opportunity + 2 Opportuinty Line Item + Quote + 2 Quote Product   
        
        
        Account a = new account();
        a.Name = 'Maciejs Account';
        
        insert a;
        
        Contact c = new contact();
        c.FirstName = 'Justin';
        c.LastName = 'Thomas';
        c.AccountId = a.id;    
        c.Communication_Channel__c = 'Cold Call';
        
        insert c;
        
        
        Opportunity o = new opportunity();
        o.Name = 'Update Expected Delivery Date';
        o.AccountId = a.Id;
        o.StageName = 'Decision Makers';
        o.CloseDate = date.today()+1;
        o.Pricebook2Id = pb1.pricebook2.id;
        
        insert o;
        
        Quote__c q = new quote__c();
        q.Account__c = a.Id;
        q.Contact__c = c.Id;
        q.Contract_Length__c = '1 Year';
        q.CurrencyIsoCode = 'USD';
        
        insert q;
        
        Quote_Product__c qp1 = new Quote_Product__c();
        qp1.Product__c = p1.Id; 
        qp1.Quote__c = q.Id;
        
        insert qp1;
        
        Quote_Product__c qp2 = new Quote_Product__c();
        qp2.Product__c = p2.Id;    
        qp2.Quote__c = q.id;
        
        insert qp2;
        
        OpportunityLineItem oli1 = new OpportunityLineItem();
        oli1.OpportunityId = o.Id;
        oli1.Product2Id = p1.id;
        oli1.Quantity = 25;
        oli1.List_Price__c = 100;
        oli1.PricebookEntryId = pb1.id;
        oli1.TotalPrice = 0;
        oli1.qpID__c = qp1.id;
        
        insert oli1;
        
        OpportunityLineItem oli2 = new OpportunityLineItem();
        oli2.OpportunityId = o.Id;
        oli2.Quantity = 25;
        oli2.Product2Id = p2.id;
        oli2.List_Price__c = 200;
        oli2.PricebookEntryId = pb2.id;
        oli2.TotalPrice = 0;
        oli2.qpID__c = qp2.id;
        
        insert oli2;
        
        
        // Pass in parameter that are used in your page
        ApexPages.currentPage().getParameters().put('Id', o.Id);
        // Pass in controller used in page 
        updateExpectedDeliveryDateController mc = new updateExpectedDeliveryDateController(); 
        
        // loop throough OLI and set fields before saving 
        for(OpportunityLineItem x : mc.OLI){
            
            x.Expected_Delivery_Date__c = date.today() +1;           
        }
        
        
        // Run the save method 
        mc.Save();
        
        // Check to see if the save method set the correct fields on both OLI
        system.assertEquals(oli1.Expected_Delivery_Date__c, qp1.Expected_Delivery_Date__c);
        system.assertEquals(oli2.Expected_Delivery_Date__c, qp2.Expected_Delivery_Date__c);
        
        //Query records again to see if value were posted to data base 
        oli1 = [Select id, name, Expected_Delivery_Date__c from opportunitylineitem where id =: oli1.id];
        QP1 = [Select id, name, Expected_Delivery_Date__c from quote_product__c where id =: qp1.Id];
        
        //Check to see if values are correct 
        system.assertEquals(oli1.Expected_Delivery_Date__c, qp1.Expected_Delivery_Date__c);
        
        //Query the other OLI + QP to see if those values poasted correctley 
        oli2 = [Select id, name, Expected_Delivery_Date__c from opportunitylineitem where id =: oli2.id];
        QP2 = [Select id, name, Expected_Delivery_Date__c from quote_product__c where id =: qp2.id];
        
        //Check the values 
        system.assertEquals(oli2.Expected_Delivery_Date__c, qp2.Expected_Delivery_Date__c);
        
    }
}