trigger renewalClosedOpptyCount on Opportunity (after delete, after insert, after undelete, 
after update) {
// Declare the variables
public set<Id> RenewalIDs = new Set<Id>();
public list<Renewal__c> RenewalToUpdate = new List<Renewal__c>();

// Find Renewal to Update
if(Trigger.isInsert || Trigger.isUnDelete || Trigger.isUpdate){
    for(Opportunity  o: Trigger.new){
        if(o.Renewal__c <> NULL) {
            RenewalIDs.add(o.Renewal__c);
        }
    }
}

if(Trigger.isDelete || Trigger.isUpdate){
    for(Opportunity  o: Trigger.old){
        if(o.Renewal__c <> NULL) {
             RenewalIDs.add(o.Renewal__c);
            }
        }
    }

// Update the Renewal

if(RenewalIDs.size()>0){
for(Renewal__c r: [Select r.Id, r.Num_of_Closed_Opportunities__c,
(Select Id From Opportunities__r where IsClosed = TRUE)
From Renewal__c r where Id in :RenewalIDs])
RenewalToUpdate.add(new Renewal__c(Id=r.Id, Num_of_Closed_Opportunities__c = r.Opportunities__r.size()));
update RenewalToUpdate;
}

}