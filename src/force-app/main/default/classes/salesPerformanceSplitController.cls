//Test Class: testSalesPerformanceRequestIncentives
public with sharing class salesPerformanceSplitController {
    public id userHeaderId {get;set;}
    public String viewAs {get;set;}
    public User u {get;set;}
    public String defaultView {get;set;}
    public integer stepNumber {get;set;}
    // id passed in if split is selected off the order detail page
    public String xSpoId {get;set;}
    //Step 1
    public String orderSearchString {get;set;}
    public id selOrderId {get;set;}
    public Sales_Performance_Order__c selectedOrder {get;set;}
    public Sales_Performance_Order__c[] selectedOrderSplitRecords {get;set;}
    public integer numExistingSplits {get;set;}
    public Sales_Performance_Order__c[] spos {get;set;}
    public splitWrapper[] splitWrappers {get;set;}
    public productWrapper[] productWrappers {get;set;}
    public Sales_Performance_Order__c spoDetail {get;set;}
    //Step 2
    public integer numOfSplits {get;set;}
    public Map<String,User> salesUsersMap {get;set;}
    public boolean step2Error {get;set;}
    //Step 3
    public boolean step3Error {get;set;}
    //Step 4
    AT_Product_Mapping__c[] productMapping;
    Map<String,Decimal> productRates;
    //Step 5
    public Sales_Performance_Request__c spr {get;set;}
    public boolean splitsSubmitted {get;set;}
    public string errorString {get;set;}
    public string splitReason {get;set;}
    public id existingSprId {get;set;}
    public set<String> approverNames {get;set;}
    
    public salesPerformanceSplitController() {
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias, FirstName, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID FROM User WHERE id = :viewAs LIMIT 1];
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
        userHeaderId = u.id;
        salesUsersMap = new Map<String,User>();
        numExistingSplits = 0;
        xSpoId = ApexPages.currentPage().getParameters().get('spoID');
        if(xSpoId == null || xSpoId == ''){
            stepNumber = 1;
        }else{
            findSpoFromParam();
        }
        //Query product mapping which will be used for the incentive rates
        productMapping = [SELECT id,Salesforce_Product_Name__c,Single_Year_Incentive_Rate__c,
                          Multi_Year_Incentive_Rate__c,Admin_Tool_Product_Name__c,
                          Product_Platform__c,Category__c
                          FROM AT_Product_Mapping__c];
        productRates = new Map<String,Decimal>();
        for(AT_Product_Mapping__c pm:productMapping){
            String key = pm.Admin_Tool_Product_Name__c+'-'+pm.Product_Platform__c+'-'+pm.Salesforce_Product_Name__c+'-'+pm.Category__c;
            productRates.put(key+'-Single', pm.Single_Year_Incentive_Rate__c);
            productRates.put(key+'-Multi', pm.Multi_Year_Incentive_Rate__c);
        }
        spr = new Sales_Performance_Request__c();
    }
    
    // --- STEP 1 - ORDER SELECTION ---
    
    //Search query for sales performance orders in step 1
    public void searchOrders(){
        selOrderId = null;
        string query = 'SELECT id, Order_ID__c, Owner.Name, OwnerId, Contract_Number__c, ' +
            'Account__r.Name, Order_Date__c, Account__r.AdminId__c, Contract_Length__c, Month__c, Year__c, Account__c, isSplit__c, isSplitMaster__c, Booking_Amount__c, ' +
            '(SELECT id, OwnerId, Original_Booking_Amount__c, Product_Name__c, Name, Booking_Amount__c, Order_Item__c, Order_Item_ID__c, Order_ID__c, '+
            'Order_Item__r.Admin_Tool_Product_Name__c, Order_Item__r.Product_Platform__c, Order_Item__r.Salesforce_Product_Name__c, Month__c, Order_Date__c, Year__c, '+
            'Order_Item__r.Category__c, Contract_Length__c, Account__c, Invoice_Amount__c, Contract__c, Contract_Number__c, Sales_Rep_Sales_Performance_Summary__c, '+
            'Sales_Manager__c, Sales_Director__c, Sales_VP__c, Group__c, Role__c, Total_Incentive_Amount__c FROM Sales_Performance_Items__r), ' +
            '(SELECT id, OwnerId, Owner.Name, Name, SPI_Split_From__c, SPO_Split_From__c, SPI_Split_To__c, SPO_Split_To__c, Booking_Amount__c, '+
            'Incentive_Amount__c, Product_Name__c, Split_Group__c, Sales_Performance_Request__c FROM Sales_Performance_Item_Splits__r ORDER BY Split_Group__c ASC) ' +
            'FROM Sales_Performance_Order__c ' +
            'WHERE (Account__r.Name LIKE \'%' + String.escapeSingleQuotes(orderSearchString) + '%\' ' +
            'OR Contract_Number__c LIKE \'%' + String.escapeSingleQuotes(orderSearchString) + '%\') '+
            'AND ((isSplit__c = TRUE AND isSplitMaster__c = TRUE) OR isSplit__c = FALSE) '+
            'AND Num_Pending_Sales_Performance_Requests__c = 0 '+
            'AND Type__c = \'Booking\' '+
            'ORDER BY Order_Date__c DESC NULLS LAST LIMIT 5';
        spos = database.query(query);
    }
    
    //Finds and sets selectedOrder based on the id passed in from the html parameter
    public void findSpoFromParam(){
        selOrderId = null;
        selectedOrder = [SELECT id, Order_ID__c, Owner.Name, OwnerId, Contract_Number__c,
                         Account__r.Name, Order_Date__c, Account__r.AdminId__c, Contract_Length__c, Month__c, Year__c, Account__c, isSplit__c, isSplitMaster__c, Booking_Amount__c,
                         (SELECT id, OwnerId, Original_Booking_Amount__c, Product_Name__c, Name, Booking_Amount__c, Order_Item__c, Order_Item_ID__c, Order_ID__c,
                          Order_Item__r.Admin_Tool_Product_Name__c, Order_Item__r.Product_Platform__c, Order_Item__r.Salesforce_Product_Name__c, Month__c, Order_Date__c, Year__c,
                          Order_Item__r.Category__c, Contract_Length__c, Account__c, Invoice_Amount__c, Contract__c, Contract_Number__c, Sales_Rep_Sales_Performance_Summary__c, 
                          Sales_Manager__c, Sales_Director__c, Sales_VP__c, Group__c, Role__c, Total_Incentive_Amount__c FROM Sales_Performance_Items__r),
                         (SELECT id, OwnerId, Owner.Name, Name, SPI_Split_From__c, SPO_Split_From__c, SPI_Split_To__c, SPO_Split_To__c, Booking_Amount__c, 
                          Incentive_Amount__c, Product_Name__c, Split_Group__c, Sales_Performance_Request__c FROM Sales_Performance_Item_Splits__r ORDER BY Split_Group__c ASC)
                         FROM Sales_Performance_Order__c
                         WHERE id =:xSpoId
                         AND Num_Pending_Sales_Performance_Requests__c = 0
                         AND Type__c = 'Booking'
                         LIMIT 1];
        if(selectedOrder != null){
            if(selectedOrder.Sales_Performance_Item_Splits__r.size() > 0){
                //set the number of existing splits and number of splits to the highest split group on the order
                numExistingSplits = integer.valueof(selectedOrder.Sales_Performance_Item_Splits__r[selectedOrder.Sales_Performance_Item_Splits__r.size() - 1].Split_Group__c);
                existingSprId = selectedOrder.Sales_Performance_Item_Splits__r[0].Sales_Performance_Request__c;
                numOfSplits = numExistingSplits;
            }
            goToStep2();
        }
    }
    
    //Method that sets the selected sales performance order to be split once chosen by the user
    public void selectOrder(){
        if(selOrderId != null){
            numExistingSplits = 0;
            if((selectedOrder != null && selOrderId != selectedOrder.id) || selectedOrder == null){
                clearStep2();
                for(Sales_Performance_Order__c spo:spos){
                    if(spo.id == selOrderId){
                        selectedOrder = spo;
                        break;
                    }
                }
            }
            
            //If the order is successfully set then check for existing splits and set step number to 2 to move on to next tab 
            if(selectedOrder != null){
                if(selectedOrder.Sales_Performance_Item_Splits__r.size() > 0){
                    //set the number of existing splits and number of splits to the highest split group on the order
                    numExistingSplits = integer.valueof(selectedOrder.Sales_Performance_Item_Splits__r[selectedOrder.Sales_Performance_Item_Splits__r.size() - 1].Split_Group__c);
                    existingSprId = selectedOrder.Sales_Performance_Item_Splits__r[0].Sales_Performance_Request__c;
                    numOfSplits = numExistingSplits;    
                }
                goToStep2();
            }
        }
    }
    
    public void viewOrderDetails(){
        id spoId = ApexPages.currentPage().getParameters().get('spoId');
        if(spoId != null){
            for(Sales_Performance_Order__c spo:spos){
                if(spo.id == spoId){
                    spoDetail = spo;
                    break;
                }
            }
        }
    }
    
    public void goToStep2(){
        stepNumber = 2;
        if(numExistingSplits > 0){
            createSplits();    
        }
    }
    
    // --- STEP 2 - SPLIT OWNERSHIP ---
    
    //Query the list of sales users for the split owner options
    public String[] getSalesUsersDropdown() {
        String[] options = new list<String>();
        for (User usr : [Select Id, Name, Sales_Performance_Manager_ID__c, Sales_Manager__c, Sales_Director__c, Sales_VP__c, Group__c, Role__c
                         From User where isActive = TRUE and Department__c = 'Sales' and 
                         Group__c !='EHS Enterprise Sales' and Group__c !='EHS Mid-Market Sales'  
                         ORDER BY Name ASC]) {
                             options.add(usr.Name);
                             salesUsersMap.put(usr.Name,usr);
                         }
        return options;
    }
    
    //Check for existing splits on the selected order and create new/additional splits
    public void createSplits(){
        step2Error = false;
        splitWrappers = new list <splitWrapper>();
        productWrappers = new list<productWrapper>();
        set<id> existingSplitOwnerIds = new set<id>();
        //create product wrappers to use for grouping later 
        for(Sales_Performance_Item__c spi:selectedOrder.Sales_Performance_Items__r){
            productWrappers.add(new productWrapper(spi));
        }
        //get all the owner id's of any existing splits on this order
        for(Sales_Performance_Item_Split__c spis:selectedOrder.Sales_Performance_Item_Splits__r){
            existingSplitOwnerIds.add(spis.OwnerId);
        }
        //if the order is already split then loop over the existing split and create split wrappers for each owner
        if(selectedOrder.Sales_Performance_Item_Splits__r.size() > 0){
            for(id owner:existingSplitOwnerIds){
                boolean master = false;
                Sales_Performance_Item_Split__c[] ownerSplits = new list<Sales_Performance_Item_Split__c>();
                String ownerName;
                for(Sales_Performance_Item_Split__c spis:selectedOrder.Sales_Performance_Item_Splits__r){
                    if(spis.OwnerId == owner){
                        ownerName = spis.Owner.Name;
                        ownerSplits.add(spis);
                        if(spis.OwnerId == selectedOrder.OwnerId){
                            master = true;
                        }
                    }
                }
                splitWrappers.add(new splitWrapper(ownerName, ownerSplits, master));
            }
        }
        //if numOfSplits is selected in dropdown create any additional new splits required
        if(numOfSplits != null && numOfSplits > 0){
            for (integer i=1+numExistingSplits; i <= numOfSplits; i++) {
                Sales_Performance_Item_Split__c[] ownerSplits = new list<Sales_Performance_Item_Split__c>();
                for(Sales_Performance_Item__c spi:selectedOrder.Sales_Performance_Items__r){
                    Sales_Performance_Item_Split__c newSplit = new Sales_Performance_Item_Split__c();
                    newSplit.Split_Group__c = i;
                    newSplit.SPI_Split_From__c = spi.id;
                    newSplit.SPO_Split_From__c = selectedOrder.id;
                    newSplit.Status__c = 'Open';
                    newSplit.Booking_Amount__c = 0;
                    newSplit.Product_Name__c = spi.Product_Name__c;
                    if(i == 1){
                        newSplit.OwnerId = spi.OwnerId;
                        newSplit.SPI_Split_To__c = spi.id;
                        newSplit.SPO_Split_To__c = selectedOrder.id;
                    }
                    ownerSplits.add(newSplit);
                }
                if(ownerSplits[0].OwnerId != null){
                    splitWrappers.add(new splitWrapper(selectedOrder.Owner.Name, ownerSplits, true));
                }else{
                    splitWrappers.add(new splitWrapper(null, ownerSplits, false));
                }
            }
        }
    }
    
    public void assignSplitOwnership(){
        step2Error = false;
        for(splitWrapper wrap:splitWrappers){
            wrap.splitOwner = salesUsersMap.get(wrap.splitOwnerName);
            if(wrap.splitOwner != null){
                for(Sales_Performance_Item_Split__c spis:wrap.splits){
                    spis.OwnerId = wrap.splitOwner.id;
                }
            }else{
                step2Error = true;
            }
        }
        if(!step2Error){
            calculateRemainingBookings();
            stepNumber = 3;
        }
    }
    
    // --- STEP 3 - BOOKINGS ASSIGNMENT ---
    
    //Calculates the remaining bookings amount to be split for each item
    public void calculateRemainingBookings(){
        for(productWrapper prodWrap: productWrappers){
            prodWrap.remainingBookings = prodWrap.item.Original_Booking_Amount__c;
            for(splitWrapper splitWrap: splitWrappers){
                for(Sales_Performance_Item_Split__c split:splitWrap.splits){
                    if(split.SPI_Split_From__c == prodWrap.item.id){
                        if(split.Booking_Amount__c == null){
                            split.Booking_Amount__c = 0.00;
                        }
                        prodWrap.remainingBookings =  prodWrap.remainingBookings - split.Booking_Amount__c;
                    }
                }
            }
        }
    }
        
    //Check that bookings amounts have been correctly distributed and go to step 4 
    public void checkBookingsAmounts(){
        step3Error = false;
        for(productWrapper prodWrap: productWrappers){
            if(prodWrap.remainingBookings != 0){
                step3Error = true;  
            }
        }
        //Find and set standard Incentive Rate
        for(Sales_Performance_Item__c spi:selectedOrder.Sales_Performance_Items__r){
            Decimal incentiveRate;
            if(spi.Order_Item__c != null){
                string key = spi.Order_Item__r.Admin_Tool_Product_Name__c+'-'+spi.Order_Item__r.Product_Platform__c+'-'+spi.Order_Item__r.Salesforce_Product_Name__c+'-'+spi.Order_Item__r.Category__c;
                if(spi.Contract_Length__c == null || spi.Contract_Length__c == 1 || spi.Contract_Length__c == 0){
                    key+='-Single';
                }else if(spi.Contract_Length__c > 1){
                    key+='-Multi';
                }
                incentiveRate = productRates.get(key);
            }
            for(splitWrapper splitWrap: splitWrappers){
                for(Sales_Performance_Item_Split__c split:splitWrap.splits){
                    if(split.Booking_Amount__c == null){
                        split.Booking_Amount__c = 0.00;
                    }
                    if(split.SPI_Split_From__c == spi.id && incentiveRate != null){
                        split.Incentive_Amount__c = split.Booking_Amount__c*(incentiveRate/100);
                    }
                }
            }
        }
        if(!step3Error){
            stepNumber = 4;
        }
    }
    
    // --- STEP 4 - Incentive ASSIGNMENT ---
    
    //Takes you to the Summary Page
    public void goToSummary(){
        for(splitWrapper splitWrap: splitWrappers){
            for(Sales_Performance_Item_Split__c split:splitWrap.splits){
                if(split.Incentive_Amount__c == null){
                    split.Incentive_Amount__c = 0.00;
                }
            }
        }
        stepNumber = 5;        
    }
    
    
    // --- STEP 5 - Summary/Approval/Creation ---
    public void submitSplits(){
        //Clear out any existing errors
        errorString = null;
        
        //Check to make sure a reason in entered for the split
        if(splitReason == null || splitReason == ''){
            errorstring = 'Missing Required Field: Reason for Split';
        }
        
        //If there is an exisitng SPR then query it and set it as the sprToDelete
        Sales_Performance_Request__c sprToDelete;
        if(existingSprId != null){
            sprToDelete = [SELECT id FROM Sales_Performance_Request__c WHERE id =: existingSprId LIMIT 1];
        }
        
        //Find SPS records for the split owners from the time of original order
        set<id> splitOwnerIds = new set<id>();
        for(splitWrapper s: splitWrappers){
            splitOwnerIds.add(s.SplitOwner.id);
        }
        Sales_Performance_Summary__c[] splitOwnerSPSs = [SELECT id,Sales_Rep__c,Key__c,Manager__c,Manager__r.isActive,Sales_Director__c,
                                                         Sales_Director__r.isActive, Sales_VP__c,Group__c,Role__c, Manager__r.Name, Sales_Director__r.Name
                                                         FROM Sales_Performance_Summary__c 
                                                         WHERE Sales_Rep__c in:splitOwnerIds 
                                                         and Year__c =:selectedOrder.Year__c
                                                         and Month__c =:selectedOrder.Month__c];
        //Loop through the SPS's and set the approver ids based on the MGR and Director at the time of the order 
        set<id> approverIdSet = new set<id>();
        id[] approverIds = new list<id>();
        //if SPS's were found
        if(splitOwnerSPSs.size() > 0){
             approverNames = new set<String>();
            //Add manager id's to approver list first (if still active and not the manager submitting)
            for(Sales_Performance_Summary__c sps:splitOwnerSPSs){
                if(sps.Manager__r.isActive && sps.Manager__c != u.id){
                    approverIdSet.add(sps.Manager__c);
                    approverNames.add(sps.Manager__r.Name);
                }
            }
            //Once managers added then add director id's to approver list (if still active and not the director submitting)
            for(Sales_Performance_Summary__c sps:splitOwnerSPSs){
                if(sps.Sales_Director__r.isActive && sps.Sales_Director__c != u.id){
                    approverIdSet.add(sps.Sales_Director__c);
                    approverNames.add(sps.Sales_Director__r.Name);
                }
            }
            approverIds.addAll(approverIdSet);
        }
        
        //Create a new SPR record for the approval
        spr = new Sales_Performance_Request__c(OwnerID = u.id, Type__c = 'Split',Reason__c = splitReason,Status__c = 'Pending Approval',Sales_Performance_Order__c = selectedOrder.id);
        
        //Find the SPS to associate the SPR to
        spr.Sales_Performance_Summary__c = salesPerformanceRequestUtility.findSprSps(selectedOrder, null);
        
        //Set the new SPR records remaining fields and insert
        if(errorString == null || errorString == ''){
            if(sprToDelete != null){
                spr.isEditedRequest__c = true;
            }
            if(approverIds.size() > 0){
                spr.Assigned_Approver_1__c = approverIds[0];
                if(approverIds.size() > 1){
                    spr.Assigned_Approver_2__c = approverIds[1];
                }
                if(approverIds.size() > 2){
                    spr.Assigned_Approver_3__c = approverIds[2];
                }
                if(approverIds.size() > 3){
                    spr.Assigned_Approver_4__c = approverIds[3];
                }
                if(approverIds.size() > 4){
                    spr.Assigned_Approver_5__c = approverIds[4];
                }
                if(approverIds.size() > 5){
                    spr.Assigned_Approver_6__c = approverIds[5];
                }
                if(approverIds.size() > 6){
                    spr.Assigned_Approver_7__c = approverIds[6];
                }
                if(approverIds.size() > 7){
                    spr.Assigned_Approver_8__c = approverIds[7];
                }
                if(approverIds.size() > 8){
                    spr.Assigned_Approver_9__c = approverIds[8];
                }
                if(approverIds.size() > 9){
                    spr.Assigned_Approver_10__c = approverIds[9];
                }
            }
            try{
                insert spr;
            }catch(exception e){
                errorString = string.valueof(e);
            }
        }
        
        //Loop over the split wrappers and associate each SPIS record to the newly created SPR and add to a list. Then upsert the SPIS's
        if(errorString == null || errorString == ''){
            Sales_Performance_Item_Split__c[] splits = new list<Sales_Performance_Item_Split__c>();
            for(splitWrapper splitWrap: splitWrappers){
                for(Sales_Performance_Item_Split__c split:splitWrap.splits){
                    split.Sales_Performance_Request__c = spr.id;
                    split.Status__c = 'Pending Approval';
                    splits.add(split);
                }
            }
            try{
                upsert splits;  
            }catch(exception e){
                errorString = string.valueof(e);
                delete spr;
            }
        }
        
        //If this is a director then set the SPR as 'Approved' and update
        if(errorString == null || errorString == ''){
            if(defaultView == 'Director' || defaultView == 'VP'){
                spr.Status__c = 'Approved';
            }else{
                spr.Requires_Approval__c = true;
            }
            try{
                update spr;
            }catch(exception e){
                errorString = string.valueof(e);
            }
        }
        
        //If no errors then check the splitsSubmitted boolean
        if(errorString == null || errorString == ''){
            if(sprToDelete != null){
                delete sprToDelete;
            }
            splitsSubmitted = true;
        }
        
    } 
    
    
    // --- NAVIGATION FUNCTIONS ---
    
    //Change tabs
    public void changeTabs(){
        stepNumber = integer.valueOf(ApexPages.currentPage().getParameters().get('step'));
    }
    
    //Clear out data from step 1
    public void clearStep1(){
        selOrderId = null;
        selectedOrder = null;
        orderSearchString = null;
        spos = null;
    }
    
    //Clear out data from step 2
    public void clearStep2(){
        numOfSplits = null;
        splitWrappers = new list <splitWrapper>();
    }
    
    // --- WRAPPER CLASSES ---
    
    //Split Wrapper
    public class splitWrapper{
        public boolean isMaster {get;set;}
        public String splitOwnerName {get;set;}
        public User splitOwner {get;set;}
        public Sales_Performance_Item_Split__c[] splits {get;set;}
        public splitWrapper(String xSplitOwnerName, Sales_Performance_Item_Split__c[] xSplits, Boolean xIsMaster){
            splitOwnerName = xSplitOwnerName;
            splits = xSplits;
            isMaster = xIsMaster;
        }
    }
    
    //Product Wrapper
    public class productWrapper{
        public Sales_Performance_Item__c item {get;set;}
        public decimal remainingBookings {get;set;} 
        public productWrapper(Sales_Performance_Item__c xItem){
            item = xItem;
            remainingBookings = xItem.Original_Booking_Amount__c;
        }
    }
}