@isTest(SeeAllData = true)
private class testQuoteProposalNeedNew {
    
    static testMethod void test1(){
        
        
        PageReference pageRef = new PageReference('apex/quote_proposal_need_new_user'); 
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote_Proposal_Need__c());
        quoteProposalNeedNew myController = new quoteProposalNeedNew(testController);
        
        system.assertEquals(myController.productWrappers[0].productStatus, 'Available');
        
        //test that move to selected picklist method working
        myController.productWrappers[0].action = true;
        myController.moveToSelected();
        system.assertEquals(myController.productWrappers[0].action, false);
        system.assertEquals(myController.productWrappers[0].productStatus, 'Selected');
        
        //test that move to available picklist method working
        myController.productWrappers[0].action = true;
        myController.moveToAvailable();
        system.assertEquals(myController.productWrappers[0].action, false);
        system.assertEquals(myController.productWrappers[0].productStatus, 'Available');
        
        //test AddWrapper function works
        integer listSize =  myController.descriptionWrappers.size();
        myController.addWrapper();
        system.assertEquals(myController.descriptionWrappers.size(), listSize + 1);
        
        //test bullet is removed and list is reordered
        integer descrSize = myController.descriptionWrappers.size();
        ApexPages.currentPage().getParameters().put('bullet', '1');
        myController.removeBullet();
        system.assertEquals(myController.descriptionWrappers.size(), descrSize - 1);
        system.assertEquals(myController.descriptionWrappers[1].order, 1);
        
        myController.productFamily = 'Services - Core';
        myController.queryProducts();
        myController.getFamilyOptions();
        myController.createNeed();
        
        myController = new quoteProposalNeedNew(testController);
        
        myController.productWrappers[1].productStatus = 'Selected';   
        myController.need.Need__c = 'Hey';
        myController.need.Need_Heading__c = 'Test Heading';
        myController.descriptionWrappers[0].bullet = 'Hello';
        
        myController.pageName = 'quote_proposal_need_new_user'; 
        myController.createNeed();
        myController.needsManagementHome();        
        //test that fields for quote proposal need filled out correctly
        system.assertEquals(myController.need.Description__c, 'Hello');
        system.assertEquals(myController.need.Product__c, myController.productWrappers[1].prod.name);
        system.assertEquals(myController.need.Product_Family__c, myController.productWrappers[1].prod.family);
        system.assertEquals(myController.need.Available_For_Use__c, true);
        system.assertEquals(myController.need.Global__c, false);
        system.assertEquals(myController.pageName, 'quote_proposal_need_new_user');
        
    }
    
    
    
}