@isTest (SeeAllData=true)
private class testKnowledgeArticleSetAssignedTo {    
    //Test method
    
    static testmethod void test(){
        string[] innerList;
        User u =[Select ID from User where LastName='McCauley' and isActive = TRUE limit 1];
        User u2 = [Select ID from User where LastName='Vandenberg' and isActive = TRUE limit 1]; 

    	//Create Knowledge Article Version
        FAQ__kav faqKAV1 = new FAQ__kav (Title='Test Article Version', UrlName = 'Test1'); 
        insert faqKAV1;
        faqKAV1 = [SELECT ArticleNumber FROM FAQ__kav WHERE Id = :faqKAV1.Id];    
        
        //Get the associated Knowledge Article 
        FAQ__ka faqKA = [Select Id FROM FAQ__ka WHERE ArticleNumber = :faqKAV1.ArticleNumber];
        
        //Assign this Article to Mark
        KbManagement.PublishingService.assignDraftArticleTask(faqKA.id, u.ID, null, null, false);
        
        //Create variable for method 
        innerList = new list<String> ();
        innerList.add(faqKA.id);  //faqKA.id is equivalent to faqKAV.KnowledgeArticleId;
        innerList.add(u2.Id);
          
        List<List<String>> testparm;
        testparm = new List<List<String>>();
        testparm.add(innerList);
        
        //Use method to assign the article to Tara and check that the change has occurred. 
        knowledgeArticleSetAssignedTo.setAssignedTo(testparm);
        FAQ__kav kav = [SELECT AssignedToId FROM FAQ__kav WHERE Id = :faqKAV1.Id];
        
        System.assertEquals(kav.AssignedToId, u2.Id);
        
    }
    
}