@isTest(seeAllData=true)
private class testquoteApproval {

        static testmethod void test1() {
        User[] users = new list<User>();
        //User user1 = [SELECT id, Alias, Group__c FROM User WHERE isActive = true AND Department__c = 'Customer Care' AND Group__c = 'Customer Care' LIMIT 1];
        User user2 = [SELECT id, Alias, Group__c, Product_Group__c FROM User WHERE isActive = true AND Department__c = 'Sales' AND Group__c = 'Mid-Market' AND Role__c = 'Associate' LIMIT 1];
        //User user3 = [SELECT id, Alias, Group__c FROM User WHERE isActive = true AND Department__c = 'Sales' AND Group__c = 'Mid-Market' AND Role__c = 'Sales Executive' LIMIT 1];
        //User user4 = [SELECT id, Alias, Group__c FROM User WHERE isActive = true AND Department__c = 'Sales' AND Group__c = 'Mid-Market' AND Role__c = 'Territory Sales Executive' LIMIT 1];
        //User user5 = [SELECT id, Alias, Group__c FROM User WHERE isActive = true AND Department__c = 'Sales' AND Group__c = 'Mid-Market' AND Role__c = 'Territory Account Manager' LIMIT 1];
        User user6 = [SELECT id, Alias, Group__c, Product_Group__c FROM User WHERE isActive = true AND Department__c = 'Customer Care' AND Group__c = 'Retention' LIMIT 1];
        //users.add(user1); 
        user2.Product_Group__c = null;
        users.add(user2); 
        //users.add(user3); 
        //users.add(user4); 
        //users.add(user5); 
        user6.Product_Group__c = null;
        users.add(user6);
        for (User u : users) {
            System.runAs(u) {
                runTest(u); 
            }
        }
    }
    
    static void runTest(User u) {
        //set up the quote
        Account a = new Account(Name='Test Account', Active_MSDS_Management_Products__c='GM');
        insert a;
        
        Contact c = new Contact(FirstName='Firstname', LastName='Lastname', AccountId=a.id);
        insert c;
        
        Quote_Promotion__c p = new Quote_Promotion__c(Name='Promo', Start_Date__c=date.today(), End_Date__c=date.today().addDays(90), Code__c=u.Alias+'_Promo');
        insert p;
        
        Quote_Promotion_Product__c qpp = new Quote_Promotion_Product__c(Quote_Promotion__c=p.id, Name__c='HQ Account', Product__c='01t80000002NON4', Discount__c=1, Discount_Method__c='Amount Off', 
                                                                        Year_1__c=true, Year_2__c=true, Year_3__c=true, Year_4__c=true, Year_5__c=true);
        insert qpp;
        
        Quote__c q = new Quote__c(Account__c=a.id, Contact__c=c.id, Promotion__c=p.id, Contract_Length__c='5 Years', Price_Book__c='01s300000001EAF');
        insert q;
        
        Quote_Product__c[] qps = new list<Quote_Product__c>();
        for (integer i=0; i<10; i++) {
            Quote_Product__c qp = new Quote_Product__c();
            qp.Quote__c = q.id;
            qp.Quantity__c = 1;
            qp.Y1_Quote_Price__c = 1000; qp.Y1_List_Price__c = 1500; qp.Year_1__c = true; 
            qp.Y2_Quote_Price__c = 1000; qp.Y2_List_Price__c = 1500; qp.Year_2__c = true;
            qp.Y3_Quote_Price__c = 1000; qp.Y3_List_Price__c = 1500; qp.Year_3__c = true;
            qp.Y4_Quote_Price__c = 1000; qp.Y4_List_Price__c = 1500; qp.Year_4__c = true;
            qp.Y5_Quote_Price__c = 1000; qp.Y5_List_Price__c = 1500; qp.Year_5__c = true; 
            if (i==0) {	
                qp.Name__c = 'HQ Account'; qp.Product__c = '01t80000002NON4'; qp.PricebookEntryId__c = '01u80000006eap9';
                qp.Promo_Discount_Percent__c = 1; qp.Promo_Y1__c = true; qp.Promo_Y2__c = true; qp.Promo_Y3__c = true; qp.Promo_Y4__c = true; qp.Promo_Y5__c = true;
                qp.Y1_Quote_Price__c = 10;
                qp.Type__c = 'Renewal';
                qp.Renewal_Amount__c = 1;
            }
            if (i==1) {
                qp.Name__c = 'eBinder Valet'; qp.Product__c = '01t80000002NOOR'; qp.PricebookEntryId__c = '01u80000006eaqb';
                qp.Year_1__c = false;
                qp.Y2_Quote_Price__c = 10000.00;
                qp.Type__c = 'New';
                qp.Rush__c = 'Yes';
                qp.Indexing_Language__c = 'English';
            }
            if (i==2) {
                qp.Name__c = 'On-Demand Training - Instructor Role - Stand Alone'; qp.Product__c = '01t80000003kZ0m'; qp.PricebookEntryId__c = '01u80000009y06A';
                qp.Year_1__c = false; 
                qp.Year_2__c = false;
            }
            if (i==3) {
                qp.Name__c = 'Update Services - Ongoing Indexing'; qp.Product__c = '01t34000003vc2J'; qp.PricebookEntryId__c = '01u3400000DI4WK';
                qp.Year_1__c = false; 
                qp.Year_2__c = false;
                qp.Year_3__c = false;
            }
            if (i==4) {
                qp.Name__c = 'Custom Services Project'; qp.Product__c = '01t80000003RcTu'; qp.PricebookEntryId__c = '01u80000008yoj2';
            }
            if (i==5) {
                qp.Name__c = 'Addl Licensing Fees'; qp.Product__c = '01t80000002NONE'; qp.PricebookEntryId__c = '01u80000006eapJ'; qp.Type__c = 'New';
            }
            if (i==6) {
                qp.Name__c = 'Custom Label'; qp.Product__c = '01t80000002NPrl'; qp.PricebookEntryId__c = '01u80000006ekuO';
                qp.Type__c = 'Renewal';
                qp.Renewal_Amount__c = 2000.00;
            }
            if (i==7) {
                qp.Name__c = 'Webpliance'; qp.Product__c = '01t80000002NONi'; qp.PricebookID__c = '01u80000006eapn';
            }
            if (i==8) {
                qp.Name__c = 'PPI'; qp.Product__c = '01t80000003PMBN'; qp.PricebookID__c = '01u80000007Tlhq';
            }
            if (i==9) {
                qp.Name__c = 'Spill Center'; qp.Product__c = '01t80000002NOPF'; qp.PricebookID__c = '01u80000006earP';
            }
            
            qps.add(qp);
        }
        insert qps;
        
        string addFields = 'CreatedBy.Department__c, CreatedBy.Group__c, CreatedBy.Role__c, Product__r.Sales_Executive_Approval__c, Product__r.Online_Training_Approval__c, Product__r.Associate_Approval__c, '+
            'Product__r.Authoring_Sales_Executive_Approval__c, Product__r.Languages_Requiring_Approval__c, Product__r.Enterprise_Sales_Rep_Approval__c, Product__r.Senior_Sales_Executive_Approval__c, Product__r.Team_Leader_Approval__c, Product__r.Director_Approval__c, '+
            'Product__r.VP_Approval__c, Product__r.Sales_Product_Grouping__c, Quote__r.Account__r.Active_MSDS_Management_Products__c, Quote__r.Department__c, Product__r.ODT__c, Quote__r.Price_Book__c, Product__r.Family, CreatedBy.Product_Group__c ';
        qps = (Quote_Product__c[])new dynamicQuery().queryList('Quote_Product__c', addFields, 'Quote__c = \''+q.id+'\'', null, null);
        
        quoteApproval app = new quoteApproval();
        if (u.Group__c != 'Retention') {
            qps = app.salesProductCheck(qps, FALSE);
            for (Quote_Product__c qp : qps) {
                qp = app.findSalesApprovers(qp);
            }
        } else {
            qps = app.retentionProductApproval(qps);
        }
        
    }
    
    
    
        static testmethod void test2() {
            User user7 = [SELECT id, Alias, Group__c, Product_Group__c FROM User WHERE isActive = true AND Department__c = 'Customer Care' AND Product_Group__c = 'Customer Care' LIMIT 1];
                System.runAs(user7) {

             /*User user7 = [SELECT id, Alias, Group__c, Product_Group__c FROM User WHERE isActive = true AND Department__c = 'Customer Care' AND Product_Group__c = 'Customer Care' LIMIT 1];
                System.runAs(user7) {
                    
                    Account a = new Account(Name='Test Account', Active_MSDS_Management_Products__c='GM');
                    insert a;
                    
                    Contact c = new Contact(FirstName='Firstname', LastName='Lastname', AccountId=a.id);
                    insert c;
                    
                    Quote_Promotion__c p = new Quote_Promotion__c(Name='Promo', Start_Date__c=date.today(), End_Date__c=date.today().addDays(90), Code__c=user7.Alias+'_Promo');
                    insert p;
                    
                    Quote_Promotion_Product__c qpp = new Quote_Promotion_Product__c(Quote_Promotion__c=p.id, Name__c='HQ Account', Product__c='01t80000002NON4', Discount__c=1, Discount_Method__c='Amount Off', 
                                                                                    Year_1__c=true, Year_2__c=true, Year_3__c=true, Year_4__c=true, Year_5__c=true);
                    insert qpp;
                    
                    Quote__c q = new Quote__c(Account__c=a.id, Contact__c=c.id, Promotion__c=p.id, Contract_Length__c='5 Years', Price_Book__c='01s300000001EAF');
                    insert q;

                    Quote_Product__c[] qps = new list<Quote_Product__c>();
                    for (integer i=0; i<2; i++) {
                        Quote_Product__c qp = new Quote_Product__c();
                        qp.Quote__c = q.id;
                        qp.Quantity__c = 1;
                        qp.Y1_Quote_Price__c = 1000; qp.Y1_List_Price__c = 1500; qp.Year_1__c = true; 
                        qp.Y2_Quote_Price__c = 1000; qp.Y2_List_Price__c = 1500; qp.Year_2__c = true;
                        qp.Y3_Quote_Price__c = 1000; qp.Y3_List_Price__c = 1500; qp.Year_3__c = true;
                        qp.Y4_Quote_Price__c = 1000; qp.Y4_List_Price__c = 1500; qp.Year_4__c = true;
                        qp.Y5_Quote_Price__c = 1000; qp.Y5_List_Price__c = 1500; qp.Year_5__c = true; 
                        if (i==0) {	
                            qp.Name__c = 'MSDS Uploads'; qp.Product__c = '01t80000002NOP0'; qp.PricebookEntryId__c = '01u80000006earAAAQ';
                            qp.Y1_Quote_Price__c = 99;
                            qp.Type__c = 'New';
                        }
                        if (i==1) {
                            qp.Name__c = 'Additional Site Administrators'; qp.Product__c = '01t80000002NPPs'; qp.PricebookEntryId__c = '01u80000006ehZpAAI';
                            qp.Y2_Quote_Price__c = 99.00;
                            qp.Type__c = 'New';
                        }
                    }
                    
                    insert qps;
                    
                    string addFields = 'CreatedBy.Department__c, CreatedBy.Group__c, CreatedBy.Role__c, Product__r.Sales_Executive_Approval__c, Product__r.Online_Training_Approval__c, Product__r.Associate_Approval__c, '+
                        'Product__r.Authoring_Sales_Executive_Approval__c, Product__r.Languages_Requiring_Approval__c, Product__r.Enterprise_Sales_Rep_Approval__c, Product__r.Senior_Sales_Executive_Approval__c, Product__r.Team_Leader_Approval__c, Product__r.Director_Approval__c, '+
                        'Product__r.VP_Approval__c, Product__r.Sales_Product_Grouping__c, Quote__r.Account__r.Active_MSDS_Management_Products__c, Quote__r.Department__c, Product__r.ODT__c, Quote__r.Price_Book__c, Product__r.Family, CreatedBy.Product_Group__c ';
                    qps = (Quote_Product__c[])new dynamicQuery().queryList('Quote_Product__c', addFields, 'Quote__c = \''+q.id+'\'', null, null);
                    
                    System.debug('Size is ' + qps.size());
                    quoteApproval app = new quoteApproval();
                    
                    qps = app.salesProductCheck(qps, FALSE);    
                }
        }
*/
            
                    Account a = new Account(Name='Test Account', Active_MSDS_Management_Products__c='GM');
                    insert a;
                    
                    Contact c = new Contact(FirstName='Firstname', LastName='Lastname', AccountId=a.id);
                    insert c;
                    
                    Quote_Promotion__c p = new Quote_Promotion__c(Name='Promo', Start_Date__c=date.today(), End_Date__c=date.today().addDays(90), Code__c=user7.Alias+'_Promo');
                    insert p;
                    
                    Quote_Promotion_Product__c qpp = new Quote_Promotion_Product__c(Quote_Promotion__c=p.id, Name__c='HQ Account', Product__c='01t80000002NON4', Discount__c=1, Discount_Method__c='Amount Off', 
                                                                                    Year_1__c=true, Year_2__c=true, Year_3__c=true, Year_4__c=true, Year_5__c=true);
                    insert qpp;
                    
                    Quote__c q = new Quote__c(Account__c=a.id, Contact__c=c.id, Promotion__c=p.id, Contract_Length__c='5 Years', Price_Book__c='01s300000001EAF');
                    insert q;
                    
                    Quote_Product__c[] qps = new list<Quote_Product__c>();
                    
                    Quote_Product__c qp = new Quote_Product__c();
                    qp.Quote__c = q.id;
                    qp.Quantity__c = 1;
                    qp.Y1_Quote_Price__c = 1000; qp.Y1_List_Price__c = 1500; qp.Year_1__c = true; 
                    qp.Y2_Quote_Price__c = 1000; qp.Y2_List_Price__c = 1500; qp.Year_2__c = true;
                    qp.Y3_Quote_Price__c = 1000; qp.Y3_List_Price__c = 1500; qp.Year_3__c = true;
                    qp.Y4_Quote_Price__c = 1000; qp.Y4_List_Price__c = 1500; qp.Year_4__c = true;
                    qp.Y5_Quote_Price__c = 1000; qp.Y5_List_Price__c = 1500; qp.Year_5__c = true; 
                    qp.Name__c = 'Additional Site Administrators'; qp.Product__c = '01t80000002NPPs'; qp.PricebookEntryId__c = '01u80000006ehZpAAI';
                    qp.Y1_Quote_Price__c = 90;
                    qp.Y2_Quote_Price__c = 80;
                    qp.Y3_Quote_Price__c = 70;
                    qp.Y4_Quote_Price__c = 60;
                    qp.Y5_Quote_Price__c = 50;
                    qps.add(qp);
                    
                    
                    Quote_Product__c qp2 = new Quote_Product__c();
                    qp2.Quote__c = q.id;
                    qp2.Quantity__c = 1;
                    qp2.Y1_Quote_Price__c = 1000; qp.Y1_List_Price__c = 1500; qp.Year_1__c = true; 
                    qp2.Y2_Quote_Price__c = 1000; qp.Y2_List_Price__c = 1500; qp.Year_2__c = true;
                    qp2.Y3_Quote_Price__c = 1000; qp.Y3_List_Price__c = 1500; qp.Year_3__c = true;
                    qp2.Y4_Quote_Price__c = 1000; qp.Y4_List_Price__c = 1500; qp.Year_4__c = true;
                    qp2.Y5_Quote_Price__c = 1000; qp.Y5_List_Price__c = 1500; qp.Year_5__c = true; 
                    qp2.Name__c = 'MSDS Uploads'; qp.Product__c = '01t80000002NOP0'; qp.PricebookEntryId__c = '	01u80000006earAAAQ';
                    qps.add(qp2);
            
                    insert qps;
                    
                    string addFields = 'CreatedBy.Department__c, CreatedBy.Group__c, CreatedBy.Role__c, Product__r.Sales_Executive_Approval__c, Product__r.Online_Training_Approval__c, Product__r.Associate_Approval__c, '+
                        'Product__r.Authoring_Sales_Executive_Approval__c, Product__r.Languages_Requiring_Approval__c, Product__r.Enterprise_Sales_Rep_Approval__c, Product__r.Senior_Sales_Executive_Approval__c, Product__r.Team_Leader_Approval__c, Product__r.Director_Approval__c, '+
                        'Product__r.VP_Approval__c, Product__r.Sales_Product_Grouping__c, Quote__r.Account__r.Active_MSDS_Management_Products__c, Quote__r.Department__c, Product__r.ODT__c, Quote__r.Price_Book__c, Product__r.Family, CreatedBy.Product_Group__c, Product__r.Product_Group__c, Product__r.ERS_Discount_Approval__c, Product__r.Emergency_Response_Approval__c ';
                    qps = (Quote_Product__c[])new dynamicQuery().queryList('Quote_Product__c', addFields, 'Quote__c = \''+q.id+'\'', null, null);
                    
                    System.debug('Size is ' + qps.size());
                     quoteApproval app = new quoteApproval();
                     qps = app.salesProductCheck(qps, TRUE);
                }
            
        }
    
}