trigger accountTrigger on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (trigger.isBefore) {
        
        // BEFORE INSERT
        if (trigger.isInsert) {
            accountTriggerHandler.beforeInsert(trigger.new);
        }
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            accountTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
        
        // BEFORE DELETE
        if (trigger.isDelete) {
            accountTriggerHandler.beforeDelete(trigger.old, trigger.new);
        }
        
    }
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            accountTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            accountTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
        // AFTER DELETE
        if (trigger.isDelete) {
            accountTriggerHandler.afterDelete(trigger.old);
        }
        
    }
    if(!test.isRunningTest()){
        dlrs.RollupService.triggerHandler(); 
    }
}