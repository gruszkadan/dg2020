@isTest
public class testProposalBuilderUtility {
    
    public static testmethod void test1() {
        
        //example of data saved on the quote under JSON_Proposal_Data__c  which contains the needs requirements page
        String JsonString = '{"jsonWrapper": [{"Id":"","Name":"Proposed Solution","identifier":"Proposed Solution=","Order":1},{"Id":"","Name":"Needs Requirements","identifier":"Needs Requirements=","Order":2},{"Id":"069M0000000LmPfIAK","Name":"Legal","identifier":"Legal=069M0000000LmPfIAK","Order":3}]}';  
        System.assert( proposalBuilderUtility.containsNeeds(JsonString) == true);
        
        //example of data saved on the quote under JSON_Proposal_Data__c  which DOES NOT contain the needs requirements page
        String JsonStringTwo = '{"jsonWrapper": [{"Id":"","Name":"Proposed Solution","identifier":"Proposed Solution=","Order":1},{"Id":"069M0000000LmPfIAK","Name":"Legal","identifier":"Legal=069M0000000LmPfIAK","Order":2}]}';  
        System.assert( proposalBuilderUtility.containsNeeds(JsonStringTwo) == false);
        
        System.assert( proposalBuilderUtility.returnDisplayValue(49, 0.9871) == 48.37);
        System.assert( proposalBuilderUtility.returnDisplayValue(48.44, 1) == 48.44);
        
    }
}