@isTest(seeAllData=true)
public class testProposalBuilderProposedSolution {
    
    //test single quote
    public static testmethod void test1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Contract_Length__c = '2 Years';
        newQuote.Healthcare_Customer__c='No';
        insert newQuote;
        
        string myQuoteName = [Select Name from Quote__c where Id = :newQuote.Id].Name;
        
        Quote_Product__c myMainPdt = new Quote_Product__c();
        myMainPdt.Name__c = 'Verification Services';
        myMainPdt.Product__c = [SELECT id FROM Product2 WHERE Name = 'Verification Services' LIMIT 1].id;
        myMainPdt.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Verification Services' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        myMainPdt.Quantity__c = 30;
        myMainPdt.Print_Group__c = 'Services';
        myMainPdt.Quote__c = newQuote.Id;
        myMainPdt.Year_1__c = TRUE;
        myMainPdt.Year_2__c = TRUE;
        myMainPdt.Year_3__c = TRUE;
        insert myMainPdt;
        
        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        ProposalBuilderProposedSolution con = new ProposalBuilderProposedSolution(); 
        
        ApexPages.currentPage().getParameters().put('QuoteId', newQuote.Id);
        con.quoteWrapperList[0].pricing = 'Specific Date';
        
        //confirm error if try to save without setting a specific date
        con.productOrderString = myQuoteName + '==' + myMainPdt.Print_Group__c + '=' + myMainPdt.Id;
        con.Next();
        System.assert(con.ValError == true);
        
        //repeat with field populated
        con.quoteWrapperList[0].quote.Pricing_Terms_Specific_Date__c =  system.today().addMonths(3);
        con.Next();
        System.assert(con.ValError == false);
        
        Quote__c checkQuoteData = [Select Pricing_Terms__c, Pricing_Terms_Specific_Date__c from Quote__c where Id = :newQuote.Id];
        System.assert(checkQuoteData.Pricing_Terms__c == 'Specific Date');
        System.assert(checkQuoteData.Pricing_Terms_Specific_Date__c != NULL);
    }
    
    public static testmethod void test2() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Healthcare_Customer__c='No';
        newQuote.Contract_Length__c = '1 Year';
        insert newQuote;
        
        Quote_Product__c HQPdt = new Quote_Product__c();
        HQPdt.Name__c = 'Authoring - MSDS Authoring';
        HQPdt.Product_Print_Name__c = 'Authoring - MSDS Authoring';
        HQPdt.Print_Group__c = 'MSDS/ Chemical Management';
        HQPdt.Product__c = [SELECT id FROM Product2 WHERE Name = 'Authoring - MSDS Authoring' LIMIT 1].id;
        HQPdt.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - MSDS Authoring' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        HQPdt.Quantity__c = 30;
        HQPdt.Quote__c = newQuote.Id;
        HQPdt.Year_1__c = TRUE;
        HQPdt.Year_2__c = TRUE;
        HQPdt.Year_3__c = TRUE;
        HQPdt.Sort_Order__c = 0;
        insert HQPdt; 
        
        Quote_Product__c myMainPdt = new Quote_Product__c();
        myMainPdt.Name__c = 'Verification Services';
        myMainPdt.Product_Print_Name__c = 'Verification Services';
        myMainPdt.Print_Group__c = 'Services';
        myMainPdt.Product__c = [SELECT id FROM Product2 WHERE Name = 'Verification Services' LIMIT 1].id;
        myMainPdt.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Verification Services' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        myMainPdt.Quantity__c = 30;
        myMainPdt.Quote__c = newQuote.Id;
        myMainPdt.Year_1__c = TRUE;
        myMainPdt.Year_2__c = TRUE;
        myMainPdt.Year_3__c = TRUE;
        myMainPdt.Main_Bundled_Product__c = true;
        myMainPdt.Bundled_Indexing_Display_Type__c = 'Combined';
        insert myMainPdt;
        
        //set this to parent and create children
        myMainPdt.Group_Name__c = 'Verification Services';
        myMainPdt.Group_Parent_Id__c = myMainPdt.Id;
        update myMainPdt;
        
        Quote_Product_Bundle__c myBundle = new Quote_Product_Bundle__c();
        myBundle.Quote__c = newQuote.Id;
        myBundle.Main_Quote_Product__c = myMainPdt.Id;
        insert myBundle;
        
        Quote_Product__c myChildPdt = new Quote_Product__c();
        myChildPdt.Name__c = 'Ongoing Indexing Field - Container Indexing';
        myChildPdt.Print_Group__c = 'Services';
        myChildPdt.Product_Print_Name__c =  'Ongoing Indexing Field - Container Indexing';
        myChildPdt.Product__c = [SELECT id FROM Product2 WHERE Name = 'Ongoing Indexing Field - Container Indexing' LIMIT 1].id;
        myChildPdt.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Ongoing Indexing Field - Container Indexing' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        myChildPdt.Quantity__c = 30;
        myChildPdt.Quote__c = newQuote.Id;
        myChildPdt.Group_Name__c = 'Verification Services';
        myChildPdt.Group_Parent_Id__c = myMainPdt.Id;
        myChildPdt.Group_ID__c = myMainPdt.Id;
        myChildPdt.Year_1__c = TRUE;
        myChildPdt.Year_2__c = TRUE;
        myChildPdt.Year_3__c = TRUE;
        myChildPdt.Bundle__c = TRUE;
        myChildPdt.Bundled_Product__c = myMainPdt.Id;
        myChildPdt.Quote_Product_Bundle__c = myBundle.Id;
        insert myChildPdt;

        Quote_Product__c myChildPdtB = new Quote_Product__c();
        myChildPdtB.Name__c = 'Ongoing Indexing Field - Banned Product';
        myChildPdtB.Product_Print_Name__c =  'Ongoing Indexing Field - Banned Product';
        myChildPdtB.Print_Group__c = 'Services';
        myChildPdtB.Product__c = [SELECT id FROM Product2 WHERE Name = 'Ongoing Indexing Field - Banned Product' LIMIT 1].id;
        myChildPdtB.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Ongoing Indexing Field - Banned Product' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        myChildPdtB.Quantity__c = 30;
        myChildPdtB.Quote__c = newQuote.Id;
        myChildPdtB.Group_Name__c = 'Verification Services';
        myChildPdtB.Group_Parent_Id__c = myMainPdt.Id;
        myChildPdtB.Group_ID__c = myMainPdt.Id;
        myChildPdtB.Year_1__c = TRUE;
        myChildPdtB.Year_2__c = TRUE;
        myChildPdtB.Year_3__c = TRUE;
        myChildPdt.Bundle__c = TRUE;
        myChildPdtB.Bundled_Product__c = myMainPdt.Id;
        myChildPdtB.Quote_Product_Bundle__c = myBundle.Id;
        insert myChildPdtB;
        
        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        ProposalBuilderProposedSolution con = new ProposalBuilderProposedSolution(); 
        
        //confrim controller correctly sorted two families
        System.assert(con.quoteWrapperList[0].printGroupWrapperList.size() == 2);
        
        string myQuoteName = [Select Name from Quote__c where Id = :newQuote.Id].Name;
        con.productOrderString = myQuoteName + '==' + myMainPdt.Print_Group__c + '=' + myMainPdt.Id + ',' 
            + myQuoteName + '==' + myChildPdt.Print_Group__c + '=' + myChildPdt.Id + ','
            + myQuoteName + '==' + myChildPdtB.Print_Group__c + '=' + myChildPdtB.Id + ','
            + myQuoteName + '==' + HQPdt.Print_Group__c + '=' + HQPdt.Id;         
        System.debug(con.productOrderString);
        con.Next();
        
        Quote_Product__c checkHQData = [Select Sort_Order__c, Bundled_Indexing_Display_Type__c, Proposal_Family_Sort_Order__c from Quote_Product__c where id = :HQPdt.Id];
        Quote_Product__c checkMaintData = [Select Sort_Order__c, Bundled_Indexing_Display_Type__c, Proposal_Family_Sort_Order__c from Quote_Product__c where id = :myMainPdt.Id];
        Quote_Product__c checkChildBData = [Select Sort_Order__c, Bundled_Indexing_Display_Type__c, Proposal_Family_Sort_Order__c from Quote_Product__c where id = :myChildPdtB.Id];
        
        //Confrim sort order (pdt level and family level)
        System.assert(checkHQData.Sort_Order__c == 3 && checkHQData.Proposal_Family_Sort_Order__c == 1);
        System.assert(checkMaintData.Sort_Order__c == 0 && checkMaintData.Proposal_Family_Sort_Order__c == 0);
        System.assert(checkChildBData.Sort_Order__c == 2 && checkChildBData.Proposal_Family_Sort_Order__c == 0);
        
        //Confirm the children's display time is set to combined
        System.assert(checkChildBData.Bundled_Indexing_Display_Type__c == 'Combined');
    }
    
    
    public static testmethod void test3() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        //insert test option package
        Quote_Options__c qop = new Quote_Options__c(Type__c='New');
        insert qop;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Contract_Length__c = '3 Years';
        newQuote.Healthcare_Customer__c='No';
        newQuote.Quote_Options__c = qop.id;
        insert newQuote;
        
        Quote__c newQuoteB = new Quote__c();
        newQuoteB.Account__c = newAccount.Id;
        newQuoteB.Contact__c = newContact.Id;
        newQuoteB.Contract_Length__c = '4 Years';
        newQuoteB.Healthcare_Customer__c='No';
        newQuoteB.Quote_Options__c = qop.id;
        insert newQuoteB;
        
        Quote_Product__c myMainPdt = new Quote_Product__c();
        myMainPdt.Name__c = 'Verification Services';
        myMainPdt.Product_Print_Name__c = 'Verification Services';
        myMainPdt.Print_Group__c = 'Services';
        myMainPdt.Product__c = [SELECT id FROM Product2 WHERE Name = 'Verification Services' LIMIT 1].id;
        myMainPdt.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Verification Services' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        myMainPdt.Quantity__c = 30;
        myMainPdt.Quote__c = newQuote.Id;
        myMainPdt.Year_1__c = TRUE;
        myMainPdt.Year_2__c = TRUE;
        myMainPdt.Year_3__c = TRUE;
        myMainPdt.Main_Bundled_Product__c = true;
        insert myMainPdt;
        
        //set this to parent and create children
        myMainPdt.Group_Name__c = 'Verification Services';
        myMainPdt.Group_Parent_Id__c = myMainPdt.Id;
        update myMainPdt;
        
        Quote_Product_Bundle__c myBundle = new Quote_Product_Bundle__c();
        myBundle.Quote__c = newQuote.Id;
        myBundle.Main_Quote_Product__c = myMainPdt.Id;
        insert myBundle;
        
        Quote_Product__c myChildPdt = new Quote_Product__c();
        myChildPdt.Name__c = 'Ongoing Indexing Field - Container Indexing';
        myChildPdt.Print_Group__c = 'Services';
        myChildPdt.Product_Print_Name__c =  'Ongoing Indexing Field - Container Indexing';
        myChildPdt.Product__c = [SELECT id FROM Product2 WHERE Name = 'Ongoing Indexing Field - Container Indexing' LIMIT 1].id;
        myChildPdt.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Ongoing Indexing Field - Container Indexing' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        myChildPdt.Quantity__c = 30;
        myChildPdt.Quote__c = newQuote.Id;
        myChildPdt.Group_Name__c = 'Verification Services';
        myChildPdt.Group_Parent_Id__c = myMainPdt.Id;
        myChildPdt.Group_ID__c = myMainPdt.Id;
        myChildPdt.Year_1__c = TRUE;
        myChildPdt.Year_2__c = TRUE;
        myChildPdt.Year_3__c = TRUE;
        myChildPdt.Bundle__c = TRUE;
        myChildPdt.Bundled_Product__c = myMainPdt.Id;
        myChildPdt.Quote_Product_Bundle__c = myBundle.Id;
        insert myChildPdt;
        
        Quote_Product__c myChildPdtB = new Quote_Product__c();
        myChildPdtB.Name__c = 'Ongoing Indexing Field - Banned Product';
        myChildPdtB.Product_Print_Name__c =  'Ongoing Indexing Field - Banned Product';
        myChildPdtB.Print_Group__c = 'Services';
        myChildPdtB.Product__c = [SELECT id FROM Product2 WHERE Name = 'Ongoing Indexing Field - Banned Product' LIMIT 1].id;
        myChildPdtB.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Ongoing Indexing Field - Banned Product' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        myChildPdtB.Quantity__c = 30;
        myChildPdtB.Quote__c = newQuote.Id;
        myChildPdtB.Group_Name__c = 'Verification Services';
        myChildPdtB.Group_Parent_Id__c = myMainPdt.Id;
        myChildPdtB.Group_ID__c = myMainPdt.Id;
        myChildPdtB.Year_1__c = TRUE;
        myChildPdtB.Year_2__c = TRUE;
        myChildPdtB.Year_3__c = TRUE;
        myChildPdt.Bundle__c = TRUE;
        myChildPdtB.Bundled_Product__c = myMainPdt.Id;
        myChildPdtB.Quote_Product_Bundle__c = myBundle.Id;
        insert myChildPdtB;

        Quote_Product__c HQPdt = new Quote_Product__c();
        HQPdt.Name__c = 'Authoring - MSDS Authoring';
        HQPdt.Product_Print_Name__c = 'Authoring - MSDS Authoring';
        HQPdt.Print_Group__c = 'MSDS/ Chemical Management';
        HQPdt.Product__c = [SELECT id FROM Product2 WHERE Name = 'Authoring - MSDS Authoring' LIMIT 1].id;
        HQPdt.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Authoring - MSDS Authoring' AND isActive = true AND Pricebook2.Name = 'Standard Price Book' LIMIT 1].id;
        HQPdt.Quantity__c = 30;
        HQPdt.Quote__c = newQuoteB.Id;
        HQPdt.Year_1__c = TRUE;
        HQPdt.Year_2__c = TRUE;
        HQPdt.Year_3__c = TRUE;
        insert HQPdt; 

        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        ProposalBuilderProposedSolution con = new ProposalBuilderProposedSolution(); 
        
        //Check controller structure: 1) confrim controller correctly sorted two quotes, 2) Check that QuoteB's default is not included
        System.assert(con.quoteWrapperList.size() == 2);
        System.assert(con.quoteWrapperList[1].includeOption == false);
        
        //include Quote B on the proposal - so that fields save
        ApexPages.currentPage().getParameters().put('QuoteId', newQuoteB.Id);
        con.includeQuote();

        string myQuoteName = [Select Name from Quote__c where Id = :newQuote.Id].Name;
        string myQuoteNameB = [Select Name from Quote__c where Id = :newQuoteB.Id].Name;
        con.productOrderString = myQuoteName + '==' + myMainPdt.Print_Group__c + '=' + myMainPdt.Id + ',' 
            + myQuoteName + '==' + myChildPdt.Print_Group__c + '=' + myChildPdt.Id + ','
            + myQuoteName + '==' + myChildPdtB.Print_Group__c + '=' + myChildPdtB.Id + ','
            + myQuoteNameB + '==' + HQPdt.Print_Group__c + '=' + HQPdt.Id;         
        
        con.Next();        
        Quote_Product__c checkHQData = [Select Sort_Order__c, Bundled_Indexing_Display_Type__c, Proposal_Family_Sort_Order__c from Quote_Product__c where id = :HQPdt.Id];
        Quote_Product__c checkMaintData = [Select Sort_Order__c, Bundled_Indexing_Display_Type__c, Proposal_Family_Sort_Order__c from Quote_Product__c where id = :myMainPdt.Id];
        Quote_Product__c checkChildBData = [Select Sort_Order__c, Bundled_Indexing_Display_Type__c, Proposal_Family_Sort_Order__c from Quote_Product__c where id = :myChildPdtB.Id];
        
        //Confrim sort order (pdt level and family level)
        System.assert(checkHQData.Sort_Order__c == 0 && checkHQData.Proposal_Family_Sort_Order__c == 0);
        System.assert(checkMaintData.Sort_Order__c == 0 && checkMaintData.Proposal_Family_Sort_Order__c == 0);
        System.assert(checkChildBData.Sort_Order__c == 2 && checkChildBData.Proposal_Family_Sort_Order__c == 0);	
        
        System.debug(checkChildBData.Bundled_Indexing_Display_Type__c);
        System.assert(checkChildBData.Bundled_Indexing_Display_Type__c =='Itemized');
        
		ApexPages.currentPage().getParameters().put('QuoteId', newQuote.Id);
        con.setEmployee();
        con.setDescription();
        ApexPages.currentPage().getParameters().put('QuotePdtId', myChildPdtB.Id);
        con.hideValue();
        con.Next(); 
        Quote_Product__c checkmyChildPdtBAgain = [Select hide_quantity__c from Quote_Product__c where id = :myChildPdtB.Id];
        System.assert(checkmyChildPdtBAgain.hide_quantity__c ==  true);
    }
    
    public static testmethod void test4() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
		
        Product_Print_Grouping__c PPG = [Select id, Name__c from Product_Print_Grouping__c where Name__c = 'Implementation Fee' limit 1];

        Quote_Options__c qop = new Quote_Options__c(Type__c='New');
        insert qop;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Healthcare_Customer__c='No';
        newQuote.Contract_Length__c = '5 Years';
        newQuote.Include_Option_In_Proposal__c = true;
        newQuote.Quote_Options__c = qop.id;
        insert newQuote;
        
        Quote_Product__c myMainPdt = new Quote_Product__c();
        myMainPdt.Name__c = 'Audit & Inspection';
        myMainPdt.Product_Print_Name__c = 'Audit & Inspection';
        myMainPdt.Print_Group__c = 'Services';
        myMainPdt.Product__c = [SELECT id FROM Product2 WHERE Name = 'Audit & Inspection' LIMIT 1].id;
        myMainPdt.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Audit & Inspection' AND isActive = true AND Pricebook2.Name = 'Standard Price - Group  - Off the Shelf' LIMIT 1].id;
        myMainPdt.Quantity__c = 30;
        myMainPdt.Quote__c = newQuote.Id;
        myMainPdt.Year_1__c = TRUE;
        myMainPdt.Year_2__c = TRUE;
        myMainPdt.Year_3__c = TRUE;
        
        //myMainPdt.Main_Bundled_Product__c = true;
        // myMainPdt.Group_Name__c = 'Verification Services';
        insert myMainPdt;
        
        Quote_Product__c myPdtB = new Quote_Product__c();
        myPdtB.Name__c = 'Audit & Inspection - Implementation Fee';
        myPdtB.Product_Print_Name__c = 'Audit & Inspection - Implementation Fee';
        myPdtB.Print_Group__c = 'Services';
        myPdtB.Product__c = [SELECT id FROM Product2 WHERE Name = 'Audit & Inspection - Implementation Fee' LIMIT 1].id;
        //myPdtB.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Audit & Inspection - Implementation Fee' AND isActive = true AND Pricebook2.Name = 'Standard Price - Group - Off the Shelf' LIMIT 1].id;
        myPdtB.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Audit & Inspection' AND isActive = true AND Pricebook2.Name = 'Standard Price - Group  - Off the Shelf' LIMIT 1].id;
        myPdtB.Quantity__c = 30;
        myPdtB.Quote__c = newQuote.Id;
        myPdtB.Year_1__c = TRUE;
        myPdtB.Product_Print_Grouping_Name__c = 'Implementation Fee';
        //myPdtB.Group_Name__c = 'Verification Services';
        insert myPdtB;
        
        Quote_Product__c myMainPdtC = new Quote_Product__c();
        myMainPdtC.Name__c = 'Performance Metric';
        myMainPdtC.Product_Print_Name__c = 'Performance Metric';
        myMainPdtC.Print_Group__c = 'Services';
        myMainPdtC.Product__c = [SELECT id FROM Product2 WHERE Name = 'Performance Metric' LIMIT 1].id;
        myMainPdtC.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Performance Metric' AND isActive = true AND Pricebook2.Name = 'Standard Price - Group  - Off the Shelf' LIMIT 1].id;
        myMainPdtC.Quantity__c = 30;
        myMainPdtC.Quote__c = newQuote.Id;
        myMainPdtC.Year_1__c = TRUE;
        myMainPdtC.Year_2__c = TRUE;
        myMainPdtC.Year_3__c = TRUE;
        //myMainPdt.Main_Bundled_Product__c = true;
        // myMainPdt.Group_Name__c = 'Verification Services';
        insert myMainPdtC;
        
        Quote_Product__c myPdtD = new Quote_Product__c();
        myPdtD.Name__c = 'Performance Metric - Implementation Fee';
        myPdtD.Product_Print_Name__c = 'Performance Metric - Implementation Fee';
        myPdtD.Print_Group__c = 'Services';
        myPdtD.Product__c = [SELECT id FROM Product2 WHERE Name = 'Performance Metric - Implementation Fee' LIMIT 1].id;
        myMainPdtC.PricebookEntryId__c = [SELECT id FROM PricebookEntry WHERE Name = 'Performance Metric' AND isActive = true AND Pricebook2.Name = 'Standard Price - Group  - Off the Shelf' LIMIT 1].id;
        myPdtD.Quantity__c = 30;
        myPdtD.Quote__c = newQuote.Id;
        myPdtD.Year_1__c = TRUE;
        myPdtD.Product_Print_Grouping_Name__c = 'Implementation Fee';
        //myPdtB.Group_Name__c = 'Verification Services';
        insert myPdtD;
        
        //set this to parent and create children
        //myMainPdt.Group_Parent_Id__c = myMainPdt.Id;
        //update myMainPdt;
        
        Quote_Product_Bundle__c myBundle = new Quote_Product_Bundle__c();
        myBundle.Quote__c = newQuote.Id;
        myBundle.Main_Quote_Product__c = myMainPdt.Id;
        insert myBundle;
        
        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        ProposalBuilderProposedSolution con = new ProposalBuilderProposedSolution(); 
        ApexPages.currentPage().getParameters().put('QuotePdtId', myPdtD.Id);
        con.hideValue();
        
        ApexPages.currentPage().getParameters().put('input', myPdtD.Id);
        ApexPages.currentPage().getParameters().put('year', '1');
        con.showInput();
        
        
        ApexPages.currentPage().getParameters().put('currentQuote', newQuote.Id);
        string myQuoteName = [Select Name from Quote__c where Id = :newQuote.Id].Name;
        con.quoteWrapperList[0].buttons[0].isSelected =true;
        con.productOrderString = myQuoteName + '==' + myMainPdt.Print_Group__c + '=' + myMainPdt.Id;
        ApexPages.currentPage().getParameters().put('type', PPG.id);
        con.selectGroupingButton();
       
        con.updateGroups();   
        
        con.currentGroupParent = PPG.id;
        con.updateGroupPrices();
        con.clearfield();
        
    }

}