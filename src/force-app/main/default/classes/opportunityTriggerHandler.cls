public class opportunityTriggerHandler {
    
    // BEFORE INSERT
    public static void beforeInsert(Opportunity[] Opportunities) {
        
        //Add Active Products found on the Account (if any) on the Opportunity to help determine Upsell/Cross-sell
        opportunityUtility.addProducts(Opportunities);
        
        //Add Created By Role to Opportunity
        opportunityUtility.opportunityCreated(Opportunities);
    }
    
    // BEFORE UPDATE
    public static void beforeUpdate(Opportunity[] oldOpportunities, Opportunity[] newOpportunities) {}
    
    // BEFORE DELETE
    public static void beforeDelete(Opportunity[] oldOpportunities, Opportunity[] newOpportunities) {}
    
    // AFTER INSERT
    public static void afterInsert(Opportunity[] oldOpportunities, Opportunity[] newOpportunities) {
        Opportunity[] insertedNewOpps = new list<Opportunity>();
        Opportunity[] insertedRenewalOpps = new list<Opportunity>();
        for (Opportunity opp : newOpportunities) {
            if (opp.ContactID__c != null) {
                if (opp.Owner_Group__c == 'Retention') {
                    insertedRenewalOpps.add(opp);
                } else if(opp.StageName != 'Discovery'){
                    insertedNewOpps.add(opp);
                }
            }
        }
        if(insertedNewOpps.size() > 0){
            //send to poiActionSession for a confirmed date
            poiActionSession.sessionConfirmedDate(insertedNewOpps, 'Opportunity');
            // Send to stageStatusUtility to update the contact's stage 
            poiStageStatusUtility.newOpportunityPOIStage(insertedNewOpps);
        }
        if (insertedRenewalOpps.size() > 0) {
            poiStageStatusUtility.renewalOpportunityPOIStage(insertedRenewalOpps);
        }
    }    
    
    // AFTER UPDATE
    public static void afterUpdate(Opportunity[] oldOpportunities, Opportunity[] newOpportunities) {
        // Lists to hold closed "New" and "Renewal"
        Opportunity[] closedSalesOpportunities = new list<Opportunity>();
        Opportunity[] closedRenewalOpportunities = new list<Opportunity>();
        Opportunity[] newOppsMovingOutOfDiscovery = new list<Opportunity>();
        
        for (integer i=0; i<newOpportunities.size(); i++) {
            //If Opp goes from Discovery to anoher stage and is not closed
            if(newOpportunities[i].isClosed == FALSE && oldOpportunities[i].StageName == 'Discovery' && newOpportunities[i].StageName != 'Discovery') { 
                newOppsMovingOutOfDiscovery.add(newOpportunities[i]);
            }
            // Opportunity is closed
            if(oldOpportunities[i].isClosed == FALSE && newOpportunities[i].isClosed == TRUE) { 
                // New opportunities
                if (newOpportunities[i].Owner_Sales_Team__c!='' && newOpportunities[i].Owner_Sales_Team__c !='None' && newOpportunities[i].Opportunity_Type__c=='New') {
                    closedSalesOpportunities.add(newOpportunities[i]);
                }
                // Sets the STAGE on the contact
                if (newOpportunities[i].Owner_Group__c == 'Retention') {
                    closedRenewalOpportunities.add(newOpportunities[i]);
                }
            }
        }
        
        if(newOppsMovingOutOfDiscovery.size() > 0){
            //send to poiActionSession for a confirmed date
            poiActionSession.sessionConfirmedDate(newOppsMovingOutOfDiscovery, 'Opportunity');
            // Send to stageStatusUtility to update the contact's stage 
            poiStageStatusUtility.newOpportunityPOIStage(newOppsMovingOutOfDiscovery);
        }
        if(closedSalesOpportunities.size()>0){
            poiStageStatusUtility.updateStatus(closedSalesOpportunities);
            // double check this
            poiStageStatusUtility.newOpportunityPOIStage(closedSalesOpportunities);
        }
        if (closedRenewalOpportunities.size() > 0) {
            poiStageStatusUtility.renewalOpportunityPOIStage(closedRenewalOpportunities);
        }
        
    }
    
    // AFTER DELETE
    public static void afterDelete(Opportunity[] oldOpportunities, Opportunity[] newOpportunities) {}
    
    // AFTER UNDELETE
    public static void afterUndelete(Opportunity[] oldOpportunities, Opportunity[] newOpportunities) {}
    
}