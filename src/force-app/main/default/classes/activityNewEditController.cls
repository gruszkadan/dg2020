public class activityNewEditController {
    public String recordId {get;set;}
    public id whoId {get;set;}
    public String sObjectType {get;set;}
    public String action {get;set;}
    public Task task {get;set;}
    public Event theEvent {get;set;}
    public User user {get;set;}
    public String accountId {get;set;}
    public Account theAccount {get;set;}
    public String whatType {get;set;}
    public String whatId {get;set;}
    public String whoType {get;set;}
    public boolean isLogACall {get;set;}
    public static String acctPrefix = Account.sObjectType.getDescribe().getKeyPrefix();
    public static String opptyPrefix = Opportunity.sObjectType.getDescribe().getKeyPrefix();
    public static String leadPrefix = Lead.sObjectType.getDescribe().getKeyPrefix();
    public Contact contactInfo {get; set;}
    public Lead leadInfo {get; set;}
    public String Email {get; set;}
    public String Phone {get; set;}
    public String rURL {get;set;}
    public Opportunity[] opportunities {get;set;}
    public String opportunitySelection {get;set;}
    public Opportunity newOpportunity {get;set;}
    public Opportunity upsertOpportunity {get;set;}
    public boolean hasOpenOppInSuite {get;set;}
    public boolean currentOpportunityClosed {get;set;}
    
    public activityNewEditController(){
        //Query User
        user= [Select id, UserPreferencesTaskRemindersCheckboxDefault, Name, Group__c, Suite_Of_Interest__c, Department__c From User where id =: UserInfo.getUserId() LIMIT 1];
        
        //Check page name to dermine activity type 
        String pageName = ApexPages.currentPage().getUrl().substringAfter('apex/');
        if(pageName.contains('task')){
            sObjectType = 'Task';
        }else if (pageName.contains('event')){
            sObjectType = 'Event';
        }
        
        //Check for record id
        recordId = apexpages.currentpage().getparameters().get('id');
        
        //Initialize the record variables
        if(sObjectType == 'Task'){
            task = new Task();
        }else if(sObjectType == 'Event'){
            theEvent = new Event();
        }
        action = 'New';
        //If an Id was found then query the record
        if(recordId != null){
            action = 'Edit';
            queryRecord();
        }
        
        //Initialize Opportunity
        newOpportunity = new Opportunity();
        
        //Set up data
        loadData();
        findOpportunities();
        defaultOpportunitySelection();
        defaultCampaign();
        findCallType();
    }
    
    //The method that queries all fields on the task or event being worked edited
    public void queryRecord(){
        map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        map<string, Schema.SObjectField> fieldMap = schemaMap.get(sObjectType).getDescribe().fields.getMap();
        string csv = ''; 
        for (string fieldName : fieldMap.keyset()) {
            if (csv == null || csv == '') {
                csv = fieldName;
            } else {
                csv = csv + ', ' + fieldName; 
            }
        }
        string query = 'SELECT '+ csv + ' FROM ' + sObjectType + ' WHERE id=:recordId LIMIT 1';
        if(sObjectType == 'Task'){
            task = Database.query(query);
        }else{
            theEvent = Database.query(query);
        }
    }
    
    //Method to set up the activity data on page load
    public void loadData(){
        
        //If the activity is a task
        if(sObjectType == 'Task'){
            
            //Check url parameters specific to tasks
            rURL = ApexPages.currentPage().getParameters().get('retURL');
            whatID = ApexPages.currentPage().getParameters().get('whatid');
            if(whatId == null){
                whatID = ApexPages.currentPage().getParameters().get('what_id');
            }
            if(whatId == null && task.WhatId != null){
                whatID = task.WhatId;
            }
            Id userID = ApexPages.currentPage().getParameters().get('user');
            String e = ApexPages.currentPage().getParameters().get('e');
            String p = ApexPages.currentPage().getParameters().get('p');
            String subject = ApexPages.currentPage().getParameters().get('tsk5');
            whoID = ApexPages.currentPage().getParameters().get('whoID');
            if(whoID == null){
                whoID = ApexPages.currentPage().getParameters().get('who_id');
            }
            if(whoId == null && task.WhoId != null){
                whoId = task.WhoId;
            }
            String status = ApexPages.currentPage().getParameters().get('tsk12');
            String close = ApexPages.currentPage().getParameters().get('close');
            
            
            
            //Determine What & Who Type and find Account ID
            if(whatID != NULL){
                if(String.valueOf(whatID).startsWith(acctPrefix)){
                    whatType = 'Account';
                    accountID = whatId;
                }
                if(String.valueOf(whatID).startsWith(opptyPrefix)){
                    whatType = 'Opportunity';
                    accountID = [SELECT id,AccountId FROM Opportunity WHERE id=:whatId].AccountId;
                }  
            }else if(task.AccountId != null){
                whatType = 'Account';
            }
            if(whoID != NULL){
                if(String.valueOf(whoID).startsWith(leadPrefix)){
                    whoType = 'Lead';
                }
            } 
            
            //For new Tasks
            if(action == 'New'){
                //Default fields
                task.WhatId = whatID;
                task.OwnerId = userID;
                if(task.OwnerId == null){
                    task.OwnerId = user.id;
                }
                task.Subject = subject;
                if(whoId != null && (string.valueof(whoId.getSobjectType()) == 'Contact' || String.Valueof(whoId.getSobjectType())== 'Lead')){
                    task.WhoID = whoID;
                }
                task.Status = status;
                if (status == 'Completed') {
                    task.ActivityDate = date.today();
                    isLogACall = true;
                }
                
                //Set reminder preference
                if(user.UserPreferencesTaskRemindersCheckboxDefault == true){
                    task.IsReminderSet = true;
                } else {
                    task.IsReminderSet = false;
                }
            }else{
                //When editing tasks
                if(close == '1'){
                    task.Status = 'Completed';
                }
                if(task.WhatId == NULL && task.AccountId !=NULL){
                    task.WhatId = task.AccountId;
                }
                
                //set variables - used later
                whoId = task.whoId;
                accountID = task.AccountId;
                
            }
            
            //Query for the account
            if(accountID != null){
                checkAccount();
            }
            
        }else if(sObjectType == 'Event'){
            //For Events
            
            //Pull URl Parameters specific to Events
            rURL = ApexPages.currentPage().getParameters().get('retURL');
            
            //When Editing an Event
            if(recordId != NULL){
                
                //Query Account
                if(theEvent.AccountId != NULL){
                    accountId = theEvent.AccountId;
                    checkAccount();
                }
                
                //Set Variables
                if(theEvent.WhatId != NULL){
                    if(String.valueOf(theEvent.WhatId).startsWith(acctPrefix)){
                        whatType = 'Account';
                    }
                    if(String.valueOf(theEvent.WhatId).startsWith(opptyPrefix)){
                        whatType = 'Opportunity';
                    }
                    whatId = theEvent.WhatId;
                }
                if(theEvent.WhatId == null && theEvent.whoID != null){
                    if(String.valueOf(theEvent.whoID).startsWith(leadPrefix)){
                        whoType = 'Lead';
                    }
                }
                if(whatType =='Opportunity'){
                    accountID = [SELECT id,AccountId FROM Opportunity WHERE id=:theEvent.whatId].AccountId;
                }
                
            } else {
                //For New Events
                
                //Get url parameters specific to new events
                whatID = ApexPages.currentPage().getParameters().get('what_id');
                whoID = ApexPages.currentPage().getParameters().get('who_id');
                
                //Determine What & Who Type and accountId 
                if(whatID != NULL){
                    if(String.valueOf(whatID).startsWith(acctPrefix)){
                        whatType = 'Account';
                        accountId = whatId;
                    }
                    if(String.valueOf(whatID).startsWith(opptyPrefix)){
                        whatType = 'Opportunity';
                        accountID = [SELECT id,AccountId FROM Opportunity WHERE id=:whatId].AccountId;
                    }
                }
                if(whatID == null && whoID != null){
                    if(String.valueOf(whoID).startsWith(leadPrefix)){
                        whoType = 'Lead';
                    }
                }
                
                //Default Fields
                theEvent.WhatId = whatID;
                theEvent.OwnerId = user.id;
                theEvent.Event_Status__c = 'Scheduled';
                if(whoType =='Lead'){
                    theEvent.WhoId = whoID;
                }
                
            }
        }
        
        //Query for Who fields
        findWhoInfo();
    }
    
    //Query Account Information
    public void checkAccount() {
        theAccount = [SELECT id, Date_Connected_with_DM__c, Rating, Account_Rating_Reason__c, Ergonomics_Rating__c, Ergonomics_Rating_Reason__c, Online_Training_Rating__c, Online_Training_Rating_Reason__c, 
                      Authoring_Rating__c, Authoring_Rating_Reason__c, EHS_Rating__c, EHS_Rating_Reason__c, Num_of_SDS__c, Num_of_Employees__c, Current_Ergonomics_Solution__c, Ergonomics_Not_a_Target_Reason__c, Ergonomics_Not_Interested_Reason__c,
                      Current_Training_Method__c, Training_Not_a_Target_Reason__c, Training_Not_Interested_Reason__c
                      FROM Account 
                      WHERE id = : accountID LIMIT 1];
    }
    
    //Query Contact or Lead information
    public void findWhoInfo(){
        //variables to hold email and phone displayed on page
        email = null;
        phone = null;
        
        //Set whoId based on which object is being used
        if(task != null){
            whoId = task.WhoId;
        }else{
            whoId = theEvent.WhoId;
        }
        
        //Query for either Contact or lead information based on the type of the Who relationship
        if(whoId != null && string.valueof(whoId.getsobjecttype()) == 'Contact'){
            contactInfo = [select ID, Email, Name, Phone, Lead_Created_Date__c,AccountID,Account.Name,
                           Lead_Source_Category__c,Lead_Source_Detail__c,Lead_Source_Type__c,mkto_si__Last_Interesting_Moment_Date__c,
                           CreatedDate,Lead_Source__c from Contact where ID=:WhoId];
            email = contactInfo.Email;
            phone = contactInfo.Phone;	
        }
        if(whoId != null && string.valueof(whoId.getsobjecttype()) == 'Lead'){
            leadInfo = [select ID, Email, Phone, Name from Lead where ID=:WhoId];
            email = leadInfo.Email;
            phone = leadInfo.Phone;	
        }
        
        //Default the new opportunity name to account name initially
        if(contactInfo != null && (newOpportunity.Name == '' || newOpportunity.Name == null)){
            newOpportunity.Name = contactinfo.Account.Name;
        }
        
        //Default the campaign for the new opportunity based on the selected contact
        defaultCampaign();
    }
    
    //Returns Select Options of Contacts related to the what account or returns the lead if the activity is associated to a Lead 
    public List<SelectOption> getWhoSelectList() {        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--Select--',false));        
        
        //Query for records dependent on whoType
        if(whoType != 'Lead'){
            List<Contact> contacts;
            
            contacts = [SELECT Id, Name FROM Contact where AccountID=:accountId Order By Name ASC limit 500];
            for (Contact contact : contacts) {
                options.add(new SelectOption(contact.ID, contact.Name));
            }
        }else if(whoType == 'Lead'){
            List<Lead> leads = [SELECT Id, Name FROM Lead where ID=:whoId Order By Name ASC];
            for (Lead lead : leads) {
                options.add(new SelectOption(lead.ID, lead.Name));
            }
        } 
        return options;
    }
    
    //Sets the reminder datetime
    public PageReference updateTime() {
        if(task.ActivityDate != NULL){
            date myDate = task.ActivityDate;
            Time myTime = Time.newInstance(08, 00, 00, 00);
            DateTime D = DateTime.newInstance(myDate,myTime);
            task.ReminderDateTime = D;
            if(user.UserPreferencesTaskRemindersCheckboxDefault == true){
                task.IsReminderSet = true;
            } else {
                task.IsReminderSet = false;
            }
        } else {
            task.ReminderDateTime = NULL;
            task.IsReminderSet = false;
        }
        return null;
    }
    
    //Returns list of subjects for Events
    public List<SelectOption> getSubjects(){
        List<SelectOption> subjects = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Event.Subject.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        subjects.add(new SelectOption('', '-- Select Subject --',false));
        for( Schema.PicklistEntry f : ple)
        {
            subjects.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return subjects;
    }   
    
    //Sets End Date for Events
    public void changeEndDate() {
        if(action == 'New' && theEvent.StartDateTime != null){
            DateTime StartDateTime = theEvent.StartDateTime;
            DateTime StartDateTime1Hour = StartDateTime.addHours(1);
            if(StartDateTime1Hour !=NULL){
                theEvent.EndDateTime = StartDateTime1Hour;
            }else{
                theEvent.EndDateTime = dateTime.now();
            }
        }else{
            theEvent.EndDateTime = null;
        }
    }
    
    //Validation upon saving
    public boolean validation() {
        //Default to true
        boolean pass = true;
        
        //For Tasks
        if(sObjectType == 'Task'){
            if (accountID != null) {
                //Make sure a Contact is selected
                if (task.WhoId == null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a Contact'));
                    pass = false;
                }
                //Category & Call Result are required when completing a task
                if (task.Status == 'Completed' && (task.Call_Result__c == null || task.Category__c == null)) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a category & call result'));
                    pass = false;
                }
            }
        }else{
            //For Events
            
            //Validation
            if(theEvent.WhoId == NULL){
                If(WhoType != null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a '+whoType));
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a Contact'));
                }
                pass = false;
            }
            if(theEvent.Subject == NULL){
                ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'Please select a Subject'));
                pass = false;
            }
            if(theEvent.StartDateTime == NULL){
                ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'Please select a Start Date'));
                pass = false;
            }
            if (theEvent.Event_Status__c == 'Completed' && (theEvent.Call_Result__c == null || theEvent.Category__c == null)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a category & call result'));
                pass = false;
            }
        }
        
        //When Not associated to a lead - Run Opportunity Validation
        if(whoType != 'Lead'){
            
            //If the activity is completed an opportunity must be associated
            if((task != null && task.Status == 'Completed' && (opportunitySelection == null || (hasOpenOppInSuite && opportunitySelection == 'OptOut')))
               || (theEvent != null && theEvent.Event_Status__c == 'Completed' && (opportunitySelection == null || (hasOpenOppInSuite && opportunitySelection == 'OptOut')))){
                   pass = false;
                   ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'Please make an opportunity selection'));
               }
            
            //If selecting an existing opportunity
            if(opportunitySelection != null && opportunitySelection != 'OptOut' && opportunitySelection != 'NewOpportunity'){
                //Close Date is required to be filled in for the selected opportunity
                for(Opportunity opp:opportunities){
                    if(opp.id == id.valueof(opportunitySelection) && opp.CloseDate == null){
                        pass = false;
                        ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'Please enter an opportunity Close Date'));
                    }
                }
                //If selecting to Opt Out
            }else if(opportunitySelection != null && opportunitySelection == 'OptOut'){
                // Opt Out Reason must be filled in 
                if(sObjectType == 'Task' && Task.Status == 'Completed' && task.Opt_Out_Reason__c == null){
                    pass = false;
                    ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'Please select an Opportunity Opt Out Reason'));
                }else if(sObjectType == 'Event' && theEvent.Event_Status__c == 'Completed' && theEvent.Opt_Out_Reason__c == null){
                    pass = false;
                    ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'Please select an Opportunity Opt Out Reason'));
                }
                //If Creating a new opportunity
            }else if(opportunitySelection != null && opportunitySelection == 'NewOpportunity'){
                //Name is required
                if(newOpportunity.name == null){
                    pass = false;
                    ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'Please enter an Opportunity Name'));
                }
                //A campaign selection is required
                if(newOpportunity.CampaignId == null){
                    pass = false;
                    ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'Please select a Campaign'));
                }
            }
        }
        return pass;
    }
    
    //Save Method
    public PageReference save() {
        if (validation()) {
            try{
                //Upsert the Opportunity
                processOpportunity();
                
                //Upsert the Activity 
                if(sObjectType == 'Task'){
                    if(upsertOpportunity != null && upsertOpportunity.id != null){
                        task.WhatId = upsertOpportunity.id;
                    }
                    upsert task;
                    if(theAccount != null){
                        update theAccount;
                    }
                }else{
                    if(upsertOpportunity != null && upsertOpportunity.id != null){
                        theEvent.WhatId = upsertOpportunity.id;
                    }
                    theEvent.ActivityDateTime = theEvent.StartDateTime;
                    upsert theEvent;
                }
            }catch(DMLexception e){
                ApexPages.addMessages(e);
                return null;
            } 
            PageReference pageRef;
            if(sObjectType == 'Task'){
                if(task.WhatId != null){
                    pageRef = new PageReference('/'+ task.WhatId);
                }else{
                    pageRef = new PageReference('/'+ task.WhoId);
                }
            }else{
                if(theEvent.WhatId != null){
                    pageRef = new PageReference('/'+ theEvent.WhatId);
                }else{
                    pageRef = new PageReference('/'+ theEvent.WhoId);
                }
            }
            If(accountId != null){
                pageRef = new PageReference('/'+ accountId);
            }
            return pageRef;
        } else {
            return null;
        }
    }   
    
    //Save & Create new Event Method
    public PageReference saveNewEvent() {
        if (validation()) {
            try{
                //Upsert the Opportunity
                processOpportunity();
                
                //Upsert the Activity 
                if(sObjectType == 'Task'){
                    if(upsertOpportunity != null && upsertOpportunity.id != null){
                        task.WhatId = upsertOpportunity.id;
                    }
                    upsert task;
                    if(theAccount != null){
                        update theAccount;
                    }
                }else{
                    if(upsertOpportunity != null && upsertOpportunity.id != null){
                        theEvent.WhatId = upsertOpportunity.id;
                    }
                    theEvent.ActivityDateTime = theEvent.StartDateTime;
                    upsert theEvent;
                }
            }catch(DMLexception e){
                ApexPages.addMessages(e);
                return null;
            } 
            //PageReference newEvent = new PageReference('/00U/e?');
            PageReference newEvent = new PageReference('/apex/event_new_edit?');
            newEvent.setRedirect(true);
            if(sObjectType == 'Task'){
                if(task.whatId != NULL){
                    newEvent.getParameters().put('what_id',task.whatId); 
                } 
                if(task.whoId != NULL){
                    newEvent.getParameters().put('who_id', task.whoId);    
                }
            }else{
                if(theEvent.whatId != NULL){
                    newEvent.getParameters().put('what_id',theEvent.whatId); 
                } 
                if(theEvent.whoId != NULL){
                    newEvent.getParameters().put('who_id', theEvent.whoId);    
                }
            }
            newEvent.getParameters().put('user',user.id);
            newEvent.getParameters().put('retURL',rURL);
            return newEvent;  
        } else {
            return null;
        }
        
    }
    
    //Save & Create new Event Method
    public PageReference saveNew() {
        if (validation()) {
            try{
                //Upsert the Opportunity
                processOpportunity();
                
                //Upsert the Activity 
                if(sObjectType == 'Task'){
                    if(upsertOpportunity != null && upsertOpportunity.id != null){
                        task.WhatId = upsertOpportunity.id;
                    }
                    upsert task;
                    if(theAccount != null){
                        update theAccount;
                    }
                }else{
                    if(upsertOpportunity != null && upsertOpportunity.id != null){
                        theEvent.WhatId = upsertOpportunity.id;
                    }
                    theEvent.ActivityDateTime = theEvent.StartDateTime;
                    upsert theEvent;
                }
            }catch(DMLexception e){
                ApexPages.addMessages(e);
                return null;
            } 
            PageReference task_new = Page.task_new_edit;
            task_new.setRedirect(true);
            
            if(sObjectType == 'Task'){
                if(task.whatId != NULL){
                    task_new.getParameters().put('whatid',task.WhatId); 
                } 
                if(task.whoId != NULL){
                    task_new.getParameters().put('whoID', task.whoId); 
                    task_new.getParameters().put('e', email);
                    task_new.getParameters().put('p', phone);    
                }
                task_new.getParameters().put('tsk5',task.subject);
            }else{
                if(theEvent.whatId != NULL){
                    task_new.getParameters().put('whatid',theEvent.WhatId); 
                } 
                if(theEvent.whoId != NULL){
                    task_new.getParameters().put('whoID', theEvent.whoId); 
                    task_new.getParameters().put('e', email);
                    task_new.getParameters().put('p', phone);
                }
                task_new.getParameters().put('tsk5',theEvent.subject);
            }
            
            task_new.getParameters().put('user',user.id);
            task_new.getParameters().put('retURL',rURL); 
            
            return task_new;  
        } else {
            return null;
        }
    }
    
    //Cancel Button Method
    public PageReference cancel() {
        if(rURL != null){
            PageReference pageRef = new PageReference(rURL);
            return pageRef;
        } else {
            PageReference pageRef = new PageReference('/home/home.jsp');
            return pageRef;
        }
    }  
    
    //Method to query opportunity table
    public void findOpportunities(){
        opportunities = [Select id,isClosed,Name,OwnerId,Amount,StageName,CloseDate,Suite_of_Interest__c,Contract_Length__c,Currency__c,Currency_Rate__c,
                         (SELECT id, Name__c, Indexing_Language__c, Y1_Total_Price__c, Y2_Total_Price__c, Y3_Total_Price__c, Y4_Total_Price__c, Y5_Total_Price__c, Total_Value__c
                          from OpportunityLineItems 
                          ORDER BY Group_Name__c ASC, Group_Parent_ID__c ASC, Group_ID__c ASC NULLS FIRST, Name__c ASC)
                         FROM Opportunity 
                         WHERE AccountId =:accountId and (isclosed = false OR (isClosed = true and closedate >= Last_N_Days:90) OR id=:whatId) and Opportunity_Type__c = 'New' 
                         ORDER BY isClosed ASC,CloseDate DESC];
        
        //Determine if the current opportunity is closed and if there is an open opportunity in the users suite
        currentOpportunityClosed = false;
        hasOpenOppInSuite = false;
        for(Opportunity o:opportunities){
            if(o.IsClosed == false && o.Suite_Of_Interest__c == user.Suite_Of_Interest__c){
                hasOpenOppInSuite = true;
            }
            if(o.id == whatId && o.isclosed){
                currentOpportunityClosed = true;
            }
        }
    }
    
    //Sets the default selected opportunity
    public void defaultOpportunitySelection(){
        if(whatType == 'Opportunity' && whatId != null){
            opportunitySelection = id.valueof(whatId);
        }else{
            if(action == 'New' || (action == 'Edit' && whatType != null && whatType != 'Opportunity')){
                for(Opportunity o:opportunities){
                    if(o.OwnerId == user.id){
                        opportunitySelection = o.id;
                        whatId = o.id;
                        whatType = 'Opportunity';
                        if(sObjectType == 'Task'){
                            task.WhatId = o.id;
                        }else if(sObjectType == 'Event'){
                            theEvent.WhatId = o.id;
                        }
                        break;
                    }
                }
            }
        }
        if(opportunitySelection == null || opportunitySelection == ''){
            opportunitySelection = 'NewOpportunity';
        }
    }
    
    //Set Whatid Based on the Opportunity Table Selection
    public void changeWhatSelection(){
        if(opportunitySelection == 'NewOpportunity'){
            whatId = null;
            if(sObjectType == 'Task'){
                task.WhatId = null;
                task.Type__c = 'Initial';
            }else if(sObjectType == 'Event'){
                theEvent.WhatId = null;
                theEvent.Type__c = 'Initial';
            }
        }else if(opportunitySelection == 'OptOut'){
            whatId = accountId;
            if(sObjectType == 'Task'){
                task.WhatId = accountId;
                task.Type__c = 'Initial';
            }else if(sObjectType == 'Event'){
                theEvent.WhatId = accountId;
                theEvent.Type__c = 'Initial';
            }
        }else{
            whatId = opportunitySelection;
            whatType = 'Opportunity';
            if(sObjectType == 'Task'){
                task.WhatId = opportunitySelection;
            }else if(sObjectType == 'Event'){
                theEvent.WhatId = opportunitySelection;
            }
            findCallType();
        }
    }
    
    //Pulls back campaigns from all contacts on the account
    public List<SelectOption> getCampaigns() {
        list<CampaignMember> campaignMembers = new list<CampaignMember>();
        if(accountId != null){
            campaignMembers = [Select Campaign.id, Campaign.Name From CampaignMember where contact.Accountid =:accountId order by FirstRespondedDate Desc Limit 100];
        }
        
        list<SelectOption> options = new list<SelectOption>();
        set<SelectOption> optionsSet = new set<SelectOption>();
        optionsSet.add(new SelectOption('', '-- Select Campaign --',false));
        
        Campaign unknownCampaign =  [Select ID, Name from Campaign where Name='Unknown' limit 1];
        optionsSet.add(new SelectOption(unknownCampaign.id, unknownCampaign.name,false));
        
        for(CampaignMember x : campaignMembers){
            optionsSet.add(new SelectOption(x.Campaign.id, x.campaign.name));    
        }
        
        options.addall(optionsSet);
        
        Return options;
    }
    
    //Defaults the Campaign based on the contact
    public void defaultCampaign(){
        if(sObjectType == 'Task'){
            whoId = task.WhoId;
        }else if(sObjectType == 'Event'){
            whoId = theEvent.WhoId;
        }
        newOpportunity.CampaignId = null;
        if(whoId != null){
            CampaignMember[] respondedCampaign = [Select CampaignId, Campaign.Name, CreatedDate From CampaignMember where ContactID=:WhoId and HasResponded = TRUE ORDER By First_Responded_Date__c DESC Limit 1];
            CampaignMember[] lastCampaign = [Select CampaignId, Campaign.Name, CreatedDate From CampaignMember where ContactID=:WhoId ORDER By CreatedDate DESC Limit 1];
            if(respondedCampaign.size() > 0){
                newOpportunity.CampaignId = respondedCampaign[0].CampaignId;
            }else if(lastCampaign.size() > 0){
                newOpportunity.CampaignId = lastCampaign[0].CampaignId;
            }  
        }
    }
    
    //Upsert Logic for Opportunity
    public void processOpportunity(){
        if(opportunitySelection != null && opportunitySelection != 'OptOut' && opportunitySelection != 'NewOpportunity' && whatType != null && whatType != 'Lead'){
            for(Opportunity opp:opportunities){
                if(opp.id == id.valueof(opportunitySelection)){
                    upsertOpportunity = opp;
                }
            }
        }else if(opportunitySelection != null && opportunitySelection == 'NewOpportunity' && whatType != null && whatType != 'Lead'){
            upsertOpportunity = newOpportunity;
            Date cCD = contactInfo.CreatedDate.date();
            upsertOpportunity.AccountID = contactInfo.AccountID;
            upsertOpportunity.CloseDate = date.today().addMonths(18);
            upsertOpportunity.StageName = 'Discovery';
            upsertOpportunity.ContactID__c = contactInfo.id;
            upsertOpportunity.Suite_Of_Interest__c = user.Suite_Of_Interest__c;
            upsertOpportunity.OwnerID = user.ID;
            upsertOpportunity.Amount = 0;
            upsertOpportunity.Opportunity_Type__c ='New';
            upsertOpportunity.LeadSource = contactInfo.Lead_Source__c;
            upsertOpportunity.Lead_Source_Category__c = contactInfo.Lead_Source_Category__c;
            upsertOpportunity.Lead_Source_Detail__c = contactInfo.Lead_Source_Detail__c;
            upsertOpportunity.Lead_Source_Type__c = contactInfo.Lead_Source_Type__c;
            upsertOpportunity.Last_Interesting_Moment_Date__c = contactInfo.mkto_si__Last_Interesting_Moment_Date__c;
            if(contactInfo.Lead_Created_Date__c != NULL){
                Date cLCD = contactInfo.Lead_Created_Date__c.date();
                if(contactInfo.Lead_Created_Date__c < contactInfo.CreatedDate){
                    upsertOpportunity.Lead_CreatedDate__c = cLCD;
                } else {
                    upsertOpportunity.Lead_CreatedDate__c = contactInfo.CreatedDate.date();
                }
            } else {
                upsertOpportunity.Lead_CreatedDate__c = contactInfo.CreatedDate.date();
            }
        }
        
        if(upsertOpportunity != null){
            //Upsert the Opportunity
            upsert upsertOpportunity;
            
            //Create Opportunity Contacts if necessary
            OpportunityContactRole[] existingOppContacts = [SELECT id from OpportunityContactRole Where ContactId=:whoId and OpportunityId =:upsertOpportunity.id];
            
            if(existingOppContacts.size() == 0){
                OpportunityContactRole newOpportunityContact = new OpportunityContactRole();
                newOpportunityContact.OpportunityId = upsertOpportunity.id;
                newOpportunityContact.ContactId = contactInfo.id;
                newOpportunityContact.Role = 'Decision Maker';
                newOpportunityContact.isPrimary = true;        
                insert newOpportunityContact;
            }
        }
    }
    
    public void findCallType(){
        //For new Tasks
        if(action == 'New'){
            String callType = 'Initial';
            if(sObjectType == 'Task'){
                whatId = task.WhatId;
            }else{
                whatId = theEvent.WhatId;
            }
            //Type Automation Logic
            if(whatID != null && whatType == 'Opportunity'){
                AggregateResult[] completedDemo = new list <AggregateResult>();
                completedDemo = [Select MAX(CreatedDate) maxCDate
                                 From Event 
                                 where Suite_of_Interest__c =: user.Suite_Of_Interest__c 
                                 AND Event_Status__c = 'Completed'
                                 AND WhatId =: whatID
                                ];
                if((datetime)completedDemo[0].get('maxCDate') != null){
                    callType = 'Follow-Up';
                }
                
            }
            if(sObjectType == 'Task'){
                task.Type__c = callType;
            }else{
                theEvent.Type__c = callType;
            }
        }
    }
    
}