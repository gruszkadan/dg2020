public class promoManagementController {
    public User u {get;set;}
    public list<Quote_Promotion__c> promos {get;set;}
    public Contract__c[] contracts {get;set;}
    public Quote_Product__c[] quoteProds {get;set;}
    public Quote__c[] quotes {get;set;}
    public string selOwner {get;set;}
    public string selStatus {get;set;}
    private promoWrapper[] promoList {get;set;}
    public string[] statusList {get;set;}
    public string statusFilter {get;set;}
    public string searchName {get;set;}
    private string saveSearchName;
    private string queryPromo;
    public string recPerPage {get; set;}
    public SelectOption[] recPerPageOption {get; set;}  
    public string sortFieldSave;
    public quoteAppWrapper[] wrappers {get;set;}
    //permissions
    public boolean canCreate {get;set;}
    public boolean canEdit {get;set;}
    public boolean canManageNeeds {get;set;}
    
    public promoManagementController() {
        u = [SELECT id, Name, ProfileId FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
        permissions();
        
        canManageNeeds = MSDS_Custom_Settings__c.getInstance().Can_Manage_Needs__c;
        
        promoList = new list<promoWrapper>();
        
        recPerPageOption = new list<SelectOption>();
        recPerPageOption.add(new SelectOption('10','10'));
        recPerPageOption.add(new SelectOption('25','25'));
        recPerPageOption.add(new SelectOption('50','50'));
        recPerPageOption.add(new SelectOption('100','100'));
        recPerPageOption.add(new SelectOption('200','200'));
        recPerPage = '10';
        
        statusList = new list<string> {'New','Pending Approval','Scheduled','Active','Expired','Cancelled','All'};
            sortFieldSave = sortField;
        
        if (apexpages.currentpage().getparameters().get('status') == null) {
            statusFilter = 'All';
        } else {
            statusFilter = apexpages.currentpage().getparameters().get('status');
        }
        
        buildQuery();  
        findApprovalItems();
    }
    	//Create select list filter options
        public SelectOption[] getStatusOptions() {
        SelectOption[] ops = new list<SelectOption>();
        ops.add(new SelectOption('New', 'New'));
        ops.add(new SelectOption('Pending Approval', 'Pending Approval'));
        ops.add(new SelectOption('Scheduled', 'Scheduled'));
        ops.add(new SelectOption('Active', 'Active'));
        ops.add(new SelectOption('Expired', 'Expired'));
        ops.add(new SelectOption('Cancelled', 'Cancelled'));
        ops.add(new SelectOption('All', 'All'));
        return ops;
    }
    
    
    //find the object's create and edit permissions
    public void permissions() {
        ObjectPermissions[] ops = [SELECT id, ParentId, PermissionsCreate, PermissionsEdit
                                   FROM ObjectPermissions
                                   WHERE Parent.IsOwnedByProfile = true
                                   AND Parent.ProfileId = : u.ProfileId
                                   AND SObjectType = 'Quote_Promotion__c'];
        canCreate = ops[0].PermissionsCreate;
        canEdit = ops[0].PermissionsEdit;
    }
    
    public void findApprovalItems() {
        quoteProds = new list<Quote_Product__c>();
        quotes = new list<Quote__c>();
        contracts = new list<Contract__c>();
        set<string> quoteProdIds = new set<string>();
        set<string> contractIds = new set<string>();
        set<string> quoteIds = new set<string>();
        wrappers = new list<quoteAppWrapper>();
        //loop through all approvals assigned to the current user
        for (ProcessInstanceWorkItem item :  [SELECT ProcessInstance.TargetObjectId, ActorId
                                              FROM ProcessInstanceWorkitem 
                                              WHERE ActorId = :u.id]) {
                                                  string targetId = item.ProcessInstance.TargetObjectId;
                                                  
                                                  //if the target object's keyfix is a08, its a quote product
                                                  if (targetId.left(3) == 'a08') {
                                                      quoteProdIds.add(targetId);
                                                  }
                                                  
                                                  //of the target object's keyfix is a07, its a contract
                                                  if (targetId.left(3) == 'a07') {
                                                      contractIds.add(targetId);
                                                  }
                                                  
                                                  if (targetId.left(3) == 'a09') {
                                                      quoteIds.add(targetId);
                                                  }
                                              }
        if (quoteProdIds.size() > 0) {
            //query the quote products in approval
            quoteProds = [SELECT id, Name__c, Approval_Link__c, Quote__c, Quote__r.Name, Quote__r.Owner.Name, Quote__r.Account__r.Name, CreatedDate FROM Quote_Product__c WHERE id IN :quoteProdIds ORDER BY LastModifiedDate ASC];
            for (Quote_Product__c qp : quoteProds) {
                string appLink = '/apex/quoteProduct_view?product_id='+qp.id+'&quote_id='+qp.Quote__c;
                quoteAppWrapper wrap = new quoteAppWrapper(qp.id, qp.Quote__r.Name, qp.Name__c, qp.Quote__r.Owner.Name, qp.Quote__r.Account__r.Name, appLink);
                wrappers.add(wrap);
            }
        }
        
        if (contractIds.size() > 0) {
            //query the contracts in approval
            contracts = [SELECT id, Name, Owner.Name, CreatedDate, Account__r.Name, Status__c, Unsigned_Contract__c FROM Contract__c WHERE id IN :contractIds ORDER BY LastModifiedDate ASC];
        }
        
        if (quoteIds.size() > 0) {
            quotes = [SELECT id, Name, Owner.Name, Account__r.Name FROM Quote__c WHERE id IN : quoteIds ORDER BY LastModifiedDate ASC];
            for (Quote__c q : quotes) {
                quoteAppWrapper wrap = new quoteAppWrapper(q.id, q.Name, null, q.Owner.Name, q.Account__r.Name, null);
                wrappers.add(wrap);
            }

        }
    }
    
    public void cancelPromo() {
        id cancelPromoId = ApexPages.currentPage().getParameters().get('cancelId');
        System.debug('cancelPromoId = '+cancelPromoId);
        if (cancelPromoId != null) {
            for (promoWrapper wrap : getCurrentPromoList()) {
                if (wrap.promo.id == cancelPromoId) {
                    wrap.promo.Status__c = 'Cancelled';
                    update wrap.promo;
                }
            }
        }
    }
    
    public PageReference clonePromo() {
        id clonePromoId = ApexPages.currentPage().getParameters().get('cloneId');
        return new PageReference('/apex/promo_new_step1?cloneId='+clonePromoId).setRedirect(true);        
    }
    
    public PageReference editPromo() {
        id editPromoId = ApexPages.currentPage().getParameters().get('editId');
        return new PageReference('/apex/promo_new_step1?id='+editPromoId).setRedirect(true);        
    }
    
    public PageReference createNewPromo() {
        return new PageReference('/apex/promo_new_step1').setRedirect(true);
    }
    
    public ApexPages.StandardSetController stdSetControllerPromo {
        get {
            if (stdSetControllerPromo == null) {
                stdSetControllerPromo = new ApexPages.StandardSetController(Database.getQueryLocator(queryPromo));
                // sets the number of records in each page set
                stdSetControllerPromo.setPageSize(integer.valueOf(recPerPage));
            }
            return stdSetControllerPromo;
        }
        set;
    }
    
    public promoWrapper[] getCurrentPromoList() {
        promoList = new list<promoWrapper>();
        for (Quote_Promotion__c qp : (list<Quote_Promotion__c>)stdSetControllerPromo.getRecords()) {
            promoList.add(new promoWrapper(qp));
        }
        return promoList;
    }

    public PageReference searchPromo() {
        saveSearchName = searchName;
        buildQuery();
        return null;
    }
    
    public void buildQuery() {
        stdSetControllerpromo = null;
        string queryWhere = '';
        if (statusFilter == null || statusFilter.trim().length() == 0) {
            statusFilter = 'All';
        }
        queryPromo = 'SELECT id, Name, Num_of_Contracts__c, Num_of_Items_on_Promo__c, Num_of_Opportunities__c, Num_of_Quotes__c, Code__c,'+
            'End_Date__c, Fees_Language_Changes__c, Fees_Language_Changes_Text__c, Opportunity_Value__c, Request_Contract_Change__c,'+
            'Request_Contract_Change_Text__c, Start_Date__c, Status__c FROM Quote_Promotion__c '; 
        
        if (statusFilter != 'All') {
            queryWhere = buildWhere(queryWhere, ''+string.escapeSingleQuotes('Status__c')+' = \''+string.escapeSingleQuotes(statusFilter)+'\'');
        }
        if (saveSearchName != null) {
            queryWhere = buildWhere(queryWhere, ' (Name LIKE \'%' + String.escapeSingleQuotes(SaveSearchName) + '%\'');
            queryWhere += ' OR Code__c LIKE \'%' + String.escapeSingleQuotes(SaveSearchName) + '%\')';
        }
        queryPromo += queryWhere;
        queryPromo += ' ORDER BY '+string.escapeSingleQuotes(sortField)+' '+string.escapeSingleQuotes(SortDirection) + ' LIMIT 10000';
        system.debug('queryPromo:' + queryPromo);
    }
    
    public string buildWhere(string qw, string cond) {
        if (qw == '') {
            return ' WHERE ' + cond;
        } else {
            return qw + ' AND ' + cond;
        }
    }
    
    public string sortDirection {
        get { 
            if (sortDirection == null) { 
                sortDirection = 'asc'; 
            } 
            return SortDirection;  
        }
        set;
    }
    
    public string sortField {
        get { 
            if (sortField == null) {
                sortField = 'Name'; 
            } 
            return sortField; 
        }
        set; 
    }
    
    public void sortToggle() {
        SortDirection = SortDirection.equals('asc') ? 'desc NULLS LAST' : 'asc';
        if (SortFieldSave != SortField) {
            SortDirection = 'asc';
            statusFilter = 'All';
            SortFieldSave = SortField;
        }
        BuildQuery();
    }
    
    public class promoWrapper {
        public Quote_Promotion__c promo {get;set;}
        
        public promoWrapper(Quote_Promotion__c xPromo) {
            promo = xPromo;
        }
    }
    
    public class quoteAppWrapper {
        public id itemId {get;set;}
        public string quoteName {get;set;}
        public string quoteProdName {get;set;}
        public string ownerName {get;set;}
        public string acctName {get;set;}
        public string appLink {get;set;}
        
        public quoteAppWrapper(id xId, string xQuoteName, string xQuoteProdName, string xOwnerName, string xAcctName, string xAppLink) {
            itemId = xId;
            quoteName = xQuoteName;
          	quoteProdName = xQuoteProdName;
            ownerName = xOwnerName;
            acctName = xAcctName;
            if (xAppLink != null) {
                appLink = xAppLink;
            } else {
                appLink = '/'+xId;
            }
            
        }
    }
    
}