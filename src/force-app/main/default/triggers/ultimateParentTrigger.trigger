trigger ultimateParentTrigger on Account (after insert, after update) {
    
    //list to hold upa records
    Ultimate_Parent_Accounts__c[] upas = new List<Ultimate_Parent_Accounts__c>();
    
    //if insert
    if (trigger.isInsert) {
        for (integer i=0; i<trigger.new.size(); i++) {
            if (trigger.new[i].ParentId != null) {
                upas.add(new Ultimate_Parent_Accounts__c(Name = trigger.new[i].id, Account_Name__c = trigger.new[i].Name, AccountId__c = trigger.new[i].id, Date__c = datetime.now()));
            }
        }
        
        //if update
    } else {
        for (integer i=0; i<trigger.new.size(); i++) {
            if (trigger.old[i].ParentId != trigger.new[i].ParentId) {
                upas.add(new Ultimate_Parent_Accounts__c(Name = trigger.new[i].id, Account_Name__c = trigger.new[i].Name, AccountId__c = trigger.new[i].id, Date__c = datetime.now()));
            }
        }
    }
    
    if (upas.size()>0) {
        upsert upas AccountId__c;
    }
    
}