public class marketoWebServices {
    /**
* This Class Is Used to Communicate with the Marketo API
* Currently this is used by the Product of Interest Program
**/
    
    //Method to get the Marketo Activities
    public static string marketoData(string marketoObject, string activityIDs){
        //Was the Request a Success
        boolean success;
        //Base of the Endpoint URL
        string endPointURL ='https://907-JRM-499.mktorest.com/rest/v1/';
        //Data Retrivied
        string marketoJSONData;
        //Get the Auth
        string authToken = getAuthToken();
        //Next Page Token
        string nextPageToken;
        //Paging Token
        string pagingToken;
        //SF Log ID
        id sfLogID;
        //Get Next Page Token Stored in SF Log
        Salesforce_Log__c[] sfl = [Select ID, Log__c from Salesforce_Log__c where Class__c='NextPageToken' limit 1];
        if(sfl.size() >0){
           pagingToken = sfl[0].log__c;
           sfLogID = sfl[0].ID;
        } else {
            pagingToken = getPagingToken(authToken);
        }
        
        //Building the Endpoint URL
        //Add the Marketo Object
        if(marketoObject != NULL){
            endPointURL = endPointURL + marketoObject +'.json?';
        } else {
            salesforceLog.createLog('POI',TRUE,'marketoWebServices','marketoData','No Marketo Object Identified');
        }
        //Add the Auth Token
        if(test.isRunningTest()) {
            authToken ='123456789';
        } else {
            if(authToken != NULL){
            endPointURL = endPointURL + 'access_token=' + authToken;
            } else {
                salesforceLog.createLog('POI',TRUE,'marketoWebServices','marketoData','No Auth Token');
            }
        }
        //Add the Paging Token
        if(pagingToken !=NULL){
            endPointURL = endPointURL + '&nextPageToken='+pagingToken;
        } else {
            salesforceLog.createLog('POI',TRUE,'marketoWebServices','marketoData','No Page Token');
        }
        system.debug('pagingToken'+pagingToken);
        //Add the Activity IDs if there are any
        if(activityIDs != NULL){
            endPointURL = endPointURL + '&activityTypeIds='+activityIDs+'&batchSize=300';
        }
        
        //Init the HTTP Class
        Http http = new Http();
        
        //Init the HTTP Request Class
        HttpRequest req = new HttpRequest();
        
        //Setting the Endpoint
        req.setEndpoint(endPointURL);
        req.setTimeout(45000);
        system.debug('EndPoint'+req.getEndpoint());
        //Since we are getting data, we set the method to Get
        req.setMethod('GET');
        
        //Send the Request
        HttpResponse res = http.send(req);
        
        // If the request is successful, parse the JSON response.
        if(res.getStatusCode() == 200) {
            //Check the Sucess of the Request
            success = jsonUtility.marketoErrors(res.getBody(),'marketoWebServices','marketoData');
            if (test.isRunningTest()) {
                success = true;
            }
            //If the request was a success
            if(success){
                //Pass the JSON Body to the JSON Parser
                marketoJSONData = res.getBody();
                //Write the JSON to the Log
                salesforceLog.createLog('POI',FALSE,'marketoWebServices','marketoData',marketoJSONData);
                //Get the Next Page Token
                nextPageToken = jsonUtility.marketoMoreResults(res.getBody(),'marketoWebServices','marketoData');
            }  
        } else {
            //If there was an error, write to the Salesforce Log
            salesforceLog.createLog('POI',TRUE,'marketoWebServices','marketoData',res.getBody());
        }
        if(nextPageToken !=NULL){
            if(sfLogID != NULL){
                sfl[0].Log__c = nextPageToken;
                update sfl;
            } else {
                 salesforceLog.createLog('POI',FALSE,'NextPageToken','marketoData',nextPageToken);
            }
        }
        return marketoJSONData;
    }
    
    //Request for Authorization
    public static string getAuthToken() {
        //Base of the Endpoint URL
        string endPointURL ='https://907-JRM-499.mktorest.com/';
        String authToken;
        
        //Init the HTTP Class
        Http http = new Http();
        
        //Init the HTTP Request Class
        HttpRequest req = new HttpRequest();
        
        //Set the Endpoint, Client ID & Client Secret are provided by Marketo in the Admin Section
        req.setEndpoint(endPointURL+ 'identity/oauth/token?grant_type=client_credentials&client_id=d60dfbcd-f660-4ad7-95c3-7ca47f18bb2d&client_secret=VVEZEHyxHOIApZsl4tFbLGWNtAJMVLFM');
        
        //Since we are Getting a Token, We set the method to Get
        req.setMethod('GET');
        
        //Send the Request
        HttpResponse res = http.send(req);
        
        // If the request is successful, parse the JSON response.
        if(res.getStatusCode() == 200) {
            //Pass the JSON Body to the JSON Parser
            authToken = jsonUtility.tokenParser(res.getBody(), 'access_token');
        } else {
            //If there was an error, write to the Salesforce Log
            salesforceLog.createLog('POI',TRUE,'marketoWebServices','getAuthToken',res.getBody());
        }
        return authToken;
    }
    
    //get the paging token
    public static string getPagingToken(string authToken) {
        //Base of the Endpoint URL
        string endPointURL ='https://907-JRM-499.mktorest.com/rest/v1/';
        String pagingToken;
        //We only want activities that happend in the last 15 mins we subtract 15 minutes from now
        string[] dt = string.valueOfGMT(datetime.now().addMinutes(-15)).split(' ');
        string[] ts = dt[1].split(':');
        string sinceDateTime = dt[0]+'T'+ts[0]+':'+ts[1]+':00Z';
        boolean success;
        //Init the HTTP Class
        Http http = new Http();
        
        //Init the HTTP Request Class
        HttpRequest req = new HttpRequest();
        
        //Set the Endpoint, Client ID & Client Secret are provided by Marketo in the Admin Section
        req.setEndpoint(endPointURL+'activities/pagingtoken.json?access_token='+ authToken+'&sinceDatetime='+sinceDatetime);
        // req.setTimeout(45000);
        //Since we are Getting a Token, We set the method to Get
        req.setMethod('GET');
        
        //Send the Request
        HttpResponse res = http.send(req);
        // If the request is successful, parse the JSON response.
        if(res.getStatusCode() == 200) {
            success = jsonUtility.marketoErrors(res.getBody(),'marketoWebServices','getPagingToken');
            if(success == TRUE){
                //Pass the JSON Body to the JSON Parser
                pagingToken = jsonUtility.tokenParser(res.getBody(), 'nextPageToken');
            }
        } else {
            //If there was an error, write to the Salesforce Log
            salesforceLog.createLog('POI',TRUE,'marketoWebServices','getPagingToken',res.getBody());
        }
        return pagingToken;
    }
}