public class proposalSystemRequirements {
    
    public List<Quote_Product__c> quoteProds {get;set;}
    public Id quoteId {get;set;}
    public Quote__c quote {get;set;}
    public String quoteProductQuery {get;set;}
    public List<productCategoryWrapper> productCategoryWrappers {get;set;}
    public Set<String> prodCategories {get;set;}
    public List<Proposal_System_Requirements__mdt> psr {get;set;}
    
    public proposalSystemRequirements(){
        
        productCategoryWrappers = new List<productCategoryWrapper>();
        
        quoteId = ApexPages.currentPage().getParameters().get('id');
        
        quote = [SELECT Quote_Options__c, Id FROM Quote__c WHERE Id = :quoteId];
        
        queryQuoteProducts();
        
        prodCategories = new Set<String>();
        
        //list of Product Categories of products to filter proposal system requirements
        for(Quote_Product__c qp: quoteProds){
            prodCategories.add(qp.Product__r.Proposal_System_Requirements_Category__c);
        }
        
        psr = [SELECT Category__c, Product_Display__c, Sort_Order__c, Static_Product_Display__c, System_Requirements__c
               FROM Proposal_System_Requirements__mdt WHERE Category__c IN :prodCategories ORDER BY Sort_Order__c ASC];
        
        createCategoryWrappers();
        
    }
    
    public class productCategoryWrapper{
        
        public String productPrintName{get;set;}
        public String productCategory {get;set;}
        
        public productCategoryWrapper(String xProductPrintName, String xProductCategory){
            
            productPrintName = xProductPrintName;
            productCategory = xProductCategory;
            
        }
        
    } 
    
    public void queryQuoteProducts(){
        
        quoteProductQuery = 'SELECT Product_Print_Name__c, Product__r.Proposal_System_Requirements_Category__c FROM Quote_Product__c WHERE Product__r.Proposal_System_Requirements_Category__c != null';
        
        if(quote.Quote_Options__c != null){
            quoteProductQuery += ' AND Quote__r.Quote_Options__c =\''+quote.Quote_Options__c+'\' AND Quote__r.Include_Option_In_Proposal__c = TRUE';
        }else{
            quoteProductQuery += ' AND Quote__c = :QuoteId';
        }
        
        quoteProductQuery += ' ORDER BY Product_Print_Name__c ASC';
        
        quoteProds = database.query(quoteProductQuery);
        
    }
    
    public void createCategoryWrappers(){
        
        for(Quote_Product__c qp: quoteProds){
            
            boolean wrapperExists = false;
            
            for(productCategoryWrapper pcw: productCategoryWrappers){
                
                //if a quote product matches with a product wrapper already added, mark it as already existing
                if(qp.product_Print_Name__c == pcw.productPrintName && qp.Product__r.Proposal_System_Requirements_Category__c == pcw.productCategory){
                    wrapperExists = true;
                }
            }
           
            //if there isn't a wrapper with particular Quote Product added already
            if(wrapperExists == false){
                
                productCategoryWrappers.add(new productCategoryWrapper(qp.Product_Print_Name__c, qp.Product__r.Proposal_System_Requirements_Category__c));
                
            }
            
            
        }
        
    }
    
}