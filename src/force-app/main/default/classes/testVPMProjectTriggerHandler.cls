@isTest(seeAllData = true)
public class testVPMProjectTriggerHandler {
    static testMethod void test1(){
         user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1];  
        Account a = new account();
        a.name = 'test1';
        a.OwnerId = Dan.Id;
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'John';
        c.LastName = 'Parker';
        c.AccountId = a.id;
        c.EHS_Contact__c = false;
        insert c;
        
        VPM_Project__c vProject = new VPM_Project__c();
        vProject.Account__c = a.Id;
        vProject.CurrencyIsoCode = 'USD';
        vProject.Scheduled_Upgrade_Date__c = Date.newInstance(2019, 10, 09);
        insert vProject;
        
        VPM_Milestone__c vpmMilestone = new VPM_Milestone__c();
        vpmMilestone.Account__c = a.Id;
        vpmMilestone.Contact__c = c.Id;
        vpmMilestone.VPM_Project__c = vProject.Id;
        //vpmMilestone.OwnerId = 
        vpmMilestone.CurrencyIsoCode = 'USD';
        insert vpmMilestone;
        
        VPM_Task__c vpmtask = new VPM_Task__c();
        vpmtask.Account__c = a.Id;
        vpmtask.OwnerId = Dan.Id;
        vpmtask.VPM_Milestone__c = vpmMilestone.Id;
        vpmtask.Email_Sequence_Number__c = 1;
        vpmtask.End_Date__c = null;
        Insert vpmtask;
        
        VPM_Task__c vpmtask1 = new VPM_Task__c();
        vpmtask1.Account__c = a.Id;
        vpmtask1.OwnerId = Dan.Id;
        vpmtask1.VPM_Milestone__c = vpmMilestone.Id;
        vpmtask1.Email_Sequence_Number__c = 2;
        vpmtask1.End_Date__c = null;
        Insert vpmtask1;
        
        VPM_Task__c vpmtask2 = new VPM_Task__c();
        vpmtask2.Account__c = a.Id;
        vpmtask2.OwnerId = Dan.Id;
        vpmtask2.VPM_Milestone__c = vpmMilestone.Id;
        vpmtask2.Email_Sequence_Number__c = 3;
        vpmtask2.End_Date__c = null;
        Insert vpmtask2;
        
        
        vProject.Scheduled_Upgrade_Date__c = date.today();
        update vProject;        
    }
}