@isTest
private class testCloneQuote {
    static testMethod void test1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
        
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c ='01t80000002NON4';
        myItem.Quote__c = newQuote.ID;
        insert myItem;
        
        
        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        cloneQuote myController = new cloneQuote(testController);
        mycontroller.createQuote(); 
    }
}