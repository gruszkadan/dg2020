trigger surveyTrigger on Survey_Feedback__c 
(before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            surveyTriggerHandler.beforeInsert(trigger.new);
        }
        if (trigger.isUpdate) {
            surveyTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
    }
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            surveyTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        if (trigger.isUpdate) {
            surveyTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
    }
}