/*
 * This class is scheduled to run every night and clear out account NPS scores that are 12+ months old
 */
global class accountNPSScoreBatchable implements Database.batchable<sObject>, Database.stateful {
    integer numAcctsFound = 0;
    string acctsFound = '';
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        string q = 'SELECT id, Name, Chemical_Management_NPS_Score__c, Chemical_Management_NPS_Last_Survey_Date__c ' +
            'FROM Account WHERE Chemical_Management_NPS_Last_Survey_Date__c != null AND Chemical_Management_NPS_Last_Survey_Date__c < LAST_N_WEEKS:56';
        if (test.isRunningTest()) {
            q += ' LIMIT 100';
        } 
        return Database.getQueryLocator(q);
    }  
    
    global void execute(Database.BatchableContext bc, list<sObject> batch) {
        Account[] accts = (list<Account>)batch;
        for (Account acct : accts) {
            acctsFound += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• ' + acct.Name + ' (<a href="https://login.salesforce.com/' + acct.id + '">' + acct.id + '</a>)<br/>';
            acct.Chemical_Management_Prev_NPS_Score__c = acct.Chemical_Management_NPS_Score__c;
            acct.Chemical_Management_NPS_Score__c = null;
            acct.Chemical_Management_NPS_Ranking__c = null;
            acct.Chemical_Management_NPS_Last_Survey_Date__c = null;
        } 
        update accts;
        numAcctsFound = accts.size();
    }
    
    global void finish(Database.BatchableContext bc) { 
        AsyncApexJob job = [SELECT id, ApexClassId, JobItemsProcessed, TotalJobItems, NumberOfErrors, ExtendedStatus, CreatedBy.Email
                            FROM AsyncApexJob
                            WHERE id = : bc.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(finish_toAddresses());
        mail.setReplyTo('salesforceops@ehs.com');   
        mail.setSubject('Account NPS Score Scheduler - ' + finish_theDate());
        mail.setHtmlBody(finish_htmlBody(job));
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }
    
    // HTML body for the results email
    global string finish_htmlBody(AsyncApexJob job) {
        string s = '';
        //title and subtitle
        s += '<span style="font-size:20px;font-weight:bold;">ACCOUNT NPS SCORE SCHEDULER - ' + finish_theDate() + '</span><br/>' +
            '<table><tr><td>&nbsp;&nbsp;&nbsp;</td><td><span style="font-size:11pt;color:gray;font-style:italic;">' +
            'Every night this logic runs to clear out any accounts with a NPS score greater than 12 months old- the following accounts have fit this criteria and the NPS scores have been reset' +
            '</span></td></tr></table><br/>';
        //accounts found
        s += '<b># Accounts: </b>' + numAcctsFound + '<br/>' + acctsFound + '<br/>';
        //errors
        s += '<hr/><br/><b># ERRORS: </b>' + job.NumberOfErrors + '<br/>';
        if (job.ExtendedStatus != null) {
            s += '<b>Error Message: </b>' + job.ExtendedStatus;
        }
        return s;
    }
    
    // List of emails to send the results to
    global string[] finish_toAddresses() {
        string[] toAddresses = new list<string>();
        set<id> setupOwnerIds = new set<id>();
        for (Survey_Settings__c ss : [SELECT SetupOwnerId FROM Survey_Settings__c WHERE Receive_NPS_Scheduler_Email__c = true]) {
            if (string.valueOf(ss.SetupOwnerId).left(3) == '005') {
                setupOwnerIds.add(ss.SetupOwnerId);
            }
        }
        if (setupOwnerIds.size() > 0) {
            for (User u : [SELECT id, Email FROM User WHERE id IN : setupOwnerIds AND Email != null]) {
                toAddresses.add(u.Email);
            }
        } else {
            toAddresses.add('salesforceops@ehs.com');
        }
        return toAddresses;
    }
    
    // Format the current date
    global string finish_theDate() {
        string[] d = string.valueOf(date.today()).split('-');
        return d[1].removeStart('0') + '/' + d[2].removeStart('0') + '/' + d[0].removeStart('0');
    }
    
}