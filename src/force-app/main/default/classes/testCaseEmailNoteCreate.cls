@isTest(seeAllData=true)
private class testCaseEmailNoteCreate {
    
    static testMethod void caseEmailNoteCreate_test1() {
        
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        //test case
        Case newCase = new Case();
        newCase.AccountId = newAcct.id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Type = 'CC Support';
        newCase.Subject = 'Test';
        newCase.Num_of_Case_Issues__c = 1;
        newCase.Origin = 'Email';
        newCase.OwnerID = [SELECT id 
                           FROM User 
                           WHERE LastName = 'Werner' 
                           LIMIT 1].id;  
        insert newCase;
        
        //test issue
        Case_Issue__c newIssue = new Case_Issue__c();
        newIssue.Associated_Case__c = newCase.id;
        newIssue.Product_In_Use__c = 'GM';
        newIssue.Product_Support_Issue__c = 'GM Support Issue';
        newIssue.Product_Support_Issue_Locations__c = 'GM Support Location';
        insert newIssue;
        
        //test emails
        EmailMessage[] emails = new List<EmailMessage>();
        for (integer i=0; i<2; i++) {
            EmailMessage newEmail = new EmailMessage();
            newEmail.ParentId = newCase.id;
            newEmail.TextBody = 'test text';
            if (i == 0) {
                newEmail.Incoming = true;
            } else {
                newEmail.Incoming = false;
            }
            emails.add(newEmail);
        }
        insert emails;
        
        //query the notes that were created
        Case_Notes__c[] notes = [SELECT id, Full_Email_Text__c 
                                 FROM Case_Notes__c 
                                 WHERE Case__c = :newCase.id];
        
        //assert that 2 notes where created
        SYSTEM.assert(notes.size() == 2, 'Expected = 2, Actual = '+notes.size());
        
        //assert the notes have the correct content
        for (integer i=0; i<2; i++) {
            SYSTEM.assert(notes[i].Full_Email_Text__c == emails[i].TextBody, 'Case note full email text does not equal the email text');
        }
           
        
        
        
        
        
        
        
    }
    
    
    
    
    
}