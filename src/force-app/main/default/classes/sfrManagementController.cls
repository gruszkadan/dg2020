public class sfrManagementController {
    public string pagename {get;set;}
    
    public sfrManagementController() {
        pagename = 'Requests To Be Assigned';
    }
    
    public void gotoEnhancements() {
        pagename = 'Enhancements';
    }
    
    public void gotoProjects() {
        pagename = 'Projects';
    }
    
    public void gotoRequestsTBA() {
        pagename = 'Requests To Be Assigned';
    }

}