/*
* Test class: testOrderItemUtility
*/
public class orderItemUtility {
    
    //Find the Account the Order Item is Associated with
    public static void findAccount(Order_Item__c[] orderItems){
        set<string> adminIds = new set<string>();
        
        //Add Order Item AccountIDs to Set
        for(Order_Item__c oi : orderItems){
            if(oi.AccountID__c != NULL){
                adminIDs.add(oi.AccountID__c);
            }
        }
        //Query the Account where the AdminID is in the AdminID Set
        Account[] accounts = [Select ID, AdminID__c from Account where AdminID__c in :adminIds];
        Map<String,Account> acctMap = new Map<String, Account>();
        for(Account a : accounts) {
            acctMap.put(a.AdminID__c, a);
            
        }
        //Set to Hold Order Item IDs that didnt find an Account
        set<String> orderItemIDs = new set<String>();
        for(Order_Item__c oi : orderItems){
            if(oi.AccountID__c != NULL){
                Account acct = acctMap.get(oi.AccountID__c);
                if(acct != NULL){
                    oi.Account__c = acct.Id;
                } else {
                    orderItemIDs.add(oi.Order_Item_ID__c);
                }
            }
            //if(orderItemIds.size()>0){
            //  salesforceLog.createLog('Order Item',TRUE,'orderItemUtility','findAccount','Couldnt Match Accounts on OrderIDs: '+orderItemIds);
            //}
        }
    }
    
    //Find Sales Rep, Sales Director, Sales Manager & Sales VP for the order
    public static void findSalesUsers(Order_Item__c[] orderItems){
        //Set to Hold all the Order Item Rep Names to Query
        set<string> repNames = new set<string>();
        //Set to Hold all the Order Item IDs that couldnt find a Rep
        set<string> orderIDsMissingReps = new set<string>();
        Order_Item__c[] orderItemsWithReps = new list <Order_Item__c>();
        
        //Add Order Item Sales Reps to Set
        for(Order_Item__c oi : orderItems){
            if(oi.Admin_Tool_Sales_Rep__c != NULL){
                repNames.add(oi.Admin_Tool_Sales_Rep__c);
            }
        }
        
        //Query the Users to find the ID of the Sales Rep, Manager, Director and VP
        User[] users = [Select ID, LastName, Group__c, Rep_Name_in_Admin_Tool__c, Sales_Performance_Manager_ID__c, Sales_Director__c, Sales_VP__c, Department__c, ForecastEnabled
                        from User where Rep_Name_in_Admin_Tool__c in :repNames];
        User house = [Select ID, Rep_Name_in_Admin_Tool__c, Sales_Performance_Manager_ID__c, Sales_Director__c, Sales_VP__c from User where Name='House Sales'];
        User customerCare = [Select ID, Rep_Name_in_Admin_Tool__c, Sales_Performance_Manager_ID__c, Sales_Director__c, Sales_VP__c from User where Name='Customer Care Sales']; 
        //User ehsSales = [Select ID, Rep_Name_in_Admin_Tool__c, Sales_Performance_Manager_ID__c, Sales_Director__c, Sales_VP__c from User where Name='EHS Sales']; 
        Map<String,User> userMap = new Map<String, User>();
        for(User u : users) {
            userMap.put(u.Rep_Name_in_Admin_Tool__c, u);
        }system.debug('userMapSize'+userMap.size());
        
        for(Order_Item__c oi : orderItems){
            if(oi.Admin_Tool_Sales_Rep__c != NULL){
                User rep = userMap.get(oi.Admin_Tool_Sales_Rep__c);
                if(rep != NULL){
                    if(rep.Department__c=='Sales'){
                       /* if(rep.Group__c=='EHS Mid-Market Sales' || rep.Group__c=='EHS Enterprise Sales'){
                            oi.OwnerID = ehsSales.Id;
                            oi.Sales_Rep__c = ehsSales.Id;
                            oi.Sales_Manager__c = ehsSales.Sales_Performance_Manager_ID__c;
                            oi.Sales_Director__c = ehsSales.Sales_Director__c;
                            oi.Sales_VP__c = ehsSales.Sales_VP__c;
                        }*/
                        if(rep.ForecastEnabled==TRUE){
                            oi.OwnerID = rep.Id;
                            oi.Sales_Rep__c = rep.Id;
                            oi.Sales_Manager__c = rep.Sales_Performance_Manager_ID__c;
                            oi.Sales_Director__c = rep.Sales_Director__c;
                            oi.Sales_VP__c = rep.Sales_VP__c;
                        } else {
                            oi.OwnerID = house.id;
                            oi.Sales_Rep__c = house.id;
                            oi.Sales_Manager__c = house.Sales_Performance_Manager_ID__c;
                            oi.Sales_Director__c = house.Sales_Director__c;
                            oi.Sales_VP__c = house.Sales_VP__c;
                        }
                    } else if (rep.Department__c=='Customer Care'){
                        oi.OwnerId = customerCare.Id;
                        oi.Sales_Rep__c = customerCare.Id;
                        oi.Sales_Manager__c = customerCare.Sales_Performance_Manager_ID__c;
                        oi.Sales_Director__c = customerCare.Sales_Director__c;
                        oi.Sales_VP__c = customerCare.Sales_VP__c;
                    } else {
                        oi.OwnerID = house.id;
                        oi.Sales_Rep__c = house.id;
                        oi.Sales_Manager__c = house.Sales_Performance_Manager_ID__c;
                        oi.Sales_Director__c = house.Sales_Director__c;
                        oi.Sales_VP__c = house.Sales_VP__c;
                    }
                } else {
                    oi.OwnerID = house.id;
                    oi.Sales_Rep__c = house.id;
                    oi.Sales_Manager__c = house.Sales_Performance_Manager_ID__c;
                    oi.Sales_Director__c = house.Sales_Director__c;
                    oi.Sales_VP__c = house.Sales_VP__c;
                }
            }
            orderItemsWithReps.add(oi);
        }
        if(orderIDsMissingReps.size()>0){
            salesforceLog.createLog('Order Item',TRUE,'orderItemUtility','findSalesUsers','Couldnt Match Sales Reps on OrderIDs: '+orderIDsMissingReps);
        }
    }
    
    //Transform Admin Tool Data to Salesforce Data (Fields: Admin_Tool_Order_Status__c, Name)
    public static void transformAdminToolData(Order_Item__c[] orderItems){
        AT_Product_Mapping__c[] productMapping = [SELECT Admin_Tool_Product_Name__c, Category__c, Salesforce_Product_Name__c, Automatic_Renewal_Creation__c, Renewal_Count_In_Stats__c, 
                                                  Subscription_Based__c, Version_Field__c, Product_Platform__c, Display_on_Account__c, Admin_Tool_Product_Type__c,
                                                  Product_Suite__c, Product_Platform_Display_Name__c, Category_Display_Name__c, Product2Id__c
                                                  FROM AT_Product_Mapping__c];
        set<string> productMappingErrors = new set<string>();
        //Loop Over the Order Items
        for(Order_Item__c oi : orderItems){
            //Order Status
            if(oi.Admin_Tool_Order_Status__c=='A') {
                oi.Status__c ='Active';
            } else if (oi.Admin_Tool_Order_Status__c=='B') {
                oi.Status__c ='Done';
            } else if (oi.Admin_Tool_Order_Status__c=='C') {
                oi.Status__c ='Cancelled';
            } else if (oi.Admin_Tool_Order_Status__c=='D') {
                oi.Status__c ='Deleted';
            } else if (oi.Admin_Tool_Order_Status__c=='V') {
                oi.Status__c ='Void';
            }
            //Name
            //if(oi.Order_Item_ID__c != NULL && !oi.name.contains('H-')){
            if(oi.Order_Item_ID__c != NULL && oi.name != NULL && !oi.name.contains('H-') || (oi.Order_Item_ID__c != NULL && oi.name == NULL)){
               oi.Name = oi.Order_Item_ID__c;
            }

            //Product Mapping Fields
            for(AT_Product_Mapping__c pm:productMapping){
                if(pm.Admin_Tool_Product_Name__c == oi.Admin_Tool_Product_Name__c &&
                   pm.Product_Platform__c == oi.Product_Platform__c){
                       if(oi.Admin_Tool_Product_Type__c == null && oi.Implementation_Model__c != null){
                           if(pm.Admin_Tool_Product_Type__c == oi.Implementation_Model__c){
                               oi.Salesforce_Product_Name__c = pm.Salesforce_Product_Name__c;
                               oi.Category__c = pm.Category__c;
                               oi.Subscription_Based__c = pm.Subscription_Based__c;
                               oi.Display_on_Account__c = pm.Display_on_Account__c;
                               oi.Product_Suite__c = pm.Product_Suite__c;
                               oi.Version_Field__c = pm.Version_Field__c;
                               oi.Product_Platform_Display_Name__c = pm.Product_Platform_Display_Name__c;
                               oi.Category_Display_Name__c = pm.Category_Display_Name__c;
                               oi.Automatic_Renewal_Creation__c = pm.Automatic_Renewal_Creation__c;
                               oi.Renewal_Count_In_Stats__c = pm.Renewal_Count_In_Stats__c;
                               oi.Product__c = pm.Product2Id__c;
                           }
                       }else if(pm.Admin_Tool_Product_Type__c == oi.Admin_Tool_Product_Type__c){
                           oi.Salesforce_Product_Name__c = pm.Salesforce_Product_Name__c;
                           oi.Category__c = pm.Category__c;
                           oi.Subscription_Based__c = pm.Subscription_Based__c;
                           oi.Display_on_Account__c = pm.Display_on_Account__c;
                           oi.Product_Suite__c = pm.Product_Suite__c;
                           oi.Version_Field__c = pm.Version_Field__c;
                           oi.Product_Platform_Display_Name__c = pm.Product_Platform_Display_Name__c;
                           oi.Category_Display_Name__c = pm.Category_Display_Name__c;
                           oi.Automatic_Renewal_Creation__c = pm.Automatic_Renewal_Creation__c;
                           oi.Renewal_Count_In_Stats__c = pm.Renewal_Count_In_Stats__c;
                           oi.Product__c = pm.Product2Id__c;
                       }
                   }
            }
            if(oi.Salesforce_Product_Name__c == null){
                productMappingErrors.add(oi.Admin_Tool_Product_Name__c);
            }
        }
        //Create a Log for any errors from the product mappings
        if(productMappingErrors.size() > 0){
            string errorString;
            for(String e:productMappingErrors){
                if(errorString == null){
                    errorString = e;
                }else{
                    errorString += ', '+e;
                }
            }
            salesforceLog.createLog('Order Item',TRUE,'orderItemUtility','transformAdminToolData','Could not find match for following Admin Tool Product Name(s): '+errorString);
        }
        
    }
    
    //Set the Subscription End Date on the Account where necessary
    public static void subscriptionEndDate(Order_Item__c[] orderItems){
        AT_Product_Mapping__c[] productMapping = [SELECT Admin_Tool_Product_Name__c, Category__c, Salesforce_Product_Name__c
                                                  FROM AT_Product_Mapping__c where Subscription_Based__c = true];
        Order_Item__c[] matchedOrderItems = new list<Order_Item__c>();
        set<id> accountIds = new set<id>();
        for(Order_Item__c oi : orderItems){
            for(AT_Product_Mapping__c pm:productMapping){
                if(oi.Admin_Tool_Product_Name__c == pm.Admin_Tool_Product_Name__c){
                    accountIds.add(oi.Account__c);
                    matchedOrderItems.add(oi);
                }
            }
        } 
        Account[] accounts = new list<Account>();
        if(accountIds.size() > 0){
            accounts = [SELECT id,Subscription_End_Date__c,Subscription_End_Date_EHS__c,Subscription_End_Date_Ergo__c from Account where id in :accountIds];
        }
        if(accounts.size() > 0){
            Account[] accountsToUpdate = new list<Account>();
            //Loop over the Accounts
            for(Account a : accounts){
                //Loop Over the Order Items to find matching one
                boolean needsUpdate = false;
                for(Order_Item__c oi : matchedOrderItems){
                    if(oi.Account__c == a.id){
                        //set the new subscription end date and add to update list if changed
                        integer month = integer.valueof(oi.Term_End_Date__c.Month());
                        integer day = integer.valueof(oi.Term_End_Date__c.Day());
                        string formattedMonth;
                        string formattedDay;
                        if(month < 10){
                            formattedMonth = '0'+string.valueof(month);
                        }else{
                            formattedMonth = string.valueof(month);
                        }
                        if(day < 10){
                            formattedDay = '0'+string.valueof(day);
                        }else{
                            formattedDay = string.valueof(day);
                        }
                        string formattedDate =  formattedMonth +'/' +formattedDay;
                        if(oi.Product_Platform__c == 'MSDSonline'){
                            if(a.Subscription_End_Date__c != formattedDate){
                                a.Subscription_End_Date__c = formattedDate;
                                needsUpdate = true;
                            }
                        }else if(oi.Product_Platform__c == 'EHS'){
                            if(a.Subscription_End_Date_EHS__c != formattedDate){
                                a.Subscription_End_Date_EHS__c = formattedDate;
                                needsUpdate = true;
                            }
                        }else if(oi.Product_Platform__c == 'Ergo'){
                            if(a.Subscription_End_Date_Ergo__c != formattedDate){
                                a.Subscription_End_Date_Ergo__c = formattedDate;
                                needsUpdate = true;
                            }
                        }
                    }
                }
                if(needsUpdate == true){
                    accountsToUpdate.add(a);
                }
            }
            if(accountsToUpdate.size() > 0){
                update accountsToUpdate;
            }
        }
        
    }
    
    //Set the Customer Since Date on the Account where necessary
    public static void customerSinceDate(Order_Item__c[] orderItems){
        set<id> accountIds = new set<id>();
        for(Order_Item__c oi : orderItems){
            accountIds.add(oi.Account__c);
        } 
        Account[] accounts = new list<Account>();
        if(accountIds.size() > 0){
            accounts = [SELECT id,Customer_Since__c from Account where id in :accountIds];
        }
        if(accounts.size() > 0){
            Account[] accountsToUpdate = new list<Account>();
            //Loop over the Accounts
            for(Account a : accounts){
                //Loop Over the Order Items to find matching one
                boolean needsUpdate = false;
                for(Order_Item__c oi : orderItems){
                    if(oi.Account__c == a.id){
                        //set the new Customer Since date and add to update list if it is less than the current value or if the current value is blank
                        if(a.Customer_Since__c == null || oi.Order_Date__c < a.Customer_Since__c){
                            a.Customer_Since__c = oi.Order_Date__c;
                            needsUpdate = true;
                        }
                    }
                }
                if(needsUpdate == true){
                    accountsToUpdate.add(a);
                }
            }
            if(accountsToUpdate.size() > 0){
                update accountsToUpdate;
            }
        }
    }
    
    //Set the Customer Annual Revenuw fields on the Account by product platform where necessary
    public static void customerAnnualRevenue(Order_Item__c[] orderItems){
        set<id> accountIds = new set<id>();
        for(Order_Item__c oi : orderItems){
            accountIds.add(oi.Account__c);
        }
        Account[] accounts = new list<Account>();
        
        // Query accounts and subquery related current active order items that have a renewal amount
        if(accountIds.size() > 0){
            accounts = [SELECT id, Customer_Annual_Revenue_EHS__c, Customer_Annual_Revenue__c, Customer_Annual_Revenue_Ergo__c,
                        (Select id, Product_Platform__c, Renewal_Amount__c from Order_Items__r where Term_Start_Date__c <= TODAY and Term_End_Date__c > TODAY
                         and Admin_Tool_Order_Status__c = 'A' and Renewal_Amount__c != NULL and Renewal_Amount__c > 0 and Product_Platform__c != null)
                        from Account where id in :accountIds];
        }
        if(accounts.size() > 0){
            Account[] accountsToUpdate = new list<Account>();
            //Loop over the Accounts
            for(Account a : accounts){
                //create decimal variables to count the the total annual revenue per product platform on the account
                decimal msdsRevenue = 0;
                decimal ehsRevenue = 0;
                decimal ergoRevenue = 0;
                //Loop Over the Order Items already on the account to count the annual revenue amounts for each platform
                if(a.Order_Items__r.size() > 0){
                    for(Order_Item__c oi : a.Order_Items__r){
                        if(oi.Product_Platform__c == 'MSDSonline'){
                            msdsRevenue = msdsRevenue + oi.Renewal_Amount__c;
                        }else if(oi.Product_Platform__c == 'EHS'){
                            ehsRevenue = ehsRevenue + oi.Renewal_Amount__c;
                        }else if(oi.Product_Platform__c == 'Ergo'){
                            ergoRevenue = ergoRevenue + oi.Renewal_Amount__c;
                        }
                    }
                    //set the new Customer Annual Revenue fields and add account to update list if changed
                    boolean needsUpdate = false;
                    if(a.Customer_Annual_Revenue__c != msdsRevenue){
                        a.Customer_Annual_Revenue__c = msdsRevenue;
                        needsUpdate = true;
                    }
                    if(a.Customer_Annual_Revenue_EHS__c != ehsRevenue){
                        a.Customer_Annual_Revenue_EHS__c = ehsRevenue;
                        needsUpdate = true;
                    }
                    if(a.Customer_Annual_Revenue_Ergo__c != ergoRevenue){
                        a.Customer_Annual_Revenue_Ergo__c = ergoRevenue;
                        needsUpdate = true;    
                    }
                    if(needsUpdate){
                        accountsToUpdate.add(a);
                    } 
                }
            }
            
            if(accountsToUpdate.size() > 0){
                update accountsToUpdate;
            }
        }
        
    }
    
    //If the invoice changes, mark the date
    public static void invoiceChange(Order_Item__c[] oldOrderItems, Order_Item__c[] newOrderItems) {
        for (integer i=0; i<newOrderItems.size(); i++) {
            newOrderItems[i].Invoice_Amount_Change_Date__c = date.today();
            newOrderItems[i].Previous_Invoice_Amount__c = oldOrderItems[i].Invoice_Amount__c;
            if(!newOrderItems[i].isRenewal__c){
                newOrderItems[i].OwnerId = [SELECT Id FROM Group WHERE Name = 'Order Item Management' and Type = 'Queue'].id;
            }
        }
    }
    
    //Finds the chemical management opportunity related to the order item
    public static void associateOpportunityFromContract(Order_Item__c[] orderItems){
        //create a set to hold all contract numbers from our order items
        set<String> contractNumbers = new set<String>();
        for(Order_Item__c oi:orderItems){
            contractNumbers.add(oi.Contract_Number__c.replace('RS','RS-'));
        }
        
        //Query contracts and all related opportunities (primary & additional)
        Contract__c[] contracts = [SELECT id,Name, Opportunity__c, Opportunity__r.Suite_of_Interest__c, Opportunity__r.CreatedDate,
                                   (SELECT id,Opportunity__r.Suite_of_Interest__c,Opportunity__r.CreatedDate FROM Additional_Opportunities__r WHERE Opportunity__r.Suite_of_Interest__c = 'MSDS Management' ORDER BY Opportunity__r.CreatedDate ASC)
                                   FROM Contract__c
                                   WHERE Name in:contractNumbers];
        
        for(Contract__c con:contracts){
            //variable to hold id of opportunity to associate to order items
            id orderItemOpportunityId;
            //if the primary opportunity on the contract is chemcial management then default this as the one for the OI
            if(con.Opportunity__r.Suite_of_Interest__c == 'MSDS Management'){
                orderItemOpportunityId = con.Opportunity__c;
            }
            //if there are additional opportunities
            if(con.Additional_Opportunities__r.size() > 0){
                //If the primary opp is non-Chem OR the first additional opp was created before the primary Chem opp
                if ((orderItemOpportunityId != null && con.Additional_Opportunities__r[0].Opportunity__r.CreatedDate < con.Opportunity__r.CreatedDate)
                    || orderItemOpportunityId == null)
                {
                    //Set the opportuinity id to the additional opp
                    orderItemOpportunityId = con.Additional_Opportunities__r[0].Opportunity__c;
                }
            }
            
            //Associate the applicable order items to the opportunity 
            for(Order_Item__c oi:orderItems){
                if(oi.Contract_Number__c == con.Name.remove('-')){
                    oi.Contract__c = con.id;
                    oi.Opportunity__c = orderItemOpportunityId;
                }
            }
        }
    }
     
}