global class backfillPrimarySolutionPOIBatchable implements Database.Batchable<SObject>, Database.Stateful {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (!test.isRunningTest()) {
            return Database.getQueryLocator([SELECT id, ContactId, Contact.Primary_Solution_Product_of_Interest__c, LeadId, Lead.Primary_Solution_Product_of_Interest__c, Primary_Solution_Product_of_Interest__c
                                             FROM CampaignMember]);
        } else {
            return Database.getQueryLocator([SELECT id, ContactId, Contact.Primary_Solution_Product_of_Interest__c, LeadId, Lead.Primary_Solution_Product_of_Interest__c, Primary_Solution_Product_of_Interest__c
                                             FROM CampaignMember LIMIT 100]);
        }   
    }
    
    global void execute(Database.BatchableContext bc, list<SObject> batch) {  
        for(CampaignMember cm : (list<CampaignMember>) batch) {
            if(cm.ContactId != null){
                cm.Primary_Solution_Product_of_Interest__c = cm.Contact.Primary_Solution_Product_of_Interest__c;
            }else{
                cm.Primary_Solution_Product_of_Interest__c = cm.Lead.Primary_Solution_Product_of_Interest__c;
            }
        }
        update batch;
    }    
    global void finish(Database.BatchableContext bc) {
        
    }
    
}