public class VPMMilestoneDetail {
    private ApexPages.StandardController controller {get; set;}
    public VPM_Milestone__c milestone {get;set;}
    public User u {get;set;}
    public id milestoneId {get;set;}
    public List<VPM_Task__c> mTasks {get;set;} 
    public List<VPM_Milestone__c> milestones {get;set;}
    public List<Contact> accountContacts {get;set;}
    public Map<ID, String> taskPhotos {get;set;}
    public string primaryContact {get;set; }
    public User Owner {get;set;}
    public boolean inlineEdit {get;set;}
    public VPM_Note__c[] notes {get;set;}
    public String noteFilter {get;set;}
    public String noteSortOrder {get;set;}
    public VPM_Note__c newNote {get;set;}
    public VPM_Note__c editNote {get;set;}
    public boolean noteError {get;set;}
    public VPM_Task__c selectedTask{get;set;}
    public string selRecordType {get;set;}
    public string selRecordId {get;set;}
    public boolean modalData {get;set;}
    
    public VPMMilestoneDetail(ApexPages.StandardController controller) { 
        this.controller = controller;
        milestoneId = ApexPages.currentPage().getParameters().get('id'); 
        u = [Select id, FullPhotoURL from User where id =: UserInfo.getUserId() LIMIT 1];
        queryMilestone();
        //query for quick nav
        milestones = [SELECT Name, Type__c, (SELECT Name, Subject__c FROM VPM_Tasks__r)  FROM VPM_Milestone__c WHERE VPM_Project__c = :milestone.VPM_Project__c ORDER BY Name ASC];
        mTasks = [SELECT Name, Status__c, Subject__c, Start_Date__c, Contact__c, Contact__r.Name, OwnerID, Owner.Name FROM VPM_Task__c WHERE VPM_Milestone__c = :milestoneID ORDER BY Name ASC];
        accountContacts = [SELECT Name, ID FROM Contact WHERE Account.ID = :milestone.Account__c];
        //005 = User IDs; Query the Picture for Owner Avatar
        if(string.valueof(milestone.OwnerId).startsWith('005')){
            Owner = [SELECT FullPhotoUrl FROM User WHERE Id = :milestone.OwnerID];
        }
        //Dans Additions
        modalData = false;
        noteSortOrder = 'DESC';
        queryNotes();
        newNote = new VPM_Note__c();
        editNote = new VPM_Note__c();
        userPictures();
    }
    
    //Query Object Fields
    public void queryMilestone() {
        string SObjectAPIName = 'VPM_Milestone__c';
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string query = 'SELECT '+commaSepratedFields+', VPM_Project__r.Name, VPM_Project__r.Type__c, Contact__r.Name FROM '+SObjectAPIName+' WHERE id = \''+milestoneId+'\' ';
       milestone = Database.query(query);
    }
    
    public void inputField(){
            inlineEdit = true;
        }
    
    
     //create list of contacts related to Project's account
    public String[] getContacts(){
        String[] contacts = new list<String>();   
        //loop through Account's contacts and and to list
        for(Contact c : accountContacts){
            contacts.add(c.name);
        }
        return contacts;
    }
    
    public PageReference saveMilestone(){
        //Loop through the Account's contacts; If a contact's name is equal to the contact selected in the select list
        //set the project's primary contact name field to that contact's ID
        for(Contact c: accountContacts){
            if(c.Name == primaryContact){
                milestone.Contact__c = c.id;
            }
        }
        update milestone;
        //redirect page
        PageReference pr = new PageReference('/apex/vpm_milestone_detail');
        pr.setRedirect(true);
        pr.getParameters().put('id',milestone.Id);
        return pr;
    }
    
    //Dans Additions
     public void queryNotes(){
        string query = 'SELECT id, Comments__c, Case_Note__c, CreatedBy.FullPhotoURL, Contact__c, Contact__r.Name, Type__c, CreatedDate, CreatedById, CreatedBy.Name, VPM_Project__c, VPM_Project__r.Name, VPM_Milestone__c, VPM_Milestone__r.Name, VPM_Task__c, VPM_Task__r.Name '
            +'FROM VPM_Note__c ' 
            +'WHERE VPM_Milestone__c =\''+milestoneId+'\'';
        if(selRecordType != null && selRecordType != ''){
            query+= 'AND '+selRecordType+' =\''+selRecordId+'\'';
        }
        if(noteFilter != '' && noteFilter == 'My Notes'){
            query+= 'AND CreatedById  =\''+u.id+'\'';
        }
        query+= 'ORDER BY CreatedDate '+noteSortOrder+' ';
        notes = database.query(query);
    }
    
    //Find the note to be edited
    public void selectNoteToEdit(){
        String editNoteId = ApexPages.currentPage().getParameters().get('editNoteId'); 
        queryNotes();
        for(VPM_Note__c n:notes){
            if(n.id == editNoteId){
                editNote = n;
            }
        }
    }
    
    //Save the edited note
    public void saveEditedNote(){
        if(editNote != null){
            update editNote;
            queryNotes();
        }
    }
    
    //Query the milestone or task that was selected
    public void querySelectedRecord(){
        selectedTask = null;
        selRecordType = ApexPages.currentPage().getParameters().get('selRecordType'); 
        selRecordId = ApexPages.currentPage().getParameters().get('selRecordId'); 
        string SObjectAPIName = selRecordType;
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string query = 'SELECT '+commaSepratedFields+' FROM '+SObjectAPIName+' WHERE id = \''+selRecordId+'\' ';
        if(selRecordType == 'VPM_Task__c'){
            selectedTask = Database.query(query);
        }
        queryNotes();
    }
    
    public void addNote(){
        noteError = false;
        if(newNote.Type__c != null && newNote.Comments__c != ''){
            newNote.VPM_Project__c = milestone.VPM_Project__c;
            newNote.VPM_Milestone__c = milestone.id;
            newNote.Contact__c = milestone.Contact__c;
            if(selectedTask != null){
                newNote.VPM_Task__c = selectedTask.Id;
                newNote.Contact__c = selectedTask.Contact__c;
            }
            insert newNote;
            newNote = new VPM_Note__c();
        }else{
            noteError = true;
        }
        queryNotes();
    }
    
    public void resetRightPanelView(){
        selRecordType = null;
        selRecordId = null;
        selectedTask = null;
        queryNotes();
    }
    
       public void userPictures(){
        
        //Create Set of OwnerIds for tasks
        Set<ID> taskOwners = new Set<ID>();
        
        //Populate Set with OwnerIds
        for(VPM_Task__c t : mTasks){
            taskOwners.add(t.OwnerId);
        }
        
        //Populate map of User IDs and FullPhotoUrls matching the corresponding OwnerIds
        taskPhotos = new map<Id, String>();
        

        for(User u : [SELECT Id, FullPhotoURL FROM User WHERE Id IN :taskOwners]){
            taskPhotos.put(u.ID, u.FullPhotoURL);
        }
        
    }
    
    public void toggleModal(){
        if(modalData){
            modalData = false;
        }else{
            modalData = true;
        }
    }
  
}