public class instanceFinder {
    public string instance {get;set;}
    public string fixedURL {get;set;}
    public string URL {get;set;}
    public string label {get;set;}
    
    public instanceFinder() {
        instance = System.URL.getSalesforceBaseUrl().getHost().remove('-api');
        fixedURL = 'https://'+instance+'/';
    } 
    
    public string getURL() {
        return URL;
    }
    
    public string getlabel() {
        return label;
    }
   
}