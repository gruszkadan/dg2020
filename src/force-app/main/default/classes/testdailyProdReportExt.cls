@isTest(SeeAllData=true)
private class testdailyProdReportExt 
{
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test1() 
    {
        Productivity_Report__c[] prs = new list<Productivity_Report__c>();
        string roles = 'Trainee;Associate;Sales Executive;Territory Sales Executive;Territory Account Manager;';
        for (string s : roles.split(';')) {
            Productivity_Report__c pr = new Productivity_Report__c();
            pr.User_LU__c = [SELECT Id, Name FROM User WHERE Name = 'Kayla Reinhackel' LIMIT 1].Id;
            pr.Rep_Role__c = s;
            pr.Run_Date__c = Date.TODAY();
            pr.Rep_Sales_Manager__c = 'Tim Ly';
            pr.Sales_Director__c = 'Eric Jackson';
            pr.Sales_Team__c = 'South';
            pr.Actual_Completed_Tasks_1__c = 30;
            pr.Goals_Completed_Tasks_1__c = 100;
            pr.Actual_Initial_Contacts_1__c = 30;
            pr.Goals_Initial_Contacts_1__c = 100;
            pr.Actual_Scheduled_Demos_1__c = 10;
            pr.Goals_Scheduled_Demos_1__c = 15;
            pr.Actual_Completed_Demos_1__c = 5;
            pr.Goals_Completed_Demos_1__c = 10;
            pr.Actual_Created_Licensing__c = 1000;
            pr.Totals_Actual_Completed_Tasks__c = 1000;
            pr.Totals_Goals_Completed_Tasks__c = 1500;
            pr.Totals_Actual_Initial_Contacts__c = 10;
            pr.Totals_Goals_initial_Contacts__c = 20;
            pr.Totals_Actual_Scheduled_Demos__c = 100;
            pr.Totals_Goals_Scheduled_Demos__c = 200;
            pr.Totals_Actual_Completed_Demos__c = 50;
            pr.Totals_Goals_Completed_Demos__c = 100;
            pr.Totals_Actual_Created_licensing__c = 2000;
            prs.add(pr);
        }
        insert prs;
        Test.startTest();
        Decimal sum_a_c_t = 50;
        Decimal sum_g_c_t = 50;
        Decimal sum_a_i_c = 50;
        Decimal sum_g_i_c = 50;
        Decimal sum_a_s_d = 50;
        Decimal sum_g_s_d = 50;
        Decimal sum_a_c_d = 50;
        Decimal sum_g_c_d = 50;
        Decimal sum_a_c_l = 50;
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new dailyprodscheduler());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, 
                            ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', 
                            String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
    
    static testmethod void test3() 
    {
        test.starttest();
        Productivity_Report__c[] prs = new list<Productivity_Report__c>();
        string roles = 'Trainee;Associate;Sales Executive;Territory Sales Executive;Territory Account Manager;';
        for (string s : roles.split(';')) {
            Productivity_Report__c pr = new Productivity_Report__c();
            pr.User_LU__c = [SELECT Id, Name FROM User WHERE Name = 'Kayla Reinhackel' LIMIT 1].Id;
            pr.Rep_Role__c = s;
            pr.Run_Date__c = Date.TODAY();
            pr.Rep_Sales_Manager__c = 'Tim Ly';
            pr.Sales_Director__c = 'Eric Jackson';
            pr.Sales_Team__c = 'South';
            pr.Actual_Completed_Tasks_1__c = 30;
            pr.Goals_Completed_Tasks_1__c = 100;
            pr.Actual_Initial_Contacts_1__c = 30;
            pr.Goals_Initial_Contacts_1__c = 100;
            pr.Actual_Scheduled_Demos_1__c = 10;
            pr.Goals_Scheduled_Demos_1__c = 15;
            pr.Actual_Completed_Demos_1__c = 5;
            pr.Goals_Completed_Demos_1__c = 10;
            pr.Actual_Created_Licensing__c = 1000;
            pr.Totals_Actual_Completed_Tasks__c = 1000;
            pr.Totals_Goals_Completed_Tasks__c = 1500;
            pr.Totals_Actual_Initial_Contacts__c = 10;
            pr.Totals_Goals_initial_Contacts__c = 20;
            pr.Totals_Actual_Scheduled_Demos__c = 100;
            pr.Totals_Goals_Scheduled_Demos__c = 200;
            pr.Totals_Actual_Completed_Demos__c = 50;
            pr.Totals_Goals_Completed_Demos__c = 100;
            pr.Totals_Actual_Created_licensing__c = 2000;
            prs.add(pr);
        }
        insert prs;
        
        ApexPages.currentPage().getParameters().put('u', 'Eric0Jackson');
        ApexPages.StandardController testController = new ApexPages.StandardController(new Productivity_Report__c());
        dailyProdReportExt myController = new dailyProdReportExt(testController);
        
        test.stoptest();
    }
    
}