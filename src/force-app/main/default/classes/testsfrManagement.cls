@isTest
private class testsfrManagement {
 
    static testmethod void sfrTBACon_1() {
        id sfRequestQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.Name = 'SF Request Queue' LIMIT 1].Queueid;
        id markId = [SELECT id FROM User WHERE Name = 'Mark McCauley' AND isActive = true LIMIT 1].id;
        id danId = [SELECT id FROM User WHERE Name = 'Daniel Gruszka' AND isActive = true LIMIT 1].id;
        
        Salesforce_Request__c sfr1 = new Salesforce_Request__c();
        sfr1.Ownerid = sfRequestQueue;
        sfr1.Summary__c = 'test';
        sfr1.Request_Type__c = 'Enhancement';
        insert sfr1;
        
        Salesforce_Request__c sfr2 = new Salesforce_Request__c();
        sfr2.Ownerid = danId;
        sfr2.Summary__c = 'test';
        sfr2.Request_Type__c = 'Enhancement';
        insert sfr2;
        
        Salesforce_Request__c sfr3 = new Salesforce_Request__c();
        sfr3.Ownerid = markId;
        sfr3.Summary__c = 'test';
        sfr3.Request_Type__c = 'Enhancement';
        insert sfr3;
        
        Salesforce_Request__c sfr4 = new Salesforce_Request__c();
        sfr4.Ownerid = danId;
        sfr4.Summary__c = 'test';
        sfr4.Request_Type__c = 'Enhancement';
        insert sfr4;
        
        ApexPages.currentPage().getParameters().put('param_reqId', sfr1.id);
        ApexPages.currentPage().getParameters().put('param_sel_type', 'Project');
        ApexPages.currentPage().getParameters().put('param_sel_projsize', 'Test size');
        sfrTBACon con = new sfrTBACon();
        con.selId = sfr1.id;
        
        
        con.gotoHome();
        con.gotoEnhancements();
        con.gotoProjects();
        con.gotoDepartmentManagement();
        con.view();
        con.querySelReq();
        con.selectRequestType();
        con.selectProjectSize();
        con.getRequestTypeVals();
        con.getEstimatedProjectSizeVals();
        con.saveSelReq();
        
        
    }
    
    static testmethod void sfrEnhancementsController_1() {
        id enhancementsQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.DeveloperName = 'SF_Requests_Enhancements' LIMIT 1].Queueid;
        id enhancementsDevQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.DeveloperName = 'Enhancements_Dev_Work_Queue' LIMIT 1].Queueid;
        
        Salesforce_Request__c sfr1 = new Salesforce_Request__c();
        sfr1.Ownerid = enhancementsQueue;
        sfr1.Summary__c = 'test';
        sfr1.Request_Type__c = 'Enhancement';
        insert sfr1;
        
        Salesforce_Request__c sfr2 = new Salesforce_Request__c();
        sfr2.Ownerid = enhancementsDevQueue;
        sfr2.Summary__c = 'test';
        sfr2.Request_Type__c = 'Enhancement';
        insert sfr2;
        
        ApexPages.currentPage().getParameters().put('id', sfr1.id);
        sfrEnhancementsController con = new sfrEnhancementsController();
        con.selId = sfr1.id;
        
        con.gotoHome();
        con.gotoTBA();
        con.gotoProjects();
        con.gotoDepartmentManagement();
        con.view();
        con.acceptOwnership();
    }
    
    static testmethod void sfrProjectsController_1() {
        id projectsQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.DeveloperName = 'SF_Requests_Projects' LIMIT 1].Queueid;
        id projectsDevQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.DeveloperName = 'Projects_Dev_Work_Queue' LIMIT 1].Queueid;
        
        Salesforce_Request__c sfr1 = new Salesforce_Request__c();
        sfr1.Ownerid = projectsQueue;
        sfr1.Summary__c = 'test';
        sfr1.Request_Type__c = 'Project';
        insert sfr1;
        
        Salesforce_Request__c sfr2 = new Salesforce_Request__c();
        sfr2.Ownerid = projectsDevQueue;
        sfr2.Summary__c = 'test';
        sfr2.Request_Type__c = 'Project';
        insert sfr2;
        
        ApexPages.currentPage().getParameters().put('id', sfr1.id);
        sfrProjectsController con = new sfrProjectsController();
        con.selId = sfr1.id;
        
        con.gotoHome();
        con.gotoTBA();
        con.gotoEnhancements();
        con.gotoDepartmentManagement();
        con.view();
        con.acceptOwnership();
    }
    
    static testmethod void sfrDepartmentManagementCon_1() {
        id projectsDevQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.DeveloperName = 'Projects_Dev_Work_Queue' LIMIT 1].Queueid;
        id enhancementsDevQueue = [SELECT Queueid FROM QueueSObject WHERE Queue.DeveloperName = 'Enhancements_Dev_Work_Queue' LIMIT 1].Queueid;
        
        Salesforce_Request__c sfr1 = new Salesforce_Request__c();
        sfr1.Ownerid = projectsDevQueue;
        sfr1.Summary__c = 'test';
        sfr1.Request_Type__c = 'Project';
        sfr1.Department__c = 'Customer Care';
        insert sfr1;
        
        Salesforce_Request__c sfr2 = new Salesforce_Request__c();
        sfr2.Ownerid = enhancementsDevQueue;
        sfr2.Summary__c = 'test';
        sfr2.Request_Type__c = 'Enhancement';
        sfr1.Department__c = 'Customer Care';
        insert sfr2;
        
        ApexPages.currentPage().getParameters().put('selDeptParam', 'Sales');
        ApexPages.currentPage().getParameters().put('id', sfr1.id);
        sfrDepartmentManagementCon con = new sfrDepartmentManagementCon();
        
        sfrDepartmentManagementCon.request req1 = new sfrDepartmentManagementCon.request(sfr1, 1);
        con.projects.add(req1);
        
        sfrDepartmentManagementCon.request req2 = new sfrDepartmentManagementCon.request(sfr2, 1);
        con.enhancements.add(req2);
               
        ApexPages.currentPage().getParameters().put('sortReqId', sfr1.id);
        ApexPages.currentPage().getParameters().put('sortlist', 'Project');
        con.sortList();
        
        ApexPages.currentPage().getParameters().put('sortReqId', sfr2.id);
        ApexPages.currentPage().getParameters().put('sortlist', 'Enhancement');
        con.sortList();
        
        con.selectDepartment();
        con.gotoHome();
        con.gotoRequestsTBA();
        con.gotoEnhancements();
        con.gotoProjects();
        con.gotoDepartmentManagement();
        con.gotoStatusWall();
    }
    
    public static testmethod void sfRequestTab_1() {
        sfRequestTab con = new sfRequestTab();
        con.gotoEnhancements();
        con.gotoTBA();
        con.gotoProjects();
        con.gotoDepartmentManagement();
        con.gotoStatusWall();
    }
    
    public static testmethod void sfrTimer() {
        id ownerId = [SELECT id FROM User WHERE Name = 'Daniel Gruszka' AND isActive = true LIMIT 1].id;
        
        Salesforce_Request__c sfr1 = new Salesforce_Request__c();
        sfr1.Ownerid = ownerId;
        sfr1.Summary__c = 'test';
        sfr1.Request_Type__c = 'Project';
        sfr1.Department__c = 'Customer Care';
        insert sfr1;
        
        Salesforce_Request_Task__c timer = new Salesforce_Request_Task__c();
        timer.Work_Request__c = sfr1.id;
        timer.Owner__c = ownerId;
        timer.Start_Date__c = dateTime.now();
        insert timer;
   
        ApexPages.currentPage().getParameters().put('selreqid',sfr1.id); 
        sfrTimerCon con = new sfrTimerCon();
        
        con.stopTimer();
    }
    
}