@isTest(seeAllData=true)
private class testPOIScheduledClass {
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    
	static testmethod void test1() {
      
        Salesforce_Log__c newLog = new Salesforce_Log__c();
        newLog.Class__c='NextPageToken';
        newLog.Program__c='POI';
        newLog.Type__c='Log';
        newLog.Method__c='marketoData';
        newLog.Log__c='dsadar432ewdxcgr5f';
        insert newLog;
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Marketo_ID__c='6523669';
        insert newContact;
        Test.startTest();
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new marketoWebServicesMock());
          String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new poiScheduledClass());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
   	static testmethod void testMarketoAuthTokenCallout() {
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new marketoWebServicesMock());
        string marketoData = marketoWebServices.getAuthToken();
        Test.stopTest();
    }
    static testmethod void testMarketoPageTokenCallout() {
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new marketoWebServicesMock());
        string marketoData = marketoWebServices.getPagingToken('0739fb66-51b1-4305-ac53-0caba5c38dce:ab');
        Test.stopTest();
    }
}