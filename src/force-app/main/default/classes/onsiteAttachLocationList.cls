//TEST CLASS - testOnsiteLocations
public class onsiteAttachLocationList {
    id qpID = ApexPages.currentPage().getParameters().get('id');
    public Quote_Product__c theQuoteProduct {get;set;}
    public Attachment newAttach {get;set;}
    public Attachment[] existingAttachments {get;set;}
    public integer attachNum {get;set;}
    
    public onsiteAttachLocationList(){
        newAttach = new Attachment();
        attachNum = 1;
        existingAttachments = new list<Attachment>();
        queryExisitng();
        theQuoteProduct = [Select id,Quote__c from Quote_Product__c where id =:qpId];
        if(existingAttachments.size() > 0){
            attachNum = attachNum + existingAttachments.size();
        }
    }
    
    public void queryExisitng(){
        existingAttachments = [Select id,name FROM Attachment WHERE parentId =:qpId AND Name LIKE 'Onsite Location Listing%' ];
    }
    
    public void save(){
        if(newAttach.Body != null){
            newAttach.ParentId = qpId;
            if(newAttach.Name.contains('.xlsx')){
                newAttach.Name = 'Onsite Location Listing '+attachNum+'.xlsx';
            }else if(newAttach.Name.contains('.xls')){
                newAttach.Name = 'Onsite Location Listing '+attachNum+'.xls';
            }else if(newAttach.Name.contains('.csv')){
                newAttach.Name = 'Onsite Location Listing '+attachNum+'.csv';
            }else if(newAttach.Name.contains('.docx')){
                newAttach.Name = 'Onsite Location Listing '+attachNum+'.docx';
            }else if(newAttach.Name.contains('.doc')){
                newAttach.Name = 'Onsite Location Listing '+attachNum+'.doc';
            }
            insert newAttach;
            Attachment newAttach2 = newAttach.clone();
            newAttach2.ParentId = theQuoteProduct.Quote__c;
            insert newAttach2;
            queryExisitng();
            newAttach = new Attachment();
            attachNum = 1;
            if(existingAttachments.size() > 0){
                attachNum = attachNum + existingAttachments.size();
            }
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Saved successfully!'));
        }
    }
}