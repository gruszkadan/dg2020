public class salesPerformanceItemTriggerHandler {
    
    public static void beforeInsert(Sales_Performance_Item__c[] oldItems, Sales_Performance_Item__c[] newItems) {
        Sales_Performance_Item__c[] originalBookings = new list<Sales_Performance_Item__c>();
        
        for (integer i=0; i<newItems.size(); i++) {
            if (newItems[i].Booking_Amount__c != null && newItems[i].Booking_Amount__c > 0 && newItems[i].Order_Item__c != NULL) {
                originalBookings.add(newItems[i]);
            }
        }
        
        if (originalBookings.size() > 0) {
            salesPerformanceItemUtility.spiOriginalBookings(originalBookings);
        }
    }
    
    public static void beforeUpdate(Sales_Performance_Item__c[] oldItems, Sales_Performance_Item__c[] newItems) {
        Sales_Performance_Item__c[] bookingChanges = new list<Sales_Performance_Item__c>();
        
        for (integer i=0; i<newItems.size(); i++) {
            if (newItems[i].Booking_Amount__c != oldItems[i].Booking_Amount__c && newItems[i].Order_Item__c != NULL){
                bookingChanges.add(newItems[i]);
            }
        }
        
        if (bookingChanges.size() > 0) {
            salesPerformanceItemUtility.spiOriginalBookings(bookingChanges);
        }
    }
    
    public static void afterInsert(Sales_Performance_Item__c[] oldItems, Sales_Performance_Item__c[] newItems) {
        Sales_Performance_Item__c[] bookingIncentiveItems = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] bookingItems = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] associateIncentiveItems = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] opportunityCreditItems = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] opportunityIncentiveItems = new list<Sales_Performance_Item__c>();
        
        for (integer i=0; i<newItems.size(); i++) {
            if (newItems[i].Type__c == 'Booking' &&
                newItems[i].Group__c != 'House Sales' &&
                newItems[i].Group__c != 'EHS Mid-Market Sales' &&
                newItems[i].Group__c != 'EHS Enterprise Sales' &&
                newItems[i].Group__c != 'Customer Care Sales' &&
                newItems[i].Booking_Amount__c != null &&
                newItems[i].Booking_Amount__c > 0 &&
                newItems[i].isSplit__c == false) {
                    bookingIncentiveItems.add(newItems[i]);
                }
            if (newItems[i].Type__c == 'Booking') {
                bookingItems.add(newItems[i]);
                if(newItems[i].Opportunity__c != null){
                    opportunityCreditItems.add(newItems[i]);
                }
            }
            if (newItems[i].Type__c == 'Associate Credit') {
                associateIncentiveItems.add(newItems[i]);
            }
            if (newItems[i].Type__c == 'Opportunity Credit') {
                opportunityIncentiveItems.add(newItems[i]);
            }
        }
        if (bookingIncentiveItems.size() > 0) {
            incentiveTransactionUtility.createBookingsIncentives(null,bookingIncentiveItems);
        }
        if (bookingItems.size() > 0) {
            salesPerformanceItemUtility.insertAssociateCreditSPI(bookingItems);
        }
        if (opportunityCreditItems.size() > 0) {
            salesPerformanceItemUtility.insertOpportunityCreditSPI(opportunityCreditItems);
        }
        if (associateIncentiveItems.size() > 0) {
            incentiveTransactionUtility.createAssociateCreditIncentives(associateIncentiveItems);
        }
        if (opportunityIncentiveItems.size() > 0) {
            incentiveTransactionUtility.createOpportunityCreditIncentives(opportunityIncentiveItems);
        }
    }
    
    public static void afterUpdate(Sales_Performance_Item__c[] oldItems, Sales_Performance_Item__c[] newItems) {
        Sales_Performance_Item__c[] updateSPOs = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] oldBookingChanges = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] newBookingChanges = new list<Sales_Performance_Item__c>();  
        Sales_Performance_Item__c[] oldAssociateBookingChanges = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] newAssociateBookingChanges = new list<Sales_Performance_Item__c>();  
        Sales_Performance_Item__c[] oldOwnerChanges = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] newOwnerChanges = new list<Sales_Performance_Item__c>(); 
        Sales_Performance_Item__c[] splitBookingChangeSPIs = new list<Sales_Performance_Item__c>(); 
        Sales_Performance_Item__c[] opportunityCreditItems = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] oldOpportunityCreditBookingChanges = new list<Sales_Performance_Item__c>();
        Sales_Performance_Item__c[] newOpportunityCreditBookingChanges = new list<Sales_Performance_Item__c>();
        
        for (integer i=0; i<newItems.size(); i++) {
            if (newItems[i].Account__c != oldItems[i].Account__c ||
                newItems[i].OwnerId != oldItems[i].OwnerId) {
                    updateSPOs.add(newItems[i]);
                    if(newItems[i].OwnerId != oldItems[i].OwnerId){
                        oldOwnerChanges.add(oldItems[i]);
                        newOwnerChanges.add(newItems[i]);
                    }
                }
            if ((newItems[i].Booking_Amount__c != oldItems[i].Booking_Amount__c) &&
                newItems[i].Group__c != 'House Sales' &&
                newItems[i].Group__c != 'EHS Mid-Market Sales' &&
                newItems[i].Group__c != 'EHS Enterprise Sales' &&
                newItems[i].isSplit__c == false || (newItems[i].isSplit__c == true && oldItems[i].isRenewal__c == false && newItems[i].isRenewal__c == true) &&
                newItems[i].Group__c != 'Customer Care Sales') {
                    if(newItems[i].Type__c == 'Booking' && newItems[i].Automated_Adjustment_Incentive__c == true){
                        oldBookingChanges.add(oldItems[i]);
                        newBookingChanges.add(newItems[i]);
                    }else if(newItems[i].Type__c == 'Associate Credit' && (newItems[i].Automated_Adjustment_Incentive__c == true && newItems[i].Inactive__c == false || (newItems[i].Inactive__c == true && oldItems[i].Inactive__c == false))){
                        oldAssociateBookingChanges.add(oldItems[i]);
                        newAssociateBookingChanges.add(newItems[i]);
                    }else if(newItems[i].Type__c == 'Opportunity Credit' && newItems[i].Automated_Adjustment_Incentive__c == true){
                        oldOpportunityCreditBookingChanges.add(oldItems[i]);
                        newOpportunityCreditBookingChanges.add(newItems[i]);
                    }
                }
            //On split this adds the split master spis to a list to update the associate credits
            if ((newItems[i].Booking_Amount__c != oldItems[i].Booking_Amount__c) &&
                newItems[i].Group__c != 'House Sales' &&
                newItems[i].Group__c != 'EHS Mid-Market Sales' &&
                newItems[i].Group__c != 'EHS Enterprise Sales' &&
                newItems[i].Group__c != 'Customer Care Sales' &&
                newItems[i].isSplit__c == true &&
                newItems[i].Type__c == 'Booking'
               ) {
                   splitBookingChangeSPIs.add(newItems[i]);
               }
            if (newItems[i].Type__c == 'Booking' &&
                newItems[i].Opportunity__c != null &&
                oldItems[i].Opportunity__c == null) {
                    opportunityCreditItems.add(newItems[i]);
                }
            
        }
        
        if (updateSPOs.size() > 0) {
            salesPerformanceOrderUtility.updateSPOFromSPI(updateSPOs);
        }
        if (oldBookingChanges.size() > 0 && newBookingChanges.size() > 0) {
            incentiveTransactionUtility.createBookingsIncentives(oldBookingChanges,newBookingChanges);
        }
        if (oldAssociateBookingChanges.size() > 0 && newAssociateBookingChanges.size() > 0) {
            incentiveTransactionUtility.adjustAssociateCreditIncentives(oldAssociateBookingChanges,newAssociateBookingChanges);
        }
        if (oldOwnerChanges.size() > 0 && newOwnerChanges.size() > 0) {
            incentiveTransactionUtility.createRepChangeIncentives(oldOwnerChanges,newOwnerChanges);
        }
        if (splitBookingChangeSPIs.size() > 0){
            salesPerformanceItemUtility.splitAssociateCreditSPI(splitBookingChangeSPIs);
        }
        if (opportunityCreditItems.size() > 0) {
            salesPerformanceItemUtility.insertOpportunityCreditSPI(opportunityCreditItems);
        }
        if (oldOpportunityCreditBookingChanges.size() > 0 && newOpportunityCreditBookingChanges.size() > 0) {
            incentiveTransactionUtility.adjustOpportunityCreditIncentives(oldOpportunityCreditBookingChanges,newOpportunityCreditBookingChanges);
        }
    }
    
    
    public static void afterDelete(Sales_Performance_Item__c[] oldItems, Sales_Performance_Item__c[] newItems) {}
    
}