<apex:page sidebar="false" tabStyle="order_management__tab" controller="orderPendingAdjustmentsController">
    <apex:includeScript value="{!URLFOR($Resource.salesperformance_resources, 'jQuery/jquery-3.2.1.min.js')}"/>
    <apex:slds />
    <apex:actionStatus id="status" onstop="openOrderModal()"/>
    <apex:actionStatus id="sendAlertStatus" onstart="showSpinner()" onstop="reloadPage()"/>
    <div id="spinner" class="slds-spinner_container slds-hide" style="position:fixed;">
        <div role="status" class="slds-spinner slds-spinner_medium slds-spinner_brand">
            <div class="slds-assistive-text">Loading Data...</div>
            <div class="slds-spinner__dot-a"></div>
            <div class="slds-spinner__dot-b"></div>
        </div>
    </div>
    <apex:form id="form">
        <apex:actionFunction action="{!reloadPage}" name="reloadPage" oncomplete="hideSpinner()" reRender="orderModalDetails"/>
        <div class="slds-scope">
            <div class="slds-m-left_x-small">
                <nav role="navigation" aria-label="Breadcrumbs">
                    <ol class="slds-breadcrumb slds-list_horizontal slds-p-bottom_x-small" >
                        <li class="slds-breadcrumb__item slds-text-title_caps">
                            <apex:commandLink action="/apex/order_management">
                                Order Management
                            </apex:commandLink>
                        </li>
                        <li class="slds-breadcrumb__item slds-text-title_caps">
                            <a style="pointer-events: none;cursor: default; font-weight:bold;">
                                Adjustment Alerts
                            </a>
                        </li>
                    </ol>
                </nav> 
            </div>
            <div class="slds-page-header">
                <div class="slds-media">
                    <div class="slds-media__figure">
                        <span class="slds-icon_container slds-icon-custom-custom93">
                            <svg aria-hidden="true" class="slds-icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/custom-sprite/svg/symbols.svg#custom93')}"></use>
                            </svg>
                        </span>
                    </div>
                    <div class="slds-media__body">
                        <h1 class="slds-page-header__title slds-truncate slds-align-middle">Order Management</h1>
                        <p class="slds-text-body_small slds-line-height_reset">Adjustment Alerts</p>
                    </div>
                </div>
            </div>
            <article class="slds-card slds-m-top_x-small">
                <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media_center slds-has-flexi-truncate">
                        <div class="slds-media__body">
                            <h2>
                                <div class="slds-card__header-link slds-truncate">
                                    <span class="slds-text-heading_small">Pending Order Adjustments({!pendingItems.size})</span>
                                </div>
                            </h2>
                        </div>
                    </header>
                </div>
                <div class="slds-card__body">
                    <table class="slds-table slds-table_fixed-layout slds-table_bordered slds-table_cell-buffer">
                        <thead>
                            <tr class="slds-text-title_caps">
                                <th scope="col">
                                    <div class="slds-truncate">Account Name</div>
                                </th>
                                 <th scope="col">
                                    <div class="slds-truncate">Account Id</div>
                                </th>
                                <th scope="col">
                                    <div class="slds-truncate">Order ID</div>
                                </th>
                                <th scope="col">
                                    <div class="slds-truncate">Order Date</div>
                                </th>
                                <th scope="col">
                                    <div class="slds-truncate">Adjustment Date</div>
                                </th>
                                <th scope="col">
                                    <div class="slds-truncate">Action</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <apex:repeat value="{!orderWrappers}" var="order">
                                <tr class="slds-hint-parent">
                                    <td>
                                        <div class="slds-truncate" title="{!order.customerName}">{!order.customerName}</div>
                                    </td>
                                     <td>
                                        <div class="slds-truncate" title="{!order.adminId}">{!order.adminId}</div>
                                    </td>
                                    <td>
                                        <div class="slds-truncate">{!order.orderId}</div>
                                    </td>
                                    <td>
                                        <div class="slds-truncate">{!order.orderDate}</div>
                                    </td>
                                    <td>
                                        <div class="slds-truncate">{!order.orderAdjustmentDate}</div>
                                    </td>
                                    <td>
                                        <div class="slds-truncate">
                                        <apex:commandLink title="Create Alert" styleClass="slds-button slds-button_icon" action="{!selectOrder}" reRender="orderModalTable,orderModalDetails,orderModalHeader" status="status">
                                            <svg class="slds-button__icon" aria-hidden="true">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#notification')}"></use>
                                            </svg>
                                            Create Alert
                                            <apex:param name="selOrder" value="{!order.OrderId}"/>
                                        </apex:commandLink>
                                        |
                                         <apex:commandLink title="Dismiss" styleClass="slds-button slds-button_icon" action="{!dismiss}" onclick="if(!confirm('Are you sure you want to dismiss this adjustment?')) return false;" reRender="null" status="sendAlertStatus">
                                            <svg class="slds-button__icon" aria-hidden="true">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#turn_off_notifications')}"></use>
                                            </svg>
                                            Dismiss
                                            <apex:param name="selOrder" value="{!order.OrderId}"/>
                                        </apex:commandLink>
                                        </div>
                                    </td>
                                </tr>
                            </apex:repeat>
                        </tbody>
                    </table>
                </div>
                <footer class="slds-card__footer"></footer>
            </article>
        </div>
        
        <!---MODAL--->
        
        <section id="orderModal" role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-describedby="modal-content-id-1" class="slds-modal slds-modal_large" style="text-align:left;">
            <div class="slds-modal__container">
                <header class="slds-modal__header">
                    <button type="button" onclick="closeModal()" class="close slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" title="Close">
                        <svg class="slds-button__icon slds-button__icon_large" aria-hidden="true">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#close')}"></use>
                        </svg>
                        <span class="slds-assistive-text">Close</span>
                    </button>
                    <apex:outputPanel id="orderModalHeader">
                        <h2 id="modal-heading-01" class="slds-text-heading_medium slds-hyphenate">Adjustment Message - {!selectedOrder.customerName} - Order# {!selectedOrder.orderId}</h2>
                    </apex:outputPanel>
                </header>
                <div class="slds-modal__content slds-p-horizontal_xx-large slds-p-vertical_small" id="modal-content-id-2">
                    <apex:outputPanel id="orderModalDetails">
                        <div class="slds-notify slds-notify_alert slds-theme_alert-texture slds-theme_error slds-m-bottom_small" role="alert" style="border-radius: 0.5rem;{!if(errorString != null,'','display:none;')}">
                            <span class="slds-icon_container slds-icon-utility-error slds-m-right_x-small" >
                                <svg class="slds-icon slds-icon_x-small" aria-hidden="true">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#error')}" />
                                </svg>
                            </span>
                            <h2>Error: {!errorString}</h2>
                        </div>
                        <div class="slds-form slds-form_stacked">
                            <div class="slds-form-element">
                                <label class="slds-form-element__label" for="input-id-01">To: (Comma Separated)</label>
                                <div class="slds-form-element__control">
                                    <apex:inputText styleclass="slds-input" value="{!selectedOrder.emailAddresses}" />
                                </div>
                            </div>
                            <div class="slds-form-element {!if(errorString != null && selectedOrder.adjustmentType = null, ' slds-has-error','')}">
                                <label class="slds-form-element__label" for="input-id-01">Adjustment Type:</label>
                                <div class="slds-form-element__control">
                                    <apex:selectList styleclass="slds-select" value="{!selectedOrder.adjustmentType}" multiselect="false" size="1">
                                        <apex:selectOptions value="{!Types}"/>
                                    </apex:selectList>
                                </div>
                            </div>
                            <div class="slds-form-element">
                                <label class="slds-form-element__label" for="input-id-01">Items:</label>
                                <div class="slds-form-element__control">
                                <table class="slds-table slds-table_fixed-layout slds-table_bordered slds-no-row-hover slds-table_cell-buffer slds-border_left slds-border_right">
                                <thead>
                                    <tr class="slds-text-title_caps">
                                        <th scope="col">
                                            <div class="slds-truncate">Order Item ID</div>
                                        </th>
                                        <th scope="col">
                                            <div class="slds-truncate">Product Name</div>
                                        </th>
                                         <th scope="col">
                                            <div class="slds-truncate">Sales Rep</div>
                                        </th>
                                        <th scope="col">
                                            <div class="slds-truncate">Original Bookings</div>
                                        </th>
                                        <th scope="col">
                                            <div class="slds-truncate">New Bookings</div>
                                        </th>
                                         <th scope="col" style="width:30%">
                                            <div class="slds-truncate">Adjustment Reason</div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <apex:repeat value="{!selectedOrder.items}" var="item">
                                        <tr class="slds-hint-parent">
                                            <td>
                                                <div class="slds-truncate">{!item.Order_Item_ID__c}</div>
                                            </td>
                                            <td>
                                                <div class="slds-truncate">{!item.Admin_Tool_Product_Name__c}</div>
                                            </td>
                                            <td>
                                                <div class="slds-truncate">{!item.Admin_Tool_Sales_Rep__c}</div>
                                            </td>
                                            <td>
                                                <div class="slds-truncate">${!itemBookingsChangeMap[item.id]}</div>
                                            </td>
                                            <td>
                                                <div class="slds-truncate">${!item.Invoice_Amount__c}</div>
                                            </td>
                                            <td>
                                                <div class="slds-truncate">
                                                    <div class="slds-form-element {!if(errorString != null && item.Invoice_Adjustment_Reason__c = null, ' slds-has-error','')}">
                                                        <div class="slds-form-element__control">
                                                            <apex:inputtextarea styleclass="slds-textarea" html-placeholder="Enter a reason for adjustment..." value="{!item.Invoice_Adjustment_Reason__c}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </apex:repeat>
                                </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </apex:outputPanel>
                </div>
                <footer class="slds-modal__footer">
                    <center>
                        <apex:commandLink styleClass="slds-button slds-button_brand" action="{!sendAlert}" status="sendAlertStatus" reRender="null">
                            <svg class="slds-button__icon slds-button__icon_left" aria-hidden="true">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#email')}"></use>
                            </svg>
                            Send Alert
                        </apex:commandLink>  
                    </center>
                </footer>
            </div>
        </section>
        <!---/MODAL--->
        <div id="backdrop" class="slds-backdrop"></div>
    </apex:form>
    <script>
    var j$ = jQuery.noConflict();
    function openOrderModal(){
        hideSpinner();
        j$('#orderModal').addClass('slds-fade-in-open');
        j$('#backdrop').addClass('slds-backdrop_open');
    }
    function closeModal(){
        j$('.slds-modal').removeClass('slds-fade-in-open');
        j$('#backdrop').removeClass('slds-backdrop_open');
    }
    function showSpinner() {
        j$("#spinner").removeClass("slds-hide");
    }
    function hideSpinner() {
        j$("#spinner").addClass("slds-hide");
    }
    </script>    
</apex:page>