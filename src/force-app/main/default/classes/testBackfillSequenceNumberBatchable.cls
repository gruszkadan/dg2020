@isTest
public class testBackfillSequenceNumberBatchable {
    
    static testmethod void test1() {
        
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;
        
        id caseOwner = [SELECT id FROM User WHERE LastName = 'Wills' LIMIT 1].id; 
        
        Case[] casesToInsert = new List<Case>();
        for(integer i = 0; i<100; i++){
            Case newCase = new Case();
            newCase.OwnerID = caseOwner;
            newCase.AccountId = newAcct.Id;
            newCase.ContactId = newCon.Id;
            newCase.Status = 'Active';
            newCase.Subject = 'Test';
            newCase.Type = 'Compliance Services';
            newCase.Primary_project_type__c = 'BVA';
            newCase.Total_Project_Amount__c = 9.99;
            casesToInsert.add(newCase);
        }
 
        
        //Upon insertion of cases witht he fields above not set to null, a rev rec and transactions will be created.
        insert casesToInsert;
        
        Revenue_Recognition__c[] revRecs = [SELECT id, (SELECT id, Recognition_Project_Status__c, Sequence_Number__c, step__c
                                                                        FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                            FROM Revenue_Recognition__c
                                            WHERE case__c = :casesToInsert];  
        
        
        Revenue_Recognition_Transaction__c[] deleteSeq = new List<Revenue_Recognition_Transaction__c>();
        
        for(Revenue_Recognition__c rr: revRecs){                
            for(Revenue_Recognition_Transaction__c rrt: rr.Revenue_Recognition_Transactions__r){
                rrt.Sequence_Number__c= NULL;
                deleteSeq.add(rrt);
            }
        }
        
        update deleteSeq;
        
        //run method
        Test.StartTest();
        backfillSequenceNumberBatchable updateSeq = new backfillSequenceNumberBatchable();
        ID batchprocessid = Database.executeBatch(updateSeq);
        Test.StopTest();
        
        Revenue_Recognition__c[] revRecCheck = [SELECT id, (SELECT id,Sequence_Number__c, step__c
                                                                           FROM Revenue_Recognition_Transactions__r ORDER BY Sequence_Number__c ASC)
                                                FROM Revenue_Recognition__c
                                                WHERE case__c = :casesToInsert]; 
        
        
        for(Revenue_Recognition__c rr: revRecCheck){
           
            for(Revenue_Recognition_Transaction__c rrt: rr.Revenue_Recognition_Transactions__r){
                System.assert(rrt.Sequence_Number__c != NULL);
            }
        }
        
        
    }
}