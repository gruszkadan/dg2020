public class leadNewController {
    
    public Id contactId {get;set;}
    public Contact contact {get;set;}
    public Lead newLead {get;set;}
    
    
    public leadNewController(ApexPages.StandardController stdController){
        
        
        newLead = new Lead();
        
        contactId = ApexPages.currentPage().getParameters().get('contactId');
        
        //query contact info only if contactId parameter found
        if(contactId != null){
            
            contact = [Select Id, LinkedIn_Profile__c, FirstName, LastName FROM Contact WHERE Id = :contactId];
            
            newLead.LinkedIn_Profile__c = contact.LinkedIn_Profile__c;
            newLead.FirstName = contact.FirstName;
            newLead.LastName = contact.LastName;
            newLead.Former_Administrator_Contact__c = contact.id;
            newLead.Communication_Channel__c = 'Marketing Prospecting';
            
            
            
            
        }
        
        
    }
    
    
    public PageReference saveLead(){
        
        insert newLead;
        
        //if contact paramter sent, update the contact's New Lead Id field
        if(contact != null){
            
            contact.New_Lead_ID__c = newLead.id;
            update contact;
        }
      
        PageReference lead_detail = new PageReference('/apex/lead_detail?id=' + newLead.ID);
        return lead_detail;  
        
    }
    

    
}