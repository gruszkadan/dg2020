public class taskTriggerHandler {
    
    // BEFORE INSERT
    public static void beforeInsert(Task[] tasks) {
        activityUtility.taskCreated(tasks);
        activityUtility.activityDateUpdate(tasks);
        // If a task is inserted with call results, fill out the POI Status and the Unable to Connect on the task 
        Task[] poiCallResults = new list<Task>(); 
        for (Task t : tasks) {
            if (t.Call_Result__c != null) {
                poiCallResults.add(t);
            }
        }
        if (poiCallResults.size() > 0) {
            activityUtility.poiCallResults(poiCallResults);
        }
    }
    
    // BEFORE UPDATE
    public static void beforeUpdate(Task[] oldTasks, Task[] newTasks) {
        //Check to See if the Task Owner has Changed
        Task[] ownerChanged = new list<Task>();
        
        // If a task's call results are updated, fill out the POI Status and Unable to Connect on the task
        Task[] poiCallResults = new list<Task>(); 
        
        Task[] activityDateChange = new list<Task>();
        for (integer i=0; i<newTasks.size(); i++) {
            if(newTasks[i].OwnerID != oldTasks[i].ownerID){
                ownerChanged.add(newTasks[i]);
            }
            if (newTasks[i].Call_Result__c != oldTasks[i].Call_Result__c) {
                poiCallResults.add(newTasks[i]);
            }
            if(newTasks[i].ActivityDate != oldTasks[i].ActivityDate){
                activityDateChange.add(newTasks[i]);
            }
        }
        if(ownerChanged.size()>0){
            activityUtility.taskOwnerUpdate(ownerChanged);
        }
        if(activityDateChange.size()>0){
            activityUtility.activityDateUpdate(activityDateChange);
        }
        if (poiCallResults.size() > 0) {
            activityUtility.poiCallResults(poiCallResults);
        }
    }
    
    // BEFORE DELETE
    public static void beforeDelete(Task[] oldTasks, Task[] newTasks) {}
    
    // AFTER INSERT
    public static void afterInsert(Task[] oldTasks, Task[] newTasks) {
        
        //Check to See if the Call Results Changed for Sales Tasks
        Task[] salesTasks = new list<Task>();
        for (integer i=0; i<newTasks.size(); i++) {
            if (newTasks[i].New_Department__c == 'Sales' && newTasks[i].WhoId != null) {
                salesTasks.add(newTasks[i]);
            }
        }
        if(salesTasks.size()>0){
            poiStageStatusUtility.updateStatus(salesTasks);
        }
    }
    
    // AFTER UPDATE
    public static void afterUpdate(Task[] oldTasks, Task[] newTasks) {
        //Check to See if the Call Results Changed for Sales Tasks
        Task[] tasksWithCallResults = new list<Task>();
        for (integer i=0; i<newTasks.size(); i++) {
            if ((oldTasks[i].isClosed == false && newTasks[i].isClosed == true && newTasks[i].Call_Result__c != null) 
                || (oldTasks[i].isClosed == true && oldTasks[i].Call_Result__c != newTasks[i].Call_Result__c)
                || (oldTasks[i].WhoId == null && newTasks[i].WhoId != null && (newTasks[i].New_Department__c == 'Sales' 
                                                                               || ((oldTasks[i].isClosed == false && newTasks[i].isClosed == true && newTasks[i].Call_Result__c != null) 
                                                                                   || (oldTasks[i].isClosed == true && oldTasks[i].Call_Result__c != newTasks[i].Call_Result__c))))
               ) {
                   tasksWithCallResults.add(newTasks[i]);
               }
            
        }
        if(tasksWithCallResults.size()>0){
            poiStageStatusUtility.updateStatus(tasksWithCallResults);
        }
    }
    
    // AFTER DELETE
    public static void afterDelete(Task[] oldTasks, Task[] newTasks) {}
    
    // AFTER UNDELETE
    public static void afterUndelete(Task[] oldTasks, Task[] newTasks) {}
    
}