@isTest(seeAllData = true)
public class testContactUtility {
    static testMethod void test1(){
        
        Account a = new Account();
        a.Name = 'Maciejs Account';
        a.Ergonomics_Account_Owner__c = [select id, name from user where name = 'Adam Nieder' limit 1].id;
        
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'Tommy';
        c.LastName = 'Fleetwood';
        c.AccountId = a.id;
        c.EHS_Contact__c = false;
        c.Scheduled_Upgrade_Date__c = null;
        c.Upgrade_Email_1_Sent_Date__c = null;
        c.Upgrade_Email_2_Sent_Date__c = null;
        c.Upgrade_Email_3_Sent_Date__c = null;
        c.Upgrade_Email_3_To_Send_Date__c = null;
        insert c;
        
        Contact c1 = new Contact();
        c1.FirstName = 'James';
        c1.LastName = 'Joseph';
        c1.AccountId = a.id;
        c1.EHS_Contact__c = false;
        c1.Scheduled_Upgrade_Date__c = null;
        c1.Upgrade_Email_1_Sent_Date__c = null;
        c1.Upgrade_Email_2_Sent_Date__c = null;
        c1.Upgrade_Email_3_Sent_Date__c = null;
        c1.Upgrade_Email_3_To_Send_Date__c = null;
        insert c1;
        
        VPM_Project__c vProject1 = new VPM_Project__c();
        vProject1.Account__c = a.Id;
        vProject1.Contact__c = c.Id;
        vProject1.CurrencyIsoCode = 'USD';
        vProject1.Scheduled_Upgrade_Date__c = null;
        insert vProject1;
        
        VPM_Milestone__c vpmMilestone1 = new VPM_Milestone__c();
        vpmMilestone1.Account__c = a.Id;
        vpmMilestone1.Contact__c = c.Id;
        vpmMilestone1.VPM_Project__c = vProject1.Id;
        vpmMilestone1.CurrencyIsoCode = 'USD';
        insert vpmMilestone1;
        
        VPM_Task__c vpmtask1 = new VPM_Task__c();
        vpmtask1.Account__c = a.Id;
        vpmtask1.Contact__c = c.Id;
        vpmtask1.VPM_Milestone__c = vpmMilestone1.Id;
        vpmtask1.Email_Sequence_Number__c = 1;
        vpmtask1.Start_Date__c = null;
        vpmtask1.End_Date__c = null;
        VPMtask1.VPM_Project__c = vProject1.Id;
        Insert vpmtask1;
        
        VPM_Task__c vpmtask2 = new VPM_Task__c();
        vpmtask2.Account__c = a.Id;
        vpmtask2.Contact__c = c.Id;
        vpmtask2.Status__c = 'Not Started';
        vpmtask2.VPM_Milestone__c = vpmMilestone1.Id;
        vpmtask2.Email_Sequence_Number__c = 2;
        vpmtask2.Start_Date__c = NULL;
        vpmtask2.End_Date__c = null;
        VPMtask2.VPM_Project__c = vProject1.Id;
        Insert vpmtask2;
        
        VPM_Task__c vpmtask3 = new VPM_Task__c();
        vpmtask3.Account__c = a.Id;
        vpmtask3.Contact__c = c.Id;
        vpmtask3.VPM_Milestone__c = vpmMilestone1.Id;
        vpmtask3.Email_Sequence_Number__c = 3;
        vpmtask3.Start_Date__c = null;
        vpmtask3.Status__c = 'Not Started';
        vpmtask3.End_Date__c = null;
        VPMtask3.VPM_Project__c = vProject1.Id;
        Insert vpmtask3;
        
        vProject1.Scheduled_Upgrade_Date__c = date.today();
        update vProject1;
        
        VPMtask2.Contact__c = c1.Id;
        update VPMtask2;
        
        
        set<id> ContactIds = new set<Id>();
        
        contactIds.add(c.id);
        
        
        contactUtility.EHSContactCheck(ContactIds);
        
    }
    
}