@isTest(SeeAllData=true)
private class  testAdminToolUpdateRequest {
	public static testmethod void RegularTests() {        
        Test.startTest();
        
        test1();          //test 1 - create a reservation
        test2(); //test 2 - create a reservation not in New Status        
        Test.stopTest();
    }
    
    static testmethod void test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.AdminID__c = '123456';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        PageReference pageRef = Page.adminToolUpdateRequest;
        pageRef.getParameters().put('type', 'a');
        pageRef.getParameters().put('accountID', newAccount.Id);
        Test.setCurrentPageReference(pageRef);
        
        adminToolUpdateRequest atUpdate = new adminToolUpdateRequest();
        atUpdate.getNAICS();
        atUpdate.cancel();
        atUpdate.createCase();
    }
    
     static testmethod void test2() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.AdminID__c = '678910';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        PageReference pageRef = Page.adminToolUpdateRequest;
        pageRef.getParameters().put('type', 'c');
        pageRef.getParameters().put('contactID', newContact.Id);
        Test.setCurrentPageReference(pageRef);
        
        adminToolUpdateRequest atUpdate = new adminToolUpdateRequest();
        atUpdate.createCase();
    }

}