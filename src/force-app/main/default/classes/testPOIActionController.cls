@isTest(seeAllData = true)
private class testPOIActionController {
    private static testmethod void test1() {
        Account newAccount = testAccount();
        insert newAccount;
        Contact master = testContact(newAccount.id);
        insert master;
        
        //Create a queue record
        Lead newLead = testLead();
        insert newLead;
        
        //Master Contact's POI
        Product_of_Interest__c newPoi = new Product_of_Interest__c();
        newPoi.Contact__c = master.id;
        insert newPoi;
        
        //MSDS Open Action
        Product_of_Interest_Action__c msdsOpenAction = new Product_of_Interest_Action__c();
        msdsOpenAction.Contact__c = master.id;
        msdsOpenAction.Product_of_Interest__c = newPoi.id;
        msdsOpenAction.Marketo_ID__c = '1234567';
        msdsOpenAction.Product_Suite__c = 'MSDS Management';
        msdsOpenAction.Product__c = 'HQ';
        msdsOpenAction.Action__c = 'Request Demo';
        insert msdsOpenAction;
        
        //Attachment
        Attachment testAttachment = new Attachment();   	
        testAttachment.Name='TestAttachment.docx';
        Blob bodyBlob=Blob.valueOf('Test Attachment Body');
        testAttachment.body=bodyBlob;
        testAttachment.ParentId = newPoi.id;
        insert testAttachment;
        
        //Pass the extension controller/params
        ApexPages.currentPage().getParameters().put('Id', msdsOpenAction.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Product_of_Interest_Action__c());
        poiActionController myController = new poiActionController(testController);
    }
    public static Account testAccount() {
        return new Account(Name='Test Account', OwnerId='00580000007F76q');
    }
    
    public static Contact testContact(id accountId) {
        return new Contact(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', AccountId=accountId);
    }
    
    public static Lead testLead() {
        return new Lead(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', Company = 'Test Account');
    }
}