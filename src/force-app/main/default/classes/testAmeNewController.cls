@isTest()
    Private class testAmeNewController {
         @isTest(seeAllData =True)
        static void test1(){
            // Setup, we will need to create an account, contact and order item. This will be used to test the Ame_new_controller class
            Account newAccount = new Account();
            newAccount.Name = 'Test Account';
            insert newAccount;
            
            Contact newContact = new Contact();
            newContact.FirstName = 'Bob';
            newContact.LastName = 'Bob';
            newContact.AccountID = newAccount.Id;
            newContact.Primary_Admin__c = true;
            insert newContact;
            
            testDataUtility Util = new testDataUtility(); 
            order_item__c newOrderItem =  Util.createOrderItem('House Sale', '99654321');
            newOrderItem.Product_Platform__c = 'MSDSonline';
            newOrderItem.Renewal_Amount__c = 100;
            newOrderItem.Account__c = newAccount.Id;
            insert newOrderItem;
            
            //Create new AME called RelatedAme
            Account_Management_event__c relatedAme = new Account_Management_event__c();
            //Set the required fields for test 
            relatedAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();
            relatedAme.Account__c = newAccount.id;
            relatedAme.Contact__c = newContact.id;
            
            insert relatedAme;
             
            //redirecting the apex page too go to the new account ID 
            //Passing in the controller and extension we are using in the class to test the controller 
            ApexPages.currentPage().getParameters().put('parentId', newAccount.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(new Account_Management_Event__c());
            ameNewController myController = new ameNewController(testController); 
            system.currentpagereference().getparameters().put('OrderItem', neworderitem.Id);

            
            // assert is used to check if oue new account Id is equal to the value being stored in the Account__c field 
            system.assertequals(newAccount.Id,mycontroller.ame.account__c);
            // checking to see if order item wrappers is set to 1
            system.assertequals(1,mycontroller.orderItemWrappers.size());
            //Initialize the contacts and status and clear contact methods 
            mycontroller.getContactSelectList();
            mycontroller.getStatus();
            mycontroller.ame.RecordTypeId = mycontroller.rtRenew;
            mycontroller.createQuoteOpportunity = true;
                
            // can't save because no ame.ID associated with our AME
            mycontroller.Save();
            system.assertequals(null,mycontroller.ame.Id);
            
            
           //setting all required fields on the ame_new page 
            mycontroller.ame.RecordTypeId = mycontroller.rtRenew;
            mycontroller.InitializeData();
            mycontroller.ame.Contact__c = newContact.id;
            mycontroller.ame.Account__c = newAccount.Id;
            mycontroller.ame.Status__c = 'Active';
            mycontroller.ame.Batch_Month__c	= 'Mar';   
            mycontroller.ame.Batch_Year__c = '2014';
            mycontroller.ame.Renewal_Date__c = date.today()+2;
            mycontroller.orderItemWrappers[0].isselected = true;
            mycontroller.SetRenewalDate();
            mycontroller.save();
            system.assert(mycontroller.ame.Id != null);
            
            
            
          

            // Finding order items and making sure the Id matches from the order item and the new AME that was just created 
            Order_Item__c updatedOrderItem = [Select id, Account_Management_Event__c
                                              from Order_Item__c
                                              Where id=:newOrderItem.Id];
            
            
            
            
            
            mycontroller.ame.RecordTypeId = mycontroller.rtRenew;
            mycontroller.ame.Contact__c = newContact.id;
            mycontroller.ame.Account__c = newAccount.Id;
            mycontroller.ame.Status__c = 'Active';
            mycontroller.ame.Batch_Month__c	= 'FEB';   
            mycontroller.ame.Batch_Year__c = '2017';
            mycontroller.ame.Renewal_Date__c = date.today()+2;
            mycontroller.orderItemWrappers[0].isselected = false;
            mycontroller.save();
            
            // checking to see if our work items have the correct AME ID associated with it 
            system.assert(updatedOrderItem.Account_Management_Event__c == mycontroller.ame.Id);
            
            
             
            
            
            
        }
        @isTest(seeAllData = True)
        static void test2(){
            //Test data we will need for our second test
            Account newAccount = new Account();
            newAccount.Name = 'Test Account';
            insert newAccount;
            
            Contact newContact2 = new Contact();
            newContact2.FirstName = 'Frank';
            newContact2.LastName = 'Gore';
            newContact2.AccountID = newAccount.Id;
            insert newContact2;
            
            //Create new AME and store it in the RelatedAme variable
            Account_Management_event__c relatedAme = new Account_Management_event__c();
            // set record type, account and contact on relatedAme
            relatedAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();
            relatedAme.Account__c = newAccount.id;
            relatedAme.Contact__c = newContact2.id;
            
            insert relatedAme;
            
            //passing in the ID and record type on the related AME
            //setting up the test controller 
            //passing in the controller and to able to test it 
            ApexPages.currentPage().getParameters().put('Id', relatedAme.Id);
            ApexPages.currentPage().getParameters().put('RecordType', Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('At Risk').getRecordTypeId());
            ApexPages.StandardController testController = new ApexPages.StandardController(new Account_Management_Event__c());
            ameNewController myController = new ameNewController(testController); 
            //Setting the default fields we need 
            mycontroller.ame.Contact__c = newContact2.id;
            mycontroller.ame.Account__c = newAccount.id;
        }
        
        static testmethod void test3() {
            user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1];  
            
            //Insert Account
            Account Acc = new Account();
            Acc.Name = 'Ford';
            Acc.Customer_Status__c = 'Prospect'; 
            Acc.OwnerID= Dan.Id;
            Acc.NAICS_Code__c = '123456';
            Acc.AdminId__c = 'TAME123';
            insert Acc; 
            
            //Insert Contact
            Contact Con = new Contact();
            Con.LastName = 'Lahners';
            Con.AccountId = Acc.Id;
            Con.OwnerID= Dan.Id;
            insert Con;
            
            Account_Management_Event__c Ame = new Account_Management_Event__c();
            Account_Management_Event__c Ame1 = new Account_Management_Event__c();
            
            Ame.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('At Risk').getRecordTypeId();
            Ame.OwnerId = Dan.Id;
            Ame.Status__c = 'Active';
            Ame.At_Risk_Source__c = 'Usage';
            Ame.At_Risk_Status__c = 'Active';
            AME.Account__c = acc.id;
            insert Ame;
            
            
            
            //insert new opp
            Opportunity Opp = new Opportunity();
            Opp.Name = 'Ford1';
            Opp.StageName= 'System Demo';
            Opp.OwnerID= Dan.Id;
            opp.AccountId = Acc.id;
            Opp.CloseDate = date.today();
            Opp.Account_Management_Event__c = Ame.Id;
            insert Opp;
            
            //Creating list of order items
            Order_Item__c[] testOIs = new list<Order_Item__c>();
            testDataUtility Util = new testDataUtility(); 
            
            //adding order Item 1
            Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
            oi1.Order_ID__c = 'test456';
            oi1.Month__c = 'January';
            oi1.Salesforce_Product_Name__c = 'HQ';
            oi1.Category__c = 'MSDS Management';
            oi1.Product_Platform__c = 'MSDSonline';
            oi1.Renewal_Amount__c = 1000;
            oi1.AccountID__c = 'TAME123';
            oi1.Term_Start_Date__c = Date.today().addDays(-10);
            oi1.Term_End_Date__c = Date.today().addDays(10);
            testOIs.add(oi1);
            
            Order_Item__c oi2 = util.createOrderItem('TestVP', 'test2');
            oi2.Order_ID__c = 'test456';
            oi2.Month__c = 'January';
            oi2.Salesforce_Product_Name__c = 'HQ';
            oi2.Category__c = 'MSDS Management';
            oi2.Product_Platform__c = 'MSDSonline';
            oi2.Renewal_Amount__c = 1000;
            oi2.AccountID__c = 'TAME123';
            oi2.Term_Start_Date__c = Date.today().addDays(-10);
            oi2.Term_End_Date__c = Date.today().addDays(10);
            testOIs.add(oi2);
            insert testOIs;
            
            ApexPages.currentPage().getParameters().put('Id', Ame.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(Ame);
            ameNewController myController = new ameNewController(testController);
            
            
            //setting all fields to save cancellation AME
            Ame1.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();
            System.debug(Ame.RecordTypeId);
            Ame1.Contact__c = Con.id;
            Ame1.Account__c = Acc.Id;	
            Ame1.OwnerId = Dan.Id;
            Ame1.Status__c = 'Active';
            Ame1.Cancellation_Reason__c = 'Fake Order';   
            Ame1.New_System__c = 'Paper System';
            myController.orderItemWrappers[0].isselected = true;
            
            Order_Item__c updatedOrderItem = [Select id, Account_Management_Event__c
                                              from Order_Item__c
                                              Where id=:oi1.Id];
            System.debug(updatedOrderItem.Account_Management_Event__c);
            myController.orderItemWrappers[0].isselected = true;
            System.assert(updatedOrderItem.Account_Management_Event__c == Ame1.Id);
            myController.cancelSave();
            
            
            
        }
        
        @isTest(seeAllData = true)
        static void test4() {
            //Data to test ame_new at risk record typr tp fully cover save method 
            Account na = new Account();
            na.Name = 'Test Account';
            insert na;
            
            Contact nc = new Contact();
            nc.FirstName = 'Francesco';
            nc.LastName = 'Molinari';
            nc.AccountID = na.Id;
            nc.Primary_Admin__c = true;
            insert nc;
            
            
            ApexPages.currentPage().getParameters().put('parentId', na.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(new Account_Management_Event__c());
            ameNewController myController = new ameNewController(testController); 
            
            mycontroller.ame.RecordTypeId = mycontroller.rtAtRisk;
            mycontroller.InitializeData();
            mycontroller.ame.Contact__c = nc.id;
            mycontroller.ame.Account__c = na.Id;
            mycontroller.ame.Status__c = 'Active';
            mycontroller.ame.At_Risk_Source__c = 'Usage';
            mycontroller.ame.At_Risk_Status__c = 'Active';
            mycontroller.save();
            
            
            
            
            
        }
        
   
    }