@isTest(seeAllData=true)
private class testCampaignMemberTrigger {
    
    public static testmethod void testInsertCampaignMember() {
        Lead l = new Lead(LastName = 'Test',Company = 'Test',Primary_Solution_Product_of_Interest__c = 'MSDS Management');
        insert l;
        
        Account a = new Account(Name='Acct1');
        insert a;
        
        Contact con = new Contact(FirstName='Test',LastName='Test',AccountId=a.id, Communication_Channel__c = 'Referral', Primary_Solution_Product_of_Interest__c = 'MSDS Management');
        insert con;
        
        Campaign c = new Campaign(Name='Test Campaign');
        insert c;
        
        CampaignMember[] cMembers = new list<CampaignMember>();
        
        CampaignMember cm1 = new CampaignMember(ContactId = con.id, Status = 'Sent', CampaignId = c.id);
        cMembers.add(cm1);
        
        CampaignMember cm2 = new CampaignMember(LeadId = l.id, Status = 'Sent', CampaignId = c.id);
        cMembers.add(cm2);
        
        insert cMembers;
        
        cm1 = [select id,Primary_solution_Product_of_interest__c from CampaignMember where id =: cm1.id];
        system.assert(cm1.Primary_Solution_Product_of_Interest__c == con.Primary_Solution_Product_of_Interest__c);
        cm2 = [select id,Primary_solution_Product_of_interest__c from CampaignMember where id =: cm2.id];
        system.assert(cm2.Primary_Solution_Product_of_Interest__c == l.Primary_Solution_Product_of_Interest__c);
    }
}