trigger caseTrigger on Case (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (trigger.isBefore) {
        
        // BEFORE INSERT
        if (trigger.isInsert) {
            caseTriggerHandler.beforeInsert(trigger.new);
        }
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            caseTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
        
    }
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            caseTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            caseTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
    }
    if(!test.isRunningTest()){
        dlrs.RollupService.triggerHandler(); 
    }
}