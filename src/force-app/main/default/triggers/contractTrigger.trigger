trigger contractTrigger on Contract__c (after update) {
    
    if(trigger.isAfter){
        
        //After Update
        if(trigger.isUpdate){
            contractTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
    }
}