/*
* Test class: testSalesPerformanceHeaderController
*/
public class salesPerformanceHeaderController {
    public string currentPage {get;set;}
    public string userID {get;set;}
    public string activeTab {get;set;}
    public string viewAs {get;set;}
    public string viewAsName {get;set;}
    public string errorMsg {get;set;}
    public User u {get;set;}
    public boolean canViewAs {get;set;}
    public boolean canViewAsMultipleTeams {get;set;}
    public boolean canViewSetup {get;set;}
    Public Set<User> subordinates {get;set;}
    
    public salesPerformanceHeaderController(){
        canViewAs = Sales_Performance_Settings__c.getInstance().View_As_Enabled__c;
        canViewAsMultipleTeams = Sales_Performance_Settings__c.getInstance().View_As_Multiple_Sales_Teams__c;
        canViewSetup = Sales_Performance_Settings__c.getInstance().Can_View_Setup__c;
        if(salesPerformanceUtility.canViewAs() && ApexPages.currentPage().getParameters().get('va') != userInfo.getUserId()){
            User viewAsUser = [Select id,name,email from User where id =:ApexPages.currentPage().getParameters().get('va') Limit 1];
            viewAsName = viewAsUser.Name;
        }
        u = [SELECT id, EmployeeNumber, FirstName, UserRoleID, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL FROM User WHERE id = :userInfo.getUserId() LIMIT 1];
        subordinates = new set<User>();
    }
    
    
    public String[] getSubordinateUsers() {
        set<String> options = new set<String>();
        //Get subordinate Roles
        Set<Id> allSubRoleIds;
        if(Sales_Performance_Settings__c.getInstance().Default_View__c == 'VP' || test.isRunningTest()){
            allSubRoleIds = getAllSubRoleIds(new Set<ID>{[Select id from UserRole where Name = 'VP Sales' Limit 1].id});
        }else{
            allSubRoleIds = getAllSubRoleIds(new Set<ID>{u.UserRoleID});
        }
        //Get the subordinate users
        if(canViewAs){
            for (User usr : [Select Id, Name From User where UserRoleId IN :allSubRoleIds and isActive = TRUE and Department__c = 'Sales' and 
                             Group__c !='EHS Enterprise Sales' and Group__c !='EHS Mid-Market Sales'  
                             ORDER BY Name ASC]) {
                                 options.add(usr.Name);
                                 subordinates.add(usr);
                             }
        }
        if(canViewAsMultipleTeams){
            for (User usr : [Select Id, Name From User where EmployeeNumber =:u.EmployeeNumber and id !=:u.id  
                             ORDER BY Name ASC]) {
                                 options.add(usr.Name);
                                 subordinates.add(usr);
                             }
        }
        string[] optionsList = new list<String>();
        optionsList.addAll(options);
        return optionsList;
    }
    
    private static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
        Set<ID> currentRoleIds = new Set<ID>();
        //Get the roles underneath the role that was passed in
        for(UserRole userRole :[select Id from UserRole where ParentRoleId IN :roleIds AND ParentRoleID != null])
            currentRoleIds.add(userRole.Id);
        //Get more roles
        if(currentRoleIds.size() > 0){
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        }
        return currentRoleIds; 
    }
    
    public PageReference viewAsRedirect(){
        for(User s:subordinates){
            if(s.Name == viewAsName){
                viewAs = s.id;
                PageReference pr = new PageReference('/apex/'+currentPage);
                pr.getParameters().put('va', viewAs);
                return pr.setRedirect(true);
            }
        }
        errorMsg = 'Error: User Not Found';
        return null;
    }
    
    public PageReference resetView(){
        PageReference pr = new PageReference('/apex/'+currentPage);
        return pr.setRedirect(true);
    }
}