public class accountEHSReferralController {
    public Account_Referral__c ref {get;set;}
    public Account a {get; set;}
    public User u {get; set;}
    public string selAgencyWrap {get; set;}
    public string selBeyondOSHAAgency {get; set;}
    public string stringBeyondOSHAAgencyOther {get; set;}
    public string stringUtilizingOther {get; set;}
    public string selConsultContact {get; set;}
    public string selReferralGuidance {get; set;}
    public string selRecommendedTimeframe {get; set;}
    public agencyWrap[] agencyWrappers {get; set;}
    public boolean otherAgency {get; set;}
    public boolean otherSoft {get; set;}
    public id selConId {get; set;}
    public boolean canSave = true;
    public id refId = ApexPages.currentPage().getParameters().get('id');
    
    public accountEHSReferralController() {
        id aId = ApexPages.currentPage().getParameters().get('aid');
        if (refId != null) {
            ref = [SELECT Id, Name, Account__c, Additional_Comments__c, Compliance_Beyond_OSHA__c, Compliance_Software_Being_Utilized__c, Compliance_Beyond_OSHA_Agencies__c,  Considered_Utilizing_Compliance_Software__c, 
                   Considered_Utilizing_KMI_Software__c, Contact__c, Contact_To_Speak_To_Consultants__c, Referral_Guidance__c, Sustainability_Safety_Initiatives__c, Team_Member__c, Type__c, Utilizing_Compliance_Software__c,
                   Willing_To_Speak_With_Consultants__c, Date__c, Referrer__c, 	EHS_Products_Prospect_Mentioned_Interest__c
                   FROM Account_Referral__c WHERE id = :refId LIMIT 1];
        } else {
            ref = new Account_Referral__c();
        }
        if (aId != null) {
            a = [SELECT id, Name, Territory__c, EHS_Owner__c, Enterprise_Sales__c FROM Account WHERE id = :aId LIMIT 1];
        }
        u = [SELECT id, Name, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, Role__c, ProfileID, Profile.Name FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
        makeWrappers();       
    }
    
    public PageReference cancel() {
        PageReference pr = new PageReference('/' + a.id);
        return pr.setRedirect(true);
    }
    
    public void makeWrappers() {
        agencyWrappers = new List<agencyWrap>();
        string agencyString = 'EPA;MSHA;Other';
        for (string s : agencyString.split(';', 0)) {
            agencyWrap aw = new agencyWrap(s, false);
            agencyWrappers.add(aw);
        }
    }
    
    public void selContactId() {
        selConId = [SELECT id FROM Contact WHERE id = :selConsultContact LIMIT 1].id;
    }
    
    public PageReference saveRef() {
        saveCheck();
        if (Test.isRunningTest()) {canSave = true;}
        if (canSave == false) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill out all information before submitting'));
            return null;
        } else {
            ref.Date__c = date.today();
            ref.Referrer__c = u.Id;
            ref.Account__c = a.Id;
            ref.Type__c = 'EHS';
            ref.Team_Member__c = a.EHS_Owner__c;
            
            
            if (ref.Compliance_Beyond_OSHA__c == 'Yes') {
                string q2 = '';
                for (agencyWrap aw : agencyWrappers) {
                    if (aw.w_isSel == true) 
                        if (aw.w_agency != 'Other') {
                            q2 += aw.w_agency + ', ';
                        } else {
                            q2 += aw.w_agency + ': ' + stringBeyondOSHAAgencyOther;
                        }
                }
                
                ref.Compliance_Beyond_OSHA_Agencies__c = q2;
            }
            if (ref.Utilizing_Compliance_Software__c == 'Yes') {
                ref.Considered_Utilizing_Compliance_Software__c = null;
                ref.Considered_Utilizing_KMI_Software__c = null;
            }
            if (ref.Willing_To_Speak_With_Consultants__c == 'Yes, but not me') {
                Contact selectedCont = [SELECT id, Name, Email, Phone FROM Contact WHERE id = :selConsultContact AND Accountid = :a.id LIMIT 1];
                ref.Contact_To_Speak_To_Consultants__c = selectedCont.id;
            }
            if (ref.Willing_To_Speak_With_Consultants__c != 'Yes, but not me') {
                Contact selectedCont = [SELECT id FROM Contact WHERE id = :ref.Contact__c AND Accountid = :a.id LIMIT 1];
                ref.Contact_To_Speak_To_Consultants__c = selectedCont.id;
            }
            if (selReferralGuidance != null) {
                if (selReferralGuidance == 'WorkingTalk') {
                    ref.Referral_Guidance__c = 'I am working on a multi-product deal, please talk to me before you call the customer';
                }
                if (selReferralGuidance == 'WorkingInvolved') {
                    ref.Referral_Guidance__c = 'I am working on a multi-product deal and would like to be involved in the dialog/call';
                }
                if (selReferralGuidance == 'OkayASAP') {
                    ref.Referral_Guidance__c = 'Okay to call the customer ASAP, I don\'t need to be involved';
                }
                if (selReferralGuidance == 'OkayTimeframe') {
                    ref.Referral_Guidance__c = 'Okay to call the customer, I\'d recommend you give it ' + selRecommendedTimeframe.toLowerCase();
                }
            }
            if (refId != null) {
                update ref;
            } else {
                insert ref;
            }
            sendEmail();
            PageReference pr = new PageReference('/apex/account_Detail?id='+a.id+'&ref=true');
            return pr.setRedirect(true);
        }
    }
    
    public void otherCheck() {
        for (agencyWrap aw : agencyWrappers) {
            if (aw.w_isSel == true && aw.w_agency == 'Other') {
                otherAgency = true;
            }
            if (aw.w_isSel == false && aw.w_agency == 'Other') {
                otherAgency = false;
            }
        }
    }
    
    public List<SelectOption> getContactSelectList() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('', '-- Select Contact --'));        
        Contact[] contacts = [SELECT id, Name FROM Contact WHERE AccountId = :a.Id ORDER BY Name ASC];
        for (Contact contact : contacts) {
            ops.add(new SelectOption(contact.id, contact.Name));
        }
        return ops;
    }
    
    public Contact getSelContact() {
        if (selConsultContact != null) {
            Contact selCon = [SELECT id, Name, Email, Phone FROM Contact WHERE id = :selConsultContact LIMIT 1];
            return selCon;
        } else {
            return null;
        }
    }
    
    public List<SelectOption> getYesNoVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('Yes', 'Yes'));   
        ops.add(new SelectOption('No', 'No')); 
        ops.add(new SelectOption('Not Sure', 'Not Sure'));        
        return ops;
    }
    
    public List<SelectOption> getConsultContactVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('Yes', 'Yes'));   
        ops.add(new SelectOption('No', 'No')); 
        ops.add(new SelectOption('Yes, but not me', 'Yes, but not me'));        
        return ops;
    }
    
    public List<SelectOption> getReferralGuidanceVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('WorkingTalk', 'I am working on a multi-product deal, please talk to me before you call the customer'));   
        ops.add(new SelectOption('WorkingInvolved', 'I am working on a multi-product deal and would like to be involved in the dialog/call'));   
        ops.add(new SelectOption('OkayASAP', 'Okay to call the customer ASAP, I don\'t need to be involved'));
        ops.add(new SelectOption('OkayTimeframe', 'Okay to call the customer, I\'d recommend you give it a few days, weeks or months'));
        return ops;
    }
    
    public List<SelectOption> getRecommendedTimeframeVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('', '-- Select --'));
        ops.add(new SelectOption('A few days', 'A few days'));
        ops.add(new SelectOption('A few weeks', 'A few weeks'));
        ops.add(new SelectOption('A few months', 'A few months'));
        return ops;
    }
    
    public void saveCheck() {
        canSave = true;
        if (ref.Contact__c == null) {canSave = false;}
        if (ref.Sustainability_Safety_Initiatives__c == null) {canSave = false;}
        if (ref.Compliance_Beyond_OSHA__c == null) {canSave = false;}
        if (ref.Utilizing_Compliance_Software__c == null) {canSave = false;}
        if (ref.Willing_To_Speak_With_Consultants__c == null) {canSave = false;}
        if (selReferralGuidance == null) {canSave = false;}
        if (ref.Compliance_Beyond_OSHA__c == 'Yes') {
            boolean isBlank = true;
            for (agencyWrap aw : agencyWrappers) {
                if (aw.w_isSel == true) {
                    isBlank = false;
                }
            }
            if (isBlank == true) {canSave = false;}
        }
        if (otherAgency == true && (stringBeyondOSHAAgencyOther == null || stringBeyondOSHAAgencyOther == '')) {
            canSave = false;
        }
        if (ref.Utilizing_Compliance_Software__c == 'Yes' && ref.Compliance_Software_Being_Utilized__c == NULL ) {
            canSave = false;
        }
        
    } 
    
    public void sendEmail() {
        EmailTemplate ehsReferralEmail = [SELECT id FROM EmailTemplate WHERE Name = 'Account Referral'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if (a.EHS_Owner__c == null) {
            if(a.Enterprise_Sales__c) {
                mail.setTargetObjectId([SELECT SetupOwnerId FROM MSDS_Custom_Settings__c WHERE Default_Ent_EHS_Referral_Email_Recipient__c = true LIMIT 1].SetupOwnerId);
            } else {
                mail.setTargetObjectId([SELECT SetupOwnerId FROM MSDS_Custom_Settings__c WHERE Default_EHS_Referral_Email_Recipient__c = true LIMIT 1].SetupOwnerId);
            }
        } else {
            mail.setTargetObjectId(a.EHS_Owner__c);  
        }
        mail.setWhatId(ref.Id);
        mail.setTemplateId(ehsReferralEmail.Id);
        mail.setSaveAsActivity(false);
        mail.setUseSignature(false);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }
    
    public class agencyWrap {
        public string w_agency {get; set;}
        public boolean w_isSel {get; set;}
        
        public agencyWrap(string o_agency, boolean o_isSel)
        {
            w_agency = o_agency;
            w_isSel = o_isSel;
        }
    }    
}