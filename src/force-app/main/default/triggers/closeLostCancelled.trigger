trigger closeLostCancelled on Opportunity (after update) {
    
    Opportunity[] opps = new List<Opportunity>();
    Set<id> oppIds = new Set<id>();
    Set<id> acctIds = new Set<id>();
    Set<id> renewalIds = new Set<id>();
    
    // loop through opportunities and find the ones that have a renewal__c record
    // 		add the opportunity to a list to loop through
    // 		add the opportunity ids, account ids, and renewal ids to a set for querying
    for (Opportunity opp : trigger.new) {
        if (opp.Renewal__c != null) {
            opps.add(opp);
            oppIds.add(opp.id);
            acctIds.add(opp.AccountId);
            renewalIds.add(opp.Renewal__c);
        }
    }
    
    // if opportunities meeting the criteria were found, query the other opportunities
    if (opps.size() > 0) {
        Opportunity[] otherOpps = [SELECT id, Renewal__c, AccountId
                                   FROM Opportunity
                                   WHERE isClosed = false
                                   AND Renewal__c IN :renewalIds	
                                   AND AccountId IN :acctIds
                                   AND id NOT IN :oppIds];
        
        // if other opportunities were found, start looping through the original opportunities and the other opportunities to match them
        if (otherOpps.size() > 0) {
            Set<Opportunity> oppSet = new Set<Opportunity>();
            for (Opportunity opp : opps) {
                for (Opportunity otherOpp : otherOpps) {
                    if (otherOpp.Renewal__c == opp.Renewal__c && otherOpp.AccountId == opp.AccountId) {
                        if (opp.StageName == 'Closed Lost' && opp.Closed_Reasons__c == 'Cancel') {
                            otherOpp.StageName = 'Closed Lost';
                            otherOpp.Closed_Reasons__c = 'Cancel';
                            otherOpp.CloseDate = date.today();
                            oppSet.add(otherOpp);
                        }
                    }
                }
            }
            
            Opportunity[] updOpps = new List<Opportunity>();
            if (oppSet.size() > 0) {
                for (Opportunity opp : oppSet) {
                    updOpps.add(opp);
                }
                update updOpps;
               
            }
        }
        
        
    }
    
    
}