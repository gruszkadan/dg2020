public class surveyUtility {
    
    //When a survey is completed, update the NPS Score and NPS Date on the contact
    // called by surveyTriggerHandler.afterUpdate()
    public static void contactNPSScoreUpdate(Survey_Feedback__c[] surveys) {
        Contact[] updList = new list<Contact>();
        set<id> contactIds = new set<id>();
        for (Survey_Feedback__c surv : surveys) {
            contactIds.add(surv.Contact__c);
        }
        Contact[] contacts = [SELECT id, Chemical_Management_NPS_Score__c, Chemical_Management_NPS_Last_Survey_Date__c
                              FROM Contact
                              WHERE id IN : contactIds];
        for (Survey_Feedback__c surv : surveys) {
            Contact con;
            for (Contact contact : contacts) {
                if (contact.id == surv.Contact__c) {
                    con = contact;
                }  
            }
            if (con != null) {
                if (con.Chemical_Management_NPS_Score__c != surv.NPS_Score_Num__c || con.Chemical_Management_NPS_Last_Survey_Date__c != surv.Survey_Completed_Date__c) {
                    con.Chemical_Management_NPS_Score__c = surv.NPS_Score_Num__c;
                    con.Chemical_Management_NPS_Last_Survey_Date__c = surv.Survey_Completed_Date__c;
                    updList.add(con); 
                }
            } 
        }
        if (updList.size() > 0) {
            try {
                update updList;
            } catch(exception e) {
                salesforceLog.createLog('Survey Feedback', true, 'surveyUtility', 'contactNPSScoreUpdate', string.valueOf(e));
            }
        }
    }
    
    //When an ETS survey is created, create a survey queue
    // called by surveyTriggerHandler.afterInsert()
    public static void ets_addToSurveyQueue(Survey_Feedback__c[] surveys) {
        Survey_Queue__c[] newQueues = new list<Survey_Queue__c>();
        set<id> idSet = new set<id>();
        set<id> caseIdSet = new set<id>();
        for (Survey_Feedback__c surv : surveys) {
            idSet.add(surv.Contact__c); 
            caseidSet.add(surv.Case__c);
            Survey_Queue__c newQueue = new Survey_Queue__c();
            if (surv.Name_of_Survey__c == 'ETS - Onboarding') {
                newQueue.Send_Date__c = date.today() + 60;
            }
            if (surv.Name_of_Survey__c == 'ETS - Services') {
                newQueue.Send_Date__c = date.today() + 3;
            }
            if (surv.Name_of_Survey__c == 'ETS - Support') {
                newQueue.Send_Date__c = date.today(); 
            }
            newQueue.Contact__c = surv.Contact__c;
            newQueue.Case__c = surv.Case__c;
            newQueue.Survey__c = surv.id;
            // add new queue record to list for insertion
            newQueues.add(newQueue);   
        }
        // insert the queue records if any were made
        if (newQueues.size() > 0) {
            insert newQueues;
        }
        // update the case and attach the survey
        if (caseidSet.size() > 0) {
            Case[] cases = [SELECT id, Survey_Feedback__c FROM Case WHERE id IN :caseidSet];
            for (Survey_Feedback__c surv : surveys) {
                for (Case c : cases) {
                    if (surv.Case__c == c.id) {
                        c.Survey_Feedback__c = surv.id;
                    }
                }
            }
            update cases;
        }
    }
    
    //When an an AS survey is created, update the contact accordingly
    // called by surveyTriggerHandler.afterUpdate() 
    public static void as_contactSurveyUpdate(Survey_Feedback__c[] surveys) {
        set<id> idSet = new set<id>();
        for (Survey_Feedback__c surv : surveys) {
            idSet.add(surv.Contact__c);
        }
        if (idSet.size() > 0) {
            Contact[] contacts = [SELECT id, Survey_Number__c, Survey_Automation_Step__c, Survey_Email_Sent_Date__c, Survey_Forward_URL__c, Survey_URL__c
                                  FROM Contact 
                                  WHERE id IN : idSet];
            for (Survey_Feedback__c surv : surveys) {
                for (Contact con : contacts) {
                    if (con.id == surv.Contact__c) {
                        con.Survey_Number__c = surv.Name;
                        con.Survey_Automation_Step__c = 'AS - Pre';
                        con.Survey_Forward_URL__c = surv.Forward_Survey_URL__c.substringAfter('://');
                        con.Survey_URL__c = surv.New_Survey_URL__c.substringAfter('://');
                    }
                }
            }
            update contacts;
        }
        
        
    }
    
    //When a survey reminder must go out
    // called by surveyTriggerHandler.afterUpdate()
    public static void ets_sendReminder(Survey_Feedback__c[] surveys) {
        set<id> idSet = new set<id>();
        for (Survey_Feedback__c surv : surveys) {
            idSet.add(surv.Contact__c);
        } 
        if (idSet.size() > 0) {
            Contact[] contacts = [SELECT id, ETS_Survey_Number__c, ETS_Forward_Survey_URL__c, ETS_Survey_URL__c, ETS_Survey_Step__c 
                                  FROM Contact 
                                  WHERE id IN : idSet];
            for (Survey_Feedback__c surv : surveys) {
                for (Contact con : contacts) {
                    if (con.id == surv.Contact__c) {
                        con.ETS_Survey_Number__c = surv.Name;
                        con.ETS_Forward_Survey_URL__c = surv.Forward_Survey_URL__c.substringAfter('://');
                        con.ETS_Survey_URL__c = surv.New_Survey_URL__c.substringAfter('://');
                        con.ETS_Survey_Step__c = 'Reminder';
                    }
                }
            }
            update contacts;
        }
    }
    
    //When a survey is completed and the contact is interested in additional products
    //Called by surveyTriggerHandler.afterUpdate()
    public static void surveyUpsell(Survey_Feedback__c[] surveys) {
        set<id> idSet = new set<id>();
        for (Survey_Feedback__c surv : surveys) {
            surv.Additional_Services_Interest__c = TRUE;
            idSet.add(surv.Contact__c);
        } 
        if (idSet.size() > 0) {
            Contact[] contacts = [SELECT id, Survey_Upsell__c FROM Contact WHERE id IN : idSet];
            for (Survey_Feedback__c surv : surveys) {
                for (Contact con : contacts) {
                    if (con.id == surv.Contact__c) {
                        con.Survey_Upsell__c = surv.Additional_Product_Interest__c;
                    }
                }
            }
            try{
                update contacts;
            } catch(exception e) {
                salesforceLog.createLog('Survey Feedback', true, 'surveyUtility', 'surveyUpsell', string.valueOf(e));
            } 
        }
    }
}