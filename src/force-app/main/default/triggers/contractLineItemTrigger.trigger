trigger contractLineItemTrigger on Contract_Line_Item__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    if(trigger.isBefore){
        //Before Insert
        if(trigger.isInsert){
            contractLineItemTriggerHandler.beforeInsert(trigger.old, trigger.new);
        }
    }
    
    if(trigger.isAfter){
        
        //After Insert
        if(trigger.isInsert){
            contractLineItemTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        //After Delete
        if(trigger.isDelete){
            contractLineItemTriggerHandler.afterDelete(trigger.old, trigger.new);
        }
        
    }
    if(!test.isRunningTest()){
        dlrs.RollupService.triggerHandler(); 
    }
}