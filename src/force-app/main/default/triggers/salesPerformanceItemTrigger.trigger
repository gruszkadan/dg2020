trigger salesPerformanceItemTrigger on Sales_Performance_Item__c
(before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            salesPerformanceItemTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        if (trigger.isUpdate) {
            salesPerformanceItemTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        if (trigger.isDelete) {
            salesPerformanceItemTriggerHandler.afterDelete(trigger.old, trigger.new);
        }
    }
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            salesPerformanceItemTriggerHandler.beforeInsert(trigger.old, trigger.new);
        }
        if (trigger.isUpdate) {
            salesPerformanceItemTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
    }
}