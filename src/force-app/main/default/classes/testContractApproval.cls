@isTest(seeAllData=true)
private class testContractApproval {
    static testmethod void test1() {
             
         // create test account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        // create test contact
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        // create test address
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        // create test contract
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        newContract.Other_Special_Payment_Terms__c = true;
        newContract.Split_Billing__c = true;
        newContract.Split_Billing_Number_of_Splits__c = 4;
        newContract.Standard_MSA__c = 'No';
        newContract.Deferred_Invoice__c = true;
        newContract.Deferred_Invoice_Date__c = date.today().addDays(100);   
        newContract.Delayed_Billing_Only__c = true;
        newContract.Delayed_Billing_Date__c = date.today().addDays(100);    
        newContract.Installments__c = true;
        newContract.Opt_out__c = true;
        newContract.Non_Standard_Service_Approval__c = true;
        newContract.Approve_Sales__c = true;
        newContract.Approve_Finance__c = true;
        newContract.Approve_Authoring__c = true;
        newContract.Approve_Orders__c = true;
        newContract.Approve_Operations__c = true;
        newContract.Modified_Down_Payment__c = true;
        newContract.US_CS_Approval__c = true;
        newContract.Webpliance_PPI_Approval__c = true;
        newContract.Custom_Payment_Net_Terms__c = true;
        newContract.Payment_Net_Terms__c = 'Net 90';
        newContract.Quoted_Currency__c = 'EUR';
        newContract.Quoted_Currency_Rate__c = 0.2;
        newContract.Billing_Currency__c = 'EUR';
        newContract.Billing_Currency_Rate__c = 0.85;
        newContract.Billing_Currency_Rate_Date__c = date.today().addDays(-30);
        newContract.Unsigned_Contract__c = true;
        insert newContract;  
        
        // create a test product
        Product2 np1 = new Product2();
        np1.Name = 'Webpliance';
        np1.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np1.Contract_Print_Group__c = 'MSDS Management';
        insert np1;
        
        Product2 np2 = new Product2();
        np2.Name = 'GM Account';
        np2.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np2.Contract_Print_Group__c = 'MSDS Management';
        insert np2;
        
        Product2 np3 = new Product2();
        np3.Name = 'Custom Label';
        np3.Proposal_Print_Group__c = 'MSDS/ Chemical Management';
        np3.Contract_Print_Group__c = 'MSDS Management';
        insert np3;
        
        Product2 np4 = new Product2();
        np4.Name = 'eBinder Valet';
        np4.Proposal_Print_Group__c = 'Services';
        np4.Contract_Print_Group__c = 'Services';
        insert np4;
        
        // create a test line item
        Contract_Line_Item__c cli1 = new Contract_Line_Item__c();
        cli1.Name__c = 'Webpliance';
        cli1.Contract__c = newContract.Id;
        cli1.Product__c = np1.id;
        cli1.Sort_order__c = 1;
        cli1.Quantity__c = 10;
        cli1.Y1_List_Price__c = 5000;
        cli1.Approval_Required__c = true;
        insert cli1;
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Name__c = 'GM Account';
        cli2.Contract__c = newContract.Id;
        cli2.Product__c = np2.id;
        cli2.Sort_order__c = 2;
        cli2.Approval_Required__c = false;
        cli2.Contract_Terms__c = 'test terms';
        cli2.Original_Char_Count__c = 1;
        insert cli2;
        
        // create a test line item
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Name__c = 'Custom Label';
        cli3.Contract__c = newContract.Id;
        cli3.Product__c = np3.id;
        cli3.Sort_order__c = 3;
        cli3.Approval_Required__c = true;
        insert cli3;
        
        Contract_Line_Item__c cli4 = new Contract_Line_Item__c();
        cli4.Name__c = 'eBinder Valet';
        cli4.Contract__c = newContract.Id;
        cli4.Product__c = np4.id;
        cli4.Sort_order__c = 4;
        cli4.Approval_Required__c = true;
        insert cli4;
        
        Contract_Line_Item__c cli5 = new Contract_Line_Item__c();
        cli5.Name__c = 'Air';
        cli5.Contract__c = newContract.Id;
        cli5.Product__c = [SELECT id FROM product2 WHERE name = 'Air' and Product_Package__c = false limit 1].id;
        cli5.Sort_order__c = 5;
        cli5.Approval_Required__c = true;
        insert cli5;
        
        retentionContractApproval rca = new retentionContractApproval();
        rca.check(newContract.id, false); 
        
        rca = new retentionContractApproval();
        rca.check(newContract.id, true); 
        
        salesContractApproval sca = new salesContractApproval();
        sca.check(newContract.id, false); 
        
        sca = new salesContractApproval();
        sca.check(newContract.id, true); 
        
        
    }
}