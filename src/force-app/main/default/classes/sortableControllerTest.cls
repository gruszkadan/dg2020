@isTest()
private class sortableControllerTest {
    
    static testmethod void test1() {
        
        string oid = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        
        Salesforce_Request__c req1 = new Salesforce_Request__c();
        req1.Summary__c = 'test1';
        req1.Status__c = 'Requested';
        req1.Description__c = 'test description';
        req1.Ownerid = oid;
        req1.Requested_By__c = oid;
        insert req1;
        
        Salesforce_Request__c req2 = new Salesforce_Request__c();
        req2.Summary__c = 'test2';
        req2.Status__c = 'Scoping';
        req2.Description__c = 'test description';
        req2.Ownerid = oid;
        req2.Requested_By__c = oid;
        insert req2;
        
        Salesforce_Request__c req3 = new Salesforce_Request__c();
        req3.Summary__c = 'test3';
        req3.Status__c = 'In Development';
        req3.Description__c = 'test description';
        req3.Ownerid = oid;
        req3.Requested_By__c = oid;
        insert req3;
        
        sortableController2 con = new sortableController2();
        con.refreshRequests();
        
    }
    
}