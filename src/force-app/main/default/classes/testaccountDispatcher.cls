@istest(SeeAllData=true)
private class testaccountDispatcher{
    
    public static testmethod void RegularTests() {        
        Test.startTest();
        
        runTest1();
        
        Test.stopTest();
    }
    
    public static void runTest1() {
        
        Account a = new Account();
        a.Name = 'Account 1';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Contact 1';
        c.AccountId = a.Id;
        insert c;
        
        
        ApexPages.currentPage().getParameters().put('id', a.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        accountDispatcher ad = new accountDispatcher(testController);
        
        
        
        ad.redirect();
        ad.redirect2();
        ad.gotoStandard();
        ad.gotoEditStandard();
        ad.gotoMin();
        ad.gotoSales();
        ad.gotoEditSales();
    }
        
        
        
    }