public class sfUsage_add 
{
    public User u {get;set;}
    public SF_Usage__c sfu {get;set;}
    public Integer useSize {get;set;}
    public String[] appList {get;set;}
    public String soql {get;set;}
    
    public sfUsage_add()
    {
        u = [ SELECT Id, Name, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, Role__c, ProfileID, Profile.Name 
             FROM User 
             WHERE Id =: UserInfo.getUserId() LIMIT 1 ]; 
        String SobjectApiName = 'SF_Usage__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get( SobjectApiName ).getDescribe().fields.getMap();
        String commaSepratedFields = '';
        for( String fieldName : fieldMap.keyset() )
        {
            if( commaSepratedFields == null || commaSepratedFields == '' )
            {
                commaSepratedFields = fieldName;
            }
            else
            {
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        soql = 'SELECT ' + commaSepratedFields + ' FROM ' + SobjectApiName + ' WHERE Name =\''+u.Name+ '\' LIMIT 1';
    }
    
    public void addView( String app )
    {
        checkUse();
        String var1 = app+'_Views__c';
        String var2 = app+'_Last_Use__c';
        SF_Usage__c sfu = Database.query( soql );
        Integer iX = Integer.valueOf( sfu.get( var1 ) );
        sfu.put( var1, iX+1 );
        sfu.put( var2, Date.TODAY() );
        update sfu;
    }
    
    public void addSearch( String app )
    {
        checkUse();
        String var1 = app+'_Searches__c';
        String var2 = app+'_Last_Use__c';
        SF_Usage__c sfu = Database.query( soql );
        Integer iX = Integer.valueOf( sfu.get( var1 ) );
        sfu.put( var1, iX+1 );
        sfu.put( var2, Date.TODAY() );
        update sfu;
    }
    
    public void checkUse()
    {
        Integer useSize = [ SELECT Id FROM SF_Usage__c WHERE Name =: u.Name ].size();
        if( useSize == 0 )
        {
            SF_Usage__c nu = new SF_Usage__c();
            nu.Name = u.Name;
            insert nu;
        }
    }
    
    
}