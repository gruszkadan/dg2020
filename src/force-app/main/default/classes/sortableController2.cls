public with sharing class sortableController2 {
    public User u {get;set;}
    public Salesforce_Request__c[] reqReqs {get;set;}
    public Salesforce_Request__c[] scopeReqs {get;set;}
    public Salesforce_Request__c[] devReqs {get;set;}
    public Salesforce_Request__c[] compReqs {get;set;}
    
    public sortableController2() {
        u = [SELECT id, Name, FirstName, Email, LastName, Managerid, Department__c, Group__c, Profileid FROM User WHERE id = :UserInfo.getUserid() LIMIT 1];
        reqReqs = new List<Salesforce_Request__c>();
        scopeReqs = new List<Salesforce_Request__c>();
        devReqs = new List<Salesforce_Request__c>();
        compReqs = new List<Salesforce_Request__c>();
        findRequests();
    }    
    
    public void findRequests() {
        reqReqs.clear(); scopeReqs.clear(); devReqs.clear(); compReqs.clear();
        for (Salesforce_Request__c req : [SELECT id, Ownerid, Name, Requested_By__c, Requested_Date__c, Request_Type__c, Sort_Order__c, Status__c, Summary__c 
                                          FROM Salesforce_Request__c 
                                          WHERE LastModifiedDate > :date.today().addDays(-90) AND Ownerid = :u.id AND Requested_By__c = :u.id
                                          ORDER BY Sort_Order__c]) {
                                              if (req.Status__c == 'Requested') {
                                                  reqReqs.add(req);
                                              }
                                              if (req.Status__c == 'Scoping') {
                                                  scopeReqs.add(req);
                                              }
                                              if (req.Status__c == 'In Development') {
                                                  devReqs.add(req);
                                              }
                                              if (req.Status__c == 'Completed') {
                                                  compReqs.add(req);
                                              }
                                          }
        
    }
    
    public void refreshRequests() {
        findRequests();
    }
    
}