public with sharing class msdsCustomSettings {
    public MSDS_Custom_Settings__c settings {get;set;}
    public User u {get;set;}
    boolean editActive {get;set;}
    
    
    public msdsCustomSettings() {
        u = [Select id, Name, FirstName, Email, ManagerID, Department__c, Group__c, ProfileID from User where id =: UserInfo.getUserId() LIMIT 1];
        settings = MSDS_Custom_Settings__c.getvalues(System.UserInfo.getProfileId());
    }
    
    public boolean editActiveCustomer() {
        return MSDS_Custom_Settings__c.getInstance().Edit_Active_Customers__c;
    }
    
    public boolean showQuotes() {
        return MSDS_Custom_Settings__c.getInstance().Show_Quotes__c;
    }
    
    public boolean showContracts() {
        return MSDS_Custom_Settings__c.getInstance().Show_Contracts__c;
    }
    
    public boolean showSalesInsight() {
        return MSDS_Custom_Settings__c.getInstance().Show_Sales_Insight__c;
    }
    
    public boolean editAccountOwnership() {
        return MSDS_Custom_Settings__c.getInstance().Can_Change_Account_Ownership__c;
    }
    
    public decimal defaultOpportunityAmount() {
        return MSDS_Custom_Settings__c.getInstance().Default_Opportunity_Amount__c;
    }
    
    public string defaultLeadSource() {
        return MSDS_Custom_Settings__c.getInstance().Default_Lead_Source__c;
    }
    
    public boolean ViewLDR() {
        return MSDS_Custom_Settings__c.getInstance().View_Sales_LDR_Page__c;
    }
    
    public boolean viewSales() {
        return MSDS_Custom_Settings__c.getInstance().View_Sales_Page__c;
    }
    
    public boolean ViewSurveys() {
        return MSDS_Custom_Settings__c.getInstance().Show_Surveys__c;
    }
    
    public string stypes() {
        return MSDS_Custom_Settings__c.getInstance().Customer_Stories_Story_Type_Picklist__c;
    }
    
    public Boolean viewAccountLocator(){
        return MSDS_Custom_Settings__c.getInstance().View_Account_Locator__c;
    }
    
    public Boolean viewDataDotCom(){
        return MSDS_Custom_Settings__c.getInstance().View_Data_com_Button__c;
    }
    
    public Boolean viewMonthlyReview(){
        return MSDS_Custom_Settings__c.getInstance().View_MonthlyReview__c;
    }
    
    public Boolean cloneCases(){
        return MSDS_Custom_Settings__c.getInstance().Can_Clone_Cases__c;
    }
    
    public Boolean viewActivateProducts(){
        return MSDS_Custom_Settings__c.getInstance().View_Activate_Products__c;
    }
    
    public Boolean viewCustomerBillingRates(){
        return MSDS_Custom_Settings__c.getInstance().View_Customer_Billing_Rates__c;
    }
    
    public Boolean editCustomerBillingRates(){
        return MSDS_Custom_Settings__c.getInstance().Edit_Customer_Billing_Rates__c;
    }
    
    public Boolean viewSynopsis(){
        return MSDS_Custom_Settings__c.getInstance().View_Synopsis__c;
    }
    
    public Boolean editSynopsis(){
        return MSDS_Custom_Settings__c.getInstance().Edit_Synopsis__c;
    }
    
    public Boolean viewSynopsisMSDS(){
        return MSDS_Custom_Settings__c.getInstance().View_Synopsis_MSDS__c;
    }
    
    public Boolean editSynopsisMSDS(){
        return MSDS_Custom_Settings__c.getInstance().Edit_Synopsis_MSDS__c;
    }
    
    public string notificationGroups(){
        return MSDS_Custom_Settings__c.getInstance().Notification_Groups__c;
    }
    
    public Boolean changeOfContact(){
        return MSDS_Custom_Settings__c.getInstance().Change_of_Contact_Button__c;
    }
    
    public Boolean editCustomerSuccess(){
        return MSDS_Custom_Settings__c.getInstance().Edit_Customer_Success__c;
    }
    
    public Boolean editSAM(){
        return MSDS_Custom_Settings__c.getInstance().Can_Change_SAM_Owner__c;
    }
    
}