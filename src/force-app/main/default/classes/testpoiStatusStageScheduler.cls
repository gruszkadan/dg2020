@isTest(seeAllData=true)
private class testpoiStatusStageScheduler {
    
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test_status_resolvedUnableToConnect() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        c.POI_Status_MSDS__c = 'Working - Pending Response';
        c.POI_Status_Change_Date_MSDS__c = date.today().addDays(-180);
        c.Num_Of_Unable_To_Connect_Tasks_MSDS__c = 5;
        c.POI_Status_EHS__c = 'Working - Pending Response';
        c.POI_Status_Change_Date_EHS__c = date.today().addDays(-180);
        c.Num_Of_Unable_To_Connect_Tasks_EHS__c = 5;
        c.POI_Status_AUTH__c = 'Working - Pending Response';
        c.POI_Status_Change_Date_AUTH__c = date.today().addDays(-180);
        c.Num_Of_Unable_To_Connect_Tasks_AUTH__c = 5;
        c.POI_Status_ODT__c = 'Working - Pending Response';
        c.POI_Status_Change_Date_ODT__c = date.today().addDays(-180);
        c.Num_Of_Unable_To_Connect_Tasks_ODT__c = 5;
        c.POI_Status_ERGO__c = 'Working - Pending Response';
        c.POI_Status_Change_Date_ERGO__c = date.today().addDays(-180);
        c.Num_Of_Unable_To_Connect_Tasks_ERGO__c = 5;
        insert c;
        Lead l = testLead();
        l.POI_Status_MSDS__c = 'Working - Pending Response';
        l.POI_Status_Change_Date_MSDS__c = date.today().addDays(-180);
        l.Num_Of_Unable_To_Connect_Tasks_MSDS__c = 5;
        l.POI_Status_EHS__c = 'Working - Pending Response';
        l.POI_Status_Change_Date_EHS__c = date.today().addDays(-180);
        l.Num_Of_Unable_To_Connect_Tasks_EHS__c = 5;
        l.POI_Status_AUTH__c = 'Working - Pending Response';
        l.POI_Status_Change_Date_AUTH__c = date.today().addDays(-180);
        l.Num_Of_Unable_To_Connect_Tasks_AUTH__c = 5;
        l.POI_Status_ODT__c = 'Working - Pending Response';
        l.POI_Status_Change_Date_ODT__c = date.today().addDays(-180);
        l.Num_Of_Unable_To_Connect_Tasks_ODT__c = 5;
        l.POI_Status_ERGO__c = 'Working - Pending Response';
        l.POI_Status_Change_Date_ERGO__c = date.today().addDays(-180);
        l.Num_Of_Unable_To_Connect_Tasks_ERGO__c = 5;
        insert l;
         Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new poiStatusStageScheduler());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
       
        Test.stopTest();
    }
    
    static testmethod void test_stage_salesInactive() {
        Account a = testAccount();
        insert a; 
        Contact c = testContact(a.id);
        c.POI_Stage_MSDS__c = 'Sales Accepted';
        c.Last_Activity_Date_Chemical_Management__c = date.today().addDays(-180);
        c.POI_Stage_EHS__c = 'Sales Accepted';
        c.Last_Activity_Date_EHS__c = date.today().addDays(-180);
        c.POI_Stage_AUTH__c = 'Sales Accepted';
        c.Last_Activity_Date_Authoring__c = date.today().addDays(-180);
        c.POI_Stage_ODT__c = 'Sales Accepted';
        c.Last_Activity_Date_Online_Training__c = date.today().addDays(-180);
        c.POI_Stage_ERGO__c = 'Sales Accepted';
        c.Last_Activity_Date_Ergonomics__c = date.today().addDays(-180);
        insert c;
        Lead l = testLead();
        l.POI_Stage_MSDS__c = 'Sales Accepted';
        l.Last_Activity_Date_Chemical_Management__c = date.today().addDays(-180);
        l.POI_Stage_EHS__c = 'Sales Accepted';
        l.Last_Activity_Date_EHS__c = date.today().addDays(-180);
        l.POI_Stage_AUTH__c = 'Sales Accepted';
        l.Last_Activity_Date_Authoring__c = date.today().addDays(-180);
        l.POI_Stage_ODT__c = 'Sales Accepted';
        l.Last_Activity_Date_Online_Training__c = date.today().addDays(-180);
        l.POI_Stage_ERGO__c = 'Sales Accepted';
        l.Last_Activity_Date_Ergonomics__c = date.today().addDays(-180);
        insert l;
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new poiStatusStageScheduler());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        
        Test.stopTest();
    }
    
    public static Account testAccount() {
        return new Account(Name='Test', NAICS_Code__c='Test');
    }
    
    public static Contact testContact(id accountId) {
        return new Contact(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', AccountId=accountId);
    }
    
    public static Lead testLead() {
        return new Lead(FirstName='Test', LastName='Test', Company='TestCompany', Communication_Channel__c='Inbound Call');
    }

}