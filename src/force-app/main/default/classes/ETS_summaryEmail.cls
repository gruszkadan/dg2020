public class ETS_summaryEmail implements Schedulable {
    // send weekly email out monday
    
    // init dates to put on email
    date myDate;
    string sDate;
    string eDate;
    string week;
    
    public void execute(SchedulableContext ctx) {
        myDate = date.today();
        sDate = (myDate - 8).month()+'/'+(myDate - 8).day();
        eDate = (myDate - 2).month()+'/'+(myDate - 2).day();
        week = sDate+' - '+eDate;
        string urlPrefix = URL.getSalesforceBaseUrl().toExternalForm();
        
        // query list of email receipients and pass to each method
        Survey_Settings__c[] emailSettings = [SELECT id, SetupOwnerid, Onboarding_To__c, Onboarding_CC__c, Services_To__c, Services_CC__c, Support_To__c, Support_CC__c
                                              FROM Survey_Settings__c];
        onboardingEmail(emailSettings, urlPrefix); 
        servicesEmail(emailSettings, urlPrefix);
        supportEmail(emailSettings, urlPrefix);
    }
    
    public void onboardingEmail(Survey_Settings__c[] recipients, string urlPrefix1) {
        // query all negative onboarding surveys created last week
        Survey_Feedback__c[] survs = [SELECT id, Name, Account__r.Name, Contact__r.Name, Case__r.id, Case__r.CaseNumber, Start_Date__c, Survey_Completed_Date__c
                                      FROM Survey_Feedback__c WHERE (Onboarding_Experience_Satisfaction__c = 'Very Dissatisfied' OR Onboarding_Experience_Satisfaction__c = 'Dissatisfied') AND RecordType.Name = 'ETS - Onboarding' AND Survey_Completed_Date__c = LAST_WEEK 
                                      ORDER BY Survey_Completed_Date__c ASC];  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        Set<id> toSet = new Set<id>();
        Set<id> ccSet = new Set<id>();
        // loop through email recipients and add userid's to appropriate set
        for (Survey_Settings__c rec : recipients) {
            if (rec.Onboarding_To__c == true) {
                toSet.add(rec.SetupOwnerid);
            }
            if (rec.Onboarding_CC__c == true) {
                ccSet.add(rec.SetupOwnerid);
            }
        }
        string[] toAddresses = new List<string>(); 
        string[] ccAddresses = new List<string>();
        // query user emails based on what userids were added to sets
        if (toSet.size() > 0) {
            for (User u : [SELECT id, Email FROM User WHERE id IN :toSet]) {
                toAddresses.add(u.Email);
            }
        } else {
            toAddresses.add([SELECT Email FROM User WHERE LastName = 'McCauley' LIMIT 1].Email);
        }
        
        if (ccSet.size() > 0) {
            for (User u : [SELECT id, Email FROM User WHERE id IN :ccSet]) {
                ccAddresses.add(u.Email);
            }
        }
        mail.setToAddresses(toAddresses);
        mail.setCCAddresses(ccAddresses);
        mail.setReplyTo('salesforceops@ehs.com');
        mail.setSubject('Onboarding Surveys - Weekly Summary '+week);
        string html = '<style>table, td {border-bottom:1px solid black;border-collapse:collapse;}</style>';
        html += '# OF VERY DISSATISFIED ONBOARDING SURVEYS: '+survs.size();
        if (survs.size()> 0) {
            html += '<br/><br/><table cellpadding="5"><tr><td><b>Account</b></td><td><b>Contact</b></td><td><b>Case #</b></td><td><b>Sent</b></td><td><b>Completed</b></td><td><b>Survey</b></td></tr>';
            for (Survey_Feedback__c surv : survs) {
                html += '<tr><td>'+surv.Account__r.Name+'</td><td>'+surv.Contact__r.Name+'</td><td><a href="'+urlPrefix1+'/'+surv.Case__r.id+'">'+surv.Case__r.CaseNumber+'</a></td><td>'+
                    string.valueOf(surv.Start_Date__c)+'</td><td>'+string.valueOf(surv.Survey_Completed_Date__c)+'</td><td><a href="'+urlPrefix1+'/'+surv.id+'">'+surv.Name+'</a></td></tr>';
            }
            html += '</table>';
        }
        mail.setHtmlBody(html);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public void servicesEmail(Survey_Settings__c[] recipients, string urlPrefix1) {
        Survey_Feedback__c[] survs = [SELECT id, Name, Account__r.Name, Contact__r.Name, Case__r.id, Case__r.CaseNumber, Start_Date__c, Survey_Completed_Date__c, Primary_MSDS_Management_Product__c
                                      FROM Survey_Feedback__c WHERE Services_Project_Experience_Satisfaction__c = 'Very Dissatisfied' AND RecordType.Name = 'ETS - Services' AND Survey_Completed_Date__c = LAST_WEEK 
                                      ORDER BY Survey_Completed_Date__c ASC];  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        Set<id> toSet = new Set<id>();
        Set<id> ccSet = new Set<id>();
        for (Survey_Settings__c rec : recipients) {
            if (rec.Services_To__c == true) {
                toSet.add(rec.SetupOwnerid);
            }
            if (rec.Services_CC__c == true) {
                ccSet.add(rec.SetupOwnerid);
            }
        }
        string[] toAddresses = new List<string>(); 
        string[] ccAddresses = new List<string>();
        if (toSet.size() > 0) {
            for (User u : [SELECT id, Email FROM User WHERE id IN :toSet]) {
                toAddresses.add(u.Email);
            }
        } else {
            toAddresses.add([SELECT Email FROM User WHERE LastName = 'McCauley' LIMIT 1].Email);
        }
        if (ccSet.size() > 0) {
            for (User u : [SELECT id, Email FROM User WHERE id IN :ccSet]) {
                ccAddresses.add(u.Email);
            }
        }
        mail.setToAddresses(toAddresses);
        mail.setCCAddresses(ccAddresses);
        mail.setReplyTo('salesforceops@ehs.com');
        mail.setSubject('Services Surveys - Weekly Summary '+week);
        string html = '<style>table, td {border-bottom:1px solid black;border-collapse:collapse;}</style>';
        html += '# OF VERY DISSATISFIED SERVICES SURVEYS: '+survs.size();
        if (survs.size()> 0) {
            html += '<br/><br/><table cellpadding="5"><tr><td><b>Account</b></td><td><b>Contact</b></td><td><b>Primary MSDS Management Product(s)</b></td><td><b>Case #</b></td><td><b>Sent</b></td><td><b>Completed</b></td><td><b>Survey</b></td></tr>';
            for (Survey_Feedback__c surv : survs) {
                html += '<tr><td>'+surv.Account__r.Name+'</td><td>'+surv.Contact__r.Name+'</td><td>'+surv.Primary_MSDS_Management_Product__c+'</td><td><a href="'+urlPrefix1+'/'+surv.Case__r.id+'">'+surv.Case__r.CaseNumber+'</a></td><td>'+
                    string.valueOf(surv.Start_Date__c)+'</td><td>'+string.valueOf(surv.Survey_Completed_Date__c)+'</td><td><a href="'+urlPrefix1+'/'+surv.id+'">'+surv.Name+'</a></td></tr>';
            }
            html += '</table>';
        }
        mail.setHtmlBody(html);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public void supportEmail(Survey_Settings__c[] recipients, string urlPrefix1) {
        Survey_Feedback__c[] survs = [SELECT id, Name, Account__r.Name, Contact__r.Name, Case__r.id, Case__r.CaseNumber, Start_Date__c, Survey_Completed_Date__c, Primary_MSDS_Management_Product__c
                                      FROM Survey_Feedback__c WHERE (Support_Satisfaction__c = 'Very Dissatisfied' OR Support_Satisfaction__c = 'Dissatisfied') AND RecordType.Name = 'ETS - Support' AND Survey_Completed_Date__c = LAST_WEEK 
                                      ORDER BY Survey_Completed_Date__c ASC];  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        Set<id> toSet = new Set<id>();
        Set<id> ccSet = new Set<id>();
        for (Survey_Settings__c rec : recipients) {
            if (rec.Support_To__c == true) {
                toSet.add(rec.SetupOwnerid);
            }
            if (rec.Support_CC__c == true) {
                ccSet.add(rec.SetupOwnerid);
            }
        }
        string[] toAddresses = new List<string>(); 
        string[] ccAddresses = new List<string>();
        if (toSet.size() > 0) {
            for (User u : [SELECT id, Email FROM User WHERE id IN :toSet]) {
                toAddresses.add(u.Email);
            }
        } else {
            toAddresses.add([SELECT Email FROM User WHERE LastName = 'McCauley' LIMIT 1].Email);
        }
        if (ccSet.size() > 0) {
            for (User u : [SELECT id, Email FROM User WHERE id IN :ccSet]) {
                ccAddresses.add(u.Email);
            }
        }
        mail.setToAddresses(toAddresses);
        mail.setCCAddresses(ccAddresses);
        mail.setReplyTo('salesforceops@ehs.com');
        mail.setSubject('Support Surveys - Weekly Summary '+week);
        string html = '<style>table, td {border-bottom:1px solid black;border-collapse:collapse;}</style>';
        html += '# OF VERY DISSATISFIED SUPPORT SURVEYS: '+survs.size();
        if (survs.size()> 0) {
            html += '<br/><br/><table cellpadding="5"><tr><td><b>Account</b></td><td><b>Contact</b></td><td><b>Primary MSDS Management Product(s)</b></td><td><b>Case #</b></td><td><b>Sent</b></td><td><b>Completed</b></td><td><b>Survey</b></td></tr>';
            for (Survey_Feedback__c surv : survs) {
                html += '<tr><td>'+surv.Account__r.Name+'</td><td>'+surv.Contact__r.Name+'</td><td>'+surv.Primary_MSDS_Management_Product__c+'</td><td><a href="'+urlPrefix1+'/'+surv.Case__r.id+'">'+surv.Case__r.CaseNumber+'</a></td><td>'+
                    string.valueOf(surv.Start_Date__c)+'</td><td>'+string.valueOf(surv.Survey_Completed_Date__c)+'</td><td><a href="'+urlPrefix1+'/'+surv.id+'">'+surv.Name+'</a></td></tr>';
            }
            html += '</table>';
        }
        mail.setHtmlBody(html);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}