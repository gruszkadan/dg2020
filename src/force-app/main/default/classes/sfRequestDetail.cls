public class sfRequestDetail {
    
    private ApexPages.StandardController controller {get; set;}
    public Salesforce_Request__c sfr {get; set;}
    public Salesforce_Request_Task__c[] reqTask {get; set;}
    public Salesforce_Request_Note__c[] reqNote {get; set;}
    public Salesforce_Request_Test_Scenario__c[] reqScen {get; set;}
    public Salesforce_Request_Task__c newReqTask {get;set;}
    public List<Salesforce_Request_Task__c> openTask {get;set;}
    public Salesforce_Request_Test_Scenario__c[] theRequestScenarios {get;set;}
    public Salesforce_Request_Component__c[] theRequestComponents {get;set;}
    public User[] UserPicture {get;set;}
    public Attachment[] attachment {get;set;}
    public id sfrId {get; set;}
    public string pagetype {get;set;}
    public string sortOrder {get;set;}
    public string src_soql {get;set;}
    public User u {get;set;}
    public Integer numPass {get;set;}
    public Integer numFail {get;set;}
    public Integer numNull {get;set;}
    public Salesforce_Request_Task__c lastOpenTask {get; set;}
    public decimal danTotalMin {get;set;}
    public decimal alTotalMin {get;set;}
    public decimal taraTotalMin {get;set;}
    public decimal markTotalMin {get;set;}
    public decimal macTotalMin {get;set;}
    Public decimal priyankaTotalMin {get;set;}
    public decimal danTotalHour {get;set;}
    public decimal alTotalHour {get;set;}
    public decimal taraTotalHour {get;set;}
    public decimal markTotalHour {get;set;}
    public decimal macTotalHour {get;set;}
    Public decimal priyankaTotalHour {get;set;}
    public String danLastWorked {get;set;}
    public String alLastWorked {get;set;}
    public String taraLastWorked {get;set;}
    public String markLastWorked {get;set;}
    public String macLastWorked {get;set;}
    public String priyankaLastWorked {get;set;}
    public decimal alP {get;set;}
    public decimal danP {get;set;}
    public decimal taraP {get;set;}
    public decimal markP {get;set;}
    public decimal macP {get;set;}
    Public decimal priyankaP {get;set;}
    public String alPic {get;set;}
    public String danPic {get;set;}
    public String taraPic {get;set;}
    public String markPic {get;set;}
    public String macPic {get;set;}
    Public String priyankaPic {get;set;}
    public User uOwner {get;set;}
   
    
    
    
    public sfRequestDetail(ApexPages.StandardController controller) { 
        
        this.controller = controller;
        sfrId = ApexPages.currentPage().getParameters().get('id'); 
        u = [Select id, FullPhotoURL, Name, LastName, Role__c, SF_Request_Owner_Change__c, SF_Request_Status_Change__c, Profile.Name from User where id =: UserInfo.getUserId() LIMIT 1];
        queryRequest();
        reqTask = [Select ID, Name, Work_Request__c, Start_Date__c, End_Date__c, Owner__c, Minutes_Worked__c, Owner__r.Name 
                   from Salesforce_Request_Task__c where Work_Request__c =: sfr.Id order by End_Date__c DESC nulls first limit 950];
        reqNote = [SELECT ID, Name, Comments__c, CreatedBy.Name, CreatedDate FROM Salesforce_Request_Note__c WHERE Salesforce_Update__c =: sfr.Id];
        reqScen = [SELECT ID, Step__c, Name, Owner__r.Name, Result__c FROM Salesforce_Request_Test_Scenario__c WHERE Salesforce_Request__c =: sfr.Id ORDER BY Step__c asc limit 950];
         src_soql = 'SELECT Salesforce_Component__c, Salesforce_Request__c, Notes__c, Name, Id, Deployed__c, Deployed_to_Production_Staging__c, Deployed_to_MSDS_Dev__c,Deployed_to_MSDS_Test__c, '+
             'Salesforce_Component__r.Num_of_Open_Requests__c, Salesforce_Component__r.Name, Type__c, Object__c, Name__c '+
             'from Salesforce_Request_Component__c where Salesforce_Request__c =:sfrId ';
        sortOrder = 'ORDER BY Type__c ASC';
        theRequestComponents = Database.query (src_soql+sortOrder+' NULLS LAST');
        attachment = [SELECT ID, Name, LastModifiedDate, OwnerId FROM Attachment WHERE ParentId =: sfr.Id];
        UserPicture = [SELECT Name, SmallPhotoUrl, FullPhotoUrl FROM user WHERE Profile.Name = 'SF Operations'];
        if(string.valueof(sfr.OwnerId).startsWith('005')){
            uOwner = [SELECT FullPhotoUrl FROM User WHERE Id = :sfr.OwnerId];

        }
        
        
        openTask = new List<Salesforce_Request_Task__c>();

        if (reqTask.size() > 0) {
            for (Salesforce_Request_Task__c t: reqTask) {
                if (t.End_Date__c == NULL && t.Owner__c == u.Id){
                    openTask.add(t); 
                }
            }
        }
        
        if(openTask.size()>0){
            lastOpenTask = [Select ID, Work_Request__c, Start_Date__c, End_Date__c from Salesforce_Request_Task__c where
                            Work_Request__c =: sfr.Id and End_Date__c = NULL and Owner__c = :u.Id ORDER BY Start_Date__c desc limit 1];
        }
       
        newReqTask = new Salesforce_Request_Task__c();

                      
        calculateTeamBreakdown();
        insertPictures();
        countScenario(); 
    }
    
        public void queryRequest() {
        string SObjectAPIName = 'Salesforce_Request__c';
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string query = 'SELECT '+commaSepratedFields+' FROM '+SObjectAPIName+' WHERE id = \''+sfrId+'\' ';
        sfr = Database.query(query);
    }
    
   public void reorderComps() {
        theRequestComponents = Database.query (src_soql+sortOrder+' NULLS LAST');
         }

     public void acceptOwnershipChange () {
        if (sfr.Id != null ) {
            try {
                Salesforce_Request__c sfrOwner = [select Id, OwnerID from Salesforce_Request__c where Id=:sfr.Id];
                sfrOwner.OwnerID = u.Id;
                update sfrOwner;
            }
            catch(Exception e){
                ApexPages.addMessages(e);
            }
        } 
    }
     public PageReference acceptOwnership() {
        acceptOwnershipChange();
        PageReference sf_Request_detail = Page.sf_Request_detail;
        sf_Request_detail.setRedirect(true);
        sf_Request_detail.getParameters().put('id',sfr.Id); 
        return sf_Request_detail; 
            
    }
    
        public PageReference completeMerge(){
        acceptOwnershipChange();
        acceptOwnership();
        sfr.Status__c = 'Completed';
        sfr.OwnerId = u.id;
        sfr.completed_Date__c = date.TODAY();
        update sfr;
        PageReference p1 = Page.SfRequest_detail;
        p1.setredirect(true);
        p1.getParameters().put('id',sfr.Id);
        return p1;
        }
    
    public PageReference startTimer() {
        try {
            newReqTask.Work_Request__c = sfr.Id;
            newReqTask.Owner__c = u.Id;
            newReqTask.Start_Date__c = system.Datetime.now();
            insert newReqTask;
            if(sfr.Start_Date__c == NULL){
                sfr.Start_Date__c = date.today();
                update sfr;
            }
        } 
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        PageReference sf_Request_detail = Page.sf_Request_detail;
        sf_Request_detail.setRedirect(true);
        sf_Request_detail.getParameters().put('id',sfr.Id); 
        return sf_Request_detail; 
    }
     
    public PageReference endTimer() {
        try {
            lastOpenTask.End_Date__c = system.Datetime.now();
            update lastOpenTask;
        } 
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        PageReference sf_Request_detail = Page.sf_Request_detail;
        sf_Request_detail.setRedirect(true);
        sf_Request_detail.getParameters().put('id',sfr.Id); 
        return sf_Request_detail;  
        
    }
    
    public PageReference completeButton(){
        controller.save();
        sfr.Status__c = 'Completed';
        sfr.Completed_Date__c = Date.today();
        if(openTask.size() > 0){
            if(lastOpenTask.Start_Date__c != NULL && lastOpenTask.End_Date__c == NULL){
                lastOpenTask.End_Date__c = DateTime.now();
                update lastOpenTask;
            }
        }
        if(sfr.Request_Type__c == 'Admin'){
            if(sfr.Admin_Work_Type__c != NULL){
                update sfr;
                return NULL;
            }
            else{
                
                return NULL;
            }
        }
        else{
            update sfr;
            return NULL;
        }
    }
    
    
    
    public PageReference addScenario() {
        PageReference sfRequestTestScenario_add_edit = Page.sfRequestTestScenario_add_edit;
        sfRequestTestScenario_add_edit.setRedirect(true);
        sfRequestTestScenario_add_edit.getParameters().put('id',sfr.Id); 
        return sfRequestTestScenario_add_edit;        
    } 
    public PageReference scenarioDetail() {
        id scenid = System.currentPageReference().getParameters().get('scenid');
        PageReference sfRequestTestScenario_detail = Page.sfRequestTestScenario_detail;
        sfRequestTestScenario_detail.setRedirect(true);
        sfRequestTestScenario_detail.getParameters().put('id',sfr.id);
        sfRequestTestScenario_detail.getParameters().put('scenid',scenid);
        return sfRequestTestScenario_detail;
    } 
    
    public PageReference scenDetailReorder(){
        PageReference sfRequestTestScenario_reorder = Page.sfRequestTestScenario_reorder;
        sfRequestTestScenario_reorder.setRedirect(true);
        sfRequestTestScenario_reorder.getParameters().put('id',sfr.Id); 
        return sfRequestTestScenario_reorder; 
    }
       public void saveComponentEdits() {
        update theRequestComponents;
    }
    
    public void countScenario() {
        numPass = 0;
        numFail = 0;
        numNull = 0;
        for (Salesforce_Request_Test_Scenario__c scen : reqScen) {
            if (scen.Result__c == 'Pass') {
                numPass++;
            }if (scen.Result__c == 'Fail') {
                numFail++;
            }if (scen.Result__c == null) {
                numNull++;
            }
        }
    }
    
    public void calculateTeamBreakdown(){
        
        danTotalMin = 0;
        alTotalMin = 0;
        taraTotalMin = 0;
        markTotalMin = 0;
        macTotalMin = 0;
        priyankaTotalMin =0;
        
        
        if(reqTask.size() > 0){
            List<Salesforce_Request_Task__c> danTasks = new List<Salesforce_Request_Task__c>();
            List<Salesforce_Request_Task__c> alTasks = new List<Salesforce_Request_Task__c>();
            List<Salesforce_Request_Task__c> taraTasks = new List<Salesforce_Request_Task__c>();
            List<Salesforce_Request_Task__c> markTasks = new List<Salesforce_Request_Task__c>();
            List<Salesforce_Request_Task__c> macTasks = new List<Salesforce_Request_Task__c>();
            List<Salesforce_Request_Task__c> priyankaTasks = new List<Salesforce_Request_Task__c>();

            for(Salesforce_Request_Task__c t : reqTask){
                if(t.Owner__r.Name == 'Daniel Gruszka' && t.Minutes_worked__c != null){  
                    danTotalMin += t.Minutes_Worked__c;
                    danTasks.add(t);
                    
                }
                if(t.Owner__r.Name == 'Al Powell' && t.Minutes_worked__c != null){   
                    alTotalMin += t.Minutes_Worked__c; 
                    alTasks.add(t);
                }
                if(t.Owner__r.Name == 'Tara Wills' && t.Minutes_worked__c != null){   
                    taraTotalMin += t.Minutes_Worked__c;
                    taraTasks.add(t);
                }
                if(t.Owner__r.Name == 'Mark McCauley' && t.Minutes_worked__c != null){   
                    markTotalMin += t.Minutes_Worked__c;
                    markTasks.add(t);
                }
                 if(t.Owner__r.Name == 'Mac Nosek' && t.Minutes_worked__c != null){   
                    macTotalMin += t.Minutes_Worked__c;
                    macTasks.add(t);
                }   
                 if(t.Owner__r.Name == 'Priyanka Goriparthi' && t.Minutes_worked__c != null){   
                    priyankaTotalMin += t.Minutes_Worked__c;
                    priyankaTasks.add(t);
                } 
            }  
            
            danTotalHour = (danTotalMin / 60).setScale(2);
            alTotalHour = (alTotalMin / 60).setScale(2);
            taraTotalHour = (taraTotalMin / 60).setScale(2);
            markTotalHour = (markTotalMin / 60).setScale(2); 
            macTotalHour = (macTotalMin / 60).setScale(2); 
            priyankaTotalHour = (priyankaTotalMin / 60).setScale(2);
           
            
            if(danTasks.size() > 0){
                danLastWorked = danTasks[0].End_Date__c.format();
            }
            if(alTasks.size() > 0){
                alLastWorked = alTasks[0].End_Date__c.format();   
            }
            if(taraTasks.size() > 0){
                taraLastWorked = taraTasks[0].End_Date__c.format();    
            }
            if(markTasks.size() > 0){
                markLastWorked = markTasks[0].End_Date__c.format(); 
            }
            if(macTasks.size() > 0){
                macLastWorked = macTasks[0].End_Date__c.format(); 
            }
             if(priyankaTasks.size() > 0){
                priyankaLastWorked = priyankaTasks[0].End_Date__c.format(); 
            }
            
            
            decimal totalMin = danTotalMin + alTotalMin + taraTotalMin + markTotalMin + macTotalMin + priyankaTotalMin;
           
            if(totalMin != 0){
                
                decimal alPercent = (alTotalMin / totalMin)*100;
                alP = alPercent.setScale(2);
                decimal danPercent = (danTotalMin / totalMin)*100;
                danP = danPercent.setScale(2);
                decimal taraPercent = (taraTotalMin / totalMin)*100;
                taraP = taraPercent.setScale(2);
                decimal markPercent = (markTotalMin / totalMin)*100;
                markP = markPercent.setScale(2);
                decimal macPercent = (macTotalMin / totalMin)*100;
                macP = macPercent.setScale(2);
                decimal priyankaPercent = (priyankaTotalMin / totalMin)*100;
                priyankaP = priyankaPercent.setScale(2);
                
            }
            
         
        }
    }
    
    public PageReference deleteTask() {
        
        Id taskId = ApexPages.currentPage().getParameters().get('TaskId');
        
        for(Salesforce_Request_Task__c t: reqTask){
            if(t.Id == taskId){
                delete t;   
            }
        }
        PageReference sf_request_detail = Page.sf_request_detail;
        sf_request_detail.setRedirect(true);
        sf_request_detail.getParameters().put('id',sfr.Id); 
        return sf_request_detail; 
    }
   public PageReference deleteNote() {
        
        Id noteId = ApexPages.currentPage().getParameters().get('NoteId');
        
        for(Salesforce_Request_Note__c n: reqNote){         
            if(n.Id == noteId){
                delete n;
            }
            
        }
        
        PageReference sf_request_detail = Page.sf_request_detail;
        sf_request_detail.setRedirect(true);
        sf_request_detail.getParameters().put('id',sfr.Id); 
        return sf_request_detail; 
            
    }
    
   public PageReference deleteAttachment() {
         
        Id attachId = ApexPages.currentPage().getParameters().get('attachmentId');
        
        for(Attachment a : attachment){
            if(a.Id == attachId){
                delete a;  
            }  
        }
        PageReference sf_request_detail = Page.sf_request_detail;
        sf_request_detail.setRedirect(true);
        sf_request_detail.getParameters().put('id',sfr.Id); 
        return sf_request_detail;  
    }
    
    public void insertPictures(){
        
        For(User u : UserPicture){
            if(u.Name == 'Al Powell'){
                alPic = u.FullPhotoUrl;  
            }
            if(u.Name == 'Daniel Gruszka'){
                danPic = u.FullPhotoUrl;  
            }
            if(u.Name == 'Tara Wills'){  
                taraPic = u.FullPhotoUrl;   
            }
            if(u.Name == 'Mark McCauley'){  
                markPic = u.FullPhotoUrl;  
            } 
            if(u.Name == 'Mac Nosek'){  
                macPic = u.FullPhotoUrl;  
            } 
            if(u.Name == 'Priyanka Goriparthi'){  
                priyankaPic= u.FullPhotoUrl;  
            }
        }
    }
    
    public PageReference mergeRedirect(){
          
             if(sfr.Request_Type__c == 'Merge'){
                 
              PageReference sfRequest_detail = Page.sfRequest_detail;
              sfRequest_detail.setRedirect(true);
              sfrequest_detail.getParameters().put('id',sfr.Id); 
              return sfRequest_detail;
             }
        else{
            return null;
        }
    
    }
    
}