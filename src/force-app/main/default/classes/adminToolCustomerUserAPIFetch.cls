public class adminToolCustomerUserAPIFetch implements Schedulable{
/**
* This scheduled class starts the process to get updated Customer and User data from the Admin Tool
* This calls the adminToolCustomerApiQueueable
**/
	 public void execute(SchedulableContext SC) {
        System.enqueueJob(new adminToolCustomerApiQueueable());
    }
}