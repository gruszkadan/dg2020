@isTest(SeeAllData=true)
private class testContractNoteController {

    static testmethod void test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c=newAccount.Id;
        newContract.Contact__c=newContact.Id;
        newContract.OwnerID='00580000003UChI';
        insert newContract;
        
        Contract_Note__c newNote = new Contract_Note__c();
        newNote.Comments__c = 'testetstestsetsetse';
        newNote.Contract__c = newContract.Id;
        insert newNote;
        
        ApexPages.currentPage().getParameters().put('CF00N800000057HgX_lkid', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract_Note__c());
        contractNoteController myController = new contractNoteController(testController);
        myController.note.Contract__c = newContract.ID;
        myController.sendRepUpdateEmail();
        myController.onSave();
        myController.pendingOrdersContractStatus();
    }
    static testmethod void test2() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c=newAccount.Id;
        newContract.Contact__c=newContact.Id;
        newContract.OwnerID='00580000003UChI';
        newContract.Order_Submitted_By__c='00580000003UChI';
        insert newContract;
        
        Contract_Note__c newNote = new Contract_Note__c();
        newNote.Comments__c = 'testetstestsetsetse';
        newNote.Contract__c = newContract.Id;
        insert newNote;
        
        ApexPages.currentPage().getParameters().put('CF00N800000057HgX_lkid', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract_Note__c());
        contractNoteController myController = new contractNoteController(testController);
        myController.note.Contract__c = newContract.Id;
        myController.declineOrderSubmit();
        
    }
    
    static testmethod void test3() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c=newAccount.Id;
        newContract.Contact__c=newContact.Id;
        newContract.OwnerID='00580000003UChI';
        newContract.Contract_Type__c ='New';
        newContract.Order_Submitted_By__c ='00580000003UChI';
        insert newContract;
        
        Contract_Note__c newNote = new Contract_Note__c();
        newNote.Comments__c = 'testetstestsetsetse';
        newNote.Contract__c = newContract.Id;
        insert newNote;
        
        ApexPages.currentPage().getParameters().put('CF00N800000057HgX_lkid', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract_Note__c());
        contractNoteController myController = new contractNoteController(testController);
        myController.note.Contract__c = newContract.ID;
        myController.sendRepUpdateEmail();
        myController.onSave();
        myController.pendingOrdersContractStatus();
    }

    static testmethod void test4() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c=newAccount.Id;
        newContract.Contact__c=newContact.Id;
        newContract.OwnerID='00580000003UChI';
        insert newContract;
        
        ApexPages.currentPage().getParameters().put('CF00N800000057HgX_lkid', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract_Note__c());
        contractNoteController myController = new contractNoteController(testController);
        
        Contract_Note__c newNote = new Contract_Note__c();
        newNote.Comments__c = 'testetstestsetsetse';
        newNote.Contract__c = newContract.Id;
        
        myController.note.Contract__c = newContract.ID;
        
        myController.repUpdate();
    }
    static testmethod void test5() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c=newAccount.Id;
        newContract.Contact__c=newContact.Id;
        newContract.Order_Submitted_By__c = [SELECT id FROM User WHERE id = :UserInfo.getUserId() LIMIT 1].id;
        newContract.OwnerID= [SELECT QueueId FROM QueueSObject WHERE Queue.Name = 'Orders Queue' LIMIT 1].QueueId;
        insert newContract;
         
        Contract_Note__c newNote = new Contract_Note__c();
        newNote.Comments__c = 'testetstestsetsetse';
        newNote.Contract__c = newContract.Id;
        insert newNote;
        
        ApexPages.currentPage().getParameters().put('CF00N800000057HgX_lkid', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract_Note__c());
        contractNoteController myController = new contractNoteController(testController);
        myController.note.Contract__c = newContract.ID;
        
        myController.sendAlertEmailRenewal();
        myController.sendAlertEmailNew();
        
    }
}