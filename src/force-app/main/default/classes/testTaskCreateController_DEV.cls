@isTest
private class testTaskCreateController_DEV {
    static testmethod void test1() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        ApexPages.currentPage().getParameters().put('tsk12','Completed');
        ApexPages.currentPage().getParameters().put('whoid',newContact.id);
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new Task());
        taskCreateController_DEV myController = new taskCreateController_DEV(testController);
        myController.rURL = '/a.Id';
        myController.cType ='Follow-Up';
        myController.what = a.Id;
        myController.who = newContact.Id;
        myController.AccountId = a.id;
        myController.theContacts = new list<Contact>();
        myController.checkAccount();
        myController.save();
        myController.testSave();
        myController.updateTime();
        myController.getContactSelectList();
    }
    static testmethod void test2() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        ApexPages.currentPage().getParameters().put('tsk12','Completed');
        ApexPages.currentPage().getParameters().put('whoid',newContact.id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Task());
        taskCreateController_DEV myController = new taskCreateController_DEV(testController);
        myController.rURL = '/a.Id';
        myController.cType ='Follow-Up';
        myController.what = a.Id;
        myController.who = newContact.Id;
        myController.theContacts = new list<Contact>();
        myController.theContacts.add(newContact);
        myController.AccountId = a.id;
        myController.checkAccount();
        myController.saveNew();
    }
    static testmethod void test3() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        ApexPages.currentPage().getParameters().put('tsk12','Completed');
        ApexPages.currentPage().getParameters().put('whatid',a.id);
        ApexPages.currentPage().getParameters().put('whoid',newContact.id);
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new Task());
        taskCreateController_DEV myController = new taskCreateController_DEV(testController);
        myController.rURL = '/a.Id';
        myController.cType ='Follow-Up';
        myController.what = a.Id;
        myController.who = newContact.Id;
        myController.AccountId = a.id;
        myController.checkAccount();
        
        myController.saveNewEvent();
    }
}