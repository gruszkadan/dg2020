@isTest(SeeAllData=true)
public with sharing class test1YearOpptyCount { 
    static testMethod void test1() { 
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account 666666666776';
        newAccount.NAICS_Code__c = '12345';
        insert newAccount;
        
        Renewal__c newRenew = new Renewal__c();
        newRenew.Account__c = newAccount.ID;
        newRenew.Status__c = 'Active';
        insert newRenew;
        
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'Test Account 666666666776';
        newOppty.AccountID = newAccount.Id;
        newOppty.Type = 'Renewal';
        newOppty.Renewal__c = newRenew.Id;
        newOppty.CloseDate = System.today();
        newOppty.Term_in_Years__c = '1 Year';
        newOppty.StageName = 'Closed Won';
        newOppty.Closed_Reasons__c = 'Closed 5 Years';
        insert newOppty;
        
        Opportunity newOppty1 = new Opportunity();
        newOppty1.Name = 'Test Account 666666666776';
        newOppty1.AccountID = newAccount.Id;
        newOppty1.Type = 'Renewal';
        newOppty1.Term_in_Years__c = '3 Years';
        newOppty1.Renewal__c = newRenew.Id;
        newOppty1.CloseDate = System.today();
        newOppty1.StageName = 'New';
        insert newOppty1;
        
        
        
        Renewal__c renewal = [select id, status__c, Num_of_1_Year_Opportunities__c from Renewal__c where Id = :newRenew.Id]; 
        System.assertEquals(1,renewal.Num_of_1_Year_Opportunities__c); 
    }
    
    static testMethod void test2() { 
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account 666666666776';
        newAccount.NAICS_Code__c = '12345';
        insert newAccount;
        
        Renewal__c newRenew = new Renewal__c();
        newRenew.Account__c = newAccount.ID;
        newRenew.Status__c = 'Active';
        insert newRenew;
        
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'Test Account 666666666776';
        newOppty.AccountID = newAccount.Id;
        newOppty.Type = 'Renewal';
        newOppty.Renewal__c = newRenew.Id;
        newOppty.CloseDate = System.today();
        newOppty.Term_in_Years__c = '1 Year';
        newOppty.StageName = 'Closed Won';
        newOppty.Closed_Reasons__c = 'Closed 5 Years';
        insert newOppty;
        
        Opportunity newOppty1 = new Opportunity();
        newOppty1.Name = 'Test Account 666666666776';
        newOppty1.AccountID = newAccount.Id;
        newOppty1.Type = 'Renewal';
        newOppty1.Term_in_Years__c = '1 Year';
        newOppty1.Renewal__c = newRenew.Id;
        newOppty1.CloseDate = System.today();
        newOppty1.StageName = 'New';
        insert newOppty1;
        
        
        delete newOppty1;
        
        Renewal__c renewal = [select id, status__c, Num_of_1_Year_Opportunities__c from Renewal__c where Id = :newRenew.Id]; 
        System.assertEquals(1,renewal.Num_of_1_Year_Opportunities__c); 
    }
}