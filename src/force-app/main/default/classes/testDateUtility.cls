@isTest (SeeAllData=true)
public class testDateUtility {
    
    static testmethod void test1() { 
       
		List<Holiday> holidays = [SELECT ActivityDate FROM Holiday];
		        Set<Date> holidaysSet = new Set<Date> ();
        for (Holiday currHoliday : holidays)
        {
            holidaysSet.add(currHoliday.ActivityDate);
        }
        
        date SundayDate = System.today().toStartOfWeek();
        //date SaturdayDate = SundayDate.addDays(6);
        date pastFridayDate = SundayDate.addDays(-2);
        date ThursdayDate = SundayDate.addDays(4);
        
        if(holidaysSet.contains(pastFridayDate) || holidaysSet.contains(ThursdayDate)){
            pastFridayDate = pastFridayDate.addDays(14);
            ThursdayDate = ThursdayDate.addDays(14);
        }
     
        
        //4 business days after Friday is Thursday
        date expectedDate = dateUtility.calculateBusinessDay(pastFridayDate, 4, holidays);
        System.assert(expectedDate == ThursdayDate);
        
        integer businessDays = dateUtility.numberOfBusinessDays(pastFridayDate, ThursdayDate, holidays);
        System.debug(businessDays);
  
        
    
    }
    
    static testmethod void test2() { 
        System.assert(dateUtility.MonthName(6, true) == 'Jun');
        System.assert(dateUtility.MonthName(6, false) == 'June');
        
        System.assert(dateUtility.MonthName(4, true) == 'Apr');
        System.assert(dateUtility.MonthName(4, false) == 'April');
        
        System.assert(dateUtility.MonthName(8, true) == 'Aug');
        System.assert(dateUtility.MonthName(8, false) == 'August');
        
        System.assert(dateUtility.MonthName(1, true) == 'Jan');
        System.assert(dateUtility.MonthName(1, false) == 'January');
        
        System.assert(dateUtility.MonthName(2, true) == 'Feb');
        System.assert(dateUtility.MonthName(2, false) == 'February');
        
        System.assert(dateUtility.MonthName(12, true) == 'Dec');
        System.assert(dateUtility.MonthName(12, false) == 'December');
        
        System.assert(dateUtility.MonthName(3, true) == 'Mar');
        System.assert(dateUtility.MonthName(3, false) == 'March');
        
        System.assert(dateUtility.MonthName(11, true) == 'Nov');
        System.assert(dateUtility.MonthName(11, false) == 'November');
        
        System.assert(dateUtility.MonthName(5, true) == 'May');
        
        System.assert(dateUtility.MonthName(9, true) == 'Sept');
        System.assert(dateUtility.MonthName(9, false) == 'September');
        
        System.assert(dateUtility.MonthName(10, true) == 'Oct');
        System.assert(dateUtility.MonthName(10, false) == 'October');
    }

}