/*
* Test class: testOrderItemTrigger
*/
public class orderItemTriggerHandler {
    
    public static void beforeInsert(Order_Item__c[] orderItem) {
        Order_Item__c[] orderItemsWithContractNumbers = new list<Order_Item__c>();
        for (Order_Item__c oi:orderItem){
            if (oi.Contract_Number__c != null) {
                orderItemsWithContractNumbers.add(oi);
            }
        }
        
        //Find the Order Items Account
        orderItemUtility.findAccount(orderItem);
        
        //Find the Order Items Sales Users
        orderItemUtility.findSalesUsers(orderItem);
        
        //Transform Admin Tool Data into Salesforce Data (Fields: Name, Admin_Tool_Order_Status__c)
        orderItemUtility.transformAdminToolData(orderItem);
        
        //Associate order items to opportunities based oon contract number
        if (orderItemsWithContractNumbers.size() > 0) {
            orderItemUtility.associateOpportunityFromContract(orderItemsWithContractNumbers);
        }
        
        
    }
    
    public static void beforeUpdate(Order_Item__c[] oldOrderItem, Order_Item__c[] newOrderItem) {
        Order_Item__c[] newInvoiceAmountChange= new list<Order_Item__c>();
        Order_Item__c[] oldInvoiceAmountChange= new list<Order_Item__c>();
        //Transform Admin Tool Data into Salesforce Data if any of the Admin Tool Data was Updated (Fields: Admin_Tool_Order_Status__c)
        Order_Item__c[] adminToolDataChange = new list<Order_Item__c>();
        Order_Item__c[] salesRepChange = new list<Order_Item__c>();
        Order_Item__c[] orderItemsWithContractNumbers = new list<Order_Item__c>();
        
        for (integer i=0; i<newOrderItem.size(); i++) {
            
            if (newOrderItem[i].Invoice_Amount__c != oldOrderItem[i].Invoice_Amount__c) {
                newInvoiceAmountChange.add(newOrderItem[i]);
                oldInvoiceAmountChange.add(oldOrderItem[i]);
            }
            
            //check to see if the Admin Tool Status Changed
            if (oldOrderItem[i].Admin_Tool_Order_Status__c != newOrderItem[i].Admin_Tool_Order_Status__c || oldOrderItem[i].Admin_Tool_Product_Name__c != newOrderItem[i].Admin_Tool_Product_Name__c) {
                adminToolDataChange.add(newOrderItem[i]);
            }
            
            if (newOrderItem[i].Admin_Tool_Sales_Rep__c != oldOrderItem[i].Admin_Tool_Sales_Rep__c && newOrderItem[i].Keep_Sales_Performance_Rep__c == false) {
                salesRepChange.add(newOrderItem[i]);
            }
            
            if (oldOrderItem[i].Contract_Number__c != newOrderItem[i].Contract_Number__c  && newOrderItem[i].Contract_Number__c != null && newOrderItem[i].Opportunity__c == null) {
                orderItemsWithContractNumbers.add(newOrderItem[i]);
            }
        }
        
        if (newInvoiceAmountChange.size() > 0) {
            orderItemUtility.invoiceChange(oldInvoiceAmountChange,newInvoiceAmountChange);
        }
        
        if(salesRepChange.size()>0){
            orderItemUtility.findSalesUsers(salesRepChange);
        }
        
        if(adminToolDataChange.size()>0){
            orderItemUtility.transformAdminToolData(adminToolDataChange);
        }
        
        //Associate order items to opportunities based on contract number
        if (orderItemsWithContractNumbers.size() > 0) {
            orderItemUtility.associateOpportunityFromContract(orderItemsWithContractNumbers);
        }
        
    }
    
    public static void afterInsert(Order_Item__c[] oldOrderItem, Order_Item__c[] newOrderItem) {
        Order_Item__c[] newItems = new list<Order_Item__c>();
        Order_Item__c[] activeItems = new list<Order_Item__c>();
        
        for (integer i=0; i<newOrderItem.size(); i++) {
            //Only add NON Renewal Order Items the list
            if (!newOrderItem[i].isRenewal__c && newOrderItem[i].Product_Platform__c != 'Ergo') { 
                // Only insert SPO/SPIs for 2017+
                if (integer.valueOf(newOrderItem[i].Year__c) >= 2017) {
                    newItems.add(newOrderItem[i]);
                }
            }
            if (newOrderItem[i].Admin_Tool_Order_Status__c == 'A' && newOrderItem[i].Product_Platform__c != null) {
                activeItems.add(newOrderItem[i]);
            }
        
        }
        if(newItems.size()>0){
            //Insert Sales Performance Order records
            salesPerformanceOrderUtility.insertSPO(newItems);
            
            //Insert Sales Performance Item Records
            salesPerformanceItemUtility.insertSPI(newItems);
        }
        if (activeItems.size() > 0) {
            orderItemUtility.subscriptionEndDate(activeItems);
        }
       
        accountProductInfoUpdater.updateProducts(newOrderItem,false);
        orderItemUtility.customerAnnualRevenue(newOrderItem);
        orderItemUtility.customerSinceDate(newOrderItem);
    }
    
    public static void afterUpdate(Order_Item__c[] oldOrderItem, Order_Item__c[] newOrderItem) {
        Order_Item__c[] invoiceStatusNew = new list<Order_Item__c>();
        Order_Item__c[] invoiceStatusOld = new list<Order_Item__c>();
        Order_Item__c[] statusChanges = new list<Order_Item__c>();
        Order_Item__c[] activeItems = new list<Order_Item__c>();
        Order_Item__c[] accountProductInfoUpdates = new list<Order_Item__c>();
        Order_Item__c[] accountProductInfoUpdatesAccountChange = new list<Order_Item__c>();
        Order_Item__c[] updateSPIs = new list<Order_Item__c>();
        Order_Item__c[] orderDateChanged = new list<Order_Item__c>();
        Order_Item__c[] repChanges = new list<Order_Item__c>();
        Order_Item__c[] changeToRenewal = new list<Order_Item__c>();
        Order_Item__c[] opportunityChanged = new list<Order_Item__c>();
        Order_Item__c[] itemsForSPCreation = new list<Order_Item__c>();

        for (integer i=0; i<newOrderItem.size(); i++) {
            if (!newOrderItem[i].isRenewal__c) {
                if (newOrderItem[i].Invoice_Amount__c != oldOrderItem[i].Invoice_Amount__c ||
                    (newOrderItem[i].Admin_Tool_Order_Status__c != oldOrderItem[i].Admin_Tool_Order_Status__c && (newOrderItem[i].Admin_Tool_Order_Status__c == 'C' || newOrderItem[i].Admin_Tool_Order_Status__c == 'D' || newOrderItem[i].Admin_Tool_Order_Status__c == 'V'))) {
                        invoiceStatusNew.add(newOrderItem[i]);
                        invoiceStatusOld.add(oldOrderItem[i]);
                    }
            }
            if ((newOrderItem[i].Admin_Tool_Order_Status__c != oldOrderItem[i].Admin_Tool_Order_Status__c || newOrderItem[i].Category__c != oldOrderItem[i].Category__c) && newOrderItem[i].Account__c == oldOrderItem[i].Account__c) {
                accountProductInfoUpdates.add(newOrderItem[i]);
            }
            if (newOrderItem[i].Account__c != oldOrderItem[i].Account__c || newOrderItem[i].Subscription_Model__c != oldOrderItem[i].Subscription_Model__c || newOrderItem[i].Version__c != oldOrderItem[i].Version__c) {
                accountProductInfoUpdatesAccountChange.add(newOrderItem[i]);
            }
            if (newOrderItem[i].Admin_Tool_Order_Status__c == 'A' && newOrderItem[i].Product_Platform__c != null){
                activeItems.add(newOrderItem[i]);
            }
            if (newOrderItem[i].Account__c != oldOrderItem[i].Account__c ||
                (newOrderItem[i].Admin_Tool_Order_Status__c != oldOrderItem[i].Admin_Tool_Order_Status__c &&
                 newOrderItem[i].Admin_Tool_Order_Status__c == 'B')){
                     updateSPIs.add(newOrderItem[i]);
                 }
            if(newOrderItem[i].Sales_Rep__c != oldOrderItem[i].Sales_Rep__c){
                repChanges.add(newOrderItem[i]);
            }
            if (newOrderItem[i].Order_Date__c != oldOrderItem[i].Order_Date__c) {
                orderDateChanged.add(newOrderItem[i]);
            }
            
            //populate list with OIs that change to renewal
            if (oldOrderItem[i].isRenewal__c == false && newOrderItem[i].isRenewal__c == true){
                changeToRenewal.add(newOrderItem[i]);
            }
            if (newOrderItem[i].Opportunity__c != oldOrderItem[i].Opportunity__c && newOrderItem[i].Opportunity__c != null) {
                opportunityChanged.add(newOrderItem[i]);
            }
            //Find any order items that need to trigger manual creation of sales performance records
            if (newOrderItem[i].Trigger_Sales_Performance_Creation__c == true && oldOrderItem[i].Trigger_Sales_Performance_Creation__c == false) { 
                itemsForSPCreation.add(newOrderItem[i]);
            }
            
        }
        if (invoiceStatusNew.size() > 0) {
            // This is to automatically update the SPI before the approval processes are created
            salesPerformanceRequestUtility.createOrderItemUpdateRequest(invoiceStatusOld,invoiceStatusNew);
        }
        if (accountProductInfoUpdates.size() > 0) {
            //update product information on account records
            accountProductInfoUpdater.updateProducts(accountProductInfoUpdates,false);
        }
        if (accountProductInfoUpdatesAccountChange.size() > 0) {
            //update product information on account records
            accountProductInfoUpdater.updateProducts(accountProductInfoUpdatesAccountChange,true);
        }
        if (activeItems.size() > 0) {
            orderItemUtility.subscriptionEndDate(activeItems);
        }
        if (updateSPIs.size() > 0) {
            salesPerformanceItemUtility.updateSPIFromOrderItem(updateSPIs);
        }
        if (repChanges.size() > 0) {
            salesPerformanceItemUtility.updateSPISalesRep(repChanges);
        }
        if (orderDateChanged.size() > 0) {
            orderItemUtility.customerSinceDate(orderDateChanged);
        }
        
        if (changeToRenewal.size() > 0){
            salesPerformanceItemUtility.automatedAdjustmentSPINoApproval(changeToRenewal);
        }
       	if (opportunityChanged.size() > 0){
            salesPerformanceOrderUtility.updateSPOOpportunity(opportunityChanged);
            salesPerformanceItemUtility.updateSPIOpportunity(opportunityChanged);
        }
        
        if(itemsForSPCreation.size() > 0){
            //Insert Sales Performance Order records
            salesPerformanceOrderUtility.insertSPO(itemsForSPCreation);
            
            //Insert Sales Performance Item Records
            salesPerformanceItemUtility.insertSPI(itemsForSPCreation);
        }
        
        orderItemUtility.customerAnnualRevenue(newOrderItem);
        
    }
    
    public static void afterDelete(Order_Item__c[] oldOrderItem, Order_Item__c[] newOrderItem) {}
    
}