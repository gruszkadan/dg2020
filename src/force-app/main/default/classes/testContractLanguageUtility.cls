@isTest(SeeAllData = true)
private class testContractLanguageUtility {
    
    private static testmethod void testSpecialIndexingTerms(){
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Quote__c = newQuote.Id; 
        insert newOpportunity;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        newContract.Opportunity__c = newOpportunity.id;
        insert newContract; 
        
        Product2 newProd = new Product2();
        newProd.Name = 'Indexing Field - Test Field';
        newProd.Proposal_Print_Group__c = 'Services';
        newProd.Contract_Print_Group__c = 'Services';
        newProd.Contract_Product_Name__c = 'Test Field';
        insert newProd;
        
        Product2 newProd2 = new Product2();
        newProd2.Name = 'Indexing Field - Another One';
        newProd2.Proposal_Print_Group__c = 'Services';
        newProd2.Contract_Print_Group__c = 'Services';
        newProd2.Contract_Product_Name__c = 'Another One';
        insert newProd2;
        
        // Name__c, Product_Print_Name__c, Product__r.Contract_Product_Name__c, 
        // Contract_Terms__c, Group_Parent_ID__c, Indexing_Language__c,
        //  Rush__c, Rush_Timeframe__c
        
        Contract_Line_Item__c cli = new Contract_Line_Item__c();
        cli.Contract__c = newContract.Id;
        cli.Product__c = newProd.id;
        cli.Product_Print_Name__c = newProd.Name;
        cli.Contract_Terms__c = null;
        cli.Indexing_Language__c = 'English';
        cli.Rush__c = 'Yes';
        cli.Rush_Timeframe__c = '1 week';
        cli.Name__c = newProd.Name;
        insert cli;
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Contract__c = newContract.Id;
        cli2.Product__c = newProd2.id;
        cli2.Product_Print_Name__c = newProd2.Name;
        cli2.Contract_Terms__c = null;
        cli2.Indexing_Language__c = 'English';
        cli2.Rush__c = 'No';
        cli2.Rush_Timeframe__c = null;
        cli2.Name__c = newProd2.Name;
        insert cli2;
        
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Contract__c = newContract.Id;
        cli3.Product__c = newProd2.id;
        cli3.Product_Print_Name__c = newProd2.Name;
        cli3.Contract_Terms__c = null;
        cli3.Indexing_Language__c = 'Polish';
        cli3.Rush__c = 'No';
        cli3.Rush_Timeframe__c = null;
        cli3.Name__c = newProd2.Name;
        insert cli3;
        
        contractLanguageUtility.specialIndexingTerms(new list <id> {newContract.id});
        
        system.assert([Select Contract_Terms__c from Contract_Line_Item__C where id =:cli3.id limit 1].Contract_Terms__c != null);
        
    }
    
    private static testmethod void testCustomOnlineTraining(){
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Quote__c = newQuote.Id; 
        insert newOpportunity;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        newContract.Opportunity__c = newOpportunity.id;
        insert newContract; 
        
        Product2 newProd = [SELECT id,Name FROM Product2 WHERE name = 'Base Subscription' and Product_Package__c = false Limit 1];
        
        Product2 newProd2 = [SELECT id,Name FROM Product2 WHERE name = 'Custom Online Training' and Product_Package__c = false Limit 1];
        
        Contract_Line_Item__c cli = new Contract_Line_Item__c();
        cli.Contract__c = newContract.Id;
        cli.Product__c = newProd.id;
        cli.Product_Print_Name__c = newProd.Name;
        cli.Name__c = newProd.Name;
        insert cli;
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Contract__c = newContract.Id;
        cli2.Product__c = newProd2.id;
        cli2.Product_Print_Name__c = newProd2.Name;
        cli2.Name__c = newProd2.Name;
        insert cli2;
        
        Contract_Line_Item__c cli3 = new Contract_Line_Item__c();
        cli3.Contract__c = newContract.Id;
        cli3.Product__c = newProd.id;
        cli3.Product_Print_Name__c = newProd.Name;
        cli3.Name__c = newProd.Name;
        insert cli3;
        
        Contract_Line_Item__c cli4 = new Contract_Line_Item__c();
        cli4.Contract__c = newContract.Id;
        cli4.Product__c = newProd.id;
        cli4.Product_Print_Name__c = newProd.Name;
        cli4.Name__c = newProd.Name;
        insert cli4;
        
        customOnlineTrainingContractTerms.customOnlineTrainingLanguage(new list <id> {cli2.id});
        
        
    }
    
    private static testmethod void testLanguageSupportLanguages(){
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.CloseDate = system.today(); 
        newOpportunity.StageName = 'Test';
        newOpportunity.Quote__c = newQuote.Id; 
        insert newOpportunity;
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '1 Year';
        newContract.Opportunity__c = newOpportunity.id;
        newContract.Billing_Currency_Rate__c = 1;
        newContract.Billing_Currency__c = 'USD';
        insert newContract; 
        
        Product2 newProd = [SELECT id,Name FROM Product2 WHERE name = 'Language Support - Per Language' and Product_Package__c = false Limit 1];
        
        
        Contract_Line_Item__c cli = new Contract_Line_Item__c();
        cli.Contract__c = newContract.Id;
        cli.Product__c = newProd.id;
        cli.Product_Print_Name__c = newProd.Name;
        cli.Name__c = newProd.Name;
        cli.Language_Support_Languages__c = 'English;Polish;';
        insert cli;
        
        languageSupportContractTerms.languageSupportTerms(new list <id> {cli.id});
        
        
    }
}