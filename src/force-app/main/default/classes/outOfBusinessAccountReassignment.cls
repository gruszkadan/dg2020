global class outOfBusinessAccountReassignment implements Database.Batchable<SObject>, Database.Stateful{
    global integer numOfAccounts;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (!test.isRunningTest()) {
            return Database.getQueryLocator([Select ID, Customer_Status__c, OwnerID, Authoring_Account_Owner__c, Demo_Setter1__c,Online_Training_Account_Owner__c,
                                             Online_Training_Secondary_Account_Owner__c, Ergonomics_Account_Owner__c, EHS_Owner__c, Platform_Sales_Rep__c, Secondary_Account_Owner__c, Territory__c
                                             from Account where Customer_Status__c='Out of Business' and Owner.Alias !='autosim']);
        } else {
            return Database.getQueryLocator([Select ID, Customer_Status__c, OwnerID, Authoring_Account_Owner__c, Demo_Setter1__c,Online_Training_Account_Owner__c,
                                             Online_Training_Secondary_Account_Owner__c, Ergonomics_Account_Owner__c, EHS_Owner__c, Platform_Sales_Rep__c, Secondary_Account_Owner__c, Territory__c
                                             from Account where Customer_Status__c='Out of Business' and Owner.Alias !='autosim' LIMIT 100]);
        }
    }
    
    global void execute(Database.BatchableContext bc, list<SObject> batch) {
        Account[] accountsToUpdate = new List<Account>();
        User autosim = [select ID from User where Alias='autosim'];
        for (Account acct : (list<Account>) batch) {
            if (acct.Customer_Status__c == 'Out of Business') {
                acct.OwnerID = autosim.Id;
                acct.Authoring_Account_Owner__c = autosim.Id;
                acct.Online_Training_Account_Owner__c = autosim.Id;
                acct.Online_Training_Secondary_Account_Owner__c = NULL;
                acct.Ergonomics_Account_Owner__c = autosim.Id;
                acct.EHS_Owner__c = autosim.Id;
                acct.Platform_Sales_Rep__c = autosim.Id;
                acct.Territory__c = NULL;
                //clear enterprise fields
                acct.Enterprise_Sales__c = false;
                acct.Enterprise_sales_Industry__c = null;
                acct.Campaign_Status__c = null;
                accountsToUpdate.add(acct);
            }
        }
        numOfAccounts = accountsToUpdate.size();
        update accountsToUpdate;
    }
    
    global void finish(Database.BatchableContext bc) {
        //Compose Async Apex Job info
        AsyncApexJob oAsyncApexJob = [SELECT Id, ApexClassId,
                                      JobItemsProcessed,
                                      TotalJobItems,
                                      NumberOfErrors,
                                      ExtendedStatus,
                                      CreatedBy.Email
                                      FROM AsyncApexJob
                                      WHERE id =: bc.getJobId()];
        //Compose Single EmailMessage service
        string htmlBody ='<b>Out of Business Account Reassignment Results</b></br></br><b># of Accounts Updated:</b> '+numOfAccounts+'</br></br><b># of Errors:</b> '+ oAsyncApexJob.NumberOfErrors;
        if(oAsyncApexJob.ExtendedStatus != NULL){
            htmlBody = htmlBody + '</br></br><b>Error Message:</b> '+ oAsyncApexJob.ExtendedStatus;
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] sToAddress = new String[]{'salesforceops@ehs.com'};
            mail.setToAddresses(sToAddress);
        mail.setReplyTo('mmccauley@ehs.com');                      
        mail.setSenderDisplayName('Mark McCauley');
        mail.setSubject('Batch Job Summary: Out of Business Account Reassignment');
        mail.setHtmlBody(htmlBody);
        Messaging.sendEmail( new Messaging.SingleEmailMessage[]{mail});
        
        User u = [SELECT id, Validation_Rules_Active__c FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
        u.Validation_Rules_Active__c = true;
        update u;
    }
}