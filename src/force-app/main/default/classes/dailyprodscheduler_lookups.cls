public with sharing class dailyprodscheduler_lookups implements Schedulable {

    public void execute(SchedulableContext ctx) {
        
        dlrs__LookupRollupSummary__c[] c = [SELECT Id FROM dlrs__LookupRollupSummary__c WHERE Name LIKE '%PR:%'];

        for( dlrs__LookupRollupSummary__c rollup : c )
        {
            dlrs.RollupService.runJobToCalculate(rollup.Id);
        }
        
    }
}