@isTest
private class testpromoManagementController {
    
    //create all the data
    public static testmethod void test() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            
            On_off__c onoff = new On_Off__c(Name = 'ultimateParentTrigger', Activated__c = false);
            insert onoff;
            
            //query a profile
            Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'SF Operations']; 
            
            //director user
            User directorUser = new User(Alias = 'du', Email='standarduser@testorg.com', 
                                         EmailEncodingKey='UTF-8', LastName='Testing', Group__c='Mid-Market', LanguageLocaleKey='en_US', 
                                         LocaleSidKey='en_US', ProfileId = p.Id, 
                                         TimeZoneSidKey='America/Los_Angeles', UserName='directoruser@testorg.com');
            insert directorUser;
            
            //manager user
            User managerUser = new User(Alias = 'mu', Email='standarduser@testorg.com', 
                                        EmailEncodingKey='UTF-8', LastName='Testing', Group__c='Mid-Market', LanguageLocaleKey='en_US', 
                                        LocaleSidKey='en_US', ProfileId = p.Id, ManagerId = directorUser.id, Sales_Director__c = directorUser.id,
                                        TimeZoneSidKey='America/Los_Angeles', UserName='manageruser@testorg.com');
            insert managerUser;
            
            //running user
            User runningUser = new User(Alias = 'ru', Email='standarduser@testorg.com', 
                                        EmailEncodingKey='UTF-8', LastName='Testing', Group__c='Mid-Market', LanguageLocaleKey='en_US', 
                                        LocaleSidKey='en_US', ProfileId = p.Id, ManagerId = managerUser.id, Sales_Director__c = directorUser.id,
                                        TimeZoneSidKey='America/Los_Angeles', UserName='runningUser@testorg.com');
            insert runningUser;
            
            System.runAs(runningUser) {
                //insert account
                Account newAcct = new Account(Name = 'Test 1');
                insert newAcct;
                
                //insert contact
                Contact newCon = new Contact(FirstName = 'Test', LastName = 'Test', AccountId = newAcct.id);
                insert newCon;
                
                //insert opportunity
                Opportunity newOpp = new Opportunity(Name = 'Test', AccountId = newAcct.id, StageName = 'New', CloseDate = date.today().addDays(90));
                insert newOpp;
                
                //insert promo
                Quote_Promotion__c newpromo = new Quote_Promotion__c(Name = 'TestClassPromo', Start_Date__c = date.today().addDays(-2), End_Date__c = date.today().addDays(10), Code__c = 'TestClassPromo');
                insert newpromo;
                
                //insert quote
                Quote__c newQuote = new Quote__c(Account__c = newAcct.id, Opportunity__c = newOpp.id, Contact__c = newCon.id, Contract_Length__c = '1 Year', Promotion__c = newPromo.id, Pricing_Terms__c = '60 Days from Proposal Date',
                                                Approve_Expired_Promo_Pricing_Terms__c = true);
                insert newQuote;
                
                //submit quote for approval
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setObjectId(newQuote.id);
                req1.setNextApproverIds(new Id[] {runningUser.Sales_Director__c});
                Approval.ProcessResult result1 = Approval.process(req1);

                
                //insert quote product
                Quote_Product__c newQp = new Quote_Product__c();
                newQp.Quantity__c = 1;
                newQp.Y1_Quote_Price__c = 78;
                newQp.Y1_List_Price__c = 100;
                newQp.Approval_Required_by__c = 'Sales Manager';
                newQp.Approval_Status__c = 'Not Submitted';
                newQp.Quote__c = newQuote.Id;
                newQp.Year_1__c = true;
                insert newQp;
                
                //submit qp for approval
                Approval.ProcessSubmitRequest req2 = new Approval.ProcessSubmitRequest();
                req2.setObjectId(newQp.id);
                Approval.ProcessResult result2 = Approval.process(req2);
                
                //insert an address for the contract
                Address__c newAdd = new Address__c();
                newAdd.Account__c = newAcct.Id;
                newAdd.City__c = 'test';
                newAdd.Country__c = 'USA';
                newAdd.IsPrimary__c = true;
                newAdd.State__c = 'IL';
                newAdd.Street__c = '222';
                newAdd.Zip_Postal_Code__c = '612365';
                insert newAdd;
                
                //insert contract
                Contract__c newContract = new Contract__c();
                newContract.Account__c = newAcct.Id;
                newContract.Contact__c = newCon.Id;
                newContract.Address__c = newAdd.Id;
                newContract.Opportunity__c = newOpp.id;
                newContract.Contract_Type__c ='New';
                newContract.Contract_Length__c = '1 Year';
                newContract.Approve_Orders__c = true;
                insert newContract;  
                
                Approval.ProcessSubmitRequest req3 = new Approval.ProcessSubmitRequest();
                req3.setObjectId(newContract.id);
                req3.setNextApproverIds(new Id[] {runningUser.ManagerId});
                Approval.ProcessResult result3 = Approval.process(req3);
                
                testUser(newPromo);
                testManager(managerUser);
                testDirector(directorUser);
            }
            
        }
        
        
    }
    
    static void testUser(Quote_Promotion__c newPromo) {
        promoManagementController con = new promoManagementController();
        
        //cancel the promo
        ApexPages.currentPage().getParameters().put('cancelId', newpromo.id);
        con.cancelPromo();
        newPromo = [SELECT id, Status__c FROM Quote_Promotion__c where id = : newPromo.id LIMIT 1];
        System.assert(newPromo.Status__c == 'Cancelled', 'should be cancelled, actually is '+newPromo.Status__c);
        
        //clone the promo
        ApexPages.currentPage().getParameters().put('cloneId', newpromo.id);
        con.clonePromo();
        
        //edit the promo
        ApexPages.currentPage().getParameters().put('editId', newpromo.id);
        con.editPromo();
        
        //create a new promo
        con.createNewPromo();
        
        //promo search
        con.searchName = 'TestClassPromo';
        con.searchPromo();
        con.sortToggle();
        System.assert(con.getCurrentPromoList().size() == 1, 'expected 1, returned : '+con.getCurrentPromoList().size());
    }
    
    static void testManager(User managerUser) {
        System.runAs(managerUser) {
            promoManagementController con = new promoManagementController();
            System.assert(con.quoteProds.size() == 1,'expected 1, returned '+con.quoteProds.size());
            System.assert(con.contracts.size() == 1,'expected 1, returned '+con.contracts.size());
        }
    }  
    
    static void testDirector(User directorUser) {
        System.runAs(directorUser) {
            promoManagementController con = new promoManagementController();
            System.assert(con.quotes.size() == 1,'expected 1, returned '+con.quotes.size());
        }
    }  
    
    
}