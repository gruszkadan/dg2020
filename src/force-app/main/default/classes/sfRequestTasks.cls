public class sfRequestTasks {
 public List<sfTasksWrapper> wrappers {get; set;}
 public static Integer toDelIdent {get; set;}
 public static Integer addCount {get; set;}
 private Integer nextIdent=1;
 public Salesforce_Request__c theRequest {get;set;}
 public Salesforce_Request_Task__c[] theTasks {get;set;}
  
 public sfRequestTasks()
 {
 	Id sfRequestId = ApexPages.currentPage().getParameters().get('id');	
 	theRequest = [SELECT Id, Name, Description__c, Summary__c from Salesforce_Request__c where ID=:sfRequestId];
 	theTasks = [SELECT Description__c, Step_Number__c, Work_Request__c, Owner__c, Start_Date__c, End_Date__c, Name, Id
 					from Salesforce_Request_Task__c where Work_Request__c =:theRequest.ID];
  wrappers=new List<sfTasksWrapper>();
  for (Integer idx=0; idx<1; idx++)
  {
   wrappers.add(new sfTasksWrapper(nextIdent++));
  }
 }
  
 public void delWrapper()
 {
  Integer toDelPos=-1;
  for (Integer idx=0; idx<wrappers.size(); idx++)
  {
   if (wrappers[idx].ident==toDelIdent)
   {
    toDelPos=idx;
   }
  }
   
  if (-1!=toDelPos)
  {
   wrappers.remove(toDelPos);
  }
 }
  
 public void addRows()
 {
  for (Integer idx=0; idx<addCount; idx++)
  {
   wrappers.add(new sfTasksWrapper(nextIdent++));
  }
 }
  
 public PageReference save()
 {
  List<Salesforce_Request_Task__c> sftcs=new List<Salesforce_Request_Task__c>();
  for (sfTasksWrapper wrap : wrappers)
  {
   sftcs.add(wrap.sftc);
  }
   
  insert sftcs;
   
  return new PageReference('/' + theRequest.ID);
 }
  
 public class sfTasksWrapper
 {
  public Salesforce_Request_Task__c sftc {get; private set;}
  public Integer ident {get; private set;}
   
  public sfTasksWrapper(Integer inIdent)
  {
   Id sfRequestId = ApexPages.currentPage().getParameters().get('id');	
   ident=inIdent;
   sftc=new Salesforce_Request_Task__c(Work_Request__c = sfRequestId, Step_Number__c = inIdent);
  }
 }
 
     public PageReference saveEdits(){
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(theTasks.size()>0)
                upsert(theTasks);
                 }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
    return new PageReference('/' + theRequest.ID);
    }
    
     static testmethod void canCreateSFRequest() {
        
        Salesforce_Request__c newRequest = new Salesforce_Request__c();
        newRequest.Summary__c = 'Test Account';
        insert newRequest;
        
        Salesforce_Request_Task__c newTask = new Salesforce_Request_Task__c();
        newTask.Work_Request__c = newRequest.Id;
        newTask.Description__c = 'Test Account';
        insert newTask;
                                
        ApexPages.currentPage().getParameters().put('Id', newRequest.Id);
        sfRequestTasks myController = new sfRequestTasks();
        myController.saveEdits(); 
        myController.save();
        myController.delWrapper();
        myController.addRows();
     }    
}