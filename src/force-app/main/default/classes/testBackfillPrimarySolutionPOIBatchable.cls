@isTest()
private class testBackfillPrimarySolutionPOIBatchable {
    
    public static testmethod void test1() {
        
        
        Account act = new Account(Name = 'Test Account', NAICS_Code__c = '23 - Construction');
        insert act;
        
        Contact con1 = new Contact(FirstName = 'Test', LastName ='Person1', AccountId = act.Id, Primary_Solution_Product_of_Interest__c = 'MSDS Management');
        insert con1;
        
   
                
        Campaign c = new Campaign(Name='Test Campaign');
        insert c;
        
  
        
        CampaignMember cm2 = new CampaignMember(ContactId = con1.id, Status = 'Sent', CampaignId = c.id);
        insert cm2;
        
        
        backfillPrimarySolutionPOIBatchable upb = new backfillPrimarySolutionPOIBatchable();
        id batchprocessId = Database.executeBatch(upb);
    }
    
}