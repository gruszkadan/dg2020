@isTest
private class testContactController {

    public static testmethod void RegularTests() {        
        Test.startTest();
        test1();   
        test2();
        test3();
        test4();
        Test.stopTest();
    }

    static testmethod void test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        ApexPages.currentPage().getParameters().put('Id', newContact.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contact());
        contactController myController = new contactController(testController);
        
        myController.saveATUpdate();
        myController.requestUpdate();
        myController.newContactSavenew();
        myController.changeOfContact();
        myController.redirectToNewPage();
        
    }
    
    static testmethod void test2() {
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contact());
        contactController myController = new contactController(testController);
        
        myController.newContactSave();        
    }
    
    static testmethod void test3() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        ApexPages.currentPage().getParameters().put('accid', newAccount.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contact());
        contactController myController = new contactController(testController);
        
        myController.newContactSave();        
    }
    
    static testmethod void test4() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        //current Primary Admin and Billing Contact for the account
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
    	newContact.CustomerAdministratorId__c = '12345';
        newContact.Billing_Contact__c = true;
        newContact.Update_Type__c = 'Primary Admin & Billing Contact Update';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Contact newContact1 = new Contact();
        newContact1.FirstName = 'Jones';
        newContact1.LastName = 'Bob';
        newContact1.AccountID = newAccount.Id;
        insert newContact1;
        
         Case newCase = new Case();
        newCase.AccountID = newAccount.Id;
        newCase.ContactID = newContact.Id;
        newCase.Status = 'Active';
        newCase.Type = 'Compliance Services';
        newCase.Change_Of_Contact_Source__c = 'Email';
        newCase.Subject = 'Test';
        newCase.OwnerID = '00580000003UChI';  
        insert newCase;
        
        ApexPages.currentPage().getParameters().put('contactID', newContact.Id);
        changeOfContact myController = new changeOfContact();
        myController.oldContactStatus = 'Active';
        myController.oldContactCases = [Select ID from Case where ID=: newCase.id];
        myController.newContactID = newContact1.id;
        myController.processCoC();
        
       System.debug(newContact.CustomerAdministratorId__c);
       System.debug(newContact.Billing_Contact__c);
       System.debug(newContact1.CustomerAdministratorId__c);
       System.debug(newContact1.Billing_Contact__c);
        
        myController.cancel();
        myController.getStatus();
        myController.getContactSelectList();
       
       
    }
    
     static testmethod void test5() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        ApexPages.currentPage().getParameters().put('accid', newAccount.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contact());
        contactController myController = new contactController(testController);
        
        myController.newContactSave();        
    }
    
    //Testing Upon insert of new contact that the ES Email Domain field is marked as true when containing an email on the list
    static testmethod void test6() {
        
       Account newAccount = new Account();
        newAccount.Name = 'Test Account';
       newAccount.Customer_Status__c ='Active';
       insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Email = 'bob@test.com';
        insert newContact;
        
        ApexPages.currentPage().getParameters().put('Id', newContact.Id);
        ApexPages.currentPage().getParameters().put('accid', newAccount.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contact());
        contactController myController = new contactController(testController);

        contact updatedContact = [Select ES_Email_Domain__c from Contact where ID  = :newContact.Id];
        System.assert(updatedContact.ES_Email_Domain__c = TRUE);
        
    }
    
    //Testing upon update that the Email Domain field is marked as false when email is not on the ES Email Domain list
    static testmethod void test7() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        //newContact.Email = 'bob@test.com';
        insert newContact;
        
        ApexPages.currentPage().getParameters().put('Id', newContact.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contact());
        contactController myController = new contactController(testController);

        myController.theContact.Email = 'bob@test.com';
        update myController.theContact;
        myController.checkEmailThenSave();
        System.debug(myController.theContact.Email);
        myController.createNewLead();
        
        contact updatedContact = [Select Email, ES_Email_Domain__c from Contact where Id = :myController.theContact.Id];
        System.debug('Test 7 New Email ' + updatedContact.Email);
        System.assert(updatedContact.ES_Email_Domain__c == TRUE);
        
    }
    
}