trigger userTrigger on User
(before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    if (trigger.isBefore) {
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            userTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
    }
    
    if (trigger.isAfter) {
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            userTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
        // AFTER INSERT
        if (trigger.isInsert) {
             userTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
    }
    
    
}