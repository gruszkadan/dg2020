public class poiController {
    public Product_of_Interest__c poi {get;set;}
    public Product_of_Interest_Action__c[] poiActions {get;set;}
    public Attachment[] attachments {get;set;}
    public Product_of_Interest__History[] poiHistory {get;set;}
    public id poiId;
    public User u {get;set;}
    
    public poiController(ApexPages.StandardController controller) {
        poiId = ApexPages.currentPage().getParameters().get('id');
        queryPOI();
        poiActions = new list<Product_of_Interest_Action__c>();
        poiActions = [Select id,Name,Product__c,Action__c,SystemModStamp from Product_of_Interest_Action__c where Product_of_Interest__c =:poiId ORDER BY CreatedDate DESC];
        attachments = new list<Attachment>();
        attachments = [Select id,Name,ParentId,createdbyid,lastmodifieddate from Attachment WHERE ParentId =:poiId ORDER BY CreatedDate DESC];
        poiHistory = new list<Product_of_Interest__History>();
        poiHistory = [SELECT CreatedBy.Name, CreatedDate, Field, NewValue, OldValue, ParentId FROM Product_of_Interest__History WHERE ParentId =:poiId ORDER BY CreatedDate DESC];
        u = [Select id, Profile.Name, Name from User where Id =:UserInfo.getUserId()];
    }
    public void queryPOI(){
        String SobjectApiName = 'Product_of_Interest__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        String addFields = ', CreatedBy.Name, LastModifiedBy.Name';
        String poiQuery = 'select ' + commaSepratedFields + addFields + ' from ' + SobjectApiName + ' where id = :poiId LIMIT 1';
        system.debug('poiId is '+poiId);
        this.poi = Database.query(poiQuery);
    }
    
    public PageReference removeAttachment(){
        id attchid = ApexPages.currentPage().getParameters().get('aid');
        for(attachment a:attachments){
            if(a.id == attchid){
                try {
                    delete a;
                } catch(exception e) {
                    salesforceLog.createLog('Product of Interest', true, 'poiController', 'removeAttachment', +string.valueOf(e));
                }
            }
        }
        PageReference poi_detail = Page.poi_detail;
        poi_detail.setRedirect(true);
        poi_detail.getParameters().put('id',poiId);
        return poi_detail;
    }
    
}