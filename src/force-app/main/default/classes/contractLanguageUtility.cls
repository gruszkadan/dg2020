public class contractLanguageUtility {
    @InvocableMethod (label='Contract Special Indexing Language')
    public static void specialIndexingTerms(List<id> contractIds) {
        //Query Contracts and Sub-Query Contract Line Items related to each
        Contract__c[] contracts = [SELECT id,IsFreeDatabaseBuild__c,Num_of_Special_Indexing_Products__c,Special_Indexing_Fields__c,
                                   (SELECT id, Name__c, Product_Print_Name__c, Product__r.Contract_Product_Name__c, Contract_Terms__c, Group_Parent_ID__c, Indexing_Language__c, Rush__c, Rush_Timeframe__c FROM Contract_Items__r ORDER BY Indexing_Language__c ASC)
                                   FROM Contract__c
                                   WHERE id in:contractIds];
        
        //Query for the special indexing terms
        String specialIndexingTerms = [SELECT id, Description__c
                                       FROM Description_Dictionary__c
                                       WHERE Flow_Referance_Name__c = 'special_indexing'
                                       LIMIT 1].Description__c;
        
        //List to hold the contract line items that will need to be updated with the new language
        Contract_Line_Item__c[] lineItemsToUpdate = new list <Contract_Line_Item__c>(); 
        
        if(Contracts.size() > 0){
            //Loop over the contracts
            for(Contract__c c:contracts){
                boolean hasEBinderOrScan = false;
                for(Contract_Line_Item__c cli:c.Contract_Items__r){
                    if(cli.Name__c == 'eBinder Valet' || cli.Name__c == 'Scan/PDF Process'){
                        hasEBinderOrScan = true;
                    }
                }
                
                //If the contract is a free database build or if it is not and has no ebinder or scan project
                if(c.IsFreeDatabaseBuild__c == true || (c.IsFreeDatabaseBuild__c == false && hasEBinderOrScan == false)){
                    
                    //Loop over its special indexing products
                    string previousIndexingLanguage = '';
                    for(Contract_Line_Item__c cli:c.Contract_Items__r){
                        if(cli.Name__c.startsWith('Indexing Field -')){
                            cli.Contract_Terms__c = null;
                            //Check if we already created terms for the language grouping 
                            if(cli.Indexing_Language__c != previousIndexingLanguage){
                                
                                //Loop over the items to find all indexing fields for the language
                                set<String> indexingFields = new set<String>();
                                for(Contract_Line_Item__c field:c.Contract_Items__r){
                                    if(field.Name__c.startsWith('Indexing Field -') || field.Name__c.startsWith('Indexing Package -')){
                                        if(field.Indexing_Language__c == cli.Indexing_Language__c){
                                            indexingFields.add(field.Product__r.Contract_Product_Name__c);
                                        }
                                    }
                                }
                                //Create a string for indexing fields to insert in language
                                string indexingFieldsString;
                                for(String s:indexingFields){
                                    if(indexingFieldsString == null){
                                        indexingFieldsString = s;
                                    }else{
                                        indexingFieldsString += ', '+s;
                                    }
                                }
                                
                                //Modify the contract item terms
                                
                                //string to hold the main terms that will be appended to the line item
                                string itemTerms = specialIndexingTerms;
                                //Add in the additional indexed fields
                                itemTerms = itemTerms.replace('&lt;Additional_Indexed_Fields&gt;', indexingFieldsString);
                                
                                //Add indexing language in the terms
                                itemTerms = itemTerms.replace('&lt;$language&gt;', cli.Indexing_Language__c);
                                
                                //Add indexing language in the heading
                                string headingLanguage = '';
                                if(cli.Indexing_Language__c != null && cli.Indexing_Language__c != '' && cli.Indexing_Language__c != 'English'){
                                    headingLanguage = ' - '+cli.Indexing_Language__c;
                                }
                                itemTerms = itemTerms.replace('&lt;$productLanguage&gt;', headingLanguage);
                                
                                //Add Rush Language
                                string rushLanguage = '';
                                for(Contract_Line_Item__c field:c.Contract_Items__r){
                                    if(field.Name__c.startsWith('Indexing Field -') || field.Name__c.startsWith('Indexing Package -')){
                                        if(field.Indexing_Language__c == cli.Indexing_Language__c){
                                            if(field.Rush__c == 'Yes'){
                                                rushLanguage += '<li>A rush fee has been associated to indexing the following field: '+field.Product__r.Contract_Product_Name__c +' as a part of this project. VelocityEHS will deliver this project ' + field.Rush_Timeframe__c +' from execution of this Customer Order Form.</li>';
                                            }
                                        }
                                    }
                                }
                                itemTerms = itemTerms.replace('<li>&lt;$rushLanguage&gt;</li>', rushLanguage);
                                
                                //Set the new contract terms on the item;
                                cli.Contract_Terms__c = itemTerms;
                                
                                //Add to update list
                                lineItemsToUpdate.add(cli);
                                
                                //Set the currentIndexingLanguage as the language of the product just updated
                                previousIndexingLanguage = cli.Indexing_Language__c;
                            }
                        }
                    }  
                }
            }
            
            //Update items with new language
            if(lineItemsToUpdate.size() > 0){
                update lineItemsToUpdate;
            }
        }
        
        
    }
}