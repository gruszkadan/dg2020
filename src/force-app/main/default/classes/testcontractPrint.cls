@isTest(seeAllData = true)
private class testcontractPrint {
    
    static testmethod void test1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;      
        
        Address__c address = new Address__c();
        address.Account__c = newAccount.Id;
        address.City__c = 'test';
        address.Country__c = 'USA';
        address.IsPrimary__c = true;
        address.State__c = 'IL';
        address.Street__c = '222';
        address.Zip_Postal_Code__c = '612365';
        insert address;
        
        
        Contract__c newContract = new Contract__c();
        newContract.Account__c = newAccount.Id;
        newContract.Contact__c = newContact.Id;
        newContract.Address__c = address.Id;
        newContract.Contract_Type__c ='New';
        newContract.Contract_Length__c = '5 Years';
        insert newContract;  
        
        Product2 newProd =  [ SELECT id, Name, Contract_Print_Group__c FROM Product2 WHERE Name = 'Custom Print Job' LIMIT 1 ];
        
        Contract_Line_Item__c cli = new Contract_Line_Item__c();
        cli.Contract__c = newContract.Id;
        cli.Product__c = newProd.id;
        cli.Contract_Terms_Print_Page__c = 1;
        cli.Sort_order__c = 2;
        insert cli;
        
        
        Contract_Line_Item__c cli2 = new Contract_Line_Item__c();
        cli2.Contract__c = newContract.Id;
        cli2.Product__c = newProd.id;
        cli2.Contract_Terms_Print_Page__c = 12;
        cli2.Sort_order__c = 2;
        insert cli2;   
        
        ApexPages.currentPage().getParameters().put('id', newContract.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Contract__c());
        contractPrint myController = new contractPrint(testController);
        myController.theContract = newContract;
        myController.contractTerms = new List<Contract_Line_Item__c>();
        myController.contractTerms.add(cli);
        myController.contractTerms.add(cli2);
        myController.theAddress = address;
        myController.test = 'test';
        myController.terms = 'terms';
        myController.countTerms = 5;
    }
    
    
    
}