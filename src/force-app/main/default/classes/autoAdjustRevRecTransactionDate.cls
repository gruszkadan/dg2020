global class autoAdjustRevRecTransactionDate Implements Schedulable {
    
    //schedule class by: 
    //autoAdjustRevRecTransactionDate m = new autoAdjustRevRecTransactionDate();
    //String sch = '0 0 1 * * ?';
	//String jobID = system.schedule('autoAdjustRevRecTransactionDate Schedule Class', sch, m);

    
    global void execute(SchedulableContext sc){
        date todaysDate = System.today();
        Revenue_Recognition_Transaction__c[] revRecTransactions = [SELECT Expected_Date__c, Force_Subsequent_Expected_Date_Change__c
                                                                   FROM Revenue_Recognition_Transaction__c 
                                                                   WHERE Expected_Date__c < :todaysDate AND isForecast__c = true];
        
        //Query for holidays
        Holiday[] holidays = [SELECT ActivityDate FROM Holiday]; 
        
        for (Revenue_Recognition_Transaction__c RRT : revRecTransactions){
            RRT.Expected_Date__c = dateUtility.calculateBusinessDay(RRT.Expected_Date__c,1,holidays);
            RRT.Force_Subsequent_Expected_Date_Change__c = true;
        }
        try{
            update revRecTransactions;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            string[] Emails = New List<string>();
            if(Test.isRunningTest()){
                Emails.add('taraswills@gmail.com'); 
            }else{
                Emails.add('salesforceops@ehs.com');
            }
            mail.setToAddresses(Emails);
            mail.setSubject('Revenue Recognition Transaction Date Adjustment Notification');
            string emailBody ='<p style="font-size: 75%;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;"><b>AUTO DATE ADJUSTMENT NOTIFICATION</b></br></br></p>'+
                '<p style="font-size: 75%;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;">The expected date of ' + revRecTransactions.size() + ' revenue recognition transactions were updated this morning.</br></br></p>';
            mail.sethtmlbody(emailBody); 
            Messaging.sendEmail(New Messaging.SingleEmailMessage[]{mail});
            
        }catch(exception e){
            salesforceLog.createLog('Revenue Recognition', true, 'autoAdjustRevRecTransactionDate', 'schedule Class', string.valueOf(e));
        }
    }
}