public class contentVersionTriggerHandler {
    // AFTER INSERT
    public static void afterInsert(ContentVersion[] oldContentVersions, ContentVersion[] newContentVersions) {
        contentUtility.sendContentNotification(newContentVersions);
    }
}