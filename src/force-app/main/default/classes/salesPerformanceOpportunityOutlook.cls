/*
* Test class: testSalesPerformanceController
*/
public with sharing class salesPerformanceOpportunityOutlook {
    public string userID { get; set; }
    public User u { get; set; }
    public String defaultView { get; set; }
    public Opportunity[] opptys { get; set; }
    public string errorMessage { get; set; }
    public boolean isSuccess { get; set; }
    public string alertIcon { get; set; }
    public date monthLastDay {get;set;}
    //v1.1 Additions
    public String viewAs { get; set; }
    public String viewAsDefaultView { get; set; }
    //v1.1 temporary opp filter additions
    public opportunityFilter[] opportunityFilters {get;set;}
    public opportunityFilter[] selectedFilters {get;set;}
    public string ownerGroupsSelectedString {get;set;}
    public string closingMonth {get;set;}
    public string currentMonth {get;set;}
    public string nextMonth {get;set;}
    public string nextNextMonth {get;set;}
    //KPI Additions
    public Sales_Performance_Summary__c sps {get;set;}
    String spsMonth {get;set;}
    public decimal opportunityTotalAmount {get;set;} 
    
    public salesPerformanceOpportunityOutlook() {
        opportunityFilters = new list<opportunityFilter>();
        selectedFilters = new list<OpportunityFilter>();
        opportunityFilters.add(new opportunityFilter('Authoring Sales'));
        opportunityFilters.add(new opportunityFilter('EHS Enterprise Sales'));
        opportunityFilters.add(new opportunityFilter('EHS Mid-Market Sales'));    
        opportunityFilters.add(new opportunityFilter('Enterprise Sales'));
        opportunityFilters.add(new opportunityFilter('Ergo'));
        opportunityFilters.add(new opportunityFilter('Mid-Market'));
        opportunityFilters.add(new opportunityFilter('Not Specified'));
        opportunityFilters.add(new opportunityFilter('Online Training'));
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias, FirstName, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID FROM User WHERE id = :viewAs LIMIT 1];
        if (defaultView == 'VP' && viewAs == userInfo.getUserId()) {
            u = [SELECT id, Alias, FirstName, LastName, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID FROM User WHERE UserRole.Name = 'VP Sales' AND isActive = true and LastName != 'Haling' LIMIT 1];
        }
        userID = u.id;
        //get defaultView to display when viewing as
        viewAsDefaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
        monthLastDay = Date.Today().toStartofMonth().addMonths(1) - 1;
        
        currentMonth= dateTime.now().format('MMMMM');
        nextMonth = dateTime.now().addMonths(1).format('MMMMM');
        nextNextMonth = dateTime.now().addMonths(2).format('MMMMM');
        
        closingMonth = 'THIS_MONTH';
        queryOpptys();
        findCurrentUserSPS();
    }
    
    public void newMonth(){
        queryOpptys();
        findCurrentUserSPS();
    }
    
    public void queryOpptys(){
        string userField = 'OwnerID';
        string mgrField = '';
        date nextMonth = date.today().addMonths(2).toStartOfMonth().addDays(-1);
        if (viewAsDefaultView == 'Manager') {
            mgrField = 'Owner.ManagerId';
        } else if (viewAsDefaultView == 'Director') {
            mgrField = 'Owner.Sales_Director__c';
        } else if (viewAsDefaultView == 'VP') {
            mgrField = 'Owner.Sales_VP__c';
        }
        
        string query = 'Select ID, StageName, AccountId, CloseDate, Next_Task_Scheduled_On__c, Owner.Name, OwnerId, Owner.FullPhotoURL, Name, Amount, Closed_Reasons__c, Main_Competitor__c, Other_Main_Competitor__c, Reason_Lost__c from Opportunity ';
        query += 'WHERE isClosed = FALSE and CloseDate = '+closingMonth+' and Opportunity_Type__c != null and (OwnerId = \''+userId+'\' or OwnerId = \'' + userId.left(15) + '\' ';
        if(viewAsDefaultView == 'Manager' || viewAsDefaultView == 'Director' || viewAsDefaultView == 'VP'){
            query += 'or ' + mgrField + '=\'' + userID + '\' or ' + mgrField + '=\'' + userID.left(15) + '\' '; 
        }
        query += ') ';
        if(closingMonth =='NEXT_N_MONTHS:2'){
            query+='and CloseDate > NEXT_MONTH ';
        }
        if(selectedFilters.size() > 0){
            query += 'and ';
            string ownerGroups;
            for(opportunityFilter f:selectedFilters){
                if(ownerGroups == null){
                    ownergroups = '(Owner_Group__c = \''+f.groupName+'\'';
                    ownerGroupsSelectedString = f.GroupName;
                }else{
                    ownerGroups += ' or Owner_Group__c = \''+f.groupName+'\'';
                    ownerGroupsSelectedString += ', '+f.GroupName;
                }
            }
            ownerGroups += ') ';
            query += ownerGroups;
        }
        query += 'Order By CloseDate ASC';
        opptys = database.query(query);
    }
    
    public void saveOpportunties() {
        if (opptys.size() > 0) {
            isSuccess = TRUE;
            alertIcon = 'success';
            errorMessage = '';
            try {
                update opptys;
            } catch(System.DMLexception e) {
                errorMessage = string.valueOf(e);
                isSuccess = false;
                alertIcon = 'error';
            }
        }
        queryOpptys();
    }
    
    public List<SelectOption> getStageName() {
        List<SelectOption> options = new List<SelectOption> ();
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f : ple) {
            if (f.getLabel() != 'New' && f.getLabel() != 'Discussion' && f.getLabel() != 'Legal Review' && f.getLabel() != 'Waiting for Final Approval'
                && f.getLabel() != 'Commitment' && f.getLabel() != 'Closed/Won' && f.getLabel() != 'Closed Lost' && f.getLabel() != 'Closed' && f.getLabel() != 'Closed Other') {
                    options.add(new SelectOption(f.getLabel(), f.getValue()));
                }
        }
        return options;
    }
   
    public class opportunityFilter{
        public String groupName {get;set;}
        public boolean isSelected {get;set;}
        
        opportunityFilter(string name){
            groupName = name;
        }
    }
    
    public void applyOpportunityFilter(){
        ownerGroupsSelectedString = null;
        selectedFilters = new list<OpportunityFilter>();
        for(opportunityFilter f:opportunityFilters){
            if(f.isSelected == true){
                selectedFilters.add(f);
            }
        }
        queryOpptys();
    }
    
    public void removeAllOpportunityFilters(){
        ownerGroupsSelectedString = null;
        for(opportunityFilter f:opportunityFilters){
            f.isSelected = false;
        }
        selectedFilters = new list<OpportunityFilter>();
        queryOpptys();
    }
    
    public void findCurrentUserSPS(){
        spsMonth = ApexPages.currentPage().getParameters().get('spsMonth');
        if(spsMonth == null){
            spsMonth = currentMonth;
        }
        Sales_Performance_Summary__c[] spsList = [SELECT id,Quota__c,Total_Bookings__c,Quota_Percent__c
                                               FROM Sales_Performance_Summary__C
                                               WHERE Sales_Rep__c =:u.id
                                               AND Month__c =:spsMonth
                                               AND Year__c =:string.valueof(datetime.now().year())];
        sps = new Sales_Performance_Summary__c();
        if(spsList != null && spsList.size() > 0){
            sps = spsList[0];
        }
    }
}