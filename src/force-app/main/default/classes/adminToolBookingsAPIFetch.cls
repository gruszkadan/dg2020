public class adminToolBookingsAPIFetch implements Schedulable{
/**
* This scheduled class starts the process to get updated Bookings data from the Admin Tool
* This calls the adminToolBookingsDataQueueable
**/
	 public void execute(SchedulableContext SC) {
        System.enqueueJob(new adminToolBookingsDataQueueable());
    }
}