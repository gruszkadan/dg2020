public class activityUtility {
    public static void taskCreated(Task[] tasks) {
        //Set to Hold UserIDs we want to Query
        set<id> userIDs = new set<id>();
        //Go through all the tasks and add the UserIDs to the Set
        for(Task t : tasks){
            userIDs.add(t.OwnerID);
            userIDs.add(System.UserInfo.getUserId());
        }
        //Query the Users
        User[] users = [Select ID, Role__c, Start_Date__c, Department__c,Group__c,Sales_Director__r.Name, Sales_Team__c, Suite_Of_Interest__c from User where ID in: userIDs];
        for(Task t : tasks){
            for(User u : Users){
                //Get Information for the User Creating the Task
                if(u.Id == System.UserInfo.getUserId()){
                    t.New_Created_By_Role__c = u.Role__c;
                    t.New_Created_By_Start_Date__c = u.Start_Date__c;
                    t.Created_By_Group__c = u.Group__c;
                }
                //Get Information for the User who Owns the Task
                if(u.Id == t.OwnerId){
                    t.New_Department__c = u.Department__c;
                    t.New_Group__c = u.Group__c;
                    t.New_Owner_Role__c = u.Role__c;
                    t.New_Sales_Director__c = u.Sales_Director__r.Name;
                    t.New_Sales_Team__c = u.Sales_Team__c;
                    t.Suite_Of_Interest__c = u.Suite_Of_Interest__c;
                }
            }
        }
    }
    
    public static void taskOwnerUpdate(Task[] tasks) {
        //Set to Hold UserIDs
        set<id> userIDs = new set<id>();
        //Go through all the tasks and add the UserIDs to the Set
        for(Task t : tasks){
            userIDs.add(t.OwnerID);
        }
        //Query the Users
        User[] users = [Select ID, Role__c, Start_Date__c, Department__c,Group__c,Sales_Director__r.Name, Sales_Team__c, Suite_Of_Interest__c from User where ID in: userIDs];
        for(Task t : tasks){
            for(User u : Users){
                //Update the Task with the New Owner Information
                if(u.Id == t.OwnerId){
                    t.New_Department__c = u.Department__c;
                    t.New_Group__c = u.Group__c;
                    t.New_Owner_Role__c = u.Role__c;
                    t.New_Sales_Director__c = u.Sales_Director__r.Name;
                    t.New_Sales_Team__c = u.Sales_Team__c;
                    t.Suite_Of_Interest__c = u.Suite_Of_Interest__c;
                }
            }
        }
    }
    
     public static void eventCreated(Event[] events) {
        //Set to Hold UserIDs we want to Query
        set<id> userIDs = new set<id>();
        //Go through all the tasks and add the UserIDs to the Set
        for(Event e : events){
            userIDs.add(e.OwnerID);
            userIDs.add(System.UserInfo.getUserId());
        }
        //Query the Users
        User[] users = [Select ID, Role__c, Start_Date__c, Department__c,Group__c,Sales_Director__r.Name, Sales_Team__c, Suite_Of_Interest__c from User where ID in: userIDs];
        for(Event e : events){
            for(User u : Users){
                //Get Information for the User Creating the Task
                if(u.Id == System.UserInfo.getUserId()){
                    e.New_Created_By_Role__c = u.Role__c;
                    e.New_Created_By_Start_Date__c = u.Start_Date__c;
                    e.Created_By_Group__c = u.Group__c;
                }
                //Get Information for the User who Owns the Task
                if(u.Id == e.OwnerId){
                    e.New_Department__c = u.Department__c;
                    e.New_Group__c = u.Group__c;
                    e.New_Owner_Role__c = u.Role__c;
                    e.New_Sales_Director__c = u.Sales_Director__r.Name;
                    e.New_Sales_Team__c = u.Sales_Team__c;
                    e.Suite_Of_Interest__c = u.Suite_Of_Interest__c;
                }
            }
        }
    }
    
    public static void eventOwnerUpdate(Event[] events) {
        //Set to Hold UserIDs
        set<id> userIDs = new set<id>();
        //Go through all the tasks and add the UserIDs to the Set
        for(Event e : events){
            userIDs.add(e.OwnerID);
        }
        //Query the Users
        User[] users = [Select ID, Role__c, Start_Date__c, Department__c,Group__c,Sales_Director__r.Name, Sales_Team__c, Suite_Of_Interest__c from User where ID in: userIDs];
        for(Event e : events){
            for(User u : Users){
                //Update the Task with the New Owner Information
                if(u.Id == e.OwnerId){
                    e.New_Department__c = u.Department__c;
                    e.New_Group__c = u.Group__c;
                    e.New_Owner_Role__c = u.Role__c;
                    e.New_Sales_Director__c = u.Sales_Director__r.Name;
                    e.New_Sales_Team__c = u.Sales_Team__c;
                    e.Suite_Of_Interest__c = u.Suite_Of_Interest__c;
                }
            }
        }
    }
    
    public static void activityDateUpdate(Task[] tasks){
        //Update the Task with the new ActivityDate
        for(Task t : tasks){
            t.Activity_Date_Only__c = t.ActivityDate;
        }
    }
    
    public static void activityDateEventUpdate(Event[] events){
        //Update the Event with the new ActivityDate
        for(Event e : events){
            e.Activity_Date_Only__c = date.valueof(e.StartDateTime);
        }
    }
    
    public static void poiCallResults(Task[] tasks) {
        POI_Call_Results_Mapping__c[] callResults = [SELECT id, Call_Results__c, POI_Status__c, Add_To_Unable_To_Connect__c
                                                     FROM POI_Call_Results_Mapping__c];
        for (Task t : tasks) {
            for (POI_Call_Results_Mapping__c cr : callResults) {
                if(t.Opt_Out_Reason__c != null && t.Opt_Out_Reason__c == cr.Call_Results__c){
                    t.POI_Status__c = cr.POI_Status__c;
                    t.POI_Unable_To_Connect__c = cr.Add_To_Unable_To_Connect__c;
                }else if (t.Opt_Out_Reason__c == null && t.Call_Result__c == cr.Call_Results__c) {
                    t.POI_Status__c = cr.POI_Status__c;
                    t.POI_Unable_To_Connect__c = cr.Add_To_Unable_To_Connect__c;
                }
            }
        }
    }
}