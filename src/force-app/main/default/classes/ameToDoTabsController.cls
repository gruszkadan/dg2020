public class ameToDoTabsController {
    public Attachment[] attachments {get;set;}
    public Id selectedAMEId {get;set;}
    Public string currentView {get;set;}
    public string sortField {get;set;} 
    public string sortDirection {get;set;}
    public string ViewAsUser {get;set;}
    public Account_Management_Event__c ame {get;set;}
    public Account ActDetail {get;set;}
    public Task[] QueryOpenTasks {get;set;}
    public Task[] QueryCompTasks {get;set;}
    public Task[] QueryAllTasks {get;set;}
    //  public Task[] QueryAllTasks {get;set;}
    public Task[] AddTasks {get;set;}
    public Task ViewTask {get;set;}
    public Task InlineTask {get;set;}
    public ProcessInstance[] ameApproval {get;set;}
    public list<User> Users {get;set;}
    public string errorMsg {get;set;}
    public Boolean TaskUpserted {get;set;}
    public id TaskId {get;set;}
    public Order_Item__c[] ameOrderItems {get;set;}
    public boolean showTab {get;set;}
    public boolean editingTask {get;set;}
    Public string EditTasks {get;set;}    
    
    public void getInitialize() {
        //Call all methods to query all necessary data on page load 
        Queryattachments();
        getAmeRecord();
        getAccount();
        getOpenTasks();
        getClosedTasks();
        getApprovals();
        NewTask();
        RefreshTaskTable();
        
        AddTasks = new list<Task>();
        
        InlineTask();
        EditTasks = '';
        
        // Query for orderItems (show/hide cancellaed products tab)
        
        ameOrderItems = [SELECT Id, Admin_Tool_Product_Name__c, Renewal_Amount__c, Term_Start_Date__c, Term_End_Date__c, Do_Not_Renew__c, Account__r.Channel__c, Cancelled_On_AME__c 
                         FROM Order_Item__c 
                         WHERE Cancelled_on_AME__c =: ame.Id];
        
        if(ameOrderItems.size() > 0){
            showTab = true;
        }else{
            showTab = false;
        }
        
        
    }
    
    public Account_Management_Event__c getAmeRecord() {
        //Query for ame fields diplayed on related ame and new task modal pages
        ame = [SELECT ID, name, account__r.name, Account__c, Owner.name, ownerId, Contact__r.name, Related_AME__c, RecordTypeId, RecordType.name FROM Account_Management_Event__c WHERE Id =: selectedAMEId]; 
        return ame;
    }
    public Account getAccount() {
        getAmeRecord();
        //query for all fields that will be dislayed on the account detail page
        ActDetail = [SELECT id, name, Health_Score__c, Chemical_Management_NPS_Score__c, Num_of_Locations__c, AnnualRevenue, Num_of_NA_Locations__c, Current_Chemical_Management_System__c,
                     Num_of_Locations_Under_Contract__c, NAICS_Code__c, Num_of_SDS__c, Primary_NAICS_Code__c, Num_of_Employees__c, Primary_SIC_Code__c, Num_of_Beds__c,
                     Company_Type__c, Companies_Encountered__c, Chemical_Management_Version__c, AID__c
                     FROM Account WHERE Id =: ame.Account__c];
        
        return actDetail;
    }
    
    public Task[] getOpenTasks() {
        getAmeRecord();
        //query for all open tasks related to the account associated with the ame 
        QueryOpenTasks =  [SELECT id, AccountId, Status, OwnerId, owner.name, Type__c, Description, ActivityDate, Subject
                           FROM Task 
                           WHERE WhatId =: ame.Id AND Status != 'Completed'
                           ORDER BY ActivityDate DESC];      
        
        return QueryOpenTasks;
    }
    
    public Task[] getClosedTasks() {
        getAmeRecord();
        //query for all closed tasks related to the account associated with the ame 
        QueryCompTasks = [SELECT id, AccountId, Status, OwnerId, owner.name, Type__c, Description, ActivityDate, Subject
                          FROM Task 
                          WHERE WhatId =: ame.Id AND Status = 'Completed'
                          ORDER BY ActivityDate ASC];
        
        return QueryCompTasks;
    }

    public void RefreshTaskTable(){
        
        
        QueryAllTasks = [SELECT id, AccountId, Status, OwnerId, owner.name, Type__c, Description, ActivityDate, Subject
                         FROM Task 
                         WHERE WhatId =: ame.Id 
                         ORDER BY ActivityDate DESC];
        
    }
    
    
    
    public List<SelectOption> getReps() {
        // method to auto populate the assigned to field in new task modal 
        // Only if created by rentention rep/manager
        // Query users where their role contains 'Retention'
        users = [Select Id, Name From User 
                 where isActive = TRUE
                 and UserRole.Name LIKE '%Retention%'
                 ORDER BY Name ASC];
        
        //add first selection of --Select User-- to the select options list 
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- Select User --',false));        
        //Loop through the user query and add them to the select option list 
        for (User user : users) {
            options.add(new SelectOption(user.ID, user.Name));
        }
        return options;
    }  
    
    public void Queryattachments() {
        //method to find atachemnts related to the selected ame to be displated on the attachments tab 
        attachments = [SELECT ID, Name, LastModifiedDate, OwnerId, ContentType, CreatedDate, BodyLength FROM Attachment WHERE ParentId =: selectedAMEId];
    }
    
    public ProcessInstance[] getApprovals() {
        //Query apporval process instance and the work items related to the process instance tp be displayed on approval tab 
        ameApproval = [SELECT CompletedDate, ElapsedTimeInDays, ElapsedTimeInHours, ElapsedTimeInMinutes, Id, CreatedDate, ProcessDefinitionId, Status, SubmittedById, TargetObjectId,
                       (SELECT Id, StepStatus, ActorId, Actor.Name, OriginalActorId, OriginalActor.Name, CreatedDate, Comments FROM StepsAndWorkitems ORDER BY CreatedDate DESC, Id DESC)
                       FROM ProcessInstance 
                       WHERE TargetObjectId =: selectedAMEId
                       ORDER BY CompletedDate DESC
                       NULLS FIRST];
        
        return ameApproval;
    }
    
    public void NewTask(){
        // method to create new task when user preses the new button on task page 
        // Initialize new task 
        ViewTask = new Task();
        // Set fields regarding the new task 
        ViewTask.OwnerId = ame.OwnerId;
        ViewTask.whatid = ame.id;
        ViewTask.WhoId = ame.Contact__c;
        ViewTask.Status = 'Not Started';
        //set the varable we are using to confirm the task was upserted 
        TaskUpserted = null;
        editingTask = false;
    }
    
    public Boolean canSave(Task t){
        //Method to validate that all the required fields are filled in on new / edit task modals 
        //Set variable to true 
        boolean pass = true;
        // if field is null set variable to false 
        if(t.OwnerId  == null){
            pass = false; 
        }
  
        if(t.ActivityDate  == null){
            pass = false;
        }
        
        if(t.Status == null){
            pass = false; 
        }
       if(t.subject == '' || t.subject == '--Select--'){
                pass = false;
            } 

        
        if(!FeatureManagement.checkPermission('AME_Console_Task_Table') ){
            if(t.Type__c == null){
                pass = false;
            } 
        }
                  
        if(FeatureManagement.checkPermission('AME_Console_Task_Table') ){
            if(t.Description == null){
                pass = false;
            } 
        }
        return pass;        
    }
    
    Public void SaveTask(){
        //method to upsert task only if user fills in all required fields and the valadation passes  
        //If can save method passes
        //set TaskUpserted variable to true and errorMsg to null and upsert task 
        if(canSave(ViewTask) == true){
            TaskUpserted = true;
            errorMsg = null;
            Upsert ViewTask; 
            // if can Cansave fails set the Taskupserted variable to false and display error message 
        }Else{ 
            TaskUpserted = false;
            errorMsg = 'Please fill in all required fields!';
        } 
    }
    
    
    Public void EditTask(){
        // method to be able to edit tasks that are open 
        // get paramenter related to open task 'currentTask' will hold the task Id
        string CurrentTaskId = system.currentpagereference().getparameters().get('currentTask'); 
        // Set Taskupserted variable to false 
        TaskUpserted = false;
        editingTask = true;
        //Loop through the open task and find the matching id
        for(Task T : QueryOpenTasks){
            If(T.Id == CurrentTaskId){
                ViewTask = T;
            }  
        }
    }
    
    Public void EditCompTask(){
        //method to give the user the ability to edit closed tasks 
        //get paramenter related to closed task 'CompTask' will hold the task Id
        string CompTaskId = system.currentpagereference().getparameters().get('CompTask'); 
        // set the Taskupserted variable to false 
        TaskUpserted = false;
        editingTask = true;
        //Loop through the open task and find the matching id
        for(Task T : QueryCompTasks){
            If(T.Id == CompTaskId){
                ViewTask = T;
            }  
        }
    }
    
    Public void EditAllTasks(){
        string AllTaskID = system.currentpagereference().getparameters().get('alltask'); 
        
        for(Task T : QueryAllTasks){
            If(T.Id == AllTaskID){
                EditTasks += T.id + ';';
            }  
        }
    }
    
    
    public void SaveTaskTable(){
        string SaveTaskTableID = system.currentpagereference().getparameters().get('SaveTaskTable'); 
        
        for(Task ut : QueryAllTasks){
            If(ut.id == SaveTaskTableID){
                if(canSave(ut) == true){
                    errorMsg = null;
                    update ut;
                    
                    String[] updatedEditTasks = new list<String>();
                    for(string x : EditTasks.split(';')){
                        if(x != ut.id){
                            updatedEditTasks.add(x);
                        }
                    }
                    EditTasks = String.join(updatedEditTasks,';');
                }else{
                    errorMsg = 'Please fill in all required fields!';
                }
            }
        }
    }
    
    Public void DeleteTask(){
        
        string AllTaskID = system.currentpagereference().getparameters().get('alltask'); 
        
        for(Task T : QueryAllTasks){
            If(T.Id == AllTaskID){
                Delete T;
            }  
        }
        RefreshTaskTable();        
    }
    
    Public void InlineTask(){
        InlineTask = new Task();
        
        InlineTask.OwnerId = UserInfo.getUserId();
        InlineTask.ActivityDate = date.today();
        InlineTask.whatid = ame.id;
        InlineTask.WhoId = ame.Contact__c;
        InlineTask.Status = 'Not Started';
        
        AddTasks.add(InlineTask);
        
    }
    
    Public void SaveInlineTask(){
        string index = system.currentpagereference().getparameters().get('index'); 
        
        if(canSave(addTasks[integer.valueof(index)]) == true){
            errorMsg = null;
            insert addTasks[integer.valueof(index)];
            AddTasks.remove(integer.valueof(index));
            RefreshTaskTable();
            
        }else{
            errorMsg = 'Please fill in all required fields!';
        }
    }
    
    
    
    
    //after the creation or edit of a task, reload page 
    public pageReference returnToAmeConsole(){        
        pageReference pr = page.ame_console_to_do;
        pr.getParameters().put('Id', selectedAMEId);
        pr.getParameters().put('currentView', currentView);
        pr.getParameters().put('sortField', sortField);
        pr.getParameters().put('sortDirection', sortDirection);
        pr.getParameters().put('va', ViewAsUser);   
        pr.setRedirect(true);
        return pr;
    }
    
}