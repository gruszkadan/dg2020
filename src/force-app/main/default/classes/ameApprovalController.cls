public class ameApprovalController {
    // These variables are holding vairaibles that are used to hold a specfic value used later in the page 
    public string approvalStatus {get;set;}
    public id ameId {get;set;}
    public string closeorRollover {get;set;}
    public string approvalComments {get;set;}
    public Order_Item__c[] ameOrderItems {get;set;}
    public Account_Management_Event__c ame {get;set;}
    //This varible is used when the user selects to roll over tasks to a new AME 
    public Account_Management_Event__C pendingAme {get;set;}
    //Creating lists varaibales that will hold the value of the queries needed 
    public Task[] taskList {get;set;}
    public ProcessInstanceWorkItem[] piwi {get;set;}
    
    
    public ameApprovalController(ApexPages.StandardController stdcontroller) {
        // We are setting the variables below at page load 
        approvalStatus ='Approve';
        ameId = ApexPages.currentPage().getParameters().get('Id');
        closeorRollover = 'Close';
        
        
        //In this query  we are finding open tasks related to our AME 
        TaskList = [Select id, Status
                    From Task
                    Where IsClosed = false and whatId = :ameId];
        
        //Query for list of order items related to cancellation ame
          
         ameOrderItems = [SELECT Id, Admin_Tool_Product_Name__c, Renewal_Amount__c, Term_Start_Date__c, Term_End_Date__c, Do_Not_Renew__c, Account__r.Channel__c, Cancelled_On_AME__c 
                            //(Select Id, RecordTypeId FROM Account_Management_Events__r WHERE RecordType.Name = 'Cancellation' ) 
                             FROM Order_Item__c 
                             WHERE Admin_Tool_Order_Status__c = 'A' AND Cancelled_on_AME__c = :ameId];
        
        //In this query we are finding whats is in our current AME to be mapped to our new AME later Also, it is used to map fields when creating new tasks when the AME is "Rejected"
        pendingAme = [Select id, Account__c, Contact__c, Status__c, RecordTypeId, ownerId, name, Cancellation_Reason__c, New_System__c, RecordType.Name, Notes__c
                      From Account_Management_Event__c
                      Where Id = :ameId];
        
        // We are querying the process instance work item related to our AME  
        piwi = [SELECT id,ProcessInstanceId,ActorId
                FROM ProcessInstanceWorkItem
                WHERE ProcessInstance.completedDate = null and ProcessInstance.TargetObjectId = :ameId];
        
    }
    public PageReference Submit(){
        //Set variables that will be used in to display new ame name when task get created for the new ame roll over 
        boolean rollOver = false;
        id NewAmeId;
        
        //Creating a new process instance work item, setting the approval comments and what approval status the user selected 
        //Then we are starint with the first process instance wok item and returning the results in the approval steps related list 
        Approval.ProcessWorkitemRequest preq = new Approval.ProcessWorkitemRequest();
        preq.setComments(approvalComments);
        preq.setAction(approvalStatus);
        preq.setWorkitemId(piwi[0].id);
        Approval.ProcessResult result = Approval.process(preq);
        
        //If the user selected to approve the ame and choses tp close out the tasks on the AME 
        if(approvalStatus == 'Approve' && closeorRollover == 'Close'){
            //We will loop through our task list and for all open tasks we are setting the status to complete 
            For(Task t : taskList){
                t.Status = 'Completed';  
            }    
        }
        //if the user selects to approve the AME abd roll the tasks to a new AME
        if(approvalStatus == 'Approve' && closeorRollover == 'Roll'){
            //We will create a new AME 
            //Then we will use our "Pending AME" query to set the fields in our new AME
            Account_Management_Event__c NewAme = new Account_Management_Event__c();
            newAme.Account__c = pendingAme.Account__c;
            newAme.Contact__c = pendingAme.Contact__c;
            newAme.RecordTypeId = pendingAme.RecordTypeId;
            newAme.Related_AME__c = ameId;
            newAme.OwnerId = pendingAme.OwnerId;
            newAme.Status__c = 'Active';
            
            insert Newame;
            
            //We will need to loop through our open task list and set the "Related to Id" feild to our AME ID
            For(Task t : taskList){
                t.WhatId = newame.Id;
            }
            // 
            rollover = true;
            NewAmeId = NewAme.Id;
            
        }
        // If the user selects to reject the approval 
        // Loop through the task list and mark all open task to completed 
        if(approvalStatus == 'Reject'){
            For(Task t : taskList){
                t.Status = 'Completed';
            } 
            //Then we will create a new task and set to feilds to a hard coded value or get the value from the pendingAme query 
            Task newTask = new Task();
            newTask.OwnerId = pendingAme.OwnerId;
            newTask.Status = 'Not Started';
            newTask.ActivityDate = Date.today();
            newTask.Description = approvalComments;
            newTask.Type__c = 'Update';
            newTask.Subject = 'Approval Revision';
            newTask.WhatId = ameId;
            newTask.WhoId =  pendingAme.Contact__c;
            
            insert newTask;
            
        }
        //If the task list query returns and records we will update the tasklist
        //if there are 0 tasks we will not update the list
        if(Tasklist.size() > 0){
            
            update tasklist;
        }
        // Creating a page refrence to return user to current ame
        //set parameters in url whn rolling over tasks 
        //get new ame id to be used to display ame name when rolling over open tasks 
        //Return the user to the VF AME detail page 
        
        PageReference pr = page.ame_detail;
        pr.getParameters().put('Id', ameid);
        pr.getParameters().put('rollOver', string.valueOf(rollover));
        pr.getParameters().put('RollId', NewAmeId);
        
        return pr.setRedirect(true);
    }
}