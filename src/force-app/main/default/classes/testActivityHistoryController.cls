@isTest(seeAllData=true)
private class testActivityHistoryController {
    
    static testMethod void test1() {
        
        id ownerId = [SELECT id 
                      FROM User 
                      WHERE FirstName = 'Ryan' 
                      AND LastName = 'Werner' LIMIT 1].id;
        
        Account a = new Account(Name='Test Account');
        insert a;
        
        Contact c = new Contact(FirstName='Bill', LastName='Test', AccountId=a.id);
        insert c;
        
        Task[] tasks = new List<Task>();
        for (integer i=0; i<3; i++) {
            Task t = new Task();
            t.OwnerId = ownerId;
            t.Subject = 'Test task';
            t.WhatId = a.id;
            t.WhoId = c.id;
            t.Status = 'Completed';
            if (i==2) {
                t.Important_Note__c = true;
            }
            tasks.add(t);
        }
        insert tasks;
        
        Event[] events = new List<Event>();
        for (integer i=0; i<3; i++) {
            Event e = new Event();
            e.OwnerId = ownerId;
            e.Subject = 'Test event';
            e.WhatId = a.id;
            e.WhoId = c.id;
            e.DurationInMinutes = 10;
            e.StartDateTime = datetime.now();
            e.Event_Status__c = 'Completed';
            if (i==2) {
                e.Important_Note__c = true;
            }
            events.add(e);
        }
        insert events;
        
        ApexPages.currentPage().getParameters().put('id',a.id);
        ApexPages.currentPage().getParameters().put('f','All Important');
        activityHistoryController con = new activityHistoryController();
		con.queryAll(); 
        con.createHistories();
        ApexPages.currentPage().getParameters().put('ahf','');
        con.filterList();
        con.gotoNextPage();
        con.gotoNextPage();
        con.gotoPrevPage();
        con.back();
        
    }
    
}