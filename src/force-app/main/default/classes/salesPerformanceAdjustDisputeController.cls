//Test Class: testSalesPerformanceRequestIncentives
public with sharing class salesPerformanceAdjustDisputeController {
    public String viewAs {get;set;}
    public string userID {get;set;}
    public User u {get;set;}
    public User currentUser {get;set;}
    public String defaultView {get;set;}
    public Integer stepNumber {get;set;}
    public Sales_Performance_Request__c spr {get;set;}
    //Step 1
    public String orderSearchString {get;set;}
    public id selOrderId {get;set;}
    public Sales_Performance_Order__c[] spoSearchResults {get;set;}
    public Sales_Performance_Order__c selectedOrder {get;set;}
    public Sales_Performance_Order__c spoDetail {get;set;}
    public itemWrapper[] itemWrappers {get;set;}
    //Step 2
    public list<String> approverNames {get;set;}
    public String errorString {get;set;}
    public String reason {get;set;}
    public boolean requestSubmitted {get;set;}
    
    public salesPerformanceAdjustDisputeController(){
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias, FirstName, LastName, Name, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID, Sales_Director__c 
             FROM User 
             WHERE id = :viewAs LIMIT 1];
        
        userID = u.id;
        //get defaultView to display when viewing as
        defaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
        //Set the step number to 1 on page load
        string xSpoId = ApexPages.currentPage().getParameters().get('id');
        if(xSpoId == null || xSpoId == ''){
            stepNumber = 1;
        }else{
            findSpoFromParam(xSpoId);
        }
    } 
    
    //Search query for sales performance orders in step 1
    public void searchOrders(){
        selOrderId = null;
        string query = 'SELECT id, Order_ID__c, Owner.Name, OwnerId, Contract_Number__c, Booking_Amount__c, ' +
            'Account__r.Name, Order_Date__c, Account__r.AdminId__c, Contract_Length__c, Month__c, Year__c, Account__c, isSplit__c, isSplitMaster__c, ' +
            '(SELECT id, OwnerId, Original_Booking_Amount__c, Product_Name__c, Name, Booking_Amount__c, Order_Item__c, Order_Item_ID__c, Order_ID__c, '+
            'Month__c, Order_Date__c, Year__c, Contract_Length__c, Account__c, Invoice_Amount__c, Contract__c, Contract_Number__c, Sales_Rep_Sales_Performance_Summary__c, '+
            'Sales_Manager__c, Sales_Director__c, Sales_VP__c, Group__c, Role__c, Total_Incentive_Amount__c FROM Sales_Performance_Items__r), ' +
            '(SELECT id, OwnerId, Owner.Name, Name, SPI_Split_From__c, SPO_Split_From__c, SPI_Split_To__c, SPO_Split_To__c, Booking_Amount__c, '+
            'Incentive_Amount__c, Product_Name__c, Split_Group__c, Sales_Performance_Request__c FROM Sales_Performance_Item_Splits__r ORDER BY Split_Group__c ASC) ' +
            'FROM Sales_Performance_Order__c ' +
            'WHERE (Account__r.Name LIKE \'%' + String.escapeSingleQuotes(orderSearchString) + '%\' ' +
            'OR Contract_Number__c LIKE \'%' + String.escapeSingleQuotes(orderSearchString) + '%\') '+
            'AND Type__c = \'Booking\' ';
        if(defaultView == 'Rep'){
            query+= 'AND OwnerId = \''+userId+'\' ';
        }
        query+= 'AND Num_Pending_Sales_Performance_Requests__c = 0 LIMIT 5 ';
        spoSearchResults = database.query(query);
    }
    
    //Finds and sets selectedOrder based on the id passed in from the html parameter
    public void findSpoFromParam(string paramId){
        selOrderId = null;
        selectedOrder = [SELECT id, Order_ID__c, Owner.Name, OwnerId, Contract_Number__c,Booking_Amount__c,
                         Account__r.Name, Order_Date__c, Account__r.AdminId__c, Contract_Length__c, Month__c, Year__c, Account__c, isSplit__c, isSplitMaster__c,
                         (SELECT id, OwnerId, Original_Booking_Amount__c, Product_Name__c, Name, Booking_Amount__c, Order_Item__c, Order_Item_ID__c, Order_ID__c,
                          Order_Item__r.Admin_Tool_Product_Name__c, Order_Item__r.Product_Platform__c, Order_Item__r.Salesforce_Product_Name__c, Month__c, Order_Date__c, Year__c,
                          Order_Item__r.Category__c, Contract_Length__c, Account__c, Invoice_Amount__c, Contract__c, Contract_Number__c, Sales_Rep_Sales_Performance_Summary__c, 
                          Sales_Manager__c, Sales_Director__c, Sales_VP__c, Group__c, Role__c, Total_Incentive_Amount__c FROM Sales_Performance_Items__r)
                         FROM Sales_Performance_Order__c
                         WHERE id =:paramId
                         AND Num_Pending_Sales_Performance_Requests__c = 0 
                         AND Type__c = 'Booking'
                         LIMIT 1];
        if(selectedOrder != null){
            createOrderWrappers();
            stepNumber = 2;
        }
    }
    
    //Method that sets the selected sales performance order once chosen by the user
    public void selectOrder(){
        if(selOrderId != null){
            if((selectedOrder != null && selOrderId != selectedOrder.id) || selectedOrder == null){
                for(Sales_Performance_Order__c spo:spoSearchResults){
                    if(spo.id == selOrderId){
                        selectedOrder = spo;
                        createOrderWrappers();
                        stepNumber = 2;
                        break;
                    }
                }
            }
        }
    }
    
    public void createOrderWrappers(){
        itemWrappers = new list<itemWrapper>();
        for(Sales_Performance_Item__c spi:selectedOrder.Sales_Performance_Items__r){
            itemWrappers.add(new itemWrapper(spi));
        }
    }
    
    public void viewOrderDetails(){
        id spoId = ApexPages.currentPage().getParameters().get('spoId');
        if(spoId != null){
            for(Sales_Performance_Order__c spo:spoSearchResults){
                if(spo.id == spoId){
                    spoDetail = spo;
                    break;
                }
            }
        }
    }
    
    public void backToStep1(){
        orderSearchString = null;
        selectedOrder = null;
        spoSearchResults = null;
        stepNumber = 1;
    }
    
    public void needsAdjustment(){
        id item = ApexPages.currentPage().getParameters().get('needsAdjustmentId');
        for (itemWrapper i:itemWrappers){
            if(i.spi.id == item){
                if(i.needsAdjustment){
                    i.needsAdjustment = false;
                }else{
                    i.needsAdjustment = true;
                }
            }
        }
    }
    
    //Wrapper to hold selected order and proposed incentives
    public class itemWrapper {
        public Sales_Performance_Item__c spi {get;set;}
        public decimal proposedIncentive {get;set;}
        public boolean needsAdjustment {get;set;}
        
        public itemWrapper(Sales_Performance_Item__c xSpi){
            spi = xSpi;
            needsAdjustment = false;
        }
    }
    
    public void submitAdjustment(){
        approverNames = new list<String>();
        errorString = null;
        requestSubmitted = false;
        
        //Create a new SPR record for the approval
        spr = new Sales_Performance_Request__c(OwnerId = u.id, Type__c = 'Adjustment/Dispute',Reason__c = reason,Status__c = 'Pending Approval',Sales_Rep__c = selectedOrder.OwnerId, Requires_Approval__c = true, Sales_Performance_Order__c = selectedOrder.id);
        
        //Find the SPS to associate the SPR to
        spr.Sales_Performance_Summary__c = salesPerformanceRequestUtility.findSprSps(selectedOrder, null);
        
        //Loop through the list of item wrappers and create a new list of strings for ones that have incentive adjustments
        String[] adjustmentsList = new list<String>();
        for(itemWrapper wrap:itemWrappers){
            if(wrap.needsAdjustment){
                adjustmentsList.add(wrap.spi.id+'|'+wrap.proposedIncentive+'|'+wrap.spi.Total_Incentive_Amount__c);
            }
        }
        
        //If there are any items with an adjustment
        if(adjustmentsList.size() > 0){
            if(reason != ''){
                //convert the list to JSON and set the json field on the SPR
                spr.JSON__c = json.serialize(adjustmentsList);
                //query the manager & director fields for the owner of the order and set approver fields as necessary
                User orderOwner = [SELECT id,ManagerId,Manager.Name,Sales_Director__c,Sales_Director__r.Name FROM User WHERE id =:selectedOrder.OwnerId LIMIT 1];
                if(defaultView == 'Rep'){
                    spr.Assigned_Approver_1__c = orderOwner.ManagerId;
                    approverNames.add(orderOwner.Manager.Name);
                    if(orderOwner.ManagerId != orderOwner.Sales_Director__c){
                        spr.Assigned_Approver_2__c = orderOwner.Sales_Director__c;
                        approverNames.add(orderOwner.Sales_Director__r.Name);
                    }
                }else if(defaultView == 'Manager'){
                    spr.Assigned_Approver_1__c = orderOwner.Sales_Director__c;
                    approverNames.add(orderOwner.Sales_Director__r.Name);
                }else{
                    spr.Status__c = 'Approved'; 
                    spr.Requires_Approval__c = false;
                }
                insert spr;
                requestSubmitted = true;
            }else{
                errorString = 'You must enter a reason for the request.';
            }
        }else{
            errorString = 'No adjustments have been proposed. At least one item must have a proposed incentive change.';
        }
    }
}