@isTest
public class testSalesPerformancePendingSprsScheduler {
    

    public static testmethod void test1(){
        
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            //Create test data
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            
            DateTime d = datetime.now().addMonths(-1);
            String monthName= d.format('MMMMM');
            System.debug(monthName);
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Month__c = monthName;
            s.Year__c = string.valueof(d.year());
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            //s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Sales_Director__c = dir.id;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            
            Sales_Performance_Request__c newSPR = new Sales_Performance_Request__c(Rollup_to_Rep__c = TRUE,Rollup_to_Manager__c = TRUE,Rollup_to_Director__c = TRUE,Rollup_to_VP__c = TRUE,Incentive_Month__c = 'Previous Month');
            newSPR.Type__c = 'Ghost Booking';
            newSPR.Status__c ='Pending Approval';
            newSPR.Requires_Approval__c = TRUE;
            newSPR.Assigned_Approver_1__c = mgr.id;
            newSPR.Assigned_Approver_2__c = dir.id;
            newSPR.Assigned_Approver_3__c = vp.id;
            newSPR.OwnerId = rep.id;
            newSPR.Sales_Rep__c = rep.id;
            newSPR.Booking_Amount__c = 100;
            newSPR.Incentive_Amount__c = 20;
            newSPR.Sales_Performance_Summary__c = s.id;
            insert newSPR;
            
            
            //Sales_Performance_Request__c newSPR2 = new Sales_Performance_Request__c(Rollup_to_Rep__c = TRUE,Rollup_to_Manager__c = TRUE,Rollup_to_Director__c = TRUE,Rollup_to_VP__c = TRUE,Incentive_Month__c = 'Previous Month');
           // newSPR.Type__c = 'Ghost Booking';
            //newSPR.Status__c ='Pending Approval';
            //newSPR.Requires_Approval__c = TRUE;
           // newSPR.Assigned_Approver_1__c = mgr.id;
           // newSPR.Assigned_Approver_2__c = dir.id;
           // newSPR.Assigned_Approver_3__c = vp.id;
           // newSPR.OwnerId = rep.id;
           // newSPR.Sales_Rep__c = rep.id;
           // newSPR.Booking_Amount__c = 150;
           // newSPR.Incentive_Amount__c = 101;
            //newSPR.Month_Incentive_Applied__c = Datetime.now().addMonths(-1).format('MMMM');
            //newSPR.Year_Incentive_Applied__c = Datetime.now().addMonths(-1).format('YYYY');
           // newSPR.Sales_Performance_Summary__c = s.id;
            //System.debug(newSPR.Month_Incentive_Applied__c);
           // insert newSPR2;
            
            //System.debug(newSPR2.id);
            System.debug(newSPR.id);
            
            ProcessInstance SPR2piwi = [Select id, TargetObjectId, (Select id,  ProcessInstanceId from WorkItems) from ProcessInstance]; //where targetObjectId = :newSPR2.id
            System.debug(SPR2piwi );
        	//we need SPR2piwi.id	
         
            approval.ProcessWorkitemRequest preq = new Approval.ProcessWorkitemRequest();
        	preq.setComments('Get to Approver Two');
        	preq.setAction('Approve');
            preq.setWorkitemId(SPR2piwi.WorkItems[0].id);
        	Approval.ProcessResult result = Approval.process(preq);
            
           	Sales_Performance_Request__c checkSPR2 = [Select id, Current_Approval_Step__c from Sales_Performance_Request__c];
            System.debug(checkSPR2.Current_Approval_Step__c);

        Test.startTest();
        salesPerformancePendingSprsScheduler sch1 = new salesPerformancePendingSprsScheduler();
        string schTime = '0 11 18 * * ?';
        System.schedule('Test SPR Scheduler', schTime, sch1);           
        Test.stopTest();
        }
    }
    
    public static testmethod void test2(){
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            //Create test data
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            
            DateTime d = datetime.now().addMonths(-1);
            String monthName= d.format('MMMMM');
            System.debug(monthName);
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Month__c = monthName;
            s.Year__c = string.valueof(d.year());
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            //s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Sales_Director__c = dir.id;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            
            Sales_Performance_Request__c newSPR = new Sales_Performance_Request__c(Rollup_to_Rep__c = TRUE,Rollup_to_Manager__c = TRUE,Rollup_to_Director__c = TRUE,Rollup_to_VP__c = TRUE,Incentive_Month__c = 'Previous Month');
            newSPR.Type__c = 'Ghost Booking';
            newSPR.Status__c ='Pending Approval';
            newSPR.Requires_Approval__c = TRUE;
            newSPR.Assigned_Approver_1__c = mgr.id;
            newSPR.Assigned_Approver_2__c = dir.id;
            newSPR.Assigned_Approver_3__c = vp.id;
            newSPR.OwnerId = rep.id;
            newSPR.Sales_Rep__c = rep.id;
            newSPR.Booking_Amount__c = 100;
            newSPR.Incentive_Amount__c = 20;
            newSPR.Sales_Performance_Summary__c = s.id;
            insert newSPR;
            
            //System.debug(newSPR2.id);
            System.debug(newSPR.id);
            
            
            Test.startTest();
            salesPerformancePendingSprsScheduler sch1 = new salesPerformancePendingSprsScheduler();
            string schTime = '0 11 18 * * ?';
            System.schedule('Test SPR Scheduler', schTime, sch1);
            
            
            Test.stopTest();
        }
    }
    
        public static testmethod void test3(){
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            //Create test data
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            
            DateTime d = datetime.now().addMonths(-1);
            String monthName= d.format('MMMMM');
            System.debug(monthName);
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Month__c = monthName;
            s.Year__c = string.valueof(d.year());
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            //s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Sales_Director__c = dir.id;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;

            Sales_Performance_Request__c newSPR = new Sales_Performance_Request__c(Rollup_to_Rep__c = TRUE,Rollup_to_Manager__c = TRUE,Rollup_to_Director__c = TRUE,Rollup_to_VP__c = TRUE,Incentive_Month__c = 'Previous Month');
            newSPR.Type__c = 'Ghost Booking';
            newSPR.Status__c ='Pending Approval';
            newSPR.Requires_Approval__c = TRUE;
            newSPR.Assigned_Approver_1__c = mgr.id;
            newSPR.Assigned_Approver_2__c = dir.id;
            newSPR.Assigned_Approver_3__c = vp.id;
            newSPR.OwnerId = rep.id;
            newSPR.Sales_Rep__c = rep.id;
            newSPR.Booking_Amount__c = 100;
            newSPR.Incentive_Amount__c = 20;
            newSPR.Sales_Performance_Summary__c = s.id;
            insert newSPR;
            
            //System.debug(newSPR2.id);
            System.debug(newSPR.id);
            
            ProcessInstance SPR2piwi = [Select id, TargetObjectId, (Select id,  ProcessInstanceId from WorkItems) from ProcessInstance]; //where targetObjectId = :newSPR2.id
            System.debug(SPR2piwi );	
         
            approval.ProcessWorkitemRequest preq = new Approval.ProcessWorkitemRequest();
        	preq.setComments('Get to Approver Two');
        	preq.setAction('Approve');
            preq.setWorkitemId(SPR2piwi.WorkItems[0].id);
        	Approval.ProcessResult result = Approval.process(preq);
            
           	Sales_Performance_Request__c checkSPR2 = [Select id, Current_Approval_Step__c from Sales_Performance_Request__c];
            System.debug(checkSPR2.Current_Approval_Step__c);
            
            
            ProcessInstance SPR2piwi2 = [Select id, TargetObjectId, (Select id,  ProcessInstanceId from WorkItems) from ProcessInstance]; 
            System.debug(SPR2piwi2 );	
            
            approval.ProcessWorkitemRequest preq2 = new Approval.ProcessWorkitemRequest();
        	preq2.setComments('Get to Approver Two');
        	preq2.setAction('Approve');
            preq2.setWorkitemId(SPR2piwi2.WorkItems[0].id);
        	Approval.ProcessResult results = Approval.process(preq2);


        Test.startTest();
        salesPerformancePendingSprsScheduler sch1 = new salesPerformancePendingSprsScheduler();
        string schTime = '0 11 18 * * ?';
        System.schedule('Test SPR Scheduler', schTime, sch1);

            
        Test.stopTest();
        }
    }

}