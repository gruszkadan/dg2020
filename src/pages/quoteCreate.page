<apex:page standardController="Quote__c" extensions="quoteNew" tabStyle="Quote__c">
    <apex:sectionHeader title="New Customer Quote" subtitle="Step 1"/>
    <apex:pageMessages id="msgs"/>
    <apex:form > 
        <apex:pageBlock title="Quote Information" mode="edit" id="pbs">
            <apex:pageBlockButtons >
                <apex:commandButton action="{!productEntry}" value="Save & Add Product(s)" rerender="msgs"/>
                <apex:commandButton action="{!cancel}" value="Cancel"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection showHeader="true" title="Information" columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Account" />
                    <apex:outputPanel > 
                        <div class="requiredInput"> 
                            <div class="requiredBlock"></div>
                            <apex:inputField value="{!quote.Account__c}"/>
                        </div>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Contact" />
                    <apex:outputPanel > 
                        <div class="requiredInput"> 
                            <div class="requiredBlock"></div>
                            <apex:selectList id="contactName"  size="1" value="{!quote.Contact__c}">
                                <apex:selectOptions value="{!ContactSelectList}"/>
                                <apex:actionSupport event="onchange" action="{!setName}" rerender="pbs"/>
                            </apex:selectList> 
                        </div>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!Quote__c.Type__c = 'New'}">
                    <apex:outputLabel value="Promo Code"/>
                    <apex:outputPanel id="item1">
                        <apex:selectList size="1" value="{!selPromo}">
                            <apex:selectOptions value="{!PromoSelectList}"/>
                            <apex:actionSupport event="onchange" action="{!findPromoDetails}" rerender="pbs"/>
                        </apex:selectList>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Contract Length" />
                    <apex:outputPanel > 
                        <div class="requiredInput"> 
                            <div class="requiredBlock"></div>
                            <apex:selectList value="{!quote.Contract_Length__c}" multiselect="false" size="1">
                                <apex:selectOptions value="{!ContractLengthVals}"/>
                            </apex:selectList>
                            <apex:outputText rendered="{!selPromo != null && promo.Minimum_Contract_Length__c != null}">
                                <span style="color:red;">This promo has a minimum contract length of {!promo.Minimum_Contract_Length__c}</span>
                            </apex:outputText>
                        </div>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:inputField label="Travelers Customer?" value="{!Quote.Travelers_Customer__c}"/>
                <apex:inputField label="Healthcare Customer?" value="{!Quote.Healthcare_Customer__c}">
                    <apex:actionSupport event="onchange" reRender="pbs"/>
                </apex:inputField>
                <apex:inputField rendered="{!Quote.Healthcare_Customer__c = 'Yes'}" label="GPO" value="{!Quote.GPO__c}"/>
                <apex:inputField label="VPPPA Customer?" value="{!Quote.VPPPA_Customer__c}"/>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Currency"/>
                    <apex:panelGroup >
                        <div class="requiredInput">
                            <div class="requiredBlock"/>
                            <apex:selectList size="1" value="{!selCurrency}">
                                <apex:selectOptions value="{!CurrencyTypes}"/>
                            </apex:selectList>
                        </div>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:inputField value="{!Quote.Spring_Into_Compliance__c}"/>
                <apex:inputField rendered="{!Quote.Department__c ='Customer Care' && u.Group__c = 'Retention'}" value="{!Quote.Type__c}"/>
                <apex:outputField rendered="{!Quote.Department__c ='Customer Care' && u.Group__c = 'Retention' && Quote.Account_Management_Event__c = ''}" value="{!Quote.Renewal__c}"/>
                <apex:outputField rendered="{!Quote.Department__c ='Customer Care' && u.Group__c = 'Retention' && Quote.Account_Management_Event__c != ''}" value="{!Quote.Account_Management_Event__c}"/>
                <apex:inputField rendered="false" value="{!Quote.Lead_Type__c}"/>
                <apex:inputField rendered="false" value="{!Quote.Opportunity__c}"/>
                <apex:inputField rendered="false" value="{!Quote.Department__c}" />
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Promo Details" columns="1" rendered="{!selPromo != null}">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Name"/>
                    <apex:outputText value="{!promo.Name}"/>
                </apex:pageBlockSectionItem>
                <apex:outputField value="{!promo.Code__c}"/>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Minimum Contract Length"/>
                    <apex:outputPanel layout="block">
                        <apex:outputText rendered="{!promo.Minimum_Contract_Length__c = null}">None</apex:outputText>
                        <apex:outputText rendered="{!promo.Minimum_Contract_Length__c != null}">{!promo.Minimum_Contract_Length__c}</apex:outputText>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem> 
                <apex:outputField value="{!promo.Start_Date__c}"/>
                <apex:outputField value="{!promo.End_Date__c}"/>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Included Products"/>
                    <apex:pageBlockTable value="{!promoProds}" var="p" style="width:85%;">
                        <apex:column headerValue="Name" width="300">
                            {!p.Name__c}
                            &nbsp;&nbsp;&nbsp;
                        </apex:column>
                        <apex:column headerValue="Discount">
                            {!p.Discount__c} {!p.Discount_Method__c}
                            &nbsp;&nbsp;&nbsp;
                        </apex:column>
                        <apex:column headerValue="Auto-Add">
                            <apex:outputField value="{!p.Auto_Add__c}"/>
                        </apex:column>
                        <apex:column headerValue="Year 1">
                            <apex:outputField value="{!p.Year_1__c}"/>
                        </apex:column>
                        <apex:column headerValue="Year 2">
                            <apex:outputField value="{!p.Year_2__c}"/>
                        </apex:column>
                        <apex:column headerValue="Year 3">
                            <apex:outputField value="{!p.Year_3__c}"/>
                        </apex:column>
                        <apex:column headerValue="Year 4">
                            <apex:outputField value="{!p.Year_4__c}"/>
                        </apex:column>
                        <apex:column headerValue="Year 5">
                            <apex:outputField value="{!p.Year_5__c}"/>
                        </apex:column>
                        <apex:column headerValue="Main Bundled Product">
                            <apex:outputField value="{!p.Is_Main_Bundled_Product__c}"/>
                        </apex:column>
                    </apex:pageBlockTable>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>