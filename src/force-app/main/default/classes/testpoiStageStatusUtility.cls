@isTest(seeAllData=true)
private class testpoiStageStatusUtility {
    
    static testmethod void test_leadContactStatus() {
        //Setup the test data
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        insert c;
        Lead l = testLead();
        insert l;
        test.startTest();
        c.Status__c = 'Out of Business';
        update c;
        l.Status = 'Dead - Out of Business';
        update l;
        test.stopTest();
    }
    
    static testmethod void test_opportunityStatus() {
        Account[] accts = new list<Account>();
        Account a1 = testAccount();
        Account a2 = testAccount();
        Account a3 = testAccount();
        Account a4 = testAccount();
        Account a5 = testAccount();
        accts.addAll(new list<Account>{a1, a2, a3, a4, a5});
        insert accts;
        
        Contact[] conts = new list<Contact>();
        Contact c1 = testContact(a1.id);
        Contact c2 = testContact(a2.id);
        Contact c3 = testContact(a3.id);
        Contact c4 = testContact(a4.id);
        Contact c5 = testContact(a5.id);
        conts.addAll(new list<Contact>{c1, c2, c3, c4, c5});
        insert conts;
        
        Opportunity[] opps = new list<Opportunity>();
        Opportunity opp1 = testOpportunity(a1.id, c1.id);
        opp1.Suite_Of_Interest__c = 'MSDS Management';
        opp1.Opportunity_Type__c = 'New';
        Opportunity opp2 = testOpportunity(a2.id, c2.id);
        opp2.Suite_Of_Interest__c = 'EHS Management';
        opp2.Opportunity_Type__c = 'New';
        Opportunity opp3 = testOpportunity(a3.id, c3.id);
        opp3.Suite_Of_Interest__c = 'MSDS Authoring';
        opp3.Opportunity_Type__c = 'New';
        Opportunity opp4 = testOpportunity(a4.id, c4.id);
        opp4.Suite_Of_Interest__c = 'On-Demand Training';
        opp4.Opportunity_Type__c = 'New';
        Opportunity opp5 = testOpportunity(a5.id, c5.id);
        opp5.Suite_Of_Interest__c = 'Ergonomics';
        opp5.Opportunity_Type__c = 'New';
        opps.addAll(new list<Opportunity>{opp1, opp2, opp3, opp4, opp5});
        insert opps;
        
        test.startTest();
        opp1.StageName = 'Closed/Won';
        opp2.StageName = 'Closed/Won';
        opp3.StageName = 'Closed/Won';
        opp4.StageName = 'Closed';
        opp4.Closed_Reasons__c = 'Not ready to adapt';
        opp5.StageName = 'Closed Lost';
        opp5.Reason_Lost__c = 'Lost due to price';
        opp5.Main_Competitor__c = '3E';
        opp5.Pricing_Details__c = 'test';
        update opps;
        test.stopTest();
    } 
    
    static testmethod void test_taskStatus_msds() {
        User owner = [SELECT id FROM User WHERE isActive = true AND Suite_Of_Interest__c = 'MSDS Management' LIMIT 1];
        Account a = testAccount();
        insert a;
        Contact c1 = testContact(a.id);
        c1.POI_Status_MSDS__c = 'Working - Connected';
        insert c1;
        Lead l = testLead();
        insert l;
        Task c1Task1 = new Task(WhoId=c1.id, Status='Completed', Call_Result__c='Call - No Result', 
                                POI_Status__c='Working - Pending Response', POI_Unable_To_Connect__c=true);
        
        Task c2Task1 = new Task(WhoId=c1.id, Status='Completed', Call_Result__c='Call - No Result', 
                                POI_Status__c='Open', POI_Unable_To_Connect__c=true);
        
        Task lTask1 = new Task(WhoId=l.id, Status='Completed', Call_Result__c='Confirm Demo', 
                               POI_Status__c='Working - Connected', POI_Unable_To_Connect__c=false);
        test.startTest();
        System.runAs(owner) {
            insert new list<Task>{c1Task1,c2task1,lTask1};
                }
        test.stopTest(); 
    }
    
    static testmethod void test_taskStatus_ehs() {
        User owner = [SELECT id FROM User WHERE isActive = true AND Suite_Of_Interest__c = 'EHS Management' LIMIT 1];
        Account a = testAccount();
        insert a;
        Contact c1 = testContact(a.id);
        c1.POI_Status_EHS__c = 'Working - Connected';
        insert c1;
        Lead l = testLead();
        insert l;
        Task c1Task1 = new Task(WhoId=c1.id, Status='Completed', Call_Result__c='Call - No Result', 
                                POI_Status__c='Working - Pending Response', POI_Unable_To_Connect__c=true);
        
        Task c2Task1 = new Task(WhoId=c1.id, Status='Completed', Call_Result__c='Call - No Result', 
                                POI_Status__c='Open', POI_Unable_To_Connect__c=true);
        
        Task lTask1 = new Task(WhoId=l.id, Status='Completed', Call_Result__c='Confirm Demo', 
                               POI_Status__c='Working - Connected', POI_Unable_To_Connect__c=false);
        test.startTest();
        System.runAs(owner) {
            insert new list<Task>{c1Task1,c2task1,lTask1};
                }
        test.stopTest(); 
    }
    
    static testmethod void test_taskStatus_auth() {
        User owner = [SELECT id FROM User WHERE isActive = true AND Suite_Of_Interest__c = 'MSDS Authoring' LIMIT 1];
        Account a = testAccount();
        insert a;
        Contact c1 = testContact(a.id);
        c1.POI_Status_AUTH__c = 'Working - Connected';
        insert c1;
        Lead l = testLead();
        insert l;
        Task c1Task1 = new Task(WhoId=c1.id, Status='Completed', Call_Result__c='Call - No Result', 
                                POI_Status__c='Working - Pending Response', POI_Unable_To_Connect__c=true);
        
        Task c2Task1 = new Task(WhoId=c1.id, Status='Completed', Call_Result__c='Call - No Result', 
                                POI_Status__c='Open', POI_Unable_To_Connect__c=true);
        
        Task lTask1 = new Task(WhoId=l.id, Status='Completed', Call_Result__c='Confirm Demo', 
                               POI_Status__c='Working - Connected', POI_Unable_To_Connect__c=false);
        test.startTest();
        System.runAs(owner) {
            insert new list<Task>{c1Task1,c2task1,lTask1};
                }
        test.stopTest(); 
    }
    
    static testmethod void test_taskStatus_odt() {
        User owner = [SELECT id FROM User WHERE isActive = true AND Suite_Of_Interest__c = 'On-Demand Training' LIMIT 1];
        Account a = testAccount();
        insert a;
        Contact c1 = testContact(a.id);
        c1.POI_Status_ODT__c = 'Working - Connected';
        insert c1;
        Lead l = testLead();
        insert l;
        Task c1Task1 = new Task(WhoId=c1.id, Status='Completed', Call_Result__c='Call - No Result', 
                                POI_Status__c='Working - Pending Response', POI_Unable_To_Connect__c=true);
        
        Task c2Task1 = new Task(WhoId=c1.id, Status='Completed', Call_Result__c='Call - No Result', 
                                POI_Status__c='Open', POI_Unable_To_Connect__c=true);
        
        Task lTask1 = new Task(WhoId=l.id, Status='Completed', Call_Result__c='Confirm Demo', 
                               POI_Status__c='Working - Connected', POI_Unable_To_Connect__c=false);
        test.startTest();
        System.runAs(owner) {
            insert new list<Task>{c1Task1,c2task1,lTask1};
                }
        test.stopTest(); 
    }
    
    static testmethod void test_taskStatus_ergo() {
        User owner = [SELECT id FROM User WHERE isActive = true AND Suite_Of_Interest__c = 'Ergonomics' LIMIT 1];
        Account a = testAccount();
        insert a;
        Contact c1 = testContact(a.id);
        c1.POI_Status_ERGO__c = 'Working - Connected';
        insert c1;
        Lead l = testLead();
        insert l;
        Task c1Task1 = new Task(WhoId=c1.id, Status='Completed', Call_Result__c='Call - No Result', 
                                POI_Status__c='Working - Pending Response', POI_Unable_To_Connect__c=true);
        
        Task c2Task1 = new Task(WhoId=c1.id, Status='Completed', Call_Result__c='Call - No Result', 
                                POI_Status__c='Open', POI_Unable_To_Connect__c=true);
        
        Task lTask1 = new Task(WhoId=l.id, Status='Completed', Call_Result__c='Confirm Demo', 
                               POI_Status__c='Working - Connected', POI_Unable_To_Connect__c=false);
        test.startTest();
        System.runAs(owner) {
            insert new list<Task>{c1Task1,c2task1,lTask1};
                }
        test.stopTest(); 
    }
    
    static testmethod void test_taskStatus_none() {
        User owner = [SELECT id FROM User WHERE isActive = true AND Suite_Of_Interest__c = 'MSDS Management' LIMIT 1];
        Account a = testAccount();
        insert a;
        Contact c1 = testContact(a.id);
        c1.POI_Status_MSDS__c = 'Open';
        insert c1;
        Task c1Task1 = new Task(WhoId=c1.id, Status='In Progress', Call_Result__c='Confirm Demo', 
                                POI_Status__c='Working - Pending Response', POI_Unable_To_Connect__c=true);
        
        test.startTest();
        System.runAs(owner) {
            insert c1Task1;
        }
        test.stopTest(); 
    }
    
    static testmethod void test_accountStatus() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        insert c;
        a.Customer_Status__c = 'Out of Business';
        test.startTest();
        update a;
        test.stopTest();
    }
    
    static testmethod void test_resetStatus() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        insert c;
        Lead l = testLead();
        insert l;
        Product_Of_Interest__c[] pois = new list<Product_Of_Interest__c>();
        Product_Of_Interest__c poi1 = new Product_Of_Interest__c(Contact__c=c.id);
        Product_Of_Interest__c poi2 = new Product_Of_Interest__c(Lead__c=l.id);
        pois.add(poi1); pois.add(poi2);
        insert pois;
        Product_Of_Interest_Action__c[] actions = new list<Product_Of_Interest_Action__c>();
        Product_Of_Interest_Action__c msds1 = new Product_Of_Interest_Action__c(Action_ID__c='msds1', Product__c='HQ', Action__c='Request Demo', Product_Suite__c='MSDS Management',
                                                                                Product_Of_Interest__c=poi1.id, Contact__c=c.id);
        Product_Of_Interest_Action__c msds2 = new Product_Of_Interest_Action__c(Action_ID__c='msds2', Product__c='HQ', Action__c='Request Demo', Product_Suite__c='MSDS Management',
                                                                                Product_Of_Interest__c=poi1.id, Lead__c=l.id);
        Product_Of_Interest_Action__c auth1 = new Product_Of_Interest_Action__c(Action_ID__c='auth1', Product__c='HQ', Action__c='Request Demo', Product_Suite__c='MSDS Authoring',
                                                                                Product_Of_Interest__c=poi1.id, Contact__c=c.id);
        Product_Of_Interest_Action__c auth2 = new Product_Of_Interest_Action__c(Action_ID__c='auth2', Product__c='HQ', Action__c='Request Demo', Product_Suite__c='MSDS Authoring',
                                                                                Product_Of_Interest__c=poi1.id, Lead__c=l.id);
        Product_Of_Interest_Action__c odt1 = new Product_Of_Interest_Action__c(Action_ID__c='odt1', Product__c='HQ', Action__c='Request Demo', Product_Suite__c='On-Demand Training',
                                                                               Product_Of_Interest__c=poi1.id, Contact__c=c.id);
        Product_Of_Interest_Action__c odt2 = new Product_Of_Interest_Action__c(Action_ID__c='odt2', Product__c='HQ', Action__c='Request Demo', Product_Suite__c='On-Demand Training',
                                                                               Product_Of_Interest__c=poi1.id, Lead__c=l.id);
        Product_Of_Interest_Action__c ergo1 = new Product_Of_Interest_Action__c(Action_ID__c='ergo1', Product__c='HQ', Action__c='Request Demo', Product_Suite__c='Ergonomics',
                                                                                Product_Of_Interest__c=poi1.id, Contact__c=c.id);
        Product_Of_Interest_Action__c ergo2 = new Product_Of_Interest_Action__c(Action_ID__c='ergo2', Product__c='HQ', Action__c='Request Demo', Product_Suite__c='Ergonomics',
                                                                                Product_Of_Interest__c=poi1.id, Lead__c=l.id);
        Product_Of_Interest_Action__c ehs1 = new Product_Of_Interest_Action__c(Action_ID__c='ehs1', Product__c='HQ', Action__c='Request Demo', Product_Suite__c='EHS Management',
                                                                               Product_Of_Interest__c=poi2.id, Contact__c=c.id);
        Product_Of_Interest_Action__c ehs2 = new Product_Of_Interest_Action__c(Action_ID__c='ehs2', Product__c='HQ', Action__c='Request Demo', Product_Suite__c='EHS Management',
                                                                               Product_Of_Interest__c=poi2.id, Lead__c=l.id);
        actions.addAll(new list<Product_Of_Interest_Action__c>{msds1,msds2,ehs1,ehs2,auth1,auth2,odt1,odt2,ergo1,ergo2});
        test.startTest();
        poiStageStatusUtility.resetStatus(actions);
        test.stopTest();
        
    }
    
    static testmethod void test_updateStageFromStatus_workingPendingResponse() {
        //Setup the test data
        Account newAccount = testAccount();
        insert newAccount;
        Contact newContact = testContact(newAccount.id);
        newContact.POI_Stage_MSDS__c = 'Neutral Interest';
        newContact.POI_Stage_EHS__c = 'Neutral Interest';
        newContact.POI_Stage_AUTH__c = 'Neutral Interest';
        newContact.POI_Stage_ODT__c = 'Neutral Interest';
        newContact.POI_Stage_ERGO__c = 'Neutral Interest';
        insert newContact;
        test.startTest();
        newContact.POI_Status_MSDS__c = 'Working - Pending Response';
        newContact.POI_Status_EHS__c = 'Working - Pending Response';
        newContact.POI_Status_AUTH__c = 'Working - Pending Response';
        newContact.POI_Status_ODT__c = 'Working - Pending Response';
        newContact.POI_Status_ERGO__c = 'Working - Pending Response';
        update newContact;
        test.stopTest();
    }
    
    static testmethod void test_updateStageFromStatus_workingConnected() {
        //Setup the test data
        Account newAccount = testAccount();
        insert newAccount;
        Contact newContact = testContact(newAccount.id);
        newContact.POI_Stage_MSDS__c = 'Neutral Interest';
        newContact.POI_Stage_EHS__c = 'Neutral Interest';
        newContact.POI_Stage_AUTH__c = 'Neutral Interest';
        newContact.POI_Stage_ODT__c = 'Neutral Interest';
        newContact.POI_Stage_ERGO__c = 'Neutral Interest';
        insert newContact;
        test.startTest();
        newContact.POI_Status_MSDS__c = 'Working - Connected';
        newContact.POI_Status_EHS__c = 'Working - Connected';
        newContact.POI_Status_AUTH__c = 'Working - Connected';
        newContact.POI_Status_ODT__c = 'Working - Connected';
        newContact.POI_Status_ERGO__c = 'Working - Connected';
        update newContact;
        test.stopTest();
    }
    
    static testmethod void test_updateStageFromStatus_salesInactiveStage() {
        //Setup the test data
        Account newAccount = testAccount();
        insert newAccount;
        Contact newContact = testContact(newAccount.id);
        newContact.POI_Stage_MSDS__c = 'Neutral Interest';
        newContact.POI_Stage_EHS__c = 'Neutral Interest';
        newContact.POI_Stage_AUTH__c = 'Neutral Interest';
        newContact.POI_Stage_ODT__c = 'Neutral Interest';
        newContact.POI_Stage_ERGO__c = 'Neutral Interest';
        insert newContact;
        Lead l = testLead();
        insert l;
        test.startTest();
        l.POI_Status_MSDS__c = 'Resolved - Poor Title / No Influence';
        newContact.POI_Status_MSDS__c = 'Resolved - Poor Title / No Influence';
        newContact.POI_Status_EHS__c = 'Resolved - Poor Title / No Influence';
        newContact.POI_Status_AUTH__c = 'Resolved - Poor Title / No Influence';
        newContact.POI_Status_ODT__c = 'Resolved - Poor Title / No Influence';
        newContact.POI_Status_ERGO__c = 'Resolved - Poor Title / No Influence';
        update newContact;
        update l;
        test.stopTest();
    }
    
    static testmethod void test_updateStageFromActiveProducts() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);        
        c.LDR_Contact__c = true;
        c.POI_Stage_MSDS__c = 'Neutral Interest';
        c.POI_Stage_EHS__c = 'Neutral Interest';
        c.POI_Stage_AUTH__c = 'Neutral Interest';
        c.POI_Stage_ODT__c = 'Neutral Interest';
        c.POI_Stage_ERGO__c = 'Neutral Interest';
        insert c;
        test.startTest();
        a.Active_MSDS_Management_Products__c = 'HQ';
        a.EHS_Active_Licensing__c = 'Safety Meetings';
        a.Active_Products__c = 'MSDS Authoring;On-Demand Training';
        a.Ergo_Active_Licensing__c = 'Ergonomics Licensing';
        update a;
        a.Active_MSDS_Management_Products__c = null;
        a.EHS_Active_Licensing__c = null;
        a.Active_Products__c = null;
        a.Ergo_Active_Licensing__c = null;
        a.Customer_Status__c = 'Out of Business';
        update a; 
        test.stopTest();
    }
    
    static testmethod void test_newContactPOIStage_msds() {
        //The stage set is dependent on the user. We need to query various sales users to run as
        User u = [SELECT id FROM User WHERE Suite_Of_Interest__c = 'MSDS Management' AND isActive = true LIMIT 1];
        //Setup the test data
        Account newAccount = testAccount();
        insert newAccount;
        Contact newContact = testContact(newAccount.id);
        Contact newContact2 = testContact(newAccount.id);
        test.startTest();
        System.runAs(u) {
            insert newContact;
            newContact.POI_Stage_MSDS__c = 'Client';
            update newContact;
            insert newContact2;
        }
        test.stopTest();
    }
    
    static testmethod void test_newContactPOIStage_ehs() {
        //The stage set is dependent on the user. We need to query various sales users to run as
        User u = [SELECT id FROM User WHERE Suite_Of_Interest__c = 'EHS Management' AND isActive = true LIMIT 1];
        //Setup the test data
        Account newAccount = testAccount();
        insert newAccount;
        Contact newContact = testContact(newAccount.id);
        Contact newContact2 = testContact(newAccount.id);
        test.startTest();
        System.runAs(u) {
            insert newContact;
            newContact.POI_Stage_EHS__c = 'Client';
            update newContact;
            insert newContact2;
        }
        test.stopTest();
    }
    
    static testmethod void test_newContactPOIStage_auth() {
        //The stage set is dependent on the user. We need to query various sales users to run as
        User u = [SELECT id FROM User WHERE Suite_Of_Interest__c = 'MSDS Authoring' AND isActive = true LIMIT 1];
        //Setup the test data
        Account newAccount = testAccount();
        newAccount.Active_Products__c = 'HQ';
        insert newAccount;
        Contact newContact = testContact(newAccount.id);
        Contact newContact2 = testContact(newAccount.id);
        test.startTest();
        System.runAs(u) {
            insert newContact;
            newContact.POI_Stage_AUTH__c = 'Client';
            update newContact;
            insert newContact2;
        }
        test.stopTest();
    }
    
    static testmethod void test_newContactPOIStage_odt() {
        //The stage set is dependent on the user. We need to query various sales users to run as
        User u = [SELECT id FROM User WHERE Suite_Of_Interest__c = 'On-Demand Training' AND isActive = true LIMIT 1];
        //Setup the test data
        Account newAccount = testAccount();
        newAccount.Active_Products__c = 'HQ';
        insert newAccount;
        Contact newContact = testContact(newAccount.id);
        Contact newContact2 = testContact(newAccount.id);
        test.startTest();
        System.runAs(u) {
            insert newContact;
            newContact.POI_Stage_ODT__c = 'Client';
            update newContact;
            insert newContact2;
        }
        test.stopTest();
    }
    
    static testmethod void test_newContactPOIStage_ergo() {
        //The stage set is dependent on the user. We need to query various sales users to run as
        User u = [SELECT id FROM User WHERE Suite_Of_Interest__c = 'Ergonomics' AND isActive = true LIMIT 1];
        //Setup the test data
        Account newAccount = testAccount();
        insert newAccount;
        Contact newContact = testContact(newAccount.id);
        Contact newContact2 = testContact(newAccount.id);
        test.startTest();
        System.runAs(u) {
            insert newContact;
            newContact.POI_Stage_ERGO__c = 'Client';
            update newContact;
            insert newContact2;
        }
        test.stopTest();
    }
    
    static testmethod void test_newContactPOIStage_nosuite() {
        //Setup the test data 
        Account newAccount = testAccount();
        newAccount.Active_MSDS_Management_Products__c = 'HQ';
        newAccount.EHS_Active_Licensing__c = 'Safety Meetings';
        newAccount.Active_Products__c = 'On-Demand Training;MSDS Authoring';
        newAccount.Ergo_Active_Licensing__c = 'Ergonomics Licensing'; 
        insert newAccount;
        Contact newContact = testContact(newAccount.id);
        test.startTest();
        insert newContact;
        newContact.POI_Stage_ERGO__c = 'Client';
        test.stopTest();
    }
    
    static testmethod void test_newOpportunityPOIStage_noProds() {
        Account a = testAccount();
        insert a;
        Contact c = testContact(a.id);
        insert c;
        Opportunity[] opps = new List<Opportunity>();
        for (integer i=0; i<5; i++) {
            Opportunity opp = testOpportunity(a.id, c.id);
            if (i==0) {opp.Suite_Of_Interest__c = 'MSDS Management';}
            if (i==1) {opp.Suite_Of_Interest__c = 'EHS Management';}
            if (i==2) {opp.Suite_Of_Interest__c = 'MSDS Authoring';}
            if (i==3) {opp.Suite_Of_Interest__c = 'On-Demand Training';}
            if (i==4) {opp.Suite_Of_Interest__c = 'Ergonomics';}
            opps.add(opp);
        }
        test.startTest();
        insert opps;
        test.stopTest();
    }
    
    static testmethod void test_newOpportunityPOIStage_wProds() {
        Account[] accounts = new list<Account>();
        Account a1 = testAccount();
        a1.Active_MSDS_Management_Products__c = 'HQ';
        a1.Active_Products__c = 'Something';
        a1.EHS_Active_Licensing__c = 'Safety Meetings';
        a1.Ergo_Active_Licensing__c = 'Ergonomics Licensing';
        Account a2 = testAccount();
        accounts.addAll(new list<Account>{a1, a2});
        insert accounts; 
        
        Contact[] contacts = new list<Contact>();
        
        Contact c1 = testContact(a1.id);
        c1.POI_Stage_MSDS__c = 'Client';
        c1.POI_Stage_EHS__c = 'Client';
        c1.POI_Stage_AUTH__c = 'Client';
        c1.POI_Stage_ODT__c = 'Client';
        c1.POI_Stage_ERGO__c = 'Client';
        Contact c2 = testContact(a2.id);
        c2.POI_Stage_MSDS__c = 'Sales Accepted';
        c2.POI_Stage_EHS__c = 'Sales Accepted';
        c2.POI_Stage_AUTH__c = 'Sales Accepted';
        c2.POI_Stage_ODT__c = 'Sales Accepted';
        c2.POI_Stage_ERGO__c = 'Sales Accepted';
        
        contacts.addAll(new list<Contact>{c1, c2});
        insert contacts;
        
        Opportunity[] opps = new List<Opportunity>();
        
        Opportunity msds1 = testOpportunity(a1.id, c1.id);
        msds1.Suite_Of_Interest__c = 'MSDS Management';
        Opportunity msds2 = testOpportunity(a2.id, c2.id);
        msds2.Suite_Of_Interest__c = 'MSDS Management';
        
        Opportunity ehs1 = testOpportunity(a1.id, c1.id);
        ehs1.Suite_Of_Interest__c = 'EHS Management';
        Opportunity ehs2 = testOpportunity(a2.id, c2.id);
        ehs2.Suite_Of_Interest__c = 'EHS Management';
        
        Opportunity auth1 = testOpportunity(a1.id, c1.id);
        auth1.Suite_Of_Interest__c = 'MSDS Authoring';
        Opportunity auth2 = testOpportunity(a2.id, c2.id);
        auth2.Suite_Of_Interest__c = 'MSDS Authoring';
        
        Opportunity odt1 = testOpportunity(a1.id, c1.id);
        odt1.Suite_Of_Interest__c = 'On-Demand Training';
        Opportunity odt2 = testOpportunity(a2.id, c2.id);
        odt2.Suite_Of_Interest__c = 'On-Demand Training';
        
        Opportunity ergo1 = testOpportunity(a1.id, c1.id);
        ergo1.Suite_Of_Interest__c = 'Ergonomics';
        Opportunity ergo2 = testOpportunity(a2.id, c2.id);
        ergo2.Suite_Of_Interest__c = 'Ergonomics';
        
        opps.addAll(new list<Opportunity>{msds1, msds2, ehs1, ehs2, auth1, auth2, odt1, odt2, ergo1, ergo2}); 
        insert opps;
        
        test.startTest();
        poiStageStatusUtility.newStageClient(opps);
        test.stopTest();
    }
    
    static testmethod void test_newStageCient_msds() {
        Account a = testAccount();
        a.Active_MSDS_Management_Products__c = 'HQ';
        insert a;
        Contact c = testContact(a.id);
        insert c;
        
        Opportunity[] opps = new list<Opportunity>();
        
        Opportunity renewal = testOpportunity(a.id, c.id);
        renewal.Opportunity_Type__c = 'Renewal';
        renewal.Suite_Of_Interest__c = 'MSDS Management';
        opps.add(renewal);
        
        Opportunity msds1 = testOpportunity(a.id, c.id);
        msds1.Suite_Of_Interest__c = 'MSDS Management';
        msds1.ContactID__c = c.id;
        msds1.Opportunity_Type__c = 'New';
        //opps.add(msds1);
        
        Opportunity msds2 = testOpportunity(a.id, c.id);
        msds2.Suite_Of_Interest__c = 'MSDS Management';
        msds2.ContactID__c = c.id;
        msds2.Opportunity_Type__c = 'New';
        msds2.StageName = 'Closed/Won';
        opps.add(msds2);
        
        insert opps;
        
        test.startTest();
        poiStageStatusUtility.newStageClient(new list<Opportunity>{msds2});
        test.stopTest();
    }
    
    static testmethod void test_newStageCient_ehs() {
        Account a = testAccount();
        a.EHS_Active_Licensing__c = 'Safety Meetings';      
        insert a;
        Contact c = testContact(a.id);
        c.POI_Stage_EHS__c = 'Client';
        insert c;
        
        Opportunity ehs2 = testOpportunity(a.id, c.id);
        ehs2.Suite_Of_Interest__c = 'EHS Management';
        ehs2.ContactID__c = c.id;
        ehs2.Opportunity_Type__c = 'New';
        ehs2.StageName = 'System Demo';
        insert ehs2;
        
        test.startTest();
        ehs2.StageName = 'Closed/Won';
        update ehs2;
        poiStageStatusUtility.newStageClient(new list<Opportunity>{ehs2});
        test.stopTest();
    }
    
    static testmethod void test_newStageCient_auth() {
        Account a = testAccount();
        a.Active_Products__c = 'MSDS Authoring';      
        insert a;
        Contact c = testContact(a.id);
        c.POI_Stage_AUTH__c = 'Client';
        insert c;
        
        Opportunity auth = testOpportunity(a.id, c.id);
        auth.Suite_Of_Interest__c = 'MSDS Authoring';
        auth.ContactID__c = c.id;
        auth.Opportunity_Type__c = 'New';
        auth.StageName = 'System Demo';
        insert auth;
        
        test.startTest();
        auth.StageName = 'Closed/Won';
        update auth;
        poiStageStatusUtility.newStageClient(new list<Opportunity>{auth});
        test.stopTest();
    }
    
    static testmethod void test_newStageCient_odt() {
        Account a = testAccount();
        a.Active_Products__c = 'On-Demand Training';      
        insert a;
        Contact c = testContact(a.id);
        c.POI_Stage_ODT__c = 'Client';
        insert c;
        
        Opportunity odt = testOpportunity(a.id, c.id);
        odt.Suite_Of_Interest__c = 'On-Demand Training';
        odt.ContactID__c = c.id;
        odt.Opportunity_Type__c = 'New';
        odt.StageName = 'System Demo';
        insert odt;
        
        test.startTest();
        odt.StageName = 'Closed/Won';
        update odt;
        poiStageStatusUtility.newStageClient(new list<Opportunity>{odt});
        test.stopTest();
    }
    
    static testmethod void test_newStageCient_ergo() {
        Account a = testAccount();
        a.Ergo_Active_Licensing__c = 'Ergonomics Licensing';      
        insert a;
        Contact c = testContact(a.id);
        c.POI_Stage_ERGO__c = 'Client';
        insert c;
        
        Opportunity ergo = testOpportunity(a.id, c.id);
        ergo.Suite_Of_Interest__c = 'Ergonomics';
        ergo.ContactID__c = c.id;
        ergo.Opportunity_Type__c = 'New';
        ergo.StageName = 'System Demo';
        insert ergo;
        
        test.startTest();
        ergo.StageName = 'Closed/Won';
        update ergo;
        poiStageStatusUtility.newStageClient(new list<Opportunity>{ergo});
        test.stopTest();
    }
    
    static testmethod void test_renewalOpportunityPOIStage() {
        Account a = testAccount();
        a.Active_MSDS_Management_Products__c = 'HQ';
        insert a;
        Contact c = testContact(a.id);
        insert c;
        
        Opportunity renewal = testOpportunity(a.id, c.id);
        renewal.Opportunity_Type__c = 'Renewal';
        renewal.Suite_Of_Interest__c = 'MSDS Management';
        insert renewal;
        
        c.POI_Stage_MSDS__c = 'Client';
        update c;
        
        
        test.startTest();
        poiStageStatusUtility.renewalOpportunityPOIStage(new list<Opportunity>{renewal});
        renewal.StageName = 'Closed Lost';
        update renewal;
        poiStageStatusUtility.renewalStageClient(new list<Opportunity>{renewal});
        test.stopTest();
    }
    
    static testmethod void test_accountContactsStageUpdate() {
        Account a = testAccount();
        insert a;
        Contact[] contacts = new list<Contact>();
        Contact c1 = testContact(a.id);
        c1.POI_Stage_MSDS__c = 'Neutral Interest';
        c1.POI_Stage_EHS__c = 'Neutral Interest';
        c1.POI_Stage_AUTH__c = 'Neutral Interest';
        c1.POI_Stage_ODT__c = 'Neutral Interest';
        c1.POI_Stage_ERGO__c = 'Neutral Interest';
        contacts.add(c1);
        Contact c2 = testContact(a.id);
        c2.POI_Stage_MSDS__c = 'Neutral Interest';
        c2.POI_Stage_EHS__c = 'Neutral Interest';
        c2.POI_Stage_AUTH__c = 'Neutral Interest';
        c2.POI_Stage_ODT__c = 'Neutral Interest';
        c2.POI_Stage_ERGO__c = 'Neutral Interest';
        contacts.add(c2);
        insert contacts;
        
        test.startTest();
        c1.POI_Stage_MSDS__c = 'Client';
        c1.POI_Stage_EHS__c = 'Client';
        c1.POI_Stage_AUTH__c = 'Client';
        c1.POI_Stage_ODT__c = 'Client';
        c1.POI_Stage_ERGO__c = 'Client';
        update c1;
        test.stopTest();
    }
    
    public static Opportunity testOpportunity(id accountId, id contactId) {
        return new Opportunity(Name='Test', StageName='System Demo', CloseDate=date.today(), AccountId=accountId, ContactID__c=contactId);
    }
    
    public static Account testAccount() {
        return new Account(Name='Test Account', NAICS_Code__c='Test');
    }
    
    public static Contact testContact(id accountId) {
        return new Contact(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', AccountId=accountId);
    }
    
    public static Lead testLead() {
        return new Lead(FirstName='Test', LastName='Test', Company='Company1', Communication_Channel__c='Inbound Call');
    }
}