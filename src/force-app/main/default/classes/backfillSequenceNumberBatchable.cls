global class backfillSequenceNumberBatchable implements Database.Batchable<SObject>, Database.Stateful{

    
    global Database.QueryLocator start(Database.BatchableContext bc) {
         
            return Database.getQueryLocator([SELECT id,  
                                             (SELECT id,Sequence_Number__c 
                                                FROM Revenue_Recognition_Transactions__r ORDER BY CreatedDate ASC)
                                                FROM Revenue_Recognition__c
                                                WHERE of_Missing_Seq__c > 0]);

    }
    
    global void execute(Database.BatchableContext bc, list<SObject> batch) {
        Revenue_Recognition_Transaction__c[] rrtToUpdate = new List<Revenue_Recognition_Transaction__c>();
        
        for(Revenue_Recognition__c rr : (list<Revenue_Recognition__c>) batch){
            integer sequenceNum = 0;
            for(Revenue_Recognition_Transaction__c rrt: rr.Revenue_Recognition_Transactions__r){
                sequenceNum++;
                rrt.Sequence_Number__c = sequenceNum;
                rrtToUpdate.add(rrt);   
            }     
        } 
        
        try{
            update rrtToUpdate;   
        }
        catch(exception e) {
            salesforceLog.createLog('Revenue Recognition', true, 'backfillSequenceNumberBatchable', 'updateOldTransactionsSequenceNumber', string.valueOf(e));
        }

    }
    
    global void finish(Database.BatchableContext bc) {}
}