@isTest(seeAllData = true)
public class testContractTriggerHandler {
    static testMethod void test1(){
            
        Account a = new account();
        a.name = 'Macs Account';
        
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'Dustin';
        c.LastName = 'Johnson';
        c.AccountId = a.id;
        c.EHS_Contact__c = false;
        
        insert c;
        
        Opportunity o = new opportunity();
        o.Name = 'Macs Opp';
        o.AccountId = a.id;
        o.StageName = 'New';
        o.CloseDate = date.today()+1;
        insert o;
        
        
        product2 p = [select id, name, PricebookEntryId__c, Product_Platform__c from product2 where Product_Platform__c = 'EHS Management' AND name = 'air' limit 1];

        
        Quote__c q = new quote__c();
        q.Account__c = a.id;
        q.Contact__c = c.id;
        q.Opportunity__c = o.id;
        q.Status__c = 'Active';
        q.Contract_Length__c = '1 Year';
        
        insert q;
        
        
        contract__c contr = new contract__c();
        contr.Account__c = a.id;
        contr.Contact__c = c.id;
        contr.Opportunity__c = o.id;
        contr.Status__c = 'Approved';
        contr.Num_of_EHS_Management_Products__c = 0;
       
        
        insert contr;
        
        Contract_Line_Item__c cli = new Contract_Line_Item__c();        
        cli.Contract__c = contr.id;
        cli.Implementation_Model__c = 'Simple';
        cli.Subscription_Model__c = 'Business';
        cli.OpportunityId__c = o.id;
        cli.Product__c = p.id;
        cli.Name__c = p.Name;
       
       
        
        insert cli;
      
       Contract_Line_Item__c t = [select id, name, product_platform__c from Contract_Line_Item__c where id =: cli.id];
        
        Contract__c co = [Select id, name, num_of_ehs_management_products__c from contract__c where id =: contr.id]; 


        //Manually updating field because look up roll up dont run in test 
        contr.Num_of_EHS_Management_Products__c = 1;
        
        update contr;

      
    }
}