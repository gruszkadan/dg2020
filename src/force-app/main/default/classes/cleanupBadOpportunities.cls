global class cleanupBadOpportunities implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Task[] tasksToUpdate = new list<Task>();
        Opportunity[] o = [Select ID,AccountId, (Select id,WhatId from Tasks) from Opportunity where Opportunity_Type__c = '' and CreatedDate >= THIS_YEAR];
        if(o.size()>0){
            for(Opportunity opp:o){
                system.debug('size is '+opp.Tasks.size());
                if(opp.Tasks.size() > 0){
                    for(Task t:opp.Tasks){
                        t.WhatId = opp.AccountId;
                        tasksToUpdate.add(t);
                    }
                }
            }
            if(tasksToUpdate.size() > 0){
                try {
                    update tasksToUpdate;
                } catch(exception e) {
                    salesforceLog.createLog('Task', true, 'cleanupBadOpportunities', 'execute', string.valueOf(e));
                }                
            }
            try {
                delete o;
            } catch(exception e) {
                salesforceLog.createLog('Opportunity', true, 'cleanupBadOpportunities', 'execute', string.valueOf(e));
            }
        }
    }
}