public class outOfBusinessAccountScheduler implements Schedulable {
    public void execute(SchedulableContext ctx) {
        User u = [SELECT id, Validation_Rules_Active__c FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
        u.Validation_Rules_Active__c = FALSE;
        update u;
        outOfBusinessAccountReassignment oob = new outOfBusinessAccountReassignment();
        database.executebatch(oob,200);
    }
}