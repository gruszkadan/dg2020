/*
* This will create new summaries
* Test class: testSalesPerformanceSummaryScheduler
*/ 
global class salesPerformanceSummaryBatchableStep1 implements Database.Batchable<SObject>, Database.Stateful {
    
    integer yearOffset = 0;
    string stringYear = string.valueOf((date.today().year()) + yearOffset);
    integer numYear = date.today().year() + yearOffset;
    integer numRepsCreated = 0;
    integer numCreated = 0;
    string htmlBody = '';
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (!test.isRunningTest()) {
            return Database.getQueryLocator([SELECT id, Name, Role__c, Sales_Performance_Manager_ID__c, Sales_Manager__c, Sales_VP__c, Group__c, SIC_Group__c, Sales_Director__c
                                             FROM User
                                             WHERE ForecastEnabled = true
                                             ORDER BY Role_Order__c ASC]);
        } else {
            return Database.getQueryLocator([SELECT id, Name, Role__c, Sales_Performance_Manager_ID__c, Sales_Manager__c, Sales_VP__c, Group__c, SIC_Group__c, Sales_Director__c
                                             FROM User
                                             WHERE ForecastEnabled = true
                                             AND Alias = 'TestREP' LIMIT 1]); 
        }
    }
    
    global void execute(Database.BatchableContext bc, list<sObject> batch) {
        
        Sales_Performance_Summary__c[] upsList = new list<Sales_Performance_Summary__c>();
        
        // Get the rep we're working with
        User rep = (User)batch[0];        
        boolean summaryCreated = false;
        
        // Query the reps quotas for the year
        ForecastingQuota[] quotas = new list<ForecastingQuota>();
        quotas.addAll([SELECT id, QuotaOwnerId, QuotaAmount, StartDate
                       FROM ForecastingQuota
                       WHERE QuotaOwnerId = : rep.id
                       AND CALENDAR_YEAR(StartDate) = : numYear
                       ORDER BY StartDate ASC]);
        
        if (quotas.size() == 0) {
            ForecastingQuota[] newQuotas = new list<ForecastingQuota>();
            integer nowMonth;
            if (!test.isRunningTest()) {
                nowMonth = integer.valueOf(date.today().month());
            } else {
                nowMonth = 1;
            }
            for (integer m = nowMonth; m<=12; m++) {
                ForecastingQuota q = new ForecastingQuota();
                string d = stringYear+'-';
                if (m < 10) {
                    d += '0'+m+'-01';
                } else {
                    d += m+'-01';
                }
                q.StartDate = date.valueOf(d);
                q.QuotaOwnerId = rep.id;
                q.ForecastingTypeId = '0Db800000004hFWCAY';
                q.QuotaAmount = 0;
                newQuotas.add(q);
            }
            if (!test.isRunningTest()) {
                insert newQuotas;
            }
            quotas.addAll(newQuotas);
        }
        
        // Query the summaries for the year
        Sales_Performance_Summary__c[] mySummaries = new list<Sales_Performance_Summary__c>();
        mySummaries.addAll([SELECT id, Owner.Name, OwnerId, Manager__c, Manager_Sales_Performance_Summary__c, Month__c, Month_Number__c, Quota__c, Role__c, Sales_Team_Manager__c, Year__c, Group__c
                            FROM Sales_Performance_Summary__c 
                            WHERE OwnerId = : rep.Id
                            AND Year__c = : stringYear
                            ORDER BY Month_Number__c ASC]);
        
        // If the rep has a sales performance manager, query his/her summaries as well
        Sales_Performance_Summary__c[] mgrSummaries = new list<Sales_Performance_Summary__c>();
        if (rep.Sales_Performance_Manager_ID__c != null) {
            mgrSummaries.addAll([SELECT id, OwnerId, Month__c, Month_Number__c, Year__c
                                 FROM Sales_Performance_Summary__c 
                                 WHERE OwnerId = : rep.Sales_Performance_Manager_ID__c
                                 AND Year__c = : stringYear
                                 ORDER BY Month_Number__c ASC]);
        }
        
        decimal ytdQuota = 0.00;
        
        // Loop through each month and match the quota and summary to that month
        for (integer m=1; m<=12; m++) {
            ForecastingQuota quota;
            Sales_Performance_Summary__c mySummary = new Sales_Performance_Summary__c();
            Sales_Performance_Summary__c mgrSummary;
            
            // Loop through our quotas
            if (quotas.size() > 0) {
                for (ForecastingQuota q : quotas) {
                    if (q.StartDate.month() == m) {
                        quota = q;
                        ytdQuota = ytdQuota + quota.QuotaAmount;
                    }
                }
            } 
            
            // If a quota is found, create/update the summary
            if (quota != null) {
                
                // Loop through our summaries and match the rep's summary
                if (mySummaries.size() > 0) {
                    for (Sales_Performance_Summary__c s : mySummaries) {
                        if (s.Month_Number__c == m) {
                            mySummary = s;
                        }
                    }
                }
                
                // Loop through the manager's summaries
                if (mgrSummaries.size() > 0) {
                    for (Sales_Performance_Summary__c s : mgrSummaries) {
                        if (s.Month_Number__c == m) {
                            mgrSummary = s;
                        }
                    }
                }
                
                // If there is a summary, we need to double check the QuotaAmount matches. If not, update it
                if (mySummary.id == null) {
                    // If there isn't a summary, we need to create one
                    mySummary.OwnerId = rep.id;
                    mySummary.Sales_Rep__c = rep.id;
                    mySummary.Manager__c = rep.Sales_Performance_Manager_ID__c;
                    if (mgrSummary != null) {
                        mySummary.Manager_Sales_Performance_Summary__c = mgrSummary.id;
                    }
                    mySummary.Month__c = findMonth(m);
                    mySummary.Quota__c = quota.QuotaAmount;
                    mySummary.Role__c = rep.Role__c;
                    mySummary.Group__c = rep.Group__c;
                    mySummary.Sales_Director__c = rep.Sales_Director__c;
                    mySummary.Sales_VP__c = rep.Sales_VP__c;
                    if (rep.Role__c != 'Sales Manager' && rep.Role__c != 'Director' && rep.Role__c != 'VP') {
                        mySummary.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
                    } else {
                        mySummary.Sales_Team_Manager__c = rep.id;
                    }
                    mySummary.Year__c = stringYear;
                    mySummary.YTD_Quota__c = ytdQuota;
                    if (rep.SIC_Group__c != null) {
                        mySummary.SIC_Group__c = integer.valueOf(rep.SIC_Group__c);
                    }
                    upsList.add(mySummary);
                    numCreated++;
                    summaryCreated = true;
                }
            }
        }
       
        if (summaryCreated) {
            numRepsCreated++;
        }
      
        if (upsList.size() > 0) {
            try {
                upsert upsList;
            } catch(exception e) {
                salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceSummaryBatchable', 'execute', string.valueOf(e));
            }
        }
        
    }
    
    global void finish(Database.BatchableContext bc) {
         // Run step2 - this will update summaries if a role or team is changed
        salesPerformanceSummaryBatchableStep2 step2 = new salesPerformanceSummaryBatchableStep2();
        Database.executeBatch(step2, 1);
    }
    
    global string findMonth(integer m) {
        string month = '';
        if (m == 1) {
            month = 'January';
        } else if (m == 2) {
            month = 'February';
        } else if (m == 3) {
            month = 'March';
        } else if (m == 4) {
            month = 'April';
        } else if (m == 5) {
            month = 'May';
        } else if (m == 6) {
            month = 'June';
        } else if (m == 7) {
            month = 'July';
        } else if (m == 8) {
            month = 'August';
        } else if (m == 9) {
            month = 'September';
        } else if (m == 10) {
            month = 'October';
        } else if (m == 11) {
            month = 'November';
        } else if (m == 12) {
            month = 'December';
        }
        return month;
    }
    
}