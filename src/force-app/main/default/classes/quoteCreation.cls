public with sharing class quoteCreation {
    public Account currentAccount {get;set;}
    public Contact currentContact {get;set;}
    public Opportunity theOpportunity {get;set;}
    public CampaignMember[] campaign {get;set;}
    public CampaignMember primaryCampaign {get;set;}
    public List<Quote__c> currentQuotes {get; set;}
    public String quoteIdChosen {get; set;}
    public Quote__c[] openQuotes {get; set;}
    public ID contactID {get;set;}
    public ID accountID {get;set;}
    public ID opportunityID {get;set;}
    
    public quoteCreation(ApexPages.StandardController stdController) {
        this.quote = (Quote__c)stdController.getRecord();
        Id accountID= System.currentPageReference().getParameters().get('CF00N800000055Rbg_lkid');
        Id contactID= System.currentPageReference().getParameters().get('CF00N800000055Rbj_lkid');   
        opportunityID = System.currentPageReference().getParameters().get('opportunityID');
        currentAccount = [Select ID, Name from Account where ID =:accountID];
        openQuotes = [SELECT ID, Name, Count_of_Employees__c, Y1_Total__c, Num_of_Line_Items__c, CreatedDate, Status__c, Currency__c, Currency_Rate__c
                      from Quote__c where Status__c ='Active' AND Account__c=:currentAccount.ID];
        if(opportunityID !=NULL){
            theOpportunity = [select ID, Name, Account_Management_Event__c from Opportunity where ID=:opportunityID];
        }
        setupQuotes();   
        createQuote();
    }
    
    private void setupQuotes(){
        currentQuotes = [SELECT ID, Name, Count_of_Employees__c, Y1_Total__c, Num_of_Line_Items__c, CreatedDate, Status__c, Currency__c, Currency_Rate__c
                         from Quote__c where Status__c='Active' and Account__c=:currentAccount.ID 
                        ORDER BY CreatedDate DESC];
    }    
    
    public PageReference archiveQuote(){
        Quote__c toArchive=new Quote__c(id=quoteIdChosen,status__c = 'Archived');
        update toArchive;
        setupQuotes();
        return createQuote();
    }
    
    Account account;
    Contact contact;
    Quote__c quote;
    
    public Account getAccount(){
        if(account == null) account = new Account();
        return account;
    }
    
    public Contact getContact(){
        if(contact == null) contact = new Contact();
        return contact;
    }
    
    public Quote__c getQuote(){
        if(quote == null) quote = new Quote__c();
        return quote;
    }
    
    public PageReference productEntry(){
        insert quote;
        PageReference productEntry = Page.quoteProductEntry;
        productEntry.setRedirect(true);
        productEntry.getParameters().put('id',quote.id); 
        return  productEntry;
    }
    
    public PageReference skipArchiving(){
        PageReference skipArchiving;
        skipArchiving = Page.quoteCreate;
        skipArchiving.getParameters().put('CF00N800000055Rbg_lkid',currentAccount.ID);
        skipArchiving.getParameters().put('CF00N800000055Rbg',currentAccount.Name);
        if(opportunityID !=NULL){            
            skipArchiving.getParameters().put('CF00N800000055Rcb_lkid', theOpportunity.ID);
            skipArchiving.getParameters().put('CF00N800000055Rcb', theOpportunity.Name);
            if(theOpportunity.Account_Management_Event__c != NULL){
                skipArchiving.getParameters().put('ameId', theOpportunity.Account_Management_Event__c);
            }
        }
        if(contactID !=NULL){
            skipArchiving.getParameters().put('CF00N800000055Rbj_lkid',contactID);
        } 
        skipArchiving.getParameters().put('scontrolCaching', '1');
        skipArchiving.getParameters().put('sfdc.override', '1');
        return skipArchiving.setRedirect(true);    
    }
    
    public PageReference createQuote(){
        PageReference createQuote;
        if(currentQuotes.size()== 0){
            createQuote = Page.quoteCreate;
            createQuote.getParameters().put('CF00N800000055Rbg_lkid',currentAccount.ID);
            createQuote.getParameters().put('CF00N800000055Rbg',currentAccount.Name);
            if(contactID !=NULL){
                createQuote.getParameters().put('CF00N800000055Rbj_lkid',contactID);
            }
            if(opportunityID !=NULL){                
                createQuote.getParameters().put('CF00N800000055Rcb_lkid', theOpportunity.ID);
                createQuote.getParameters().put('CF00N800000055Rcb', theOpportunity.Name);
                if(theOpportunity.Account_Management_Event__c != NULL){
                    createQuote.getParameters().put('ameId', theOpportunity.Account_Management_Event__c);
                }
            }
            createQuote.getParameters().put('scontrolCaching', '1');
            createQuote.getParameters().put('sfdc.override', '1');
        }else{
            return null;
        }
        return createQuote.setRedirect(true);
    }
}