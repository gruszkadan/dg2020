@isTest(SeeAllData=true)
private class testRenewalNewOpportunity {
    
    static testMethod void test1() {
        Date myDate = Date.newInstance(2011, 11, 18);
        Time myTime = Time.newInstance(3, 3, 3, 0);
        DateTime dt = DateTime.newInstance(myDate, myTime);
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        newContact.Lead_Source_Category__c = 'Cold Call';
        newContact.Lead_Source_Detail__c = 'Website - Live Chat';
        newContact.Lead_Source_Type__c ='Internet';
        newContact.mkto_si__Last_Interesting_Moment_Date__c = dt;
        insert newContact;
        
        Campaign newCampaign = new Campaign();
        newCampaign.Name = 'Unknown';
        newCampaign.Status = 'In Progress';
        newCampaign.IsActive = TRUE;
        insert newCampaign;
        
        CampaignMember cm = new CampaignMember();
        cm.contactid = newContact.id;
        cm.CampaignId = newCampaign.id;
        
        insert cm;
        
        User u = [Select ID from User where Department__c ='Sales' and Group__c ='Mid-Market' and isActive = true limit 1];

        System.runAs(u){
            ApexPages.currentPage().getParameters().put('accid', newAccount.Id);
            ApexPages.currentPage().getParameters().put('contactID', newContact.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(new Opportunity());
            renewal_new_opportunity myController = new renewal_new_opportunity(testController);
            
            mycontroller.getCampaigns();
            myController.saveOppty();
            Opportunity oppty = [SELECT id, OwnerId, Lead_Source_Category__c, Lead_Source_Detail__c, Last_Interesting_Moment_Date__c
                                 FROM Opportunity WHERE Accountid = :newAccount.id LIMIT 1];
            System.assertEquals ('Cold Call', oppty.Lead_Source_Category__c);
            System.assertEquals (dt, oppty.Last_Interesting_Moment_Date__c);
        }
    }
    
    static testMethod void test2() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Renewal__c newRenew = new Renewal__c();
        newRenew.Account__c = newAccount.ID;
        newRenew.Contact__c = newContact.Id;
        newRenew.Status__c = 'Active';
        insert newRenew;
        
        User u = [Select ID from User where Department__c ='Customer Care' and Group__c ='Retention' and isActive = true limit 1];
        Campaign c = [Select ID, Name from Campaign where Name='Unknown' limit 1];

        System.runAs(u){
            ApexPages.currentPage().getParameters().put('accid', newAccount.Id);
            ApexPages.currentPage().getParameters().put('contactID', newContact.Id);
            ApexPages.currentPage().getParameters().put('renewal', newRenew.Id);
            ApexPages.StandardController testController = new ApexPages.StandardController(new Opportunity());
            renewal_new_opportunity myController = new renewal_new_opportunity(testController);
            
            
            myController.o.StageName = 'New';
            myController.o.Name = 'Test';
            myController.o.CloseDate = system.today(); 
            myController.saveOppty();
        }
    }
}