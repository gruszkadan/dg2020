@isTest
private class testAccountHierarchy{

    static testMethod void testAccountHierarchy(){

        Account newAccount = new Account();
        newAccount.Name = 'HierarchyTest0';
        newAccount.Customer_Status__c ='Prospect';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        insert newAccount;
        
        Account newAccount1 = new Account();
        newAccount1.Name = 'HierarchyTest4';
        newAccount1.ParentID = newAccount.Id;
        newAccount1.Customer_Status__c ='Prospect';
        newAccount1.BillingPostalCode = '97031';
        newAccount1.OwnerID='00580000003UChI';
        newAccount1.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount1.Authoring_Account_Owner__c='00580000003UChI';
        insert newAccount1;
        
        Account newAccount2 = new Account();
        newAccount2.Name = 'HierarchyTest9';
        newAccount2.ParentID = newAccount.Id;
        newAccount2.Customer_Status__c ='Prospect';
        newAccount2.BillingPostalCode = '97031';
        newAccount2.OwnerID='00580000003UChI';
        newAccount2.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount2.Authoring_Account_Owner__c='00580000003UChI';
        insert newAccount2;
        
        Account newAccount3 = new Account();
        newAccount3.Name = 'HierarchyTest2';
        newAccount3.ParentID = newAccount.Id;
        newAccount3.Customer_Status__c ='Prospect';
        newAccount3.BillingPostalCode = '97031';
        newAccount3.OwnerID='00580000003UChI';
        newAccount3.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount3.Authoring_Account_Owner__c='00580000003UChI';
        newAccount3.AdminId__c='test123';
        insert newAccount3;
        
        Account newAccount4 = new Account();
        newAccount4.Name = 'HierarchyTest3';
        newAccount4.ParentID = newAccount.Id;
        newAccount4.Customer_Status__c ='Prospect';
        newAccount4.BillingPostalCode = '97031';
        newAccount4.OwnerID='00580000003UChI';
        newAccount4.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount4.Authoring_Account_Owner__c='00580000003UChI';
        insert newAccount4;
        
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Opportunity 1';
        Opp.AccountId = newAccount4.Id;
        Opp.StageName = 'New';
        Opp.CloseDate = date.today()+2;
        
        insert Opp;
        
        Order_Item__c[] oItems = new List<Order_Item__c>();
        integer orderItemIdNum = 1;
        for (integer i=0; i<8; i++) {
            Order_Item__c oi = new Order_Item__c();
            oi.Account__c = newAccount4.id;
            oi.Name = 'Test';
            oi.Invoice_Amount__c = 10;
            oi.Order_Date__c = date.today();
            oi.Order_ID__c = 'test123';
            oi.Order_Item_ID__c = 'test123'+string.valueOf(orderItemIdNum);
            oi.Year__c = string.valueOf(date.today().year()); 
            oi.Month__c = datetime.now().format('MMMMM');
            oi.Contract_Length__c = 3;
            oi.AccountID__c = 'test123';
            oi.Term_Start_Date__c = date.today();
            oi.Term_End_Date__c = date.today().addYears(1);
            oi.Term_Length__c = 1;
            oi.Contract_Number__c = 'test123';
            oi.Sales_Location__c = 'Chicago';
            oi.Admin_Tool_Order_Type__c = 'New';
            if (i == 0) {
                oi.Admin_Tool_Product_Name__c = 'HQ';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
            } if (i == 1) {
                oi.Admin_Tool_Product_Name__c = 'Data Management';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Admin_Tool_Product_Type__c = 'Data Import';
                oi.Product_Platform__c = 'EHS';
            } if (i == 2) {
                oi.Admin_Tool_Product_Name__c = 'Audit and Inspection';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'EHS';
            } if (i == 3) {
                oi.Admin_Tool_Product_Name__c = 'CS - Other';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
            } if (i == 4) {
                oi.Admin_Tool_Product_Name__c = 'Desktop Backup';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'MSDSonline';
            } if (i == 5) {
                oi.Admin_Tool_Product_Name__c = 'Ergo';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'Ergo';
            } if (i == 6) {
                oi.Admin_Tool_Product_Name__c = 'Ergonomics Consulting Services';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Product_Platform__c = 'Ergo';
            } if (i == 7) {
                oi.Admin_Tool_Product_Name__c = 'Internationalization';
                oi.Admin_Tool_Order_Status__c = 'A';
                oi.Admin_Tool_Product_Type__c = 'Language Translation (per word)';
                oi.Product_Platform__c = 'EHS';
            } 
            oItems.add(oi); 
            orderItemIdNum++;
        }
        insert oItems;
        
        Account topAccount = [ Select id, name from account where name = 'HierarchyTest0' limit 1 ];
        Account middleAccount = [ Select id, parentID, name from account where name = 'HierarchyTest4' limit 1 ];
        Account bottomAccount = [ Select id, parentID, name from account where name = 'HierarchyTest9' limit 1 ];
        Account[] accountList = [ Select id, parentID, name from account where name like 'HierarchyTest%' ];

        test.startTest();
        
        PageReference AccountHierarchyPage = Page.AccountHierarchyPage;
        Test.setCurrentPage( AccountHierarchyPage );
        ApexPages.currentPage().getParameters().put( 'id', topAccount.id );
    
        // Instanciate Controller
        AccountStructure controller = new AccountStructure();
        
        // Call Methodes for top account
        controller.setcurrentId( null );
        AccountStructure.ObjectStructureMap[] smt1 = new AccountStructure.ObjectStructureMap[]{};
        smt1 = controller.getObjectStructure();
        System.Assert( smt1.size() > 0, 'Test failed at Top account, no Id' );

        controller.setcurrentId( String.valueOf( topAccount.id ) );
        AccountStructure.ObjectStructureMap[] smt2 = new AccountStructure.ObjectStructureMap[]{};
        smt2 = controller.getObjectStructure();
        System.Assert( smt2.size() > 0, 'Test failed at Top account, with Id: '+smt2.size() );

        //Call ObjectStructureMap methodes
        smt2[0].setnodeId( '1234567890' );
        smt2[0].setlevelFlag( true );
        smt2[0].setlcloseFlag( false );
        smt2[0].setnodeType( 'parent' );
        smt2[0].setcurrentNode( false );
        smt2[0].setaccount( topAccount );
        
        String nodeId = smt2[0].getnodeId();
        Boolean[] levelFlag = smt2[0].getlevelFlag();
        Boolean[] closeFlag = smt2[0].getcloseFlag();
        String nodeType = smt2[0].getnodeType();
        Boolean currentName = smt2[0].getcurrentNode();
        Account smbAccount = smt2[0].getaccount();

        // Call Methodes for middle account
        controller.setcurrentId( String.valueOf( middleAccount.id ) );
        AccountStructure.ObjectStructureMap[] smm = new AccountStructure.ObjectStructureMap[]{};
        smm = controller.getObjectStructure();
        System.Assert( smm.size() > 0, 'Test failed at middle account' );

        // Call Methodes for bottom account
        controller.setcurrentId( String.valueOf( bottomAccount.id ) );
        AccountStructure.ObjectStructureMap[] smb = new AccountStructure.ObjectStructureMap[]{};
        smb = controller.getObjectStructure();
        System.Assert( smb.size() > 0, 'Test failed at top account' );
        
        test.stopTest();
    }
}