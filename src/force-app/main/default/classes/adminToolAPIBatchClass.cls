global class adminToolAPIBatchClass implements Database.Batchable<Order_Item__c> {
    Order_Item__c[] source;
    
    
    global adminToolAPIBatchClass(Order_Item__c[] source) {
        this.source = source;
    }
    
    global Iterable<Order_Item__c> start(Database.BatchableContext bc) {
        orderItemBatchFeeder oibf = new orderItemBatchFeeder(source);
        return oibf;
    }
    global void execute(Database.BatchableContext bc, Order_Item__c[] scope) {
        try {
            upsert scope Order_Item_ID__c;
        } catch(System.DMLexception e) {
            salesforceLog.createLog('Admin Tool API', TRUE, 'adminToolUtility', 'upsertOrderData', string.valueOf(e));
        }
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}