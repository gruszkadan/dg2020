@isTest
private class testAccountOrderItems {
    
    
    
    @isTest  static void testCreatingLists(){
        
        Account acct= new Account();
        acct.Name = 'Test Account';
        insert acct;
        
        Order_Item__c a = new Order_Item__c();
        a.account__c = acct.ID;
        a.Name = 'Test';
        a.Invoice_Amount__c = 10;
        a.Order_Date__c = date.today();
        a.Order_ID__c = 'test123';
        a.Order_Item_ID__c = 'test123';
        a.Year__c = string.valueOf(date.today().year()); 
        a.Month__c = datetime.now().format('MMMMM');
        a.Contract_Length__c = 3;
        a.AccountID__c = 'test123';
        a.Term_Start_Date__c = date.today();
        a.Term_End_Date__c = date.today().addYears(1);
        a.Term_Length__c = 1;
        a.Contract_Number__c = 'test123';
        a.Sales_Location__c = 'Chicago';
        a.Admin_Tool_Order_Type__c = 'New';
        a.Admin_Tool_Product_Name__c = 'HQ';
        a.Admin_Tool_Order_Status__c = 'A';
        a.Product_Platform__c = 'MSDSonline';
        
        insert a;
        

        Order_Item__c i = new Order_Item__c();
        i.account__c = acct.ID;
        i.Name = 'Test2';
        i.Invoice_Amount__c = 10;
        i.Order_Date__c = date.today();
        i.Order_ID__c = 'test1234';
        i.Order_Item_ID__c = 'test1234';
        i.Year__c = string.valueOf(date.today().year()); 
        i.Month__c = datetime.now().format('MMMMM');
        i.Contract_Length__c = 3;
        i.AccountID__c = 'test123';
        i.Term_Start_Date__c = date.today();
        i.Term_End_Date__c = date.today().addYears(1);
        i.Term_Length__c = 1;
        i.Contract_Number__c = 'test123';
        i.Sales_Location__c = 'Chicago';
        i.Admin_Tool_Order_Type__c = 'New';
        i.Admin_Tool_Product_Name__c = 'HQ';
        i.Admin_Tool_Order_Status__c = 'C';
        i.Product_Platform__c = 'MSDSonline';

       
        insert i;
       

        ApexPages.currentPage().getParameters().put('id', acct.Id);
        accountOrderItems myController = new accountOrderItems();
        
        System.assertEquals(myController.activeList.size(), 1);
        System.assertEquals(myController.inactiveList.size(), 1);
        System.assertEquals(myController.inactiveList[0].Status__c, 'Cancelled');
        System.assertEquals(myController.activeList[0].Status__c, 'Active');
        System.assertEquals(myController.Account.Name, 'Test Account');
   
    }
    
    
    
    
    
    
    
    
    
}