@isTest()
Private class testOrderManualEntryHumantech {
    @isTest(seeAllData = True)
    static void test1() {
        // To get full coverage delete OI that start with 8 to allow class to create the first one at 800000
        // If statments in class li 110-125 will be covered by data in each method 
        // test 1-5 cover the order type new/renewal dependent on if the product is on the contract for the forst year or not 
        // test 6-9 cover the year for the order item wrapper being inserted  therfore if year 1 + 2 is checked create an OI for year 2 
        // Test 1 year contract scenario 
        // Set order type to new for the product 
       
        //Create account, contact, CLI
        //Pass in controller and contract ID and call save method 
        Account a = new Account();
        a.name = 'Tigers Compound';
        insert a;

          product2 p = new product2();
        p.name = 'Ergo';
        p.Reoccurring__c = true;

        insert p;
          
        Contract__c cont1 = new contract__c();
        cont1.Account__c = a.id;
        cont1.A2_Saas_Start_date__c = date.today();
        cont1.Order_Submitted_Date__c = date.today();
        cont1.Contract_length__c = '1 Year';
        insert cont1;
        
        contract_line_item__c CLI = new contract_line_item__c();
        CLI.Year_1__c = true;
        CLI.Year_2__c = false;
        CLI.Year_3__c = false;
        CLI.Year_4__c = false;
        CLI.Year_5__c = false;
        CLI.Contract__c = cont1.id; 
        CLI.Product__c = p.id;
        insert CLI;
        
        ApexPages.currentPage().getParameters().put('Id', cont1.id);
        orderManualEntryHumantech mc = new orderManualEntryHumantech();
        
        mc.save();
        
    }
    
    @isTest(seeAllData = True)
    static void test2() {
        // Test 2 years contract scenario 
        // Set order type to new only for the product that starts year 1 and renewal for the 2nd year of contract 
         
         //Create account, contact, CLI
        //Pass in controller and contract ID and call save method 
        Account ac = new Account();
        ac.name = 'Maciejs Bean';
        ac.AdminID__c = null;
        ac.CustomerID__c = null;
        
        insert ac;

          product2 p = new product2();
        p.name = 'Ergo';
        p.Reoccurring__c = true;

        insert p;
        
        Contract__c contract = new contract__c();
        contract.Account__c = ac.id;
        contract.A2_Saas_Start_date__c = date.today();
        contract.Order_Submitted_Date__c = date.today();
        contract.Contract_length__c = '2 Years';
        insert contract;
        
        contract_line_item__c CLI = new contract_line_item__c();
        CLI.Year_1__c = true;
        CLI.Year_2__c = true;
        CLI.Year_3__c = false;
        CLI.Year_4__c = false;
        CLI.Year_5__c = false;
        CLI.Contract__c = contract.id; 
        CLI.Product__c = p.id;
        insert CLI;
        
        ApexPages.currentPage().getParameters().put('Id', contract.id);
        orderManualEntryHumantech mc = new orderManualEntryHumantech();
        
        mc.save();
        
    }
    
    @isTest(seeAllData = True)
    static void test3() {
        // Test 3 years contract scenario
        // Set order type year 1 = new year 2 and 3 = renewal 
        Account ac = new Account();
        ac.name = 'Maciejs test';
        insert ac;

  product2 p = new product2();
        p.name = 'Ergo';
        p.Reoccurring__c = true;

        insert p;
        
        Contract__c contract = new contract__c();
        contract.Account__c = ac.id;
        contract.A2_Saas_Start_date__c = date.today();
        contract.Order_Submitted_Date__c = date.today();
        contract.Contract_length__c = '3 Years';
        insert contract;
        
        contract_line_item__c CLI = new contract_line_item__c();
        CLI.Year_1__c = true;
        CLI.Year_2__c = true;
        CLI.Year_3__c = true;
        CLI.Year_4__c = false;
        CLI.Year_5__c = false;
        CLI.Contract__c = contract.id; 
        CLI.Product__c = p.id;
        insert CLI;
        
        ApexPages.currentPage().getParameters().put('Id', contract.id);
        orderManualEntryHumantech mc = new orderManualEntryHumantech();
        
        mc.save();
    }
    
    @isTest(seeAllData = True)
    static void test4() {
        // Test 4 years contract scenario
        // Set order type year 1 = new year 2 + 3 + 4 = renewal 
 
        Account ac = new Account();
        ac.name = 'Maciejs Account';
        insert ac;

          product2 p = new product2();
        p.name = 'Ergo';
        p.Reoccurring__c = true;

        insert p;
        
        Contract__c contract = new contract__c();
        contract.Account__c = ac.id;
        contract.A2_Saas_Start_date__c = date.today();
        contract.Order_Submitted_Date__c = date.today();
        contract.Contract_length__c = '4 Years';
        insert contract;
        
        contract_line_item__c CLI = new contract_line_item__c();
        CLI.Year_1__c = true;
        CLI.Year_2__c = true;
        CLI.Year_3__c = true;
        CLI.Year_4__c = true;
        CLI.Year_5__c = false;
        CLI.Contract__c = contract.id; 
        CLI.Product__c = p.id;
        insert CLI;
        
        ApexPages.currentPage().getParameters().put('Id', contract.id);
        orderManualEntryHumantech mc = new orderManualEntryHumantech();
        
        mc.save();
    }
    
    
    @isTest(seeAllData = True)
    static void test5() {
        // Test 5 years contract scenario 
        // Set order type year 1 = new year 2 + 3 + 4 + 5 = renewal 
        Account ac = new Account();
        ac.name = 'New test';
        insert ac;

        product2 p = new product2();
        p.name = 'Ergo';
        p.Reoccurring__c = true;

        insert p;
        
        Contract__c contract = new contract__c();
        contract.Account__c = ac.id;
        contract.A2_Saas_Start_date__c = date.today();
        contract.Order_Submitted_Date__c = date.today();
        contract.Contract_length__c = '5 Years';
        insert contract;
        
        contract_line_item__c CLI = new contract_line_item__c();
        CLI.Year_1__c = true;
        CLI.Year_2__c = true;
        CLI.Year_3__c = true;
        CLI.Year_4__c = true;
        CLI.Year_5__c = true;
        CLI.Contract__c = contract.id; 
        CLi.Product__c = p.id;
        insert CLI;
        
        ApexPages.currentPage().getParameters().put('Id', contract.id);
        orderManualEntryHumantech mc = new orderManualEntryHumantech();
        
        mc.save();
        
    }
    
    
    
    @isTest(seeAllData = True)
    static void test6() {
        //Test to make the order type is set to new for products that dont start on the contract till year 2
        //Set order type to new for the products that enter the contract the first time  (Year 2)
        
        Account ac = new Account();
        ac.name = 'New test';
        ac.AdminID__c = null;
        ac.CustomerID__c = null;
        insert ac;
        
        Contract__c contract = new contract__c();
        contract.Account__c = ac.id;
        contract.A2_Saas_Start_date__c = date.today();
        contract.Order_Submitted_Date__c = date.today();
        contract.Contract_length__c = '2 Years';
        insert contract;
        
        contract_line_item__c CLI = new contract_line_item__c();
        CLI.Year_1__c = false;
        CLI.Year_2__c = true;
        CLI.Contract__c = contract.id; 
        insert CLI;
        
        ApexPages.currentPage().getParameters().put('Id', contract.id);
        orderManualEntryHumantech mc = new orderManualEntryHumantech();
        
        mc.save();
    }
    
    @isTest(seeAllData = True)
    static void test7() {
        //Test to make the order type is set to new for products that dont start on the contract till year 3 
       //Set order type to new for the products that enter the contract the first time (Year 3) 

        Account ac = new Account();
        ac.name = 'New test';
        ac.AdminID__c = null;
        ac.CustomerID__c = null;
        insert ac;
        
        Contract__c contract = new contract__c();
        contract.Account__c = ac.id;
        contract.A2_Saas_Start_date__c = date.today();
        contract.Order_Submitted_Date__c = date.today();
        contract.Contract_length__c = '3 Years';
        insert contract;
        
        contract_line_item__c CLI = new contract_line_item__c();
        CLI.Year_1__c = false;
        CLI.Year_2__c = false;
        CLI.Year_3__c = true;
        CLI.Contract__c = contract.id; 
        insert CLI;
        
        ApexPages.currentPage().getParameters().put('Id', contract.id);
        orderManualEntryHumantech mc = new orderManualEntryHumantech();
        
        mc.save();
    }
    
    @isTest(seeAllData = True)
    static void test8() {
        //Test to make the order type is set to new for products that dont start on the contract till year 4
        //Set order type to new for the products that enter the contract the first time (Year 4) 

        Account ac = new Account();
        ac.name = 'New test';
        ac.AdminID__c = null;
        ac.CustomerID__c = null;
        insert ac;
        
        Contract__c contract = new contract__c();
        contract.Account__c = ac.id;
        contract.A2_Saas_Start_date__c = date.today();
        contract.Order_Submitted_Date__c = date.today();
        contract.Contract_length__c = '4 Years';
        insert contract;
        
        contract_line_item__c CLI = new contract_line_item__c();
        CLI.Year_1__c = false;
        CLI.Year_2__c = false;
        CLI.Year_3__c = false;
        CLI.Year_4__c = true;
        CLI.Contract__c = contract.id; 
        insert CLI;
        
        ApexPages.currentPage().getParameters().put('Id', contract.id);
        orderManualEntryHumantech mc = new orderManualEntryHumantech();
        
        mc.save();
    }
    
    @isTest(seeAllData = True)
    static void test9() {
        //Test to make the order type is set to new for products that dont start on the contract till year 5
       //Set order type to new for the products that enter the contract the first time (Year 5)

        Account ac = new Account();
        ac.name = 'New test';
        ac.AdminID__c = null;
        ac.CustomerID__c = null;
        insert ac;
        
        Contract__c contract = new contract__c();
        contract.Account__c = ac.id;
        contract.A2_Saas_Start_date__c = date.today();
        contract.Order_Submitted_Date__c = date.today();
        contract.Contract_length__c = '5 Years';
        insert contract;
        
        contract_line_item__c CLI = new contract_line_item__c();
        CLI.Year_1__c = false;
        CLI.Year_2__c = false;
        CLI.Year_3__c = false;
        CLI.Year_4__c = false;
        CLI.Year_5__c = true;
        CLI.Contract__c = contract.id; 
        insert CLI;
        
        ApexPages.currentPage().getParameters().put('Id', contract.id);
        orderManualEntryHumantech mc = new orderManualEntryHumantech();
        
        mc.save();
    }
    
}