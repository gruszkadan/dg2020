//Test Class: testSalesPerformanceRequestIncentives,testOrderPendingAdjustments
public class sprEmailTableController {
    public String sprId {get;set;}
    public Sales_Performance_Request__c spr {get;set;}
    public String baseUrl {get;set;}
    //Splits
    public Sales_Performance_Item_Split__c [] splits {get;set;}
    public map<id,String> productMap {get;set;}
    public set<String> owners {get;set;}
    public map<String,Sales_Performance_Item_Split__c> ownerMap {get;set;}
    //Adjustments/Disputes
    public String[] adjustments {get;set;}
    public set<id> spiIds {get;set;}
    public map<id,String> spiAmountMap {get;set;}
    public map<id,String> spiOriginalAmountMap {get;set;}
    public Sales_Performance_Item__c[] items {get;set;}
    //Automated Adjustment
    public decimal originalBookings {get;set;}
    public decimal originalIncentive {get;set;}
    public map<id,String> spiStatusMap {get;set;}
    
    public void getData(){
        baseUrl = string.valueOf(System.URL.getSalesforceBaseUrl());
        if(sprId != null){
            spr = [SELECT id,Name,Booking_Amount__c,Incentive_Amount__c,Current_Approver_Email__c,Status__c,Sales_Performance_Order__r.Account__r.Name,Owner.Name,Reason__c,Type__c,JSON__c,Sales_Performance_Order__r.Contract_Number__c,Sales_Rep__r.Name
                   FROM Sales_Performance_Request__c
                   WHERE id =:sprId LIMIT 1];
            //If split then create columns and rows for splits
            if(spr.Type__c == 'Split'){
                splits = [Select id, Product_Name__c, SPI_Split_From__c, Split_Group__c, Booking_Amount__c, Incentive_Amount__c, Owner.Name 
                          From Sales_Performance_Item_Split__c
                          WHERE Sales_Performance_Request__c =:spr.id
                          ORDER BY Split_Group__c ASC NULLS LAST];
                owners = new set<String>();
                ownerMap = new map<String,Sales_Performance_Item_Split__c>();
                productMap = new map<id,String>();
                if(splits.size() > 0){
                    for(Sales_Performance_Item_Split__c split:splits){
                        owners.add(split.Owner.Name);
                        ownerMap.put(split.Owner.Name+'|'+split.Product_Name__c+'|'+split.SPI_Split_From__c,split);
                        productMap.put(split.SPI_Split_From__c,split.Product_Name__c);
                    }
                }
            }
            
            //If adjustment/dispute then create columns and rows for adjustments
            else if(spr.Type__c == 'Adjustment/Dispute'){
                adjustments = (List<String>)System.JSON.deserialize(spr.JSON__c, List<String>.class);
                spiIds = new set<id>();
                spiAmountMap = new map<id,String>();
                spiOriginalAmountMap = new map<id,String>();
                for(String s:adjustments){
                    String[] sList = s.split('\\|');
                    spiIds.add(sList[0]);
                    spiAmountMap.put(sList[0],sList[1]);
                    spiOriginalAmountMap.put(sList[0],sList[2]);
                }
                items = [SELECT id,Product_Name__c,Total_Incentive_Amount__c, Owner.Name
                         FROM Sales_Performance_Item__c
                         WHERE id in:spiIds];
                
            }
            //If adjustment/dispute then create columns and rows for adjustments
            else if(spr.Type__c == 'Automated Adjustment'){
                adjustments = (List<String>)System.JSON.deserialize(spr.JSON__c, List<String>.class);
                spiIds = new set<id>();
                spiAmountMap = new map<id,String>();
                spiOriginalAmountMap = new map<id,String>();
                spiStatusMap = new map<id,String>();
                spiIds.add(adjustments[1]);
                spiAmountMap.put(adjustments[1],adjustments[4]);
                spiOriginalAmountMap.put(adjustments[1],adjustments[3]);
                spiStatusMap.put(adjustments[1],adjustments[5]);
                items = [SELECT id,Product_Name__c,Total_Incentive_Amount__c, Owner.Name
                         FROM Sales_Performance_Item__c
                         WHERE id in:spiIds];
                
            }
        }
    }
}