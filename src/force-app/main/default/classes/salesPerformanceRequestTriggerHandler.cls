public class salesPerformanceRequestTriggerHandler {
    
    public static void beforeInsert(Sales_Performance_Request__c[] oldRequests, Sales_Performance_Request__c[] newRequests) {}
    
    public static void beforeUpdate(Sales_Performance_Request__c[] oldRequests, Sales_Performance_Request__c[] newRequests) {
        Sales_Performance_Request__c[] currentApproverUpdate = new list<Sales_Performance_Request__c>();
        for (integer i=0; i<newRequests.size(); i++) {
            if (newRequests[i].Current_Approver__c != oldRequests[i].Current_Approver__c) {
                currentApproverUpdate.add(newRequests[i]);
            }
        }
        if(currentApproverUpdate.size() >0){
            salesPerformanceRequestUtility.updateCurrentApproverLookup(currentApproverUpdate);
        }
    }
    
    public static void afterInsert(Sales_Performance_Request__c[] oldRequests, Sales_Performance_Request__c[] newRequests) {
        Sales_Performance_Request__c[] requiresApproval = new list<Sales_Performance_Request__c>();
        Sales_Performance_Request__c[] approvedManualAdjustments = new list<Sales_Performance_Request__c>();
        Sales_Performance_Request__c[] otherIncentive = new list<Sales_Performance_Request__c>();
        Sales_Performance_Request__c[] ghostBooking = new list<Sales_Performance_Request__c>();
        Sales_Performance_Request__c[] processedEmail = new list<Sales_Performance_Request__c>();
        
        
        for (integer i=0; i<newRequests.size(); i++) {
            if (newRequests[i].Requires_Approval__c == TRUE) {
                requiresApproval.add(newRequests[i]);
            }
            if (newRequests[i].Type__c == 'Adjustment/Dispute' && newRequests[i].Requires_Approval__c == FALSE && newRequests[i].Status__c == 'Approved') {
                approvedManualAdjustments.add(newRequests[i]);
            }
            if (newRequests[i].Type__c == 'Add Incentive' && newRequests[i].Requires_Approval__c == FALSE && newRequests[i].Status__c == 'Approved') {
                otherIncentive.add(newRequests[i]);
            }
            if (newRequests[i].Type__c == 'Ghost Booking' && newRequests[i].Requires_Approval__c == FALSE && newRequests[i].Status__c == 'Approved') {
                ghostBooking.add(newRequests[i]);
            }
            if (newRequests[i].Status__c == 'Approved' && newRequests[i].Requires_Approval__c == FALSE) {
                processedEmail.add(newRequests[i]);
            }
            
            
        }
        if(requiresApproval.size() >0){
            salesPerformanceRequestUtility.submitForApproval(requiresApproval);
        }
        if(approvedManualAdjustments.size() >0){
            incentiveTransactionUtility.createManualAdjustmentIncentives(approvedManualAdjustments);
        }
        if(otherIncentive.size() >0){
            salesPerformanceRequestUtility.buildIncentiveTransaction(otherIncentive);
        }
        if(ghostBooking.size() >0){
            salesPerformanceOrderUtility.insertGhostBookingSPO(ghostBooking[0]);
        }
        if(processedEmail.size() >0){
            salesPerformanceUtility.sendSprApprovedRejectedNotification(processedEmail);
        }
        
    }
    
    
    public static void afterUpdate(Sales_Performance_Request__c[] oldRequests, Sales_Performance_Request__c[] newRequests) {
        Sales_Performance_Request__c[] approvedSplits = new list<Sales_Performance_Request__c>();
        Sales_Performance_Request__c[] approvedManualAdjustments = new list<Sales_Performance_Request__c>();
        Sales_Performance_Request__c[] requiresApproval = new list<Sales_Performance_Request__c>();
        Sales_Performance_Request__c[] otherIncentive = new list<Sales_Performance_Request__c>();
        Sales_Performance_Request__c[] ghostBooking = new list<Sales_Performance_Request__c>();
        Sales_Performance_Request__c[] automatedAdjustments = new list<Sales_Performance_Request__c>();
        Sales_Performance_Request__c[] processedEmail = new list<Sales_Performance_Request__c>();
        
        for (integer i=0; i<newRequests.size(); i++) {
            if (newRequests[i].Type__c == 'Split' && oldRequests[i].Status__c != 'Approved' && newRequests[i].Status__c == 'Approved') {
                approvedSplits.add(newRequests[i]);
            }
            if (newRequests[i].Type__c == 'Adjustment/Dispute' && oldRequests[i].Status__c != 'Approved' && newRequests[i].Status__c == 'Approved') {
                approvedManualAdjustments.add(newRequests[i]);
            }
            if (newRequests[i].Type__c == 'Add Incentive' && oldRequests[i].Status__c != 'Approved' && newRequests[i].Status__c == 'Approved') {
                otherIncentive.add(newRequests[i]);
            }
            if (newRequests[i].Type__c == 'Ghost Booking' && oldRequests[i].Status__c != 'Approved' && newRequests[i].Status__c == 'Approved') {
                ghostBooking.add(newRequests[i]);
            }
            if (newRequests[i].Type__c == 'Automated Adjustment' && oldRequests[i].Status__c != 'Approved' && newRequests[i].Status__c == 'Approved') {
                automatedAdjustments.add(newRequests[i]);
            }
            if (newRequests[i].Requires_Approval__c == TRUE && oldRequests[i].Requires_Approval__c == FALSE) {
                requiresApproval.add(newRequests[i]);
            }
            if (newRequests[i].Adjustment_Type__c != 'FX Change' && newRequests[i].Status__c != oldRequests[i].Status__c && (newRequests[i].Status__c == 'Approved' || newRequests[i].Status__c == 'Rejected')) {
                processedEmail.add(newRequests[i]);
            }
            
        }
        if(approvedSplits.size() >0){
            salesPerformanceSplitUtility.processSplit(approvedSplits[0]);
        }
        if(approvedManualAdjustments.size() >0){
            incentiveTransactionUtility.createManualAdjustmentIncentives(approvedManualAdjustments);
        }
        if(otherIncentive.size() >0){
            salesPerformanceRequestUtility.buildIncentiveTransaction(otherIncentive);
        }
        if(requiresApproval.size() >0){
            salesPerformanceRequestUtility.submitForApproval(requiresApproval);
        }
        if(ghostBooking.size() >0){
            salesPerformanceOrderUtility.insertGhostBookingSPO(ghostBooking[0]);
        }
        if(automatedAdjustments.size() >0){
            salesPerformanceItemUtility.automatedAdjustmentSPI(automatedAdjustments);
        }
        if(processedEmail.size() >0){
            salesPerformanceUtility.sendSprApprovedRejectedNotification(processedEmail);
        }
        
        
    }
    
}