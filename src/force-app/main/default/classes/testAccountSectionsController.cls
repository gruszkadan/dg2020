@isTest(seeAllData=true)
private class testAccountSectionsController {

    static testmethod void test1() {
        string refList = 'Authoring;Chemical Management;Ergonomics;Training;EHS;';
        
        Account a = new Account(Name='Test Account');
        insert a;
        
        Contact c = new Contact(FirstName='Bill', LastName='Test', AccountId=a.id);
        insert c;
        
        Account_Referral__c[] ars = new List<Account_Referral__c>();
        for (string s : refList.split(';',0)) {
            Account_Referral__c ar = new Account_Referral__c();
            ar.Account__c = a.id;
            ar.Contact__c = c.id;
            ar.Status__c = null;
            ar.Type__c = s;
            ar.Compliance_Beyond_OSHA__c = 'Yes';
            ars.add(ar);
        }
        insert ars;
        
        Referral_Settings__c[] rss = new List<Referral_Settings__c>();
        for (string s : refList.split(';',0)) {
            Referral_Settings__c rs = new Referral_Settings__c();
            rs.Name = s+' test';
            rs.Sort_Order__c = 1;
            rs.Type__c = s;
            rs.Question__c = 'Test question?';
            if (s == 'Authoring') {
                rs.Account_Referral_API_Name__c = 'Number_Of_Products_Requiring_SDS__c';
                rs.Account_API_Name__c = 'Number_Of_Products_Requiring_SDS__c';
            }
            if (s == 'Chemical Management') {
                rs.Account_Referral_API_Name__c = 'Num_Employees__c';
                rs.Account_API_Name__c = 'Num_of_Employees__c';
            }
            if (s == 'Ergonomics') {
                rs.Account_Referral_API_Name__c = 'Employees_Sit_In_Front_of_Computer__c';
                rs.Account_API_Name__c = 'Employees_Sit_In_Front_of_Computer__c';
            }
            if (s == 'Training') {
                rs.Account_Referral_API_Name__c = 'Safety_Training_Online__c';
                rs.Account_API_Name__c = 'Safety_Training_Online__c';
            }
            if (s == 'EHS') {
                rs.Account_Referral_API_Name__c = 'Compliance_Beyond_OSHA__c';
                rs.Account_API_Name__c = 'Compliance_Beyond_OSHA__c';
            }
            rss.add(rs);
        }
        insert rss;
        
        accountSectionsController con = new accountSectionsController();
        
        con.xRefs = new List<Account_Referral__c>();
        con.xRefSets = new List<Referral_Settings__c>();
        con.xAcct = a;
        con.xRefs.addAll(ars);
        con.xRefSets.addAll(rss);
        system.assert(con.xRefs.size() == 5, 'xRefs list size is incorrect: expected 5, returned '+con.xRefs.size());
        system.assert(con.xRefSets.size() == 5, 'xRefSets list size is incorrect: expected 5, returned '+con.xRefSets.size());
        
        con.getRefs();
        system.assert(con.getRefs() == refList, 'getRefs() method returned incorrect string: expected "Authoring;Chemical Management;Ergonomics;Training;EHS;", returned '+con.getRefs());
        
        con.getNewRefs();
        system.assert(con.getNewRefs() == refList, 'getNewRefs() method returned incorrect string: expected "Authoring;Chemical Management;Ergonomics;Training;EHS;", returned '+con.getNewRefs());
        
        con.getPrevRefs();
        for (integer i=0; i<con.xRefs.size(); i++) {
            system.assert(con.getPrevRefs()[i].refType == refList.split(';',0)[i], 'getPrevRefs() method returned prevRef wrappers with incorrect type: expected '+refList.split(';',0)[i]+', returned '+con.getPrevRefs()[i].refType);
        }
        
        ApexPages.currentPage().getParameters().put('refType', 'EHS');
        con.referralCompare();
        for (integer i=0; i<con.qWrappers.size(); i++) {
            con.qWrappers[i].selVal = 'refVal';
        }
        system.assert(con.qWrappers.size() == 1, 'qWrappers.size() is incorrect: expected 1, returned '+con.qWrappers.size());
        
        con.saveComparison();
        system.assert(a.Compliance_Beyond_OSHA__c == 'Yes', 'saveComparison() method did not assign the correct value for "Compliance_Beyond_OSHA__c": expected "Yes", returned '+a.Compliance_Beyond_OSHA__c);
    }
}