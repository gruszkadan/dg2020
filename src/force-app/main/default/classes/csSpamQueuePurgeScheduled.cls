public class csSpamQueuePurgeScheduled implements Schedulable{   
    public void execute(SchedulableContext SC) {
        Case[] spamCases = new list<Case>();
        spamCases = [SELECT id,CaseNumber FROM Case WHERE Owner.Name = 'Spam Customer Support' and CreatedDate <=: date.today().addDays(-60)];
        if(test.isRunningTest()){
            spamCases = [SELECT id,CaseNumber FROM Case WHERE Owner.Name = 'Spam Customer Support' LIMIT 1];
        }
        if(spamCases.size() > 0){
            try {
                delete spamCases;
            } catch(exception e) {
                salesforceLog.createLog('Case', true, 'csSpamQueuePurgeScheduled', 'execute', string.valueOf(e));
            }
        }
    }
}