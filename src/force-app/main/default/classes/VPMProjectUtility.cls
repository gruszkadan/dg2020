public class VPMProjectUtility {
    
    public static void projectStatusEvaluation(VPM_Milestone__c[] milestones){
        
        Set<Id> projectIds = new Set<Id>();
        for(VPM_Milestone__c m : milestones){
            projectIds.add(m.VPM_Project__c);
        }    
        
        List<VPM_Project__c> relatedProjects = new List<VPM_Project__c>();    
        relatedProjects = [Select id, (Select id, Status__c from VPM_Milestones__r) from  VPM_Project__c where id IN :projectIds];   
         
        
        for(VPM_Project__c p: relatedProjects){
            
            boolean hasOpenMilestones = false;
            for(VPM_Milestone__c m: p.VPM_Milestones__r){
                if(m.Status__c != 'Closed'){
                    hasOpenMilestones = true;
                }
            }
            if(hasOpenMilestones == true){
                p.Project_End_Date__c = NULL;
                p.Project_Status__c = 'Open';
            }else{
                p.Project_End_Date__c = System.today();
                p.Project_Status__c = 'Closed';
            }
        }
        try{
            update relatedProjects;
        }catch(exception e){
            salesforceLog.createLog('VPM_Project__c', true, 'VPMProjectUtility', 'projectStatusEvaluation', +string.valueOf(e));
        }        
        
    }

    
}