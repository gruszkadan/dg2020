trigger revenueRecognitionTrigger on Revenue_Recognition__c (after insert, after update) {
   
    //AFTER TRIGGER
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
              revenueRecognitionTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
              revenueRecognitionTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
    }
    
}