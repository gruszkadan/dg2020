@isTest(SeeAllData=true)
private class testCorporateAccountController 
{
    
    public static testmethod void testCorporateAccountController()
    {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        insert newAccount;
        
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        corporateAccountController myController = new corporateAccountController(testController);
        
        myController.corpAccountApproval();
        myController.corpAccountReject();
        myController.approveCorpAccount();
        myController.rejectCorpAccount();
        myController.reload();
        
    }
    
    public static testmethod void testcorpAcctRefController()
    {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        newAccount.Corporate_Account__c = true;
        insert newAccount;
        
        Corporate_Account_Referral__c newRef = new Corporate_Account_Referral__c();
        newRef.Rep__c = '00580000003UChI';
        newRef.Reviewer__c = '00580000003UChI';
        newRef.Critical_Issues__c = 'Test issues';
        newRef.Referral_Type__c = 'Referal type';
        newref.Account__c = newAccount.Id;
        insert newRef;
        
        ApexPages.currentPage().getParameters().put('Id', newRef.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Corporate_Account_Referral__c());
        corpAcctRefController myController = new corpAcctRefController(testController);
        
        myController.ref = newRef;
        myController.a = newAccount;
        
        
        
        
        myController.SendCorpAccountReferralEmail();
        myController.referralCancel();
        myController.reload();
        myController.approveCorpAccountReferral();
        myController.corpAccountReferralApproval();
        myController.rejectCorpAccountReferral();
        
        
        
        
        
    }
}