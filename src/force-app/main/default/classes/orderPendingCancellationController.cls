public class orderPendingCancellationController {
    Public Account_Management_Event__c[] Pending {get;set;}
    Public Group Cancel {get;set;}
    Public Opportunity[] open {get;set;}
    Public Opportunity[] close {get;set;}
    public ameWrapper[] ameWrappers {get;set;}
    public Account_Management_Event__c[] UpdateList {get;set;}
    Public boolean selectAll {get;set;}
    Public boolean error {get;set;}
    public boolean errormsg {get;set;}
    public string errormessage {get;set;}
    
    public orderPendingCancellationController(){
        //Set Variables on load 
        selectAll = false;
        error = false;
        errormsg = false; 
        
        //Query to find ID of Cancellation Queue 
        //This will be used in the Updated Wrappers method 
        Cancel = [Select Id from Group where Type = 'Queue' and DeveloperNAME = 'Order_Cancellation' limit 1]; 
        
        // call method on the load to generate wrappers/table data 
        UpdatedWrappers();
        
    }
    
    //Wrapper class for AME's 
    public class ameWrapper {
        public Account_Management_Event__c ame {get;set;}
        public boolean isSelected {get;set;}
        
        public ameWrapper(Account_Management_Event__c xAme){
            ame = xAme;
            isSelected = false;
        }
    }
    
    Public void ProcessCancelation(){
        //Method to process cancellations 
        // Intialize Lists needed in method 
        Updatelist = new list <Account_Management_Event__c>();
        set <id> Related = new set <id>();
        Close = new list <opportunity>();
        
        // Set error message to fasle when method starts running
        error = false;
        errormsg = false; 
        
        
        //loop through ame wrappers and if AME is selected but the user didnt fill out the Cancellation Processed date fire the error message  
        //If AME is slected and no error occours set the fields and add to list to update at the end of method 
        //Find all related AMEs and add them too a seperate list 
        for(amewrapper x : amewrappers){
            if(x.isselected == true && x.ame.Cancellation_Processed_Date__c == null){
                error = true;
            }Else if (x.isselected == true ) {
                x.ame.OwnerId =  x.ame.Sent_to_Orders_By__c;
                x.ame.Status__c = 'Closed';
                x.ame.Cancellation_Processed_By__c = UserInfo.getUserId();  
                
                updatelist.add(x.ame);
                
                if(x.ame.Related_AME__c != null) {
                    
                    Related.add(x.ame.Related_AME__c);
                    
                }
                
            }
        }
        
        
        if(error == false){
            
            
            
            //Loop through AMEs in the related list and set status to closed 
            //Add those AMEs to the update list 
            for(Account_Management_Event__c r : [Select Id, name, Related_AME__c from Account_Management_Event__c where id =: Related]){   
                r.Status__c = 'Closed';
                updatelist.add(r);
            }
            
            //Query Opps that are open and in our update list 
            open = [Select id, Account_Management_Event__c from Opportunity where isclosed = false AND Account_Management_Event__c in:updateList];
            
            //loop through those records and set fields 
            //Add those records to the Close list 
            for(Opportunity o : open){
                o.StageName = 'Closed Lost';
                o.Reason_Lost__c = 'Cancel';
                o.CloseDate = date.today();
                
                Close.add(o);
                
            }
            
            // Update AMEs and Opportunitys
            
            try{
                
                update close;
                update updatelist;
                
            }catch (Exception e)  {
                
                errormsg = true;
                errormessage = e.getMessage();
                
            }
            
            // call method again to update the table once canelation is processed
            UpdatedWrappers();
            
            
            
        }
    }
    
    Public void CheckboxSelectAll(){
        //Loop through ame wrappers and assign the is selcted vaiable to the select all variable 
        for(amewrapper a : amewrappers){
            a.isSelected = selectall;
        } 
    } 
    
    
    
    Public void UpdatedWrappers(){
        //Query to find all AME's that are in the Order Cancellation Quene 
        
        
        Pending = [Select Id, name, Account__c, Account__r.AdminID__c, Account__r.name, Sent_to_Orders_By__c,
                   Sent_to_Orders_By__r.name, Sent_to_Orders_Date__c, Cancellation_Processed_Date__c, Related_AME__c 
                   from Account_Management_Event__c where OwnerId =: Cancel.id];
        
        //Initialize new list of AME wrappers that will hold 
        ameWrappers = new List<ameWrapper>();
        //When query returns records add to wrapper class 
        if(Pending.size() > 0){
            for(Account_Management_Event__c am:Pending){
                ameWrappers.add(new ameWrapper(am));
            }
        }
    }
}