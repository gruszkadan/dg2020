@isTest(SeeAllData = true)

private class testQuoteProposalNeedManagement {
    
    static testMethod void test1(){
        
        Quote_Proposal_Need__c testQuote = new Quote_Proposal_Need__c();
        testQuote.Available_For_Use__c = true;
        testQuote.Global__c = true;
        insert testQuote;
        
        Product2 prod = [Select Id, Family, Name FROM Product2 WHERE Name = 'GM Account'];
        
        //create product need and link to product and QPN that is in table
        Product_Need__c prodNeed = new Product_Need__c();
        prodNeed.Product__c = prod.Id;
        prodNeed.Quote_Proposal_Need__c = testQuote.Id;
        insert prodNeed;
        
        quoteProposalNeedManagement con = new quoteProposalNeedManagement(); 
        
        //set picklist values to match criteria of QPN
        con.selectedFamily = prod.Family;
        con.selectedProduct = prod.Name;
        con.isAvailable = 'true';
        con.isGlobal = 'true';
        
        
        //query QPN
        con.queryQuoteProposalNeeds();
        
        //grab QPN id to run delete method
        ApexPages.currentPage().getParameters().put('needId', testQuote.id);
        con.deleteQuoteProposalNeed();
        
        List<Quote_Proposal_Need__c> testQPN = [SELECT Id FROM Quote_Proposal_Need__c WHERE Id = :testQuote.id];
        system.assertEquals(testQPN.size(), 0);
        
        con.getFamilyOptions();
        con.getProductOptions();
        con.getAvailForUse();
        con.getGlobal();
        con.getShow();
        
        
        
        
    }
    
    
    
    
    
}