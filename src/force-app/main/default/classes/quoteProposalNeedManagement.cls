public class quoteProposalNeedManagement {
    
    public List<Quote_Proposal_Need__c> quoteProposalNeeds {get;set;}
    public List<Quote_Proposal_Need__c> tempquoteProposalNeeds {get;set;}
    public Set<Product_Need__c> picklistSource {get;set;}
    public List<Product2> prods {get;set;}
    public string ShowNumber {get;set;}
    public string selectedFamily {get;set;}
    public string selectedProduct {get;set;}
    public string isAvailable {get;set;}
    public string isGlobal {get;set;}
    public boolean famChange {get;set;}
   
    
    
    
    public quoteProposalNeedManagement(){
        QueryQuoteProposalNeeds();
        prods = [SELECT Family, Name FROM Product2 WHERE isActive = true];
       	ShowNumber = '10'; 
        buildPicklist();
    }
    
    
    public void QueryQuoteProposalNeeds(){
        quoteProposalNeeds = new List<Quote_Proposal_Need__c>();
        string query = 'SELECT id, Need__c, Available_For_Use__c, Description__c, Product__c, Product_Family__c, Global__c, Need_Heading__c, Display_Need_For_All_Products__c, (Select Product__r.Family, Product__r.Name FROM Product_Needs__r';
        if (selectedFamily != null || selectedproduct != null){
            query += ' WHERE ';
            if (selectedFamily != null){
                query +=  'Product__r.Family = \'' + selectedFamily +  '\'';
            }
            if (selectedproduct != null){
                if(selectedFamily != null){
                    query += ' AND ';
                }
                query +=  'Product__r.Name = \'' + selectedProduct +  '\'';
            }
        }
        query += ' ) FROM Quote_Proposal_Need__c';
   		if (isAvailable != null || isGlobal != null) {
          
            query += ' WHERE ';
            if (isAvailable != null){
                query += 'Available_For_Use__c=' + isAvailable ;
            }
            if (isGlobal != null){
                if(isAvailable != null){
                    query += ' AND ';
                        }
                query += 'Global__c=' + isGlobal;
            }
        }
        query += ' ORDER BY Display_Need_For_All_Products__c ASC, LastModifiedDate desc';
        tempquoteProposalNeeds = database.query(query);

        if (selectedFamily != null || selectedproduct != null){
            for(Quote_Proposal_Need__c n:tempquoteProposalNeeds ){
                if(n.Product_Needs__r.size() > 0 || n.Display_Need_For_All_Products__c == true) {
                    quoteProposalNeeds.add(n);  
                }
            }
        }else{
            quoteProposalNeeds = tempquoteProposalNeeds;
        }
    }
    
    //Only runs on load -- since all variables are null this provides the base data for picklists and building out product tables
    public void buildPicklist(){
        picklistSource = new Set<Product_Need__c>();
        for(Quote_Proposal_Need__c n:tempquoteProposalNeeds ){
            if(n.Product_Needs__r.size() > 0) {
                for(Product_Need__c pN: n.Product_Needs__r){
                    if(pN != NULL){
                        picklistSource.add(pN);
                    }  
                }     
            }
        }
    }

    public void clearThenQuery(){
        selectedproduct=NULL;
        QueryQuoteProposalNeeds();
    }
    
    public PageReference deleteQuoteProposalNeed() {
        Id quoteProposalNeedId = ApexPages.currentPage().getParameters().get('NeedId');
        for(Quote_Proposal_Need__c qPN : quoteProposalNeeds){
            if(qPN.Id == quoteProposalNeedId){
                delete qPN;  
            }  
        }
        PageReference quote_proposal_need_management = Page.quote_proposal_need_management;
        quote_proposal_need_management.setRedirect(true);
        return quote_proposal_need_management;  
    }
    
  
    
    public List<SelectOption> getProductOptions() {
        Set<SelectOption> options = new Set<SelectOption>();  
        options.add(new selectOption('', ' - All -'));
        for(Product_Need__c p : picklistSource){
            if(p.Product__r.Family != null && selectedFamily == null){
                options.add(new SelectOption(p.Product__r.Name, p.Product__r.Name));
            }
            if(p.Product__r.Family != null && selectedFamily == p.Product__r.Family){
                options.add(new SelectOption(p.Product__r.Name, p.Product__r.Name));
            }
        }
        List<SelectOption> optionList = new List<SelectOption>(options);
        optionList.sort();
        return optionList;
    }
    
    
    public List<SelectOption> getFamilyOptions() {
        Set<SelectOption> options = new Set<SelectOption>();  
        options.add(new selectOption('', ' - All -'));
        for(Product_Need__c p : picklistSource){
            if(p.Product__r.Family != null){
                options.add(new SelectOption(p.Product__r.family, p.Product__r.family));
            }
        }
        List<SelectOption> optionList = new List<SelectOption>(options);
        optionList.sort();
        return optionList;
    }
    
  
    public List<SelectOption> getAvailForUse() {
        List<SelectOption> options = new List<SelectOption>();  
        options.add(new selectOption('', ' - All -'));
        options.add(new selectOption('true', 'Yes'));
        options.add(new selectOption('false', 'No'));
        return options;
    }
    
    public List<SelectOption> getGlobal() {
        List<SelectOption> options = new List<SelectOption>();  
        options.add(new selectOption('', ' - All -'));
        options.add(new selectOption('true', 'Yes'));
        options.add(new selectOption('false', 'No'));
        return options;
    }
    
    public List<SelectOption> getShow() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new selectOption('5', '5'));
        options.add(new selectOption('10', '10'));
        options.add(new selectOption('25', '25'));
        options.add(new selectOption('50', '50'));
        options.add(new selectOption('100', '100'));
        options.add(new selectOption('200', '200'));
        return options;
    }
    
    
    
}