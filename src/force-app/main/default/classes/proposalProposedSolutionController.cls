public class proposalProposedSolutionController {
    public id quoteId {get;set;}
    public id quoteOpsId {get;set;}
    public quoteWrapper[] quoteWrapperList {get;set;}
    public boolean includeAuthoringServicesPage {get;set;}
    public boolean allQuantitiesAreHidden {get;set;}
    public Product_Print_Grouping__c[] allPPGs {get;set;}

    
    public proposalProposedSolutionController(){
       
        allQuantitiesAreHidden = true;
        includeAuthoringServicesPage = false;
        quoteId = ApexPages.currentPage().getParameters().get('id'); 
        quoteOpsId = [SELECT Quote_Options__c FROM Quote__c WHERE id = :quoteId LIMIT 1].Quote_Options__c;
        Quote__c[] getData = new List<Quote__c>();
        
        allPPGs = [Select id, Name__c from Product_Print_Grouping__c];
        Map<string, List<quoteProductWrapper>> correlatedProductsMap = new Map<string, List<quoteProductWrapper>>();
        
        string query = 'Select Id, Name, JSON_Proposal_Data__c, Pricing_Terms__c, Pricing_Terms_Specific_Date__c, Proposal_Show_Locations__c, Proposal_Show_Employees__c, Proposal_Show_Product_Descriptions__c, Bundled_Products__c, Ergo_Level_2_Hourly_Rate__c, Ergo_Level_3_Hourly_Rate__c, ' + 
            ' Y1_Total__c,  Y2_Total__c, Y3_Total__c, Y4_Total__c, Y5_Total__c, Contract_Length__c, Include_Option_In_Proposal__c, Option_Name__c, Currency__c, Currency_Rate__c, HQ_Qty__c, Num_of_Authoring_Products__c, Healthcare_Customer__c, Proposal_Locations__c, ' +
            '(SELECT Print_Group__c, Hide_Quantity__c, Quantity__c, Bundled_Product__c, Y1_Total_with_Bundle__c, Product__r.Proposal_Description__c, Product__r.Quantity_Calculation__c, Product__r.Quantity_to_Display_on_Proposal__c, Product__r.Custom_Proposal_Quantity__c, Y2_Total_with_Bundle__c, Y3_Total_with_Bundle__c, Y4_Total_with_Bundle__c, Y5_Total_with_Bundle__c, Y1_Total_Price__c, Y2_Total_Price__c,Y3_Total_Price__c,Y4_Total_Price__c,Y5_Total_Price__c, quote__r.Currency_Rate__c, ' +
            'Product_Print_Name__c,Product__c,Quote_Price__c ,Quote__c, Y1_Discount__c, Y2_Discount__c, Y3_Discount__c, Y4_Discount__c, Y5_Discount__c, Year_1__c, Year_2__c, Year_3__c, Year_4__c, Year_5__c, Main_Bundled_Product__c, Bundled_Indexing_Display_Type__c, Combined_Indexing_Language__c, Auth_Project_Terms__c, Y3_Custom_Proposal_List_Price__c, Y4_Custom_Proposal_List_Price__c, Y5_Custom_Proposal_List_Price__c, Y2_Custom_Proposal_List_Price__c, Y1_Custom_Proposal_List_Price__c, ' +
            'Proposal_Family_Sort_Order__c, Sort_Order__c, Pricing_Display__c, Y1_List_Price__c, Y2_List_Price__c, Y3_List_Price__c, Y4_List_Price__c, Y5_List_Price__c, Quantity_Calculation__c, Group_ID__c, 	Group_Parent_ID__c, Product__r.Display_Cost_Per_Student__c, Per_Unit_Price__c, Product__r.Display_Num_of_Seats__c, Product__r.Display_Cost_Per_Seat__c,  Product_Print_Grouping_Name__c,  Product__r.Product_Print_Grouping__c, Product__r.Can_Hide_Quantity__c,' +
            'Product__r.Display_Num_of_Courses__c , Name__c, ODT_Scorm_Num_of_Courses__c, ODT_num_of_Courses__c, Product__r.Display_Num_of_Students__c, Product__r.Display_Num_of_Enrollments_Per_Student__c, ODT_Flex_Max_Num_of_Enrollments__c, Special_Indexing_Fields__c, Proposal_Subpoint__c, Product__r.Proposal_Subpoint__c, Indexing_Language__c, Group_Name__c,  Product__r.Ergonomics__c '
            +'FROM Quote_Products__r ORDER BY Proposal_Family_Sort_Order__c ASC, Sort_Order__c ASC) FROM Quote__c';
        
        if (quoteOpsId != null) {
            query += ' WHERE Quote_Options__c= \'' + quoteOpsId +  '\' ';
        }else {
            query += ' WHERE Id = \'' + quoteId +  '\' ';
        }
        query += ' AND Include_Option_In_Proposal__c = true ';
        getData = database.query(query);
        
        quoteWrapperList = new List<quoteWrapper>();
        for(quote__c q: getData){
            if(q.Num_of_Authoring_Products__c != NULL && q.Num_of_Authoring_Products__c != 0){
                includeAuthoringServicesPage = true;  
            }
            List<quoteProductWrapper> allquoteProductWrap = new List<quoteProductWrapper>();
            List<PrintGroupWrapper> PrintGroupWrapperList = new List<PrintGroupWrapper>();
            
            //Create placeholder empty Lists
            List<quoteProductWrapper> emptyListProducts = new  List<quoteProductWrapper>();
            List<PrintGroupWrapper> emptyListPrintGroups = new  List<PrintGroupWrapper>();
            
            //build quoteWrapper
            quoteWrapper thisQuotewrap;          
            if(q.Id == quoteId){
                thisQuotewrap = new quoteWrapper(q, true, emptyListPrintGroups);  
            }else{
                thisQuotewrap = new quoteWrapper(q, false, emptyListPrintGroups); 
            }
            
            
            //build quote product wrappers - one per wrappper
            if(q.Quote_Products__r.size() > 0){
                for (Quote_Product__c qp: q.Quote_Products__r){
                    
                    if(qp.Bundled_Product__c == NULL){
                        thisQuotewrap.BundledTotalPriceY1 += proposalBuilderUtility.returnDisplayValue(qp.Y1_Total_with_Bundle__c, q.Currency_Rate__c);  
                        thisQuotewrap.BundledTotalPriceY2 += proposalBuilderUtility.returnDisplayValue(qp.Y2_Total_with_Bundle__c, q.Currency_Rate__c);
                        thisQuotewrap.BundledTotalPriceY3 += proposalBuilderUtility.returnDisplayValue(qp.Y3_Total_with_Bundle__c, q.Currency_Rate__c);
                        thisQuotewrap.BundledTotalPriceY4 += proposalBuilderUtility.returnDisplayValue(qp.Y4_Total_with_Bundle__c, q.Currency_Rate__c);
                        thisQuotewrap.BundledTotalPriceY5 += proposalBuilderUtility.returnDisplayValue(qp.Y5_Total_with_Bundle__c, q.Currency_Rate__c);
                    }
                    
                    //thisQuotewrap.BundledTotalPriceY1 += util.returnDisplayValue(qp.Y1_Total_Price__c,q.Currency_Rate__c);  
                    //thisQuotewrap.BundledTotalPriceY2 += util.returnDisplayValue(qp.Y2_Total_Price__c,q.Currency_Rate__c);  
                    //thisQuotewrap.BundledTotalPriceY3 += util.returnDisplayValue(qp.Y3_Total_Price__c,q.Currency_Rate__c);  
                    //thisQuotewrap.BundledTotalPriceY4 += util.returnDisplayValue(qp.Y4_Total_Price__c,q.Currency_Rate__c);  
                    //thisQuotewrap.BundledTotalPriceY5 += util.returnDisplayValue(qp.Y5_Total_Price__c,q.Currency_Rate__c);  

                    
                    if(qp.Hide_Quantity__c == false && allQuantitiesAreHidden){
                        allQuantitiesAreHidden = false;
                    }
                    if(qp.Combined_Indexing_Language__c != NULL && (qp.Bundled_Indexing_Display_Type__c == 'Combined' || qp.Auth_Project_Terms__c != NULL)){
                        thisQuotewrap.hasIndexFields = true;
                    }
                    //If quote contains HQ account, we will display a statement below table.
                    if(qp.Product_Print_Name__c.startsWithIgnoreCase('HQ')) {
                        thisQuotewrap.hasHQPdt = true;
                    } 
                    //check if it has any ergo products and check if contains leve 2 and level 3
                    if(qp.Product__r.Ergonomics__c){
                        thisQuotewrap.hasErgo = true;
                        //If the quote does not have Ergo Level 2 or 3, a new table displays after with Ergo 2 and 3
                        if(qp.Product_Print_Name__c.contains('Level 2')) {
                            thisQuotewrap.hasErgoLevel2 = true;
                        } 
                        if(qp.Product_Print_Name__c.contains('Level 3')) {
                            thisQuotewrap.hasErgoLevel3 = true;
                        }
                    } 
                    
                    //if the family list is empty add first family. Else, if this quote family is not already in the familyList
                    if(PrintGroupWrapperList.size() == 0){
                        PrintGroupWrapperList.add(new printGroupWrapper(emptyListProducts, qp.Print_Group__c));
                    }else{
                        integer count = 0;
                        for(printGroupWrapper pGwrap: PrintGroupWrapperList){
                            if(pGwrap.family == qp.Print_Group__c){
                                count++;
                            }
                        } 
                        if(count == 0){
                            PrintGroupWrapperList.add(new printGroupWrapper(emptyListProducts, qp.Print_Group__c));  
                        }
                    }
                    
                    //Prviosuly allquoteProductWrap.add(new quoteProductWrapper(qp)); 
                    if(qp.Product_Print_Grouping_Name__c != NULL){
                        string key = qp.Product__r.Product_Print_Grouping__c + '-' + thisQuotewrap.quote.id;
                        if(correlatedProductsMap.containsKey(key)){                            
                            correlatedProductsMap.get(key).add(new quoteProductWrapper(qp, null));
                        }else{                            
                            //make a new product wrapper and add this to the map
                            for(Product_Print_Grouping__c ppg: allPPGs){
                                if(ppg.id == qp.Product__r.Product_Print_Grouping__c){
                                    
                                    quoteProductWrapper spoof = new quoteProductWrapper(null, ppg);
                                    spoof.family = qp.Print_Group__c;
                                    allquoteProductWrap.add(spoof);                                    
                                    
                                    List<quoteProductWrapper> temp = new List<quoteProductWrapper>();
                                    temp.add(new quoteProductWrapper(qp, null));
                                    correlatedProductsMap.put(key, temp);
                                }
                            }
                            
                        }                        
                    }else{
                        allquoteProductWrap.add(new quoteProductWrapper(qp, null));  
                    }
                }
            }
            
            for(quoteProductWrapper w: allquoteProductWrap){
                if(w.ppg != NULL){
                    string key = w.ppg.id + '-' + thisQuotewrap.quote.id;                    
                    setGroupvalues(w, correlatedProductsMap.get(key));
                }      
            }
            
            //Query for level 2 and level 3 language if has ergo products but does not include level 2 or 3
            if(thisQuotewrap.hasErgo){
                if(thisQuotewrap.hasErgoLevel2 == false){
                    thisQuotewrap.level2Lang =[SELECT Proposal_Subpoint__c FROM Product2 WHERE Name = 'Ergonomics Level 2 Support - Remote' LIMIT 1].Proposal_Subpoint__c;
                }
                if(thisQuotewrap.hasErgoLevel3 == false){
                    thisQuotewrap.level3Lang =[SELECT Proposal_Subpoint__c FROM Product2 WHERE Name = 'Ergonomics Level 3 Support - On-Site' LIMIT 1].Proposal_Subpoint__c;
                }
            }
            
            for(PrintGroupWrapper PGWrap: PrintGroupWrapperList){
                List<quoteProductWrapper> tempquoteProductList = new List<quoteProductWrapper>();
                for(quoteProductWrapper QPWrap: allquoteProductWrap){                    
                    if(PGWrap.family == QPWrap.qProduct.Print_Group__c || PGWrap.family == QPWrap.family){
                        tempquoteProductList.add(QPWrap);
                        
                    }
                    PGWrap.quoteProductList = tempquoteProductList;
                }
                
                //PGWrap.quoteProductList[0].isFirst = true;
                
                thisQuotewrap.printGroupWrapperList.add(PGWrap);
            }
            
            quoteWrapperList.add(thisQuotewrap);
        }  
        quoteWrapperList.sort(); 
    }
    
    
    
    public void setGroupvalues(quoteProductWrapper parent, List<quoteProductWrapper> children){
        parent.ListPriceY1 = 0;
        parent.ListPriceY2 = 0;
        parent.ListPriceY3 = 0;
        parent.ListPriceY4 = 0;
        parent.ListPriceY5 = 0;
        parent.BundledTotalPriceY1 = 0;
        parent.BundledTotalPriceY2 = 0;
        parent.BundledTotalPriceY3 = 0;
        parent.BundledTotalPriceY4 = 0;
        parent.BundledTotalPriceY5 = 0;
        parent.CustomListPriceY1 = 0;
        parent.CustomListPriceY2 = 0;
        parent.CustomListPriceY3 = 0;
        parent.CustomListPriceY4 = 0;
        parent.CustomListPriceY5 = 0;       
        boolean bundled = true;
        boolean hasDiscount = false;
        boolean year1 = false;
        boolean year2 = false;
        boolean year3 = false;
        boolean year4 = false;
        boolean year5 = false;
        string priceDisplay = '';
        boolean canHideQuant = true;
        boolean isHidden = true;
        
        
        for(quoteProductWrapper child: children){
            
            if(!child.qproduct.product__r.Can_Hide_quantity__c){
                canHideQuant = false;
            }
            if(!child.qproduct.Hide_quantity__c){
                isHidden = false;
            }            
            
            if(child.qproduct.Bundled_Product__c != NULL){
                if(child.qproduct.Year_1__c){
                    year1 = true;  
                } 
                if(child.qproduct.Year_2__c){
                    year2 = true;  
                } 
                if(child.qproduct.Year_3__c){
                    year3 = true;  
                } 
                if(child.qproduct.Year_4__c){
                    year4 = true;  
                } 
                if(child.qproduct.Year_5__c){
                    year5 = true;  
                } 
                
                if(priceDisplay != NULL){
                    priceDisplay = child.qproduct.Pricing_Display__c;
                }                             
            }else{
                bundled = false;
                priceDisplay = child.qproduct.Pricing_Display__c;

                
                if(child.qproduct.Year_1__c){
                    year1 = true; 
                    priceDisplay = child.qproduct.Pricing_Display__c;
                    
                    parent.BundledTotalPriceY1 += child.BundledTotalPriceY1;
                    parent.CustomListPriceY1 += nullsToZero(child.qproduct.Y1_Custom_Proposal_List_Price__c);
                    if(child.qproduct.Quantity_Calculation__c){
                        parent.ListPriceY1 += (nullsToZero(child.qproduct.Y1_List_Price__c) * child.qproduct.Quantity__c);
                    }else{
                        parent.ListPriceY1 += nullsToZero(child.qproduct.Y1_List_Price__c); 
                    }
                    
                }
                if(child.qproduct.Year_2__c){
                    year2 = true;                    
                    parent.BundledTotalPriceY2 += child.BundledTotalPriceY2;
                    parent.CustomListPriceY2 += nullsToZero(child.qproduct.Y2_Custom_Proposal_List_Price__c);
                    if(child.qproduct.Quantity_Calculation__c){
                        parent.ListPriceY2 += (nullsToZero(child.qproduct.Y2_List_Price__c) * child.qproduct.Quantity__c);
                    }else{
                        parent.ListPriceY2 += nullsToZero(child.qproduct.Y2_List_Price__c); 
                    }
                }
                if(child.qproduct.Year_3__c){ 
                    year3= true;
                    parent.BundledTotalPriceY3 += child.BundledTotalPriceY3;
                    parent.CustomListPriceY3 += nullsToZero(child.qproduct.Y3_Custom_Proposal_List_Price__c);
                    if(child.qproduct.Quantity_Calculation__c){
                        parent.ListPriceY3 += (nullsToZero(child.qproduct.Y3_List_Price__c) * child.qproduct.Quantity__c);
                    }else{
                        parent.ListPriceY3 += nullsToZero(child.qproduct.Y3_List_Price__c); 
                    }
                }
                if(child.qproduct.Year_4__c){ 
                    year4 = true;
                    parent.BundledTotalPriceY4 += child.BundledTotalPriceY4;
                    parent.CustomListPriceY4 += nullsToZero(child.qproduct.Y4_Custom_Proposal_List_Price__c);
                    if(child.qproduct.Quantity_Calculation__c){
                        parent.ListPriceY4 += (nullsToZero(child.qproduct.Y4_List_Price__c) * child.qproduct.Quantity__c);
                    }else{
                        parent.ListPriceY4 += nullsToZero(child.qproduct.Y4_List_Price__c); 
                    }
                }
                if(child.qproduct.Year_5__c){      
                    year5 = true;
                    parent.BundledTotalPriceY5 += child.BundledTotalPriceY5;
                    parent.CustomListPriceY5 += nullsToZero(child.qproduct.Y5_Custom_Proposal_List_Price__c);
                    if(child.qproduct.Quantity_Calculation__c){
                        parent.ListPriceY5 += (nullsToZero(child.qproduct.Y5_List_Price__c) * child.qproduct.Quantity__c);
                    }else{
                        parent.ListPriceY5 += nullsToZero(child.qproduct.Y5_List_Price__c); 
                    }
                }
            }
        }
        
        parent.isBundled = bundled;
        parent.canHideQuantity = canHideQuant;       
        parent.quantityIsHidden = isHidden;
        
        parent.includesYear1 = year1;
        parent.includesYear2 = year2;
        parent.includesYear3 = year3;
        parent.includesYear4 = year4;
        parent.includesYear5 = year5;
        
        if(parent.BundledTotalPriceY1 < parent.ListPriceY1 ||
           parent.BundledTotalPriceY2 < parent.ListPriceY2 ||
           parent.BundledTotalPriceY3 < parent.ListPriceY3 ||
           parent.BundledTotalPriceY4 < parent.ListPriceY4 ||
           parent.BundledTotalPriceY5 < parent.ListPriceY5){
               parent.isDiscounted = true;
           }else{
               parent.isDiscounted = false;
           }
        parent.displayOpts = priceDisplay;
        
        
    }
    
    
    public decimal nullsToZero(decimal price){
        if(price == null){
            return 0;
        }else{
            return price;
        }        
    }
    
    
    
    //wrapper of quotes
    public class quoteWrapper implements Comparable{
        
        public quote__c quote {get;set;}
        public boolean isMainQuote{get;set;}
        public List<printGroupWrapper> printGroupWrapperList {get;set;}
        public integer contractLength {get;set;}
        public integer sortOrder {get;set;}
        public string Name {get;set;}
        public boolean hasIndexFields {get;set;}
        public boolean hasHQPdt {get;set;}
        public boolean hasErgo {get;set;}
        public boolean hasErgoLevel2 {get;set;}
        public boolean hasErgoLevel3 {get;set;}
        public string level2Lang {get;set;}
        public string level3Lang {get;set;}
        public decimal BundledTotalPriceY1 {get;set;}
        public decimal BundledTotalPriceY2 {get;set;}
        public decimal BundledTotalPriceY3 {get;set;}
        public decimal BundledTotalPriceY4 {get;set;}
        public decimal BundledTotalPriceY5 {get;set;}
        public string family {get;set;}
        
        public quoteWrapper(quote__c xQuote, boolean xIsMainQuote, List<printGroupWrapper> xprintGroupWrapperList){
            BundledTotalPriceY1 = 0;
            BundledTotalPriceY2 = 0;
            BundledTotalPriceY3 = 0;
            BundledTotalPriceY4 = 0;
            BundledTotalPriceY5 = 0;

            hasHQPdt = false;
            hasErgo  = false;
            hasErgoLevel2 = false;
            hasErgoLevel3 = false;
            printGroupWrapperList = xprintGroupWrapperList;
            quote = xQuote;
            isMainQuote = xIsMainQuote;
            if(isMainQuote == true){
                sortOrder = 0;
            }else{
                sortOrder = 1;
            }
            Name = xQuote.Name; 
            //convert contract length to number
            if(quote.Contract_Length__c == '5 Years'){
                contractLength = 5;
            }
            if(quote.Contract_Length__c == '4 Years'){
                contractLength = 4;
            }
            if(quote.Contract_Length__c == '3 Years'){
                contractLength = 3;
            }
            if(quote.Contract_Length__c == '2 Years'){
                contractLength = 2;
            }
            if(quote.Contract_Length__c == '1 Years'){
                contractLength = 1;
            }
        }
        
        public Integer compareTo(Object compareTo) {
            if(sortOrder == NULL){
                return name.compareTo(((quoteWrapper)compareTo).name); 
            }else{
                Integer returnValue = 0;
                if (sortOrder > ((quoteWrapper)compareTo).sortOrder) {
                    returnValue = 1;
                }
                if (sortOrder < ((quoteWrapper)compareTo).sortOrder){
                    returnValue = -1;
                }
                return returnValue; 
            }
        }
    }
    
    
    //wrapper for each Print Group which holds a list of quoteProductWrappers
    public class printGroupWrapper{
        public string family {get;set;}
        public List<quoteProductWrapper> quoteProductList {get;set;}
        
        public printGroupWrapper(List<quoteProductWrapper> xQuotePdts, string xFamily){
            quoteProductList = xQuotePdts;
            family = xFamily;
        }        
    }
    
    //every quote product gets a wrapper which contains main product data and a list of any correlated products
    public class quoteProductWrapper{ 
        public Quote_Product__c qProduct {get;set;}
        public boolean isChild {get;set;}
        public boolean isFirst {get;set;}
        public integer indexField {get;set;}
        
        
        //variables for group parents
        public Product_Print_Grouping__c ppg {get;set;}
        public decimal ListPriceY1 {get;set;}
        public decimal ListPriceY2 {get;set;}
        public decimal ListPriceY3 {get;set;}
        public decimal ListPriceY4 {get;set;}
        public decimal ListPriceY5 {get;set;}
        public decimal BundledTotalPriceY1 {get;set;}
        public decimal BundledTotalPriceY2 {get;set;}
        public decimal BundledTotalPriceY3 {get;set;}
        public decimal BundledTotalPriceY4 {get;set;}
        public decimal BundledTotalPriceY5 {get;set;}
        public decimal CustomListPriceY1 {get;set;}
        public decimal CustomListPriceY2 {get;set;}
        public decimal CustomListPriceY3 {get;set;}
        public decimal CustomListPriceY4 {get;set;}
        public decimal CustomListPriceY5 {get;set;}
        //public string showInputYear {get;set;}
        public boolean includesYear1 {get;set;}
        public boolean includesYear2 {get;set;}
        public boolean includesYear3 {get;set;}
        public boolean includesYear4 {get;set;}
        public boolean includesYear5 {get;set;}
        public decimal year1Price {get;set;}
        public decimal year2Price {get;set;}
        public decimal year3Price {get;set;}
        public decimal year4Price {get;set;}
        public decimal year5Price {get;set;}
        public boolean isDiscounted {get;set;}
        public boolean isBundled {get;set;}
        public string family {get;set;}
        public boolean canHideQuantity {get;set;}
        public boolean quantityIsHidden {get;set;}
        public id identifier {get;set;}
        public List<quoteProductWrapper> children {get;set;}
        public string displayOpts {get;set;}
        
        public quoteProductWrapper(Quote_Product__c xqProduct, Product_Print_Grouping__c xppg ){
            if(xppg != NULL){
                ppg = xppg; 
                identifier = xppg.id;                
                ppg = xppg;                
                year1Price = 1;
                year2Price = 2;
                year3Price = 3;
                year4Price = 4;
                year4Price = 5;
                ListPriceY1 = 0;
                ListPriceY2 = 0;
                ListPriceY3 = 0;
                ListPriceY4 = 0;
                ListPriceY5 = 0;
                BundledTotalPriceY1 = 0;
                BundledTotalPriceY2 = 0;
                BundledTotalPriceY3 = 0;
                BundledTotalPriceY4 = 0;
                BundledTotalPriceY5 = 0;
                CustomListPriceY1 = 0;
                CustomListPriceY2 = 0;
                CustomListPriceY3 = 0;
                CustomListPriceY4 = 0;
                CustomListPriceY5 = 0;
                isDiscounted = false;
                //isGroupingParent = TRUE;
            }else{   




                qProduct = xqProduct;
                if(xqProduct.Group_ID__c == NULL){
                    isChild = false;
                }else{
                    isChild = true;
                }
                if(xqProduct.Group_ID__c == NULL && xqProduct.Group_Parent_ID__c != NULL && xqProduct.Bundled_Indexing_Display_Type__c == 'Combined' ||  xqProduct.Auth_Project_Terms__c != NULL){
                    indexField = integer.valueOf(xqProduct.Combined_Indexing_Language__c.Left(1));
                }
                
                if (xqProduct.Proposal_Subpoint__c == null) {
                    if (xqProduct.Product__r.Proposal_Subpoint__c != null) {
                        qProduct.Proposal_Subpoint__c = xqProduct.Product__r.Proposal_Subpoint__c;
                    }
                }
                if(qProduct.Proposal_Subpoint__c != NULL){
                    string thestring = qProduct.Proposal_Subpoint__c;
                    qProduct.Proposal_Subpoint__c = thestring.replace('{!QUANTITY}', string.valueOf(qProduct.Quantity__c));
                }

                BundledTotalPriceY1 = proposalBuilderUtility.returnDisplayValue(qproduct.Y1_Total_with_Bundle__c, qproduct.quote__r.Currency_Rate__c);
                BundledTotalPriceY2 = proposalBuilderUtility.returnDisplayValue(qproduct.Y2_Total_with_Bundle__c, qproduct.quote__r.Currency_Rate__c);
                BundledTotalPriceY3 = proposalBuilderUtility.returnDisplayValue(qproduct.Y3_Total_with_Bundle__c, qproduct.quote__r.Currency_Rate__c);
                BundledTotalPriceY4 = proposalBuilderUtility.returnDisplayValue(qproduct.Y4_Total_with_Bundle__c, qproduct.quote__r.Currency_Rate__c);
                BundledTotalPriceY5 = proposalBuilderUtility.returnDisplayValue(qproduct.Y5_Total_with_Bundle__c, qproduct.quote__r.Currency_Rate__c);


                
            } 
        }
    }
}