public class revRecTransactionTriggerHandler {

	// AFTER INSERT
    public static void afterInsert(Revenue_Recognition_Transaction__c[] oldRevRecs, Revenue_Recognition_Transaction__c[] newRevRecs) {
    }
    
    // AFTER UPDATE
    public static void afterUpdate(Revenue_Recognition_Transaction__c[] oldRevRecs, Revenue_Recognition_Transaction__c[] newRevRecs) {
        Revenue_Recognition_Transaction__c[] forceDateChangesOld = new List<Revenue_Recognition_Transaction__c>();
        Revenue_Recognition_Transaction__c[] forceDateChangesNew = new List<Revenue_Recognition_Transaction__c>();
        Revenue_Recognition_Transaction__c[] recognizedTransactions = new List<Revenue_Recognition_Transaction__c>();
        
        for (integer i=0; i<newRevRecs.size(); i++) {
            if (newRevRecs[i].Recognition_Date__c != null && oldRevRecs[i].Recognition_Date__c == null) {
                recognizedTransactions.add(newRevRecs[i]);
            }
            if (newRevRecs[i].Force_Subsequent_Expected_Date_Change__c == true && oldRevRecs[i].Force_Subsequent_Expected_Date_Change__c == false) {
                forceDateChangesOld.add(oldRevRecs[i]);
                forceDateChangesNew.add(newRevRecs[i]);
            }
        }
        
        //If transactions were recognized - reforecast future dates based on recognition date
        if(recognizedTransactions.size() > 0){
            revenueRecognitionUtility.updateSubsequentDatesOnRecognition(recognizedTransactions);
        }
        
        //If subsequent date changes are needed
        if(forceDateChangesNew.size() > 0){
            revenueRecognitionUtility.updateSubsequentDates(forceDateChangesOld, forceDateChangesNew);
        }
    }
}