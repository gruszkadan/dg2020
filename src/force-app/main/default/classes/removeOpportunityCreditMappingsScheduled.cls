public class removeOpportunityCreditMappingsScheduled implements Schedulable{
    public void execute(SchedulableContext SC) {
        //Query for any mappings expiring prior to today
        Sales_Incentive_Setting__c[] opportunityCreditMappings = [SELECT id,Incentive_Stop_Date__c
                                                                  FROM Sales_Incentive_Setting__c
                                                                  WHERE Incentive_Stop_Date__c != null 
                                                                  AND Incentive_Stop_Date__c < :date.today()
                                                                  AND RecordType.Name = 'Mid-Market Opportunity'];
        
        //Delete List
        Sales_Incentive_Setting__c[] mappingsToDelete = new list <Sales_Incentive_Setting__c>();
        
        //Find Holidays
        Holiday[] holidays = [SELECT ActivityDate FROM Holiday];
        
        //Loop over any found mappings to check if they are eligible to be deleted
        for(Sales_Incentive_Setting__c sis:opportunityCreditMappings){
            //If the incentive stop date + 6 business days is less tan today then we can delete
            if(dateUtility.calculateBusinessDay(sis.Incentive_Stop_Date__c.toStartOfMonth().addMonths(1).addDays(-1), 6, holidays) < date.today()){
                mappingsToDelete.add(sis);
            }
        }
        
        //Delete any eligible mappings
        if(mappingsToDelete.size() > 0){
            delete mappingsToDelete;
        }
        
    }
}