<apex:page id="proposalHQProductDetail" showHeader="FALSE" sidebar="FALSE" renderAS="PDF" standardController="Quote__c" standardStylesheets="false">
    <apex:stylesheet value="{!URLFOR($Resource.proposalPrintResource, 'default.css')}"/>
    <body style="page-break-after:always;" class="customerCare">
        <div class="header">
            <h1>MSDS Fax-Back &amp; Medical Support</h1>
            <p>
                Our 24/7 MSDS Fax-Back &amp; Medical Support hotline ensures your employees are never without access to your company’s inventory of safety data sheets or emergency support.
            </p>
        </div>
        <div class="sidebar">
            <div class="content">
                <div class="logo">
                    <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/logo-msdsonline.png')}" styleClass="msdsonlineLogo" width="234" height="59" />        
                </div>
                <apex:image value="{!URLFOR($Resource.proposalPrintResource, '/images/faxback-sidebar.jpg')}" styleClass="customerCarePhoto" width="319" height="179" />        
                <h2>Benefits</h2>
                <p>
                    <span class="careFeature">Around-the-Clock Employee Access</span><br/>
                    Our Fax-Back Service is available to workplaces around the world with translators on-hand 24 hours a day, 7 days a week, assuring that your employees always have critical safety information at their fingertips. 
                </p>
                <p>
                    <span class="careFeature">Simple to Use</span><br/>
                    Your staff can call our hotline day or night for fax access to your company’s library of safety data sheets and medical support for chemical exposure incidents.
                </p>
                <p>
                    <span class="careFeature">Regulatory Compliant</span><br/>
                    As a component of your MSDSonline account, our Fax-Back Service meets OSHA requirements for electronic safety data sheet management.
                </p>
                <p>
                    <span class="careFeature">Full Right-to-Know Compliance</span><br/>
                    When your employees have limited access to the Internet, or even a computer, your safety data sheets are still readily available through our hotline. 
                </p>
                <p>
                    <span class="careFeature">Excellent Customer Care</span><br/>
                    Our dedicated support team takes pride in your satisfaction. You get ongoing access to implementation assistance, general and technical support, product training, and more!
                </p>
            </div>
        </div>    
        <div class="pageContent">
            <p>
                <span class="careFeature">Around-the-Clock Safety Data Sheet &amp; Emergency Exposure Support</span><br/> 
                With the MSDS Fax-Back Service from MSDSonline, whether you have domestic or global fax-back needs, your employees are never without access to your company’s inventory of safety data sheets. Employees can quickly receive an MSDS while working in the field or in areas with limited Internet or computer access by simply calling our hotline.
            </p> 
            <p>
                <span class="careFeature">24/7 Access to Your MSDSs</span><br/> 
                Our Fax-Back Service is available around-the-clock, 365 days a year to facilities around the world. That means no matter the time of day, your employees will always have access to safety information through a convenient hotline. In addition, a dedicated team of specialists, including translators, registered nurses, emergency medical technicians, toxicologists, paramedics and pharmacists are on call to provide chemical exposure support if necessary.
            </p> 
            <p>
                <span class="careFeature">Compliant Backup</span><br/> 
                Our MSDS Fax-Back Service is an effective back-up option for MSDS deployment. Ensuring compliance with OSHA Hazard Communication and right-to-know requirements, our automated fax technology works seamlessly with your MSDSonline account to deliver fax access to your company’s site-specific database of safety data sheets. What’s more, if the requested MSDS is not in your library, our team of dedicated specialists will search the extensive MSDSonline database, which contains millions of MSDSs, to find the specific document of interest.
            </p>
            <p>
                <span class="careFeature">Safety Is Our Top Priority</span><br/> 
                At MSDSonline, our mission is to provide easy-to-use solutions that help you deliver safety information to your employees in a timely and cost-effective manner. In fact, we’ve designed all of our products and services with your employees’ safety in mind. So, while we can’t prevent accidents from happening, we can help improve workplace safety by giving you the tools to make critical information accessible to all employees – all the time.
            </p>
        </div>
        <div class="footer">
            Proprietary &amp; Confidential. &copy; Copyright MSDSonline Inc., DBA <em>VelocityEHS</em>. Do not distribute without authorized consent.
            <div class="pageNumber">
                Page <span class="pagenumber"/> of <span class="pagecount"/>
            </div>
        </div>
    </body>
</apex:page>