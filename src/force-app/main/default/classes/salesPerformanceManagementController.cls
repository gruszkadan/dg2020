public class salesPerformanceManagementController {
    public string userId {get;set;}
    public month[] wrappers {get;set;}
    public User rep {get;set;}
    public User mgr {get;set;}
    public User dir {get;set;}
    public User vp {get;set;}
    
    public salesPerformanceManagementController() {
        wrappers = new list<month>();
        userId = '0053400000966TqAAI';
        find();
    }
    
    public SelectOption[] getuserList() {
        SelectOption[] ops = new list<SelectOption>();
        ops.add(new SelectOption('', '-Select-'));
        for (User u : [SELECT id, Name
                       FROM User
                       WHERE ForecastEnabled = true
                       ORDER BY Name]) {
                           ops.add(new SelectOption(u.id, u.Name));
                       }
        return ops;
    }
    
    public void find() {
        rep = [SELECT id, FirstName, LastName, Role__c, Department__c, Group__c, Sales_Team__c, Sales_Performance_Manager_ID__c, Sales_Director__c, Sales_VP__c
               FROM User
               WHERE id = : userId LIMIT 1];
        if (rep.id != null) {
            mgr = [SELECT id, Name
                   FROM User
                   WHERE id = : rep.Sales_Performance_Manager_ID__c];
            dir = [SELECT id, Name
                   FROM User
                   WHERE id = : rep.Sales_Director__c];
            vp = [SELECT id, Name
                  FROM User
                  WHERE id = : rep.Sales_VP__c];
            
            Sales_Performance_Summary__c[] SPSs = [SELECT id, Name, OwnerId, Month__c, Month_Number__c, My_Bookings__c, Total_Bookings__c
                                                   FROM Sales_Performance_Summary__c
                                                   WHERE OwnerId = : rep.id
                                                   AND Year__c = '2017'
                                                   ORDER BY Month_Number__c ASC];
            
            Sales_Performance_Item__c[] SPIs = [SELECT id, Order_Item_ID__c, Booking_Amount__c, Month__c, Month_Number__c, Sales_Rep_Sales_Performance_Summary__c, Sales_Rep_Sales_Performance_Summary__r.Name,
                                                Manager_Sales_Performance_Summary__c, Manager_Sales_Performance_Summary__r.Name,
                                                Director_Sales_Performance_Summary__c, Director_Sales_Performance_Summary__r.Name,
                                                VP_Sales_Performance_Summary__c, VP_Sales_Performance_Summary__r.Name
                                                FROM Sales_Performance_Item__c
                                                WHERE OwnerId = : rep.id
                                                AND Year__c = '2017'
                                                ORDER BY Order_Item_ID__c ASC];
            
            Order_Item__c[] OIs = [SELECT id, Order_Item_ID__c, Invoice_Amount__c, Month__c, Month_Number__c
                                   FROM Order_Item__c
                                   WHERE OwnerId = : rep.id
                                   AND isRenewal__c = false
                                   AND Year__c = '2017'
                                   ORDER BY Order_Item_ID__c ASC];
            
            for (integer i=1; i<=12; i++) {
                Sales_Performance_Summary__c monthSPS;
                Sales_Performance_Item__c[] monthSPIs = new list<Sales_Performance_Item__c>();
                Order_Item__c[] monthOIs = new list<Order_Item__c>();
                for (Sales_Performance_Summary__c sps : SPSs) {
                    if (sps.Month_Number__c == i) {
                        monthSPS = sps;
                    }
                }
                for (Sales_Performance_Item__c spi : SPIs) {
                    if (spi.Month_Number__c == i) {
                        monthSPIs.add(spi);
                    }
                }
                for (Order_Item__c oi : OIs) {
                    if (oi.Month_Number__c == i) {
                        monthOIs.add(oi);
                    }
                }
                
                wrappers.add(new month(i, monthSPS, monthSPIs, monthOIs));
            }
        } 
    }
    
    public class month {
        public string month {get;set;}
        public Sales_Performance_Summary__c sps {get;set;}
        public Sales_Performance_Item__c[] spis {get;set;}
        public Order_Item__c[] ois {get;set;}
        
        public month(integer i, Sales_Performance_Summary__c xsps, Sales_Performance_Item__c[] xspis, Order_Item__c[] xois) {
            findMonth(i);
            sps = xsps;
            spis = xspis;
            ois = xois;
        }
         
        public void findMonth(integer i) {
            if (i == 1) {
                month = 'January';
            } else if (i == 2) {
                month = 'February';
            } else if (i == 3) {
                month = 'March';
            } else if (i == 4) {
                month = 'April';
            } else if (i == 5) {
                month = 'May';
            } else if (i == 6) {
                month = 'June';
            } else if (i == 7) {
                month = 'July';
            } else if (i == 8) {
                month = 'August';
            } else if (i == 9) {
                month = 'September';
            } else if (i == 10) {
                month = 'October';
            } else if (i == 11) {
                month = 'November';
            } else {
                month = 'December';
            }
        }
    }
}