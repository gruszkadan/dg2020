@isTest(seeAllData=true)
private class testAuthoringDeliveryEmail {
    static testmethod void test1() {
         //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id, email='test@testvehs.com');
        insert newCon;
        
        //test case
        Case newCase = new Case();
        newCase.AccountId = newAcct.id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Type = 'Authoring Services';
        newCase.Subject = 'Test';
        newCase.Num_of_Case_Issues__c = 1;
        newCase.Origin = 'Email';
        newCase.OwnerID = [SELECT id
                           FROM User 
                           WHERE LastName = 'Gruszka' 
                           LIMIT 1].id;  
        insert newCase;
        
        //test attachments
        Attachment testAttachment = new Attachment();   	
    	testAttachment.Name='TestAttachment.docx';
    	Blob bodyBlob=Blob.valueOf('Test Attachment Body');
    	testAttachment.body=bodyBlob;
        
        Attachment testAttachment2 = new Attachment();   	
        testAttachment2.Name='TestAttachment.csv';
        Blob bodyBlob2=Blob.valueOf('Test Attachment Body');
        testAttachment2.body=bodyBlob2;
        
        ApexPages.currentPage().getParameters().put('contactid', newCon.Id);
        ApexPages.currentPage().getParameters().put('caseid', newCase.Id);
        authoringDeliveryEmail myController = new authoringDeliveryEmail();
        //Assert that the contactid is being set correctly
        SYSTEM.assert(MyController.ourContact.id == newCon.id, 'Expected '+ newCon.id+' Found '+MyController.ourContact.id);
        //Assert that the ParentId is being set correctly
        SYSTEM.assert(MyController.emailMsg.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.emailMsg.ParentId);
        //Assert that the contact email is being queried/set correctly
        SYSTEM.assert(MyController.ourContact.email == newCon.email, 'Expected '+ newCon.email+' Found '+MyController.ourContact.email);
        
        myController.ourAttachment.Name = testAttachment.Name;
        myController.ourAttachment.Body = testAttachment.Body;
        myController.ourAttachment2.Name = testAttachment2.Name;
        myController.ourAttachment2.Body = testAttachment2.Body;
        myController.ourAttachment3.Name = testAttachment.Name;
        myController.ourAttachment3.Body = testAttachment.Body;
        myController.ourAttachment4.Name = testAttachment2.Name;
        myController.ourAttachment4.Body = testAttachment2.Body;
        myController.ourAttachment5.Name = testAttachment.Name;
        myController.ourAttachment5.Body = testAttachment.Body;
        myController.addAttachment();
        //Assert that the attachments are getting the parentIds set correctly
        SYSTEM.assert(MyController.ourAttachment.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.ourAttachment.ParentId);
        SYSTEM.assert(MyController.ourAttachment2.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.ourAttachment2.ParentId);
        SYSTEM.assert(MyController.ourAttachment3.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.ourAttachment3.ParentId);
        SYSTEM.assert(MyController.ourAttachment4.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.ourAttachment4.ParentId);
        SYSTEM.assert(MyController.ourAttachment5.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.ourAttachment5.ParentId);
        
        attachment[] att = [SELECT id,Body,Name,ParentId from attachment where parentId =: newCase.id];
        SYSTEM.assert(att.size() == 5, 'Expected '+ 5 +' Found '+att.size());
        MyController.attachmentList = att;
        id attId =  myController.attachmentList[3].id;
        ApexPages.currentPage().getParameters().put('aid', attId);
        myController.removeAttachment();
        //assert that the correct record got deleted
        att = [SELECT id,Body,Name,ParentId from attachment where parentId =: newCase.id];
        MyController.attachmentList = att;
        boolean deleted = true;
        for(attachment a:myController.attachmentList){
            if(a.id == attId){
                deleted = false;
            }
        }
        SYSTEM.assert(deleted == true, 'Expected true Found '+deleted);
        
        myController.addlTo = 'test2@testvehs.com,test3@testvehs.com';
        myController.emailMsg.CcAddress ='testcc@testvehs.com';
        myController.emailMsg.Subject = 'Test Subject';
        myController.emailMsg.Body__c = '<b>Test Body</b>';
        myController.emailMsg.Num_of_Docs__c = 10;
        myController.ourAttachment = new attachment();
        myController.ourAttachment2 = new attachment();
        myController.ourAttachment3 = new attachment();
        myController.ourAttachment4 = new attachment();
        myController.ourAttachment5 = new attachment();
        myController.sendEmail();
        //Assert that the html and text body got copied over correctly from the custom field
        SYSTEM.assert(MyController.emailMsg.HtmlBody == myController.emailMsg.Body__c, 'Expected '+myController.emailMsg.Body__c+'Found '+MyController.emailMsg.HtmlBody);
        SYSTEM.assert(MyController.emailMsg.TextBody == 'Test Body', 'Expected Test Body Found '+MyController.emailMsg.TextBody);
        //Assert that the attachments got deleted off the case
        Attachment[] att2 = [SELECT id,Body,Name,ParentId from attachment where parentId =: newCase.id];
        SYSTEM.assert(att2.size() == 0, 'Expected '+ 0 +' Found '+att2.size());
        //Assert that the attachments got added on the EmailMessage
        Attachment[] att3 = [SELECT id,Body,Name,ParentId from attachment where parentId =: myController.emailMsg.id];
        SYSTEM.assert(att3.size() == 4, 'Expected '+ 4 +' Found '+att3.size());
        

    }
    static testmethod void test2() {
      //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id, email='test@testvehs.com');
        insert newCon;
        
        //test case
        Case newCase = new Case();
        newCase.AccountId = newAcct.id;
        newCase.ContactId = newCon.Id;
        newCase.Status = 'Active';
        newCase.Type = 'Authoring Services';
        newCase.Subject = 'Test';
        newCase.Num_of_Case_Issues__c = 1;
        newCase.Origin = 'Email';
        newCase.OwnerID = [SELECT id
                           FROM User 
                           WHERE LastName = 'Gruszka' 
                           LIMIT 1].id;  
        insert newCase;
        
        //test attachments
        Attachment testAttachment = new Attachment();   	
    	testAttachment.Name='TestAttachment.docx';
    	Blob bodyBlob=Blob.valueOf('Test Attachment Body');
    	testAttachment.body=bodyBlob;
        
        Attachment testAttachment2 = new Attachment();   	
        testAttachment2.Name='TestAttachment.csv';
        Blob bodyBlob2=Blob.valueOf('Test Attachment Body');
        testAttachment2.body=bodyBlob2;
        
        ApexPages.currentPage().getParameters().put('contactid', newCon.Id);
        ApexPages.currentPage().getParameters().put('caseid', newCase.Id);
        authoringDeliveryEmail myController = new authoringDeliveryEmail();
        //Assert that the contactid is being set correctly
        SYSTEM.assert(MyController.ourContact.id == newCon.id, 'Expected '+ newCon.id+' Found '+MyController.ourContact.id);
        //Assert that the ParentId is being set correctly
        SYSTEM.assert(MyController.emailMsg.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.emailMsg.ParentId);
        //Assert that the contact email is being queried/set correctly
        SYSTEM.assert(MyController.ourContact.email == newCon.email, 'Expected '+ newCon.email+' Found '+MyController.ourContact.email);
        
        myController.ourAttachment.Name = testAttachment.Name;
        myController.ourAttachment.Body = testAttachment.Body;
        myController.ourAttachment2.Name = testAttachment2.Name;
        myController.ourAttachment2.Body = testAttachment2.Body;
        myController.ourAttachment3.Name = testAttachment.Name;
        myController.ourAttachment3.Body = testAttachment.Body;
        myController.ourAttachment4.Name = testAttachment2.Name;
        myController.ourAttachment4.Body = testAttachment2.Body;
        myController.ourAttachment5.Name = testAttachment.Name;
        myController.ourAttachment5.Body = testAttachment.Body;
        myController.addAttachment();
        //Assert that the attachments are getting the parentIds set correctly
        SYSTEM.assert(MyController.ourAttachment.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.ourAttachment.ParentId);
        SYSTEM.assert(MyController.ourAttachment2.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.ourAttachment2.ParentId);
        SYSTEM.assert(MyController.ourAttachment3.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.ourAttachment3.ParentId);
        SYSTEM.assert(MyController.ourAttachment4.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.ourAttachment4.ParentId);
        SYSTEM.assert(MyController.ourAttachment5.ParentId == newCase.id, 'Expected '+ newCase.id+' Found '+MyController.ourAttachment5.ParentId);
        
        attachment[] att = [SELECT id,Body,Name,ParentId from attachment where parentId =: newCase.id];
        SYSTEM.assert(att.size() == 5, 'Expected '+ 5 +' Found '+att.size());
        MyController.attachmentList = att;
        myController.sendEmail();
        myController.cancel();
        //Assert that the attachments got deleted when cancelling
        att = [SELECT id,Body,Name,ParentId from attachment where parentId =: newCase.id];
        SYSTEM.assert(att.size() == 0, 'Expected '+ 0 +' Found '+att.size());
    }
    
}