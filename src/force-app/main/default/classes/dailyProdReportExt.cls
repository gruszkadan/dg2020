global with sharing class dailyProdReportExt {
    private ApexPages.StandardController controller {get; set;}
    public User u {get;set;}
    public User usr {get;set;}
    public Productivity_Report__c[] pr {get;set;}
    public Map<String, List<Productivity_Report__c>> mymap1 {get;set;}
    public String soql {get;set;}
    public Decimal[] totallist {get;set;}
    public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }
    
    public dailyProdReportExt(ApexPages.StandardController controller) {
        this.controller = controller; 
        string month = string.valueof(datetime.now().format('MMM'));
        string mtd = month+'_MTD_Bookings__c';
        usr = [ SELECT Id, Name, Role__c, Sales_Manager__c, Sales_Director__c, LastName FROM User WHERE id = :UserInfo.getUserId() LIMIT 1 ];
        soql = 'SELECT Id, Name, Run_Date__c, Sales_Director__c, Rep_Department__c, Rep_Name__c, Rep_Sales_Manager__c, Rep_Role__c, Percent_Completed_Demos__c, Percent_Scheduled_Demos__c, Percent_Completed_Tasks__c, Percent_Initial_Contacts__c, Actual_Completed_Tasks_1__c, Goals_Completed_Tasks_1__c, Actual_Initial_Contacts_1__c, Goals_Initial_Contacts_1__c, Actual_Scheduled_Demos_1__c, Goals_Scheduled_Demos_1__c, Actual_Completed_Demos_1__c, Goals_Completed_Demos_1__c, Actual_Created_Licensing__c, Sales_Team__c FROM Productivity_Report__c WHERE Run_Date__c = TODAY';
        
        if (usr.Role__c == 'Salesforce Admin') {
            u = [SELECT Id, Name, Role__c, Sales_Manager__c, Sales_Director__c, LastName FROM User WHERE Name = :ApexPages.currentPage().getParameters().get('u').replace('0', ' ') limit 1];
        } else {
            u = [SELECT Id, Name, Role__c, Sales_Manager__c, Sales_Director__c, LastName FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
        }
        
        if (u.Role__c == 'Director' || u.Role__c == 'Sales Manager') {
            soql += ' ORDER BY User_LU__r.FirstName ASC';
        } else {
            soql += ' AND Rep_Sales_Manager__c = \''+String.escapeSingleQuotes(u.Sales_Manager__c)+'\' ORDER BY User_LU__r.FirstName ASC';
        }
        Productivity_Report__c[] soqlq = Database.query(soql);
        String[] salesteams = new List<String>();
        User[] usrs = [ SELECT Id, Sales_Team__c FROM User WHERE isActive = TRUE AND Role__c = 'Sales Manager' ORDER BY Sales_Team__c ASC ];
        for (User usr : usrs) {
            salesteams.add(usr.Sales_Team__c);
        }
        mymap1 = new Map<String, List<Productivity_Report__c>>();
        Productivity_Report__c[] teamlist = new List<Productivity_Report__c>(); 
        for (Productivity_Report__c p : soqlq) {
            if (!mymap1.containsKey(p.Sales_Team__c)) {
                List<Productivity_Report__c> prteams = [SELECT Id, Name, Run_Date__c, Rep_Department__c, Rep_Name__c, Rep_Sales_Manager__c, Rep_Role__c, 
                                                        Percent_Completed_Demos__c, Percent_Scheduled_Demos__c, Percent_Completed_Tasks__c, Percent_Initial_Contacts__c, 
                                                        Actual_Completed_Tasks_1__c, Goals_Completed_Tasks_1__c, Actual_Initial_Contacts_1__c, Goals_Initial_Contacts_1__c, 
                                                        Actual_Scheduled_Demos_1__c, Goals_Scheduled_Demos_1__c, Actual_Completed_Demos_1__c, Goals_Completed_Demos_1__c, 
                                                        Actual_Created_Licensing__c, Totals_Actual_Completed_Tasks__c, Totals_Goals_Completed_Tasks__c, Totals_Percent_Completed_Tasks_1__c,
                                                        Totals_Actual_Initial_Contacts__c, Totals_Goals_Initial_Contacts__c, Totals_Percent_Initial_Contacts_1__c,
                                                        Totals_Actual_Scheduled_Demos__c, Totals_Goals_Scheduled_Demos__c, Totals_Percent_Scheduled_Demos_1__c,
                                                        Totals_Actual_Completed_Demos__c, Totals_Goals_Completed_Demos__c, Totals_Percent_Completed_Demos_1__c,
                                                        Totals_Actual_Created_Licensing__c, Sales_Team__c
                                                        FROM Productivity_Report__c WHERE Run_Date__c = TODAY AND Sales_Team__c = :p.Sales_Team__c ORDER BY Rep_Name__c ASC];
                teamlist.add(prteams[0]);
                mymap1.put(p.Sales_Team__c, prteams);
            }
        }
        Decimal tot_a_ct = 0;
        Decimal tot_g_ct = 0;
        Decimal tot_a_ic = 0;
        Decimal tot_g_ic = 0;
        Decimal tot_a_sd = 0;
        Decimal tot_g_sd = 0;
        Decimal tot_a_cd = 0;
        Decimal tot_g_cd = 0;
        Decimal tot_a_cl = 0;
        totallist = new List<Decimal>();
        for (Productivity_Report__c tt : teamlist) {
            tot_a_ct = tot_a_ct+tt.Totals_Actual_Completed_Tasks__c;
            tot_g_ct = tot_g_ct+tt.Totals_Goals_Completed_Tasks__c;
            tot_a_ic = tot_a_ic+tt.Totals_Actual_Initial_Contacts__c;
            tot_g_ic = tot_g_ic+tt.Totals_Goals_Initial_Contacts__c;
            tot_a_sd = tot_a_sd+tt.Totals_Actual_Scheduled_Demos__c;
            tot_g_sd = tot_g_sd+tt.Totals_Goals_Scheduled_Demos__c;
            tot_a_cd = tot_a_cd+tt.Totals_Actual_Completed_Demos__c;
            tot_g_cd = tot_g_cd+tt.Totals_Goals_Completed_Demos__c;
            tot_a_cl = tot_a_cl+tt.Totals_Actual_Created_Licensing__c;
        }
        totallist.add(tot_a_ct);
        totallist.add(tot_g_ct);
        totallist.add(((tot_a_ct/tot_g_ct)*100).round());
        totallist.add(tot_a_ic);
        totallist.add(tot_g_ic);
        totallist.add(((tot_a_ic/tot_g_ic)*100).round());
        totallist.add(tot_a_sd);
        totallist.add(tot_g_sd);
        totallist.add(((tot_a_sd/tot_g_sd)*100).round());
        totallist.add(tot_a_cd);
        totallist.add(tot_g_cd);
        totallist.add(((tot_a_cd/tot_g_cd)*100).round());
        totallist.add(tot_a_cl);
    }
}