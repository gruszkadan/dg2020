trigger salesPerformanceSummaryTrigger on Sales_Performance_Summary__c
(before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            salesPerformanceSummaryTriggerHandler.beforeInsert(trigger.old, trigger.new);
        }
        if (trigger.isUpdate) {
            salesPerformanceSummaryTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
    }
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            salesPerformanceSummaryTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        if (trigger.isUpdate) {
            salesPerformanceSummaryTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }

    }
    
}