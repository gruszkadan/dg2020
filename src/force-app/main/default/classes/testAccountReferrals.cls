@isTest(seeAllData=true)
private class testAccountReferrals {
    static testmethod void testAccountReferralController1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Ownerid = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        insert newAccount;
        Contact newContact = new Contact();
        newContact.FirstName = 'Test';
        newContact.LastName = 'Testington';
        newContact.Phone = '8888888888';
        newContact.Email = 'test@test.com';
        newContact.Accountid = newAccount.id;
        insert newContact;
        
        string[] refList = 'EHS,Authoring,Ergonomics,ODT,Chemical Management'.split(',',0);
        id referrerId = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        
        Account_Referral__c[] newRefs = new list<Account_Referral__c>();
        for (string ref : refList) {
            Account_Referral__c newRef = new Account_Referral__c();
            newRef.Account__c = newAccount.id;
            newRef.Contact__c = newContact.id;
            newRef.Referrer__c = referrerId;
            newRef.Type__c = ref;
            newRefs.add(newRef);
        }
        insert newRefs;
        ApexPages.currentPage().getParameters().put('id', newAccount.Id);
        accountReferralController con = new accountReferralController();
    }
    
    static testmethod void testAccountEHSReferralController() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Ownerid = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        newAccount.EHS_Owner__c = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Test';
        newContact.LastName = 'Testington';
        newContact.Phone = '8888888888';
        newContact.Email = 'test@test.com';
        newContact.Accountid = newAccount.id;
        insert newContact;
        
        Account_Referral__c newRef = new Account_Referral__c();
        newRef.Account__c = newAccount.id;
        newRef.Contact__c = newContact.id;
        newRef.Referrer__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        newRef.Type__c = 'EHS';
        insert newRef;
        
        ApexPages.currentPage().getParameters().put('aid', newAccount.Id);
        accountEHSReferralController con = new accountEHSReferralController();
        
        con.ref.Willing_To_Speak_With_Consultants__c = 'Yes, but not me';
        con.ref.Compliance_Beyond_OSHA__c = 'Yes';
        con.selReferralGuidance = 'WorkingTalk';
        con.selRecommendedTimeframe = 'A few days';
        con.selConsultContact = newContact.id;
        con.agencyWrappers[0].w_isSel = true;
        
        con.saveRef();
        con.Cancel();
        con.otherCheck();
        con.getContactSelectList();
        con.getYesNoVals();
        con.getConsultContactVals();
        con.getReferralGuidanceVals();
        con.getRecommendedTimeframeVals();
        con.getSelContact();
    }
    
    static testmethod void testAccountODTReferralController() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Ownerid = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        newAccount.Online_Training_Account_Owner__c = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Test';
        newContact.LastName = 'Testington';
        newContact.Phone = '8888888888';
        newContact.Email = 'test@test.com';
        newContact.Accountid = newAccount.id;
        insert newContact;
        
        Account_Referral__c newRef = new Account_Referral__c();
        newRef.Account__c = newAccount.id;
        newRef.Contact__c = newContact.id;
        newRef.Referrer__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        newRef.Type__c = 'ODT';
        insert newRef;
        
        ApexPages.currentPage().getParameters().put('aid', newAccount.Id);
        accountODTReferralController con = new accountODTReferralController();
        
        con.ref.Willing_To_Speak_With_Consultants__c = 'Yes, but not me';
        con.selReferralGuidance = 'WorkingTalk';
        con.selRecommendedTimeframe = 'A few days';
        con.selConsultContact = newContact.id;
        con.selSafetyContact = newContact.id;
        
        con.saveRef();
        con.Cancel();
        con.getContactSelectList();
        con.getYesNoVals();
        con.getReferralGuidanceVals();
        con.getRecommendedTimeframeVals();
        con.getSelContact();
        con.selContactId();
        con.getSelSafeContact();
        con.selSafetyGroupContactId();
        con.getYesNoOnlyVals();
        con.getConsultContactVals();
        con.getReferralGuidanceVals();
        con.getCurrentSafetyTrainingVals();
        con.getObtainContentVals();
        con.getOutsideVendorTypeVals();
        con.getTrainingInfoRelevanceVals();
        con.getSafetyTrainingOnlineVals();
        con.getMultipleLocationVals();
    }
    
    static testmethod void testAccountAuthoringReferralController() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Ownerid = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        newAccount.Authoring_Account_Owner__c = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Test';
        newContact.LastName = 'Testington';
        newContact.Phone = '8888888888';
        newContact.Email = 'test@test.com';
        newContact.Accountid = newAccount.id;
        insert newContact;
        
        Account_Referral__c newRef = new Account_Referral__c();
        newRef.Account__c = newAccount.id;
        newRef.Contact__c = newContact.id;
        newRef.Referrer__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        newRef.Type__c = 'Authoring';
        insert newRef;
        
        ApexPages.currentPage().getParameters().put('aid', newAccount.Id);
        accountAuthoringReferralController con = new accountAuthoringReferralController();
        
        con.saveRef();
        con.getContactSelectList();
        con.getYesNoVals();
        con.getYesNoOnlyVals();
        
    }
    
    static testmethod void testAccountErgoReferralController() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Ownerid = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        newAccount.Ergonomics_Account_Owner__c = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Test';
        newContact.LastName = 'Testington';
        newContact.Phone = '8888888888';
        newContact.Email = 'test@test.com';
        newContact.Accountid = newAccount.id;
        insert newContact;
        
        Account_Referral__c newRef = new Account_Referral__c();
        newRef.Account__c = newAccount.id;
        newRef.Contact__c = newContact.id;
        newRef.Referrer__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        newRef.Type__c = 'Ergonomics';
        insert newRef;
        
        ApexPages.currentPage().getParameters().put('aid', newAccount.Id);
        accountErgoReferralController con = new accountErgoReferralController();
        
        con.saveRef();
        con.cancel();
        con.getContactSelectList();
       // con.getYesNoVals();
        
    }
    
    static testmethod void testAccountChemMgmtReferralController() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Ownerid = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        newAccount.Ergonomics_Account_Owner__c = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Test';
        newContact.LastName = 'Testington';
        newContact.Phone = '8888888888';
        newContact.Email = 'test@test.com';
        newContact.Accountid = newAccount.id;
        insert newContact;
        
        Account_Referral__c newRef = new Account_Referral__c();
        newRef.Account__c = newAccount.id;
        newRef.Contact__c = newContact.id;
        newRef.Referrer__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
        newRef.Type__c = 'Chemical Management';
        insert newRef;
        
        ApexPages.currentPage().getParameters().put('aid', newAccount.Id);
        accountChemMgmtReferralController con = new accountChemMgmtReferralController();
        
        con.saveRef();
        con.cancel();
        con.getContactSelectList();
        con.getCoverageVals();
        con.getCurrentMgmtMethodVals();
        
    }
    
    static testmethod void testAccountEHSEnterpriseReferralController() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Ownerid = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        newAccount.Authoring_Account_Owner__c = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Test';
        newContact.LastName = 'Testington';
        newContact.Phone = '8888888888';
        newContact.Email = 'test@test.com';
        newContact.Accountid = newAccount.id;
        insert newContact;  
        
        MSDS_Custom_Settings__c newSetting = new MSDS_Custom_Settings__c();
        
        newSetting.Include_in_EHS_ES_Referral_Notification__c = true;
        newSetting.SetupOwnerID = [SELECT Id FROM User WHERE Name = 'Mark McCauley'].Id;
        
        insert newSetting;
        
        
        ApexPages.currentPage().getParameters().put('aid', newAccount.Id);
        accountEHSEnterpriseReferralController con = new accountEHSEnterpriseReferralController();
        
        
        
        
        
        con.getContactSelectList();
        con.getLeadVals();
        con.saveRef();
        
        con.ref.Lead_Type__c = 'Outbound Lead';
        con.ref.Reason_for_Referral__c = 'Number of Employees';
        con.ref.Mid_Market_Determine__c = 'Test';
        con.ref.Primary_Driver__c = 'Test';
        con.ref.Budget_Authority_Etc__c = 'Test';
        
        con.saveRef();
        
        con.cancel();
        
        id newId = [SELECT id FROM Account_Referral__c WHERE Account__c = :newAccount.Id LIMIT 1].Id;
        
        System.assertEquals(con.ref.Type__c, 'EHS Enterprise');
        System.assert(newId != null);
        
        ApexPages.currentPage().getParameters().put('aid', newAccount.Id);
        ApexPages.currentPage().getParameters().put('id', newId);
        con = new accountEHSEnterpriseReferralController();
        
        con.ref.Reason_for_Referral__c = 'Single Sign-On (SSO)'; 
        
        con.saveRef();
        System.assertEquals(con.ref.Reason_for_Referral__c, 'Single Sign-On (SSO)');
        
        
    }
    
 
    
    /*static testmethod void testAccountAveryReferralController() {
Account newAccount = new Account();
newAccount.Name = 'Test Account';
newAccount.Ownerid = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
newAccount.Ergonomics_Account_Owner__c = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
insert newAccount;

Contact newContact = new Contact();
newContact.FirstName = 'Test';
newContact.LastName = 'Testington';
newContact.Phone = '8888888888';
newContact.Email = 'test@test.com';
newContact.Accountid = newAccount.id;
insert newContact;

Account_Referral__c newRef = new Account_Referral__c();
newRef.Account__c = newAccount.id;
newRef.Contact__c = newContact.id;
newRef.Referrer__c = [SELECT id FROM User WHERE LastName = 'Werner' LIMIT 1].id;
newRef.Type__c = 'Avery';
insert newRef;

ApexPages.currentPage().getParameters().put('aid', newAccount.Id);
accountAveryReferralController con = new accountAveryReferralController();

con.saveRef();
con.cancel();
con.getContactSelectList();

}
*/
}