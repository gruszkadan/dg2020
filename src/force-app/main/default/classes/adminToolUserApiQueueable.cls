public class adminToolUserApiQueueable implements Queueable, Database.AllowsCallouts {
/**
* This is called from the adminToolCustomerApiQueueable Class as part of a Chained Job
* This calls the Admin Tool Web Services to get the User Data from the Admin Tool
**/
    public void execute(QueueableContext context) {
        String salesforceEnvironment;
        String adminToolEnvironment;
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D0v000000DvZCEAM'){
            salesforceEnvironment = 'Production';
            adminToolEnvironment = 'Production';
        }else{
            salesforceEnvironment ='Sandbox';
            adminToolEnvironment = 'Staging';
        }
        string adminToolData = adminToolWebServices.fetchAdminToolData('UserData', date.Today().format(), adminToolEnvironment);
        adminToolUtility atUtility = new adminToolUtility();
        if(adminToolData != NULL){
            atUtility.upsertUserData(adminToolData);
        }
    }
}