public class quoteUtility {
    Quote__c q;
    Quote_Product__c[] quoteProducts;
    public list<quoteProductWrapper> quoteProductWrappers {get;set;}
    public list<deferredApprovalQuoteProduct> deferredApprovalWrappers {get;set;}
    
    // when we initialize this class we pass the quote
    public quoteUtility(Quote__c xq, Quote_Product__c[] xquoteProducts) {
        q = xq;
        quoteProducts = xquoteProducts;
        quoteProductWrappers = new list<quoteProductWrapper>();
        deferredApprovalWrappers = new list<deferredApprovalQuoteProduct>();
        if(quoteProducts != null && quoteProducts.size() > 0){
            for(Quote_Product__c qp:quoteProducts){
                quoteProductWrappers.add(new quoteProductWrapper(qp));
                //Create Deferred Approval Wrappers for Products with an Estimated Price applied
                if(qp.Estimated_Price_Applied__c && qp.Approval_Status__c == 'Not Submitted'){
                    deferredApprovalWrappers.add(new deferredApprovalQuoteProduct(qp));
                }
            }
        }
    }
    
    
    //testing this now
    public Quote_Product__c[] approvalCheck(Quote_Product__c[] qps, boolean isStep4) {
        if (q.Owner_Group__c == 'Retention') {
            qps = new quoteApproval().retentionProductApproval(qps);
        } else { 
            qps = new quoteApproval().salesProductCheck(qps, isStep4);
        }
        return qps;
    }
    
    // quoteProductEntryStep3 - called when editing the price of a single line item
    public Quote_Product__c discountCheck(Quote_Product__c qp) {
        qp.Price_Override__c = false;
        
        //look at which years are selected
        if (qp.Year_1__c) {
            
            //if the rep entered a quote price or a promo entered a quote price
            if (qp.Y1_Quote_Price__c != null) {
                
                //if the qp doesnt have a promo on y1
                if (!qp.Promo_Y1__c) {
                    //if the list price is greater than the quote price, the rep is discounting the product
                    if (qp.Y1_List_Price__c > qp.Y1_Quote_Price__c) {
                        qp.Price_Override__c = true;
                    }
                } else {
                    
                    //if the qp has a promo, find the actual price of the promo
                    decimal y1PromoPrice = qp.Y1_List_Price__c - (qp.Y1_List_Price__c * (qp.Promo_Discount_Percent__c / 100)).setScale(2, System.RoundingMode.HALF_UP);
                    
                    //if the quote price doesnt equal the promo price and the quote price doesnt equal the list price, the rep is discounting a product that already has a discount
                    if (qp.Y1_Quote_Price__c != y1PromoPrice && qp.Y1_Quote_Price__c != qp.Y1_List_Price__c) {
                        qp.Price_Override__c = true;
                    }
                }
            } else {
                
                //if there is no Y1 quote price, make sure we clear out the y1 discount percent
                qp.Y1_Discount__c = 0;
            }
        } 
        if (qp.Year_2__c) {
            if (qp.Y2_Quote_Price__c != null) {
                if (!qp.Promo_Y2__c) {
                    if (qp.Y2_List_Price__c > qp.Y2_Quote_Price__c) {
                        qp.Price_Override__c = true;
                    }
                } else {
                    decimal y2PromoPrice = qp.Y2_List_Price__c - (qp.Y2_List_Price__c * (qp.Promo_Discount_Percent__c / 100)).setScale(2, System.RoundingMode.HALF_UP);
                    if (qp.Y2_Quote_Price__c != y2PromoPrice && qp.Y2_Quote_Price__c != qp.Y2_List_Price__c) {
                        qp.Price_Override__c = true;
                    }
                }
            } else {
                qp.Y2_Discount__c = 0;
            }
        } 
        if (qp.Year_3__c) {
            if (qp.Y3_Quote_Price__c != null) {
                if (!qp.Promo_Y3__c) {
                    if (qp.Y3_List_Price__c > qp.Y3_Quote_Price__c) {
                        qp.Price_Override__c = true;
                    }
                } else {
                    decimal y3PromoPrice = qp.Y3_List_Price__c - (qp.Y3_List_Price__c * (qp.Promo_Discount_Percent__c / 100)).setScale(2, System.RoundingMode.HALF_UP);
                    if (qp.Y3_Quote_Price__c != y3PromoPrice && qp.Y3_Quote_Price__c != qp.Y3_List_Price__c) {
                        qp.Price_Override__c = true;
                    }
                }
            } else {
                qp.Y3_Discount__c = 0;
            }
        } 
        if (qp.Year_4__c) {
            if (qp.Y4_Quote_Price__c != null) {
                if (!qp.Promo_Y4__c) {
                    if (qp.Y4_List_Price__c > qp.Y4_Quote_Price__c) {
                        qp.Price_Override__c = true;
                    }
                } else {
                    decimal y4PromoPrice = qp.Y4_List_Price__c - (qp.Y4_List_Price__c * (qp.Promo_Discount_Percent__c / 100)).setScale(2, System.RoundingMode.HALF_UP);
                    if (qp.Y4_Quote_Price__c != y4PromoPrice && qp.Y4_Quote_Price__c != qp.Y4_List_Price__c) {
                        qp.Price_Override__c = true;
                    }
                }
            } else {
                qp.Y4_Discount__c = 0;
            }
        } 
        if (qp.Year_5__c) {
            if (qp.Y5_Quote_Price__c != null) {
                if (!qp.Promo_Y5__c) {
                    if (qp.Y5_List_Price__c > qp.Y5_Quote_Price__c) {
                        qp.Price_Override__c = true;
                    }
                } else {
                    decimal y5PromoPrice = qp.Y5_List_Price__c - (qp.Y5_List_Price__c * (qp.Promo_Discount_Percent__c / 100)).setScale(2, System.RoundingMode.HALF_UP);
                    if (qp.Y5_Quote_Price__c != y5PromoPrice && qp.Y5_Quote_Price__c != qp.Y5_List_Price__c) {
                        qp.Price_Override__c = true;
                    }
                }
            } else {
                qp.Y5_Discount__c = 0;
            }
        } 
        
        if (!qp.Price_Override__c) {
            qp.Discount_Reason__c = null;
            qp.Other_Discount_Reason__c = null;
        }
        
        return qp;
    }
    
    // 
    public Quote_Product__c findApprovers(Quote_Product__c qp) {
        if (q.Owner_Group__c == 'Retention') {
            qp = new quoteApproval().retentionDiscountApproval(qp);
        } else {
            qp = new quoteApproval().findSalesApprovers(qp);
        }
        return qp;
    }
    
    // quoteProductEntryStep3 - find the quoted price for each product entered in by the rep
    public Quote_Product__c findQuotedPrice(Quote_Product__c qp) {
        if (q.Owner_Group__c == 'Retention') {
            qp = retentionQuotedPrice(qp);
        } else {
            // currently sales quoted price is entered in directly by the rep
        }
        return qp;
    }
    
    // RETENTION - find the quoted price for the line item
    public static Quote_Product__c retentionQuotedPrice(Quote_Product__c qp) {
        
        // only for renewal products
        if (qp.Type__c == 'Renewal') {
            
            // make sure the rep fills out at least one of these fields before calculating quoted price
            //if (qp.Renewal_Amount__c != 0 || qp.New_Revenue_Price__c != 0 || qp.PI__c != 0) {
            
            // quoted price for renewal products = renewal amount + new revenue price + pi
            decimal quotedPrice = qp.Renewal_Amount__c + qp.New_Revenue_Price__c + qp.PI__c;
            
            // if the product has quantity calculation, divide by the quantity
            if (qp.Quantity_Calculation__c) {
                quotedPrice = quotedPrice / qp.Quantity__c;
            }
            
            // set the years 1-5 quoted price
            if (qp.Year_1__c) {
                if(qp.Y1_List_Price__c != null){
                    qp.Y1_Quote_Price__c = quotedPrice;
                }else{
                    qp.Y1_List_Price__c = quotedPrice;    
                }
            }
            
            if (qp.Year_2__c) {
                if(qp.Y2_List_Price__c != null){
                    qp.Y2_Quote_Price__c = quotedPrice;
                }else{
                    qp.Y2_List_Price__c = quotedPrice;    
                    
                }
            }
            if (qp.Year_3__c) {
                if(qp.Y3_List_Price__c != null){
                    qp.Y3_Quote_Price__c = quotedPrice;
                }else{
                    qp.Y3_List_Price__c = quotedPrice;    
                }
            }
            
            if (qp.Year_4__c) {
                if(qp.Y4_List_Price__c != null){
                    qp.Y4_Quote_Price__c = quotedPrice;
                }else{
                    qp.Y4_List_Price__c = quotedPrice;    
                }
            }
            
            if (qp.Year_5__c) {
                if(qp.Y5_List_Price__c != null){
                    qp.Y5_Quote_Price__c = quotedPrice;
                }else{
                    qp.Y5_List_Price__c = quotedPrice;    
                }
            }
        }
        return qp;
    }
    
    // quoteProductEntry - runs on save & continue and fills out Special_Indexing_Fields__c for the main product
    public Quote_Product__c[] indexingUpdate(Quote_Product__c[] qps) {
        for (Quote_Product__c qp : qps) {
            if (qp.Indexing_Type__c == 'Standard + Additional') {
                string indexingProds = '';
                for (Quote_Product__c indxQp : qps) {
                    if (indxQp.Group_ID__c == qp.id) {
                        indexingProds += indxQp.Product__r.Contract_Product_Name__c+', ';
                    }
                }
                if (indexingProds != '') {
                    qp.Special_Indexing_Fields__c = indexingProds.removeEnd(', ');
                }
            }
        }
        return qps;
    }
    
    public class quoteProductWrapper{
        public quote_product__c quoteProduct {get;set;}
        public productType[] productTypes {get;set;}
        public quoteProductWrapper(Quote_Product__c xqp) {
            quoteProduct = xqp;
            productTypes = new list<productType>();
            if(quoteProduct.Product__r.Product_Types__c != null){
                for(String type:quoteProduct.Product__r.Product_Types__c.split(';')){
                    if(quoteProduct.Product_Types__c == null || (quoteProduct.Product_Types__c != null && !quoteProduct.Product_Types__c.contains(type))){
                        productTypes.add(new productType(type,false));
                    }else{
                        productTypes.add(new productType(type,true));
                    }
                }
            }
        }
        
        public boolean getShowCustomProductType(){
            boolean showCustomType = false;
            for(productType t:productTypes){
                if(t.option =='Custom' && t.isSelected){
                    showCustomType = true;
                }
            }
            return showCustomType;
        }
        
        public string selectedProductTypes(){
            string productTypeString;
            for(productType t:productTypes){
                if(t.isSelected){
                    if(productTypeString == null){
                        productTypeString = t.option;
                    }else{
                        productTypeString += ';'+t.Option;
                    }
                }
            }
            return productTypeString;
        }
        
        
    }
    
    public class productType{
        public string option {get;set;}
        public boolean isSelected {get;set;}
        
        productType(string xOption, boolean xIsSelected){
            option = xOption;
            isSelected = xIsSelected;
        }
    }
    
    //Sets field on quote products passed in from based on the quote product wrappers
    public Quote_Product__c[] processQuoteProductWrappers(Quote_Product__c[] xQuoteProducts){
        //For each quote product
        for(Quote_Product__c qp:xQuoteProducts){
            for(quoteProductWrapper wrap:quoteProductWrappers){
                //Find Matching Wrapper
                if(wrap.quoteProduct.id == qp.id){
                    //Set Selected Product Types
                    qp.Product_Types__c = wrap.selectedProductTypes();
                    //If custom is not selected then clear the field
                    if(!wrap.getShowCustomProductType()){
                        qp.Product_Type_Custom__c = null;
                    }
                    break;
                }
            }
        }
        return xQuoteProducts;
    }
    
    
    //Find all non renewing order items 
    public static Order_item__c[] nonRenewingOrderItems(string AmeId){
        List<Order_item__c> returnList = new List<Order_item__c>();
        If(AmeId != Null){
            returnList = [Select id, Name, Salesforce_Product_Name__c, Product__c, Renewal_Amount__c, Product__r.Name,Account__r.Num_of_Employees__c from Order_Item__c where Cancelled_on_AME__c = :AmeId];
        }
        return returnList;
    }
    
    //Used to auto-create quotes and opportunities for renewal AME's 
    public static void createQuoteFromAME(Account_Management_Event__c[] ames){
        
        //Query standard pricebook to associate to quote
        Pricebook2 pb = [SELECT id,Name FROM Pricebook2 WHERE Name = 'Standard Price Book' LIMIT 1];
        
        //List of new quotes to be created - for each AME create a new quote
        Quote__c [] newQuotes = new list<Quote__c>();
        for(Account_Management_Event__c ame:ames){
            Quote__c newQ = new Quote__c();
            newQ.Account__c = ame.Account__c;
            newQ.Contact__c = ame.Contact__c;
            newQ.OwnerId = ame.OwnerId; 
            newQ.Account_Management_Event__c = ame.id;
            newQ.Type__c = 'Renewal';
            newQ.Price_Book__c = pb.id;
            newQ.Department__c = 'Customer Care';
            newQuotes.add(newQ);
        }
        if(newQuotes.size() > 0){
            //Insert quotes after setting the remaining quote default fields
            insert setQuoteBaseFields(newQuotes); 
            
            //Create quote products for each of the new quotes
            createQuoteProductsFromAME(newQuotes);
            
            //Once quote products have been created then create opportunities for each of the new quotes
            opportunityUtility.createOpportunityFromQuote(newQuotes);
        }
    }
    
    //Used to set default field values when creating a quote 
    public static Quote__c[] setQuoteBaseFields (Quote__c[] baseQuotes){
        
        //Create a set of contact ids used to query for contacts related to the quotes
        set<id> contactIds = new set<id>();
        for(Quote__c quote:baseQuotes){
            contactIds.add(quote.Contact__c);
        }
        
        //Create a map of contacts to reference when creating quotes
        map<id,Contact> contactMap = new map<id,Contact>([SELECT id,Lead_Type__c FROM Contact WHERE id in:contactIds]);
        
        //Create a map of campaign members to reference when creating quotes
        map<id,CampaignMember> campaignMemberMap = new map<id,CampaignMember>();
        for(CampaignMember cm:[Select id, ContactId, CampaignId, Campaign.Name, CreatedDate From CampaignMember where ContactID in:contactIds ORDER By CreatedDate DESC]){
            if(!campaignMemberMap.containsKey(cm.ContactId)){
                campaignMemberMap.put(cm.ContactId,cm);
            }
        }
        
        //Query to find unknown campaign
        Campaign unknownCampaign = [Select ID, Name from Campaign where Name='Unknown' limit 1];
        
        for(Quote__c quote:baseQuotes){
            //If the quote is related to an AME then it is a renewal
            if(quote.Account_Management_Event__c != NULL){
                quote.Type__c ='Renewal';
            }
            //Else it is new
            else{
                quote.Type__c ='New';
                
                //Assign to Campaign
                if(campaignMemberMap.containsKey(quote.Contact__c)){
                    quote.Campaign__c = campaignMemberMap.get(quote.Contact__c).CampaignId;
                } else {
                    quote.Campaign__c = unknownCampaign.Id;
                }
                
                //Set Lead Type
                quote.Lead_Type__c = contactMap.get(quote.Contact__c).Lead_Type__c;
            }
            
            //Set Currency Fields
            quote.Currency__c = 'USD';
            quote.Currency_Rate__c = 1;
            quote.Currency_Rate_Date__c = date.today();
            
            //Set Contract Length
            quote.Contract_Length__c = '3 Years';
        }
        return baseQuotes;
    }
    
    //Used to auto-create quote products for renewal AME's
    public static void createQuoteProductsFromAME(Quote__c[] quotes){
        
        //Create a set of AME ids used to query for related ames
        set<id> ameIds = new set<id>();
        for(Quote__c quote:quotes){
            ameIds.add(quote.Account_Management_Event__c);
        }
        
        //Create a map of ames and their related order items to reference when creating quote products
        map<id,Account_Management_Event__c> ameMap = new map<id,Account_Management_Event__c>([SELECT id, (SELECT id, Product__c, Renewal_Amount__c, Product__r.Name,Account__r.Num_of_Employees__c FROM Order_Items__r Where Product__c != null) FROM Account_Management_Event__c WHERE id in:ameIds]);
        
        //Create a set of all product ids for each order item related to the AME's
        set<id> productIds = new set<id>();
        for (id key : ameMap.keySet()) {
            Account_Management_Event__c ame = ameMap.get(key);
            for(Order_Item__c item:ame.Order_Items__r){
                if(item.Product__c != null){
                    productIds.add(item.Product__c);
                }
            }
        }
        
        //Query for the standard pricebook
        Pricebook2 pb = [SELECT id,Name,EHS_Pricing__c, X2_Year_Discount__c,X3_Year_Discount__c,X4_Year_Discount__c,X5_Year_Discount__c
                         FROM Pricebook2 WHERE Name = 'Standard Price Book' LIMIT 1];
        
        //Query for all pricebookentries on the standard pricebook for any products we need
        PriceBookEntry[] pbEntries = [select Name, Implementation_Cost__c, ID, Quantity_Field_Name__c, Pricebook2Id, Product2Id, Product2.Quantity_Field_Name__c,Product2.Proposal_Subpoint__c, Product2.Quantity_Calculation__c,
                                      Product2.Proposal_Print_Group__c,Product2.Proposal_Product_Name__c,Product2.Custom_Quote__c,Product2.Product_Package__c,Product2.Has_Overage_Amount__c,
                                      Product2.Overage_Amount__c,Product2.Add_On_Product__c,Product2.Has_Special_Indexing_Fields__c,Product2.Available_Languages__c,
                                      Product2.Auto_Select_Years__c,Product2.Show_Qty_Price__c, Product2.Name, Price_Break_1__c, Price_Break_2__c, Price_Break_3__c, Price_Break_4__c, Price_Break_5__c, Price_Break_6__c, Price_Break_7__c, Price_Break_8__c, Price_Break_9__c, 
                                      Price_Break_10__c, Price_Break_11__c, Price_Break_12__c, Price_Break_13__c, Price_Break_14__c, Price_Break_15__c, Qty_Break_1__c, Qty_Break_2__c, Qty_Break_3__c, Qty_Break_4__c, Qty_Break_5__c, Qty_Break_6__c, Qty_Break_7__c, Qty_Break_8__c,
                                      Qty_Break_9__c, Qty_Break_10__c, Qty_Break_11__c,Qty_Break_12__c,Qty_Break_13__c,Qty_Break_14__c,Qty_Break_15__c,
                                      Product2.Minimum_Price__c, Product2.Per_Unit_Shipping_Cost__c, Product2.Estimate_Price__c
                                      from PricebookEntry where isActive=TRUE and PriceBook2.Name = 'Standard Price Book' and Product2Id in:productIds];
        
        //Create a map to reference PBE records based on the product id
        map<id,PriceBookEntry> pbeMap = new map<id,PriceBookEntry>();
        for(PriceBookEntry pbe:pbEntries){
            pbemap.put(pbe.Product2Id,pbe);
        }
        
        //Create a map to hold quotes and their respective new quote products
        map<Quote__c,Quote_Product__c[]> quoteProductMap = new map<Quote__c,Quote_Product__c[]>();
        
        //For each quote create a new quote product for each order item pulled back related to each AME
        for(Quote__c quote:quotes){
            Account_Management_Event__c ame = ameMap.get(quote.Account_Management_Event__c);
            Quote_Product__c[] newQuoteProducts = new list<Quote_Product__c>();
            
            for(Order_Item__c item:ame.Order_Items__r){
                newQuoteProducts.add(mapOrderItemToQP(item, pbeMap.get(item.Product__c), quote));
            }
            
            /*
for(Order_Item__c item:ame.Order_Items__r){
//Find the products pricebook entry
PriceBookEntry pbe = pbeMap.get(item.Product__c);

//Create a new quote product and set fields
Quote_Product__c qp = new Quote_Product__c();
qp.Quote__c = quote.id;
qp.Product__c = item.Product__c;
qp.Name__c = item.Product__r.Name;
if (pbe.Product2.Name == 'HQ Account' || pbe.Product2.Name == 'HQ RegXR Account' ||  pbe.Product2.Name == 'HQ Implementation Fee' ||  pbe.Product2.Name == 'HQ RegXR Implementation Fee') {
qp.Quantity__c = item.Account__r.Num_of_Employees__c;
}else{
qp.Quantity__c = 1;
}

//Set Renewal Amount and Retention fields
if(item.Renewal_Amount__c != null){
qp.Renewal_Amount__c = item.Renewal_Amount__c;
}else{
qp.Renewal_Amount__c = 0;
}
qp.New_Revenue_Price__c = 0;
qp.PI__c = 0;
qp.Renewal_Order_Item__c = item.id;
//Set remaining fields
qp = setBaseQuoteProductFields(quote,qp,pbe);
qp = retentionQuotedPrice(qp);

//Add to insert list
newQuoteProducts.add(qp);
}
*/
            //Create a mapping for the quote and its respective new quote products to be used for pricing
            quoteProductMap.put(quote,newQuoteProducts);
        }
        
        //Initialize pricing engine
        quoteProductPricing pricingEngine = new quoteProductPricing();
        
        //List to hold all the new quote products to be inserted post pricing
        Quote_Product__c[] quoteProductsToInsert = new list<Quote_Product__c>();
        
        //For each quote price its products and add them to the insert list
        for (Quote__c key : quoteProductMap.keySet()) {
            Quote_Product__c[] newQuoteProducts = pricingEngine.priceProductsLogic(key, quoteProductMap.get(key), pb, null, pbEntries, null);
            quoteProductsToInsert.addAll(newQuoteProducts);
        }
        
        //Insert new quote products
        insert quoteProductsToInsert;
        
        
    }
    
    public static Quote_Product__c mapOrderItemToQP(Order_Item__c item, PriceBookEntry pbe, quote__c q){
        
        //Create a new quote product and set fields
        Quote_Product__c qp = new Quote_Product__c();
        qp.Quote__c = q.Id;
        qp.Product__c = item.Product__c;
        qp.Name__c = item.Product__r.Name;
        if (pbe.Product2.Name == 'HQ Account' || pbe.Product2.Name == 'HQ RegXR Account' ||  pbe.Product2.Name == 'HQ Implementation Fee' ||  pbe.Product2.Name == 'HQ RegXR Implementation Fee') {
            qp.Quantity__c = item.Account__r.Num_of_Employees__c;
        }else{
            qp.Quantity__c = 1;
        }
        
        //Set Renewal Amount and Retention fields
        if(item.Renewal_Amount__c != null){
            qp.Renewal_Amount__c = item.Renewal_Amount__c;
        }else{
            qp.Renewal_Amount__c = 0;
        }
        qp.New_Revenue_Price__c = 0;
        qp.PI__c = 0;
        qp.Renewal_Order_Item__c = item.id;
        //Set remaining fields
        qp = setBaseQuoteProductFields(q,qp,pbe);
        qp = retentionQuotedPrice(qp);
        return qp;
    }
    
    //Used to set default field values when creating a quote product 
    public static Quote_Product__c setBaseQuoteProductFields(Quote__c quote, Quote_Product__c qp, PriceBookEntry pbe){
        qp.Quantity_Field_Name__c = pbe.Product2.Quantity_Field_Name__c;
        qp.Proposal_Subpoint__c = pbe.Product2.Proposal_Subpoint__c;
        qp.Quantity_Calculation__c = pbe.Product2.Quantity_Calculation__c;
        qp.Print_Group__c = pbe.Product2.Proposal_Print_Group__c;
        qp.Product_Print_Name__c = pbe.Product2.Proposal_Product_Name__c;
        qp.Custom_Quote__c = pbe.Product2.Custom_Quote__c; 
        qp.Is_Product_Package__c = pbe.Product2.Product_Package__c;
        qp.Has_Overage_Amount__c = pbe.Product2.Has_Overage_Amount__c;
        qp.Overage_List_Price__c = pbe.Product2.Overage_Amount__c;
        qp.Add_On_Product__c = pbe.Product2.Add_On_Product__c;
        qp.PricebookID__c = pbe.Pricebook2Id;
        qp.PricebookEntryId__c = pbe.Id;
        if(quote.Type__c == 'New'){
            qp.Type__c = 'New';
        } else {
            qp.Type__c = 'Renewal';
        }
        if(quote.Type__c=='Renewal'){
            if(qp.Renewal_Amount__c == null){
                qp.Renewal_Amount__c = 0.00;
            }
            qp.PI__c = 0.00;
            qp.New_Revenue_Price__c = 0.00;
        }
        if(pbe.Product2.Show_Qty_Price__c == FALSE){
            qp.Quantity__c = 1;
        }
        if(pbe.Product2.Has_Special_Indexing_Fields__c == TRUE){
            qp.Indexing_Type__c = 'Standard';
        }
        if(pbe.Product2.Available_Languages__c != NULL){
            qp.Indexing_Language__c = 'English';
        }
        if(pbe.Implementation_Cost__c != NULL && pbe.Quantity_Field_Name__c =='# of Employees'){
            qp.Quantity__c = quote.Account__r.Num_of_Employees__c;
        }
        if(quote.Account_Management_Event__c != null || pbe.Product2.Auto_Select_Years__c == true) {    
            if(quote.Contract_Length__c =='5 Years'){
                qp.Year_1__c = True;
                qp.Year_2__c = True;
                qp.Year_3__c = True;
                qp.Year_4__c = True;
                qp.Year_5__c = True;
            }
            if(quote.Contract_Length__c =='4 Years'){
                qp.Year_1__c = True;
                qp.Year_2__c = True;
                qp.Year_3__c = True;
                qp.Year_4__c = True;
            }
            if(quote.Contract_Length__c =='3 Years'){
                qp.Year_1__c = True;
                qp.Year_2__c = True;
                qp.Year_3__c = True;
            }
            if(quote.Contract_Length__c =='2 Years'){
                qp.Year_1__c = True;
                qp.Year_2__c = True;
            }
            if(quote.Contract_Length__c =='1 Year'){
                qp.Year_1__c = True;
            }
        }
        return qp;
    }
    
    public Quote_Product__c[] calculateParentProductQuanity(Quote_Product__c[] qps) {
        for (Quote_Product__c qp : qps) {
            if (qp.Product__r.Has_Sub_Products__c) {
                qp.Quantity__c = 0;
                for (Quote_Product__c qp2 : qps) {
                    if(qp2.Group_Id__c == qp.Group_Parent_Id__c && qp2.Quantity__c != null){
                        qp.Quantity__c += qp2.Quantity__c;
                    }
                }
            }
        }
        return qps;
    }
    
    public Quote_Product__c[] copyQuantityFromPackageMainProduct(Quote_Product__c[] qps){
        for (Quote_Product__c qp : qps) {
            if (qp.Product__r.Product_Package_Main_Product__c) {
                for (Quote_Product__c qp2 : qps) {
                    if(qp2.Product__r.Use_Package_Parent_Quantity__c && qp2.Package_Parent_Id__c != null && qp2.Package_Parent_Id__c == qp.Package_Parent_Id__c){
                        qp2.Quantity__c = qp.Quantity__c;
                    }
                }
            }
        }
        return qps;
    }

    public class deferredApprovalQuoteProduct{
        public boolean isSelected {get;set;}
        public Quote_Product__c qp {get;set;}

        public deferredApprovalQuoteProduct(Quote_Product__c xQP){
            qp = xQP;
            isSelected = false;
        }
    }

    public Quote_Product__c[] selectedDeferredApprovalQuoteProducts(){
        Quote_Product__c[] selectedProducts = new list <Quote_Product__c>();
        for(deferredApprovalQuoteProduct daProd:deferredApprovalWrappers){
            if(daProd.isSelected){
                selectedProducts.add(daProd.qp);
            }
        }
        return selectedProducts;
    }
    
}