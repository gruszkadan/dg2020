public class eventController {
    private Apexpages.StandardController controller; 
    public Event newEvent {get;set;}
    public Event theEvent {get;set;}
    public Event e {get;set;}
    public Account theAccount {get;set;}
    public Case theCase {get;set;}
    public Opportunity theOpportunity {get;set;}
    public String new_edit {get;set;}
    public ID what {get; set;}
    public ID user {get; set;}
    public ID who {get; set;}
    public String cType {get; set;}
    public String rURL {get; set;}
    public String callResult {get; set;}
    public String editPage {get; set;}
    public String subject {get; set;}
    public User u {get; set;}
    public String activityDate {get; set;}
    public String whatType {get;set;}
    public String whoType {get;set;}
    public static String acctPrefix =  Account.sObjectType.getDescribe().getKeyPrefix();
    public static String opptyPrefix =  Opportunity.sObjectType.getDescribe().getKeyPrefix();
    public static String leadPrefix =  Lead.sObjectType.getDescribe().getKeyPrefix();
    public static String casePrefix =  Case.sObjectType.getDescribe().getKeyPrefix();
    date myDate = date.today();
    dateTime myDateTime = dateTime.now();
    public DateTime StartDateTime {get;set;}
    public DateTime StartDateTime1Hour {get;set;}
    public string WhoLeadSource {get;set;}
    public ID cID {get;set;}
    public String STD {get;set;}
    public boolean error {get;set;}
    public Opportunity[] ownerOpenOpps {get;set;}
    
    public eventController(ApexPages.StandardController stdController) {
        error = false;
        u= [Select ID, UserPreferencesTaskRemindersCheckboxDefault, Name, Department__c From User where id =: UserInfo.getUserId() LIMIT 1];
        this.controller = stdController;
        Id eventID = ApexPages.currentPage().getParameters().get('id');
        String retURL = ApexPages.currentPage().getParameters().get('retURL');
        if(eventID != NULL){
            theEvent = [select ID, OwnerID, WhoID, Event_Status__c, Subject, Unable_login_gotomeeting__c, AccountID, WhatID, StartDateTime, EndDateTime,
                        Call_Result__c, Type__c, Description, Approval_Process__c, Value_Proposition__c, Important_Note__c,
                        IsReminderSet, ReminderDateTime, Who.type from Event where ID= :eventID];
            what = theEvent.WhatId;
            who = theEvent.WhoId;
            rURL = retURL;
            new_edit = 'edit';  
            if(theEvent.AccountId != NULL){
                theAccount = [Select ID, Date_Connected_with_DM__c from Account where ID=:theEvent.AccountId];
            }
            if(theEvent.WhatId != NULL){
                if(String.valueOf(theEvent.WhatId).startsWith(acctPrefix)){
                    whatType = 'Account';
                }
                if(String.valueOf(theEvent.WhatId).startsWith(opptyPrefix)){
                    whatType = 'Opportunity';
                }
                if(String.valueOf(theEvent.WhatId).startsWith(casePrefix)){
                    whatType = 'Case';
                } 
            }
            if(theEvent.WhatId == null && theEvent.whoID != null){
                if(String.valueOf(theEvent.whoID).startsWith(leadPrefix)){
                    whoType = 'Lead';
                }
            }
            if(whatType =='Case'){
                theCase = [Select ID, AccountID, ContactID from Case where ID=:theEvent.WhatId];
            }
            
            if(whatType =='Opportunity'){
                theOpportunity = [Select ID, AccountID from Opportunity where ID=:theEvent.WhatId];
            }
        } else {
            Id whatID = ApexPages.currentPage().getParameters().get('what_id');
            String callType = ApexPages.currentPage().getParameters().get('type');
            Id userID = ApexPages.currentPage().getParameters().get('user');
            Id whoID = ApexPages.currentPage().getParameters().get('who_id');
            if(userID == NULL) {
                userID = ApexPages.currentPage().getParameters().get('aid');
            }
            newEvent = new Event();
            if(whatID != NULL){
                if(String.valueOf(whatID).startsWith(acctPrefix)){
                    whatType = 'Account';
                }
                if(String.valueOf(whatID).startsWith(opptyPrefix)){
                    whatType = 'Opportunity';
                }
                if(String.valueOf(whatID).startsWith(casePrefix)){
                    whatType = 'Case';
                } 
            }
            if(whatType =='Case'){
                theCase = [Select ID, AccountID, ContactID from Case where ID=:whatID];
            }
            
            if(whatType =='Opportunity'){
                theOpportunity = [Select ID, AccountID from Opportunity where ID=:whatId];
            }
            if(whatID == null && whoID != null){
                if(String.valueOf(whoID).startsWith(leadPrefix)){
                    whoType = 'Lead';
                }
            }
            new_edit = 'new';
            what = whatID;
            userID = UserInfo.getUserId();
            cType = callType;
            newEvent.WhatId = whatID;
            newEvent.Type__c = callType;
            newEvent.OwnerId = userID;
            newEvent.Subject = subject;
            newEvent.Event_Status__c = 'Scheduled';
            newEvent.StartDateTime = StartDateTime;
            if(whoType =='Lead'){
                newEvent.WhoId = whoID;
            }
            rURL = retURL;   
        }        
    }  
    
    
    public List<SelectOption> getSubjects()
    {
        List<SelectOption> subjects = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult =
            Event.Subject.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        subjects.add(new SelectOption('', '-- Select Subject --',false));
        for( Schema.PicklistEntry f : ple)
        {
            subjects.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return subjects;
    }
    
    public List<SelectOption> getContactSelectList() {
        //returns a list of Contacts as SelectOptions - limited to 500
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- Select --',false));
        if(whatType == 'Account'){        
            
            List<Contact> contacts = [SELECT Id, Name FROM Contact where AccountID=:What Order By Name ASC limit 500];
            for (Contact contact : contacts) {
                options.add(new SelectOption(contact.ID, contact.Name));
            }
        }
        
        if(whatType == 'Opportunity' ){        
            
            List<Contact> contacts = [SELECT Id, Name FROM Contact where AccountID=:theOpportunity.AccountID Order By Name ASC limit 500];
            for (Contact contact : contacts) {
                options.add(new SelectOption(contact.ID, contact.Name));
            }
        }
        if(whatType =='Case'){
            List<Contact> contacts = [SELECT Id, Name FROM Contact where AccountID=:theCase.AccountID Order By Name ASC limit 500];
            for (Contact contact : contacts) {
                options.add(new SelectOption(contact.ID, contact.Name));
            }
        }
        if(whoType == 'Lead'){
            List<Lead> leads = [SELECT Id, Name FROM Lead where ID=:Who Order By Name ASC];
            for (Lead lead : leads) {
                options.add(new SelectOption(lead.ID, lead.Name));
            }
        } 
        return options;
    }
    
    public Contact getContact( Id cID) {       
        List<Contact> contacts = [SELECT Name, ID FROM Contact WHERE Id = :cID LIMIT 1];
        if (contacts != null && contacts.size()>0) {
            return contacts[0];
        } else {
            return null;
        }
    }
    
    public PageReference setName() {
        Contact contact = getContact(cID);
        if (contact != null) who = contact.ID;
        contactLeadSource();
        return null;
    }
    
    public void contactLeadSource() {
        if(new_edit != 'edit'){
            Contact c = [select ID, LeadSource from Contact where ID=: newEvent.WhoId limit 1];
            whoLeadSource = c.LeadSource;
        } else{
            whoLeadSource = null;
        }
    }
    
    public PageReference changeEndDate (){
        if(new_edit == 'new'){
            StartDateTime = newEvent.StartDateTime;
            StartDateTime1Hour = StartDateTime.addHours(1);
            if(StartDateTime1Hour !=NULL){
                updateEndDate();
            }else{
                newEvent.EndDateTime = dateTime.now();
            }
        }else{
            theEvent.EndDateTime = null;
        }
        return null;
    }
    
    public void updateEndDate (){
        if(new_edit =='new'){
            newEvent.EndDateTime = StartDateTime1Hour;
        } else {
            theEvent.EndDateTime = null;
        }
    }
    
    public PageReference newEventErrorCheck (){
        error = false;
        if(whatType=='Account' && newEvent.WhoId == NULL){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'You Must Select a Contact'));
            error = true;
        }
        if(whatType=='Opportunity' && newEvent.WhoId == NULL){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'You Must Select a Contact'));
            error = true;
        }
        if(whoType=='Lead' && newEvent.WhoId == NULL){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'You Must Select a Lead'));
            error = true;
        }
        if(newEvent.Subject == NULL){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'You Must Select a Subject'));
            error = true;
        }
        if(newEvent.StartDateTime == NULL){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'You Must Select a Start Date'));
            error = true;
        }
        return null;
    }
    
    public PageReference eventErrorCheck (){
        error = false;
        if(theEvent.AccountId != NULL && theEvent.WhoId == NULL){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'You Must Select a Contact'));
            error = true;
        }
        if(theEvent.Who.Type =='Lead' && theEvent.WhoId == NULL){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'You Must Select a Lead'));
            error = true;
        }
        if(theEvent.Subject == NULL){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'You Must Select a Subject'));
            error = true;
        }
        if(theEvent.StartDateTime == NULL){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, 'You Must Select a Start Date'));
            error = true;
        }
        return null;
    }    
    public void queryownerOpenOpps(){
        if(newEvent.OwnerId != NULL && whatType == 'Account' && newEvent.whatId != NULL){
            ownerOpenOpps = [SELECT Id, Name, OwnerId 
                             FROM Opportunity 
                             WHERE OwnerId =:newEvent.OwnerId 
                             AND isClosed = False 
                             AND AccountId =:newEvent.whatId LIMIT 1];
        } else {
            ownerOpenOpps = null;
        }
    }
    
    
    public PageReference newEventSave() {
        newEventErrorCheck ();
        if(error ==true){
            return null;
        }
        if(newEvent.EndDateTime == NULL)
        {
            newEvent.DurationInMinutes = 60;
        }
        insert newEvent;
        if(whatType=='Account' && newEvent.whatId != NULL){
            queryownerOpenOpps();
        } else {
            ownerOpenOpps = null;
        }
        
        if(whatType =='Account' && newEvent.WhoId != NULL && newEvent.Subject !='Repeat Demo' && newEvent.Subject !='Conference Call'
           && newEvent.Subject !='IT Conference Call' && newEvent.Subject !='Purchase Discussion' && newEvent.Subject !='Onsite Customer Meeting'){
               if(ownerOpenOpps.size() == 0 ){
                   PageReference newOppty = Page.opportunity_new;
                   newOppty.setRedirect(true);
                   newOppty.getParameters().put('contactID', newEvent.WhoId);  
                   newOppty.getParameters().put('opp6', WhoLeadSource);
                   return newOppty; 
               } 
               PageReference pageRef = new PageReference('/' + newEvent.WhatId);
               return pageRef;
           }
        if(whatType =='Account' && newEvent.WhoId != NULL ){
            if(newEvent.Subject =='Repeat Demo' || newEvent.Subject =='Conference Call' ||
               newEvent.Subject =='IT Conference Call' || newEvent.Subject =='Purchase Discussion'){
                   PageReference pageRef = new PageReference('/' + newEvent.WhatId);
                   return pageRef;
               }
        }
        if(whatType=='Opportunity' && newEvent.WhoId != NULL){
            PageReference pageRef = new PageReference('/' + newEvent.WhatId);
            return pageRef;
        }
        if(whatType!='Opportunity' && whatType!='Account' && newEvent.WhoId != NULL){
            PageReference pageRef = new PageReference('/' + newEvent.WhoId);
            return pageRef;
        }
        PageReference pageRef = new PageReference('/' + newEvent.Id);
            return pageRef;
    }
    
    public PageReference save() {
        error = false;
        eventErrorCheck();
        if(error ==false){
            update theEvent;
            if(rURL != null){
                PageReference pageRef = new PageReference(rURL);
                return pageRef;
            } else {
                PageReference pageRef = new PageReference('/' + theEvent.Id);
                return pageRef;
            }
        }
        return null;
    }
    
    public PageReference saveNewEvent() {     
        eventErrorCheck();
        if(error ==true){
            return null;
        }
        update theEvent;
        PageReference newSaveEvent = new PageReference('/00U/e?');
        newSaveEvent.setRedirect(true);
        if(what != NULL){
            newSaveEvent.getParameters().put('what_id',what); 
        } 
        if(who != NULL){
            newSaveEvent.getParameters().put('who_id', who);        
        }
        newSaveEvent.getParameters().put('user',user);
        newSaveEvent.getParameters().put('retURL',rURL);         
        return newSaveEvent;  
    }
    
    /*public PageReference saveNewEventNewTask() {
newEventErrorCheck();
if(error ==true){
return null;
}
insert newEvent;
PageReference task_new = Page.task_new;
task_new.setRedirect(true);
if(newEvent.WhatId != NULL){
task_new.getParameters().put('whatid',newEvent.WhatId); 
} 
if(newEvent.WhoId != NULL){
task_new.getParameters().put('whoId', newEvent.WhoId);  
}
task_new.getParameters().put('tsk5',subject);
task_new.getParameters().put('user',u.Id);
task_new.getParameters().put('retURL',rURL);
if(newEvent.WhatId != NULL){
task_new.getParameters().put('type','Follow-Up');
} else {
task_new.getParameters().put('type','Initial');
}
return task_new;
}*/
    
    public PageReference saveNew() {
        error = false;
        eventErrorCheck();
        if(error ==false){
            update theEvent;
            PageReference task_new = Page.task_new;
            task_new.setRedirect(true);
            if(what != NULL){
                task_new.getParameters().put('whatid',what); 
            } 
            if(who != NULL){
                task_new.getParameters().put('whoID', who);     
            }
            task_new.getParameters().put('tsk5',subject);
            task_new.getParameters().put('user',u.Id);
            if(what != NULL){
                task_new.getParameters().put('type','Follow-Up');
            } else {
                task_new.getParameters().put('type','Initial');
            }
            
            return task_new;  
        }
        return null;
    }
}