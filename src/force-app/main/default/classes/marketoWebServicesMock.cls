global class marketoWebServicesMock implements HttpCalloutMock {
    //  Default Constructor
    global marketoWebServicesMock() {}
    
    //  Handle responses for two callouts
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
       
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
 
        //  Set the response body, based on endpoint. - What if your method calls example1 twice and you need a different response?
        if (req.getEndpoint() == 'https://907-JRM-499.mktorest.com/rest/v1/activities.json?&nextPageToken=dsadar432ewdxcgr5f&activityTypeIds=25&batchSize=300')
            res.setBody('{"requestId": "1514c#15adc8e8a60","result": [{"id": 427680513,"marketoGUID": 427680513,"leadId": 6523669,"activityDate": "2017-03-10T19:59:05Z",	"activityTypeId": 25,"campaignId": 12207,"primaryAttributeValueId": 18202,"primaryAttributeValue": "zOP POI ERGO.POI_Request Demo_Ergonomics","attributes": []},{"id": 427680520,"marketoGUID": 427680520,"leadId": 6523661,"activityDate": "2017-03-10T19:59:26Z","activityTypeId": 25,"campaignId": 12208,"primaryAttributeValueId": 18158,"primaryAttributeValue": "zOP POI MSDS.POI_Request Quote_HQ RegXR","attributes": []}],"success": true,"nextPageToken": "QUH5NTX2IQP7CGLPLWR5VONQ4XFON5S3ZY7MLF7EO6LQTL44UPTQ====","moreResult": true}' );
        else if (req.getEndpoint() == 'https://907-JRM-499.mktorest.com/identity/oauth/token?grant_type=client_credentials&client_id=d60dfbcd-f660-4ad7-95c3-7ca47f18bb2d&client_secret=VVEZEHyxHOIApZsl4tFbLGWNtAJMVLFM')
            res.setBody('{"access_token": "0739fb66-51b1-4305-ac53-0caba5c38dce:ab","token_type": "bearer","expires_in": 794,"scope": "salesforcemerge@msdsonline.com"}');
 		else if (req.getEndpoint() == 'https://907-JRM-499.mktorest.com/rest/v1/activities/pagingtoken.json?access_token=0739fb66-51b1-4305-ac53-0caba5c38dce:ab&sinceDatetime=2017-03-24T00:00:00Z')
            res.setBody('{"requestId": "a255#15b10244747","success": true,"nextPageToken": "I5JBEYNNS2P4FQKBEDB57Q5WDJX43ZEDAPFJ4UVVNOYGUTFXMZAQ===="}');
        return res;
    }
}