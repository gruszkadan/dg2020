public with sharing class sfRequest {
    private ApexPages.StandardController controller {get; set;}
    public Salesforce_Request__c sfr {get;set;}
    public User u {get;set;}
    public string pagetype {get;set;}
    public Salesforce_Request_Task__c newReqTask; 
    public Salesforce_Request_Task__c[] reqTask {get; set;}
    public Salesforce_Request_Component__c[] theRequestComponents {get;set;}
    public Salesforce_Request_Test_Scenario__c[] theRequestScenarios {get;set;}
    public List<Salesforce_Request_Task__c> openTask {get;set;}
    public Salesforce_Request_Task__c lastOpenTask {get; set;}
    public string sortOrder {get;set;}
    public string sortOrder2 {get;set;}
    public string src_soql {get;set;}
    public string srs_soql {get;set;}
    public id sfrId {get;set;}
    public boolean terrEnrich {get;set;}
    public boolean isPinned {get;set;}
    public Integer numPass{get;set;}
    public Integer numFail {get;set;}
    public Integer numNull {get;set;}
    public decimal danTotalHour {get;set;}
    public decimal alTotalHour {get;set;}
    public decimal taraTotalHour {get;set;}
    public decimal markTotalHour {get;set;}
    public String danLastWorked {get;set;}
    public String alLastWorked {get;set;}
    public String taraLastWorked {get;set;}
    public String markLastWorked {get;set;}

    
    public sfRequest(ApexPages.StandardController controller) {
        this.controller = controller;
        sfrId = ApexPages.currentPage().getParameters().get('id');  
        u = [Select id, Name, LastName, Role__c, SF_Request_Owner_Change__c, SF_Request_Status_Change__c, Profile.Name from User where id =: UserInfo.getUserId() LIMIT 1];
        queryRequest();
        src_soql = 'SELECT Salesforce_Component__c, Salesforce_Request__c, Notes__c, Name, Id, Deployed__c, Deployed_to_Production_Staging__c, Deployed_to_MSDS_Dev__c,Deployed_to_MSDS_Test__c, '+
            'Salesforce_Component__r.Num_of_Open_Requests__c, Salesforce_Component__r.Name, Type__c, Object__c, Name__c '+
            'from Salesforce_Request_Component__c where Salesforce_Request__c =:sfrId ';
        sortOrder = 'ORDER BY Type__c ASC';
        theRequestComponents = Database.query (src_soql+sortOrder+' NULLS LAST');
        reqTask = [Select ID, Work_Request__c, Start_Date__c, End_Date__c, Owner__c, Minutes_Worked__c, Owner__r.Name from Salesforce_Request_Task__c where Work_Request__c =: sfr.Id order by End_Date__c DESC nulls last];
        openTask = new List<Salesforce_Request_Task__c>();

        if (reqTask.size() > 0) {
            for (Salesforce_Request_Task__c t: reqTask) {
                if (t.End_Date__c == NULL && t.Owner__c == u.Id){
                    openTask.add(t); 
                }
            }
        }
        if(openTask.size()>0){
            lastOpenTask = [Select ID, Work_Request__c, Start_Date__c, End_Date__c from Salesforce_Request_Task__c where
                            Work_Request__c =: sfr.Id and End_Date__c = NULL and Owner__c = :u.Id ORDER BY Start_Date__c desc limit 1];
        }
        newReqTask = new Salesforce_Request_Task__c();
        if(sfr.Request_Type__c == 'Merge'){
            pagetype = 'merge';
        } else{
            pagetype = 'request';
        }
        if (u.LastName == 'Savage' && sfr.Request_Type__c == 'Territory Enrichment') {terrEnrich = true;}
        srs_soql = 'SELECT id,Scenario__c,Result__c,Expected_Outcome__c,Step__c, Owner__c, Salesforce_Request__c, Name '+
            'from Salesforce_Request_Test_Scenario__c where Salesforce_Request__c =:sfrId ';
        sortOrder2 = 'ORDER BY Step__c ASC';
        theRequestScenarios = Database.query (srs_soql+sortOrder2+' NULLS LAST');
        countScenario();
        calculateTeamBreakdown();
      
        
    }
    
    public void queryRequest() {
        string SObjectAPIName = 'Salesforce_Request__c';
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string query = 'SELECT '+commaSepratedFields+' FROM '+SObjectAPIName+' WHERE id = \''+sfrId+'\' ';
        sfr = Database.query(query);
    }
    
    public void reorderComps() {
        theRequestComponents = Database.query (src_soql+sortOrder+' NULLS LAST');
    }
    
    public void acceptOwnershipChange () {
        if (sfr.Id != null ) {
            try {
                Salesforce_Request__c sfrOwner = [select Id, OwnerID from Salesforce_Request__c where Id=:sfr.Id];
                sfrOwner.OwnerID = u.Id;
                update sfrOwner;
            }
            catch(Exception e){
                ApexPages.addMessages(e);
            }
        } 
    } 
    
    public PageReference startTimer() {
        try {
            newReqTask.Work_Request__c = sfr.Id;
            newReqTask.Owner__c = u.Id;
            newReqTask.Start_Date__c = system.Datetime.now();
            insert newReqTask;
            if(sfr.Start_Date__c == NULL){
                sfr.Start_Date__c = date.today();
                update sfr;
            }
        } 
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        PageReference sfRequest_detail = Page.sfRequest_detail;
        sfRequest_detail.setRedirect(true);
        sfRequest_detail.getParameters().put('id',sfr.Id); 
        return sfRequest_detail;        
    }
    
    public PageReference endTimer() {
        try {
            lastOpenTask.End_Date__c = system.Datetime.now();
            update lastOpenTask;
        } 
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        PageReference sfRequest_detail = Page.sfRequest_detail;
        sfRequest_detail.setRedirect(true);
        sfRequest_detail.getParameters().put('id',sfr.Id); 
        return sfRequest_detail;        
    }
    
    public PageReference acceptOwnership() {
        acceptOwnershipChange();
        PageReference sfRequest_detail = Page.sfRequest_detail;
        sfRequest_detail.setRedirect(true);
        sfRequest_detail.getParameters().put('id',sfr.Id); 
        return sfRequest_detail;        
    }
    
    public PageReference addComponent() {
        PageReference sfRequestComponentsAdd = Page.sfRequestComponentsAdd;
        sfRequestComponentsAdd.setRedirect(true);
        sfRequestComponentsAdd.getParameters().put('id',sfr.Id); 
        return sfRequestComponentsAdd;        
    }        
    
    public PageReference editComponent() {
        PageReference sfRequestComponentsEdit = Page.sfRequestComponentsEdit;
        sfRequestComponentsEdit.setRedirect(true);
        sfRequestComponentsEdit.getParameters().put('id',sfr.Id); 
        return sfRequestComponentsEdit;        
    }        
    
    public PageReference completeMerge(){
        acceptOwnershipChange();
        acceptOwnership();
        sfr.Status__c = 'Completed';
        sfr.OwnerId = u.id;
        sfr.completed_Date__c = date.TODAY();
        update sfr;
        PageReference p1 = Page.SfRequest_detail;
        p1.setredirect(true);
        p1.getParameters().put('id',sfr.Id);
        return p1;
    }
    
    public void saveComponentEdits() {
        update theRequestComponents;
    }
    
    public PageReference completeButton(){
        controller.save();
        sfr.Status__c = 'Completed';
        sfr.Completed_Date__c = Date.today();
        if(openTask.size() > 0){
            if(lastOpenTask.Start_Date__c != NULL && lastOpenTask.End_Date__c == NULL){
                lastOpenTask.End_Date__c = DateTime.now();
                update lastOpenTask;
            }
        }
        if(sfr.Request_Type__c == 'Admin'){
            if(sfr.Admin_Work_Type__c != NULL){
                update sfr;
                return NULL;
            }
            else{
                
                return NULL;
            }
        }
        else{
            update sfr;
            return NULL;
        }
    }
    
    public PageReference addScenario() {
        PageReference sfRequestTestScenario_add_edit = Page.sfRequestTestScenario_add_edit;
        sfRequestTestScenario_add_edit.setRedirect(true);
        sfRequestTestScenario_add_edit.getParameters().put('id',sfr.Id); 
        return sfRequestTestScenario_add_edit;        
    } 
    public PageReference scenarioDetail() {
        id scenid = System.currentPageReference().getParameters().get('scenid');
        PageReference sfRequestTestScenario_detail = Page.sfRequestTestScenario_detail;
        sfRequestTestScenario_detail.setRedirect(true);
        sfRequestTestScenario_detail.getParameters().put('id',sfr.id);
        sfRequestTestScenario_detail.getParameters().put('scenid',scenid);
        return sfRequestTestScenario_detail;
    } 
    
    public PageReference scenDetailReorder(){
        PageReference sfRequestTestScenario_reorder = Page.sfRequestTestScenario_reorder;
        sfRequestTestScenario_reorder.setRedirect(true);
        sfRequestTestScenario_reorder.getParameters().put('id',sfr.Id); 
        return sfRequestTestScenario_reorder; 
    }
    
    public void countScenario() {
        numPass = 0;
        numFail = 0;
        numNull = 0;
        for (Salesforce_Request_Test_Scenario__c scen : theRequestScenarios) {
            if (scen.Result__c == 'Pass') {
                numPass++;
            }if (scen.Result__c == 'Fail') {
                numFail++;
            }if (scen.Result__c == null) {
                numNull++;
            }
        }
    }
    
    public void calculateTeamBreakdown() 
    {
        
        decimal danTotalMin = 0;
        decimal alTotalMin = 0;
        decimal taraTotalMin = 0;
        decimal markTotalMin = 0;
        
        
        if(reqTask.size() > 0){
            List<Salesforce_Request_Task__c> danTasks = new List<Salesforce_Request_Task__c>();
            List<Salesforce_Request_Task__c> alTasks = new List<Salesforce_Request_Task__c>();
            List<Salesforce_Request_Task__c> taraTasks = new List<Salesforce_Request_Task__c>();
            List<Salesforce_Request_Task__c> markTasks = new List<Salesforce_Request_Task__c>();
            
            for(Salesforce_Request_Task__c t : reqTask){
                if(t.Owner__r.Name == 'Daniel Gruszka' && t.Minutes_worked__c != null){  
                    danTotalMin += t.Minutes_Worked__c;
                    danTasks.add(t);
                  
                    
                }
                if(t.Owner__r.Name == 'Al Powell' && t.Minutes_worked__c != null){   
                    alTotalMin += t.Minutes_Worked__c; 
                    alTasks.add(t);
                }
                if(t.Owner__r.Name == 'Tara Wills' && t.Minutes_worked__c != null){   
                    taraTotalMin += t.Minutes_Worked__c;
                    taraTasks.add(t);
                }
                if(t.Owner__r.Name == 'Mark McCauley' && t.Minutes_worked__c != null){   
                    markTotalMin += t.Minutes_Worked__c;
                    markTasks.add(t);
                }                 
            }  
            
            danTotalHour = (danTotalMin / 60).setScale(2);
            alTotalHour = (alTotalMin / 60).setScale(2);
            taraTotalHour = (taraTotalMin / 60).setScale(2);
            markTotalHour = (markTotalMin / 60).setScale(2); 
            
            if(danTasks.size() > 0){
                danLastWorked = danTasks[0].End_Date__c.format();
            }
            if(alTasks.size() > 0){
                alLastWorked = alTasks[0].End_Date__c.format();   
            }
            if(taraTasks.size() > 0){
                taraLastWorked = taraTasks[0].End_Date__c.format();    
            }
            if(markTasks.size() > 0){
                markLastWorked = markTasks[0].End_Date__c.format(); 
            }
            
            
        }
        
        
        
        
    }
    
    public PageReference redirect(){
        if(sfr.Request_Type__c != 'Merge' && u.Profile.Name != 'SF Operations' && u.Profile.Name != 'System Administrator'){
            PageReference sf_request_detail = Page.sf_request_detail;
            sf_request_detail.setRedirect(true);
            sf_request_detail.getParameters().put('id',sfr.Id); 
            return sf_request_detail;
        }
        else{
            return null;
        }
    }
    
    
}