@isTest(SeeAllData=true)

public class testCleanupBadOpportunities {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testMethod void test1() { 
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account 666666666776';
        insert newAccount;
        
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'Test Account 666666666776';
        newOppty.AccountID = newAccount.Id;
        newOppty.CloseDate = system.today();
        newOppty.StageName = 'Demo Scheduled';
        insert newOppty;
        
        Opportunity newOppty1 = new Opportunity();
        newOppty1.Name = 'Test Account 666666666776';
        newOppty1.AccountID = newAccount.Id;
        newOppty1.CloseDate = system.today();
        newOppty1.StageName = 'Demo Scheduled';
        newOppty1.Opportunity_Type__c = 'New';
        insert newOppty1;
        
        Task newTask = new Task(WhatId = newOppty.id);
        insert newTask;
        
        String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, 
                                       new cleanupBadOpportunities());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, 
                            ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', 
                            String.valueOf(ct.NextFireTime));
    }
    
}