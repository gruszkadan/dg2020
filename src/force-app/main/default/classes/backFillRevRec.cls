public class backFillRevRec {
    
    public static void backFillTransactions(){
        //Query for holidays
        Holiday[] holidays = [SELECT ActivityDate FROM Holiday]; 

        Revenue_Recognition__c[] RevRecRecords = [SELECT id, Backfill_Complete__c, Case__c,Case__r.Primary_Project_Type__c,Case__r.Type, Start_at_Step__c, Current_Total_Project_Amount__c,
                                                  Phase_Amount__c,Total_Amount_Recognized__c,Remaining_Amount_Forecasted__c,Max_Forecasted_Step_Number__c,Step_Number__c,Current_Sequence_Number__c,
                                                  (SELECT id,Sequence_Number__c,Number_of_Days_Forecasted_in_Status__c,Recognition_Date__c,Expected_Date__c, Step__c 
                                                  FROM Revenue_Recognition_Transactions__r ORDER BY CreatedDate ASC)
                                                  FROM Revenue_Recognition__c
                                                  WHERE Start_at_Step__c != NULL AND Backfill_Complete__c = False];
        
      
        //Upsert list for transactions
        Revenue_Recognition_Transaction__c[] rrtToUpsert = new List<Revenue_Recognition_Transaction__c>();
        
        //create case type set and query for MDT's
        set<string> caseTypes = new set<string>();
        set<Id> caseIds = new set<Id>();
        for (Revenue_Recognition__c r: RevRecRecords){
            caseTypes.add(r.Case__r.Type);
            caseIds.add(r.Case__c);
        }
        
        Revenue_Recognition_Setting__mdt[] rrsMDTs = [SELECT id, Revenue_Recognition_Percent__c, Project_Status__c, Step__c, Case_Type__c, Primary_Project_Type__c, 
                                                      Can_Be_Added_By_User__c, Insert_On_Case_Creation__c, Number_of_Days__c, Number_of_Days_in_Status__c
                                                      FROM Revenue_Recognition_Setting__mdt 
                                                      WHERE Case_Type__c IN :caseTypes AND Can_Be_Added_By_User__c = false AND Insert_On_Case_Creation__c = true ORDER BY Step__c ASC];
        
        Revenue_Recognition__c[] rrToUpdate = new List<Revenue_Recognition__c>();

        for(Revenue_Recognition__c rr : RevRecRecords){
            //integer firstNewTrans = rr.Revenue_Recognition_Transactions__r.size();
            integer sequenceNum = 0;
            for(Revenue_Recognition_Transaction__c rrt: rr.Revenue_Recognition_Transactions__r){
                sequenceNum++;
                rrt.Sequence_Number__c = sequenceNum;
                rrtToUpsert.add(rrt); 
            }
            
            
            decimal totalAmountForecasted = rr.Total_Amount_Recognized__c + rr.Remaining_Amount_Forecasted__c;
            date previousexpectedDate = Date.newInstance(2018, 12, 13);

            //loop over the metadata types
            for (Revenue_Recognition_Setting__mdt rrs: rrsMDTs){
                //if the mdt is matched and is for a step that comes after the max forecasted step currently on the rev rec
                if(rr.Case__r.Type == rrs.Case_Type__c 
                   && rr.Case__r.Primary_Project_Type__c == rrs.Primary_Project_Type__c	
                   && rrs.Step__c >= rr.Start_at_Step__c)
                {
                    //Create a new rrt for the step
                    sequenceNum++;
                    Revenue_Recognition_Transaction__c rrt = new Revenue_Recognition_Transaction__c();
                    rrt.Sequence_Number__c = sequenceNum;
                    rrt.Revenue_Recognition__c = rr.id;
                    rrt.Recognition_Percent__c = rrs.Revenue_Recognition_Percent__c;
                    rrt.Recognition_Project_Status__c = rrs.Project_Status__c;
                    rrt.Step__c = rrs.Step__c;
                    if(rrs.Step__c == 1){
                        rrt.Recognition_Date__c = Date.newInstance(2018, 12, 13);
                    }
                    
                    decimal recAmount = RevenueRecognitionUtility.calculateRecognitionAmount(rrs.Revenue_Recognition_Percent__c, rr.Current_Total_Project_Amount__c, totalAmountForecasted);
                    if(recAmount < 0){
                        recAmount = 0;
                    }
                    rrt.Recognition_Amount__c = recAmount;
                    //rrt.Recognition_Amount__c = RevenueRecognitionUtility.calculateRecognitionAmount(rrs.Revenue_Recognition_Percent__c, rr.Phase_Amount__c, totalAmountForecasted);
                    date expectedDate = dateUtility.calculateBusinessDay(previousexpectedDate, integer.valueOf(rrs.Number_Of_Days__c), holidays);  
                    rrt.Expected_Date__c = expectedDate;
                    rrt.Original_Expected_Date__c = expectedDate;
                    
                    rrt.Number_of_Days_Forecast__c = rrs.Number_Of_Days__c;
                    
                    rrt.Number_of_Days_Forecasted_in_Status__c = rrs.Number_of_Days_in_Status__c;
                    //update variables
                    totalAmountForecasted += rrt.Recognition_Amount__c;
                    previousexpectedDate = expectedDate;
                    rrtToUpsert.add(rrt);
                }
            }
            //update rev rec field
            rr.Phase_Amount__c = rr.Current_Total_Project_Amount__c;
            rr.Backfill_Complete__c = true;
            rrToUpdate.add(rr);

        }
        try{
            update rrToUpdate;
            upsert rrtToUpsert;   
        }
        catch(exception e) {
            salesforceLog.createLog('Revenue Recognition', true, 'revenueRecognitionUtility', 'createRevRecRecords', string.valueOf(e));
        }
    }
}