public class ultimateParentUpdater implements Schedulable {
    
    public void execute(SchedulableContext ctx) {
        Account[] trgAccts = new list<Account>();		
        Account[] parAccts = new list<Account>();		
        Account[] ultAccts = new list<Account>();
        Account[] childAccts = new list<Account>();		
        set<id> newUltParIds = new set<id>();
        set<id> oldUltParIds = new set<id>();
        set<Account> updSet = new set<Account>();
        Account[] updList = new list<Account>();
        Ultimate_Parent_Accounts__c[] delList = new List<Ultimate_Parent_Accounts__c>();
        set<id> trgAcctIds = new set<id>();
        
        for (Ultimate_Parent_Accounts__c upa : [SELECT id, AccountId__c FROM Ultimate_Parent_Accounts__c]) {
            trgAcctIds.add(upa.AccountId__c);
            delList.add(upa);
        }
        
        if (trgAcctIds.size() > 0) {
            
            trgAccts.addAll([SELECT id, Name, ParentId, Ultimate_Parent__c, Is_Ultimate_Parent__c, Parent.ParentId, Parent.Ultimate_Parent__c, Parent.Is_Ultimate_Parent__c
                             FROM Account
                             WHERE id IN : trgAcctIds]);            
            
            childAccts.addAll([SELECT id, Name, Ultimate_Parent__c, Is_Ultimate_Parent__c,
                               ParentId,
                               Parent.ParentId,
                               Parent.Parent.ParentId,
                               Parent.Parent.Parent.ParentId,
                               Parent.Parent.Parent.Parent.ParentId,
                               Parent.Parent.Parent.Parent.Parent.ParentId 
                               FROM Account
                               WHERE ParentId IN : trgAcctIds
                               OR Parent.ParentId IN : trgAcctIds
                               OR Parent.Parent.ParentId IN : trgAcctIds
                               OR Parent.Parent.Parent.ParentId IN : trgAcctIds
                               OR Parent.Parent.Parent.Parent.ParentId IN : trgAcctIds
                               OR Parent.Parent.Parent.Parent.Parent.ParentId IN : trgAcctIds]);
            
            for (Account trgAcct : trgAccts) {
                if (trgAcct.ParentId != null) {
                    if (trgAcct.Parent.ParentId == null) {
                        trgAcct.Ultimate_Parent__c = trgAcct.ParentId;
                        newUltParIds.add(trgAcct.ParentId);
                    } else {
                        if (trgAcct.Parent.Ultimate_Parent__c != null) {
                            trgAcct.Ultimate_Parent__c = trgAcct.Parent.Ultimate_Parent__c;
                        }
                    }
                } else {
                    if (trgAcct.Ultimate_Parent__c != null) {
                        oldUltParIds.add(trgAcct.Ultimate_Parent__c);
                    } 
                }
                
                Account[] myChildAccts = new list<Account>();
                for (Account childAcct : childAccts) {
                    if (childAcct.ParentId == trgAcct.id ||
                        childAcct.Parent.ParentId == trgAcct.id ||
                        childAcct.Parent.Parent.ParentId == trgAcct.id ||
                        childAcct.Parent.Parent.Parent.ParentId == trgAcct.id ||
                        childAcct.Parent.Parent.Parent.Parent.ParentId == trgAcct.id ||
                        childAcct.Parent.Parent.Parent.Parent.Parent.ParentId == trgAcct.id) {
                            myChildAccts.add(childAcct);
                        }
                }
                if (myChildAccts.size() > 0) {
                    for (Account childAcct : myChildAccts) {
                        if (trgAcct.ParentId != null) {
                            childAcct.Ultimate_Parent__c = trgAcct.Ultimate_Parent__c;
                            trgAcct.Is_Ultimate_Parent__c = false;
                        } else {
                            childAcct.Ultimate_Parent__c = trgAcct.id;
                            trgAcct.Ultimate_Parent__c = null;
                            trgAcct.Is_Ultimate_Parent__c = true;
                        }
                        updSet.add(childAcct);
                    }
                } else {
                    trgAcct.Is_Ultimate_Parent__c = false;
                }
                updSet.add(trgAcct);
                
            }
            
            if (newUltParIds.size() > 0) {
                parAccts.addAll([SELECT id, Name
                                 FROM Account
                                 WHERE id IN : newUltParIds]);
                for (Account parAcct : parAccts) {
                    parAcct.Is_Ultimate_Parent__c = true;
                    parAcct.Ultimate_Parent__c = null;
                    updSet.add(parAcct);
                }
            }
            
            if (oldUltParIds.size() > 0) {
                ultAccts.addAll([SELECT id, Name
                                 FROM Account
                                 WHERE id IN : oldUltParIds]);
                Account[] ultChildAccts = new list<Account>();
                ultChildAccts.addAll([SELECT id, ParentId, Name,
                                      Parent.ParentId,
                                      Parent.Parent.ParentId,
                                      Parent.Parent.Parent.ParentId,
                                      Parent.Parent.Parent.Parent.ParentId,
                                      Parent.Parent.Parent.Parent.Parent.ParentId 
                                      FROM Account
                                      WHERE ParentId IN : oldUltParIds
                                      OR Parent.ParentId IN : oldUltParIds
                                      OR Parent.Parent.ParentId IN : oldUltParIds
                                      OR Parent.Parent.Parent.ParentId IN : oldUltParIds
                                      OR Parent.Parent.Parent.Parent.ParentId IN : oldUltParIds
                                      OR Parent.Parent.Parent.Parent.Parent.ParentId IN : oldUltParIds]);
                for (Account ultAcct : ultAccts) {
                    Account[] myChildAccts = new list<Account>();
                    for (Account childAcct : ultChildAccts) {
                        if (childAcct.ParentId == ultAcct.id ||
                            childAcct.Parent.ParentId == ultAcct.id ||
                            childAcct.Parent.Parent.ParentId == ultAcct.id ||
                            childAcct.Parent.Parent.Parent.ParentId == ultAcct.id ||
                            childAcct.Parent.Parent.Parent.Parent.ParentId == ultAcct.id ||
                            childAcct.Parent.Parent.Parent.Parent.Parent.ParentId == ultAcct.id) {
                                myChildAccts.add(childAcct);
                            }
                    }
                    if (myChildAccts.size() == 0) {
                        ultAcct.Is_Ultimate_Parent__c = false;
                        updSet.add(ultAcct);
                    }
                }
            }
        }
        
        string deDup = '';
        if (updSet.size() > 0) {
            for (Account acct : updSet) {
                if (!deDup.contains(acct.id)) {
                    updList.add(acct);
                    deDup += acct.id+',';
                }
            }
        }
        
        if (updList.size() > 0) {
            update updList;
        }
        
        if (delList.size() > 0) {
            delete delList;
        }
        
        system.abortJob(ctx.getTriggerId());
        if (!Test.isRunningTest()) {
            ultimateParentUpdater e = new ultimateParentUpdater();
            String sch = '0 15 7,8,9,10,11,12,13,14,15,16,17,18 ? * MON-FRI';
            system.schedule('Ultimate Parent Updater', sch, e);
        }
        
    }
    
}