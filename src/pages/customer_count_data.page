<apex:page standardStylesheets="true" sidebar="true" showHeader="true" docType="html-5.0" controller="customerCountDataController"
    readOnly="true">

    <!-- background-color: rgb(250, 250, 249); background-color:#cfeef8; -->
    <style>
        .slds-scope .slds-card {}

        .footerStyle {
            background-color: rgb(250, 250, 249);
            border-bottom: none;
        }


        .velocityFont {
            color: rgb(0, 104, 146);
            font-weight: bold;
        }


        .ehsFont {
            color: rgb(0, 182, 222);
            font-weight: bold;
        }

        .vDarkBlue {
            background-color: rgb(0, 104, 146) !important;
        }

        .vLightBlue {
            background-color: rgb(0, 182, 222);
        }
    </style>

    <apex:form id="form">
        <div class="slds-scope">
            <apex:slds />

            <div class="slds-media__body slds-p-vertical_small" style="width:60%;">
                <center>
                    <h2 class="slds-card__header-title">
                        <div class="slds-truncate velocityFont" style="font-size: 1.5rem;">
                            <span>Velocity</span>
                            <span class="ehsFont">EHS</span>
                            <span> Customer Counts</span>
                        </div>
                    </h2>
                </center>
            </div>

            <article class="slds-card" style="width:60%;">
                <table id="table" class="slds-table slds-table_col-bordered">
                    <tbody>
                        <tr class="footerStyle">
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="30">
                                <div class="slds-truncate" style="color:rgb(0, 182, 222);font-weight:bold;font-size: .9rem;">Total Active Customers</div>
                            </th>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="30"
                                width="30%">
                                <div class="slds-truncate" style="color:rgb(0, 182, 222);font-weight:bold;font-size: .9rem;" title="Count">
                                    <apex:outputText value="{0, number, ###,##0}">
                                        <apex:param value="{!totalWithoutChemTelAuth}" />
                                    </apex:outputText>
                                </div>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </article>
            <div class="slds-truncate" style="color:rgb(0, 104, 146);font-size:.8rem;font-style:italic"> Note – Totals at the bottom of each section will not match the sum of each product count due to customers having multiple products.</div>
            <br/>
            <article class="slds-card slds-p-vertical_top-small" style="width:60%;">

                <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media_center slds-has-flexi-truncate">
                        <div class="slds-media__body">
                            <h2 class="slds-card__header-title">
                                <div class="slds-card__header-link slds-truncate">
                                    <span class="velocityFont">Chemical Management</span>
                                </div>
                            </h2>
                        </div>

                    </header>
                </div>

                <table id="table" class="slds-table slds-table_col-bordered " style="border-top: 1px solid rgb(221, 219, 218); border-bottom: 1px solid rgb(221, 219, 218);">
                    <thead>
                        <tr>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="26">
                                <div class="slds-truncate " title="Chemical Management">Products</div>
                            </th>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="26"
                                width="30%">
                                <div class="slds-truncate" title="Count">Count</div>
                            </th>

                        </tr>
                    </thead>
                    <tbody style="border-bottom: 1px solid rgb(221, 219, 218); border-top: 1px solid rgb(221, 219, 218); width: 100%;" id="toReviewTable">


                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                HQ
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['HQ']}" />
                                </apex:outputText>
                            </td>
                        </tr>

                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                HQ RegXR
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['HQ RegXR']}" />
                                </apex:outputText>
                            </td>
                        </tr>

                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                GM
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['GM']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                GM Pro
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['GM Pro']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                GM Upgrade
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['GM Upgrade']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Webpliance
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Webpliance']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                PPI
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!totalPPI}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left ">
                                Emergency Response Services
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!totalERS}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                ChemTel - Emergency Response
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['ChemTel - Emergency Response']}" />
                                </apex:outputText>
                            </td>
                        </tr>


                    </tbody>
                    <tfoot class="footerStyle">
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left velocityFont">
                                Total MSDS
                            </td>
                            <td class="slds-text-align_left velocityFont">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!suiteCounts['MSDS Management']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </article>

            <article class="slds-card" style="width:60%;">
                <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media_center slds-has-flexi-truncate">
                        <div class="slds-media__body">
                            <h2 class="slds-card__header-title">
                                <div class="slds-card__header-link slds-truncate">
                                    <span class="velocityFont">EHS Management</span>
                                </div>
                            </h2>
                        </div>

                    </header>
                </div>

                <table id="table" class="slds-table slds-table_col-bordered " style="border-top: 1px solid rgb(221, 219, 218); border-bottom: 1px solid rgb(221, 219, 218);">

                    <thead>
                        <tr>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="26">
                                <div class="slds-truncate" title="EHS Management">Products</div>
                            </th>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="26"
                                width="30%">
                                <div class="slds-truncate" title="Count">Count</div>
                            </th>

                        </tr>
                    </thead>
                    <tbody style="border-bottom: 1px solid rgb(221, 219, 218); border-top: 1px solid rgb(221, 219, 218); width: 100%;" id="toReviewTable">


                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Air
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Air']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Audit and Inspection
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Audit and Inspection']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Compliance Management
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!totalCompliance}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Corrective Action
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Corrective Action']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                IH Program Management
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['IH Program Management']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Incident Management
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Incident Management']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                MOC
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Management of Change(MOC)']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Performance Metrics
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Performance Metrics']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Risk Analysis
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Risk Analysis']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Safety Meetings
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Safety Meetings']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Title V
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Title V']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Training Tracking
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Training Tracking']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                TRI
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['TRI (Toxic Release Inventory)']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Waste
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Waste']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Water
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Water']}" />
                                </apex:outputText>
                            </td>
                        </tr>

                    </tbody>
                    <tfoot class="footerStyle">
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left velocityFont">
                                Total EHS
                            </td>
                            <td class="slds-text-align_left velocityFont">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!suiteCounts['EHS Management']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </article>

            <article class="slds-card" style="width:60%;">
                <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media_center slds-has-flexi-truncate">
                        <div class="slds-media__body">
                            <h2 class="slds-card__header-title">
                                <div class="slds-card__header-link slds-truncate">
                                    <span class="velocityFont">Ergonomics</span>
                                </div>
                            </h2>
                        </div>

                    </header>
                </div>
                <table id="table" class="slds-table slds-table_col-bordered " style="border-top: 1px solid rgb(221, 219, 218); border-bottom: 1px solid rgb(221, 219, 218);">
                    <thead>
                        <tr>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="26">
                                <div class="slds-truncate">Products</div>
                            </th>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="26"
                                width="30%">
                                <div class="slds-truncate" title="Count">Count</div>
                            </th>

                        </tr>

                    </thead>
                    <tbody style="border-bottom: 1px solid rgb(221, 219, 218); border-top: 1px solid rgb(221, 219, 218); width: 100%;" id="toReviewTable">


                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Ergo Advocate
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Ergo']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                The Humantech System
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['The Humantech System']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Ergopoint
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Ergopoint']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot class="footerStyle">
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left velocityFont">
                                Total Ergo
                            </td>
                            <td class="slds-text-align_left velocityFont">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!suiteCounts['Ergonomics']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </article>
        
            <article class="slds-card" style="width:60%;">
                <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media_center slds-has-flexi-truncate">
                        <div class="slds-media__body">
                            <h2 class="slds-card__header-title">
                                <div class="slds-card__header-link slds-truncate">
                                    <span class="velocityFont">Authoring</span>
                                </div>
                            </h2>
                        </div>

                    </header>
                </div>
                <table id="table" class="slds-table slds-table_col-bordered " style="border-top: 1px solid rgb(221, 219, 218); border-bottom: 1px solid rgb(221, 219, 218);">
                    <thead>
                        <tr>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="26">
                                <div class="slds-truncate">Products</div>
                            </th>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="26"
                                width="30%">
                                <div class="slds-truncate" title="Count">Count</div>
                            </th>

                        </tr>

                    </thead>

                    <tbody style="border-bottom: 1px solid rgb(221, 219, 218); border-top: 1px solid rgb(221, 219, 218); width: 100%;" id="toReviewTable">
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Chicago Authoring
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['MSDS Authoring']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Tampa Authoring
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['ChemTel - MSDS Authoring']}" />
                                </apex:outputText>
                            </td>
                        </tr>

                    </tbody>
                    <tfoot class="footerStyle">
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left velocityFont">
                                Total Authoring Customers
                            </td>
                            <td class="slds-text-align_left velocityFont">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!suiteCounts['MSDS Authoring']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </article>

            <div class="slds-truncate" style="color:rgb(0, 104, 146);font-size: .8rem;font-style:italic"> Note – This section is only for Authoring projects still in process.</div>
            <br/>
            
            <article class="slds-card" style="width:60%;">
                <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media_center slds-has-flexi-truncate">
                        <div class="slds-media__body">
                            <h2 class="slds-card__header-title">
                                <div class="slds-card__header-link slds-truncate">
                                    <span class="velocityFont">Miscellaneous</span>
                                </div>
                            </h2>
                        </div>

                    </header>
                </div>

                <table id="table" class="slds-table slds-table_col-bordered " style="border-top: 1px solid rgb(221, 219, 218); border-bottom: 1px solid rgb(221, 219, 218);">
                    <thead>
                        <tr>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="26">
                                <div class="slds-truncate" title="Miscellaneous">Products</div>
                            </th>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="26"
                                width="30%">
                                <div class="slds-truncate" title="Count">Count</div>
                            </th>

                        </tr>
                    </thead>
                    <tbody style="border-bottom: 1px solid rgb(221, 219, 218); border-top: 1px solid rgb(221, 219, 218); width: 100%;" id="toReviewTable">

                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Safety Toolkit
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!totalSafetyKit}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Environmental Toolkit
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!totalEnviroKit}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                Faxback
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!totalFaxBack}" />
                                </apex:outputText>
                            </td>
                        </tr>
                        <tr width="2rem" class="table-row slds-hint-parent bordersUpdate slds-m-_small" style="border-top: 1px solid rgb(221, 219, 218);">

                            <td class="slds-text-align_left">
                                On-Demand Training
                            </td>
                            <td class="slds-text-align_left">
                                <apex:outputText value="{0, number, ###,##0}">
                                    <apex:param value="{!productCounts['Online Training']}" />
                                </apex:outputText>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </article>
            <!-->
            <article class="slds-card" style="width:60%;">
                <table id="table" class="slds-table slds-table_col-bordered">

                    <tbody>

                        <tr class="footerStyle">
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="30">
                                <div class="slds-truncate" style="color:rgb(0, 182, 222);font-weight:bold;font-size: .9rem;">VelocityEHS Customers</div>
                            </th>
                            <th aria-label="Product/Service" aria-sort="none" class="slds-is-resizable slds-text-title_caps" scope="col" height="30"
                                width="30%">
                                <div class="slds-truncate" style="color:rgb(0, 182, 222);font-weight:bold;font-size: .9rem;" title="Count">
                                    <apex:outputText value="{0, number, ###,##0}">
                                        <apex:param value="{!totalWithoutChemTelAuth}" />
                                    </apex:outputText>
                                </div>
                            </th>

                        </tr>
                    </tbody>
                </table>
            </article>
            <!-->
        </div>
    </apex:form>
</apex:page>