/** Copyright (c) 2008, Matthew Friend, Sales Engineering, Salesforce.com Inc.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/
/**
* To adapt this to anouther Object simply search for "Change" to go to the places
* where the sObject and query must be changed
*/ 
public with sharing class AccountStructure{
    //Declare variables
    public String currentId;
    public List<ObjectStructureMap> asm ;
    public Map<String, ObjectStructureMap> masm;
    public List<Integer> maxLevel;
    public Account theAccount {get;set;}
    /**
* Contructor
*/
    public AccountStructure() {
        currentId = System.currentPageReference().getParameters().get('id');
        this.asm = new List<ObjectStructureMap>{};
            this.masm = new Map<String, ObjectStructureMap>{};
                this.maxLevel = new List<Integer>{};
                    
                    theAccount = [select ID, Name from Account where ID=:currentId];
    }
    /**
* Allow page to set the current ID
*/
    public void setcurrentId( String cid ){
        currentId = cid;
    }
    /**
* Return ObjectStructureMap to page
* @return asm
*/
    public List<ObjectStructureMap> getObjectStructure(){
        asm.clear();
        if ( currentId == null ) {
            currentId = System.currentPageReference().getParameters().get( 'id' );
        }
        System.assertNotEquals( currentId, null, 'sObject ID must be provided' );
        asm = formatObjectStructure( CurrentId );
        return asm;
    }
    /**
* Query Account from top down to build the ObjectStructureMap
* @param currentId
* @return asm
*/
    public ObjectStructureMap[] formatObjectStructure( String currentId ){
        List<ObjectStructureMap> asm = new List<ObjectStructureMap>{};
            masm.clear();
        //Change below
        List<Account> al = new List<Account>{};
            List<Opportunity> ol = new List<Opportunity>{};
                List<ID> currentParent = new List<ID>{};
                    Map<ID, String> nodeList = new Map<ID, String>{};
                        List<String> nodeSortList = new List<String>{};
                            List<Boolean> levelFlag = new List<Boolean>{};
                                List<Boolean> closeFlag = new List<Boolean>{};
                                    String nodeId = '0';
        String nodeType = 'child';
        Integer count = 0;
        Integer level = 0;
        Boolean endOfStructure = false;
        //Find highest level obejct in the structure
        currentParent.add( GetTopElement( currentId ) );
        //Loop though all children
        while ( !endOfStructure ){
            if( level == 0 ){
                //Change below
                al = [ SELECT a.ParentId, a.OwnerId, a.Name, a.BillingState, a.NAICS_Code__c, a.Customer_Status__c,
                      a.Num_of_Open_Activities__c, a.Num_of_Open_Opportunities__c, a.Id, a.Owner.LastName, a.Num_of_Opportunities__c,
                      a.Num_of_Completed_Events__c, a.Num_of_Completed_Activities__c, a.Campaign_Status__c, Online_Training_Account_Owner__r.LastName, Authoring_Account_Owner__r.LastName,
                      EHS_Owner__r.LastName, Ergonomics_Account_Owner__r.LastName,
                      (Select Salesforce_Product_Name__c, Product_Suite__c, Product_Platform__c, Category__c from Order_Items__r where Admin_Tool_Order_Status__c = 'A' and Display_on_Account__c = true ORDER BY Product_Suite__c ASC)
                      FROM Account a WHERE a.id IN : CurrentParent ORDER BY a.Name ];
            }
            else {
                //Change below
                al = [ SELECT a.ParentId, a.OwnerId, a.Name, a.BillingState, a.NAICS_Code__c, a.Customer_Status__c,
                      a.Num_of_Open_Activities__c, a.Num_of_Open_Opportunities__c, a.Id, a.Owner.LastName, a.Num_of_Opportunities__c,
                      a.Num_of_Completed_Events__c, a.Num_of_Completed_Activities__c, a.Campaign_Status__c, Online_Training_Account_Owner__r.LastName, Authoring_Account_Owner__r.LastName,
                      EHS_Owner__r.LastName, Ergonomics_Account_Owner__r.LastName,
                      (Select Salesforce_Product_Name__c, Product_Suite__c, Product_Platform__c, Category__c from Order_Items__r where Admin_Tool_Order_Status__c = 'A' and Display_on_Account__c = true ORDER BY Product_Suite__c ASC)
                      FROM Account a WHERE a.ParentID IN : CurrentParent ORDER BY a.Name ];
            }
            ol = [SELECT id, Name, AccountId, StageName, Reason_Lost__c, Value__c, CloseDate, Num_of_Licensing_Products__c, Num_of_Services_Products__c,
                  OwnerId, Contract_Numbers__c, Num_of_Products__c, Contract_Length__c, Currency__c, Currency_Rate__c, Opportunity_Type__c, isclosed,
                  (SELECT id, Name__c, Indexing_Language__c, Y1_Total_Price__c, Y2_Total_Price__c, Y3_Total_Price__c, Y4_Total_Price__c, Y5_Total_Price__c, Total_Value__c from OpportunityLineItems ORDER BY Group_Name__c ASC, Group_Parent_ID__c ASC, Group_ID__c ASC NULLS FIRST, Name__c ASC)
                  from Opportunity where accountid in:al ORDER BY CloseDate desc];
            if( al.size() == 0 ){
                endOfStructure = true;
            }
            else{
                currentParent.clear();
                for ( Integer i = 0 ; i < al.size(); i++ ){
                    Opportunity[] accountOpportunities = new list<Opportunity>();
                    if(ol != null && ol.size() > 0){
                        for(Opportunity opp:ol){
                            if(opp.AccountId == al[i].id){
                                accountOpportunities.add(opp);
                            }
                        }
                    }
                    //Change below
                    Account a = al[i];
                    nodeId = ( level > 0 ) ? NodeList.get( a.ParentId )+'.'+String.valueOf( i ) : String.valueOf( i );
                    masm.put( NodeID, new ObjectStructureMap( nodeID, levelFlag, closeFlag, nodeType, false, false, a, accountOpportunities) );
                    currentParent.add( a.id );
                    nodeList.put( a.id,nodeId );
                    nodeSortList.add( nodeId );
                }
                maxLevel.add( level );
                level++;
            }
        }
        //Account structure must now be formatted
        NodeSortList.sort();
        for( Integer i = 0; i < NodeSortList.size(); i++ ){
            List<String> pnl = new List<String> {};
                List<String> cnl = new List<String> {};
                    List<String> nnl = new List<String> {};
                        if ( i > 0 ){
                            String pn = NodeSortList[i-1];
                            pnl = pn.split( '\\.', -1 );
                        }
            String cn = NodeSortList[i];
            cnl = cn.split( '\\.', -1 );
            if( i < NodeSortList.size()-1 ){
                String nn = NodeSortList[i+1];
                nnl = nn.split( '\\.', -1 );
            }
            ObjectStructureMap tasm = masm.get( cn );
            if ( cnl.size() < nnl.size() ){
                //Parent
                tasm.nodeType = ( isLastNode( cnl ) ) ? 'parent_end' : 'parent';
            }
            else if( cnl.size() > nnl.size() ){
                tasm.nodeType = 'child_end';
                tasm.closeFlag = setcloseFlag( cnl, nnl, tasm.nodeType );
            }
            else{
                tasm.nodeType = 'child';
            }
            tasm.levelFlag = setlevelFlag( cnl, tasm.nodeType );
            //Change below
            if ( tasm.account.id == currentId ) {
                tasm.currentNode = true;
            }
            asm.add( tasm );
        }
        asm[0].nodeType = 'start';
        asm[asm.size()-1].nodeType = 'end';
        return asm;
    }
    /**
* Determin parent elements relationship to current element
* @return flagList
*/
    public List<Boolean> setlevelFlag( List<String> nodeElements, String nodeType ){
        List<Boolean> flagList = new List<Boolean>{};
            String searchNode = '';
        String workNode = '';
        Integer cn = 0;
        for( Integer i = 0; i < nodeElements.size() - 1; i++ ){
            cn = Integer.valueOf( nodeElements[i] );
            cn++;
            searchNode = workNode + String.valueOf( cn );
            workNode = workNode + nodeElements[i] + '.';
            if ( masm.containsKey( searchNode ) ){
                flagList.add( true );
            }
            else {
                flagList.add( false );
            }
        }
        return flagList;
    }
    /**
* Determin if the element is a closing element
* @return flagList
*/
    public List<Boolean> setcloseFlag( List<String> cnl, List<String> nnl, String nodeType ){
        List<Boolean> flagList = new List<Boolean>{};
            String searchNode = '';
        String workNode = '';
        Integer cn = 0;
        for( Integer i = nnl.size(); i < cnl.size(); i++ ){
            flagList.add( true );
        }
        return flagList;
    }
    /**
* Determin if Element is the bottom node
* @return Boolean
*/
    public Boolean isLastNode( List<String> nodeElements ){
        String searchNode = '';
        Integer cn = 0;
        for( Integer i = 0; i < nodeElements.size(); i++ ){
            if ( i == nodeElements.size()-1 ){
                cn = Integer.valueOf( nodeElements[i] );
                cn++;
                searchNode = searchNode + String.valueOf( cn );
            }
            else {
                searchNode = searchNode + nodeElements[i] + '.';
            }
        }
        if ( masm.containsKey( searchNode ) ){
            return false;
        }
        else{
            return true;
        }
    }
    /**
* Find the tom most element in Heirarchy
* @return objId
*/
    public String GetTopElement( String objId ){
        Boolean top = false;
        while ( !top ) {
            //Change below
            Account a = [ Select a.Id, a.Name, a.ParentId From Account a where a.Id =: objId Order By a.Name limit 1 ];
            if ( a.ParentID != null ) {
                objId = a.ParentID;
            }
            else {
                top = true;
            }
        }
        return objId ;
    }
    /**
* Wrapper class
*/
    public with sharing class ObjectStructureMap{
        public String nodeId;
        public Boolean[] levelFlag = new Boolean[]{};
        public Boolean[] closeFlag = new Boolean[]{};
        public String nodeType;
        public Boolean currentNode;
        /**
* @Change this to your sObject
*/
        public Account account;
        public Opportunity[] opportunities;
        public String getnodeId() { return nodeId; }
        public Boolean[] getlevelFlag() { return levelFlag; }
        public Boolean[] getcloseFlag() { return closeFlag; }
        public String getnodeType() { return nodeType; }
        public Boolean getcurrentNode() { return currentNode; }
        Public integer OpenOpps {get;set;}


        /**
* @Change this to your sObject
*/
        public Account getaccount() { return account; }
        public Opportunity[] getopportunities() { return opportunities; }
        public void setnodeId( String n ) { this.nodeId = n; }
        public void setlevelFlag( Boolean l ) { this.levelFlag.add(l); }
        public void setlcloseFlag( Boolean l ) { this.closeFlag.add(l); }
        public void setnodeType( String nt ) { this.nodeType = nt; }
        public void setcurrentNode( Boolean cn ) { this.currentNode = cn; }
        /**
* @Change this to your sObject
*/
        public void setaccount( Account a ) { this.account = a; }
        public void setopportunities( Opportunity[] o ) { this.opportunities = o; }
        /**
* @Change the parameters to your sObject
*/
        public boolean hasChem {get;set;}
        public boolean hasODT {get;set;}
        public boolean hasAuth {get;set;}
        public boolean hasEHS {get;set;}
        public boolean hasErgo {get;set;}
        public Map<String,String> activeProductsMap {get;set;}
        public boolean hasLicensing {get;set;}
        public boolean hasCS {get;set;}
        public boolean hasACS {get;set;}
        public boolean hasEHSLicensing {get;set;}
        public boolean hasEHSServices {get;set;}
        public boolean hasEHSAddOnServices {get;set;}
        public boolean hasErgoLicensing {get;set;}
        public boolean hasErgoServices {get;set;}
        public ObjectStructureMap(String nodeId, Boolean[] levelFlag,Boolean[] closeFlag , String nodeType, Boolean lastNode, Boolean currentNode, Account a, Opportunity[] accountOpps){
            
            this.nodeId = nodeId;
            this.levelFlag = levelFlag;
            this.closeFlag = closeFlag;
            this.nodeType = nodeType;
            this.currentNode = currentNode;
            //Change this to your sObject
            this.account = a;
            if(this.account.Order_Items__r.size() > 0){
                activeProductsMap = new map<String,String>();
                for(Order_Item__c oi:this.account.Order_Items__r){
                    activeProductsMap.put(oi.Category__c+'|'+oi.Salesforce_Product_Name__c,oi.Category__c);
                    if(oi.Product_Suite__c == 'EHS Management'){
                        hasEHS = true;
                        if(oi.Category__c == 'EHS Licensing'){
                            hasEHSLicensing = true;
                        }else if(oi.category__c =='EHS Services'){
                            hasEHSServices = true;
                        }else if (oi.Category__c == 'EHS Add On Services'){
                            hasEHSAddOnServices = true;
                        }
                    }else if(oi.Product_Suite__c == 'Ergonomics'){
                        hasErgo = true;
                        if(oi.Category__c == 'Ergo Licensing'){
                            hasErgoLicensing = true;
                        }else if(oi.category__c =='Ergo Services'){
                            hasErgoServices = true;
                        }
                    }else if(oi.Product_Suite__c == 'MSDS Authoring'){
                        hasAuth = true;
                        if(oi.category__c =='Compliance Services'){
                            hasCS = true;
                        }else if (oi.Category__c == 'Additional Compliance Solutions'){
                            hasACS = true;
                        }
                    }else if(oi.Product_Suite__c == 'MSDS Management'){
                        hasChem = true;
                        if(oi.Category__c == 'MSDS Management'){
                            hasLicensing = true;
                        }else if(oi.category__c =='Compliance Services'){
                            hasCS = true;
                        }else if (oi.Category__c == 'Additional Compliance Solutions'){
                            hasACS = true;
                        }
                    }else if(oi.Product_Suite__c == 'On-Demand Training'){
                        hasODT = true;
                        if (oi.Category__c == 'Additional Compliance Solutions'){
                            hasACS = true;
                        }
                    }
                }
            }
            this.opportunities = accountOpps;
            
            OpenOpps = 0;

            
                  
            for (opportunity o : accountopps){
                if(o.isclosed == false){
                    OpenOpps++;
                }
               
            }
      
            
        }
    }
}