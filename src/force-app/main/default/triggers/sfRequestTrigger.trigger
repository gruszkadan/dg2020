trigger sfRequestTrigger on Salesforce_Request__c (after Update) {
    
    if (trigger.isAfter){
        
        //Salesforce Request already exists, Latest_Comment field only getting updated
        if(trigger.isUpdate){
            
            sfRequestTriggerHandler.afterUpdate(trigger.old, trigger.new);
            
        }
        
    }
    
    
}