public class quoteController {
    private ApexPages.StandardController controller {get; set;}
    public User u {get;set;}
    public Quote__c quote {get;set;}
    public Quote__c[] quoteOptions {get;set;}
    public Opportunity Opp {get;set;}
    public String xmlMessage { get; set; }
    public Quote_Product__c[] shoppingCart {get;set;}
    public Quote_Product__c[] authoringProducts {get;set;}
    public List<pQuoteProduct> productList {get;set;}
    public string quoteID {get;set;}
    public Quote_Options__c qops {get;set;}
    public boolean hasMultiOptions {get;set;}
    public option[] options {get;set;}
    public boolean isErgo {get;set;}
    //promos
    public boolean isApprover {get;set;}
    public string appComments {get;set;}
    public boolean msgApprove {get;set;}
    public boolean msgReject {get;set;}
    public boolean msgRecall {get;set;}
    public boolean showContractButton {get;set;}
    private msdsCustomSettings helper = new msdsCustomSettings();
    public decimal defaultOpportunityAmount {get;set;}
    public Contact currentContact {get;set;}
    public Campaign unknownCampaign {get;set;} 
    public CampaignMember[] allCampaigns {get;set;}
    public CampaignMember primaryCampaign {get;set;}
    public CampaignMember[] respondedCampaign {get;set;}
    public List<OpportunityLineItem> oppLineItems {get;set;}
    public List<Opportunity> oppList {get;set;}
    public Order_item__c[] NonRenewingProducts {get;set;}  
    public quoteUtility util {get;set;}
    
    
    
    public quoteController(ApexPages.StandardController controller) {
        //initialize the stanrdard controller
        this.controller = controller;
        u = [SELECT id, Name, Suite_Of_Interest__c, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, Role__c, ProfileID, Profile.Name, Alias
             FROM User 
             WHERE id =: UserInfo.getUserId() LIMIT 1];
        // load the current record
        quoteId = ApexPages.currentPage().getParameters().get('id');
        if(quoteID != NULL){
            fetchQuote();
        }
        shoppingCart = [SELECT id, Name__c, Expected_Delivery_Date__c, Estimated_Price_Applied__c, Product_Types__c, Indexing_Language__c, Product_Sub_Type__c, Group_ID__c, Bundled_Product__c, Quote_Product__c, Quantity__c, Y1_Total_Price__c, Y2_Total_Price__c, Y3_Total_Price__c, Y4_Total_Price__c, Y5_Total_Price__c, Total_Value__c, Approval_Status__c, Product__r.Ergonomics__c, Product__r.Product_Types__c
                        FROM Quote_Product__c 
                        WHERE Quote__c = :Quote.id
                        AND Product__r.Parent_Product__c = null
                        ORDER BY Group_Name__c ASC, Group_Parent_ID__c ASC, Group_ID__c ASC NULLS FIRST, Product__r.Sub_Product_Sort_Order__c ASC, Name__c ASC];
        authoringProducts = [SELECT id, Name__c, Quote_Product__c, Auth_Project_Terms__c 
                             FROM Quote_Product__c
                             WHERE Quote__c = :Quote.id 
                             AND Authoring_Item__c = TRUE 
                             AND Approval_Status__c = 'Approved' 
                             ORDER BY Name__c];
        util = new quoteUtility(Quote,shoppingCart);

        
        //query any opportunities that have been created off quote and the OppLineItems related to them
        if(quote.Opportunity__c != null){
            oppLineItems = [Select Id FROM OpportunityLineItem WHERE OpportunityId = :quote.Opportunity__c];
            
        }
        
        
        
        // by default set the boolean value for multiple options to false
        hasMultiOptions = false;
        // init list of option wrappers
        options = new List<option>();
        // if the quote has quote options, query the quote options and create the wrappers list
        if (quote.Quote_Options__c != null) {
            qops = [SELECT id, Type__c FROM Quote_Options__c WHERE id = :quote.Quote_Options__c LIMIT 1];
            integer numOfOps = [SELECT id FROM Quote__c WHERE Quote_Options__c = :qops.id].size();
            if (numOfOps > 1) {
                hasMultiOptions = true;
                createOptionsWrappers();
            } 
        }
        
        checkErgo();
        checkApproval();
        
        if(quote.Contact__c !=null){
            currentContact = [Select ID, AccountID, Account.Name, CreatedDate, Lead_Source_Category__c, Lead_Source_Detail__c,
                              Lead_Source_Type__c, mkto_si__Last_Interesting_Moment_Date__c, Lead_Created_Date__c, LeadSource
                              from Contact where ID=:quote.Contact__c];
            unknownCampaign = [Select ID, Name from Campaign where Name='Unknown' limit 1];
            allCampaigns = [Select CampaignId, Campaign.Name, HasResponded, CreatedDate From CampaignMember
                            where ContactID=:currentContact.ID];
            if(allCampaigns.size()>0){
                respondedCampaign = [Select CampaignId, Campaign.Name, CreatedDate From CampaignMember
                                     where ContactID=:currentContact.ID and HasResponded = TRUE];
                if(respondedCampaign.size()>0){
                    primaryCampaign = [Select CampaignId, Campaign.Name, CreatedDate From CampaignMember where ContactID=:currentContact.ID and HasResponded = TRUE ORDER By First_Responded_Date__c DESC Limit 1];
                } else{
                    primaryCampaign = [Select CampaignId, Campaign.Name, CreatedDate From CampaignMember where ContactID=:currentContact.ID ORDER By CreatedDate DESC Limit 1];
                }
            }
        }
        
        
        showContractButton = MSDS_Custom_Settings__c.getInstance().Contract_Easy_Button__c;
        defaultOpportunityAmount = helper.defaultOpportunityAmount();
        
  		//pass in nonRenewingOrderItems method that is in Quote Utility 
        NonRenewingProducts = quoteUtility.nonRenewingOrderItems(Quote.Account_Management_Event__c); 
 
    }
    
    //create contract off of quote
    public PageReference createContract(){
        
        //if there isn't an opportunity linked to quote already
        if(quote.Opportunity__c == null){		
            Date cCD = currentContact.CreatedDate.date();
            Opp = new Opportunity();
            Opp.AccountID = quote.Account__c;
            Opp.Name = quote.Account__r.Name;
            Opp.Amount = defaultOpportunityAmount;
            Opp.StageName = 'System Demo';
            Opp.Contract_Length__c = quote.Contract_Length__c;
            
            if(allCampaigns.size()>0 && primaryCampaign.CampaignId != NULL){
                Opp.CampaignId = primaryCampaign.CampaignId;
            }
            if(allCampaigns.size() ==0){
                Opp.CampaignId = unknownCampaign.Id;
            }
            
            Opp.ContactID__c = currentContact.id;
            Opp.Suite_Of_Interest__c = u.Suite_Of_Interest__c;
            Opp.OwnerID = u.ID;
            Opp.Opportunity_Type__c ='New';
            Opp.LeadSource = currentContact.leadSource;
            Opp.Lead_Source_Category__c = currentContact.Lead_Source_Category__c;
            Opp.Lead_Source_Detail__c = currentContact.Lead_Source_Detail__c;
            Opp.Lead_Source_Type__c = currentContact.Lead_Source_Type__c;
            Opp.Last_Interesting_Moment_Date__c = currentContact.mkto_si__Last_Interesting_Moment_Date__c;
            
            if(currentContact.Lead_Created_Date__c != NULL){
                Date cLCD = currentContact.Lead_Created_Date__c.date();
                
                if(currentContact.Lead_Created_Date__c < currentContact.CreatedDate){
                    Opp.Lead_CreatedDate__c = cLCD;
                } else {
                    Opp.Lead_CreatedDate__c = cCD;
                }
            } else {
                Opp.Lead_CreatedDate__c = cCD;
            }
            
            Opp.CloseDate = date.today();
            
            try{
                insert Opp;
                
                //create a primary contact on opportunity based on contact used on quote
                OpportunityContactRole primaryContact = new OpportunityContactRole();
                primaryContact.OpportunityId = Opp.id;
                primaryContact.ContactId = currentContact.id;
                primaryContact.IsPrimary = true;
                insert primaryContact;
                
                //link the new opportunity to quote it was created from
                quote.Opportunity__c = Opp.id;
                update quote;
                
                
            }  catch(Exception e){
                ApexPages.addMessages(e);
            }
        } else {
            
            Opp = [Select Id FROM Opportunity WHERE Id = :quote.Opportunity__c];
            
            
        }
        //query quoteProds to convert into oppLineItems
        Quote_Product__c[] quoteProds = getQuoteItems( Quote.Id );
        
        lineItemUtility.createOppLineItems(quoteId, Opp.id, quoteProds, true);
      
        
        PageReference createContract = Page.create_contract;
        createContract.getParameters().put('id',quote.Opportunity__c); 
        
        createContract.setRedirect(true);
        return createContract;         
    }
    
    public Quote_Product__c[] getQuoteItems( Id quoteId ) {
        
        map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        map<string, Schema.SObjectField> fieldMap = schemaMap.get('Quote_Product__c').getDescribe().fields.getMap();
        string commaSepratedFields = ''; 
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        string additionalFields = ', Bundled_Product__r.ID, Bundled_Product__r.Name, Product__r.Parent_Product__c ';
        string query = 'SELECT '+commaSepratedFields+additionalFields+' FROM Quote_Product__c WHERE Quote__c = \''+quoteId+'\'';
        Quote_Product__c[] items = Database.query(query);
        return items;
    } 
    
    public string getappReasons() {
        string[] rs = new list<string>();
        string reasons = '';
        if (quote.Approve_Expired_Promo_Pricing_Terms__c) {
            rs.add('Pricing terms beyond promo expiration date');
        }
        if (rs.size() > 0) {
            for (string r : rs) {
                reasons += r+',';
            }
        }
        reasons = reasons.removeEnd(',');
        return reasons;
    }
    
    public string getproposedEndDate() {
        string d = string.valueOf(date.today().addDays(60)).split(' ')[0];
        string[] ds = d.split('-');
        return ds[1]+'/'+ds[2]+'/'+ds[0];
    }
    
    public PageReference approveQuote() {
        
        //get the id of the attachment being approved and then clear out the field on the current quote
        string attId = quote.Proposal_in_Approval__c;
        quote.Proposal_in_Approval__c = null;
        quote.Status__c = 'Active';
        
        update quote;
        
        boolean otherApps = false;
        
        //query any other quotes with this attachment 
        Quote__c[] appQuotes = new list<Quote__c>();
        appQuotes.addAll([SELECT id FROM Quote__c WHERE Proposal_in_Approval__c = : attId LIMIT 100]);
        if (appQuotes.size() > 0) {
            otherApps = true;
        }
        
        Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
        req.setComments(appComments);
        req.setAction('Approve');
        
        string work = '';
        for (ProcessInstanceWorkItem workItem : [SELECT p.id FROM ProcessInstanceWorkItem p WHERE p.ProcessInstance.TargetObjectid = : quote.id]) {
            work = string.ValueOf(workItem.id);            
        }
        req.setWorkItemid(work);
        Approval.Processresult result = Approval.process(req);
        
        if (!otherApps) {
            Attachment[] proAtts = [SELECT id, IsPrivate, Description FROM Attachment WHERE id = : attId LIMIT 1];
            if (proAtts.size() > 0) {
                if (proAtts[0].Description != 'Rejected') {
                    proAtts[0].IsPrivate = false;
                    proAtts[0].Description = null;
                }
            }
            update proAtts;
        }
        
        return new PageReference('/'+quote.id+'?app=1').setRedirect(true);
    }
    
    public PageReference rejectQuote() {
        Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
        req.setComments(appComments);
        req.setAction('Reject');
        string work = '';
        for (ProcessInstanceWorkItem workItem : [SELECT p.id FROM ProcessInstanceWorkItem p WHERE p.ProcessInstance.TargetObjectid = : quote.id]) {
            work = string.ValueOf(workItem.id);            
        }
        req.setWorkItemid(work);
        Approval.Processresult result = Approval.process(req);
        
        if (quote.Proposal_in_Approval__c != null) {
            Attachment[] proAtts = [SELECT id, IsPrivate FROM Attachment WHERE id = : quote.Proposal_in_Approval__c LIMIT 1];
            if (proAtts.size() > 0) {
                proAtts[0].Description = 'Rejected';
                proAtts[0].Body = null;
            }
            update proAtts;
        }
        
        quote.Status__c = 'Active';
        update quote;
        
        return new PageReference('/'+quote.id+'?app=0').setRedirect(true);
    }
    
    public PageReference recallQuote() {
        Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
        req.setComments(appComments);
        req.setAction('Removed');
        string work = '';
        for (ProcessInstanceWorkItem workItem : [SELECT p.id FROM ProcessInstanceWorkItem p WHERE p.ProcessInstance.TargetObjectid = : quote.id]) {
            work = string.ValueOf(workItem.id);            
        }
        req.setWorkItemid(work);
        Approval.Processresult result = Approval.process(req);
        
        if (quote.Proposal_in_Approval__c != null) {
            Attachment[] proAtts = [SELECT id, IsPrivate, Description FROM Attachment WHERE id = : quote.Proposal_in_Approval__c LIMIT 1];
            if (proAtts.size() > 0) {
                if (proAtts[0].Description != 'Rejected') {
                    proAtts[0].Description = 'Recalled';
                    proAtts[0].Body = null;
                }
            }
            update proAtts;
        }
        
        quote.Status__c = 'Active';
        update quote;
        
        return new PageReference('/'+quote.id+'?app=2').setRedirect(true);
    }
    
    public void checkApproval() {
        string msgApp = ApexPages.currentPage().getParameters().get('app');
        if (msgApp != null) {
            if (msgApp == '1') {
                msgApprove = true;
                msgReject = false;
                msgRecall = false;
            }
            if (msgApp == '0') {
                msgReject = true;
                msgApprove = false;
                msgRecall = false;
            }
            if (msgApp == '2') {
                msgReject = false;
                msgApprove = false;
                msgRecall = true;
            }
        }
        
        if (quote.Status__c == 'Pending Approval') {
            
            //query the approval process
            ProcessInstanceWorkItem p = [SELECT id, ActorId, OriginalActorId
                                         FROM ProcessInstanceWorkItem 
                                         WHERE ProcessInstance.TargetObjectid = : quote.id];
            
            //check if the current user is the approving user
            if (u.id == p.ActorId) {
                isApprover = true;
            }
        }
        
    }
    
    public PageReference editAttachment() {
        id attId = ApexPages.currentPage().getParameters().get('attId');
        if (attId != null) {
            return new PageReference('/'+attId+'/e').setRedirect(true);
        } else {
            return null;
        }
    }
    
    public PageReference viewAttachment() {
        id attId = ApexPages.currentPage().getParameters().get('attId');
        if (attId != null) {
            return new PageReference('/servlet/servlet.FileDownload?file='+attId).setRedirect(true);
        } else {
            return null;
        }
    }
    
    public PageReference delAttachment() {
        id attId = ApexPages.currentPage().getParameters().get('attId');
        if (attId != null) {
            Attachment att = [SELECT id FROM Attachment WHERE id = : attId LIMIT 1];
            delete att;
            return new PageReference('/'+quote.id).setRedirect(true);
        } else {
            return null;
        }
    }
    
    public Attachment[] getAtts() {
        Attachment[] atts = new list<Attachment>();
        atts.addAll([SELECT id, Name, ContentType, LastModifiedDate, CreatedBy.Name, IsPrivate, Description
                     FROM Attachment
                     WHERE ParentId = : quote.id ORDER BY CreatedDate DESC]);
        return atts;
    }
    
    public void createOptionsWrappers() {
        string SObjectAPIName = 'Quote_Product__c';
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string addFields = ', Product__r.Proposal_Hourly_Rate__c ';
        string query = 'SELECT '+commaSepratedFields+addFields+' FROM '+SObjectAPIName+' WHERE Quote__r.Quote_Options__c = \''+qOps.id+'\' ORDER BY Group_Parent_ID__c ASC, Group_Name__c ASC, Product__r.Sub_Product_Sort_Order__c ASC, Name__c ASC';
        Quote_Product__c[] opShoppingCart = Database.query(query);
        
        quoteOptions = [SELECT id, Name, Option_Name__c, Include_Option_In_Proposal__c, Main_Quote_Option__c, Items_Needing_Approval__c, Contract_Length__c, Items_Pending_Approval__c, Currency__c, Currency_Rate__c
                        FROM Quote__c 
                        WHERE Quote_Options__c = :qops.id 
                        ORDER BY CreatedDate ASC];
        for (Quote__c q : quoteOptions) {
            options.add(new option(q, opShoppingCart));
        } 
    }
    
    public class option {
        public Quote__c quote {get;set;}
        public integer numYears {get;set;}
        public Quote_Product__c[] shoppingCart {get;set;}
        public boolean isSelected {get;set;}
        
        public option(Quote__c xOp, Quote_Product__c[] xShoppingCart) {
            quote = xOp;
            shoppingCart = new List<Quote_Product__c>();
            if (xShoppingCart.size() > 0) {
                for (Quote_Product__c sc : xShoppingCart) {
                    if (sc.Quote__c == quote.id) {
                        shoppingCart.add(sc);
                    }
                }
            }
            if (quote.Main_Quote_Option__c) {
                isSelected = true;
            } else {
                isSelected = false;
            }
            numYears = integer.valueOf(quote.Contract_Length__c.left(1));
        }
    }
    
    public PageReference gotoAttachFile() {
        return new PageReference('/p/attach/NoteAttach?pid='+quote.id+'&parentname='+quote.Name+'&retURL=%2Fapex%2FquoteDetail%3Fid%3D'+quote.id+'%26sfdc.override%3D1').setRedirect(true);
    }
    
    public PageReference fetchQuote(){
        String quoteQuery;
        String SobjectApiName = 'Quote__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        
        string addFields = ', Account__r.Name ';
        
        quoteQuery = 'select ' + commaSepratedFields + addFields +' from ' +  SobjectApiName + ' where ID =\''+ quoteID + '\'';
        quote = Database.query(quoteQuery);        
        return null;
    }
    
    public List<pQuoteProduct> getProducts(){
        if(productList == null) {
            productList = new List<pQuoteProduct>();
            for(Quote_Product__c q: [select Id, Name, Quote__c, Approval_Status__c, 
                                     Approval_Required_by__c from Quote_Product__c where Quote__c =:quote.Id 
                                     AND Approval_Required_by__c != 'N/A' AND Approval_Status__c != 'Approved' AND Approval_Status__c != 'Pending' AND Estimated_Price_Applied__c = FALSE]){
                                         productList.add(new pQuoteProduct(q));
                                     }
        }
        return productList;
    }
    
    public void submitForApproval() {
        try
        {
            //We create a new list of Contacts that we be populated only with Contacts if they are selected
            List<Quote_Product__c> quoteProducts = new List<Quote_Product__c>();
            
            //We will cycle through our list of cContacts and will check to see if the selected property is set to true, if it is we add the Contact to the selectedContacts list
            for(pQuoteProduct qQpa: getProducts()) {
                quoteProducts.add(qQpa.prod);
            }
            quoteProducts.addAll(util.selectedDeferredApprovalQuoteProducts());
            for(Quote_Product__c prod: quoteProducts)
            {
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(prod.Id);
                // submit the approval request for processing
                Approval.ProcessResult result = Approval.process(req);
                // display if the reqeust was successful
                this.xmlMessage = 'Approval Request Submitted Successfully'; 
            }
        }
        
        catch(Exception e)
        {
            ApexPages.addMessages(e);
        }
    }
    
    public class pQuoteProduct {
        public Quote_Product__c prod {get;set;}     
        public Boolean selected {get;set;}
        
        public pQuoteProduct (Quote_Product__c q){
            prod = q;
            selected = false;
        } 
        
    }
    
    public void checkErgo() {
        isErgo = false;
        for (Quote_Product__c qp : shoppingCart) {
            if (qp.Product__r.Ergonomics__c) {
                isErgo = true;
            }
        }
    }
    
    public PageReference buildProposal() {
        if (hasMultiOptions) {
            for (Quote__c q : quoteOptions) {
                if (isErgo) {
                    q.Proposal_Type__c = 'Ergonomics';
                    q.Ergo_Level_2_Hourly_Rate__c = [SELECT Proposal_Hourly_Rate__c FROM Product2 WHERE Name = 'Ergonomics Level 2 Support - Remote' LIMIT 1].Proposal_Hourly_Rate__c;
                    q.Ergo_Level_3_Hourly_Rate__c = [SELECT Proposal_Hourly_Rate__c FROM Product2 WHERE Name = 'Ergonomics Level 3 Support - On-Site' LIMIT 1].Proposal_Hourly_Rate__c;
                }
                if (q.id == quote.id) {
                    q.Include_Option_In_Proposal__c = true;
                    q.Main_Quote_Option__c = true;
                } else {
                    q.Include_Option_In_Proposal__c = false;
                    q.Main_Quote_Option__c = false;
                } 
            }
            update quoteOptions;
        } else {
            quote.Include_Option_In_Proposal__c = true;
            quote.Main_Quote_Option__c = true;
            if (isErgo) {
                quote.Proposal_Type__c = 'Ergonomics';
                quote.Ergo_Level_2_Hourly_Rate__c = [SELECT Proposal_Hourly_Rate__c FROM Product2 WHERE Name = 'Ergonomics Level 2 Support - Remote' LIMIT 1].Proposal_Hourly_Rate__c;
                quote.Ergo_Level_3_Hourly_Rate__c = [SELECT Proposal_Hourly_Rate__c FROM Product2 WHERE Name = 'Ergonomics Level 3 Support - On-Site' LIMIT 1].Proposal_Hourly_Rate__c;
            }
            update quote;
        }
        return new PageReference('/apex/quotePrintOptions?id='+quote.id).setRedirect(true);
    }
    
    //Page reference/data setup for Quote5.0 
    public PageReference buildNewProposal() {
        if (hasMultiOptions) {
            for (Quote__c q : quoteOptions) {
                if (isErgo) {
                    q.Proposal_Type__c = 'Ergonomics';
                    q.Ergo_Level_2_Hourly_Rate__c = [SELECT Proposal_Hourly_Rate__c FROM Product2 WHERE Name = 'Ergonomics Level 2 Support - Remote' LIMIT 1].Proposal_Hourly_Rate__c;
                    q.Ergo_Level_3_Hourly_Rate__c = [SELECT Proposal_Hourly_Rate__c FROM Product2 WHERE Name = 'Ergonomics Level 3 Support - On-Site' LIMIT 1].Proposal_Hourly_Rate__c;
                }
                if (q.id == quote.id) {
                    q.Include_Option_In_Proposal__c = true;
                    q.Main_Quote_Option__c = true;
                } else {
                    q.Include_Option_In_Proposal__c = false;
                    q.Main_Quote_Option__c = false;
                } 
            }
            update quoteOptions;
        } else {
            quote.Include_Option_In_Proposal__c = true;
            quote.Main_Quote_Option__c = true;
            if (isErgo) {
                quote.Proposal_Type__c = 'Ergonomics';
                quote.Ergo_Level_2_Hourly_Rate__c = [SELECT Proposal_Hourly_Rate__c FROM Product2 WHERE Name = 'Ergonomics Level 2 Support - Remote' LIMIT 1].Proposal_Hourly_Rate__c;
                quote.Ergo_Level_3_Hourly_Rate__c = [SELECT Proposal_Hourly_Rate__c FROM Product2 WHERE Name = 'Ergonomics Level 3 Support - On-Site' LIMIT 1].Proposal_Hourly_Rate__c;
            }
            update quote;
        }
        return new PageReference('/apex/proposal_builder_page_selection?id='+quote.id).setRedirect(true);
    }

    public PageReference save(){
        Quote__c saveQuote = (Quote__c)this.controller.getRecord();
        if(saveQuote.Currency__c != Quote.Currency__c && saveQuote.Currency__c != null){
            DatedConversionRate[] rate = [SELECT id, ConversionRate, IsoCode
                                    FROM DatedConversionRate
                                    WHERE StartDate = TODAY
                                    AND IsoCode = :saveQuote.Currency__c LIMIT 1];
            if(rate.size() > 0){
                saveQuote.Currency_Rate__c = rate[0].ConversionRate;
            }
        }
        this.controller.save();
        return ApexPages.currentPage().setRedirect(true);
    }
    
    
    
    public PageReference backToQuote(){
        PageReference quoteDetail = Page.quoteDetail;
        quoteDetail.setRedirect(true);
        quoteDetail.getParameters().put('id',Quote.id); 
        return quoteDetail; 
    }
    
    public PageReference refreshOpportunity(){
        PageReference refreshOpportunity = Page.quote_refresh_opportunity;
        refreshOpportunity.setRedirect(true);
        refreshOpportunity.getParameters().put('ID',Quote.id); 
        refreshOpportunity.getParameters().put('oID',Quote.Opportunity__c); 
        return refreshOpportunity; 
    }
    
    public PageReference cloneQuote(){
        PageReference quoteClone = Page.quoteClone;
        quoteClone.setRedirect(true);
        quoteClone.getParameters().put('ID',Quote.id); 
        return quoteClone; 
    }
    
    public PageReference approval(){
        submitForApproval(); 
        this.xmlMessage = 'Approval Request Submitted Successfully'; 
        PageReference quoteDetail = Page.quoteDetail;
        quoteDetail.setRedirect(true);
        quoteDetail.getParameters().put('id',Quote.id); 
        return quoteDetail; 
    } 
    
    public PageReference step2(){
        PageReference step2 = Page.quoteProductEntryStep2;
        step2.setRedirect(true);
        step2.getParameters().put('id',Quote.id); 
        return step2; 
    }
    
    public PageReference addProducts(){
        PageReference addProducts = Page.quoteProductEntry;
        addProducts.setRedirect(true);
        addProducts.getParameters().put('id',Quote.id); 
        return addProducts; 
    }
    
    public PageReference convertQuote(){
        PageReference convertQuote = Page.create_oppty;
        convertQuote.setRedirect(true);
        convertQuote.getParameters().put('id',Quote.id);  
        return convertQuote; 
    }
    
    
    
    
}