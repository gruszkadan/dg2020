@isTest
public class testCancellationHumantech {
    
    @isTest  static void test1(){        
        Account acct= new Account();
        acct.Name = 'Test Account';
        insert acct;
        
        Order_Item__c a = new Order_Item__c();
        a.account__c = acct.ID;
        a.Name = 'Test';
        a.Invoice_Amount__c = 10;
        a.Order_Date__c = date.today();
        a.Order_ID__c = 'test123';
        a.Order_Item_ID__c = 'test123';
        a.Year__c = string.valueOf(date.today().year()); 
        a.Month__c = datetime.now().format('MMMMM');
        a.Contract_Length__c = 3;
        a.AccountID__c = 'test123';
        a.Term_Start_Date__c = date.today();
        a.Term_End_Date__c = date.today().addYears(1);
        a.Term_Length__c = 1;
        a.Contract_Number__c = 'test123';
        a.Sales_Location__c = 'Chicago';
        a.Admin_Tool_Order_Type__c = 'New';
        a.Admin_Tool_Product_Name__c = 'Humantech Office Ergonomics - Proof of Concept';
        a.Admin_Tool_Order_Status__c = 'A';
        a.Product_Platform__c = 'Ergonomics'; 
        a.Product_Suite__c = 'Ergonomics';
        insert a;
        
        
        Order_Item__c i = new Order_Item__c();
        i.account__c = acct.ID;
        i.Name = 'Test2';
        i.Invoice_Amount__c = 10;
        i.Order_Date__c = date.today();
        i.Order_ID__c = 'test1234';
        i.Order_Item_ID__c = 'test1234';
        i.Year__c = string.valueOf(date.today().year()); 
        i.Month__c = datetime.now().format('MMMMM');
        i.Contract_Length__c = 3;
        i.AccountID__c = 'test123';
        i.Term_Start_Date__c = date.today();
        i.Term_End_Date__c = date.today().addYears(1);
        i.Term_Length__c = 1;
        i.Contract_Number__c = 'test123';
        i.Sales_Location__c = 'Chicago';
        i.Admin_Tool_Order_Type__c = 'New';
        i.Admin_Tool_Product_Name__c = 'Humantech Office Ergonomics - Proof of Concept';
        i.Admin_Tool_Order_Status__c = 'A';
        i.Product_Platform__c = 'Ergonomics';
        i.Product_Suite__c = 'Ergonomics';
        i.Cancellation_Reason__c = 'Stopped Responding';
        i.Cancellation_Date__c = date.today();
        i.Site_Shutdown_Date__c = date.today().addDays(2);       
        insert i;
        
        
        ApexPages.currentPage().getParameters().put('id', acct.Id);
        orderCancellationHumantech myController = new orderCancellationHumantech();
        
        myController.getReasons();
        
        myController.cancellationReason = 'Feature';
        System.assert(myController.oiList.size() > 0);
        //System.assert(myController.cancelledOIs.size() > 0);
        myController.clearOtherReason();
        
        myController.oiList[0].isSelected = true;
        myController.oiList[0].oi.Site_Shutdown_Date__c = date.today().addDays(7);  
        myController.oiList[0].oi.Cancellation_Date__c = date.today().addDays(7);  
        myController.processCancellations();
        
        
    }
}