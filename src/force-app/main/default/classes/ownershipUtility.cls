public class ownershipUtility {
    
    
    public static void updateOwnership(list<sObject> records, map<string, County_Zip_Code__c> zipMap, string sObjectType, string action) {
        //list of our special countries
        string specialCountries = 'Afghanistan;Armenia;Australia;Azerbaijan;Bahrain;Bangladesh;Bhutan;Brunei;Burma;Cambodia;China;Cyprus;Egypt;Fiji;Georgia;Hong Kong;India;Indonesia;Iran;Iraq;Israel;Japan;Jordan;Kazakhstan;Kiribati;North Korea;South Korea;Kuwait;Kyrgyzstan;Laos;Lebanon;Macau;Malaysia;Maldives;Marshall Islands;Mongolia;Nauru;Nepal;New Zealand;Oman;Pakistan;Palau;Palestine;Papua New Guinea;Philippines;Qatar;Samoa;Saudi Arabia;Singapore;Solomon Islands;Sri Lanka;Syria;Taiwan;Tajikistan;Thailand;Tonga;Turkey;Turkmenistan;Tuvalu;UAE;United Arab Emirates;Uzbekistan;Vanuatu;Vietnam;Yemen;';
        
        //list of SLED NAICS codes
        string sledNAICScodes = '61 - Educational Services;92 - Public Administration (Fed/State/Local Government)';
        
        //grab the default owners and assign them
        leadCustomSettings lcd = new leadCustomSettings();
        leadCustomSettings.defaultOwners def = lcd.findDefaultOwners();
        
        if (sObjectType == 'Lead') {
            Lead[] entrrLeads = new list<Lead>();
            for (Lead l : (list<Lead>)records) {
                boolean specialCountry = false;
                if (l.Country != null) {
                    if (specialCountries.contains(l.Country)) {
                        specialCountry = true;
                    }
                }
                boolean enterprise = l.Enterprise_Sales_Lead__c;
                boolean hasTerritory = false;
                if (l.Territory__c != null) {
                    hasTerritory = true;
                } 
                if (specialCountry) {
                    if (enterprise) {
                        // *** SPECIAL COUNTRY, ENTERPRISE
                        //if its an insert, assign the default owners. if an update, make no changes
                        if (action == 'insert') {
                            entrrLeads.add(l);
                            l.Online_Training_Owner__c = def.defOTOwner;
                            l.Authoring_Owner__c = def.defAuthOwner;
                            l.EHS_Owner__c = def.specialEHSOwner;
                            //     l.Ergonomics_Owner__c = def.defErgoOwner;
                            l.Default_Owners_Assigned__c = true;
                        }
                    } else {
                        // *** SPECIAL COUNTRY, NOT ENTERPRISE
                        //if its an insert, assign the default owners. if an update, make no changes
                        if (action == 'insert') {
                            l.Chemical_Management_Owner__c = def.defInternationalOwner;
                            l.Online_Training_Owner__c = def.defOTOwner;
                            l.Authoring_Owner__c = def.defAuthOwner;
                            l.EHS_Owner__c = def.specialEHSOwner;
                            //      l.Ergonomics_Owner__c = def.defErgoOwner;
                            l.Default_Owners_Assigned__c = true;
                        }
                    }
                } else {
                    if (enterprise && hasTerritory) {
                        // *** ENTERPRISE, TERRITORY
                        //if its an insert, assign owners normally
                        if (action == 'insert') {
                            entrrLeads.add(l);
                            County_Zip_Code__c zip;
                            if(l.Country == 'United States'){
                                zip = zipMap.get(l.PostalCode.left(5));
                            }else{
                                zip = zipMap.get(l.PostalCode);  
                            }
                            if (zip == null) {
                                zip = zipMap.get(l.PostalCode.left(3));
                            }
                            l.Online_Training_Owner__c = zip.Territory__r.Online_Training_Owner__c;
                            l.Authoring_Owner__c = zip.Territory__r.Authoring_Owner__c;
                            l.EHS_Owner__c = def.entEHSOwner;
                            //        l.Ergonomics_Owner__c = zip.Territory__r.Ergonomics_Owner__c;
                        } else {
                            //if its an update and has default owners, update the training, authoring, and ergo owners
                            County_Zip_Code__c zip;
                            if(l.Country == 'United States'){
                                zip = zipMap.get(l.PostalCode.left(5));
                            }else{
                                zip = zipMap.get(l.PostalCode);  
                            }
                            if (zip == null) {
                                zip = zipMap.get(l.PostalCode.left(3));
                            }
                            l.Online_Training_Owner__c = zip.Territory__r.Online_Training_Owner__c;
                            l.Authoring_Owner__c = zip.Territory__r.Authoring_Owner__c;
                            //l.Ergonomics_Owner__c = zip.Territory__r.Ergonomics_Owner__c;
                            l.Default_Owners_Assigned__c = false;
                        }
                    } else if (enterprise && !hasTerritory) {
                        // *** ENTERPRISE, NO TERRITORY
                        //if its an insert, assign owners normally
                        if (action == 'insert') {
                            entrrLeads.add(l);
                            l.Online_Training_Owner__c = def.defOTOwner;
                            l.Authoring_Owner__c = def.defAuthOwner;
                            l.EHS_Owner__c = def.entEHSOwner;
                            //l.Ergonomics_Owner__c = def.defErgoOwner;
                            l.Default_Owners_Assigned__c = true;
                        } 
                    } else if (!enterprise && hasTerritory) {
                        // *** NOT ENTERPRISE, TERRITORY
                        County_Zip_Code__c zip;
                        if(l.Country == 'United States'){
                            zip = zipMap.get(l.PostalCode.left(5));
                        }else{
                            zip = zipMap.get(l.PostalCode);  
                        }
                        if (zip == null) {
                            zip = zipMap.get(l.PostalCode.left(3));
                        }
                        if(l.NAICS_Code__c != null && zip.Territory__r.SLED_Owner__c != null && sledNAICScodes.contains(l.NAICS_Code__c)){
                            l.Chemical_Management_Owner__c = zip.Territory__r.SLED_Owner__c;
                        }else{
                            l.Chemical_Management_Owner__c = zip.Territory__r.Territory_Owner__c;
                        }
                        l.Online_Training_Owner__c = zip.Territory__r.Online_Training_Owner__c;
                        l.Authoring_Owner__c = zip.Territory__r.Authoring_Owner__c;
                        l.EHS_Owner__c = zip.Territory__r.EHS_Owner__c;
                        //    l.Ergonomics_Owner__c = zip.Territory__r.Ergonomics_Owner__c;
                        if (action != 'insert') {
                            l.Default_Owners_Assigned__c = false;
                        }
                    } else if (!enterprise && !hasTerritory) {
                        if (action == 'insert') {
                            // *** NOT ENTERPRISE, NO TERRITORY
                            //if its an insert, assign default owners
                            if(l.Country != null && l.Country != 'United States' && l.Country != 'Canada'){
                                l.Chemical_Management_Owner__c = def.defInternationalOwner;
                            }else{
                                l.Chemical_Management_Owner__c = def.defChemMgtOwner;
                            }
                            l.Online_Training_Owner__c = def.defOTOwner;
                            l.Authoring_Owner__c = def.defAuthOwner;
                            l.EHS_Owner__c = def.defEHSOwner;
                            //   l.Ergonomics_Owner__c = def.defErgoOwner;
                            l.Default_Owners_Assigned__c = true;
                        }
                    }
                }
                l.OwnerId = l.Chemical_Management_Owner__c;
            }
            
            //if any leads need round-robin assignment, send them
            if (entrrLeads.size() > 0) {
                roundRobinAssignment.roundRobinOwnershipAssignment(entrrLeads,'Lead','Enterprise');
            }
            
            ergonomicsOwnershipAssignment(records,'Lead','Ergonomics',def.defErgoOwner);
            
            
        } else if (sObjectType == 'Account') {
            
            Map<ID, Account> parentAcctMap = new Map<ID, Account>();            
            Account[] AccountsNeedingErgonomicAssignment = new List<Account>();
            
            if (System.UserInfo.getName() != 'Territory Reassignment') {                
                
                id autosim = [SELECT id FROM User WHERE Alias = 'autosim' LIMIT 1].id;
                User u = [SELECT id, Validation_Rules_Active__c, Group__c FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
                
                Id[] parents = new List<Id>();
                
                for (Account a : (list<Account>)records) {
                    if(a.parentid != NULL){
                        parents.add(a.parentid);
                    }
                }
                
                if(parents.size() > 0){
                    parentAcctMap = new Map<ID, Account>([SELECT Id, OwnerId, Parent.OwnerId, Parent.Parent.OwnerId,
                                                          Parent.Parent.Parent.OwnerId,
                                                          Parent.Parent.Parent.Parent.OwnerId,
                                                          Parent.Parent.Parent.Parent.Parent.OwnerId,
                                                          Ergonomics_Account_Owner__c,
                                                          Parent.Ergonomics_Account_Owner__c,
                                                          Parent.Parent.Ergonomics_Account_Owner__c,
                                                          Parent.Parent.Parent.Ergonomics_Account_Owner__c,
                                                          Parent.Parent.Parent.Parent.Ergonomics_Account_Owner__c,
                                                          Parent.Parent.Parent.Parent.Parent.Ergonomics_Account_Owner__c
                                                          FROM Account 
                                                          WHERE Id = :parents]);
                }
                
                // LOOKUP city and state by zip 
                for (Account account : (list<Account>)records) {   
                    
                    if(action == 'insert'){
                        Account.OwnerId = NULL;
                    }                                       
                    
                    County_Zip_Code__c zip = null;
                    if(account.BillingPostalCode != NULL){
                        if (account.BillingCountry == 'Canada' && account.BillingPostalCode != null) {
                            if (zipMap.get(account.BillingPostalCode) != null) {
                                zip = zipMap.get(account.BillingPostalCode);
                            } else {
                                zip = zipMap.get(account.BillingPostalCode.left(3));
                            }
                        } else if(account.BillingCountry == 'United States'){
                            zip = zipMap.get(account.BillingPostalCode.left(5));
                        }
                    }
                    if (zip != null) {
                        
                        if ((Account.Enterprise_Sales__c == false && action == 'insert') || (Account.OwnerId == autosim && Account.Customer_Status__c != 'Out of Business') || (u.Group__c == 'Lead Development' && Account.Enterprise_Sales__c == false))  {
                            if(account.NAICS_Code__c != null && zip.Territory__r.SLED_Owner__c != null && sledNAICScodes.contains(account.NAICS_Code__c)){
                                account.OwnerId = zip.Territory__r.SLED_Owner__c;
                            }else{
                                account.OwnerId = zip.Territory__r.Territory_Owner__c;
                            }
                        }
                        
                        if (account.Authoring_Account_Owner__c == NULL || (Account.Authoring_Account_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                            Account.Authoring_Account_Owner__c = zip.Territory__r.Authoring_Owner__c; 
                        }
                        if (account.Online_Training_Account_Owner__c == NULL || (Account.Online_Training_Account_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                            Account.Online_Training_Account_Owner__c = zip.Territory__r.Online_Training_Owner__c; 
                        }
                        //if (account.Ergonomics_Account_Owner__c == NULL || (Account.Ergonomics_Account_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                        //     Account.Ergonomics_Account_Owner__c = zip.Territory__r.Ergonomics_Owner__c;
                        // }
                        if ((account.EHS_Owner__c == NULL && Account.Enterprise_Sales__c == false) || (Account.EHS_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                            Account.EHS_Owner__c = zip.Territory__r.EHS_Owner__c;
                        }
                    }
                    //NEW
                    //once territory logic has run, if no owner was found, set the owners to the default values. IF enterprise and it has a parent, set Chem SE owner to match the ultimate parent
                    //If no territory is found (no zip or no match), set defaults:
                    //meed something regarding Account.Customer_Status__c != 'Out of Business')
                    if(Account.Enterprise_Sales__c == true  && (action == 'insert' || (Account.OwnerId == autosim && Account.Customer_Status__c != 'Out of Business'))){
                        if(Account.ParentId != null){
                            //Account.OwnerId = getTopParentsOwner(parentAcctMap.get(Account.ParentId));
                            Account.OwnerId = getTopParentsOwner(parentAcctMap.get(Account.ParentId));
                        }else{
                            Account.OwnerId = def.defESOwner;
                        }
                    }                    
                    
                    //After everythint else has been set, if we still don't have an owner, set the owner to the default
                    if( account.OwnerId == NULL || (Account.OwnerId == autosim && Account.Customer_Status__c != 'Out of Business')){
                        if(account.BillingCountry != null && account.BillingCountry != 'United States' && account.BillingCountry != 'Canada'){
                            Account.OwnerId = def.defInternationalOwner;
                        }else{
                            Account.OwnerId = def.defChemMgtOwner;
                        }
                    }
                    
                    //test classes may not be querying/adding lead default owners; thus, owner could be null.
                    if(Account.OwnerId == NULL){    
                    	  Account.OwnerId = u.id;
                    }
                    if(Account.ParentId != null && Account.Ergonomics_Account_Owner__c == NULL){
                        Account.Ergonomics_Account_Owner__c = getTopErgoOwner(parentAcctMap.get(Account.ParentId));                      
                    }                    
                    if(FeatureManagement.checkPermission('Ergo_Owner_on_Account_Creation') && Account.Ergonomics_Account_Owner__c == NULL){
                        Account.Ergonomics_Account_Owner__c = u.id;
                    }
                    if(Account.Ergonomics_Account_Owner__c == NULL || (Account.Ergonomics_Account_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')){
                        AccountsNeedingErgonomicAssignment.add(Account);
                        //Add account to list to be sent to the ergo match method. Since method overrides the current ergo owner if its set, go ahead and set owner to default ergo owner
                        //Any account that do not find a match will have this owner
                        Account.Ergonomics_Account_Owner__c = def.defErgoOwner;
                    }
                    
                    if (account.Authoring_Account_Owner__c == NULL || (Account.Authoring_Account_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                        Account.Authoring_Account_Owner__c = def.defAuthOwner;
                    }
                    if (account.Online_Training_Account_Owner__c == NULL || (Account.Online_Training_Account_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                        Account.Online_Training_Account_Owner__c = def.defOTOwner;
                    }
                    if ((account.EHS_Owner__c == NULL && Account.Enterprise_Sales__c == false) || (Account.EHS_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                        Account.EHS_Owner__c = def.defEHSOwner;
                    }
                    
                    //End of New
                }
                
                if (action != 'insert'){
                    Contact[] cons = [SELECT id, Territory__c, AccountId FROM Contact WHERE AccountId IN :(list<Account>)records];
                    Contact[] cons_upd = new List<Contact>();
                    if (cons != null)
                    {
                        for (Account acct : (list<Account>)records)
                        {
                            for (Contact con : cons)
                            {
                                if (con.AccountId == acct.id)
                                {
                                    con.Territory__c = acct.Territory__c;
                                    cons_upd.add(con);
                                }
                            }
                        }
                        update cons_upd;
                    }
                    Opportunity[] opps = [SELECT id, Territory__c, AccountId FROM Opportunity WHERE AccountId IN :(list<Account>)records];
                    Opportunity[] opps_upd = new List<Opportunity>();
                    if (opps != null)
                    {
                        for (Account acct : (list<Account>)records)
                        {
                            for (Opportunity opp : opps)
                            {
                                if (opp.AccountId == acct.id)
                                {
                                    opp.Territory__c = acct.Territory__c;
                                    opps_upd.add(opp);
                                }
                            }
                        }
                        update opps_upd;
                    }
                    u.Validation_Rules_Active__c = true;
                    update u;
                }
                if(AccountsNeedingErgonomicAssignment.size() > 0){
                    ergonomicsOwnershipAssignment(AccountsNeedingErgonomicAssignment,'Account','Ergonomics',def.defErgoOwner);
                }
                
            }else{
                //ergonomicsOwnershipAssignment(records,'Account','Ergonomics');
            }
        }
        
    }
    
    public static string getTopParentsOwner(Account a){
        if(a.Parent.Parent.Parent.Parent.Parent.OwnerId != NULL){
            return a.Parent.Parent.Parent.Parent.Parent.OwnerId;
        }
        if(a.Parent.Parent.Parent.Parent.OwnerId != NULL){
            return a.Parent.Parent.Parent.Parent.OwnerId;
        }
        if(a.Parent.Parent.Parent.OwnerId != NULL){
            return a.Parent.Parent.Parent.OwnerId;
        }
        if(a.Parent.Parent.OwnerId != NULL){
            return a.Parent.Parent.OwnerId;
        }
        if(a.Parent.OwnerId != NULL){
            return a.Parent.OwnerId;
        }
        if(a.OwnerId != NULL){
            return a.OwnerId;
        }else{
            return null;
        }
        
    }
    
    public static string getTopErgoOwner(Account a){
        if(a.Parent.Parent.Parent.Parent.Parent.Ergonomics_Account_Owner__c != NULL){
            return a.Parent.Parent.Parent.Parent.Parent.Ergonomics_Account_Owner__c;
        }
        if(a.Parent.Parent.Parent.Parent.Ergonomics_Account_Owner__c != NULL){
            return a.Parent.Parent.Parent.Parent.Ergonomics_Account_Owner__c;
        }
        if(a.Parent.Parent.Parent.Ergonomics_Account_Owner__c != NULL){
            return a.Parent.Parent.Parent.Ergonomics_Account_Owner__c;
        }
        if(a.Parent.Parent.Ergonomics_Account_Owner__c != NULL){
            return a.Parent.Parent.Ergonomics_Account_Owner__c;
        }
        if(a.Parent.Ergonomics_Account_Owner__c != NULL){
            return a.Parent.Ergonomics_Account_Owner__c;
        }
        if(a.Ergonomics_Account_Owner__c != NULL){
            return a.Ergonomics_Account_Owner__c;
        }else{
            return null;
        }
        
    }
    
   
    
    public static void updateEHSEnterpriseOwner(Account[] accounts){
        Enterprise_Account_Assignments__c[] eaas = [SELECT id, Name, Industry__c, OwnerId__c, EHS_OwnerId__c FROM Enterprise_Account_Assignments__c];
        id KMIowner = [SELECT id FROM User WHERE LastName = 'Richey' AND FirstName = 'Brian' AND isActive = true LIMIT 1].id;
        for (Account acc : accounts) {
            if (acc.Enterprise_Sales__c == true){
                boolean ownerFound = false;
                if (acc.Enterprise_Sales_Industry__c != null){
                    for (Enterprise_Account_Assignments__c eaa : eaas){
                        if (acc.Enterprise_Sales_Industry__c == eaa.Industry__c && ownerFound == false){
                            acc.EHS_Owner__c = eaa.EHS_OwnerId__c;
                            ownerFound = true;
                        }
                    }
                    if (ownerFound == false){
                        acc.EHS_Owner__c = kmiOwner; 
                    }
                }
                else{
                    acc.EHS_Owner__c = kmiOwner;
                }
            }
        }
    }
    
    public static Id findDefaultChemManagementOwner(){
        leadCustomSettings lcd = new leadCustomSettings();
        leadCustomSettings.defaultOwners def = lcd.findDefaultOwners();
        return def.defChemMgtOwner;
    }
    
    
    
    public static void ergonomicsOwnershipAssignment(list<sObject> records,string sObjectType, string OwnerType, id defaultErgoOwner){       
        
        //Initialize Sobject fields being used in this method 
        sObjectField Company;
        sObjectField Owner;
        sObjectField EmailDomain;
        
        //Set sObjectField values for both objects lead and account
        If(sObjectType == 'Lead'){  
            Company = Lead.Company;
            Owner = Lead.Ergonomics_Owner__c;
            EmailDomain = Lead.domain__c;
        }else if (sObjectType == 'Account'){
            Company = Account.Name;
            Owner = Account.Ergonomics_Account_Owner__c;
        }
        //Intialize maps for company names and contacts 
        set<string> companyNames = new set<string>();
        set<string> EmailDomains = new set<string>();
        
        //Loop through sobject records for CompanyNames and EmailDomians then add them to a set to avoid duplicates 
        for(sObject record : records){
            companyNames.add((string)record.get(Company));            
            if(Sobjecttype == 'Lead'){
                EmailDomains.add((String)record.get(EmailDomain));                
            }
        }
        
        //Query Accounts where ergonimics owner is null and the account name is in our 'CompanyNames' set
        list<Account> ACT = [SELECT id, name, Ergonomics_Account_Owner__c
                             FROM Account
                             Where name in: companyNames and Ergonomics_Account_Owner__c != null and Customer_status__c != 'Out of Business' and Ergonomics_Account_Owner__c !=: defaultErgoOwner];        
        
        //Intialize maps that we will trasfer our data too to keep track of name and ergoOwner/emailDomian 
        Map<String,String> AccountMap = new map <String,String>();
        Map<String,String> ContactMap = new map <String,String>();
        
        //Loop through account query and insert/put account name with ergonomics owner into AccountMap
        for(Account a : ACT){
            AccountMap.put(a.name,a.Ergonomics_Account_Owner__c);
        }
        
        //leads only match on email domains; thus, only query contacts when sObject == lead
        if(SobjectType == 'Lead'){
            //Query contatcs where ergonomics owner is null and emaildomin is in our 'EmailDomians' set
            List<Contact> CON = [Select id, name, Indexed_Email_Domain__c, Account.Ergonomics_Account_Owner__c
                                 From Contact 
                                 Where Indexed_Email_Domain__c in: EmailDomains and Account.Ergonomics_Account_Owner__c != null and Indexed_Email_Domain__c != null];
            
            //Loop through contact query and insert/put contact name and email domin intp ContactMap
            for(Contact c : CON){
                ContactMap.put(c.Indexed_Email_Domain__c, c.Account.Ergonomics_Account_Owner__c);
            }
        }
        //Scenario 1 match account name on lead company name to set lead ergo owner from account ergo owner 
        //Loop through records see if the map contains company name 
        //If it does insert/put ergo owner for that specfic account into map 
        for(sObject r : records){
            if(AccountMap.containsKey((string)r.get(Company))){
                r.put(Owner, AccountMap.get((string)r.get(Company)));
            }
            
            //Scenario 2 match on contact email domain to set ergo lead owner 
            //If owner is still null and object is lead
            //Check map too see if emaildomain matches on lead 
            //If it does insert/put owner/emaildomin in map 
            
            
            if(r.get(Owner) == null && SobjectType == 'Lead'){                
                if(ContactMap.containskey((String)r.get(EmailDomain))){
                    r.put(owner, ContactMap.get((string)r.get(EmailDomain)));                    
                }
            }                
            //Scenario 3 send to round robin to assign lead ergo owner 
            //If owner is still null we would like to pass records into round robin assigment class 
            ////updated temporarily 
            if(r.get(owner) == null && sObjectType == 'Lead'){                
                roundRobinAssignment.roundRobinOwnershipAssignment(records, sObjectType, OwnerType);
            }
        }   
    }
}