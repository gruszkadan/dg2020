@isTest(seeAllData=true)
private class testLeadNewController {
    
    static testMethod void test1() {
        
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.FirstName = 'Al';
        testContact.LastName = 'Powell';
        testContact.Accountid = testAccount.id;
        testContact.Communication_Channel__c = 'Marketing Prospecting';
        testContact.LinkedIn_Profile__c = 'www.google.com';
        insert testContact;
        
        ApexPages.currentPage().getParameters().put('contactId', testContact.id);
        
        ApexPages.StandardController testController = new ApexPages.StandardController(new Lead());
        leadNewController con = new leadNewController(testController);
        
        con.newLead.Company = 'Best Buy';
        con.saveLead();
        
        //test that Lead Fields are filled in appropriately from contact info
        system.assertEquals(con.newLead.LinkedIn_Profile__c, testContact.LinkedIn_Profile__c);  
        system.assertEquals(con.newLead.FirstName, testContact.FirstName);
        system.assertEquals(con.newLead.LastName, testContact.LastName);
        system.assertEquals(con.newLead.Former_Administrator_Contact__c, testContact.id);
        
        Contact testContact2 = [Select New_Lead_Id__c From Contact WHERE id = :testContact.id];
        
        //test that new lead ID field is filled in from newly inserted Lead
        system.assertEquals(testContact2.New_Lead_ID__c, con.newLead.id);
        
        
        
    }
    
}