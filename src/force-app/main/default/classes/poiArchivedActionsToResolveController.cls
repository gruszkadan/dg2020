public class poiArchivedActionsToResolveController {
    id sfId;
    string objType;
    public string recordName {get;set;}
    public session[] msdsSessions {get;set;}
    public session[] ehsSessions {get;set;}
    public session[] authSessions {get;set;}
    public session[] odtSessions {get;set;}
    public session[] ergoSessions {get;set;}
    
    public poiArchivedActionsToResolveController() {
        sfId = apexPages.currentPage().getParameters().get('id');
        if (string.valueOf(sfId).left(3) == '001') {
            objType = 'Account';
            recordName = [SELECT Name FROM Account WHERE id = : sfId LIMIT 1].Name;
        } else {
            objType = 'Lead';
            recordName = [SELECT Name FROM Lead WHERE id = : sfId LIMIT 1].Name;
        }
        msdsSessions = new list<session>();
        ehsSessions = new list<session>();
        authSessions = new list<session>();
        odtSessions = new list<session>();
        ergoSessions = new list<session>();
        findCreateSessions();
    }
    
    public void findCreateSessions() {
        session[] tempSessions = new list<session>();
        string q = 'SELECT id, Product__c, Action__c, Action_Status__c, Needs_Resolution__c, Product_Suite__c, Session_Open__c, Product_Action_Session__c, Product_Of_Interest__c, Closed_By__c, Closed_By__r.Name ';
        if (objType == 'account') {
            q += ', Contact__c, Contact__r.Name, Contact__r.Title, Contact__r.Email, Contact__r.Phone '+
                'FROM Product_Of_Interest_Action__c WHERE Contact__r.AccountId = \''+sfId+'\' ';
        } else {
            q += ', Lead__c, Lead__r.Name, Lead__r.Title, Lead__r.Email, Lead__r.Phone, Lead__r.Company '+
                'FROM Product_Of_Interest_Action__c WHERE Lead__c = \''+sfId+'\' ';
        }
        q += 'AND Action_Status__c != \'Open\' ';
        if (objType == 'account') {
            q += 'ORDER BY Contact__r.LastName, CreatedDate DESC LIMIT 1000';
        } else {
            q += 'ORDER BY CreatedDate DESC LIMIT 1000';
        }
        
        Product_Of_Interest_Action__c[] allpoias = Database.query(q);
        if (allpoias.size() > 0) {
            set<string> sessionNums = new set<string>();
            for (Product_Of_Interest_Action__c poia : allpoias) {
                sessionNums.add(poia.Product_Action_Session__c);
            }
            if (sessionNums.size() > 0) {
                for (string s : sessionNums) {
                    Product_Of_Interest_Action__c[] poias = new list<Product_Of_Interest_Action__c>();
                    for (Product_Of_Interest_Action__c poia : allpoias) {
                        if (poia.Product_Action_Session__c == s) {
                            poias.add(poia);
                        }
                    }
                    if (poias.size() > 0) {
                        tempSessions.add(new session(poias, objType));
                    }
                }
            }
        }
        
        //once we have our temp list, reorder it
        if (tempSessions.size() > 0) {
            suiteSessions(tempSessions);
        }
    }
    
    public void suiteSessions(session[] tempSessions) {
        // Create our temp sessions. These lists split out the sessions so we can re-order them after
        session[] tempmsdsSessions = new list<session>();
        session[] tempehsSessions = new list<session>();
        session[] tempauthSessions = new list<session>();
        session[] tempodtSessions = new list<session>();
        session[] tempergoSessions = new list<session>();
        for (session s : tempSessions) {
            if (s.suite == 'MSDS') {
                tempmsdsSessions.add(s);
            } else if (s.suite == 'EHS') {
                tempehsSessions.add(s);
            } else if (s.suite == 'AUTH') {
                tempauthSessions.add(s);
            } else if (s.suite == 'ODT') {
                tempodtSessions.add(s);
            } else if (s.suite == 'ERGO') {
                tempergoSessions.add(s);
            }
        }
        // MSDS
        if (tempmsdsSessions.size() > 0) {
            for (session s : tempmsdsSessions) {
                if (s.status == 'Working - Pending Response') {
                    msdsSessions.add(s);
                }
            }
            for (session s : tempmsdsSessions) {
                if (s.status == 'Working - Connected') {
                    msdsSessions.add(s);
                }
            }
            for (session s : tempmsdsSessions) {
                if (s.status != 'Working - Pending Response' && s.status != 'Working - Connected') {
                    msdsSessions.add(s);
                }
            }
        }
        // EHS
        if (tempehsSessions.size() > 0) {
            for (session s : tempehsSessions) {
                if (s.status == 'Working - Pending Response') {
                    ehsSessions.add(s);
                }
            }
            for (session s : tempehsSessions) {
                if (s.status == 'Working - Connected') {
                    ehsSessions.add(s);
                }
            }
            for (session s : tempehsSessions) {
                if (s.status != 'Working - Pending Response' && s.status != 'Working - Connected') {
                    ehsSessions.add(s);
                }
            }
        }
        // AUTH
        if (tempauthSessions.size() > 0) {
            for (session s : tempauthSessions) {
                if (s.status == 'Working - Pending Response') {
                    authSessions.add(s);
                }
            }
            for (session s : tempauthSessions) {
                if (s.status == 'Working - Connected') {
                    authSessions.add(s);
                }
            }
            for (session s : tempauthSessions) {
                if (s.status != 'Working - Pending Response' && s.status != 'Working - Connected') {
                    authSessions.add(s);
                }
            }
        }
        // ODT
        if (tempodtSessions.size() > 0) {
            for (session s : tempodtSessions) {
                if (s.status == 'Working - Pending Response') {
                    odtSessions.add(s);
                }
            }
            for (session s : tempodtSessions) {
                if (s.status == 'Working - Connected') {
                    odtSessions.add(s);
                }
            }
            for (session s : tempodtSessions) {
                if (s.status != 'Working - Pending Response' && s.status != 'Working - Connected') {
                    odtSessions.add(s);
                }
            }
        }
        // ERGO
        if (tempergoSessions.size() > 0) {
            for (session s : tempergoSessions) {
                if (s.status == 'Working - Pending Response') {
                    ergoSessions.add(s);
                }
            }
            for (session s : tempergoSessions) {
                if (s.status == 'Working - Connected') {
                    ergoSessions.add(s);
                }
            }
            for (session s : tempergoSessions) {
                if (s.status != 'Working - Pending Response' && s.status != 'Working - Connected') {
                    ergoSessions.add(s);
                }
            }
        } 
    }
    
    public PageReference goBack() {
        return new PageReference('/'+sfId).setRedirect(true);
    }
    
    public class session {
        Product_Of_Interest_Action__c[] poias;
        public string suite {get;set;}
        public string status {get;set;}
        public string actions {get;set;}
        public string products {get;set;}
        public string sfId {get;set;}
        public string name {get;set;}
        public string title {get;set;}
        public string email {get;set;}
        public string phone {get;set;}
        public string resolvedByName {get;set;}
        
        public session(Product_Of_Interest_Action__c[] xpoias, string xobjType) {
            poias = xpoias;
            actions = '';
            products = '';
            for (Product_Of_Interest_Action__c poia : poias) {
                if (poia.Needs_Resolution__c) {
                    if (!actions.contains(poia.Action__c)) {
                        actions += poia.Action__c+', ';
                    }
                    if (!products.contains(poia.Product__c)) {
                        products += poia.Product__c+', ';
                    }
                }
            }
            actions = actions.removeEnd(', ');
            products = products.removeEnd(', ');
            if (poias[0].Product_Suite__c == 'MSDS Management') {
                suite = 'MSDS';
            } else if (poias[0].Product_Suite__c == 'EHS Management') {
                suite = 'EHS';
            } else if (poias[0].Product_Suite__c == 'MSDS Authoring') {
                suite = 'AUTH';
            } else if (poias[0].Product_Suite__c == 'On-Demand Training') {
                suite = 'ODT';
            } else if (poias[0].Product_Suite__c == 'Ergonomics') {
                suite = 'ERGO';
            }
            status = poias[0].Action_Status__c;
            if (xobjType == 'Account') {
                sfId = poias[0].Contact__c;
                name = poias[0].Contact__r.Name;
                title = poias[0].Contact__r.Title;
                email = poias[0].Contact__r.Email;
                phone = poias[0].Contact__r.Phone;
            } else {
                sfId = poias[0].Lead__c;
                name = poias[0].Lead__r.Name;
                title = poias[0].Lead__r.Title;
                email = poias[0].Lead__r.Email;
                phone = poias[0].Lead__r.Phone;
            }
            resolvedByName = poias[0].Closed_By__r.Name;
        }
        
    }
    
}