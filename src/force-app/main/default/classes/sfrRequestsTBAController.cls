public class sfrRequestsTBAController {
    public request[] queue_reqs {get;set;}
    public request[] joe_reqs {get;set;}
    public request[] mark_reqs {get;set;}
    public request[] ryan_reqs {get;set;}    
    public string queueId {get;set;}
    public string joeId {get;set;}
    public string markId {get;set;}
    public string ryanId {get;set;}
    public string joeIcon {get;set;}
    public string markIcon {get;set;}
    public string ryanIcon {get;set;}
    Set<string> ownerIds = new Set<string>();
    public string selId {get;set;}
    
    public sfrRequestsTBAController() {
        queueId = [SELECT Queueid FROM QueueSObject WHERE Queue.Name = 'SF Request Queue' LIMIT 1].Queueid;
        joeId = [SELECT id FROM User WHERE Name = 'Joseph Berkowitz' LIMIT 1].id;
        markId = [SELECT id FROM User WHERE Name = 'Mark McCauley' LIMIT 1].id;
        ryanId = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        findIcons();
        find();
    }
    
    public void find() {
        initLists();
        Salesforce_Request__c[] requests = [SELECT id, Name, Ownerid, Request_Type__c, Summary__c, Status__c, Requested_Date__c, Requested_By__c, Priority__c, Sort_Order__c, Requested_By__r.Name, Actual_Minutes__c
                                            FROM Salesforce_Request__c 
                                            WHERE Ownerid IN :ownerIds 
                                            AND Status__c != 'Completed' 
                                            AND Status__c != 'Unable to Complete'
                                            AND Request_Type__c != 'Merge'
                                            AND CreatedDate = THIS_YEAR 
                                            ORDER BY Sort_Order__c];
        for (Salesforce_Request__c r : requests) {
            if (r.Ownerid == queueId) {
                request req = new request(r);
                queue_reqs.add(req);
            }
            if (r.Ownerid == joeId) {
                request req = new request(r);
                joe_reqs.add(req);
            }
            if (r.Ownerid == markId) {
                request req = new request(r);
                mark_reqs.add(req);
            }
            if (r.Ownerid == ryanId) {
                request req = new request(r);
                ryan_reqs.add(req);
            }
        }
    }
    
    public class request {
        public Salesforce_Request__c sfr {get;set;}
        public string truncSum {get;set;}
        public string color {get;set;}
        
        public request(Salesforce_Request__c req) {
            sfr = req;
            color = '#FFFFFF';
            if (sfr.Summary__c != null) {
                truncSum = sfr.Summary__c.abbreviate(30);
            } else {
                truncSum = '';
            }
            if (sfr.Request_Type__c == 'Enhancement') {
               color = '#34BECD';
            } 
            if (sfr.Request_Type__c == 'Project') {
               color = '#7F8DE1';
            } 
            if (sfr.Request_Type__c == 'Admin') {
               color = '#EF7EAD';
            } 
            if (sfr.Request_Type__c == 'Bug') {
               color = '#F88962';
            } 
        }
    }
    
    public void initLists() {
        queue_reqs = new List<request>();
        joe_reqs = new List<request>();
        mark_reqs = new List<request>();
        ryan_reqs = new List<request>();
        ownerIds.add(queueId);
        ownerIds.add(joeId);
        ownerIds.add(markId);
        ownerIds.add(ryanId);
    }
    
    public PageReference view() {
        PageReference pr = new PageReference('/'+selId);
        return pr.setRedirect(true);
    }
    
    public void findIcons() {
        User[] users = [SELECT id, LastName, SmallPhotoURL FROM User WHERE Name = 'Ryan Werner' OR Name = 'Joseph Berkowitz' OR Name = 'Mark McCauley'];
        for (User u : users) {
            if (u.LastName == 'Berkowitz') {
                joeIcon = u.SmallPhotoURL;
            }
            if (u.LastName == 'McCauley') {
                markIcon = u.SmallPhotoURL;
            }
            if (u.LastName == 'Werner') {
                ryanIcon = u.SmallPhotoURL;
            }
        }
    }
}