public class proposalNeedsRequirements {
    public id quoteId {get;set;}
    public Quote__c thisQuote {get;set;}
    public List<Quote_Proposal_Need__c> qProposalNeeds {get;set;}
    public Map<id, Quote_Proposal_Need__c> qPNMap {get;set;}
    public List<String> orderedNeedsList {get;set;}
    
    
    public proposalNeedsRequirements(){
        quoteId = ApexPages.currentPage().getParameters().get('id');
        thisQuote = [Select Name, Needs_JSON__c from Quote__c where id = :quoteId]; 
        orderedNeedsList = (List<String>)JSON.deserialize(thisQuote.Needs_JSON__c, List<String>.class);
        qProposalNeeds = [SELECT Id, Need__c, Description__c, Need_Heading__c FROM Quote_Proposal_Need__c WHERE Id IN :orderedNeedsList AND Available_For_Use__c = true];
        qPNMap = new Map<id, Quote_Proposal_Need__c>();
        for(Quote_Proposal_Need__c qpn: qProposalNeeds){
            qPNMap.put(qpn.Id, qpn);
        } 
    }
}