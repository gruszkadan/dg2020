public class VPMProjectDetail {
    
    private ApexPages.StandardController controller {get; set;}
    public VPM_Project__c proj {get;set;}
    public User u {get;set;}
    public id projId {get;set;}
    public List<VPM_Milestone__c> pMilestones {get;set;}
    public Map<ID, String> ownerPhotos {get;set;}
    public List<User> milestoneUsers {get;set;}
    public User projOwner {get;set;}
    public boolean inlineEdit {get;set;}
    public List<Contact> accountContacts {get;set;}
    public string primaryContact {get;set; }
    public VPM_Note__c[] notes {get;set;}
    public String noteFilter {get;set;}
    public String noteSortOrder {get;set;}
    public VPM_Note__c newNote {get;set;}
    public VPM_Note__c editNote {get;set;}
    public boolean noteError {get;set;}
    public VPM_Milestone__c selectedMilestone{get;set;}
    public VPM_Task__c selectedTask{get;set;}
    public string selRecordType {get;set;}
    public string selRecordId {get;set;}
    public boolean modalData {get;set;}
    boolean hasError;
    
    public VPMProjectDetail(ApexPages.StandardController controller) { 
        this.controller = controller;
        projId = ApexPages.currentPage().getParameters().get('id'); 
        u = [Select id, FullPhotoURL, Name, LastName, Role__c, SF_Request_Owner_Change__c, SF_Request_Status_Change__c, Profile.Name from User where id =: UserInfo.getUserId() LIMIT 1];
        queryProject();
        pMilestones = [SELECT id, OwnerId, Owner.Name, Name, VPM_Project__c, Status__c, Type__c, Milestone_Start_Date__c, Contact__c, Contact__r.Name, Percent_Complete__c, 
                       (SELECT Name, OwnerId, Owner.Name, VPM_Milestone__c, Status__c, Start_Date__c, Subject__c, Contact__c, Contact__r.Name FROM VPM_Tasks__r)
                       FROM VPM_Milestone__c WHERE VPM_Project__c = :projId ORDER BY Name ASC];
        //query contacts related to Project's Account
        accountContacts = [SELECT Name, ID FROM Contact WHERE Account.ID = :proj.Account__c];
        //005 = User IDs; Query the Picture for Owner Avatar
        if(string.valueof(proj.OwnerId).startsWith('005')){
            projOwner = [SELECT FullPhotoUrl FROM User WHERE Id = :proj.OwnerID];
        }
        
        //Dans Additions
        modalData = false;
        noteSortOrder = 'DESC';
        queryNotes();
        newNote = new VPM_Note__c();
        editNote = new VPM_Note__c();
        userPictures();
    }
    
    //Query Project
    public void queryProject() {
        string SObjectAPIName = 'VPM_Project__c';
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string query = 'SELECT '+commaSepratedFields+' , Contact__r.Name FROM '+SObjectAPIName+' WHERE id = \''+projId+'\' ';
        proj = Database.query(query);
    }
    
    public void inputField(){
        inlineEdit = true;
    }
    
    //create list of contacts related to Project's account
    public String[] getContacts(){
        String[] contacts = new list<String>();   
        //loop through Account's contacts and and to list
        for(Contact c : accountContacts){
            contacts.add(c.name);
        }
        return contacts;
    }
    
    public PageReference saveProject(){
        
        //Loop through the Account's contacts; If a contact's name is equal to the contact selected in the select list
        //set the project's primary contact name field to that contact's ID
        for(Contact c: accountContacts){
            if(c.Name == primaryContact){
                proj.Contact__c = c.id;
            }
        }
        update proj;
        //redirect page
        PageReference pr = new PageReference('/apex/vpm_project_detail');
        pr.setRedirect(true);
        pr.getParameters().put('id',proj.Id);
        return pr;
    }
    
    //Dans Additions
    public void queryNotes(){
        string query = 'SELECT id, Comments__c, Case_Note__c, CreatedBy.FullPhotoURL, Contact__c, Contact__r.Name, Type__c, CreatedDate, CreatedById, CreatedBy.Name, VPM_Project__c, VPM_Project__r.Name, VPM_Milestone__c, VPM_Milestone__r.Name, VPM_Task__c, VPM_Task__r.Name '
            +'FROM VPM_Note__c ' 
            +'WHERE VPM_Project__c =\''+projId+'\'';
        if(selRecordType != null && selRecordType != ''){
            query+= 'AND '+selRecordType+' =\''+selRecordId+'\'';
        }
        if(noteFilter != '' && noteFilter == 'My Notes'){
            query+= 'AND CreatedById  =\''+u.id+'\'';
        }
        query+= 'ORDER BY CreatedDate '+noteSortOrder+' ';
        notes = database.query(query);
    }
    
    //Query the milestone or task that was selected
    public void querySelectedRecord(){
        selectedMilestone = null;
        selectedTask = null;
        selRecordType = ApexPages.currentPage().getParameters().get('selRecordType'); 
        selRecordId = ApexPages.currentPage().getParameters().get('selRecordId'); 
        string SObjectAPIName = selRecordType;
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string query = 'SELECT '+commaSepratedFields+' FROM '+SObjectAPIName+' WHERE id = \''+selRecordId+'\' ';
        if(selRecordType == 'VPM_Milestone__c'){
            selectedMilestone = Database.query(query);
        }else if(selRecordType == 'VPM_Task__c'){
            selectedTask = Database.query(query);
        }
        queryNotes();
    }
    
    //Find the note to be edited
    public void selectNoteToEdit(){
        String editNoteId = ApexPages.currentPage().getParameters().get('editNoteId'); 
        queryNotes();
        for(VPM_Note__c n:notes){
            if(n.id == editNoteId){
                editNote = n;
            }
        }
    }
    
    //Save the edited note
    public void saveEditedNote(){
        if(editNote != null){
            update editNote;
            queryNotes();
        }
    }
    
    public void addNote(){
        noteError = false;
        if(newNote.Type__c != null && newNote.Comments__c != ''){
            newNote.VPM_Project__c = projId;
            newNote.Contact__c = proj.Contact__c;
            if(selectedTask != null){
                newNote.VPM_Milestone__c = selectedTask.VPM_Milestone__c;
                newNote.VPM_Task__c = selectedTask.Id;
                newNote.Contact__c = selectedTask.Contact__c;
            }else if(selectedMilestone != null){
                newNote.VPM_Milestone__c = selectedMilestone.id;
                newNote.Contact__c = selectedMilestone.Contact__c;
            }
            insert newNote;
            newNote = new VPM_Note__c();
        }else{
            noteError = true;
        }
        queryNotes();
    }
    
    public void resetRightPanelView(){
        selRecordType = null;
        selRecordId = null;
        selectedMilestone = null;
        selectedTask = null;
        queryNotes();
    }
    
    public void userPictures(){
        
        //Create Sets of OwnerIds for milestones & tasks
        Set<ID> Owners = new Set<ID>();
        
        for(VPM_Milestone__c m : pMilestones){
            Owners.add(m.OwnerId);
            for(VPM_Task__c t : m.VPM_Tasks__r){
                Owners.add(t.OwnerId);
            } 
        }
        
        //Populate map of User IDs and FullPhotoUrls matching the corresponding OwnerIds
        ownerPhotos = new Map<Id, String>(); 
        
        for(User u : [SELECT Id, FullPhotoURL FROM User WHERE Id IN :Owners]){
            ownerPhotos.put(u.ID, u.FullPhotoURL);
        }
        
    }
    
    public void toggleModal(){
        if(modalData){
            modalData = false;
        }else{
            modalData = true;
        }
    }
    
      public PageReference cancelButton() {
        PageReference VPM_project_detail = Page.VPM_project_detail;
        VPM_project_detail.setRedirect(true);
        VPM_project_detail.getParameters().put('id',proj.Id); 
        return VPM_project_detail;        
    } 
    
}