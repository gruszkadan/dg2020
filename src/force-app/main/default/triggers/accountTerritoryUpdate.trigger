trigger accountTerritoryUpdate on Account (before insert, before update) {
    
    Set<Account> zipAccts = new Set<Account>();
    Set<Account> entAccts = new Set<Account>();
    id autosim = null;
    
    //list of SLED NAICS codes
    string sledNAICScodes = '61 - Educational Services;92 - Public Administration (Fed/State/Local Government)';
    
    if (System.UserInfo.getName() != 'Territory Reassignment') {
        for (integer i = 0; i < Trigger.new.size(); i ++) {
            if (!Trigger.isInsert) {
                //if the user changes the customer status from "out of business" to anything else
                if (trigger.old[i].Customer_Status__c == 'Out of Business' && trigger.new[i].Customer_Status__c != 'Out of Business') {
                    if (autosim == null) {
                        autosim = [SELECT id FROM User WHERE Alias = 'autosim' LIMIT 1].id;
                        
                    } 
                    if (trigger.new[i].OwnerId == autosim
                        || trigger.new[i].Online_Training_Account_Owner__c == autosim
                        || trigger.new[i].Authoring_Account_Owner__c == autosim
                        || trigger.new[i].EHS_Owner__c == autosim
                        || trigger.new[i].Ergonomics_Account_Owner__c == autosim) {
                            zipAccts.add(trigger.new[i]);
                        }
                } 
                
                // when converting leads to a new account
                if (trigger.old[i].Authoring_Account_Owner__c == null 
                    || trigger.old[i].EHS_Owner__c == null
                    || trigger.old[i].Online_Training_Account_Owner__c == null
                    || trigger.old[i].Ergonomics_Account_Owner__c == null) {
                        zipAccts.add(Trigger.new[i]);
                    }
                // on update if zip has changed, add to set to update address
                if (Trigger.old[i].BillingPostalCode != Trigger.new[i].BillingPostalCode) {
                    // if not in US, CA, or null, don't update the address
                    if (trigger.new[i].BillingCountry == 'United States' || trigger.new[i].BillingCountry == 'Canada' || trigger.new[i].BillingCountry == null) {
                        zipAccts.add(Trigger.new[i]);
                    }
                }
                // on update if enterprise sales changed to true and there is no KMI owner, add to set to assign KMI owner
                if (Trigger.old[i].Enterprise_Sales__c == false && Trigger.new[i].Enterprise_Sales__c == true && Trigger.new[i].EHS_Owner__c == null) {
                    entAccts.add(Trigger.new[i]);
                }
            }
            
            
            if (Trigger.isInsert) {
                // on insert if there's a zip, add to set to update address
                if (Trigger.new[i].BillingPostalCode != null) {
                    if (trigger.new[i].BillingCountry == 'United States' || trigger.new[i].BillingCountry == 'Canada' || trigger.new[i].BillingCountry == null) {
                        zipAccts.add(Trigger.new[i]);
                    }
                }
                // on insert if enterprise sales is true, add to set to assign KMI owner
                if (Trigger.new[i].Enterprise_Sales__c == true) {
                    entAccts.add(Trigger.new[i]);
                }
            }
        }
        
        // runs if the zip code has changed
        if (zipAccts.size() > 0) {
            // turn off v-rules
            User u = [SELECT id, Validation_Rules_Active__c, Group__c FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
            u.Validation_Rules_Active__c = false;
            update u;
            
            Set<String> zipCodes = new Set<String>();
            for (Account account : zipAccts) {
                if (account.BillingPostalCode != null) {
                    if (account.BillingCountry == 'Canada') {
                        string newZip = '';
                        string oldZip = account.BillingPostalCode.toUpperCase();
                        integer zipLength = oldZip.length();
                        if (zipLength == 3) {
                            newZip = oldZip;
                            account.BillingPostalCode = newZip;
                            zipCodes.add(newZip);
                        } else {
                            newZip = oldZip.left(3)+' '+oldZip.right(3);
                            account.BillingPostalCode = newZip;
                            zipCodes.add(newZip);
                            zipCodes.add(newZip.left(3));
                        }
                    } else {
                        zipCodes.add(account.BillingPostalCode.left(5));
                    }
                }
            }
            
            // perform logic for all accounts
            if (!zipCodes.isEmpty()) {
                Map<String, County_Zip_Code__c> cacheMap = new Map<String, County_Zip_Code__c>();
                
                // QUERY related cities and states by zip code
                for (County_Zip_Code__c zip : [select Id, Territory__c, Territory__r.Territory_Owner__c, Territory__r.SLED_Owner__c, Territory__r.Online_Training_Owner__c, 
                                               Territory__r.Authoring_Owner__c, Territory__r.Ergonomics_Owner__c,
                                               Territory__r.EHS_Owner__c, Country__c, Zip_Code__c, County__c, State__c
                                               from County_Zip_Code__c where Zip_Code__c IN :zipCodes]) {
                                                   if (zip != null) {
                                                       cacheMap.put(zip.Zip_Code__c, zip);
                                                   }
                                               }
                
                // LOOKUP city and state by zip 
                for (Account account : zipAccts) {
                    County_Zip_Code__c zip = null;
                    if (account.BillingPostalCode != null) {
                        integer codeLength = account.BillingPostalCode.length();
                        if (account.BillingCountry == 'Canada') {
                            if (codeLength == 3) {
                                zip = cacheMap.get(account.BillingPostalCode.left(3));
                            } else {
                                if (cacheMap.get(account.BillingPostalCode) != null) {
                                    zip = cacheMap.get(account.BillingPostalCode);
                                } else {
                                    zip = cacheMap.get(account.BillingPostalCode.left(3));
                                }
                            }
                        } else {
                            if (codeLength == 10) {
                                zip = cacheMap.get(account.BillingPostalCode.left(5));
                            } else {
                                zip = cacheMap.get(account.BillingPostalCode);
                            }
                        }
                    }
                    if (zip != null) {
                        Account.Billing_County__c = zip.County__c;
                        Account.Territory__c = zip.Territory__c;
                        Account.BillingState = zip.State__c;
                        Account.BillingCountry = zip.Country__c;
                        
                        if ((Account.Enterprise_Sales__c == false && Trigger.isInsert) || (Account.OwnerId == autosim && Account.Customer_Status__c != 'Out of Business') || (u.Group__c == 'Lead Development' && Account.Enterprise_Sales__c == false))  {
                            if(account.NAICS_Code__c != null && zip.Territory__r.SLED_Owner__c != null && sledNAICScodes.contains(account.NAICS_Code__c)){
                                account.OwnerId = zip.Territory__r.SLED_Owner__c;
                            }else{
                                account.OwnerId = zip.Territory__r.Territory_Owner__c;
                            }
                        } 
                        if (account.Authoring_Account_Owner__c == NULL || (Account.Authoring_Account_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                            Account.Authoring_Account_Owner__c = zip.Territory__r.Authoring_Owner__c; 
                        }
                        if (account.Online_Training_Account_Owner__c == NULL || (Account.Online_Training_Account_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                            Account.Online_Training_Account_Owner__c = zip.Territory__r.Online_Training_Owner__c; 
                        }
                        if (account.Ergonomics_Account_Owner__c == NULL || (Account.Ergonomics_Account_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                            Account.Ergonomics_Account_Owner__c = zip.Territory__r.Ergonomics_Owner__c;
                        }
                        if ((account.EHS_Owner__c == NULL && Account.Enterprise_Sales__c == false) || (Account.EHS_Owner__c == autosim && Account.Customer_Status__c != 'Out of Business')) {
                            Account.EHS_Owner__c = zip.Territory__r.EHS_Owner__c;
                        }
                    }
                }
            }
            if (!Trigger.isInsert)
            {
                Contact[] cons = [SELECT id, Territory__c, AccountId FROM Contact WHERE AccountId IN :zipAccts];
                Contact[] cons_upd = new List<Contact>();
                if (cons != null)
                {
                    for (Account acct : zipAccts)
                    {
                        for (Contact con : cons)
                        {
                            if (con.AccountId == acct.id)
                            {
                                con.Territory__c = acct.Territory__c;
                                cons_upd.add(con);
                            }
                        }
                    }
                    update cons_upd;
                }
                Opportunity[] opps = [SELECT id, Territory__c, AccountId FROM Opportunity WHERE AccountId IN :zipAccts];
                Opportunity[] opps_upd = new List<Opportunity>();
                if (opps != null)
                {
                    for (Account acct : zipAccts)
                    {
                        for (Opportunity opp : opps)
                        {
                            if (opp.AccountId == acct.id)
                            {
                                opp.Territory__c = acct.Territory__c;
                                opps_upd.add(opp);
                            }
                        }
                    }
                    update opps_upd;
                }
            }
            u.Validation_Rules_Active__c = true;
            update u;
        }
        
        // runs if the account has changed to enterprise sales
        if (entAccts.size() > 0) 
        {
            Enterprise_Account_Assignments__c[] eaas = [SELECT id, Name, Industry__c, OwnerId__c, EHS_OwnerId__c FROM Enterprise_Account_Assignments__c];
            id KMIowner = [SELECT id FROM User WHERE LastName = 'Richey' AND FirstName = 'Brian' AND isActive = true LIMIT 1].id;
            for (Account acc : trigger.new) 
            {
                if (acc.Enterprise_Sales__c == true)
                {
                    boolean ownerFound = false;
                    if (acc.Enterprise_Sales_Industry__c != null)
                    {
                        for (Enterprise_Account_Assignments__c eaa : eaas) 
                        {
                            if (acc.Enterprise_Sales_Industry__c == eaa.Industry__c && ownerFound == false)
                            {
                                acc.EHS_Owner__c = eaa.EHS_OwnerId__c;
                                ownerFound = true;
                            }
                        }
                        if (ownerFound == false)
                        {
                            acc.EHS_Owner__c = kmiOwner; 
                        }
                    }
                    else
                    {
                        acc.EHS_Owner__c = kmiOwner;
                    }
                }
            }
        }
    }
}