public class VPMTaskDetail {
    
    private ApexPages.StandardController controller {get; set;}
    public VPM_Task__c task {get;set;}
    public User u {get;set;}
    public id taskId {get;set;}
    public User Owner {get;set;}
    public boolean inlineEdit {get;set;}
    public List<Contact> accountContacts {get;set;}
    public List<VPM_Milestone__c> milestones {get;set;}
    public string primaryContact {get;set; }
    public VPM_Note__c[] notes {get;set;}
    public String noteFilter {get;set;}
    public String noteSortOrder {get;set;}
    public VPM_Note__c newNote {get;set;}
    public VPM_Note__c editNote {get;set;}
    public boolean noteError {get;set;}
    public boolean modalData {get;set;}
    
    public VPMTaskDetail(ApexPages.StandardController controller) { 
        this.controller = controller;
        taskId = ApexPages.currentPage().getParameters().get('id'); 
        u = [Select id, FullPhotoURL from User where id =: UserInfo.getUserId() LIMIT 1];
        queryTask();
        accountContacts = [SELECT Name, ID FROM Contact WHERE Account.ID = :task.Account__c];
        //query for quick nav
        milestones = [SELECT Name, Type__c, (SELECT Name, Subject__c FROM VPM_Tasks__r) FROM VPM_Milestone__c WHERE VPM_Project__c = :task.VPM_Project__c ORDER BY Name ASC];
        //005 = User IDs; Query the Picture for Owner Avatar
        if(string.valueof(task.OwnerId).startsWith('005')){
            Owner = [SELECT FullPhotoUrl FROM User WHERE Id = :task.OwnerID];
        }
        //Dans Additions
        modalData = false;
        noteSortOrder = 'DESC';
        queryNotes();
        newNote = new VPM_Note__c();
        editNote = new VPM_Note__c();
    }
    
    //Query Object Fields
    public void queryTask() {
        string SObjectAPIName = 'VPM_Task__c';
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(SObjectAPIName).getDescribe().fields.getMap();
        string commaSepratedFields = '';
        for (string fieldName : fieldMap.keyset()) {
            if (commaSepratedFields == null || commaSepratedFields == '') {
                commaSepratedFields = fieldName;
            } else {
                commaSepratedFields = commaSepratedFields+', '+fieldName;
            }
        }
        string query = 'SELECT '+commaSepratedFields+' ,VPM_Project__r.Name, VPM_Project__r.Type__c, Contact__r.Name, Contact__r.Upgrade_Email_1_Sent_Date__c, Contact__r.Upgrade_Email_2_Sent_Date__c, Contact__r.Upgrade_Email_3_Sent_Date__c FROM '+SObjectAPIName+' WHERE id = \''+taskId+'\' ';
        task = Database.query(query);
    }
    
    public void inputField(){
        inlineEdit = true;
    }
    
    public String[] getContacts(){
        String[] contacts = new list<String>();   
        //loop through Account's contacts and and to list
        for(Contact c : accountContacts){
            contacts.add(c.name);
        }
        return contacts;
    }
    
    public PageReference saveTask(){
        //Loop through the Account's contacts; If a contact's name is equal to the contact selected in the select list
        //set the project's primary contact name field to that contact's ID
        for(Contact c: accountContacts){
            if(c.Name == primaryContact){
                task.Contact__c = c.id;
            }
        }
        update task;
        //redirect page
        PageReference pr = new PageReference('/apex/vpm_task_detail');
        pr.setRedirect(true);
        pr.getParameters().put('id',task.Id);
        return pr;
    }
    
    //Dans Additions
    public void queryNotes(){
        string query = 'SELECT id, Comments__c, Case_Note__c, CreatedBy.FullPhotoURL, Contact__c, Contact__r.Name, Type__c, CreatedDate, CreatedById, CreatedBy.Name, VPM_Project__c, VPM_Project__r.Name, VPM_Milestone__c, VPM_Milestone__r.Name, VPM_Task__c, VPM_Task__r.Name '
            +'FROM VPM_Note__c ' 
            +'WHERE VPM_Task__c =\''+taskId+'\'';
        if(noteFilter != '' && noteFilter == 'My Notes'){
            query+= 'AND CreatedById  =\''+u.id+'\'';
        }
        query+= 'ORDER BY CreatedDate '+noteSortOrder+' ';
        notes = database.query(query);
    }
    
    //Find the note to be edited
    public void selectNoteToEdit(){
        String editNoteId = ApexPages.currentPage().getParameters().get('editNoteId'); 
        queryNotes();
        for(VPM_Note__c n:notes){
            if(n.id == editNoteId){
                editNote = n;
            }
        }
    }
    
    //Save the edited note
    public void saveEditedNote(){
        if(editNote != null){
            update editNote;
            queryNotes();
        }
    }
    
    public void addNote(){
        noteError = false;
        if(newNote.Type__c != null && newNote.Comments__c != ''){
            newNote.VPM_Project__c = task.VPM_Project__c;
            newNote.VPM_Milestone__c = task.VPM_Milestone__c;
            newNote.Contact__c = task.Contact__c;
            newNote.VPM_Task__c = task.Id;
            insert newNote;
            newNote = new VPM_Note__c();
        }else{
            noteError = true;
        }
        queryNotes();
    }
    
    public void toggleModal(){
        if(modalData){
            modalData = false;
        }else{
            modalData = true;
        }
    }
    
       
    public pageReference rescheduleEmail(){
        VPM_Task__c cloneTask = new VPM_Task__c();
        VPM_Project__c vpmProject = [Select Scheduled_Upgrade_Date__c, Actual_Upgrade_Date__c from VPM_Project__c where Id = :task.VPM_Project__c LIMIT 1];
        Contact Con = [Select Id, Upgrade_Email_1_Sent_Date__c, Upgrade_Email_2_Sent_Date__c, Upgrade_Email_3_Sent_Date__c from Contact Where Id = :task.Contact__c LIMIT 1];
        cloneTask = task.clone(false, true, false, false);
        cloneTask.Status__c = 'Not Started';
        cloneTask.End_Date__c = null;
        cloneTask = VPMTaskUtility.upgradeEmailVpmTask(cloneTask, vpmProject);
        insert cloneTask;
        if(cloneTask.Email_Sequence_Number__c == 1){
            Con.Upgrade_Email_1_Sent_Date__c = null;
        }
        else if(cloneTask.Email_Sequence_Number__c == 2){
            Con.Upgrade_Email_2_Sent_Date__c = null;
        }
        else if(cloneTask.Email_Sequence_Number__c == 3){
            Con.Upgrade_Email_3_Sent_Date__c = null;
        }
        update Con;
        PageReference pr = new PageReference('/apex/vpm_task_detail');
        pr.setRedirect(true);
        pr.getParameters().put('id',cloneTask.Id);
        return pr;
        
    }
    
    
}