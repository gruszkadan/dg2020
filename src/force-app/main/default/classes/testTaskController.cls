@isTest
private class testTaskController {
    
    static testmethod void taskController_test1() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        //Insert our first task
        Task t = new Task(
            subject='Initial Call',
            whatId = a.id,
            whoID = newContact.id,
            ActivityDate = date.newinstance(1960, 2, 17)
        );
        insert t;
        
        ApexPages.currentPage().getParameters().put('Id', t.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Task());
        taskController myController = new taskController(testController);
        myController.AccountID = a.id;
        myController.rURL = '/a.Id';
        myController.what = a.Id;
        myController.checkAccount();
        myController.save();
    }
    
    static testmethod void taskController_test2() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        //Insert our first task
        Task t = new Task(
            subject='Initial Call',
            whatId = a.id,
            whoID = newContact.id,
            ActivityDate = date.newinstance(1960, 2, 17)
        );
        insert t;
        
        ApexPages.currentPage().getParameters().put('Id', t.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(t);
        taskController myController = new taskController(testController);
        myController.AccountID = a.id;
        myController.who = newContact.id;
        myController.rURL = '/a.Id';
        myController.what = a.Id;
        myController.checkAccount();
        myController.updateTime();
        myController.saveNewEvent();
        myController.getContactSelectList();
        myController.setName();
    }
    
    static testmethod void taskController_test3() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        //Insert our first task
        Task t = new Task(
            subject='Initial Call',
            whatId = a.id,
            whoID = newContact.id,
            ActivityDate = date.newinstance(1960, 2, 17)
        );
        insert t;
        
        ApexPages.currentPage().getParameters().put('Id', t.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Task());
        taskController myController = new taskController(testController);
        myController.AccountID = a.id;
        myController.rURL = '/a.Id';
        myController.who = newContact.id;
        myController.what = a.Id;
        myController.checkAccount();
        myController.saveNew();
        myController.updateTime();
        myController.getContactSelectList();
        myController.checkAccount();
        myController.setName();
    }
    
    static testmethod void taskCreateController_test1() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        
        ApexPages.currentPage().getParameters().put('tsk12','Completed');
                ApexPages.currentPage().getParameters().put('whoid',newContact.id);

        ApexPages.StandardController testController = new ApexPages.StandardController(new Task());
        taskCreateController myController = new taskCreateController(testController);
        myController.rURL = '/a.Id';
        myController.cType ='Follow-Up';
        myController.what = a.Id;
        myController.who = newContact.Id;
        myController.AccountId = a.id;
        myController.theContacts = new list<Contact>();
        myController.checkAccount();
        myController.save();
        myController.updateTime();
        myController.getContactSelectList();
    }
    
    static testmethod void taskCreateController_test2() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        
        ApexPages.currentPage().getParameters().put('tsk12','Completed');
        ApexPages.currentPage().getParameters().put('whoid',newContact.id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Task());
        taskCreateController myController = new taskCreateController(testController);
        myController.rURL = '/a.Id';
        myController.cType ='Follow-Up';
        myController.what = a.Id;
        myController.who = newContact.Id;
        myController.theContacts = new list<Contact>();
        myController.theContacts.add(newContact);
        myController.AccountId = a.id;
        myController.checkAccount();
        myController.saveNew();
    }
    
    static testmethod void taskCreateController_test3() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        
         
        
        ApexPages.currentPage().getParameters().put('tsk12','Completed');
        ApexPages.currentPage().getParameters().put('whatid',a.id);
                ApexPages.currentPage().getParameters().put('whoid',newContact.id);

        ApexPages.StandardController testController = new ApexPages.StandardController(new Task());
        taskCreateController myController = new taskCreateController(testController);
        
        
        myController.rURL = '/a.Id';
        myController.cType ='Follow-Up';
        myController.what = a.Id;
        myController.who = newContact.Id;
        myController.AccountId = a.id;
        myController.checkAccount();
        myController.saveNewEvent();
        myController.activityDate = 'Test';
    }
    
  
    
    static testmethod void taskTrigger_test1() {
        
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        //Insert our first task
        Task t = new Task(
            subject='Initial Call',
            whatId = a.id,
            whoID = newContact.id,
            ActivityDate = date.newinstance(1960, 2, 17)
            
        );
        insert t;
        
        Task updateT = [Select OwnerID, Call_Result__c from Task where ID =: t.id];
        updateT.OwnerId ='00530000000os9m';
        updateT.Call_Result__c ='Connected with DM';
        updateT.Status ='Completed';
        update updateT;
    }   
    
    static testmethod void taskTrigger_test2() {
        //Setup
        Account a = new Account(name='Test');
        insert a;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = a.Id;
        insert newContact;
        
        //Insert our first task
        Task t = new Task(
            subject='Initial Call',
            whatId = a.id,
            New_Department__c = 'Sales',
            ActivityDate = date.newinstance(1960, 2, 17),
            Call_Result__c = 'Sent Email'
        );
        insert t;
        
        t.WhoId = newContact.id;
        update t;
    }
}