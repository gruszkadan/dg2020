public class proposalPrintPDF {
    
    public pageWrapper[] pageWrappers {get;set;}
    public Id quoteId {get;set;}
    public Quote__c quote {get;set;}
    public List<String> JSONIds {get;set;}
    public List<ContentVersion> contentVersions {get;set;}
    public List<Id> contentDocImages {get;set;}
    public List<Content_Document_Image__c> contentImages {get;set;}
    public Map<String, Attachment[]> contentMap {get;set;}
    public boolean displaySystemRequirements {get;set;}
    
    
    public proposalPrintPDF(){
        
        displaySystemRequirements = false;
        
        pageWrappers = new List<pageWrapper>();
        
        quoteId = apexPages.currentPage().getParameters().get('id');
        
        quote = [SELECT Id, JSON_Proposal_Data__c, Needs_JSON__c FROM Quote__c WHERE Id = :quoteId];
        
        parseJSON();
        
        contentVersions = [SELECT Content_Document_Image__c, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :JSONIds AND Content_Document_Image__c != null AND isLatest = true];
        
        contentDocImages = new List<Id>();
        
        for(ContentVersion cv: contentVersions){
            contentDocImages.add(cv.Content_Document_Image__c);
        }
        
        
        //query the attachments and ContentDocumentId for Content Document Images that were stored in JSON String
        contentImages = [SELECT Id, (Select Id FROM Attachments Order By Name ASC), (Select ContentDocumentId FROM Content_Versions__r WHERE IsLatest = true LIMIT 1) FROM Content_Document_Image__c WHERE Id IN :contentDocImages];
        
        
        //loop through contentVersion and pageWrappers; if a content document Id is found in static page wrapper, mark contentDocument for pageWrapper as true so ones without Content Document Image
        //arent displayed on VF Page
        for(ContentVersion cv : contentVersions){
            for(pageWrapper pw : pageWrappers){
                if(pw.type == 'Static' && cv.ContentDocumentId == pw.id){
                    pw.contentDocumentImageFound = true;
                }
                
            }
            
            
        }
        
        createMap();
        
        
        //query Quote Products and Proposal System Requirements Category for related product
        List<quote_product__c> quoteProds = [Select Id, Product__r.Proposal_System_Requirements_Category__c from Quote_Product__c WHERE Quote__c = :quote.id];
        
        Set<String> prodCategories = new Set<String>();
        
        for(Quote_Product__c qp: quoteProds){
            prodCategories.add(qp.Product__r.Proposal_System_Requirements_Category__c);
        }
        
        //query the metadata types that have a matching product category from the quote product's product
        List<Proposal_System_Requirements__mdt> psr = [SELECT Category__c, Product_Display__c, Sort_Order__c, Static_Product_Display__c, System_Requirements__c
                                                       FROM Proposal_System_Requirements__mdt WHERE Category__c IN :prodCategories ORDER BY Sort_Order__c ASC];
        
        
        //if there are metadata types queried that have a related product, display the System Requirements Page
        if(psr.size() > 0){
            
            displaySystemRequirements = true;
            
        }
        
    }
    
    
    public class PageWrapper{
        
        public String type {get;set;}
        public String id {get;set;}
        public boolean contentDocumentImageFound {get;set;}
        
        public PageWrapper(String xType, String xId, boolean xContentDocumentImageFound){
            
            type = xType;
            id = xId;
            contentDocumentImageFound = xContentDocumentImageFound;
            
        }
        
    }
    
    public void parseJSON(){
        
        String JSONId = '';
        String vfPage = '';
        JSONIds = new List<String>();
        
        JSONParser parser = JSON.createParser(quote.JSON_Proposal_Data__c);        
        
        //until you reach end of JSON String
        while (parser.nextToken() != null) {
            
            //if the field is ID 
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                (parser.getText() == 'Id')) {
                    // Get value of id
                    parser.nextToken();
                    
                    //if ID != null, set Text to JSONId, add to JSONIDs list and create Static wrapper
                    if(parser.getText() != null && parser.getText() != ''){
                        
                        JSONId = parser.getText();
                        JSONIds.add(JSONId);
                        pageWrappers.add(new pageWrapper('Static', JSONId, false));
                        
                        
                    } 
                    //if field is vfPage
                }else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                          (parser.getText() == 'vfPage')){
                              // get value of vfPage
                              parser.nextToken();
                              //if != null, set Text to vfPage and create Dynamic wrapper
                              if(parser.getText() != null && parser.getText() != ''){
                                  
                                  vfPage = parser.getText();
                                  pageWrappers.add(new pageWrapper('Dynamic', vfPage, false));
                                  
                              }  
                              
                          }
            
        }
        
    }
    
    
    public void createMap(){
        
        contentMap = new Map<String, Attachment[]>();
        
        for(Content_Document_Image__c c : contentImages){
            
            contentMap.put(c.Content_Versions__r[0].ContentDocumentId, c.Attachments);
            
        }
        
    }

    
}