//** Test Class: testOrderPendingAdjustments **
public class orderPendingAdjustmentsController {
    public Order_Item__c[] pendingItems {get;set;}
    public Sales_Performance_Request__c[] requests {get;set;}
    public map<id,Sales_Performance_Request__c> requestMap {get;set;}
    public map<id,decimal> itemBookingsChangeMap {get;set;}
    public orderWrapper[] orderWrappers {get;set;}
    public orderWrapper selectedOrder {get;set;}
    public String errorString {get;set;}
    
    public orderPendingAdjustmentsController(){
        id queueId = [SELECT Id FROM Group WHERE Name = 'Order Item Management' and Type = 'Queue'].id;
        pendingitems = [SELECT id,Order_ID__c,Account__r.AdminId__c,Admin_Tool_Sales_Rep__c,Account__r.Name,Order_Item_ID__c,Invoice_Adjustment_Reason__c,
                        Invoice_Amount_Change_Date__c,Invoice_Amount__c,Previous_Invoice_Amount__c,Admin_Tool_Product_Name__c,Sales_Rep__c,Sales_Rep__r.Name,
                        Order_Date__c,Sales_Rep__r.Email,Sales_Rep__r.Sales_Director__r.Email,Sales_Rep__r.Manager.Email FROM Order_Item__c WHERE OwnerId =:queueId ORDER BY Invoice_Amount_Change_Date__c ASC];
        requests = [SELECT id,Type__c,Status__c,JSON__c FROM Sales_Performance_Request__c WHERE Type__c = 'Automated Adjustment' and Status__c = 'New'];
        requestMap = new map<id,Sales_Performance_Request__c>();
        itemBookingsChangeMap = new map<id,decimal>();
        if(requests.size() > 0){
            for(Sales_Performance_Request__c spr:requests){
                string[] reqJSON = (List<String>)System.JSON.deserialize(spr.JSON__c,List<String>.Class);
                requestMap.put(reqJSON[0],spr);
            }
        }
        set<String> orderIds = new set<String>();
        for(Order_Item__c oi:pendingItems){
            orderIds.add(oi.Order_ID__c);
            itemBookingsChangeMap.put(oi.id,oi.Previous_Invoice_Amount__c);
        }
        orderWrappers = new list<orderWrapper>();
        for(String o:orderIds){
            String orderId = o;
            Order_Item__c[] items = new list<Order_Item__c>();
            Sales_Performance_Request__c[] orderRequests = new list<Sales_Performance_Request__c>();
            for(Order_Item__c oi:pendingItems){
                if(oi.Order_ID__c == o){
                    items.add(oi);
                    orderRequests.add(requestMap.get(oi.id));
                }
            }
            orderWrappers.add(new orderWrapper(orderId,items,orderRequests));
        }
    }
    
    public void selectOrder(){
       errorString = null;
        String selOrder = ApexPages.currentPage().getParameters().get('selOrder');
        for(orderWrapper o:orderWrappers){
            if(o.orderId == selOrder){
                selectedOrder = o;
            }
        }
        boolean includeAuthOnly = false;
        for(Order_Item__c oi: selectedOrder.items){
            if(oi.Admin_Tool_Product_Name__c == 'MSDS Authoring' ||
               oi.Admin_Tool_Product_Name__c == 'MSDS Translation' ||
               oi.Admin_Tool_Product_Name__c == 'Regulatory Consulting'){
                   includeAuthOnly = true;
               }
        }
        Email_Settings__c[] emails = [Select Email__c,Authoring_Only__c from Email_Settings__c where Type__c = 'Order Adjustment' AND (Authoring_Only__c = FALSE OR Authoring_Only__c = :includeAuthOnly)];
        String defaultEmails = '';
        set<string> defaultEmailSet = new set<string>();
        for(Email_Settings__c s:emails){
            defaultEmailSet.add(s.Email__c);
        }
        for(Order_Item__c oi:selectedOrder.items){
            if(oi.Sales_Rep__r.Name != 'EHS Sales'){
                defaultEmailSet.add(oi.Sales_Rep__r.Email);
                defaultEmailSet.add(oi.Sales_Rep__r.Manager.Email);
                defaultEmailSet.add(oi.Sales_Rep__r.Sales_Director__r.Email);
            }else{
                defaultEmailSet.add([SELECT Email FROM User WHERE UserRole.Name = 'EHS Business Development Director'].Email);
            }
        }
        for(String s:defaultEmailSet){
            if(defaultEmails == ''){
                defaultEmails += s;
            }else{
                defaultEmails += ','+s;
            } 
        }
        selectedOrder.emailAddresses = defaultEmails;
    }
    
    public PageReference reloadPage(){
        if(errorString == null){
            return new PageReference('/apex/order_pending_adjustments').setRedirect(true);
        }else{
            return null;
        }
    }
    
    public void dismiss(){
        try{
            errorString = null;
            String selOrder = ApexPages.currentPage().getParameters().get('selOrder');
            for(orderWrapper o:orderWrappers){
                if(o.orderId == selOrder){
                    selectedOrder = o;
                }
            }
            Sales_Performance_Request__c[] sprsToUpdate = new list<Sales_Performance_Request__c>();
            Order_Item__c[] oisToUpdate = new list<Order_Item__c>();
            for(Order_Item__c item:selectedOrder.items){
                item.OwnerId = item.Sales_Rep__c;
                oisToUpdate.add(item);
                Sales_Performance_Request__c spr = requestMap.get(item.id);
                if(requestMap.get(item.id) != null){
                    spr.Requires_Approval__c = true;
                    spr.Status__c = 'Pending Approval';
                    spr.Adjustment_Type__c = selectedOrder.adjustmentType;
                    sprsToUpdate.add(spr);
                }
            }
            update oisToUpdate;
            update sprsToUpdate;
        }catch(exception e){
            salesforceLog.createLog('Order Management', true, 'orderPendingAdjustmentsController', 'dismiss', string.valueOf(e));
        }
    }
    
    public void sendAlert(){
        //Validation
        errorString = null;
        for(Order_Item__c oi:selectedOrder.items){
            if(oi.Invoice_Adjustment_Reason__c == null || oi.Invoice_Adjustment_Reason__c == ''){
                errorString = 'Missing Required Field(s)';
            }
        }
        if(selectedOrder.adjustmentType == null || selectedOrder.adjustmentType == ''){
            errorString = 'Missing Required Field(s)';
        }
        //If Validation Passes - Send Email Alert
        if(errorString == null){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            set<String> sToAddresses = new set<String>();
            sToAddresses.addAll(selectedOrder.emailAddresses.split(','));
            list<String> sToAddressesList = new list<String>();
            for(String s:sToAddresses){
                if(s != null){
                    sToAddressesList.add(s);
                }
            }
            mail.setToAddresses(sToAddressesList);
            mail.setSubject('Adjustment Alert - '+selectedOrder.customerName);
            string htmlBody ='<p style="font-size: 75%;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;"><b>Order '+selectedOrder.orderId+' has been adjusted</b><br/><br/>'+
                'Company: '+selectedOrder.customerName+'<br/>'+
                'UID: '+selectedOrder.adminId+'<br/>'+
                'Order Date: '+selectedOrder.orderDate+'<br/><br/>';
            if(selectedOrder.items.size() > 0){
                htmlBody += '<u>Summary</u>:<br/>'+
                    '<table style="font-size: 85%;width: 100%;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;border-collapse: collapse;border-color: grey;border: 1px solid #dddbda;">'+
                    '<tr style="font-size: 0.75rem;line-height: 1.25;color: #706e6b;text-transform: uppercase;letter-spacing: 0.0625rem;">'+
                    '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 7px 7px;font-weight: 400;white-space: nowrap;">Product</th>'+
                    '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 7px 7px;font-weight: 400;white-space: nowrap;">Original Bookings</th>'+
                    '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 7px 7px;font-weight: 400;white-space: nowrap;">New Bookings</th>'+
                    '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 7px 7px;font-weight: 400;white-space: nowrap;">Rep</th>'+
                    '<th style="text-align: left;background-color: #fafaf9;color: #514f4d;padding: 7px 7px;font-weight: 400;white-space: nowrap;">Reason</th>'+
                    '</tr>';
                for(Order_Item__c item:selectedOrder.items){
                    htmlBody +='<tr>'+
                        '<td style="border-top: 1px solid #dddbda;padding: 7px 7px;">'+item.Admin_Tool_Product_Name__c+'</td>'+
                        '<td style="border-top: 1px solid #dddbda;padding: 7px 7px;">$'+itemBookingsChangeMap.get(item.id).format()+'</td>'+
                        '<td style="border-top: 1px solid #dddbda;padding: 7px 7px;">$'+item.Invoice_Amount__c.format()+'</td>'+
                        '<td style="border-top: 1px solid #dddbda;padding: 7px 7px;">'+item.Admin_Tool_Sales_Rep__c+'</td>'+
                        '<td style="border-top: 1px solid #dddbda;padding: 7px 7px;">'+item.Invoice_Adjustment_Reason__c+'</td>'+
                        '</tr>';
                }
                htmlBody += '</table></p>';
            }
            mail.setHtmlBody(htmlBody);
            try{
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});  
                Sales_Performance_Request__c[] sprsToUpdate = new list<Sales_Performance_Request__c>();
                Order_Item__c[] oisToUpdate = new list<Order_Item__c>();
                for(Order_Item__c item:selectedOrder.items){
                    item.OwnerId = item.Sales_Rep__c;
                    oisToUpdate.add(item);
                    Sales_Performance_Request__c spr = requestMap.get(item.id);
                    if(requestMap.get(item.id) != null){
                        spr.Adjustment_Type__c = selectedOrder.adjustmentType;
                        spr.Requires_Approval__c = true;
                        spr.Status__c = 'Pending Approval';
                        if(spr.Adjustment_Type__c == 'FX Change'){
                            spr.Requires_Approval__c = false;
                            spr.Status__c = 'Approved';
                            spr.Automated_Adjustment_Incentive__c = true;
                        }
                        sprsToUpdate.add(spr);
                    }
                }
                update oisToUpdate;
                update sprsToUpdate;
            }catch(exception e){
                salesforceLog.createLog('Order Management', true, 'orderPendingAdjustmentsController', 'sendAlert', string.valueOf(e));
            }
        }
    }
    
    public List<SelectOption> getTypes(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Sales_Performance_Request__c.Adjustment_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         options.add(new SelectOption('','---None---'));
        for(Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }       
        return options;
    }
    
    public class orderWrapper{
        public String orderId {get;set;}
        public String emailAddresses {get;set;}
        public String message {get;set;}
        public String customerName {get;set;}
        public String adminId {get;set;}
        public String orderDate {get;set;}
        public String orderAdjustmentDate {get;set;}
        public String adjustmentType {get;set;}
        public Order_Item__c[] items {get;set;}
        public Sales_Performance_Request__c[] orderRequests {get;set;}
        public orderWrapper(String xOrderId,Order_Item__c[] xItems,Sales_Performance_Request__c[] xOrderRequests){
            orderId = xOrderId;
            items = xItems;
            orderRequests = xOrderRequests;
            customerName = items[0].Account__r.Name;
            adminId = items[0].Account__r.AdminId__c;
            orderDate = items[0].Order_Date__c.format();
            orderAdjustmentDate = items[0].Invoice_Amount_Change_Date__c.format();
        }
    }
}