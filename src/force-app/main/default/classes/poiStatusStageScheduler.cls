/*
*  this class runs everynight and looks for actions with unable to connect >3 
* and 30 days since last status change date
*/
public class poiStatusStageScheduler implements Schedulable {
    public void execute(SchedulableContext ctx) {
        System.enqueueJob(new poiStageStatusQueueableStep1());
        System.enqueueJob(new poiStageStatusQueueableStep2());
    }
}