@isTest(seeAllData=true)
private class testCountPrimaryOppContactsUpdater {
    
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test1() {
        
        Account act = new Account(Name = 'Test Account', NAICS_Code__c = '23 - Construction');
        insert act;
        
        Contact con1 = new Contact(FirstName = 'Test', LastName ='Person1', AccountId = act.Id);
        insert con1;
        
        Contact con2 = new Contact(FirstName = 'Test', LastName ='Person2', AccountId = act.Id);
        insert con2;
        
        Opportunity op1 = new Opportunity(AccountId = act.id, Name = 'Test Opportunity 1', StageName = 'Closed', CloseDate = date.today());
        insert op1;
        
        Opportunity op2 = new Opportunity(AccountId = act.id, Name = 'Test Opportunity 2', StageName = 'Closed', CloseDate = date.today());
        insert op2;
        
        OpportunityContactRole op1cr1 = new OpportunityContactRole(OpportunityId = op1.Id, ContactId = con1.Id, IsPrimary = true);
        insert op1cr1;
        
        OpportunityContactRole op1cr2 = new OpportunityContactRole(OpportunityId = op1.Id, ContactId = con2.Id, IsPrimary = false);
        insert op1cr2;
        
        OpportunityContactRole op2cr1 = new OpportunityContactRole(OpportunityId = op2.Id, ContactId = con2.Id, IsPrimary = false);
        insert op2cr1;
        
        test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new countPrimaryOppContactsUpdater());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        test.stopTest();
    }
}