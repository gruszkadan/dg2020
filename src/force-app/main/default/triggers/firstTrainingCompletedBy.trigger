trigger firstTrainingCompletedBy on Case_Notes__c (after insert, after update) {
	
	Map<String, Case_Notes__c> cNoteMap = new Map<String, Case_Notes__c>();
    for (Case_Notes__c cNote : System.Trigger.new)
	{ 	
		Case c = [SELECT ID, First_Training_Completed_By__c from Case where ID = :cNote.Case__c];
		Case_Notes__c[] cn 	 = [SELECT CreatedByID from Case_Notes__c where Case__c=:c.ID and Training__c =1 Order By CreatedDate ASC Limit 1];
	 {
	 	if(cNote.Notes_Type__c == 'Training' || cNote.Notes_Type__c == 'Training - Client Waived' || cNote.Notes_Type__c == 'Webinar - Training')
			{
				if(c.ID != NULL)
				{
				c.First_Training_Completed_By__c = cn[0].CreatedById;
				}
			} 
			update c;			
		}
	}

}