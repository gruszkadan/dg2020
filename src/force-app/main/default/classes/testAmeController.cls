@isTest()
private class testAmeController {
    
    static testmethod void test1() {
        
        Account_Management_Event__c newAme = new Account_Management_Event__c();
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1]; 
        
        //Insert newAme record
        newAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        newAme.OwnerId = Dan.Id;
        newAme.Status__c = 'Active';
        insert newAme;
        
        //Insert Account
        Account Acc = new Account(); 
        Acc.Name = 'Macys'; 
        Acc.Customer_Status__c = 'Prospect'; 
        Acc.OwnerID= Dan.Id;
        Acc.NAICS_Code__c = '12345';
        insert Acc; 
        
        
        //Insert Contact
        Contact Con = new Contact();
        Con.LastName = 'Lahners';
        Con.AccountId = Acc.Id;
        Con.OwnerID= Dan.Id;
        insert Con;
        
        
        //Insert Opportunity
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Macys1';
        Opp.StageName= 'Closed/Won';
        Opp.OwnerID= Dan.Id;
        opp.AccountId = Acc.id;
        Opp.CloseDate = date.today();
        Opp.Account_Management_Event__c = newAme.Id;   
        Opp.Pricing_Disposition__c = 'N/A';
        insert Opp; 
        
        ApexPages.currentPage().getParameters().put('Id', newAme.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(newAme);
        ameController myController = new ameController(testController);
        myController.sentToOrders();
        myController.submitforapproval();
        newAme = [SELECT Id, Status__c FROM Account_Management_Event__c WHERE Id = :newAme.Id];
        System.assertEquals('Pending Approval', newAme.Status__c);
        
    }
    
    static testmethod void test2() {
        
        Account_Management_Event__c newAme = new Account_Management_Event__c();
        user Mark = [SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1]; 
        
        //Insert Account
        Account Acc = new Account();
        Acc.Name = 'Ford';
        Acc.Customer_Status__c = 'Prospect'; 
        Acc.OwnerID= Mark.Id;
        Acc.NAICS_Code__c = '123456';
        insert Acc; 
        
        //Insert Contact
        Contact Con = new Contact();
        Con.LastName = 'Lahners';
        Con.AccountId = Acc.Id;
        Con.OwnerID= Mark.Id;
        insert Con;
        
        //Insert newAme record
        newAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();
        newAme.OwnerId = Mark.Id;
        newAme.Status__c = 'Active';
        newAme.Notes__c = '123';
        newAme.Account__c = Acc.Id;
        newAme.Contact__c = Con.Id;
        insert newAme;
        
        ApexPages.currentPage().getParameters().put('Id', newAme.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(newAme);
        ameController myController = new ameController(testController);
        myController.approvalcomments = newAme.Id;
        myController.getContacts();
        
        //checking the save method by updating a field
        newAme.Notes__c = '1234';
        myController.save();
        System.assert(newAme.Notes__c == '1234');
        myController.convertAME();
        myController.xDelete();
        
    }
    
    static testmethod void test3() {
        Account_Management_Event__c newAme1 = new Account_Management_Event__c();
        Account_Management_Event__c newAme = new Account_Management_Event__c();
        user Mark = [SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1]; 
        
        //Insert newAme1 record
        newAme1.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();   
        newAme1.OwnerId = Mark.Id;
        newAme1.Status__c = 'Active';
        //newAme1.RecordType.Name = 'Renewal';
        insert newAme1;
        System.debug(newAme1.RecordTypeId);
        System.debug(newAme1.Id);
        
        //Insert newAme record
        newAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();
        newAme.OwnerId = Mark.Id;
        newAme.Status__c = 'Active';
        newAme.Related_AME__c = newAme1.Id;
        insert newAme;
        System.debug(newAme.Id);
        System.debug( newAme.Related_AME__c);
        
        //Insert Account
        Account Acc = new Account();
        Acc.Name = 'Macys';
        Acc.Customer_Status__c = 'Prospect'; 
        Acc.OwnerID= Mark.Id;
        Acc.NAICS_Code__c = '12345';
        insert Acc; 
        
        //Insert Contact
        Contact Con = new Contact();
        Con.LastName = 'Lahners';
        Con.AccountId = Acc.Id;
        Con.OwnerID= Mark.Id;
        insert Con;
        
        //Insert Opportunity1
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Macys3';
        Opp.StageName= 'System Demo';
        Opp.OwnerID= Mark.Id;
        Opp.AccountId = Acc.id;
        Opp.ContactId = Con.id;
        Opp.CloseDate = date.today();
        Opp.Account_Management_Event__c = newAme1.Id;
        Opp.Pricing_Disposition__c = 'N/A';
        insert Opp;
        
        ApexPages.currentPage().getParameters().put('Id', newAme.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(newAme);
        ameController myController = new ameController(testController);
        //Variables used in controller
        myController.rtRenew = newAme1.Id;
        myController.rtCancel = newAme.Id;
        myController.relatedAme = newAme1;
        myController.RecordType = newAme.RecordTypeId;
        myController.cancellationQueue = newAme;
        myController.NewAme = newAme;
        
        //calling submit for approval method
        myController.submitforapproval();
        Opp = [SELECT Id, StageName, Account_Management_Event__c from Opportunity Where IsClosed = False AND Account_Management_Event__c =: newAme1.Id ];
        System.debug(Opp.StageName);
        System.debug(Opp.Account_Management_Event__c);
        System.assertEquals('Pending Cancellation', Opp.StageName);
    }
}