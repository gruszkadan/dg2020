//TEST CLASS - testOnsiteLocations
public class onsiteInventoryQuestionnaire {
    id qpId = ApexPages.currentPage().getParameters().get('id');
    public Quote__c theQuote {get;set;}
    public Quote_Product__c d {get;set;}
    public onsiteLocations onsiteWrapper {get;set;}    
    public onsiteInventoryQuestionnaire(){
        theQuote = new Quote__c();
        d= [Select id,Name__c, Quote__r.Account__r.Name,Onsite_Locations__c,Onsite_Timeline__c,Onsite_Location_Data__c,
            Onsite_Protective_Equipment__c,Onsite_Specific_Safety_Training__c,Onsite_Shots_Vaccinations__c,
            Onsite_Vendormate_Registration__c,Quote__r.Name,Quote__r.Owner.Name,Onsite_of_Products__c,Onsite_Square_Footage__c
            FROM Quote_Product__c where id =:qpId LIMIT 1];
        system.debug('d is '+d);
        onsiteWrapper = new onsiteLocations();
        if(d.Onsite_Location_Data__c != null && d.Onsite_Location_Data__c != ''){
            string onsiteJSON = d.Onsite_Location_Data__c;
            onsiteWrapper = (onsiteLocations)JSON.deserializeStrict(onsiteJSON, onsiteLocations.class);
        }
    }
    
}