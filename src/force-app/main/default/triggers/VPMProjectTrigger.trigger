trigger VPMProjectTrigger on VPM_Project__c (after Update) {
    
    if (trigger.isAfter) {
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            VPMProjectTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
    }
}