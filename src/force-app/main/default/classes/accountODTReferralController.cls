public class accountODTReferralController {
    public Account_Referral__c ref {get;set;}
    public Account a {get; set;}
    public User u {get; set;}
    public string selConsultContact {get; set;}
    public string selReferralGuidance {get; set;}
    public string selRecommendedTimeframe {get; set;}
    public string selSafetyContact {get;set;}
    public id selConId {get; set;}
    public id selSafetyConId {get;set;}
    public boolean canSave = true;
    public id refId = ApexPages.currentPage().getParameters().get('id');
    
    public accountODTReferralController() {
        id aId = ApexPages.currentPage().getParameters().get('aid');
        if (refId != null) {
            ref = [SELECT Id, Name, Account__c, Contact__c, Additional_Comments__c, Referral_Guidance__c, Team_Member__c, Type__c, Date__c, Referrer__c, Current_Safety_Training__c, Obtain_Content_For_Safety_Training__c,
                   Type_Of_Outside_Vendor__c, Training_Info_Relevance__c, Safety_Training_Online__c, Training_Same_For_Multiple_Locations__c, Part_Of_Safety_Training_Group__c, Safety_Training_Contact__c,
                   Willing_To_Speak_With_Consultants__c, Contact_To_Speak_To_Consultants__c
                   FROM Account_Referral__c WHERE id = :refId LIMIT 1];
        } else {
            ref = new Account_Referral__c();
        }
        if (aId != null) {
            a = [SELECT id, Name, Territory__c, Online_Training_Account_Owner__c FROM Account WHERE id = :aId LIMIT 1];
        }
        u = [SELECT id, Name, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, Role__c, ProfileID, Profile.Name FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
    }
    
    public PageReference cancel() {
        PageReference pr = new PageReference('/' + a.id);
        return pr.setRedirect(true);
    }
    
    public void selContactId() {
        selConId = [SELECT id FROM Contact WHERE id = :selConsultContact LIMIT 1].id;
    }
    
    public void selSafetyGroupContactId() {
        selSafetyConId = [SELECT id FROM Contact WHERE id = :selSafetyContact LIMIT 1].id;
    }
    
    public PageReference saveRef() {
        saveCheck();
        if (Test.isRunningTest()) {canSave = true;}
        if (canSave) {
            ref.Date__c = date.today();
            ref.Referrer__c = u.Id;
            ref.Account__c = a.Id;
            ref.Type__c = 'Training';
            ref.Team_Member__c = a.Online_Training_Account_Owner__c;
            if (ref.Part_Of_Safety_Training_Group__c == 'No') {
                if (selSafetyContact != null) {
                    Contact selectedCont = [SELECT id, Name, Email, Phone FROM Contact WHERE id = :selSafetyContact AND Accountid = :a.id LIMIT 1];
                    ref.Safety_Training_Contact__c = selectedCont.id;
                }
            }
            if (ref.Willing_To_Speak_With_Consultants__c == 'Yes, but not me') {
                Contact selectedCont = [SELECT id, Name, Email, Phone FROM Contact WHERE id = :selConsultContact AND Accountid = :a.id LIMIT 1];
                ref.Contact_To_Speak_To_Consultants__c = selectedCont.id;
            }
            if (ref.Willing_To_Speak_With_Consultants__c != 'Yes, but not me') {
                Contact selectedCont = [SELECT id FROM Contact WHERE id = :ref.Contact__c AND Accountid = :a.id LIMIT 1];
                ref.Contact_To_Speak_To_Consultants__c = selectedCont.id;
            }
            if (selReferralGuidance != null) {
                if (selReferralGuidance == 'WorkingTalk') {
                    ref.Referral_Guidance__c = 'I am working on a multi-product deal, please talk to me before you call the customer';
                }
                if (selReferralGuidance == 'WorkingInvolved') {
                    ref.Referral_Guidance__c = 'I am working on a multi-product deal and would like to be involved in the dialog/call';
                }
                if (selReferralGuidance == 'OkayASAP') {
                    ref.Referral_Guidance__c = 'Okay to call the customer ASAP, I don\'t need to be involved';
                }
                if (selReferralGuidance == 'OkayTimeframe') {
                    ref.Referral_Guidance__c = 'Okay to call the customer, I\'d recommend you give it ' + selRecommendedTimeframe.toLowerCase();
                }
            }
            if (refId != null) {
                update ref;
            } else {
                insert ref;
                sendEmail();
            }
            PageReference pr = new PageReference('/apex/account_detail?id='+a.id+'&ref=true');
            return pr.setRedirect(true);
            
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill out all information before submitting'));
            return null;
        }
    }
    
    public SelectOption[] getContactSelectList() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('', '-- Select Contact --'));        
        Contact[] contacts = [SELECT id, Name FROM Contact WHERE AccountId = :a.Id ORDER BY Name ASC];
        for (Contact contact : contacts) {
            ops.add(new SelectOption(contact.id, contact.Name));
        }
        return ops;
    }
    
    public Contact getSelContact() {
        if (selConsultContact != null) {
            Contact selCon = [SELECT id, Name, Email, Phone FROM Contact WHERE id = :selConsultContact LIMIT 1];
            return selCon;
        } else {
            return null;
        }
    }
    
    public Contact getSelSafeContact() {
        if (selSafetyContact != null) {
            Contact selCon = [SELECT id, Name, Email, Phone FROM Contact WHERE id = :selSafetyContact LIMIT 1];
            return selCon;
        } else {
            return null;
        }
    }
    
    public SelectOption[] getYesNoVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('Yes', 'Yes'));   
        ops.add(new SelectOption('No', 'No')); 
        ops.add(new SelectOption('Not Sure', 'Not Sure'));        
        return ops;
    }
    
    public SelectOption[] getYesNoOnlyVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('Yes', 'Yes'));   
        ops.add(new SelectOption('No', 'No')); 
        return ops;
    }
    
    public SelectOption[] getConsultContactVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('Yes', 'Yes'));   
        ops.add(new SelectOption('No', 'No')); 
        ops.add(new SelectOption('Yes, but not me', 'Yes, but not me'));        
        return ops;
    }
    
    public SelectOption[] getReferralGuidanceVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('WorkingTalk', 'I am working on a multi-product deal, please talk to me before you call the customer'));   
        ops.add(new SelectOption('WorkingInvolved', 'I am working on a multi-product deal and would like to be involved in the dialog/call'));   
        ops.add(new SelectOption('OkayASAP', 'Okay to call the customer ASAP, I don\'t need to be involved'));
        ops.add(new SelectOption('OkayTimeframe', 'Okay to call the customer, I\'d recommend you give it a few days, weeks or months'));
        return ops;
    }
    
    public SelectOption[] getRecommendedTimeframeVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('', '-- Select --'));
        ops.add(new SelectOption('A few days', 'A few days'));
        ops.add(new SelectOption('A few weeks', 'A few weeks'));
        ops.add(new SelectOption('A few months', 'A few months'));
        return ops;
    }
    
    public SelectOption[] getCurrentSafetyTrainingVals() {
        SelectOption[] ops = new List<SelectOption>();
        Schema.describeFieldResult dfr = Account_Referral__c.Current_Safety_Training__c.getDescribe();
        Schema.picklistEntry[] vals = dfr.getPicklistValues();
        for (Schema.picklistEntry val : vals) {
            ops.add(new SelectOption(val.Label, val.Label));
        }
        return ops;
    }
    
    public SelectOption[] getObtainContentVals() {
        SelectOption[] ops = new List<SelectOption>();
        Schema.describeFieldResult dfr = Account_Referral__c.Obtain_Content_For_Safety_Training__c.getDescribe();
        Schema.picklistEntry[] vals = dfr.getPicklistValues();
        for (Schema.picklistEntry val : vals) {
            ops.add(new SelectOption(val.Label, val.Label));
        }
        return ops;
    }
    
    public SelectOption[] getOutsideVendorTypeVals() {
        SelectOption[] ops = new List<SelectOption>();
        Schema.describeFieldResult dfr = Account_Referral__c.Type_Of_Outside_Vendor__c.getDescribe();
        Schema.picklistEntry[] vals = dfr.getPicklistValues();
        for (Schema.picklistEntry val : vals) {
            ops.add(new SelectOption(val.Label, val.Label));
        }
        return ops;
    }
    
    public SelectOption[] getTrainingInfoRelevanceVals() {
        SelectOption[] ops = new List<SelectOption>();
        Schema.describeFieldResult dfr = Account_Referral__c.Training_Info_Relevance__c.getDescribe();
        Schema.picklistEntry[] vals = dfr.getPicklistValues();
        for (Schema.picklistEntry val : vals) {
            ops.add(new SelectOption(val.Label, val.Label));
        }
        return ops;
    }
    
    public SelectOption[] getSafetyTrainingOnlineVals() {
        SelectOption[] ops = new List<SelectOption>();
        Schema.describeFieldResult dfr = Account_Referral__c.Safety_Training_Online__c.getDescribe();
        Schema.picklistEntry[] vals = dfr.getPicklistValues();
        for (Schema.picklistEntry val : vals) {
            ops.add(new SelectOption(val.Label, val.Label));
        }
        return ops;
    }
    
    public SelectOption[] getMultipleLocationVals() {
        SelectOption[] ops = new List<SelectOption>();
        Schema.describeFieldResult dfr = Account_Referral__c.Training_Same_For_Multiple_Locations__c.getDescribe();
        Schema.picklistEntry[] vals = dfr.getPicklistValues();
        for (Schema.picklistEntry val : vals) {
            ops.add(new SelectOption(val.Label, val.Label));
        }
        return ops;
    }
    
    
    public void saveCheck() {
        string bug = '';
        if (ref.Contact__c == null) {
            canSave = false;
            bug += 'ref.Contact__c = null,';
        }
        if (ref.Current_Safety_Training__c == null) {
            canSave = false;
            bug += 'ref.Current_Safety_Training__c == null,';
        }
        if (ref.Training_Info_Relevance__c == null) {
            canSave = false;
            bug += 'ref.Training_Info_Relevance__c == null,';
        }
        if (ref.Safety_Training_Online__c == null) {
            canSave = false;
            bug += 'ref.Safety_Training_Online__c,';
        }
        if (ref.Training_Same_For_Multiple_Locations__c == null) {
            canSave = false;
            bug += 'ref.Training_Same_For_Multiple_Locations__c == null,';
        }
        if (ref.Part_Of_Safety_Training_Group__c == null) {
            canSave = false;
            bug += 'ref.Part_Of_Safety_Training_Group__c == null,';
        }
        if (ref.Willing_To_Speak_With_Consultants__c == null) {
            canSave = false;
            bug += 'ref.Willing_To_Speak_With_Consultants__c == null,';
        }
        if (ref.Willing_To_Speak_With_Consultants__c == 'Yes, but not me' && ref.Contact_To_Speak_To_Consultants__c == null) {
            canSave = false;
            bug += 'ref.Willing_To_Speak_With_Consultants__c == Yes, but not me && ref.Contact_To_Speak_To_Consultants__c == null,';
        }
        if (selReferralGuidance == null) {
            canSave = false;
            bug += 'selReferralGuidance == null,';
        }
        if (selReferralGuidance == 'OkayTimeframe' && selRecommendedTimeframe == null) {
            canSave = false;
            bug += 'selReferralGuidance == OkayTimeframe && selRecommendedTimeframe == null,';
        }
        system.debug(bug);
    } 
    
    public void sendEmail() {
        EmailTemplate refTemp = [SELECT id FROM EmailTemplate WHERE Name = 'Account Referral'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if (a.Online_Training_Account_Owner__c == null) {
            mail.setTargetObjectId([SELECT SetupOwnerId FROM MSDS_Custom_Settings__c WHERE Default_ODT_Referral_Email_Recipient__c = true LIMIT 1].SetupOwnerId);
        } else {
            mail.setTargetObjectId(a.Online_Training_Account_Owner__c);
        }
        mail.setWhatId(ref.Id);
        mail.setTemplateId(refTemp.Id);
        mail.setSaveAsActivity(false);
        mail.setUseSignature(false);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }
    
}