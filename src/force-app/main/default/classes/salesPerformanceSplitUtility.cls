//Test Class:testSalesPerformanceRequestIncentives
public class salesPerformanceSplitUtility {
    
    public static void processSplit(Sales_Performance_Request__c spr){
        //Query the spllits related to the passed in request
        Sales_Performance_Item_Split__c[] splits = [SELECT id, OwnerId, Product_Name__c, Booking_Amount__c, Incentive_Amount__c, 
                                                    SPI_Split_From__c, SPO_Split_From__c, SPI_Split_To__c, SPO_Split_To__c, Split_Group__c, Status__c
                                                    FROM Sales_Performance_Item_Split__c where Sales_Performance_Request__c =: spr.id];
        
        //find any existing SPO's, SPI's, and IT's that were created in any previous split of the order and add them to delete lists
        Sales_Performance_Order__c[] sposToDelete = new list<Sales_Performance_Order__c>();
        Sales_Performance_Item__c[] spisToDelete = new list<Sales_Performance_Item__c>();
        Incentive_Transaction__c[] itsToDelete = new list<Incentive_Transaction__c>(); 
        set<id> splitSpoIds = new set<id>();
        set<id> allSpoIds = new set<id>();
        for(Sales_Performance_Item_Split__c spis:splits){
            allSpoIds.add(spis.SPO_Split_To__c);
            if(spis.SPO_Split_To__c != spis.SPO_Split_From__c){
                splitSpoIds.add(spis.SPO_Split_To__c);
            }
        }
        if(splitSpoIds.size() > 0){
            Sales_Performance_Order__c[] splitExistingRecords = [SELECT id, Order_ID__c, Owner.Name, OwnerId, Contract_Number__c,
                                                                 Account__r.Name, Order_Date__c, Account__r.AdminId__c, Contract_Length__c, Month__c, Year__c, Account__c, isSplit__c, isSplitMaster__c,
                                                                 (SELECT id, OwnerId, Original_Booking_Amount__c, Product_Name__c, Name, Booking_Amount__c, Order_Item__c, Order_Item_ID__c, Order_ID__c,
                                                                  Order_Item__r.Admin_Tool_Product_Name__c, Order_Item__r.Product_Platform__c, Order_Item__r.Salesforce_Product_Name__c, Month__c, Order_Date__c, Year__c,
                                                                  Order_Item__r.Category__c, Contract_Length__c, Account__c, Invoice_Amount__c, Contract__c, Contract_Number__c FROM Sales_Performance_Items__r),
                                                                 (SELECT id FROM Incentive_Transactions__r)
                                                                 FROM Sales_Performance_Order__c
                                                                 WHERE id in:splitSpoIds];
            if(splitExistingRecords.size() > 0){
                set<String> splitOrderIDs = new set<String>();
                for(Sales_Performance_Order__c spo:splitExistingRecords){
                    sposToDelete.add(spo);
                    spisToDelete.addAll(spo.Sales_Performance_Items__r);
                    itsToDelete.addAll(spo.Incentive_Transactions__r);
                    splitOrderIDs.add(spo.Order_ID__c);
                }
                //Look for any associate credit records to delete
                Sales_Performance_Order__c[] splitAssociateCreditRecords = [SELECT id,
                                                                            (Select id FROM Sales_Performance_Items__r),
                                                                            (SELECT id FROM Incentive_Transactions__r)
                                                                            FROM Sales_Performance_Order__c
                                                                            WHERE Order_ID__c in:splitOrderIDs and Type__c = 'Associate Credit' and isSplit__c = true];
                for(Sales_Performance_Order__c spo:splitAssociateCreditRecords){
                    sposToDelete.add(spo);
                    spisToDelete.addAll(spo.Sales_Performance_Items__r);
                    itsToDelete.addAll(spo.Incentive_Transactions__r);
                }
                
                
            }
            
            
        }
        
        //Create the new split SPO's
        splits = salesPerformanceOrderUtility.insertSplitSPO(splits, spr.Reason__c); 
        
        //Create the new split SPI's if there was no error
        if(splits != null){
            splits = salesPerformanceItemUtility.insertSplitSPIs(splits); 
        }else{
            salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceSplitUtility', 'processSplit', 'There was an error step 1. SPR affected:'+spr);
        }
        
        //Create the new split IT's if there was no error
        if(splits != null){
            incentiveTransactionUtility.createSplitIncentives(splits); 
        }else{
            salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceSplitUtility', 'processSplit', 'There was an error step 2. SPR affected:'+spr);
        }
        
        //Set and update the spis's if there was no error
        if(splits != null){
            //Create and upsert SPIS's
            for(Sales_Performance_Item_Split__c split:splits){
                split.Status__c = 'Approved';
            }
            try {
                upsert splits;
                //Delete Old SPO's
                if(sposToDelete.size() > 0){
                    delete sposToDelete;
                }
                //Delete Old SPI's
                if(spisToDelete.size()>0){
                    delete spisToDelete;
                }
                //Delete Old IT's
                if(itsToDelete.size()>0){
                    delete itsToDelete;
                }
            } catch(exception e) {
                salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceSplitUtility', 'processSplit', string.valueOf(e));
            }
        }else{
            salesforceLog.createLog('Sales Incentives', true, 'salesPerformanceSplitUtility', 'processSplit', 'There was an error step 3. SPR affected:'+spr);
        }
        
    }
    
}