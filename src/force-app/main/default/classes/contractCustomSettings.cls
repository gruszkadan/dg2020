public class contractCustomSettings 
{
    public contractCustomSettings__c settings {get;set;}
    public User u {get;set;}
    
    public contractCustomSettings() 
    {
        u = [Select id, Name, FirstName, Email, ManagerID, Department__c, Group__c, ProfileID from User where id =: UserInfo.getUserId() LIMIT 1];
        settings = contractCustomSettings__c.getvalues(System.UserInfo.getProfileId());
    }
    
    public boolean viewApprovalPage() 
    {
        return contractCustomSettings__c.getInstance().View_Approval_Page__c;
    }
    
    public boolean viewManualBuild() 
    {
        return contractCustomSettings__c.getInstance().View_Manual_Build__c;
    }
    
    public boolean viewOrdersButtons() 
    {
        return contractCustomSettings__c.getInstance().View_Orders_Buttons__c;
    }
    
}