public class ameUtility {
 
    
    //Save method that will let the user save if all required feilds are filled in 
    public static Boolean canSave(Account_Management_Event__c ame){
        User u = [Select id, Name, Group__c from User where id =: UserInfo.getUserId()];
        
        id renewalRecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        id AtRiskRecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('At Risk').getRecordTypeId(); 
        id RAMRecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();
        id CancelRecordTypeid = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId(); 
        
        // set the save vairable to true at start 
        Boolean xCanSave = true;
        
        // Check to see if the required feild is left blank then if the statment is true the save variable will be changed to false (user cant save) 
        if(ame.OwnerId == null){
            xCanSave = false; }
        
        if(ame.Account__c == null){
            xCanSave = false;}
        
        if(ame.Contact__c == null){
            xCanSave = false;}
        
        if(ame.Status__c == null){
            xCanSave = false;}
        
        if(ame.recordtypeid == null){
            xCanSave = false;}
        
        //  Check to see if the required feild is blank and is in the the corresponding section on the page 
        
        if(ame.Batch_Month__c == null && ame.RecordTypeId == renewalRecordTypeId){
            xCanSave = false;
        }
        if(ame.Batch_Year__c == null && ame.RecordTypeId == renewalRecordTypeId){
            xCanSave = false;
        }
        if(ame.Price_Difference__c == null && ame.RecordTypeId == renewalRecordTypeId){
            xCanSave = false;
        } 
        if(ame.At_Risk_Status__c == null && ame.RecordTypeId == AtRiskRecordTypeId){
            xCanSave = false;
        }
        if(ame.At_Risk_Source__c == null && ame.RecordTypeId == AtRiskRecordTypeId){
            xCanSave = false;
        }
        if((ame.Notes__c == null || ame.Notes__c == '') &&  ame.RecordTypeId == CancelRecordTypeid){   
            xCanSave = false;
        }
        if(ame.Cancellation_Reason__c == null && u.Group__c == 'Retention' && ame.RecordTypeId == CancelRecordTypeid){   
            xCanSave = false;
        }
        if(ame.New_System__c == null && u.Group__c == 'Retention' && ame.RecordTypeId == CancelRecordTypeid){ 
            xCanSave = false;
        }
        if(ame.Renewal_Date__c == null && ame.RecordTypeId == renewalRecordTypeId){
            xCanSave = false;
        }
        return xCanSave;
    }
    
    public static void submitforapproval(Id ameId, String approvalcomments) 
    {
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments(approvalcomments);
        req.setObjectId(ameId);
        Approval.ProcessResult result = Approval.process(req);
    }
    
    public static void cancellationQueue(Account_Management_Event__c ameRecord)
    {
            Group orderCancellation = [Select Id From Group where DeveloperName = 'Order_Cancellation'];
            ameRecord.OwnerId = orderCancellation.Id;
            ameRecord.Status__c = 'Sent to Orders';
            ameRecord.Sent_to_Orders_By__c = UserInfo.getUserId();
            ameRecord.Sent_to_Orders_Date__c = Date.Today();
            update ameRecord;
        
    }

    
    //Mark Opportunity stage to pending Method
    public static void pendingOpp(List<Opportunity> oppList)
    {
        list<Opportunity> opp = new list<Opportunity>();
        for(Opportunity o: oppList) {
            o.StageName  = 'Pending Cancellation';
            opp.add(o);
        }
        if(oppList.size()>0){
            update opp;
        }
    }

    
    // updating ame related opp, tasks, quotes owner when ame owner changes
    
    public static void updateRelatedRecordsOwner(Account_Management_Event__c[] ameOwnerChange){
        List<Quote__c> qList = new List<Quote__c>();
        List<Opportunity> Oppy = new List<Opportunity>();
        List<Task> task = new List<Task>(); 
        Account_Management_Event__c[] ameRecordList = [Select Id, OwnerId,
                                                       (SELECT Id, OwnerId FROM Quotes__r WHERE Opportunity__r.IsClosed = false AND Quote__c.Status__c = 'Active'),
                                                       (SELECT Id, OwnerId FROM Opportunities__r WHERE IsClosed = false),
                                                       (SELECT Id, OwnerId FROM Tasks WHERE IsClosed = false)
                                                       FROM  Account_Management_Event__c WHERE ID IN :ameOwnerChange];
        for(Account_Management_Event__c ame:ameRecordList)   {
            for(Quote__c q :ame.Quotes__r){
                q.OwnerId = ame.ownerId;
                qList.add(q);
            }
            for(Opportunity Opp :ame.Opportunities__r){
                Opp.OwnerId = ame.ownerId;
                Oppy.add(Opp);
            }
            for(Task t :ame.Tasks){
                t.OwnerId = ame.ownerId;
                task.add(t);
            } 
        }
        try{
            if(qList.size()>0){
                update qlist;
            }
            if(Oppy.size()>0){
                update Oppy;
            }
            if(task.size()>0){
                update task;  
            }
        }catch(exception e) {
            salesforceLog.createLog('Account_Management_Event__c', true, 'ameUtility', 'updateRelatedRecordsOwner', string.valueOf(e));
        }   
    }
    
    
    //For each licensing order with a Term End between the specified dates, create a renewal AME and associated all order items in the order with this ame record.
    public static void  renewalAMEBatchCreation(Date contractTermStartDate, Date contractTermEndDate){
        
        Order_Item__c[] licensingOrderItems = [Select id, Order_ID__c, Account__c, Contract_Term_End_Date__c, Renewal_Start_Date__c
                                               FROM Order_Item__c
                                               WHERE Automatic_Renewal_Creation__c = true
                                               AND Renewal_Amount__c != NULL
                                               AND Renewal_Amount__c != 0
                                               AND Do_Not_Renew__c = False
                                               AND Admin_Tool_Order_Status__c = 'A'
                                               AND Account__r.Channel__c = null
                                               AND Renewal_Start_Date__c >=  :contractTermStartDate
                                               AND Renewal_Start_Date__c <= :contractTermEndDate
                                               AND Account_Management_Event__c = NULL
                                              ];
        
        
        if(licensingOrderItems.size() > 0){            
            
            id renewalId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
            id pendingRenewals = [Select queue.id from queueSobject where queue.developerName = 'Pending_Renewals'].queue.id;
            
            Map<String, orderWrapper> orderMap = new Map<String, orderWrapper>();
            Set<Id> acctIds = new Set<Id>();
            
            for(Order_Item__c oi: licensingOrderItems){
                orderMap.put(oi.Order_ID__c, new orderWrapper(oi));
                acctIds.add(oi.Account__c);
            }
            
            Account[] acct = [Select id, retention_owner__c,
                              (SELECT ID, Name from Contacts ORDER BY CustomerAdministratorId__c DESC NULLS LAST, LastModifiedDate DESC LIMIT 1), 
                              (Select id, Order_ID__c, Renewal_Count_In_Stats__c FROM Order_Items__r 
                               WHERE Order_ID__c IN :orderMap.keySet()
                               AND Renewal_Amount__c != null
                               AND Do_Not_Renew__c = false
                               AND Admin_Tool_Order_Status__c = 'A'
                               AND Account_Management_Event__c = NULL
                               ORDER BY Order_ID__c)
                              from Account where id IN :acctIds];
            
            map<string, id> getRepId = new map <string, id> ();
            
            User[] retentionUsers = [Select id, Name, LastName from User where isActive = true AND UserRole.Name != 'Director of Customer Success' AND Profile.Name LIKE '%Retention%'];
            for(user u: retentionUsers){
                getRepId.put(u.Name, u.id);
            }
            
            account_management_event__c[] ameToInsert = new List<account_management_event__c>();
            for(Account a: acct){           			
                for(Order_Item__c oi:a.Order_Items__r){                    
                    
                    if(orderMap.containsKey(oi.Order_ID__c)){
                        orderMap.get(oi.Order_ID__c).allOrderItems.add(oi);
                        if(orderMap.get(oi.Order_ID__c).newAME == NULL){
                            orderMap.get(oi.Order_ID__c).newAme = new account_management_event__c();
                            orderMap.get(oi.Order_ID__c).newAme.Account__c = a.id;
                            if(a.Contacts != NULL && a.Contacts.size()> 0){
                                orderMap.get(oi.Order_ID__c).newAme.Contact__c = a.contacts[0].ID;
                            }
                            orderMap.get(oi.Order_ID__c).newAme.Batch_Month__c = dateUtility.MonthName(contractTermEndDate.month(), TRUE);
                            orderMap.get(oi.Order_ID__c).newAme.Batch_Year__c = string.ValueOf(contractTermEndDate.year());
                            if(a.retention_owner__c != NULL && getRepId.containsKey(a.retention_owner__c)){
                                orderMap.get(oi.Order_ID__c).newAme.OwnerId = getRepId.get(a.retention_owner__c);
                            }else{
                                orderMap.get(oi.Order_ID__c).newAme.OwnerId = pendingRenewals;
                            }
                            orderMap.get(oi.Order_ID__c).newAme.Status__c = 'Not Yet Created';
                            orderMap.get(oi.Order_ID__c).newAme.Not_Yet_Processed__c = true;
                            orderMap.get(oi.Order_ID__c).newAme.Renewal_Date__c = orderMap.get(oi.Order_ID__c).licensingProduct.Renewal_Start_Date__c;
                            orderMap.get(oi.Order_ID__c).newAme.RecordTypeId = renewalId;
                            //orderMap.get(oi.Order_ID__c).newAme.Count_in_Stats__c = true;
                            orderMap.get(oi.Order_ID__c).newAme.Count_in_Stats__c = oi.Renewal_Count_In_Stats__c;
                            ameToInsert.add(orderMap.get(oi.Order_ID__c).newAme);
                        }else if(oi.Renewal_Count_In_Stats__c){
                            orderMap.get(oi.Order_ID__c).newAme.Count_in_Stats__c = true;
                        }
                    }
                }
            }
            
            try{
                insert ameToInsert;                
            }catch(exception e) {
                salesforceLog.createLog('Account_Management_Event__c', true, 'ameUtility', 'renewalAMEBatchCreation', string.valueOf(e));
            } 
            
            Order_Item__c[] orderItemsToUpdate = new List<Order_Item__c>();
            for(orderWrapper order: orderMap.values()){
                for(Order_Item__c item: order.allOrderItems){
                    item.Account_Management_Event__c = order.newAME.id;
                    orderItemsToUpdate.add(item);
                }
            }
            try{ 
                update orderItemsToUpdate;
            }catch(exception e) {
                salesforceLog.createLog('Account_Management_Event__c', true, 'ameUtility', 'renewalAMEBatchCreation', string.valueOf(e));
            }
        }
    }
    
    //OrderItem method to save cancellation RecordType(ameNewController)
    public static void canAME(Id ameId, List<Order_Item__c> oiList)
    {
        
        list<Order_Item__c> up = new list<order_item__c>();
        for(Order_Item__c oItem: oiList) {
            oItem.Cancelled_on_AME__c  = ameId;
            up.add(oItem);
        } 
        
        if(up.size()>0){
            update up;
        }
    }
    
    public class orderWrapper { 
        public Order_Item__c licensingProduct {get;set;}
        public string Order {get;set;}
        public account_management_event__c newAME {get;set;}
        public Order_Item__c[] allOrderItems {get;set;}
        
        public orderWrapper(Order_Item__c lProduct){
            licensingProduct = lProduct;
            Order = lProduct.Order_ID__c;
            allOrderItems = new List<Order_Item__c>(); 
        }
    }
    
    
    
}