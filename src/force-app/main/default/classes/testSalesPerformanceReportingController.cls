@isTest
public class testSalesPerformanceReportingController {
    
    //monthly summary general actions
    public static testmethod void test1() {
        SalesPerformanceReportingController con = new SalesPerformanceReportingController();
        con.getyears();
        con.getmonths();
        con.selectedMonth = '1';
        con.selectedYear = '2018';
        con.reportType = 'Month Summary';
        con.selectReport();
        System.assert(con.monthWord == 'January');
        
        con.backToStep1();
        System.assert(con.selectedYear == 'Select Year');
        
        con.canSendtoFinanceDates();
        
        
        System.debug(con.selectedMonth);
        System.debug(con.selectedYear);
        System.debug(con.reportType);
        System.debug(con.monthWord);
        
        
    }
    
    //get last months data
    public static testmethod void test2() {
        SalesPerformanceReportingController con = new SalesPerformanceReportingController();

        DateTime lastMonthDT = Datetime.now().addMonths(-1);

        con.selectedMonth = string.valueOf(lastMonthDT.month());
        con.selectedYear = string.valueOf(lastMonthDT.year());
        con.reportType = 'Month Summary';
        con.selectReport();
        
        System.debug(con.selectedMonth);
        System.debug(con.monthWord);
        
        
    }
    
    //see pending approvals
    public static testmethod void test3() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            //Create test data
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            DateTime d = datetime.now();
            String monthName= d.format('MMMMM');
            System.debug(monthName);
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = string.valueof(date.today().year());
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = string.valueof(date.today().year());
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = string.valueof(date.today().year());
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Month__c = monthName;
            s.Year__c = string.valueof(date.today().year());
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Sales_Director__c = dir.id;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            
            System.runAs(rep) {
                Sales_Performance_Request__c newSPR = new Sales_Performance_Request__c(Rollup_to_Rep__c = TRUE,Rollup_to_Manager__c = TRUE,Rollup_to_Director__c = TRUE,Rollup_to_VP__c = TRUE,Incentive_Month__c = 'Previous Month');
                newSPR.Type__c = 'Ghost Booking';
                newSPR.Status__c ='Pending Approval';
                newSPR.Requires_Approval__c = TRUE;
                newSPR.Assigned_Approver_1__c = dir.id;
                newSPR.OwnerId = rep.id;
                newSPR.Sales_Rep__c = rep.id;
                newSPR.Booking_Amount__c = 100;
                newSPR.Incentive_Amount__c = 20;
                insert newSPR;
            }
            
            
            SalesPerformanceReportingController con2 = new SalesPerformanceReportingController();
            con2.reportType = 'Pending Approvals';
            con2.selectReport();
            System.assert(con2.stepNumber == 2);
            System.assert(con2.pendingSprResults.size() > 0);
            
            
        }
    }
    
    
    //see monthly summary
    //process month finance report without pending requests?
    public static testmethod void test4() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            //Create test data
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            DateTime d = datetime.now();
            String monthName= d.format('MMMMM');
            System.debug(monthName);
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = string.valueof(date.today().year());
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = string.valueof(date.today().year());
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = string.valueof(date.today().year());
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Month__c = monthName;
            s.Year__c = string.valueof(date.today().year());
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Sales_Director__c = dir.id;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            Sales_Performance_Request__c newSPR = new Sales_Performance_Request__c(Rollup_to_Rep__c = TRUE,Rollup_to_Manager__c = TRUE,Rollup_to_Director__c = TRUE,Rollup_to_VP__c = TRUE,Incentive_Month__c = 'Current Month');
            newSPR.Type__c = 'Ghost Booking';
            newSPR.Status__c ='Pending Approval';
            newSPR.Requires_Approval__c = TRUE;
            newSPR.Assigned_Approver_1__c = mgr.id;
            newSPR.Assigned_Approver_2__c = dir.id;
            newSPR.Assigned_Approver_3__c = vp.id;
            newSPR.OwnerId = rep.id;
            newSPR.Sales_Rep__c = rep.id;
            newSPR.Booking_Amount__c = 100;
            newSPR.Incentive_Amount__c = 20;
            newSPR.Sales_Performance_Summary__c = s.id;
            insert newSPR;
            
            Email_Settings__c emailSetting = new Email_Settings__c();
            emailSetting.Type__c = 'Finance Incentives Report';
            emailSetting.Email__c = 't@test1.com1';
            emailSetting.Name = 'testClassEmailSetting';
            insert emailSetting;
            
            
            SalesPerformanceReportingController con2 = new SalesPerformanceReportingController();
            con2.selectedMonth = string.valueOf(Date.today().month());
            System.debug(con2.selectedMonth);
        	con2.selectedYear = string.valueOf(Date.today().year());
            System.debug(con2.selectedYear);
            con2.reportType = 'Month Summary';
            con2.selectReport();
            System.assert(con2.stepNumber == 2);
            System.assert(con2.spsResults.size() > 0);
            System.debug(con2.spsResults.size());
            System.debug(con2.pendingRelatedSprs.size());
           
            con2.downloadToExcel();
            

            
            //no approval action set, should return error and not continue process: approval should remain pending
            List<Sales_Performance_Request__c> pendingSpr= [SELECT Name,Status__c,Type__c FROM Sales_Performance_Request__c where Status__c ='Pending Approval'];
            System.assert(pendingSpr.size() == 1);

            
            //click button for modal to open
            con2.resetApprovals();
            
            //one item in approvals -- lets approve it
            con2.requestWrappers[0].approvalAction = 'Approve';
            con2.emailComments = 'Testing Emails. ';
            
            con2.completeProcess();
            System.debug('got to complete process');
      
            List<Sales_Performance_Request__c> pendingSprAfterProcess= [SELECT Name,Status__c,Type__c FROM Sales_Performance_Request__c where Status__c ='Pending Approval'];
            System.assert(pendingSprAfterProcess.size() == 0);
            System.assert(con2.reportSent  = true);
            
            //upon approval, button returns user to main page and clears fields - report Type should be null
            con2.returnToReporting();
         	System.assert(con2.reportType == NULL);
            
        }
    }
    
     public static testmethod void test5() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            //Create test data
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            DateTime d = datetime.now();
            String monthName= d.format('MMMMM');
            System.debug(monthName);
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = string.valueof(date.today().year());
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = string.valueof(date.today().year());
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = string.valueof(date.today().year());
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Month__c = monthName;
            s.Year__c = string.valueof(date.today().year());
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Sales_Director__c = dir.id;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            Sales_Performance_Request__c newSPR = new Sales_Performance_Request__c(Rollup_to_Rep__c = TRUE,Rollup_to_Manager__c = TRUE,Rollup_to_Director__c = TRUE,Rollup_to_VP__c = TRUE,Incentive_Month__c = 'Current Month');
            newSPR.Type__c = 'Ghost Booking';
            newSPR.Status__c ='Pending Approval';
            newSPR.Requires_Approval__c = TRUE;
            newSPR.Assigned_Approver_1__c = mgr.id;
            newSPR.Assigned_Approver_2__c = dir.id;
            newSPR.Assigned_Approver_3__c = vp.id;
            newSPR.OwnerId = rep.id;
            newSPR.Sales_Rep__c = rep.id;
            newSPR.Booking_Amount__c = 100;
            newSPR.Incentive_Amount__c = 20;
            newSPR.Sales_Performance_Summary__c = s.id;
            insert newSPR;
            
            Email_Settings__c emailSetting = new Email_Settings__c();
            emailSetting.Type__c = 'Finance Incentives Report';
            emailSetting.Email__c = 't@test1.com1';
            emailSetting.Name = 'testClassEmailSetting';
            insert emailSetting;
            
            
            SalesPerformanceReportingController con2 = new SalesPerformanceReportingController();
            con2.selectedMonth = string.valueOf(Date.today().month());
            System.debug(con2.selectedMonth);
        	con2.selectedYear = string.valueOf(Date.today().year());
            System.debug(con2.selectedYear);
            con2.reportType = 'Month Summary';
            con2.selectReport();
            System.assert(con2.stepNumber == 2);
            System.assert(con2.spsResults.size() > 0);
            System.debug(con2.spsResults.size());
            System.debug(con2.pendingRelatedSprs.size());
           
            con2.downloadToExcel();

            //no approval action set, should return error and not continue process: approval should remain pending
            List<Sales_Performance_Request__c> pendingSpr= [SELECT Name,Status__c,Type__c FROM Sales_Performance_Request__c where Status__c ='Pending Approval'];
            System.assert(pendingSpr.size() == 1);

            con2.completeProcess();
            System.assert(con2.reportSent == false);

            
            
        }
    }
    
}