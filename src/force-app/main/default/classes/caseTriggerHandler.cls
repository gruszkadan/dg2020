public class caseTriggerHandler {

// BEFORE INSERT
public static void beforeInsert(Case[] cases) {
    Case[] caseType90UpgradeTraining = new list<Case>(); 
    Case[] newCasesWithProjectStatuses = new list<Case>(); 
    
    for (integer i=0; i<cases.size(); i++) {
        if (cases[i].Type == '9.0 Upgrade Training') {
            caseType90UpgradeTraining.add(cases[i]);//
        }
        if (cases[i].Project_Status__c != NULL) {
            newCasesWithProjectStatuses.add(cases[i]);//
        }
    }
    
    
    if (caseType90UpgradeTraining.size() > 0) {
        caseUtility.validate90UpgradeCases(caseType90UpgradeTraining); 
    }
    if (newCasesWithProjectStatuses.size() > 0) {
        caseUtility.updateProjectStatusTimeStamps(newCasesWithProjectStatuses); 
    }
    
}
    
    
// BEFORE UPDATE
public static void beforeUpdate(Case[] oldCases, Case[] newCases) {
    Case[] ccSupportCases = new list<Case>();
    Case[] projectStatusUpdatedCases = new list<Case>();
    
    for (integer i=0; i<newCases.size(); i++) {
        // If the case type is 'CC Support'
        if (newCases[i].Type == 'CC Support') {
            ccSupportCases.add(newCases[i]);
        }
        if(oldCases[i].Project_Status__c != newCases[i].Project_Status__c){
            projectStatusUpdatedCases.add(newCases[i]);
        }
    }
    if (ccSupportCases.size() > 0) {
        caseUtility.caseHoverIssues(ccSupportCases);
    }
    if (projectStatusUpdatedCases.size() > 0) {
        caseUtility.updateProjectStatusTimeStamps(projectStatusUpdatedCases);
    }
    
}

// AFTER INSERT
public static void afterInsert(Case[] oldCases, Case[] newCases) {
    Case[] webToCase = new list<Case>();   
    Case[] caseType90UpgradeTraining = new list<Case>();
    Case[] caseNeedsRevRecRecords = new list<Case>();
    Case[] deliveryFollowUpCases = new list<Case>();
    
    for (integer i=0; i<newCases.size(); i++) {
        // If the case origin is 'Web Form'
        if (newCases[i].Origin == 'Web Form') {
            webToCase.add(newCases[i]);
        }
        if (newCases[i].Type == '9.0 Upgrade Training') {
            caseType90UpgradeTraining.add(newCases[i]);
        }
        if (newCases[i].Type == 'Delivery Follow Up' && newCases[i].AccountID != Null) {
            deliveryFollowUpCases.add(newCases[i]);
        }
        if(newCases[i].Project_Status__c  != NULL
            && newCases[i].Total_Project_Amount__c != NULL
            && newCases[i].Primary_Project_Type__c != NULL
            && (newCases[i].Type == 'Compliance Services' 
                || newCases[i].Type == 'Compliance Services - 1x Update Services' 
                || newCases[i].Type == 'Compliance Services - Data Migration'
                || newCases[i].Type == 'Compliance Services - Onsite'
                || newCases[i].Type == 'Compliance Services - Print Binder' 
                )
            ){
                caseNeedsRevRecRecords.add(newCases[i]); 
            }
    }
    
    if (webToCase.size() > 0) {
        caseUtility.webToCaseEmailMessage(webToCase);
    }
    if (caseType90UpgradeTraining.size() > 0) {
        VPMMilestoneUtility.milestoneCreation(caseType90UpgradeTraining); 
    } 
    if (caseNeedsRevRecRecords.size() > 0) {
        revenueRecognitionUtility.createRevRecRecords(caseNeedsRevRecRecords);
    }
    if (deliveryFollowUpCases.size() > 0) {
        caseUtility.DFUStatus(deliveryFollowUpCases);
    }
}

// AFTER UPDATE
public static void afterUpdate(Case[] oldCases, Case[] newCases) {
    Case[] caseOwnerContactStatusChange = new list<Case>();
    Case[] caseNeedsRevRecRecords = new list<Case>();
    Case[] casesForAdditionalRRTs = new list<Case>();
    Case[] totalProjectAmountChangesOld = new list<Case>();
    Case[] totalProjectAmountChangesNew = new list<Case>();
    Case[] deliveryFollowUpCases = new list<Case>();
    
    for (integer i=0; i<newCases.size(); i++) {
        if (newCases[i].Type == '9.0 Upgrade Training' 
            && (oldCases[i].OwnerId != newCases[i].OwnerId
                || oldCases[i].ContactId != newCases[i].ContactId
                || oldCases[i].Status != newCases[i].Status)) {
                    caseOwnerContactStatusChange.add(newCases[i]);
                }
        if (newCases[i].Type == 'Delivery Follow Up' && newCases[i].AccountID != Null && oldCases[i].Status != newCases[i].Status && oldCases[i].DFU_Status__c != newCases[i].DFU_Status__c)
        {
            deliveryFollowUpCases.add(newCases[i]);
        }
        
        if(((oldCases[i].Project_Status__c == NULL && newCases[i].Project_Status__c != NULL)
            || (oldCases[i].Total_Project_Amount__c == NULL && newCases[i].Total_Project_Amount__c != NULL)
            )
            && newCases[i].Project_Status__c  != NULL
            && newCases[i].Total_Project_Amount__c != NULL
            && (newCases[i].Type == 'Compliance Services'
                || newCases[i].Type == 'Compliance Services - 1x Update Services'
                )
            ){
                caseNeedsRevRecRecords.add(newCases[i]); 
            }
        
        //
        if(((oldCases[i].Project_Status__c == NULL && newCases[i].Project_Status__c != NULL)
            || (oldCases[i].Total_Project_Amount__c == NULL && newCases[i].Total_Project_Amount__c != NULL)
            || (oldCases[i].Primary_Project_Type__c == NULL && newCases[i].Primary_Project_Type__c != NULL)
            )
            && newCases[i].Primary_Project_Type__c != NULL
            && newCases[i].Project_Status__c  != NULL
            && newCases[i].Total_Project_Amount__c != NULL
            && (newCases[i].Type == 'Compliance Services - Data Migration'
                || newCases[i].Type == 'Compliance Services - Onsite'
                || newCases[i].Type == 'Compliance Services - Print Binder'
                )
            ){
                caseNeedsRevRecRecords.add(newCases[i]); 
            }
        
        
        
        
        if(oldCases[i].Total_Project_Amount__c != newCases[i].Total_Project_Amount__c 
            && oldCases[i].Total_Project_Amount__c != null
            && newCases[i].Total_Project_Amount__c != null)
        {
            totalProjectAmountChangesOld.add(oldCases[i]);
            totalProjectAmountChangesNew.add(newCases[i]);
        }
        if((oldCases[i].Primary_Project_Type__c == NULL && newCases[i].Primary_Project_Type__c != NULL)
            && oldCases[i].Total_Project_Amount__c != NULL
            && oldCases[i].Project_Status__c != NULL
            && newCases[i].Project_Status__c  != NULL
            && newCases[i].Total_Project_Amount__c != NULL
            && (newCases[i].Type == 'Compliance Services' || newCases[i].Type == 'Compliance Services - 1x Update Services')
            ){
                casesForAdditionalRRTs.add(newCases[i]); 
            }
    }
    
    if (caseOwnerContactStatusChange.size() > 0) {
        VPMMilestoneUtility.milestoneUpdates(caseOwnerContactStatusChange);
    }
    if (caseNeedsRevRecRecords.size() > 0) {
        revenueRecognitionUtility.createRevRecRecords(caseNeedsRevRecRecords);
    }
    if (casesForAdditionalRRTs.size() > 0) {
        revenueRecognitionUtility.createAdditionalRevRecTransactions(casesForAdditionalRRTs);
    }
    if(totalProjectAmountChangesOld.size() > 0 & totalProjectAmountChangesNew.size() > 0){
        revenueRecognitionUtility.updatePhaseAmountFromCase(totalProjectAmountChangesOld, totalProjectAmountChangesNew);
    }
    if (deliveryFollowUpCases.size() > 0) {
        caseUtility.DFUStatus(deliveryFollowUpCases);
    }
    
   }
}