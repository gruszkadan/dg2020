@isTest(SeeAllData=true)
private class testsfUsage
{
    static testmethod void runTestOne() 
    {
        User tUser = [ SELECT Id, Name
                      FROM User
                      WHERE LastName = 'Werner' LIMIT 1 ];
        
        SF_Usage__c sfu = [ SELECT Id, Name, Project_List__c, AccountLocator_Views__c, AccountLocator_Searches__c 
                          FROM SF_Usage__c
                          WHERE Name = 'MASTER' LIMIT 1 ];
        
        
        sfUsage myController = new sfUsage();
        
        
        myController.getApps();
        myController.find();
    }
    
    static testmethod void runTestTwo() 
    {
        User tUser = [ SELECT Id, Name
                      FROM User
                      WHERE LastName = 'Werner' LIMIT 1 ];
        
        SF_Usage__c sfu = [ SELECT Id, Name, Project_List__c, AccountLocator_Views__c, AccountLocator_Searches__c 
                          FROM SF_Usage__c
                          WHERE Name = 'MASTER' LIMIT 1 ];
        
        
        sfUsage_add myController = new sfUsage_add();
        
        
        myController.addView( 'AccountLocator' );
        myController.addSearch( 'AccountLocator' );
    }
}