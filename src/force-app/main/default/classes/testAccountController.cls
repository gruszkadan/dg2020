@isTest(SeeAllData=true)
private class testAccountController {
    
    static testmethod void test1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        newAccount.Corporate_Account__c = true;
        newAccount.Corporate_Pricing_Expiration_Date__c = date.today() + 30;
        newAccount.Corporate_Account_Locations_Link__c = 'testlink';
        newAccount.Corporate_Pricing_Description__c = 'test';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.LastName = 'Test';
        newContact.Accountid = newAccount.id;
        insert newContact;
        
        Account_Referral__c ref1 = new Account_Referral__c();
        ref1.Account__c = newAccount.id;
        ref1.Date__c = date.today();
        ref1.Contact__c = newContact.id;
        ref1.Referrer__c = '00580000003UChI';
        ref1.Type__c = 'Training';
        ref1.Willing_To_Speak_With_Consultants__c = 'Yes';
        insert ref1;        
        
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        ApexPages.currentPage().getParameters().put('OwnerType', 'e');
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        accountController myController = new accountController(testController);
        
        myController.authReferral();
        myController.getContactSelectList();
        myController.setECSOwner();
        myController.setAuthoringOwner();
        myController.referralCancel();
        myController.corpReferral();
        myController.requestUploadGrant();
        myController.submitCorpApp();
        myController.checkReferrals();
        myController.referralOwnerCheck();
        myController.ergoReferral();
        myController.chemReferral();
        ApexPages.currentPage().getParameters().put('refViewId', ref1.id);
        myController.viewReferral();
    }
    
    static testmethod void test5() 
    {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        newAccount.Corporate_Account__c = true;
        newAccount.Corporate_Pricing_Expiration_Date__c = date.today() + 30;
        newAccount.Corporate_Account_Locations_Link__c = 'testlink';
        newAccount.Corporate_Pricing_Description__c = 'test';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.LastName = 'Test';
        newContact.Accountid = newAccount.id;
        insert newContact;        
        
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        ApexPages.currentPage().getParameters().put('OwnerType', 'e');
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        accountController myController = new accountController(testController);
        
        myController.sendToES();
        myController.requestUpdate();
        myController.saveATUpdate();
        myController.requestUploadGrant();
        myController.saveActivateProducts();
        myController.OwnershipNotificationEmail();
        myController.getmyfile();
        myController.upload();
    }
    
    static testmethod void test2() 
    {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Prospect';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        newAccount.EHS_Owner__c = '00580000003UChI';
        insert newAccount;
        
        User u = [SELECT Id, Name, Email FROM User WHERE Id = '00580000003UChI'];
        EmailTemplate authReferral = [Select Id From EmailTemplate where Name='Account Referral'];
        
        Corporate_Account_Referral__c ref = new Corporate_Account_Referral__c();
        ref.Account__c = newAccount.id;
        insert ref;
        
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        ApexPages.currentPage().getParameters().put('OwnerType', 'm');
        ApexPages.currentPage().getParameters().put('refId', ref.id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        accountController myController = new accountController(testController);
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(newAccount.Authoring_Account_Owner__c);
        mail.setWhatId(newAccount.Id);
        mail.setTemplateId(authReferral.Id);
        mail.setSaveAsActivity(FALSE);
        mail.setUseSignature(FALSE);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        myController.authReferral();
        myController.ownershipchange();
        myController.getContactSelectList();
        myController.requestUpdate();
        myController.saveATUpdate();
        myController.setECSOwner();
        myController.setAuthoringOwner();
        myController.referralCancel();
        myController.saveOnDetail();
        myController.saveOwnershipChange();
        myController.showAttach();
        myController.hideAttach();
        myController.queryRefs();
        myController.gotoSalesLocationMap();
        myController.addUse();
        myController.ehsReferral();
        myController.odtReferral();
    }
    
    static testmethod void test3() 
    {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Prospect';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Service_Label_Authoring__c = TRUE;
        newAccount.Authoring_Service_MSDS_Authoring__c = TRUE; 
        newAccount.Authoring_Service_Regulatory_Consulting__c = TRUE;
        newAccount.Authoring_Service_Translation_Service__c = TRUE;
        insert newAccount;
        
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        ApexPages.currentPage().getParameters().put('OwnerType', 'm');
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        accountController myController = new accountController(testController);
        
        myController.authReferral();
        myController.ownershipchange();
        myController.getContactSelectList();
        myController.requestUpdate();
        myController.saveATUpdate();
        myController.setECSOwner();
        myController.setAuthoringOwner();
        myController.referralCancel();
    }
    static testmethod void test4() 
    {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c = null;
        insert newAccount;
        
        Contact c = new Contact(FirstName='Bill', LastName='Test', AccountId=newAccount.id);
        insert c;
        
        Task t = new Task();
        t.OwnerId = newAccount.OwnerId;
        t.Subject = 'Test task';
        t.WhatId = newAccount.id;
        t.WhoId = c.id;
        t.Status = 'Completed';
        t.Important_Note__c = true;
        insert t;
        
        
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        accountController myController = new accountController(testController);
        
        
        myController.environment = 'Production';
        myController.submitCorpApp();
        myController.setAuthoringOwner();
        myController.setECSOwner();
        myController.gotoActivityHistory();
        myController.getMailMergeURL();
    }
    
    static testmethod void testProductInfoController() 
    {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Prospect';
        newAccount.OwnerID = [SELECT Id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].Id;
        newAccount.Active_Additional_Compliance_Solutions__c = 't1';
        newAccount.Active_Compliance_Services_Products__c = 't2';
        newAccount.Active_MSDS_Management_Products__c = 't3';
        newAccount.Cancelled_Additional_Compliance_Solution__c = 't4';
        newAccount.Cancelled_Compliance_Services_Products__c = 't5';
        newAccount.Cancelled_MSDS_Management_Products__c = 't6';
        newAccount.Done_Additional_Compliance_Solutions__c = 't7';
        newAccount.Done_Compliance_Services_Products__c = 't8';
        newAccount.Done_MSDS_Management_Products__c = 't9';
        newAccount.EHS_Active_Licensing__c = 't10';
        newAccount.EHS_Active_Services__c = 't11';
        newAccount.EHS_Active_Add_On_Services__c = 't12';
        newAccount.EHS_Cancelled_Licensing__c = 't13';
        newAccount.EHS_Cancelled_Services__c = 't14';
        newAccount.EHS_Cancelled_Add_On_Services__c = 't15';
        newAccount.EHS_Done_Licensing__c = 't16';
        newAccount.EHS_Done_Services__c = 't17';
        newAccount.EHS_Done_Add_On_Services__c = 't18';
        insert newAccount;
        
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        productInfoController myController = new productInfoController();
        
        myController.activateProducts();
        myController.saveAP();
        myController.cancelAP();
    }
    
    static testmethod void testBillingRatesController() 
    {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Prospect';
        newAccount.OwnerID = [SELECT Id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].Id;
        insert newAccount;
        
        ApexPages.currentPage().getParameters().put('id', newAccount.Id);
        billingRatesController myController = new billingRatesController();
        
        myController.gotoCompEdit();
        myController.saveEdits();
        myController.cancelEdits();
    }
    
    static testmethod void test6() 
    {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.LastName = 'Test';
        newContact.Accountid = newAccount.id;
        insert newContact;
        
        
        Account a = [Select Active_Products__c from Account where id=: newAccount.id];
        a.Active_Products__c ='HQ Account';
        update a;
    }
    
    static testmethod void testNPS1(){
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        newAccount.Chemical_Management_NPS_Score__c = 5;
        newAccount.Chemical_Management_NPS_Ranking__c ='Promoter';
        newAccount.Chemical_Management_NPS_Last_Survey_Date__c = date.today();
        newAccount.Chemical_Management_Prev_NPS_Score__c = 3;
        insert newAccount;
        
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        accountController myController = new accountController(testController);        
    }
    
    static testmethod void testNPS2(){
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        newAccount.Chemical_Management_NPS_Score__c = 5;
        newAccount.Chemical_Management_NPS_Ranking__c ='Promoter';
        newAccount.Chemical_Management_NPS_Last_Survey_Date__c = date.today();
        newAccount.Chemical_Management_Prev_NPS_Score__c = 7;
        insert newAccount;
        
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        accountController myController = new accountController(testController);        
    }   
    
    
     static testmethod void testSendToMM() 
    {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.Enterprise_Sales__c = true;
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        newAccount.Corporate_Account__c = true;
        newAccount.Corporate_Pricing_Expiration_Date__c = date.today() + 30;
        newAccount.Corporate_Account_Locations_Link__c = 'testlink';
        newAccount.Corporate_Pricing_Description__c = 'test';
        insert newAccount;
        
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        ApexPages.currentPage().getParameters().put('OwnerType', 'e');
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        accountController myController = new accountController(testController);

        myController.sendToMM();
        Account checkMMAccount = [Select Enterprise_Sales__c from Account where id =:newAccount.Id];
        System.assert(checkMMAccount.Enterprise_Sales__c == false);
        
    }
    
    
}