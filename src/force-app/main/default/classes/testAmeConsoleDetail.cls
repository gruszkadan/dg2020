@isTest()
private class testAmeConsoleDetail {
    
    static testmethod void test1() {
        
        Account_Management_Event__c newAme = new Account_Management_Event__c();
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1];
        // user Mark = [SELECT Id FROM User WHERE Name = 'Mark McCauley' LIMIT 1];
        
        
        //Insert Account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.NAICS_Code__c = 'Test'; 
        newAccount.Customer_Status__c ='Prospect'; 
        newAccount.OwnerId = Dan.Id;
        insert newAccount;
        
        //Insert Contact
        Contact Con = new Contact();
        Con.LastName = 'Lahners';
        Con.AccountId = newAccount.Id;
        Con.OwnerID= Dan.Id;
        insert Con; 
        
        //Insert Opportunity
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Test Opportunity';
        Opp.StageName= 'System Demo';
        Opp.OwnerID= Dan.Id;
        opp.AccountId = newAccount.id;
        Opp.CloseDate = date.today();
        Opp.Account_Management_Event__c = newAme.Id;
        insert Opp;
        
        //Insert AME
        newAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        newAme.OwnerId = Dan.Id;
        newAme.Status__c = 'Active';
        newAme.Account__c = newAccount.Id;
        newAme.Contact__c = Con.Id;
        newAme.Batch_Month__c = 'Jan';
        newAme.Batch_Year__c = '2019';
        newAme.Price_Difference__c = 1.00;
        insert newAme;
        
        
        //Variables used in controller
        ameConsoleToDoDetailController ameConsole = new ameConsoleToDoDetailController();
        ameConsole.ameRecordId = newAme.Id;  
        ameConsole.cView = newAme.Id;  
        ameConsole.sField = newAme.Id;  
        ameConsole.sDirection = newAme.Id;  
        ameConsole.vUser = newAme.Id;  
        ameConsole.errorMessage = newAme.Id; 
        ameConsole.alertIcon = newAme.Id;
        ameConsole.cancellationQueue = newAme;
        ameConsole.relatedAME = newAme;
        ameConsole.isSuccess = true;
        ameConsole.getInitializeRecord();  
        
        //Submit for approval method
        ameConsole.submitforapproval();
        Account_Management_Event__c newAme2 = new Account_Management_Event__c();
        newAme2 = [SELECT Id, Status__c FROM Account_Management_Event__c WHERE Id = :newAme.Id];
        System.assertEquals('Pending Approval', newAme2.Status__c);
        
        //contact dropdown method
        ameConsole.getContactSelectList();
        ameConsole.inputFieldEdit();
        
        //Checking save method
        newAme.Notes__c = '1234';
        ameConsole.save();
        System.assert(newAme.Notes__c == '1234');
        
        //Cancel method
        ameConsole.Cancel();
    } 
    
    static testmethod void test2() {
        
        Account_Management_Event__c newAme = new Account_Management_Event__c();
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1];  
        
        //Insert Account
        Account Acc = new Account();
        Acc.Name = 'Ford';
        Acc.Customer_Status__c = 'Prospect'; 
        Acc.OwnerID= Dan.Id;
        Acc.NAICS_Code__c = '123456';
        insert Acc; 
        
        //Insert Contact
        Contact Con = new Contact();
        Con.LastName = 'Lahners';
        Con.AccountId = Acc.Id;
        Con.OwnerID= Dan.Id;
        insert Con;
        
        //insert new opp
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Ford1';
        Opp.StageName= 'System Demo';
        Opp.OwnerID= Dan.Id;
        opp.AccountId = Acc.id;
        Opp.CloseDate = date.today();
        Opp.Account_Management_Event__c = newAme.Id;
        insert Opp;
        
        //Insert New Ame
        newAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();      
        newAme.Status__c = 'Active';
        newAme.Notes__c = '123';
        newAme.Account__c = Acc.Id;
        newAme.Contact__c = Con.Id;
        newAme.Type__c = 'Escalation';
        insert newAme;
        
        //Variables used in controller
        ameConsoleToDoDetailController ameConsole = new ameConsoleToDoDetailController();
        ameConsole.ameRecordId = newAme.Id;  
        ameConsole.cView = newAme.Id;  
        ameConsole.sField = newAme.Id;  
        ameConsole.sDirection = newAme.Id;  
        ameConsole.vUser = newAme.Id;  
        ameConsole.RecordType = newAme.Id; 
        ameConsole.getInitializeRecord();  
        
        //Convert to At risk Method
        newAme.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('At Risk').getRecordTypeId();
        newAme.OwnerId = Dan.Id;
        newAme.Status__c = 'Active';
        newAme.Notes__c = '1234';
        newAme.Account__c = Acc.Id;
        newAme.Contact__c = Con.Id;
        newAme.At_Risk_Status__c = 'Active';
        newAme.At_Risk_Source__c = 'Behavior';
        update newAme;
        ameConsole.convertToAtRisk();
        
        //saveAt risk Method
        ameConsole.atRiskAME.At_Risk_Status__c = 'Active';
        ameConsole.atRiskAME.At_Risk_Source__c = 'Behavior';
        ameConsole.saveAtRisk();
        
        Account_Management_Event__c Ame1 = new Account_Management_Event__c();
        Ame1 = [SELECT Id, RecordType.Name FROM Account_Management_Event__c WHERE Id = :ameConsole.atRiskAME.Id];
        System.assert(Ame1.RecordType.Name == 'At Risk');
        
        ameConsole.rCancellation();
        ameConsole.sentToOrders();
        ameconsole.loadOrderItemWrappers();
        
    }    
    
    
    static testmethod void test3() {
        
        Account_Management_Event__c Ame = new Account_Management_Event__c();
        user Dan = [SELECT Id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1];  
        Account_Management_Event__c Ame1 = new Account_Management_Event__c();
        
        Ame.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('At Risk').getRecordTypeId();
        Ame.OwnerId = Dan.Id;
        Ame.Status__c = 'Active';
        Ame.At_Risk_Source__c = 'Usage';
        Ame.At_Risk_Status__c = 'Active';
        Ame.Cancellation_Reason__c = 'Fake Order';   
        Ame.New_System__c = 'Paper System';
        insert Ame;
        
        //Insert Account
        Account Acc = new Account();
        Acc.Name = 'Ford';
        Acc.Customer_Status__c = 'Prospect'; 
        Acc.OwnerID= Dan.Id;
        Acc.NAICS_Code__c = '123456';
        insert Acc; 
        
        //Insert Contact
        Contact Con = new Contact();
        Con.LastName = 'Lahners';
        Con.AccountId = Acc.Id;
        Con.OwnerID= Dan.Id;
        insert Con;
        
        //insert new opp
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Ford1';
        Opp.StageName= 'System Demo';
        Opp.OwnerID= Dan.Id;
        opp.AccountId = Acc.id;
        Opp.CloseDate = date.today();
        Opp.Account_Management_Event__c = Ame.Id;
        insert Opp;
        
        //Creating list of order items
        Order_Item__c[] testOIs = new list<Order_Item__c>();
        testDataUtility Util = new testDataUtility(); 
        
        //adding order Item 1
        Order_Item__c oi1 = util.createOrderItem('TestVP', 'test1');
        oi1.Order_ID__c = 'test456';
        oi1.Month__c = 'January';
        oi1.Salesforce_Product_Name__c = 'HQ';
        oi1.Category__c = 'MSDS Management';
        oi1.Product_Platform__c = 'MSDSonline';
        testOIs.add(oi1);
        
        Order_Item__c oi2 = util.createOrderItem('TestVP', 'test2');
        oi2.Order_ID__c = 'test456';
        oi2.Month__c = 'January';
        oi2.Salesforce_Product_Name__c = 'HQ';
        oi2.Category__c = 'MSDS Management';
        oi1.Product_Platform__c = 'MSDSonline';
        testOIs.add(oi2);
        insert testOIs;
        
         //Variables used in controller
        ameConsoleToDoDetailController ameConsole = new ameConsoleToDoDetailController();
        ameConsole.ameRecordId = Ame.Id;  
        ameConsole.cView = Ame.Id;  
        ameConsole.sField = Ame.Id;  
        ameConsole.sDirection = Ame.Id;  
        ameConsole.vUser = Ame.Id;  
        ameConsole.errorMessage = Ame.Id; 
        ameConsole.alertIcon = Ame.Id;
        ameConsole.isSuccess = true;
        ameConsole.getInitializeRecord();
        ameConsole.rCancellation();
   
        //setting all fields to save cancellation AME
        Ame1.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();
        System.debug(Ame.RecordTypeId);
        Ame1.Contact__c = Con.id;
        Ame1.Account__c = Acc.Id;	
        Ame1.OwnerId = Dan.Id;
        Ame1.Status__c = 'Active';
        Ame1.Cancellation_Reason__c = 'Fake Order';   
        Ame1.New_System__c = 'Paper System';
        ameConsole.oItemWrapper[0].isSelected = true;
        //ameConsole.saveCancellation();
        
        Order_Item__c updatedOrderItem = [Select id, Account_Management_Event__c
                                          from Order_Item__c
                                          Where id=:oi1.Id];
        System.debug(updatedOrderItem.Account_Management_Event__c);
        ameConsole.oItemWrapper[0].isSelected = true;
        System.assert(updatedOrderItem.Account_Management_Event__c == Ame1.Id);
        ameConsole.saveCancellation();
        ameConsole.xDelete();
        ameConsole.returnAmeConsole();
        system.debug(Ame1.Cancellation_Reason__c);
        
        
    }
}