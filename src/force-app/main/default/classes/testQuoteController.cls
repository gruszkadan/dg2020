@isTest(SeeAllData=true)
private class testQuoteController {
    
    static testmethod void quoteControllerTest() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Quote_Promotion__c p = new Quote_Promotion__c(Name = 'Promo 123', Code__c = 'P123', Start_Date__c = date.today(), End_Date__c = date.today().addDays(15));
        insert p;
        
        //insert test option package
        Quote_Options__c qop = new Quote_Options__c(Type__c='New');
        insert qop;
        
        Quote__c[] options = new List<Quote__c>();
        for (integer i=0; i<3; i++) {
            Quote__c q = new Quote__c();
            q.Account__c = newAccount.Id;
            q.Contact__c= newContact.Id;
            q.OwnerID='00580000003UlflAAC';
            q.Quote_Options__c = qop.id;
            q.Contract_Length__c = '3 Years';
            q.Option_Name__c = 'Test option';
            q.Promotion__c = p.id;
            q.Include_Option_In_Proposal__c = true;
            q.Approve_Expired_Promo_Pricing_Terms__c = true;
            options.add(q); 
        }
        insert options;
        
        Quote__c quote = options[0];
        
        Attachment a = new Attachment(Name = 'TestAtt', Body = blob.valueOf('Test'), ParentId = quote.id);
        insert a;
        
        Quote_Product__c[] qps = new List<Quote_Product__c>();
        for (Quote__c q : options) {
            Quote_Product__c myItem = new Quote_Product__c();
            myItem.Product__c ='01t80000002NON4';
            myItem.Quote__c = q.ID;
            myItem.Approval_Required_by__c = 'Manager';
            qps.add(myItem);
        }
        insert qps;     
       
        // load the page        
        PageReference pageRef = Page.quotedetail;
        pageRef.getParameters().put('Id',quote.ID);
        Test.setCurrentPageReference(pageRef);
        
        // load the extension
        ApexPages.currentPage().getParameters().put('id', quote.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteController myController = new quoteController(testController);
        String step2 = myController.step2().getUrl();
        String addProducts = myController.addProducts().getUrl();
        String convertQuote = myController.convertQuote().getUrl();
        String backToQuote = myController.backToQuote().getUrl();
        String cloneQuote = myController.cloneQuote().getUrl(); 
        string approvalstring = myController.approval().getUrl(); 
        String refreshOpportunity = myController.refreshOpportunity().getUrl(); 
        Integer startCount = myController.productList.size();
        String msg = myController.xmlMessage;
        
        myController.getappReasons();
        myController.getproposedEndDate();
        myController.getProducts();
        
        myController.buildProposal();
        myController.buildNewProposal();
        
        myController.step2();
        System.assertEquals('/apex/quoteproductentrystep2?id='+quote.Id, step2);
        
        myController.addProducts();
        System.assertEquals('/apex/quoteproductentry?id='+quote.Id, addProducts);
        
        myController.convertQuote();
        System.assertEquals('/apex/create_oppty?id='+quote.Id, convertQuote); 
        
        myController.backToQuote();
        System.assertEquals('/apex/quotedetail?id='+quote.Id, backToQuote); 
        
        myController.refreshOpportunity();
        System.assertEquals('/apex/quote_refresh_opportunity?ID='+quote.Id, refreshOpportunity);
        
        myController.cloneQuote();
        System.assertEquals('/apex/quoteclone?ID='+quote.Id, cloneQuote);
        
        myController.approval();
        System.assertEquals('/apex/quotedetail?id='+quote.Id, approvalstring);  
        
        
        myController.getProducts(); 
        Quote_Product__c[] qp2 = [select Id, Approval_Required_by__c from Quote_Product__c where Quote__c =:quote.ID  AND Approval_Required_by__c != 'N/A' AND Approval_Status__c != 'Approved' AND Approval_Status__c != 'Pending'];
        system.assert(qp2.size()==1);
        system.assert(myController.productList.size()==1);
        
        myController.submitForApproval(); 
        Quote_Product__c[] qp3 = [select Id from Quote_Product__c where Quote__c =:quote.ID];
        system.assert(myController.productList.size()==1); 
        system.assertEquals('/apex/quotedetail?id='+quote.Id, approvalstring);  
        
        //submit for approval
        Approval.processSubmitRequest req1 = new Approval.processSubmitRequest();
        req1.setComments('Test');
        req1.setObjectId(quote.Id);
        Id[] testAppList = new List<Id>();
        testAppList.add([SELECT Id FROM User WHERE LastName = 'Werner' LIMIT 1].Id);
        req1.setNextApproverIds(testAppList);
        
        Approval.ProcessResult result1 = Approval.process(req1);
        myController.approveQuote();
        
        Approval.ProcessResult result2 = Approval.process(req1);
        myController.rejectQuote();
        
        Approval.ProcessResult result3 = Approval.process(req1);
        myController.recallQuote();
        
        ApexPages.currentPage().getParameters().put('app', '0');
        myController.checkApproval();
        
        ApexPages.currentPage().getParameters().put('app', '1');
        myController.checkApproval();
        
        ApexPages.currentPage().getParameters().put('app', '2');
        myController.checkApproval();
        
        ApexPages.currentPage().getParameters().put('attId', a.id);
        myController.getAtts();
        myController.editAttachment();
        myController.viewAttachment();
        myController.delAttachment();
        myController.save();
      
    }
    
    static testmethod void testCloneQuote() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
        
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c ='01t80000002NON4';
        myItem.Quote__c = newQuote.ID;
        insert myItem;
        
        
        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        cloneQuote myController = new cloneQuote(testController);
        mycontroller.createQuote(); 
    }
    
    static testmethod void testConverToOpportunity1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Num_of_Employees__c = 50;
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Healthcare_Customer__c='No';
        newQuote.Contract_Length__c = '1 Year';
        newQuote.Travelers_Customer__c = 'No';
        newQuote.VPPPA_Customer__c = 'No';
        insert newQuote;
        
        // Lookup an existing item
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = '01t80000002NON4';
        myItem.PricebookEntryId__c ='01u80000006eap9';
        myItem.Quantity__c = 1;
        myItem.Y1_Quote_Price__c = 2.50;
        myItem.Approval_Required_by__c = 'Sales VP';
        myItem.Quote__c = newQuote.Id;
        myItem.Year_1__c = TRUE;
        insert myItem;
        
        ApexPages.currentPage().getParameters().put('ID', newQuote.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        convertToOpportunity myController = new convertToOpportunity(testController);
        myController.Oppty = new Opportunity();
        myController.Oppty.Name = 'name1';
        myController.Oppty.StageName = 'test stage';
        myController.Oppty.closeDate = date.TODAY();
        myController.createOppty();  
        
        
        

    }
 
  
    //createContract
    static testmethod void testCreateContract() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Num_of_Employees__c = 50;
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.Healthcare_Customer__c='No';
        newQuote.Contract_Length__c = '1 Year';
        newQuote.Travelers_Customer__c = 'No';
        newQuote.VPPPA_Customer__c = 'No';
        insert newQuote;
        
        // Lookup an existing item
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = '01t80000002NON4';
        myItem.PricebookEntryId__c ='01u80000006eap9';
        myItem.Quantity__c = 1;
        myItem.Y1_Quote_Price__c = 2.50;
        myItem.Approval_Required_by__c = 'Sales VP';
        myItem.Quote__c = newQuote.Id;
        myItem.Year_1__c = TRUE;
        insert myItem;
        
        ApexPages.currentPage().getParameters().put('id', newQuote.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteController myController = new quoteController(testController);
        myController.createContract();    
        
        Opportunity[] findOp = [Select id, StageName, ContactID__c, Opportunity_Type__c, CloseDate from Opportunity where AccountID = :newAccount.id];
        System.debug(findOp);
        System.assert(findOp.size() == 1);
        System.assert(findOp[0].CloseDate == date.today());
        System.assert(findOp[0].Opportunity_Type__c == 'New');

    }
 
}