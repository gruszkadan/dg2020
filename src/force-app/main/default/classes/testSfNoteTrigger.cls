@isTest
private class testSfNoteTrigger {
    
    private static testmethod void test1() {
      
        Salesforce_Request__c test = new Salesforce_Request__c();
        test.Latest_Note__c = 'Test';
        insert test;
        
        Salesforce_Request_Note__c note = new Salesforce_Request_Note__c(Salesforce_Update__c = test.ID);

        
        note.Comments__c = 'Test Test Test';
        insert note;
        
        //query new Salesforce Request Info
        test = [SELECT Id, Latest_Note__c FROM Salesforce_Request__c WHERE ID = :test.id];
        
        
        System.assertEquals(test.Latest_Note__c, 'Test Test Test');
        
    }
    
}