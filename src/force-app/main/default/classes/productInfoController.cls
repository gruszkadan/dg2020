public class productInfoController {
    /* test class:
     * testAccountController
     */
    public account a {get;set;}
    public string aId {get;set;}
    public product[] wraps {get;set;}
    public msdsCustomSettings helper = new msdsCustomSettings();
    public boolean viewAP {get;set;}
    
    // boolean values for table headers
    public boolean msds_l {get;set;}
    public boolean msds_s {get;set;}
    public boolean msds_a {get;set;}
    public boolean kmi_l {get;set;}
    public boolean kmi_s {get;set;}
    public boolean kmi_a {get;set;}
    public boolean ergo_l {get;set;}
    public boolean ergo_s {get;set;}
    
    public productInfoController() {
        aId = ApexPages.currentPage().getParameters().get('id');
        viewAP = helper.viewActivateProducts();
        wraps = new List<product>();
        if (aId != null) {
            a = [SELECT Id, Name, Customer_Status__c, GUID__c, Active_Additional_Compliance_Solutions__c, Active_Compliance_Services_Products__c, Active_MSDS_Management_Products__c, Cancelled_Additional_Compliance_Solution__c, 
                 Cancelled_Compliance_Services_Products__c, Cancelled_MSDS_Management_Products__c, Done_Additional_Compliance_Solutions__c, Done_Compliance_Services_Products__c, Done_MSDS_Management_Products__c, 
                 EHS_Active_Licensing__c, Chemical_Management_Version__c, EHS_Active_Services__c, EHS_Active_Add_On_Services__c, EHS_Cancelled_Add_On_Services__c, EHS_Cancelled_Licensing__c, EHS_Cancelled_Services__c, EHS_Done_Add_On_Services__c, 
                 EHS_Done_Licensing__c, EHS_Done_Services__c, EHS_Subscription_Model__c, Ergo_Active_Licensing__c, Ergo_Inactive_Licensing__c, Ergo_Cancelled_Licensing__c, Ergo_Active_Services__c, Ergo_Inactive_Services__c, Ergo_Cancelled_Services__c
                 FROM Account 
                 WHERE Id = :aId LIMIT 1];
        }
        //MSDS PRODUCTS
        if (0 == 0) {
            // MSDS Licensing
            if( a.Active_MSDS_Management_Products__c != null )
            {
                msds_l();
                string[] a_msdsL = a.Active_MSDS_Management_Products__c.split( ';', 0 );
                for( string al : a_msdsL )
                {
                    product prod = new product( al, 'active', 'msdsL' );
                    wraps.add( prod );
                }
            }
            
            if( a.Cancelled_MSDS_Management_Products__c != null )
            {
                msds_l();
                string[] c_msdsL = a.Cancelled_MSDS_Management_Products__c.split( ';', 0 );
                for( string cl : c_msdsL )
                {
                    product prod = new product( cl, 'cancelled', 'msdsL' );
                    wraps.add( prod );
                }
            }
            
            if( a.Done_MSDS_Management_Products__c != null )
            {
                msds_l();
                string[] d_msdsL = a.Done_MSDS_Management_Products__c.split( ';', 0 );
                for( string dl : d_msdsL )
                {
                    product prod = new product( dl, 'done', 'msdsL' );
                    wraps.add( prod );
                }
            }
            
            // MSDS Services
            if( a.Active_Compliance_Services_Products__c != null )
            {
                msds_s();
                string[] a_msdsC = a.Active_Compliance_Services_Products__c.split( ';', 0 );
                for( string ac : a_msdsC )
                {
                    product prod = new product( ac, 'active', 'msdsC' );
                    wraps.add( prod );
                }
            }
            
            if( a.Cancelled_Compliance_Services_Products__c != null )
            {
                msds_s();
                string[] c_msdsC = a.Cancelled_Compliance_Services_Products__c.split( ';', 0 );
                for( string cc : c_msdsC )
                {
                    product prod = new product( cc, 'cancelled', 'msdsC' );
                    wraps.add( prod );
                }
            }
            
            if( a.Done_Compliance_Services_Products__c != null )
            {
                msds_s();
                string[] d_msdsC = a.Done_Compliance_Services_Products__c.split( ';', 0 );
                for( string dc : d_msdsC )
                {
                    product prod = new product( dc, 'done', 'msdsC' );
                    wraps.add( prod );
                }
            }
            
            // MSDS Additional Compliance Services
            if( a.Active_Additional_Compliance_Solutions__c != null )
            {
                msds_a();
                string[] a_msdsA = a.Active_Additional_Compliance_Solutions__c.split( ';', 0 );
                for( string aa : a_msdsA )
                {
                    product prod = new product( aa, 'active', 'msdsA' );
                    wraps.add( prod );
                }
            }
            
            if( a.Cancelled_Additional_Compliance_Solution__c != null )
            {
                msds_a();
                string[] c_msdsA = a.Cancelled_Additional_Compliance_Solution__c.split( ';', 0 );
                for( string ca : c_msdsA )
                {
                    product prod = new product( ca, 'cancelled', 'msdsA' );
                    wraps.add( prod );
                }
            }
            
            if( a.Done_Additional_Compliance_Solutions__c != null )
            {
                msds_a();
                string[] d_msdsA = a.Done_Additional_Compliance_Solutions__c.split( ';', 0 );
                for( string da : d_msdsA )
                {
                    product prod = new product( da, 'done', 'msdsA' );
                    wraps.add( prod );
                }
            }
        }
        
        //EHS PRODUCTS
        if (0 == 0) {
            // EHS Licensing
            if( a.EHS_Active_Licensing__c != null )
            {
                kmi_l();
                string[] a_kmiL = a.EHS_Active_Licensing__c.split( ';', 0 );
                for( string al : a_kmiL )
                {
                    product prod = new product( al, 'active', 'kmiL' );
                    wraps.add( prod );
                }
            } 
            
            if( a.EHS_Cancelled_Licensing__c != null )
            {
                kmi_l();
                string[] c_kmiL = a.EHS_Cancelled_Licensing__c.split( ';', 0 );
                for( string cl : c_kmiL )
                {
                    product prod = new product( cl, 'cancelled', 'kmiL' );
                    wraps.add( prod );
                }
            } 
            
            if( a.EHS_Done_Licensing__c != null )
            {
                kmi_l();
                string[] d_kmiL = a.EHS_Done_Licensing__c.split( ';', 0 );
                for( string dl : d_kmiL )
                {
                    product prod = new product( dl, 'done', 'kmiL' );
                    wraps.add( prod );
                }
            } 
            
            // EHS Services
            if( a.EHS_Active_Services__c != null )
            {
                kmi_s();
                string[] a_kmiS = a.EHS_Active_Services__c.split( ';', 0 );
                for( string akS : a_kmiS )
                {
                    product prod = new product( akS, 'active', 'kmiS' );
                    wraps.add( prod );
                }
            } 
            
            if( a.EHS_Cancelled_Services__c != null )
            {
                kmi_s();
                string[] c_kmiS = a.EHS_Cancelled_Services__c.split( ';', 0 );
                for( string cs : c_kmiS )
                {
                    product prod = new product( cs, 'cancelled', 'kmiS' );
                    wraps.add( prod );
                }
            } 
            
            if( a.EHS_Done_Services__c != null )
            {
                kmi_s();
                string[] d_kmiS = a.EHS_Done_Services__c.split( ';', 0 );
                for( string ds : d_kmiS )
                {
                    product prod = new product( ds, 'done', 'kmiS' );
                    wraps.add( prod );
                }
            } 
            
            // EHS Add On Servoces
            if( a.EHS_Active_Add_On_Services__c != null )
            {
                kmi_a();
                string[] a_kmiA = a.EHS_Active_Add_On_Services__c.split( ';', 0 );
                for( string aa : a_kmiA )
                {
                    product prod = new product( aa, 'active', 'kmiA' );
                    wraps.add( prod );
                }
            } 
            
            if( a.EHS_Cancelled_Add_On_Services__c != null )
            {
                kmi_a();
                string[] c_kmiA = a.EHS_Cancelled_Add_On_Services__c.split( ';', 0 );
                for( string ca : c_kmiA )
                {
                    product prod = new product( ca, 'cancelled', 'kmiA' );
                    wraps.add( prod );
                }
            } 
            
            if( a.EHS_Done_Add_On_Services__c != null )
            {
                kmi_a();
                string[] d_kmiA = a.EHS_Done_Add_On_Services__c.split( ';', 0 );
                for( string da : d_kmiA )
                {
                    product prod = new product( da, 'done', 'kmiA' );
                    wraps.add( prod );
                }
            } 
        }
        
        //ERGO PRODUCTS
        if (0 == 0) {
            // Ergo Licensing
            if (a.Ergo_Active_Licensing__c != null) {
                ergo_l();
                string[] a_ergol = a.Ergo_Active_Licensing__c.split(';', 0);
                for (string al : a_ergol) {
                    product prod = new product(al, 'active', 'ergoL');
                    wraps.add( prod );
                }
            } 
            if (a.Ergo_Inactive_Licensing__c != null) {
                ergo_l();
                string[] i_ergol = a.Ergo_Inactive_Licensing__c.split(';', 0);
                for (string il : i_ergol) {
                    product prod = new product(il, 'done', 'ergoL');
                    wraps.add( prod );
                }
            } 
            if (a.Ergo_Cancelled_Licensing__c != null) {
                ergo_l();
                string[] c_ergol = a.Ergo_Cancelled_Licensing__c.split(';', 0);
                for (string cl : c_ergol) {
                    product prod = new product(cl, 'cancelled', 'ergoL');
                    wraps.add( prod );
                }
            } 
            // Ergo Services
            if (a.Ergo_Active_Services__c != null) {
                ergo_s();
                string[] a_ergos = a.Ergo_Active_Services__c.split(';', 0);
                for (string aes : a_ergos) {
                    product prod = new product(aes, 'active', 'ergoS');
                    wraps.add( prod );
                }
            } 
            if (a.Ergo_Inactive_Services__c != null) {
                ergo_s();
                string[] i_ergos = a.Ergo_Inactive_Services__c.split(';', 0);
                for (string is : i_ergos) {
                    product prod = new product(is, 'done', 'ergoS');
                    wraps.add( prod );
                }
            } 
            if (a.Ergo_Cancelled_Services__c != null) {
                ergo_s();
                string[] c_ergos = a.Ergo_Cancelled_Services__c.split(';', 0);
                for (string cs : c_ergos) {
                    product prod = new product(cs, 'cancelled', 'ergoS');
                    wraps.add( prod );
                }
            } 
        }
    }
    
    public class product
    {
        public string prodName {get;set;}
        public string prodStatus {get;set;}
        public string prodCat {get;set;}
        
        public product( string pN, string pS, string pC )
        {
            prodName = pN;
            prodStatus = pS;
            prodCat = pC;
        }
    }
    
    public PageReference activateProducts()
    {
        PageReference pr = new PageReference('/apex/account_activate_products?id='+aId);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference saveAP()
    {
        if(a.EHS_Active_Licensing__c != null || a.EHS_Active_Services__c != null || a.EHS_Active_Add_On_Services__c != null)
        {
            if(a.Customer_Status__c != 'Active')
            {
                a.Customer_Status__c = 'Active';
            }
        }
        update a;
        PageReference pr = new PageReference('/apex/account_detail?id='+aId);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference cancelAP()
    {
        PageReference pr = new PageReference('/apex/account_detail?id='+aId);
        pr.setRedirect(true);
        return pr;
    }
    public void msds_l()
    {
        msds_l = true;
    }
    public void msds_s()
    {
        msds_s = true;
    }
    public void msds_a()
    {
        msds_a = true;
    }
    public void kmi_l()
    {
        kmi_l = true;
    }
    public void kmi_s()
    {
        kmi_s = true;
    }
    public void kmi_a()
    {
        kmi_a = true;
    }
    public void ergo_l() {
        ergo_l = true;
    }
    public void ergo_s() {
        ergo_s = true;
    }
    
}