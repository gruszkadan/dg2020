@isTest()
private class testOrderPendingAdjustments {
    
    private static testmethod void test_sendAlert() {
        User u = [SELECT id,Name 
                  FROM User 
                  WHERE isActive = true 
                  AND Group__c = 'Mid-Market' 
                  AND Role__c = 'Sales Executive' 
                  LIMIT 1];
        
        //Create an Order Item
        testDataUtility util = new testDataUtility();
        Order_Item__c oi = util.createOrderItem(u.Name, 'test123');
        oi.Product_Platform__c = 'MSDSonline';
        insert oi;
        
        //Update the order item's invoice amount to create a pending adjustment
        oi.Invoice_Amount__c = 100;
        update oi;
        
        //Run the controller
        orderPendingAdjustmentsController con = new orderPendingAdjustmentsController();
        
        //Assert that the controller is pulling back the one adjustment that was created
        system.assertEquals(con.orderWrappers.size(), 1);
        
        SelectOption[] types = con.getTypes();
        
        ApexPages.currentPage().getParameters().put('selOrder', 'test123');
        con.selectOrder();
        
        //Assert that the controller set the selected order
        system.assertEquals(con.selectedOrder.items[0].id , oi.id);
        
        //Find the matching SPR and assert that the status is 'New'
        Sales_Performance_Request__c spr = con.requestMap.get(oi.id);
        system.assertEquals(spr.status__c, 'New');
        
        con.sendAlert();
        con.selectedOrder.items[0].Invoice_Adjustment_Reason__c = 'Test Reason';
        con.selectedOrder.adjustmentType = types[1].getValue();
        con.sendAlert();
        
        //Requery the spr and assert that is was submitted for approval
        Sales_Performance_Request__c sprNew = [SELECT id, Type__c, Status__c 
                                               FROM Sales_Performance_Request__c
                                               WHERE id =: spr.id
                                               LIMIT 1];
        system.assertEquals(sprNew.status__c, 'Pending Approval');
        
        con.reloadPage();
        
        sprNew.Status__c = 'Approved';
        update sprNew;
    }    
    
    private static testmethod void test_dismiss() {
        User u = [SELECT id,Name 
                  FROM User 
                  WHERE isActive = true 
                  AND Group__c = 'Mid-Market' 
                  AND Role__c = 'Sales Executive' 
                  LIMIT 1];
        
        User u2 = [SELECT id,Name 
                   FROM User 
                   WHERE isActive = true 
                   AND Group__c = 'Mid-Market' 
                   AND Role__c = 'Territory Sales Executive' 
                   LIMIT 1];
        
        //Create an Order Item
        testDataUtility util = new testDataUtility();
        Order_Item__c oi = util.createOrderItem(u.Name, 'test123');
        oi.Product_Platform__c = 'MSDSonline';
        insert oi;
        
        //Update the order item's invoice amount to create a pending adjustment
        oi.Invoice_Amount__c = 100;
        update oi;
        
        //Run the controller
        orderPendingAdjustmentsController con = new orderPendingAdjustmentsController();
        
        //Assert that the controller is pulling back the one adjustment that was created
        system.assertEquals(con.orderWrappers.size(), 1);
        
        ApexPages.currentPage().getParameters().put('selOrder', 'test123');
        con.dismiss();
    }    
    
}