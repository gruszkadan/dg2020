@isTest
private class testOpportunityTriggers {
    
    static testmethod void test_salesCycle() {
        
        User owner = [SELECT id
                      FROM User
                      WHERE isActive = true
                      AND Department__c = 'Sales'
                      LIMIT 1];
        
        Account acct = new Account();
        acct.Name = 'Test account';
        acct.NAICS_Code__c = '12345';
        acct.OwnerId = owner.id;
        insert acct;
        
        Event[] demos = new List<Event>();
        for (integer i=0; i<3; i++) {
            Event demo = new Event();
            demo.Subject = 'HQ - Demo';
            demo.Event_Status__c = 'Completed';
            demo.WhatId = acct.id;
            demo.OwnerId = owner.id;
            demo.DurationInMinutes = 10;
            demo.ActivityDate = date.today();
            demo.ActivityDateTime = dateTime.now();
            demos.add(demo);
        }
        insert demos;
        
        Opportunity[] opps = new List<Opportunity>();
        for (integer i = 0; i < 15; i++) {
            Opportunity opp = new Opportunity();
            opp.Name = 'Test opp';
            opp.AccountId = acct.id;
            opp.StageName = 'New';
            opp.OwnerId = owner.id;
            opp.CloseDate = date.today() + 90;
            if (i < 10) {
                opp.Sales_Cycle_Start__c = null;
            } else {
                opp.Sales_Cycle_Start__c = date.today() + 15;
            }
            opps.add(opp);
        }
        insert opps;
        
        Set<id> oppIds = new Set<id>();
        for (Opportunity opp : opps) {
            oppIds.add(opp.id);
        }
        
        Opportunity[] updOpps = [SELECT id, Sales_Cycle_Start__c
                                 FROM Opportunity
                                 WHERE id IN :oppIds];
        
        integer numUpdated = 0;
        integer numExcluded = 0;
        for (Opportunity opp : updOpps) {
            if (opp.Sales_Cycle_Start__c == date.today()) {
                numUpdated++;
            } else {
                numExcluded++;
            }
        }
        System.assert(numUpdated == 10, '');
        System.assert(numExcluded == 5, '');
        
        opps[0].StageName='Discovery';
        update opps[0];
        opps[0].StageName='Scoping';
        update opps[0];
        delete opps;
        
    }
    
    static testmethod void test_closeOtherOpportunities() {
        
        // create accounts
        Account[] accts = new List<Account>();
        for (integer i=0; i<2; i++) {
            Account a = new Account();
            a.NAICS_Code__c = 'Test NAICS code';
            a.Name = 'Test account '+i;
            accts.add(a);
        }
        insert accts;
        
        // create renewals
        Renewal__c[] rens = new List<Renewal__c>();
        for (Account a : accts) {
            Renewal__c r = new Renewal__c();
            r.Account__c = a.id;
            r.Status__c = 'Active';
            rens.add(r);
        }
        insert rens;
        
        // create opportunities that will be closed/won
        Opportunity[] cwOpps = new List<Opportunity>();
        for (Renewal__c r : rens) {                
            for (integer i=0; i<2; i++) {            
                Opportunity o = new Opportunity();
                o.Name = 'Test Closed/Won Opp';
                o.AccountId = r.Account__c;
                o.Renewal__c = r.id;
                o.StageName = 'New';  
                o.CloseDate = date.today() + 90;
                cwOpps.add(o);
            }
        }
        insert cwOpps;
        
        // create other opportunities
        Opportunity[] otherOpps = new List<Opportunity>();
        for (Renewal__c r : rens) {
            for (integer i=0; i<5; i++) {
                Opportunity o = new Opportunity();
                o.Name = 'Test Other Opp';
                o.AccountId = r.Account__c;
                o.Renewal__c = r.id;
                o.StageName = 'New';
                o.CloseDate = date.today() + 90;
                otherOpps.add(o);
            }
        }
        insert otherOpps;
        
        // update the opportunities and activate trigger
        for (Opportunity o : cwOpps) {
            o.StageName = 'Closed/Won';
        }
        update cwOpps;
        
        // create a set of ids to re-query
        Set<id> oppIds = new Set<id>();
        for (Opportunity o : cwOpps) {
            oppIds.add(o.id);
        }
        for (Opportunity o : otherOpps) {
            oppIds.add(o.id);
        }
        
        // re-query all the test opportunities
        Opportunity[] allOpps = [SELECT id, Name, AccountId, Renewal__c, StageName FROM Opportunity WHERE id IN :oppIds];
        integer numClosedWon = 0;    // count the number of closed/won opportunities
        integer numClosedLost = 0;    // count the number of other closed lost opportunities
        for (Opportunity o : allOpps) {
            if (o.StageName == 'Closed/Won') {
                numClosedWon++;
            }
            if (o.StageName == 'Closed Lost') {
                numClosedLost++;
            }
        }
        
        // system asserts
        System.assert(numClosedWon == 4, 'Number of closed/won opportunities: Expected = 4, Actual = '+numClosedWon);
        System.assert(numClosedLost == 10, 'Number of other opportunities closed lost: Expected = 10, Actual = '+numClosedLost);
    }
    
    static testmethod void test_closeLostCancelled() {
        
        // create 2 test accounts
        Account acct1 = new Account();
        acct1.Name = 'test acct 1';
        acct1.NAICS_Code__c = '12345';
        insert acct1;
        
        Account acct2 = new Account();
        acct2.Name = 'test acct 2';
        acct2.NAICS_Code__c = '12345';
        insert acct2;

        
        // create 2 renewals
        Renewal__c ren1 = new Renewal__c();
        ren1.Account__c = acct1.id;
        insert ren1;
        
        Renewal__c ren2 = new Renewal__c();
        ren2.Account__c = acct2.id;
        insert ren2;
        
        // create opportunities for trigger
        Opportunity[] opps = new List<Opportunity>();
        for (integer i = 0; i < 20; i++) {
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Closed/Won Opp';
            if (i < 10) {
                opp.AccountId = acct1.id;
                opp.Renewal__c = ren1.id;
            } else {
                opp.AccountId = acct2.id;
                opp.Renewal__c = ren2.id;
            }
            opp.StageName = 'New';  
            opp.CloseDate = date.today() + 90;
            opps.add(opp);
        }
        insert opps;
        
        // create other opportunities to be updated by trigger
        //     create 50 for each account with a renewal, and 50 without
        Opportunity[] otherOpps = new List<Opportunity>();
        for (integer i = 0; i < 20; i++) {
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Closed/Won Opp';
            if (i < 10) {
                opp.AccountId = acct1.id;
                if (i < 5) {
                    opp.Renewal__c = ren1.id;
                } else {
                    opp.Renewal__c = null;
                }
            } else {
                opp.AccountId = acct2.id;
                if (i < 15) {
                    opp.Renewal__c = ren2.id;
                } else {
                    opp.Renewal__c = null;
                }
            }
            opp.StageName = 'New';  
            opp.CloseDate = date.today() + 90;
            otherOpps.add(opp);
        }
        insert otherOpps;
        
        // add other opportunities to a set to requery
        Set<id> otherOppIds = new Set<id>();
        for (Opportunity opp : otherOpps) {
            otherOppIds.add(opp.id);
        }
        
        // update opportunities to closed lost
        for (Opportunity opp : opps) {
            opp.StageName = 'Closed Lost';
            opp.Closed_Reasons__c = 'Cancel';
            opp.CloseDate = date.today();
        }
        update opps;
        
        // requery other opportunities and count to make sure the right ones got updated
        Opportunity[] updOtherOpps = [SELECT id, AccountId, Renewal__c, StageName, Closed_Reasons__c, CloseDate
                                      FROM Opportunity
                                      WHERE id IN :otherOppIds];
        integer numAcct1Updated = 0;
        integer numAcct2Updated = 0;
        for (Opportunity opp : updOtherOpps) {
            // count num of acct1 opps updated
            if (opp.AccountId == acct1.id) {
                if (opp.StageName == 'Closed Lost' && opp.Closed_Reasons__c == 'Cancel' && opp.CloseDate == date.today()) {
                    numAcct1Updated++;
                } 
            }
            if (opp.AccountId == acct2.id) {
                if (opp.StageName == 'Closed Lost' && opp.Closed_Reasons__c == 'Cancel' && opp.CloseDate == date.today()) {
                    numAcct2Updated++;
                } 
            }
        }
        
        // system asserts
        System.assert(numAcct1Updated == 5, '');
        System.assert(numAcct2Updated == 5, '');
        
    }
    
    static testmethod void test_addProducts() {
        // create account
        Account a = testAccount();
        a.Active_Compliance_Services_Products__c = 'MSDS Authoring;Environmental Toolkit;Special Indexing';
        a.Active_Additional_Compliance_Solutions__c = 'MSDS Authoring;Environmental Toolkit;Starter Kit';
        insert a;
        
        // opportunity
        Opportunity o = new Opportunity();
        o.Name = 'TestOpp';
        o.AccountId = a.id;
        o.StageName = 'System Demp';
        o.CloseDate = date.today() + 90;
        
        test.startTest();
        insert o;
        test.stopTest();
        
    }
    
    @isTest(SeeAllData=true) static void testAutoQuoteOppCreation() {
        //Create Account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        //Create Contact
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Primary_Admin__c = true;
        insert newContact;
        
        //Renewal AME
        Account_Management_event__c ame = new Account_Management_event__c();
        ame.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        ame.Account__c = newAccount.id;
        ame.Contact__c = newContact.id;
        ame.Status__c = 'Not Yet Created';
        ame.Batch_Month__c	= 'Mar';   
        ame.Batch_Year__c = '2014';
        insert ame;

        //Create Order Item
        testDataUtility Util = new testDataUtility(); 
        order_item__c newOrderItem =  Util.createOrderItem('House Sale', 'T654321');
        newOrderItem.Product_Platform__c = 'MSDSonline';
        newOrderItem.Renewal_Amount__c = 100;
        newOrderItem.Account__c = newAccount.Id;
        newOrderItem.Account_Management_Event__c = ame.id;
        insert newOrderItem;
        
        ame.Status__c = 'Active';
        update ame;

        //Assert that the opp & quote was auto created
        Quote__c[] ameQuotes = [SELECT id FROM Quote__c WHERE Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameQuotes.size());

        Quote_Product__c[] ameQuoteProds = [SELECT id FROM Quote_Product__c WHERE Quote__r.Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameQuoteProds.size());
        
        Opportunity[] ameOpps = [SELECT id FROM Opportunity WHERE Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameOpps.size());

        OpportunityLineItem[] ameOLIs = [SELECT id FROM OpportunityLineItem WHERE Opportunity.Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameOLIs.size());
    }

    public static Account testAccount() {
        return new Account(Name='Test', NAICS_Code__c='Test');
    }
    
}