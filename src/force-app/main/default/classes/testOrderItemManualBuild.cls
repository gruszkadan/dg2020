@isTest(seeAllData=true)
private class testOrderItemManualBuild {

    static testmethod void test1() {
        
        orderItemManualBuild myController = new orderItemManualBuild();
        //Assert that the orderWrapper is created
        SYSTEM.assert(MyController.orders.size() == 1, 'Expected 1 Found '+MyController.orders.size());
        //Assert that the first order item is created
        SYSTEM.assert(MyController.orders[0].orderItems.size() == 1, 'Expected 1 Found '+MyController.orders[0].orderItems.size());
        
        myController.getAdminToolSalesReps();
        myController.getAdminToolProductNames();
        myController.addOrder();
        //Assert that the order got added
        SYSTEM.assert(MyController.orders.size() == 2, 'Expected 2 Found '+MyController.orders.size());
        
        ApexPages.currentPage().getParameters().put('atoid',MyController.orders[0].adminToolOrderID);
        myController.addOrderItem();
        //Assert that the order item got added
        SYSTEM.assert(MyController.orders[0].orderItems.size() == 2, 'Expected 2 Found '+MyController.orders[0].orderItems.size());
        
        ApexPages.currentPage().getParameters().put('orderId',MyController.orders[0].orderItems[1].Order_ID__c);
        ApexPages.currentPage().getParameters().put('orderItemId',MyController.orders[0].orderItems[1].Order_Item_ID__c);
        myController.removeOrderItem();
        ApexPages.currentPage().getParameters().put('removeOrderId',MyController.orders[1].adminToolOrderID);
        myController.removeOrder();
        //Assert that there is only 1 order left
        SYSTEM.assert(MyController.orders.size() == 1, 'Expected 1 Found '+MyController.orders.size());
        //Assert that there is only one order item on the remaining order
        SYSTEM.assert(MyController.orders[0].orderItems.size() == 1, 'Expected 1 Found '+MyController.orders[0].orderItems.size());
        
        MyController.orders[0].orderItems[0].Contract_Number__c = '1234';
        MyController.orders[0].orderItems[0].AccountID__c = '1234';
        MyController.orders[0].orderItems[0].Invoice_Amount__c = 1234;
        MyController.orders[0].orderItems[0].Renewal_Amount__c = 1234;
        MyController.orders[0].orderItems[0].Admin_Tool_Product_Name__c = 'Air';
        MyController.orders[0].orderItems[0].Admin_Tool_Sales_Rep__c = 'David Staples';
        MyController.orders[0].orderItems[0].Term_Length__c = 1;
        MyController.orders[0].orderItems[0].Contract_Length__c = 1;
        MyController.orders[0].orderItems[0].Order_Date__c = date.today(); 
        MyController.orders[0].orderItems[0].Term_Start_Date__c = date.today(); 
        MyController.orders[0].orderItems[0].Term_End_Date__c = date.today();
        myController.save();
    }
}