global class retractedDemoBatchable implements Database.Batchable<SObject>, Database.Stateful{
   
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT id,ActivityDate,Event_Status__c FROM Event WHERE Event_Status__c = 'Scheduled' and ActivityDate <=:date.today().addDays(-30)]);
    }
    
    global void execute(Database.BatchableContext bc, list<SObject> batch) {
        Event[] eventsToUpdate = new List<Event>();
        for (Event e : (list<Event>) batch) {
            e.Event_Status__c = 'Retracted';
            eventsToUpdate.add(e);
        }
        update eventsToUpdate;
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    
}