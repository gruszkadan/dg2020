@isTest
private class testRemoveOpportunityCreditMappings {
    
    public static string CRON_EXP = '0 0 0 15 3 ? 2052';
    
    static testmethod void test1() {
        testDataUtility util = new testDataUtility();
        Sales_Incentive_Setting__c oppSIS = util.createOpportunityIncentiveSettings(UserInfo.getUserId());
        oppSIS.Incentive_Stop_Date__c = date.today() - 90;
        insert oppSIS;
        
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new removeOpportunityCreditMappingsScheduled());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2052-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
}