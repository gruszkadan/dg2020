/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testContactOwnerReassignment {

    static testMethod void myUnitTest() {
     Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID='00580000003UChI';
        newAccount.Online_Training_Account_Owner__c='00580000003UChI';
        newAccount.Authoring_Account_Owner__c='00580000003UChI';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.OwnerID = '00580000003UChI';
        insert newContact;
        
        newAccount.OwnerID = '005800000067J7dAAE';
        update newAccount;
        
        Contact contact1 = [select id, OwnerID from Contact where Id = :newContact.Id]; 
        System.assertEquals('005800000067J7dAAE',contact1.OwnerId);
    }
}