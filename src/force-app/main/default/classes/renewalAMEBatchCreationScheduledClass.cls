public class renewalAMEBatchCreationScheduledClass implements Schedulable {

    public void execute(SchedulableContext SC) {
        date contractTermStartDate = Date.Today().toStartOfMonth().addMonths(4).addDays(-1);           
        date contractTermEndDate = Date.Today().toStartOfMonth().addMonths(5).addDays(-2); 
        ameUtility.renewalAMEBatchCreation(contractTermStartDate, contractTermEndDate);
    }   
    
}