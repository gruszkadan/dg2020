global class autoKillOpportunitiesBatchable implements Database.Batchable<SObject>, Database.Stateful{

    global Database.QueryLocator start(Database.BatchableContext bc) {         
        if(Test.isRunningTest()){    
            return Database.getQueryLocator([SELECT id, stageName,  
                                             (SELECT id FROM Tasks WHERE isClosed = FALSE AND Department__c = 'Sales'),
                                             (SELECT id FROM Events WHERE  Event_Status__c != 'Completed' AND Department__c = 'Sales')
                                             FROM Opportunity
                                             WHERE Auto_Kill__c = TRUE   
                                             AND Opportunity_Type__c = 'New' 
                                            ]);
        }else{
            return Database.getQueryLocator([SELECT id, stageName,  
                                             (SELECT id FROM Tasks WHERE isClosed = FALSE AND Department__c = 'Sales'),
                                             (SELECT id FROM Events WHERE  Event_Status__c != 'Completed' AND Department__c = 'Sales')
                                             FROM Opportunity
                                             WHERE Auto_Kill__c = TRUE 
                                             AND Opportunity_Type__c = 'New'  
                                             AND CreatedDate >= 2019-12-16T00:00:00.000+0000
                                            ]);
        }
    }

    global void execute(Database.BatchableContext bc, list<SObject> batch) {
        Opportunity[] oppsToUpdate = new List<Opportunity>();
        Task[] tasksToUpdate = new List<Task>();
        Event[] eventsToUpdate = new List<Event>();
        
        for(Opportunity opp : (list<Opportunity>) batch){
            //Close Opportunity
            opp.StageName = 'Closed';
            opp.CloseDate = System.today();
            opp.Probability = 0;
            opp.Lost_Reasons__c = 'Auto-Kill';
            opp.Competition_Encountered_AUTH__c = 'None';
            opp.Competition_Encountered_CHEM__c = 'None';
            opp.Competition_Encountered_EHS__c = 'None';
            opp.Competition_Encountered_ERGO__c = 'None';
            opp.Competition_Encountered_ODT__c = 'None';

            for(Task t: opp.Tasks){                
                t.Status = 'Auto-Closed';
                tasksToUpdate.add(t);   
            }

            for(Event e: opp.Events){
                e.Event_Status__c = 'Auto-Closed';
                eventsToUpdate.add(e);
            }     
        } 

        try{
            update batch;
            if(tasksToUpdate.size()>0){
                update tasksToUpdate;
            }
            if(eventsToUpdate.size() > 0){
                update eventsToUpdate;
            }
        }
        catch(exception e) {
            salesforceLog.createLog('Opportunities', true, 'autoKillOpportunitiesBatchable', 'Auto kill opportunities', string.valueOf(e));
        }
    }

    global void finish(Database.BatchableContext bc) {}
}