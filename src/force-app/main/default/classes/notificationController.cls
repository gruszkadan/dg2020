public class notificationController
{
    private Apexpages.StandardController controller; 
    public id nId {get; set;}
    public Notification__c n {get; set;}
    public Notification_Audience__c[] auds {get; set;}
    public User u {get; set;}
    public Notification_Settings__c selSetting {get; set;}
    public msdsCustomSettings cs = new msdsCustomSettings();
    public User[] userList {get; set;}
    public string selAud {get; set;}
    public boolean popReset {get; set;}
    public boolean isPublished {get; set;}
    public string[] groupVals {get; set;}
    public integer[] pgNumList {get; set;}
    public userWrapper[] uWraps {get; set;}
    public userWrapper[] all_uWraps {get; set;}
    public userWrapper[] seluWraps {get; set;}
    public userWrapper[] searchWraps {get; set;}
    public integer selUserPg {get; set;}
    public string searchString {get; set;}
    public User[] spUsrList {get; set;}    
    public boolean clearAllUsers {get; set;}
    public string selUserDept {get; set;}
    integer imgNum = 0;
    
    public notificationController(ApexPages.StandardController stdController)
    {
        this.controller = stdController;
        uWraps = new List<userWrapper>();
        all_uWraps = new List<userWrapper>();
        seluWraps = new List<userWrapper>();
        searchWraps = new List<userWrapper>();
        spUsrList = new List<User>();
        u = [SELECT id, Name, FirstName, Email, LastName, ManagerID, Department__c, Group__c, Role__c, ProfileID 
             FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
        nId = ApexPages.currentPage().getParameters().get('id');
        if (nId != null)
        {
            n = [SELECT id, Name, Audience__c, Description__c, Due_Date__c, Number_Outstanding__c, Publish_Date__c, Status__c, Title__c, Owner.Name, Owner.id, Selected_Users__c
                 FROM Notification__c WHERE id = :nId LIMIT 1];
            if (n.Status__c == 'Published')
            {
                isPublished = true;
            }
            if (n.Audience__c != null)
            {
                if (n.Status__c == 'Draft')
                {
                    if (n.Audience__c == 'Specific User(s)')
                    {
                        selAud = 'User';
                        if (n.Selected_Users__c != null)
                        {
                            for (User spUsr : [SELECT id, Name, Role__c, Department__c FROM User WHERE Name IN: n.Selected_Users__c.split(',', 0) ORDER BY Name ASC])
                            {
                                spUsrList.add(spUsr);
                            }
                        }
                    }
                    else
                    {
                        Notification_Settings__c savedSetting = [SELECT id, Name, Query__c FROM Notification_Settings__c WHERE Name = :n.Audience__c LIMIT 1];
                        selAud = savedSetting.id;
                        for (User nsU : Database.query(savedSetting.Query__c.replace('*U.ID*', '\''+u.id+'\'')))
                        {
                            spUsrList.add(nsU);
                        }
                        
                    }
                }
                else
                {
                    auds = [SELECT id, Name, Completed_Date__c, Notification__c, Already_Shown__c, Notification_Read__c, Status__c, User__c, User__r.Name, User__r.Department__c, User__r.Role__c
                            FROM Notification_Audience__c WHERE Notification__c = :nId ORDER BY User__r.Name ASC];
                }
            }
        }
        else
        {
            n = new Notification__c();
        }
        groupVals = cs.notificationGroups().split(';', 0);
        selUserPg = 1;
        popReset = false;
        clearAllUsers = false;
        selUserDept = 'All';
        
    }
    
    public SelectOption[] getGroupList()
    {
        SelectOption[] ops = new List<SelectOption>();      
        ops.add(new SelectOption('', '- Audience -')); 
        ops.add(new SelectOption('User', 'Specific User(s)'));
        if (groupVals != null)
        {
            Notification_Settings__c[] nss = [SELECT id, Name, Display_Group__c FROM Notification_Settings__c ORDER BY Name ASC];
            for (Notification_Settings__c ns : nss)
            {
                for (string gVal : groupVals)
                {
                    if (ns.Display_Group__c == gVal)
                    {
                        ops.add(new SelectOption(ns.id, ns.Name));  
                    }
                }
            }
        }
        return ops;
    }
    
    public SelectOption[] getDeptVals()
    {
        SelectOption[] ops = new List<SelectOption>();      
        ops.add(new SelectOption('All', '- All -')); 
        Set<string> deptSet = new Set<string>();
        for (User depts : [SELECT id, Department__c FROM User WHERE isActive = true ORDER BY Department__c ASC])
        {
            deptSet.add(depts.Department__c);
        }
        for (string dept : deptSet)
        {
            ops.add(new SelectOption(dept, dept));  
        }
        return ops;
    }
    
    public User[] getGroupUsers()
    {
        if (selAud != null && selAud != 'User')
        {
            selSetting = [SELECT id, Name, Query__c FROM Notification_Settings__c WHERE id = :selAud LIMIT 1];
            userList = Database.query(selSetting.Query__c.replace('*U.ID*', '\''+u.id+'\'')); 
            return userList; 
        }
        else
        {
            return null;
        }
    }
    
    public void manualStatusChange()
    {
        if (n.Status__c == 'Scheduled')
        {
            n.Status__c = 'Published';
        }
        else
        {
            n.Status__c = 'Scheduled';
        }
        update n;
    }
    
    public void findUserWrappers()
    {
        if (n.Selected_Users__c == null)
        {
            n.Selected_Users__c = '';
        }
        uWraps.clear();
        all_uWraps.clear();
        seluWraps.clear();
        searchWraps.clear();
        selUserPg = 1;
        searchString = null;
        if (selAud != null && selAud == 'User')
        {
            string userSoql = 'SELECT id, Name, Role__c, FirstName, LastName, Department__c FROM User WHERE isActive = true';
            if (selUserDept != 'All')
            {
                userSoql += ' AND Department__c = \''+selUserDept+'\'';
            }
            userSoql += ' ORDER BY Name ASC';
            User[] usersToWrap = Database.query(userSoql);
            pgNumList = new List<integer>();
            for (integer iPgs = 0; iPgs < integer.valueOf((decimal.valueOf(usersToWrap.size()) / 25).round(System.RoundingMode.CEILING)); iPgs ++)
            {
                pgNumList.add(iPgs);
            }
            for (integer iPg = 0; iPg < pgNumList.size(); iPg ++)
            {
                if (iPg != pgNumList.size() - 1)
                {
                    for (integer iVal = iPg * 25; iVal < iPg * 25 + 25; iVal ++)
                    {
                        boolean alreadyAdded = false;
                        for (string sUsr : n.Selected_Users__c.split(',',0))
                        {
                            if (sUsr == usersToWrap[iVal].Name)
                            {
                                alreadyAdded = true;
                            }
                        }
                        userWrapper uwrp = new userWrapper(usersToWrap[iVal], false, iPg + 1, false, alreadyAdded);
                        uWraps.add(uwrp);
                        all_uWraps.add(uwrp);
                        if (alreadyAdded == true)
                        {
                            seluWraps.add(uwrp);
                        }
                        
                    }
                }
                else
                {
                    for (integer iVal = iPg * 25; iVal < usersToWrap.size(); iVal ++)
                    {
                        boolean alreadyAdded = false;
                        for (string sUsr : n.Selected_Users__c.split(',',0))
                        {
                            if (sUsr == usersToWrap[iVal].Name)
                            {
                                alreadyAdded = true;
                            }
                        }
                        userWrapper uwrp = new userWrapper(usersToWrap[iVal], false, iPg + 1, false, alreadyAdded);
                        uWraps.add(uwrp);
                        all_uWraps.add(uwrp);
                        if (alreadyAdded == true)
                        {
                            seluWraps.add(uwrp);
                        }
                    }
                }
            }
        }
    }
    
    public void resetGroup()
    {
        spUsrList.clear();
        n.Selected_Users__c = '';
    }
    
    public PageReference saveDraft()
    {
        n.Status__c = 'Draft';
        if (selAud != null)
        {
            if (selAud == 'User')
            {
                n.Audience__c = 'Specific User(s)';
            }
            else
            {
                n.Audience__c = [SELECT id, Name FROM Notification_Settings__c WHERE id = :selAud LIMIT 1].Name;
            }
        }
        if (nId == null)
        {
            insert n;
        }
        else
        {
            update n;
        }
        PageReference pr = new PageReference('/apex/notification_detail?id='+n.id);
        return pr.setRedirect(true);
    }
    
    public void addSelUsers()
    {
        seluWraps.clear();
        for (userWrapper uswr : uWraps)
        {
            if (uswr.w_isSelected == true)
            {
                uswr.w_isAdded = true;
                seluWraps.add(uswr);
            }
        }
        for (userWrapper uswr : searchWraps)
        {
            if (uswr.w_isSelected == true && uswr.w_isAdded == false)
            {
                seluWraps.add(uswr);
            }
        }
    }
    
    public void storeSelUsers()
    {
        for (userWrapper uswr : uWraps)
        {
            if (uswr.w_isSelected == true)
            {
                uswr.w_isAdded = true;
                seluWraps.add(uswr);
            }
        }
    }
    
    public void saveSelUsers()
    {
        for (userWrapper uswr : uWraps)
        {
            if (uswr.w_isSelected == true)
            {
                uswr.w_isAdded = true;
                seluWraps.add(uswr);
            }
        }
        for (userWrapper uswr : searchWraps)
        {
            if (uswr.w_isSelected == true && uswr.w_isAdded == false)
            {
                seluWraps.add(uswr);
            }
        }
        Set<User> spUsrSet = new Set<User>();
        n.Selected_Users__c = '';
        for (userWrapper uswr : seluWraps)
        {
            n.Selected_Users__c = n.Selected_Users__c + uswr.w_u.Name + ',';
        }
        for (User spUsr : [SELECT id, Name, Role__c, Department__c FROM User WHERE Name IN :n.Selected_Users__c.split(',', 0) ORDER BY Name ASC])
        {
            spUsrSet.add(spUsr);
        }
        spUsrList.clear();
        for (User spUsr : spUsrSet)
        {
            spUsrList.add(spUsr);
            for (userWrapper uswr : uWraps)
            {
                if (spUsr.id == uswr.w_u.id)
                {
                    uswr.w_isSaved = true;
                }
            }
        }
        if (clearAllUsers == true)
        { 
            n.Selected_Users__c = '';
            spUsrList.clear();
        }
        if (nId != null)
        {
            update n;
        }
        clearAllUsers = false;
    }
    
    public void searchUsers() 
    {
        searchWraps.clear();
        for (userWrapper uswr : all_uWraps)
        {
            if (uswr.w_u.FirstName.startsWithIgnoreCase(searchString) || uswr.w_u.LastName.startsWithIgnoreCase(searchString) || uswr.w_u.Name.startsWithIgnoreCase(searchString))
            {
                searchWraps.add(uswr);
            }
        }
    }
    
    public void clearSelected()
    {
        clearAllUsers = true;
        seluWraps.clear();
        for (userWrapper uswr : uWraps)
        {
            if (uswr.w_isSelected == true)
            {
                uswr.w_isSelected = false;
            }
        }
    }
    
    public PageReference saveSchedule()
    {
        if (n.Publish_Date__c == null || n.Due_Date__c == null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill out the publish and due dates before scheduling'));
            return null;
        }
        if (selAud == null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a target audience before scheduling'));
            return null;
        }
        if (n.Publish_Date__c <= datetime.now().addHours(-1))
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The publish date cannot be in the past'));
            return null;
        }
        if (n.Due_Date__c < date.today())
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The due date cannot be in the past'));
            return null;
        }
        if (n.Due_Date__c < n.Publish_Date__c)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The due date must be after the publish date'));
            return null;
        }
        if (n.Title__c == null || n.Description__c == null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Not all notification fields have been filled out'));
            return null;
        }
        else
        {
            try
            {
                n.Status__c = 'Scheduled';
                Notification_Settings__c nSettings = new Notification_Settings__c();
                if (selAud == null)
                {
                    if (n.Selected_Users__c == null)
                    {
                        nSettings = [SELECT id, Name, Query__c FROM Notification_Settings__c WHERE id = :n.Audience__c LIMIT 1];
                        n.Audience__c = nSettings.Name;
                    }
                }
                else
                {
                    if (selAud == 'User')
                    {
                        n.Audience__c = 'Specific User(s)';
                    }
                    else
                    {
                        nSettings = [SELECT id, Name, Query__c FROM Notification_Settings__c WHERE id = :selAud LIMIT 1];
                        n.Audience__c = nSettings.Name; 
                    }
                }
                if (nId == null)
                {
                    insert n;
                }
                else
                {
                    update n;
                }
                Notification_Audience__c[] newAuds = new List<Notification_Audience__c>();
                if (n.Audience__c == 'Specific User(s)')
                {
                    Set<string> selSet = new Set<string>();
                    for (string ss : n.Selected_Users__c.split(',', 0))
                    {
                        selSet.add(ss);
                    }
                    User[] suList = [SELECT id, Name FROM User WHERE Name IN :selSet];
                    for (string ss : selSet)
                    {
                        for (User su : suList)
                        {
                            if (ss == su.Name)
                            {
                                Notification_Audience__c newAud = new Notification_Audience__c();
                                newAud.Notification__c = n.id;
                                newAud.User__c = su.id;
                                newAuds.add(newAud);
                            }
                        }
                    }
                }
                else
                {
                    for (User uAud : Database.query(nSettings.Query__c.replace('*U.ID*', '\''+u.id+'\'')))
                    {
                        Notification_Audience__c newAud = new Notification_Audience__c();
                        newAud.Notification__c = n.id;
                        newAud.User__c = uAud.id;
                        newAuds.add(newAud);
                    }
                }
                insert newAuds;
                PageReference pr = new PageReference('/apex/notification_detail?id='+n.id);
                return pr.setRedirect(true);
            }
            catch(Exception e)
            {
                ApexPages.addMessages(e);
                return null;
            }
        }
    }
    
    public PageReference save()
    {
        update n;
        PageReference pr = new PageReference('/apex/notification_detail?id=' + n.id);
        return pr.setRedirect(true);
    }
    
    public PageReference cancel()
    {
        if (nId != null)
        {
            PageReference pr = new PageReference('/apex/notification_detail?id=' +n .id);
            return pr.setRedirect(true); 
        }
        else
        {
            PageReference pr = new PageReference('/a28/l');
            return pr.setRedirect(true); 
        }
    }
    
    public PageReference edit()
    {
        if( n.Status__c == 'Published')
        {
            isPublished = true;
        }
        PageReference pr = new PageReference('/apex/notification_edit?id=' + n.id);
        return pr.setRedirect(true);
    }
    
    public void reShow()
    {
        for (Notification_Audience__c reAud : auds)
        {
            reAud.Already_Shown__c = false;
        }
        popReset = true;
        update auds;
    }
    
    public PageReference convertToDraft()
    {
        PageReference pr = new PageReference('/apex/notification_detail?id='+n.id);
        n.Status__c = 'Draft';
        update n;
        if (auds.size() > 0)
        {
            delete auds;
            return pr.setRedirect(true);
        }
        else
        {
            return pr.setRedirect(true);
        }
    }
    
    public class userWrapper
    {
        public User w_u {get; set;}
        public boolean w_isSelected {get; set;}
        public boolean w_isAdded {get; set;}
        public integer w_pgNum {get; set;}
        public boolean w_isSaved {get; set;}
        
        public userWrapper(User w_u2, boolean w_isSelected2, integer w_pgNum2, boolean w_isAdded2, boolean w_isSaved2)
        {
            w_isSaved = w_isSaved2;
            w_u = w_u2;
            w_isSelected = w_isSelected2;
            w_isAdded = w_isAdded2;
            w_pgNum = w_pgNum2;
            if (w_isSaved == true)
            {
                w_isSelected = true;
            }
            
        }
    }
    
    public Document document
    {
        get
        {
            if (document == null)
                document = new Document();
            return document;
        }
        set;
    }
    
    public void upload()
    {
        document.AuthorId = UserInfo.getUserId();
        document.FolderId = [SELECT id FROM Folder WHERE DeveloperName = 'Notification_Images' LIMIT 1].id;
        document.Type = 'jpg';
        document.ContentType = 'image/jpeg';
        document.isPublic = true;
        document.Name = 'NOTIFIMG';
        if (imgNum == 0)
        {
            imgNum = 1;
        }
        else
        {
            imgNum ++;
        }
        document.Description = u.id+';'+imgNum;
        insert document;
        n.Description__c = n.Description__c + '<img src="/servlet/servlet.ImageServer?id='+document.id+'&oid='+UserInfo.getOrganizationId()+'"></img>';
        document.body = null;
        document = new Document();
    }
    
    
    
    
}