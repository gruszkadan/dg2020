public class salesPerformanceOrderTriggerHandler {
    
    // AFTER UPDATE
    public static void afterUpdate(Sales_Performance_Order__c[] oldSPOs, Sales_Performance_Order__c[] newSPOs) {
        Sales_Performance_Order__c[] ownerChanged = new list<Sales_Performance_Order__c>();
        Sales_Performance_Order__c[] sposForOpportunityCredit = new list<Sales_Performance_Order__c>();
        
        for (integer i=0; i<newSPOs.size(); i++) {
            if(newSPOs[i].OwnerID != oldSPOs[i].ownerID && 
               newSPOs[i].Type__c == 'Booking' &&
               newSPOs[i].isSplit__c == false &&
               newSPOs[i].Group__c != 'House Sales' &&
               newSPOs[i].Group__c != 'EHS Sales' &&
               newSPOs[i].Group__c != 'Customer Care Sales'){
                   ownerChanged.add(newSPOs[i]);
               }
            //Check SPOs for opportunity credit creation
            if(newSPOs[i].Type__c == 'Booking' &&
               newSPOs[i].isSplit__c == false &&
               newSPOs[i].Group__c != 'House Sales' &&
               newSPOs[i].Group__c != 'EHS Sales' &&
               newSPOs[i].Group__c != 'Customer Care Sales' &&
               newSPOs[i].Opportunity__c != null &&
               oldSPOs[i].Opportunity__c == null){
                   sposForOpportunityCredit.add(newSPOs[i]);
               }
        }
        
        if(ownerChanged.size()>0){
            salesPerformanceOrderUtility.insertAssociateCreditSPO(ownerChanged, true);
        }
        
        if(sposForOpportunityCredit.size()>0){
            salesPerformanceOrderUtility.insertOpportunityCreditSPO(sposForOpportunityCredit);
        }
        
    }
    
    // AFTER INSERT
    public static void afterInsert(Sales_Performance_Order__c[] oldSPOs, Sales_Performance_Order__c[] newSPOs) {
        Sales_Performance_Order__c[] splitSPOs = new list<Sales_Performance_Order__c>();
        Sales_Performance_Order__c[] sposForOpportunityCredit = new list<Sales_Performance_Order__c>();
        
        for (integer i=0; i<newSPOs.size(); i++) {
            if(newSPOs[i].Type__c == 'Booking' &&
               newSPOs[i].isSplit__c == true &&
               newSPOs[i].isSplitMaster__c == false &&
               newSPOs[i].Group__c != 'House Sales' &&
               newSPOs[i].Group__c != 'EHS Sales' &&
               newSPOs[i].Group__c != 'Customer Care Sales'){
                   splitSPOs.add(newSPOs[i]);
               }
            //Check SPOs for opportunity credit creation
            if(newSPOs[i].Type__c == 'Booking' &&
               newSPOs[i].isSplit__c == false &&
               newSPOs[i].Group__c != 'House Sales' &&
               newSPOs[i].Group__c != 'EHS Sales' &&
               newSPOs[i].Group__c != 'Customer Care Sales' &&
               newSPOs[i].Opportunity__c != null){
                   sposForOpportunityCredit.add(newSPOs[i]);
               }
        }
        
        if(splitSPOs.size()>0){
            salesPerformanceOrderUtility.insertAssociateCreditSPO(splitSPOs, false);
        }
        
        if(sposForOpportunityCredit.size()>0){
            salesPerformanceOrderUtility.insertOpportunityCreditSPO(sposForOpportunityCredit);
        }
        
        
    }
}