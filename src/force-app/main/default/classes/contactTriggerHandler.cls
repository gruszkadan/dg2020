public class contactTriggerHandler {
    
    public static void beforeInsert(Contact[] contacts) {
        Contact[] checkPrimaryAdminContacts = new list<Contact>();
        for (integer i=0; i<contacts.size(); i++) {
            if (contacts[i].CustomerAdministratorId__c != null){
                checkPrimaryAdminContacts.add(contacts[i]);
            }
        }
        
        //new List of Contacts with an email (email domain)
        Contact[] checkEnterpriseContacts = new list<Contact>();
        for (integer i=0; i<contacts.size(); i++) {
            if (contacts[i].Email_Domain__c != null){
                checkEnterpriseContacts.add(contacts[i]);
            }   
        }   
 
        //check primary admin
        if (checkPrimaryAdminContacts.size() > 0) {
            contactUtility.checkPrimaryAdmin(checkPrimaryAdminContacts);
        }
        //when a contact is inserted, make sure to reassign ownership to the account owner
        contactUtility.reassignContact(contacts, false);
        
        //when a contact is created, set the poi stage to 'Sales Accepted' for the appropriate suite
        //poiStageStatusUtility.newContactPOIStage(contacts, false);
        
        //check for communication channel and set lead source values
        //leadSourceUtility.updateLeadSourceValues(contacts, 'insert', 'Contact');
        
        // //Opt-In Sales Consulting Email Preference
        //emailPreferencesUtility.salesConsultingOptIn(contacts, 'Contact');
        
        //check for enterprise email domains
        if (checkEnterpriseContacts.size() > 0) {
            contactUtility.EnterpriseEmailCheck(checkEnterpriseContacts);
        }
        
    }
    
    public static void beforeUpdate(Contact[] oldContacts, Contact[] newContacts) {
        Contact[] statusContacts = new list<Contact>();
        Contact[] poiStatusChanges = new list<Contact>();
        Contact[] sourceChangedContacts = new list<Contact>();
        Contact[] unsubscribedContacts = new list<Contact>();
        Contact[] commChannelContacts = new list<Contact>();
        Contact[] checkPrimaryAdminContacts = new list<Contact>();
        Contact[] uncheckPrimaryAdminContacts = new list<Contact>();
        Contact[] surveySentDateUpdates = new list<Contact>();
        Contact[] primarySolutionPOIUpdates = new list<Contact>();
        Contact[] accountChanges = new list<Contact>();
        Contact[] checkEnterpriseContacts = new list<Contact>();
        
        for (integer i=0; i<newContacts.size(); i++) {
            //If a rep changes a contact's status to OOB we need to update the actions. This will change the actions' statuses to 'Resolved - Not a Target'
            if ((oldContacts[i].Status__c != newContacts[i].Status__c) && (newContacts[i].Status__c == 'Out of Business' || newContacts[i].Status__c == 'No Longer Employed')) {
                statusContacts.add(newContacts[i]);
            }
            // If the POI Status changes, we may need to update the stage
            if (oldContacts[i].POI_Status_MSDS__c != newContacts[i].POI_Status_MSDS__c ||
                oldContacts[i].POI_Status_EHS__c != newContacts[i].POI_Status_EHS__c ||
                oldContacts[i].POI_Status_AUTH__c != newContacts[i].POI_Status_AUTH__c ||
                oldContacts[i].POI_Status_ODT__c != newContacts[i].POI_Status_ODT__c ||
                oldContacts[i].POI_Status_ERGO__c != newContacts[i].POI_Status_ERGO__c) {
                    poiStatusChanges.add(newContacts[i]);
                }
            //check to see if lead source was changed
            if (oldContacts[i].LeadSource != newContacts[i].LeadSource) {
                sourceChangedContacts.add(newContacts[i]);
            }
            //check to see if unsubscribe all was changed to true
            if (oldContacts[i].Unsubscribe_All__c == FALSE && newContacts[i].Unsubscribe_All__c == TRUE){
                unsubscribedContacts.add(newContacts[i]);
            }
            //check to see if has a communication channel
            if (oldContacts[i].Communication_Channel__c == null && newContacts[i].Communication_Channel__c != null){
                commChannelContacts.add(newContacts[i]);
            }
            //check to see if primary admin needs to be checked
            if (newContacts[i].CustomerAdministratorId__c != null && oldContacts[i].CustomerAdministratorId__c == null ){
                checkPrimaryAdminContacts.add(newContacts[i]);
            }
            //check to see if primary admin needs to be unchecked
            if (newContacts[i].CustomerAdministratorId__c == null && oldContacts[i].CustomerAdministratorId__c != null ){
                uncheckPrimaryAdminContacts.add(newContacts[i]);
            }
            //check to see if survey sent date needs to be updated
            if (oldContacts[i].Survey_Email_Sent_Date__c != newContacts[i].Survey_Email_Sent_Date__c && newContacts[i].Survey_Automation_Step__c == 'AS - Survey' && newContacts[i].Survey_Email_Sent_Date__c != null){
                surveySentDateUpdates.add(newContacts[i]);
            }
            if (oldContacts[i].Primary_Solution_Product_of_Interest__c != newContacts[i].Primary_Solution_Product_of_Interest__c) {
                primarySolutionPOIUpdates.add(newContacts[i]);
            }
            //check to see if the account changed
            if (newContacts[i].AccountID != oldContacts[i].AccountID){
                accountChanges.add(newContacts[i]);
            }
            //check to see if the email domain changed
            if (newContacts[i].Email_Domain__c != oldContacts[i].Email_Domain__c ){  
                checkEnterpriseContacts.add(newContacts[i]);
            }
        }
        if(accountChanges.size()>0){
            contactUtility.reassignContact(accountChanges, false);  
        }
        if (statusContacts.size() > 0) {
            poiStageStatusUtility.updateStatus(statusContacts);
        }
        if (poiStatusChanges.size() > 0) {
            poiStageStatusUtility.updateStageFromStatus(poiStatusChanges, 'Contact');
        }
        //find the new lead source values
        if (sourceChangedContacts.size() > 0) {
            leadSourceUtility.updateLeadSourceValues(sourceChangedContacts, 'update', 'Contact');
        } 
        //unsubscribe everything on the Contact
        if (unsubscribedContacts.size() > 0) {
            emailPreferencesUtility.unsubscribeAll(unsubscribedContacts, 'Contact');
        }
        //create campaign members for on the Contact
        if (commChannelContacts.size() > 0) {
            contactUtility.campaignAssignment(commChannelContacts);
        }
        //check primary admin
        if (checkPrimaryAdminContacts.size() > 0) {
            contactUtility.checkPrimaryAdmin(checkPrimaryAdminContacts);
        }
        //check primary admin
        if (uncheckPrimaryAdminContacts.size() > 0) {
            contactUtility.uncheckPrimaryAdmin(uncheckPrimaryAdminContacts);
        }
        if (surveySentDateUpdates.size() > 0) {
            contactUtility.updateSurveySentDate(surveySentDateUpdates);
        }
        if (primarySolutionPOIUpdates.size() > 0) {
            campaignMemberUtility.primarySolutionPOI(primarySolutionPOIUpdates, 'Contact');
        }
        //check for enterprise email domains
        if (checkEnterpriseContacts.size() > 0) {
            contactUtility.EnterpriseEmailCheck(checkEnterpriseContacts);
        }
        poiMergeConversion.personMerge(newContacts, 'Contact');
    }
    
    public static void afterInsert(Contact[] oldContacts, Contact[] newContacts) {
        // After a contact is inserted, check the queues
        Contact[] contactsWithMarketoId = new list<Contact>();
        Contact[] contactsWithCommChannel = new list<Contact>();
        Contact[] billingContacts = new list<Contact>();
        
        for (integer i=0; i<newContacts.size(); i++) {
            if (newContacts[i].Marketo_ID__c != null) {
                contactsWithMarketoId.add(newContacts[i]);
            }
            if (newContacts[i].Communication_Channel__c != null) {
                contactsWithCommChannel.add(newContacts[i]);
            }
            if (newContacts[i].Billing_Contact__c == TRUE && newContacts[i].Account_Customer_Status__c == 'Active') {
                billingContacts.add(newContacts[i]);
            }
        }

        
        if (contactsWithMarketoId.size() > 0) {
            poiUtility.poiMarketoIdUpdate(contactsWithMarketoId, 'Contact');
            poiActionUtility.checkPOIActionQueues(contactsWithMarketoId, 'Contact');
        }
        if (contactsWithCommChannel.size() > 0) {
            contactUtility.campaignAssignment(contactsWithCommChannel);
        }
        if (billingcontacts.size() > 0) {
            contactUtility.billContactEmail(billingcontacts);
        }
        
    }
    
    public static void afterUpdate(Contact[] oldContacts, Contact[] newContacts) {
        //if the contact's marketoId changes, we need to check the queues
        Contact[] contactsWithMarketoId = new list<Contact>();
        Contact[] oldStageContacts = new list<Contact>();
        Contact[] newStageContacts = new list<Contact>();
        Contact[] poiScoreUpdates = new list<Contact>();
        //If the contact is moved to another account
        Contact[] accountChanges = new list<Contact>();
        //If the contact becomes a primary admin
        Contact[] npsUpdates = new list<Contact>();
        //for billing contacts
        Contact[] billingContactUpdates = new list<Contact>();
        //to set contact Upgrade dates on task
        Contact[] contactUpgradeDates = new list<Contact>();
        
        //loop and add to our lists
        for (integer i=0; i<newContacts.size(); i++) {
            
            //check the queues for the new marketoId
            if (newContacts[i].Marketo_ID__c != null && newContacts[i].Marketo_ID__c != oldContacts[i].Marketo_ID__c) {
                contactsWithMarketoId.add(newContacts[i]);
            }
            
            //Update the Stage, Status, UTC or Status Change Date on the POI and POI Actions when they update on the Contact
            // update the appropriate ones with the new stage. This will then fire an update to the POI.
            if (newContacts[i].POI_Stage_MSDS__c != oldContacts[i].POI_Stage_MSDS__c
                || newContacts[i].POI_Stage_AUTH__c != oldContacts[i].POI_Stage_AUTH__c
                || newContacts[i].POI_Stage_EHS__c != oldContacts[i].POI_Stage_EHS__c
                || newContacts[i].POI_Stage_ERGO__c != oldContacts[i].POI_Stage_ERGO__c
                || newContacts[i].POI_Stage_ODT__c != oldContacts[i].POI_Stage_ODT__c
                || newContacts[i].POI_Status_MSDS__c != oldContacts[i].POI_Status_MSDS__c
                || newContacts[i].POI_Status_AUTH__c != oldContacts[i].POI_Status_AUTH__c
                || newContacts[i].POI_Status_EHS__c != oldContacts[i].POI_Status_EHS__c
                || newContacts[i].POI_Status_ERGO__c != oldContacts[i].POI_Status_ERGO__c
                || newContacts[i].POI_Status_ODT__c != oldContacts[i].POI_Status_ODT__c
                || newContacts[i].Num_of_Unable_to_Connect_Tasks_MSDS__c != oldContacts[i].Num_of_Unable_to_Connect_Tasks_MSDS__c
                || newContacts[i].Num_of_Unable_to_Connect_Tasks_AUTH__c != oldContacts[i].Num_of_Unable_to_Connect_Tasks_AUTH__c
                || newContacts[i].Num_of_Unable_to_Connect_Tasks_EHS__c != oldContacts[i].Num_of_Unable_to_Connect_Tasks_EHS__c
                || newContacts[i].Num_of_Unable_to_Connect_Tasks_ERGO__c != oldContacts[i].Num_of_Unable_to_Connect_Tasks_ERGO__c
                || newContacts[i].Num_of_Unable_to_Connect_Tasks_ODT__c != oldContacts[i].Num_of_Unable_to_Connect_Tasks_ODT__c
                || newContacts[i].POI_Status_Change_Date_MSDS__c != oldContacts[i].POI_Status_Change_Date_MSDS__c
                || newContacts[i].POI_Status_Change_Date_AUTH__c != oldContacts[i].POI_Status_Change_Date_AUTH__c
                || newContacts[i].POI_Status_Change_Date_EHS__c != oldContacts[i].POI_Status_Change_Date_EHS__c
                || newContacts[i].POI_Status_Change_Date_ERGO__c != oldContacts[i].POI_Status_Change_Date_ERGO__c
                || newContacts[i].POI_Status_Change_Date_ODT__c != oldContacts[i].POI_Status_Change_Date_ODT__c               
               ){
                   oldStageContacts.add(oldContacts[i]);
                   newStageContacts.add(newContacts[i]);
               }
            // If Marketo updates the contact's score, update any POI's
            if (oldContacts[i].MSDS_Score__c != newContacts[i].MSDS_Score__c ||
                oldContacts[i].EHS_Score__c != newContacts[i].EHS_Score__c ||
                oldContacts[i].AUTH_Score__c != newContacts[i].AUTH_Score__c ||
                oldContacts[i].ODT_Score__c != newContacts[i].ODT_Score__c ||
                oldContacts[i].ERGO_Score__c != newContacts[i].ERGO_Score__c) {
                    poiScoreUpdates.add(newContacts[i]); 
                }
            //If the contact's account changes
            if (oldContacts[i].AccountId != newContacts[i].AccountId) {
                accountChanges.add(newContacts[i]);
            }
            //If the contact becomes a primary admin
            if (oldContacts[i].CustomerAdministratorId__c == null && newContacts[i].CustomerAdministratorId__c != null 
                || (oldContacts[i].Chemical_Management_NPS_Score__c != newContacts[i].Chemical_Management_NPS_Score__c && newContacts[i].CustomerAdministratorId__c != null)
                || (oldContacts[i].Chemical_Management_NPS_Last_Survey_Date__c != newContacts[i].Chemical_Management_NPS_Last_Survey_Date__c && newContacts[i].CustomerAdministratorId__c != null)) {
                    npsUpdates.add(newContacts[i]);
                } 
            
            //idenify when the billContact changes 
            if (newContacts[i].Billing_Contact__c != oldContacts[i].Billing_Contact__c && newContacts[i].Account_Customer_Status__c == 'Active'){  
                billingContactUpdates.add(newContacts[i]);
            }

            if(newContacts[i].Upgrade_Email_1_Sent_Date__c != null && oldContacts[i].Upgrade_Email_1_Sent_Date__c != newContacts[i].Upgrade_Email_1_Sent_Date__c
               || newContacts[i].Upgrade_Email_2_Sent_Date__c != null && oldContacts[i].Upgrade_Email_2_Sent_Date__c != newContacts[i].Upgrade_Email_2_Sent_Date__c 
               || newContacts[i].Upgrade_Email_3_Sent_Date__c != null && oldContacts[i].Upgrade_Email_3_Sent_Date__c != newContacts[i].Upgrade_Email_3_Sent_Date__c){
                   contactUpgradeDates.add(newContacts[i]);
               }
            
        }
        
        if (contactsWithMarketoId.size() > 0) {
            poiUtility.poiMarketoIdUpdate(contactsWithMarketoId, 'Contact');
            poiActionUtility.checkPOIActionQueues(contactsWithMarketoId, 'Contact');
        }
        if (oldStageContacts.size() > 0 && newStageContacts.size() > 0) {
            poiUtility.updateStageStatusUTCStatusChangeDateFromLeadContact(oldStageContacts, newStageContacts);
        }
        if (poiScoreUpdates.size() > 0) {
            poiUtility.poiScoreUpdate(poiScoreUpdates, 'Contact');
        }
        if (accountChanges.size() > 0) {
            poiActionUtility.updatePoiaOwnership(accountChanges, 'Contact');
        }
        //If the contact is now a primary admin, update the NPS info on the account
        if (npsUpdates.size() > 0) {
            contactUtility.accountNPSScoreUpdate(npsUpdates);
        }
        //system.debug('billingContactUpdates '+billingContactUpdates.size());
        if (billingContactUpdates.size() > 0) {
            contactUtility.billContactEmail(billingContactUpdates);
        }
        if(contactUpgradeDates.size() > 0) {
            VPMTaskUtility.updateTaskEndDate(contactUpgradeDates);
        }
    }
    
    public static void afterDelete(Contact[] oldContacts, Contact[] newContacts) {
        sObject[] mergedContacts = new list<sObject>();
        for(integer i=0; i<oldContacts.size(); i++){
            if(oldContacts[i].MasterRecordId != null){
                mergedContacts.add(oldContacts[i]);
            }
        }
        if(mergedContacts.size() > 0){
            poiMergeConversion.personMergeCustomSetting(mergedContacts, 'Contact');
        }
    }
    
}