trigger ETS_makeSurvey on Case (after update) {
    Set<Case> supportSet = new Set<Case>();          
    Set<id> caseSet = new Set<id>();
    Set<id> conSet = new Set<id>();
    Survey_Feedback__c[] newSurvs = new List<Survey_Feedback__c>();
    // query record type ids for surveys
    RecordType[] recTypes = [SELECT id, Name FROM RecordType WHERE Name = 'ETS - Onboarding' OR Name = 'ETS - Services' OR Name = 'ETS - Support' ORDER BY Name ASC];
    
    for (integer i = 0; i < Trigger.new.size(); i ++) {
        if (Trigger.new[i].Status == 'Closed' && Trigger.old[i].Status != 'Closed' && Trigger.new[i].Re_Open_Date__c == null) {
            if (Trigger.new[i].Type == 'IMP: HQ') {
                caseSet.add(Trigger.new[i].id);
                conSet.add(trigger.new[i].Contactid);
            }
            if (Trigger.new[i].Type == 'Compliance Services' && Trigger.new[i].Standalone__c == true) {
                caseSet.add(Trigger.new[i].id);
                conSet.add(trigger.new[i].Contactid);
            }
            if (Trigger.new[i].Type == 'CC Support' && (Trigger.new[i].ClosedDate  < date.newInstance(2018, 6, 19) || (Trigger.new[i].ClosedDate  >= date.newInstance(2018, 6, 19) && Trigger.new[i].Category__c != null && Trigger.new[i].Priority__c != null)) ) {
                supportSet.add(Trigger.new[i]);
                conSet.add(trigger.new[i].Contactid);
            }
        }
        
    }
    
    // if there are any support cases closed, loop through the issues and add to caseSet as long as it isn't a "not an msds support call"
    if (supportSet.size() > 0) {
        Set<id> idSet = new Set<id>();
        Case_Issue__c[] issues = [SELECT id, Product_Support_Issue__c, Associated_Case__c FROM Case_Issue__c WHERE Associated_Case__c IN :supportSet AND Product_Support_Issue__c != 'Not an MSDS support call'];
        for (Case_Issue__c issue : issues) {
            idSet.add(issue.Associated_Case__c);
        }
        for (Case c : [SELECT id, Contactid FROM Case WHERE id IN :idSet]) {
            caseSet.add(c.id);
        }
    }
    if (caseSet.size() > 0) {
        Case[] cases = [SELECT id, Type, No_Survey_Created_Reason__c, Contactid, Account.AdminId__c, Accountid, Contact.Opt_In_Surveys__c, Contact.Last_Transactional_Survey_Sent_Date__c, 
                        Contact.CustomerAdministratorId__c, Account.Channel__c, Account.Active_Products__c, Account.Active_MSDS_Management_Products__c
                        FROM Case WHERE id IN :caseSet];
        Survey_Queue__c[] queues = [SELECT id, Contact__c, Send_Date__c FROM Survey_Queue__c WHERE Contact__c IN :conSet];
        for (Case c : cases) {
            boolean makeSurvey = false;
            string delReason = '';
            string sType = '';
            date[] datesList = new List<date>();
            if (queues.size() > 0) {
                for (Survey_Queue__c queue : queues) {
                    if (queue.Contact__c == c.Contactid) {
                        datesList.add(queue.Send_Date__c);
                    }
                }
            }
            if (c.Type == 'IMP: HQ') {
                if (datesList.size() == 0) {
                    makeSurvey = true;
                    sType = 'ETS - Onboarding';
                } else {
                    makeSurvey = true;
                    sType = 'ETS - Onboarding';
                    for (date d : datesList) {
                        if (d > date.today() + 30 && d < date.today() + 90) {
                            makeSurvey = false;
                            delReason += 'Contact already has a survey queued at this time; ';
                        }
                    }
                }
                if (makeSurvey == true) {
                    if (c.Contact.Opt_In_Surveys__c == false) {
                        makeSurvey = false;
                        delReason += 'Contact is opted-out of surveys; ';
                    }
                    if (c.Account.Channel__c != null) {
                        makeSurvey = false;
                        delReason += 'Account has a channel code selected; ';
                    }
                    if (c.Account.AdminId__c == null) {
                        makeSurvey = false;
                        delReason += 'Contact has no admin user ID; ';
                    }
                    if (c.Account.Active_Products__c == null) {
                        makeSurvey = false;
                        delReason += 'Account has no active products; ';
                    }
                    if (c.Account.Active_Products__c != null) {
                        if (!c.Account.Active_Products__c.contains('HQ') && !c.Account.Active_Products__c.contains('HQ RegXR')) {
                            makeSurvey = false;
                            delReason += 'Account does not have HQ or HQ RegXR as active products; ';
                        }
                    }
                }
            }
            if (c.Type == 'Compliance Services') {
                if (c.Contact.Last_Transactional_Survey_Sent_Date__c == null || c.Contact.Last_Transactional_Survey_Sent_Date__c < date.today() - 16) {
                    if (datesList.size() == 0) {
                        makeSurvey = true;
                        sType = 'ETS - Services';
                    } else {
                        makeSurvey = true;
                        sType = 'ETS - Services';
                        for (date d : datesList) {
                            if (d >= date.today() && d < date.today() + 44) {
                                makeSurvey = false;
                                delReason += 'Contact already has a survey queued at this time; ';
                            }
                        }
                    }
                }
                if (makeSurvey == true) {
                    if (c.Contact.Opt_In_Surveys__c == false) {
                        makeSurvey = false;
                        delReason += 'Contact is opted-out of surveys; ';
                    }
                    if (c.Account.AdminId__c == null) {
                        makeSurvey = false;
                        delReason += 'Contact has no admin user ID; ';
                    }
                    if (c.Account.Channel__c != null) {
                        makeSurvey = false;
                        delReason += 'Account has a channel code selected; ';
                    }
                }
            }
            if (c.Type == 'CC Support') {
                if (c.Contact.Last_Transactional_Survey_Sent_Date__c == null || c.Contact.Last_Transactional_Survey_Sent_Date__c < date.today() - 30) {
                    if (datesList.size() == 0) {
                        makeSurvey = true;
                        sType = 'ETS - Support';
                    } else {
                        makeSurvey = true;
                        sType = 'ETS - Support';
                        for (date d : datesList) {
                            if (d >= date.today() && d < date.today() + 30) {
                                makeSurvey = false;
                                delReason += 'Contact already has a survey queued at this time; ';
                            }
                        }
                    }
                }
                if (makeSurvey == true) {
                    if (c.Contact.Opt_In_Surveys__c == false) {
                        makeSurvey = false;
                        delReason += 'Contact is opted-out of surveys; ';
                    }
                    if (c.Account.AdminId__c == null) {
                        makeSurvey = false;
                        delReason += 'Contact has no admin user ID; ';
                    }
                    if (c.Account.Channel__c != null) {
                        makeSurvey = false;
                        delReason += 'Account has a channel code selected; ';
                    }
                }
            }
            if (makeSurvey == true) {
                Survey_Feedback__c surv = new Survey_Feedback__c();
                if (sType == 'ETS - Onboarding') {
                    string monthNum = '';
                    if (string.valueOf(date.today().month()).length() == 1) {
                        monthNum = '0' + date.today().month();
                    } else {
                        monthNum = string.valueOf(date.today().month());
                    }
                    surv.Name = 'OS' + monthNum + string.valueOf(date.today().year()).right(2);
                    surv.RecordTypeid = recTypes[0].id;
                    surv.Start_Date__c = date.today() + 60;
                }
                if (sType == 'ETS - Services') {
                    string monthNum = '';
                    if (string.valueOf(date.today().month()).length() == 1) {
                        monthNum = '0' + date.today().month();
                    } else {
                        monthNum = string.valueOf(date.today().month());
                    }
                    surv.Name = 'CSS' + monthNum + string.valueOf(date.today().year()).right(2);
                    surv.RecordTypeid = recTypes[1].id;
                    surv.Start_Date__c = date.today() + 3;
                }
                if (sType == 'ETS - Support') {
                    string monthNum = '';
                    if (string.valueOf(date.today().month()).length() == 1) {
                        monthNum = '0' + date.today().month();
                    } else {
                        monthNum = string.valueOf(date.today().month());
                    }
                    surv.Name = 'Support' + monthNum + string.valueOf(date.today().year()).right(2);
                    surv.RecordTypeid = recTypes[2].id;
                    surv.Start_Date__c = date.today();
                }
                surv.Contact__c = c.Contactid;
                surv.Account__c = c.Accountid;
                surv.Case__c = c.id;
                surv.Primary_MSDS_Management_Product__c = c.Account.Active_MSDS_Management_Products__c;
                newSurvs.add(surv);
            }
            if (makeSurvey == false) {
                c.No_Survey_Created_Reason__c = delReason;
                update c;
            }
        }
    } 
    insert newSurvs;
}