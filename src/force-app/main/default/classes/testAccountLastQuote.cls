@isTest(SeeAllData=true)
private class testAccountLastQuote {
    public static testmethod void RegularTests() {        
        Test.startTest();
        
        test1();          //test 1 - create a reservation 
        Test.stopTest();
    }
    
    static testmethod void test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        newAccount.BillingPostalCode = '97031';
        newAccount.OwnerID = '00580000003UChI';
        newAccount.Online_Training_Account_Owner__c = '00580000003UChI';
        newAccount.Authoring_Account_Owner__c = '00580000003UChI';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        newQuote.OwnerId = '00580000003UChI';
        insert newQuote;
        
        AccountLastQuote controller = new AccountLastQuote();
        controller.accountId = newAccount.Id;
        controller.getQuoteLink();
        
    }
}