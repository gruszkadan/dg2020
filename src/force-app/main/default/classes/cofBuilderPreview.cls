public class cofBuilderPreview {

    public Contract__c thisContract {get;set;}
    public Id contractId {get;set;}
    public List<Attachment> pdfAttachments {get;set;}
    
    
    public cofBuilderPreview(){
        
        pdfAttachments = new List<Attachment>();
        contractId = ApexPages.currentPage().getParameters().get('id');
       
        
        thisContract = [SELECT Name, Status__c, (Select id, Name FROM Attachments) FROM Contract__c WHERE id = :contractId];

        for(Attachment a : thisContract.Attachments){
            String attachmentName =  a.Name.split('\\.').get(1);
            if(attachmentName == 'pdf'){
                pdfAttachments.add(a);
            }
        }   
    }
    
    public pageReference createAttachments(){
        
        blob pdfBlob;        
        if (!test.isRunningTest()) {
            pdfBlob = new PageReference('/apex/contractPrint_v2?id='+contractId+'&pdf=true').setRedirect(false).getContentAsPDF();
        } else {
            pdfBlob = Blob.valueOf('Unit Test Attachment Body');
        }

        integer pdfs =  pdfAttachments.size() + 1; 
        
        Attachment a = new attachment();
        a.ParentId = contractId;
        a.Name = thisContract.Name + '-v' + pdfs + '.pdf'; 
        a.body = pdfBlob;
        insert a;
        return new PageReference('/'+ContractId).setRedirect(true);
    }
    
}