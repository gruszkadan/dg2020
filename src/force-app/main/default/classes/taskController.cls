public with sharing class taskController {
    private Apexpages.StandardController controller; 
    private final Task Task;
    public Task t {get; set;}
    public Task tT {get; set;}
    public Task tType {get; set;}
    public Account theAccount {get;set;}
    public Contact [] theContacts {get;set;}
    public ID what {get; set;}
    public ID user {get; set;}
    public ID who {get; set;}
    public String cType {get; set;}
    public String rURL {get; set;}
    public String close {get; set;}
    public String callResult {get; set;}
    public String editPage {get; set;}
    public String subject {get; set;}
    public String accountID {get; set;}
    public User u {get; set;}
    public Contact contactInfo {get; set;}
    public Lead leadInfo {get; set;}
    public String Email {get; set;}
    public String Phone {get; set;}
    public String e {get; set;}
    public String p {get; set;}
    public String activityDate {get; set;}
    public String whatType {get;set;}
    public static String acctPrefix = Account.sObjectType.getDescribe().getKeyPrefix();
    public static String opptyPrefix = Opportunity.sObjectType.getDescribe().getKeyPrefix();
    public static String casePrefix = Case.sObjectType.getDescribe().getKeyPrefix();
    boolean hasError;
    
    public taskController(ApexPages.StandardController stdController) {
        u= [Select UserPreferencesTaskRemindersCheckboxDefault, Name, Group__c, Suite_Of_Interest__c, Department__c From User where id =: UserInfo.getUserId() LIMIT 1];
        this.controller = stdController;
        Id taskID = ApexPages.currentPage().getParameters().get('ID');
        String close = ApexPages.currentPage().getParameters().get('close');
        String retURL = ApexPages.currentPage().getParameters().get('retURL');
        Task tT = (Task)controller.getRecord();
        if(close == '1'){
            tT.Status = 'Completed';
        }
        if(tT.WhatId == NULL && tT.AccountId !=NULL){
            tT.WhatId = tT.AccountId;
        }
        what = tT.whatID;
        user = tT.OwnerId;
        cType =tT.Type__c;
        who = tT.WhoId;
        rURL = retURL;
        accountID = tT.AccountId;
        subject = tT.Subject;
        tType = [select who.id, who.type from Task where ID=:taskID]; 
        if(who !=NULL && tType.who.type == 'Contact'){
            contactInfo = [select ID, Email, Name, Phone from Contact where ID=:tType.WhoId];
            email = contactInfo.Email;
            phone = contactInfo.Phone;	
        }
        if(who !=NULL && tType.who.type == 'Lead'){
            leadInfo = [select ID, Email, Phone, Name from Lead where ID=:tType.WhoId];
            email = leadInfo.Email;
            phone = leadInfo.Phone;	
        }
        if(what != NULL){
            if(String.valueOf(what).startsWith(acctPrefix)){
                whatType = 'Account';
            }
            if(String.valueOf(what).startsWith(opptyPrefix)){
                whatType = 'Opportunity';
            }
            if(String.valueOf(what).startsWith(casePrefix)){
                whatType = 'Case';
            }  
        }
        if(what == NULL & accountID != null){
            whatType = 'Account';            
        }
        
        if(accountID != null){
            theContacts = [select ID from Contact where AccountID =:accountID];
            checkAccount();
        }
    }
    
    public PageReference updateTime() {
        t = (Task)controller.getRecord();
        if(t.ActivityDate != NULL){
            date myDate = t.ActivityDate;
            Time myTime = Time.newInstance(08, 00, 00, 00);
            DateTime D = DateTime.newInstance(myDate,myTime);
            t.ReminderDateTime = D;
            if(u.UserPreferencesTaskRemindersCheckboxDefault == true){
                t.IsReminderSet = true;
            } else {
                t.IsReminderSet = false;
            }
        } else {
            t.ReminderDateTime = NULL;
            t.IsReminderSet = false;
        }
        return null;
    }
    
    public List<SelectOption> getContactSelectList() {
        List<SelectOption> options = new List<SelectOption>();      
        if(tType.who.type != 'Lead' && whatType !='Case'){
            List<Contact> contacts = [SELECT Id, Name FROM Contact where AccountID=:accountID Order By Name ASC limit 500];
            for (Contact contact : contacts) {
                options.add(new SelectOption(contact.ID, contact.Name));
            }
        } 
        if(whatType =='Case'){
            List<Contact> contacts = [SELECT Id, Name FROM Contact where AccountID=:accountId Order By Name ASC limit 500];
            for (Contact contact : contacts) {
                options.add(new SelectOption(contact.ID, contact.Name));
            }
        } 
        if(tType.who.type == 'Lead'){
            List<Lead> leads = [SELECT Id, Name FROM Lead where ID=:Who Order By Name ASC];
            for (Lead lead : leads) {
                options.add(new SelectOption(lead.ID, lead.Name));
            }
        } 
        return options;
    }
    
    public Contact getContact() {
        //returns details of the single report
        System.debug('getContact...WhoID...' + Who);
        
        List<Contact> contacts = [SELECT Name, ID FROM Contact WHERE Id = :Who LIMIT 1];
        if (contacts != null && contacts.size()>0) {
            return contacts[0];
        } else {
            return null;
        }
    }
    
    public void setName() {
        Contact contact = getContact();
        if (contact != null) Who = contact.ID;
        
    }
    
    public void checkAccount() {
        theAccount = [SELECT id, Date_Connected_with_DM__c, Rating, Account_Rating_Reason__c, Ergonomics_Rating__c, Ergonomics_Rating_Reason__c, Online_Training_Rating__c, Online_Training_Rating_Reason__c, 
                      Authoring_Rating__c, Authoring_Rating_Reason__c, EHS_Rating__c, EHS_Rating_Reason__c, Num_of_SDS__c, Num_of_Employees__c, Current_Ergonomics_Solution__c, Ergonomics_Not_a_Target_Reason__c, Ergonomics_Not_Interested_Reason__c,
                      Current_Training_Method__c, Training_Not_a_Target_Reason__c, Training_Not_Interested_Reason__c
                      FROM Account 
                      WHERE id = : accountID LIMIT 1];
    }
    
    public void accountValidation() {
        hasError = false;
        t = (Task)controller.getRecord();
        if (t.Status == 'Completed' && u.Department__c == 'Sales' && t.Call_Result__c == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a call result'));
            hasError = true;
        }
        
        if (accountID != null) {
            try {
                update theAccount;
            } catch (System.DMLexception e) { 
                ApexPages.addMessages(e);
                hasError = true;
            }
        }
    }
    
    public PageReference save() {
        accountValidation();
        if (!hasError) {
            this.controller.save();
            if(rURL != null){
                PageReference pageRef = new PageReference(rURL);
                return pageRef;
            } else {
                PageReference pageRef = new PageReference('/' + t.Id);
                return pageRef;
            }
        } else {
            return null;
        }
    }	
    
    public PageReference saveNewEvent() {
        accountValidation();
        if (!hasError) {
            this.controller.save();
            PageReference newEvent = new PageReference('/00U/e?');
            newEvent.setRedirect(true);
            if(what != NULL){
                newEvent.getParameters().put('what_id',what); 
            } 
            if(who != NULL){
                newEvent.getParameters().put('who_id', who);	
            }
            newEvent.getParameters().put('user',user);      
            return newEvent;  
        } else {
            return null;
        }
    }
    
    public PageReference saveNew() {
        accountValidation();
        if (!hasError) {
            this.controller.save();
            if(what != NULL && whatType !='Case'){
                checkAccount();
            }
            PageReference task_new = Page.task_new;
            task_new.setRedirect(true);
            if(what != NULL){
                task_new.getParameters().put('whatid',what); 
            } 
            if(who != NULL){
                task_new.getParameters().put('whoID', who);	
                task_new.getParameters().put('e', email);
                task_new.getParameters().put('p', phone);	
            }
            task_new.getParameters().put('tsk5',subject);
            task_new.getParameters().put('user',user);
            task_new.getParameters().put('retURL',rURL); 
            if(what != NULL && whatType !='Case' && theAccount.Date_Connected_with_DM__c != NULL){
                task_new.getParameters().put('type','Follow-Up');
            } else {
                task_new.getParameters().put('type','Initial');
            }
            
            return task_new;  
        } else { 
            return null;
        }
    }
}