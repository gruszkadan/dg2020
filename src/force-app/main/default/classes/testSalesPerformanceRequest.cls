@isTest
private class testSalesPerformanceRequest {
    
    private static testmethod void test1() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            string sprID;
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            rep.Group__c = 'Enterprise Sales';
            update rep;
            
            DateTime d = datetime.now();
            String monthName= d.format('MMMMM');
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = string.valueof(date.today().year());
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = string.valueof(date.today().year());
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = string.valueof(date.today().year());
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Month__c = monthName;
            s.Year__c = string.valueof(date.today().year());
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Sales_Director__c = dir.id;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            Sales_Performance_Settings__c[] settings = new list<Sales_Performance_Settings__c>();
            Sales_Performance_Settings__c repSetting = createSetting(rep.id,'Rep');
            settings.add(repSetting);
            Sales_Performance_Settings__c mgrSetting = createSetting(mgr.id,'Manager');
            settings.add(mgrSetting);
            Sales_Performance_Settings__c dirSetting = createSetting(dir.id,'Director');
            settings.add(dirSetting);
            insert settings;
            
            test.startTest();
            System.runAs(rep) {
                Test.setCurrentPageReference(new PageReference('/apex/sales_Performance_missing_add'));
                salesPerformanceMissingAdd spma = new salesPerformanceMissingAdd();
                spma.newSPR.Sales_Rep__c = rep.id;
                spma.newSPR.Incentive_Month__c = 'Current Month';
                spma.newSPR.Incentive_Amount__c = 15;
                spma.addIncentive();            
                salesPerformanceRequestController con = new salesPerformanceRequestController();
                system.assertEquals(1, con.allRequests.size());
                con.previous();
                con.getListSizeOptions();
                test.stopTest();
            }
            
        }
    }
    
    private static testmethod void test2() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            string sprID;
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            rep.Group__c = 'Enterprise Sales';
            update rep;
            
            DateTime d = datetime.now();
            String monthName= d.format('MMMMM');
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = string.valueof(date.today().year());
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = string.valueof(date.today().year());
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = string.valueof(date.today().year());
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Month__c = monthName;
            s.Year__c = string.valueof(date.today().year());
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Sales_Director__c = dir.id;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            Sales_Performance_Settings__c[] settings = new list<Sales_Performance_Settings__c>();
            Sales_Performance_Settings__c repSetting = createSetting(rep.id,'Rep');
            settings.add(repSetting);
            Sales_Performance_Settings__c mgrSetting = createSetting(mgr.id,'Manager');
            settings.add(mgrSetting);
            Sales_Performance_Settings__c dirSetting = createSetting(dir.id,'Director');
            settings.add(dirSetting);
            insert settings;
            
            test.startTest();
            System.runAs(rep) {
                Test.setCurrentPageReference(new PageReference('/apex/sales_Performance_missing_add'));
                salesPerformanceMissingAdd spma = new salesPerformanceMissingAdd();
                spma.newSPR.Sales_Rep__c = rep.id;
                spma.newSPR.Incentive_Month__c = 'Current Month';
                spma.newSPR.Incentive_Amount__c = 15;
                spma.addIncentive();
            }
            System.runAs(mgr) {
                salesPerformanceRequestController con = new salesPerformanceRequestController();
                con.pendingFilter = TRUE;
                system.assertEquals(1, con.pendingApprovalRequests.size());
            }
            
            salesPerformanceRequestController con = new salesPerformanceRequestController();
            con.gethasPrevious();
            con.gethasNext();
            test.stopTest();
            
            
        }
    }
    private static testmethod void test3() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            string sprID;
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
            insert dir;
            
            User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
            insert mgr;
            
            User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
            insert rep;
            
            rep.Group__c = 'Enterprise Sales';
            update rep;
            
            DateTime d = datetime.now();
            String monthName= d.format('MMMMM');
            
            Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
            vpSPS.OwnerId = rep.Sales_VP__c;
            vpSPS.Month__c = monthName;
            vpSPS.Year__c = string.valueof(date.today().year());
            vpSPS.Role__c = 'VP';
            vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
            vpSPS.Sales_Rep__c = rep.Sales_VP__c;
            insert vpSPS;
            
            Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
            dirSPS.OwnerId = rep.Sales_Director__c;
            dirSPS.Month__c = monthName;
            dirSPS.Year__c = string.valueof(date.today().year());
            dirSPS.Manager__c = rep.Sales_VP__c;
            dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
            dirSPS.Role__c = 'Director';
            dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
            dirSPS.Sales_Rep__c = rep.Sales_Director__c;
            insert dirSPS;
            
            Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
            mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Month__c = monthName;
            mgrSPS.Year__c = string.valueof(date.today().year());
            mgrSPS.Manager__c = rep.Sales_Director__c;
            mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
            mgrSPS.Role__c = 'Sales Manager';
            mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
            insert mgrSPS;
            
            Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
            s.OwnerId = rep.id;
            s.Month__c = monthName;
            s.Year__c = string.valueof(date.today().year());
            s.Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
            s.Sales_Director__c = dir.id;
            s.Role__c = rep.Role__c;
            s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
            s.Sales_Rep__c = rep.id; 
            insert s;
            
            Sales_Performance_Settings__c[] settings = new list<Sales_Performance_Settings__c>();
            Sales_Performance_Settings__c repSetting = createSetting(rep.id,'Rep');
            settings.add(repSetting);
            Sales_Performance_Settings__c mgrSetting = createSetting(mgr.id,'Manager');
            settings.add(mgrSetting);
            Sales_Performance_Settings__c dirSetting = createSetting(dir.id,'Director');
            settings.add(dirSetting);
            insert settings;
            
            test.startTest();
            System.runAs(rep) {
                Test.setCurrentPageReference(new PageReference('/apex/sales_Performance_missing_add'));
                salesPerformanceMissingAdd spma = new salesPerformanceMissingAdd();
                spma.newSPR.Sales_Rep__c = rep.id;
                spma.newSPR.Incentive_Month__c = 'Current Month';
                spma.newSPR.Incentive_Amount__c = 15;
                spma.addIncentive();            
                salesPerformanceRequestController con = new salesPerformanceRequestController();
                con.approvedFilter = TRUE;
                con.pendingFilter = FALSE;
                con.queryRequests();
                system.assertEquals(0, con.allRequests.size());
                test.stopTest();
            }
            
        }
    }
    public static Sales_Performance_Settings__c createSetting(id userId, String defaultView){
        Sales_Performance_Settings__c setting = new Sales_Performance_Settings__c();
        setting.SetupOwnerId = userId;
        setting.Default_View__c = defaultView;
        return setting;
    }
    
}