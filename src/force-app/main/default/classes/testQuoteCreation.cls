@isTest
private class testQuoteCreation {
    static testmethod void test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.AccountID = newAccount.Id;
        newContact.LastName = 'Test';
        insert newContact;
        
        Quote__c newQuote = new Quote__c();
        newQuote.Account__c = newAccount.Id;
        newQuote.Contact__c = newContact.Id;
        insert newQuote;
        
        // Lookup an existing item
        Quote_Product__c myItem = new Quote_Product__c();
        myItem.Product__c = '01t80000002NON4';
        myItem.PricebookEntryId__c ='01u80000006eap9';
        myItem.Quantity__c = 1;
        myItem.Quote__c = newQuote.Id;
        insert myItem;
        
        ApexPages.currentPage().getParameters().put('CF00N800000055Rbg_lkid', newAccount.Id);
        ApexPages.currentPage().getParameters().put('CF00N800000055Rbj_lkid', newContact.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Quote__c());
        quoteCreation myController = new quoteCreation(testController);
        myController.quoteIdChosen = newQuote.ID;
        
        myController.createQuote();
        myController.skipArchiving();
        myController.archiveQuote();
        myController.productEntry();
        myController.getContact();
        myController.getAccount();
        myController.getQuote();
    }
}