@isTest()
public class testCaseNoteTrigger {
    
    //covers inserts and updated
    static testMethod void test1() {
        
         
        //test account
        Account newAcct = new Account(Name = 'Test', Customer_Status__c = 'Active');
        insert newAcct;
        
        //test contact
        Contact newCon = new Contact(LastName = 'Test', AccountId = newAcct.id);
        insert newCon;

        //test project - type 9.0 Upgrade Requires a open project
        VPM_Project__c openProject = new VPM_Project__c(Account__c = newAcct.Id,	Project_Status__c = 'Open', Type__c = 'Chemical Management Upgrade' );
        insert openProject;

        //test case
        Case newCase = new Case();
        newCase.OwnerID = [SELECT id FROM User WHERE LastName = 'Gruszka' LIMIT 1].id;  
         newCase.AccountId = newAcct.id;
         newCase.ContactId = newCon.Id;
         newCase.Status = 'Active';
         newCase.Subject = 'Test';
         newCase.Type = '9.0 Upgrade Training';
         insert newCase;
         
        //find milestone created from case
         VPM_Milestone__c milestone = [Select Status__c from VPM_Milestone__c where Case__c = :newCase.Id];
         
        //test case note
        Case_Notes__c  referralCN = new Case_Notes__c(Notes_Type__c = 'Referral', Issue__c = 'Testing VPMNote Creation', Case__c= newCase.Id);
        insert referralCN;
        
        //test case note 
        Case_Notes__c  trainingCN = new Case_Notes__c(Notes_Type__c = 'Webinar - Training', Issue__c = 'Testing VPMNote&Task Creation', Case__c= newCase.Id);
        insert trainingCN;
        
        VPM_Note__c checkingRefferalNote = [Select Name, VPM_Task__c, VPM_Milestone__c, VPM_Project__c from VPM_Note__c where Case_Note__c = :referralCN.Id ];
        System.assert(checkingRefferalNote.VPM_Task__c == NULL);
        System.assert(checkingRefferalNote.VPM_Milestone__c == milestone.Id);

		VPM_Note__c checkingTrainingNote = [Select Name, VPM_Task__c, VPM_Milestone__c, VPM_Project__c from VPM_Note__c where Case_Note__c = :trainingCN.Id ];
        System.assert(checkingTrainingNote.VPM_Task__c != NULL);
        System.assert(checkingTrainingNote.VPM_Milestone__c == milestone.Id);
        
        referralCN.Notes_Type__c = 'Training';
        referralCN.Product__c = 'Webpliance';
        update referralCN;
        VPM_Note__c checkingRefferalNote2 = [Select Name, VPM_Task__c, VPM_Milestone__c, VPM_Project__c from VPM_Note__c where Case_Note__c = :referralCN.Id ];
        System.assert(checkingRefferalNote2.VPM_Task__c != NULL && checkingRefferalNote2.VPM_Task__c != checkingTrainingNote.VPM_Task__c );
        System.assert(checkingRefferalNote2.VPM_Milestone__c == milestone.Id );
        
        
        trainingCN.Issue__c = 'Testing comment changes - Task and note comment fields should update';
        update trainingCN;
        
        VPM_Note__c checkingTrainingNote2 = [Select Comments__c, VPM_Task__c, VPM_Task__r.Comments__c from VPM_Note__c where Case_Note__c = :trainingCN.Id ];
        System.assert(checkingTrainingNote2.Comments__c == 'Testing comment changes - Task and note comment fields should update' );
        System.assert(checkingTrainingNote2.VPM_Task__r.Comments__c == 'Testing comment changes - Task and note comment fields should update' );
        
        trainingCN.Notes_Type__c = 'Intro Call';
        update trainingCN;
        
        VPM_Note__c checkingTrainingNote3 = [Select VPM_Task__c, VPM_Milestone__c from VPM_Note__c where Case_Note__c = :trainingCN.Id ];
        System.assert(checkingTrainingNote3.VPM_Task__c == NULL);
        System.assert(checkingTrainingNote3.VPM_Milestone__c != NULL);
        
        
        delete trainingCN;
        //for vpmnotetrigger
        delete checkingTrainingNote3;
        
        
    }  

}