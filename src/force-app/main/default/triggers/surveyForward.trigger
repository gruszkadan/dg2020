/*
this trigger runs when a contact forwards a survey and when the contact on the survey is changed
*/
trigger surveyForward on Survey_Feedback__c (after update) {
    
    string[] toAddresses = new List<string>(); 
    Set<id> delSurvs = new Set<id>();
    
    
    string urlPrefix = 'test';
    if (UserInfo.getOrganizationId() == '00D300000001HEfEAM') {
        urlPrefix = 'login';
    } 
    
    // activation stuff, delete when ready
    boolean isOn = false;
    string objid = '';
    if (!test.isRunningTest()) {
        On_Off__c onoff = [SELECT id, Name, Activated__c, Object_id__c FROM On_Off__c WHERE Name = 'ETS Surveys' LIMIT 1];
        if (onoff.Activated__c == true) {
            isOn = true;
            if (onoff.Object_id__c != null) {
                objid = onoff.Object_id__c;
            }
        }
    }
    if (test.isRunningTest()) {
        isOn = true;
    }
    
    if (isOn == true) {
        // find the recipients for the forward email survey
        findRecipients();
        for (integer i=0; i<trigger.new.size(); i++) {
            if (trigger.old[i].Was_Forwarded__c == false && trigger.new[i].Was_Forwarded__c == true) {
                Contact oldCon = [SELECT id, FirstName, LastName, Account.Name, Accountid FROM Contact WHERE id = :trigger.new[i].Contact__c LIMIT 1];
                if (oldCon != null) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(toAddresses);
                    mail.setReplyTo('rwerner@ehs.com');
                    mail.setSubject('Survey Forwarded: #'+trigger.new[i].Name);
                    string html = '';
                    html += oldCon.FirstName+' '+oldCon.LastName+' has chosen to forward survey #'+trigger.new[i].Name+'<br/><br/>'+
                        '<b>Survey Account: </b> <a href="https://'+urlPrefix+'.salesforce.com/'+oldCon.Accountid+'">'+oldCon.Account.Name+'</a><br/>'+
                        '<b>Original Contact: </b> <a href="https://'+urlPrefix+'.salesforce.com/'+oldCon.id+'">'+oldCon.FirstName+' '+oldCon.LastName+'</a><br/>'+
                        '<b>New Name Provided: </b>'+trigger.new[i].Forward_Contact_Name__c+'<br/>'+
                        '<b>New Email Provided: </b>'+trigger.new[i].Forward_Contact_Email__c+'<br/>'+
                        '<b>New Phone Provided: </b>'+trigger.new[i].Forward_Contact_Phone__c+'<br/><br/>'+
                        '<b><a href="https://'+urlPrefix+'.salesforce.com/'+trigger.new[i].id+'">LINK TO SURVEY RECORD</a></b>';
                    mail.setHtmlBody(html);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            }
            if (trigger.old[i].Was_Forwarded__c == true) {
                if (trigger.old[i].Contact__c != trigger.new[i].Contact__c) {
                    if (trigger.new[i].Name_Of_Survey__c == 'Annual Satisfaction') {
                        Contact newCon = [SELECT id, FirstName, LastName, Survey_Number__c, Survey_Automation_Step__c, Survey_Forward_URL__c, Survey_URL__c, Opt_In_Surveys__c
                                          FROM Contact WHERE id = :trigger.new[i].Contact__c LIMIT 1];
                        Contact oldCon = [SELECT id, FirstName, LastName, Survey_Number__c, Survey_Automation_Step__c, Survey_Forward_URL__c, Survey_URL__c, Opt_In_Surveys__c
                                          FROM Contact WHERE id = :trigger.old[i].Contact__c LIMIT 1];
                        oldCon.Survey_Automation_Step__c = 'AS - Forward';
                        update oldCon;
                        if (newCon.Opt_In_Surveys__c == true) {
                            newCon.Survey_Number__c = trigger.new[i].Name;
                            newCon.Survey_Automation_Step__c = 'AS - Survey';
                            newCon.Survey_URL__c = trigger.new[i].New_Survey_URL__c.substringAfter('://');
                            newCon.Survey_Forward_URL__c = trigger.new[i].Forward_Survey_URL__c.substringAfter('://');
                            update newCon;
                        } 
                    } else {
                        Contact newCon = [SELECT id, FirstName, LastName, ETS_Survey_Number__c, ETS_Survey_Step__c, ETS_Survey_URL__c, ETS_Forward_Survey_URL__c, Last_Transactional_Survey_Sent_Date__c, Opt_In_Surveys__c
                                          FROM Contact WHERE id = :trigger.new[i].Contact__c LIMIT 1];
                        Contact oldCon = [SELECT id, FirstName, LastName, ETS_Survey_Number__c, ETS_Survey_Step__c, ETS_Survey_URL__c, ETS_Forward_Survey_URL__c, Last_Transactional_Survey_Sent_Date__c, Opt_In_Surveys__c
                                          FROM Contact WHERE id = :trigger.old[i].Contact__c LIMIT 1];
                        oldCon.ETS_Survey_Number__c = null;
                        oldCon.ETS_Survey_Step__c = null;
                        oldCon.ETS_Survey_URL__c = null;
                        oldCon.ETS_Forward_Survey_URL__c = null;
                        update oldCon;
                        if (newCon.Opt_In_Surveys__c == true) {
                            newCon.ETS_Survey_Number__c = trigger.new[i].Name;
                            newCon.ETS_Survey_Step__c = 'Survey';
                            newCon.ETS_Survey_URL__c = trigger.new[i].New_Survey_URL__c.substringAfter('://');
                            newCon.ETS_Forward_Survey_URL__c = trigger.new[i].Forward_Survey_URL__c.substringAfter('://');
                            newCon.Last_Transactional_Survey_Sent_Date__c = date.today();
                            update newCon;
                        } else {
                            Case c = [SELECT id, No_Survey_Created_Reason__c FROM Case WHERE Survey_Feedback__c = :trigger.new[i].id LIMIT 1];
                            c.No_Survey_Created_Reason__c = 'Forwarded contact is opted out of surveys';
                            update c;
                            delSurvs.add(trigger.new[i].id);
                        }
                    }
                }
            }
        }
        if (delSurvs.size() > 0) {
            delSurveys();
        }
    }
    
    public void findRecipients() {
        Set<id> useridSet = new Set<id>();
        for (Survey_Settings__c recipient : [SELECT id, SetupOwnerid FROM Survey_Settings__c WHERE Survey_Forward__c = true]) {
            useridSet.add(recipient.SetupOwnerid);
        }
        if (useridSet.size() == 0) {
            toAddresses.add([SELECT Email FROM User WHERE LastName = 'Werner' LIMIT 1].Email);
            toAddresses.add([SELECT Email FROM User WHERE LastName = 'McCauley' LIMIT 1].Email);
        }
        if (useridSet.size() > 0) {
            User[] users = [SELECT id, Email FROM User WHERE id IN :useridSet];
            for (User u : users) {
                toAddresses.add(u.Email);
            }
        }        
    }
    
    public void delSurveys() {
        Survey_Feedback__c[] survsToDel = [SELECT id FROM Survey_Feedback__c WHERE id IN :delSurvs];
        if (!test.isRunningTest()) {
            delete survsToDel;
        }
    }
}