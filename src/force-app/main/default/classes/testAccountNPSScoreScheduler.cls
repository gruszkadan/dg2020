@isTest(seeAllData=false)
private class testAccountNPSScoreScheduler {
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    //sObjectField fields = new list<Schema.sObjectField>();
    
    sObjectField acctNPS = Account.Chemical_Management_NPS_Score__c;
    sObjectField acctRank = Account.Chemical_Management_NPS_Ranking__c;
    sObjectField acctDate = Account.Chemical_Management_NPS_Last_Survey_Date__c;
    sObjectField acctPrev = Account.Chemical_Management_Prev_NPS_Score__c;
    
    static testmethod void test_accountNPSScoreBatchable() {
        Account[] accts = new list<Account>();
        for (integer i=1; i<=50; i++) {
            Account acct = new Account(Name = 'Test', Customer_Status__c = 'Active', AdminID__c = 'id'+i);
            if (1 == 1) {
                acct.Chemical_Management_NPS_Score__c = 8;
                acct.Chemical_Management_NPS_Ranking__c = 'Promoter';
                acct.Chemical_Management_NPS_Last_Survey_Date__c = date.Today().addDays(-350) - i;
                acct.Chemical_Management_Prev_NPS_Score__c = 4;
            }
            accts.add(acct);
        }
        insert accts;
        
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new accountNPSScoreScheduler());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        accountNPSScoreBatchable upb = new accountNPSScoreBatchable();
        id batchprocessId = Database.executeBatch(upb);
        
        Test.stopTest();
        Account[] check = [Select ID, Chemical_Management_NPS_Score__c, Chemical_Management_Prev_NPS_Score__c, Chemical_Management_NPS_Last_Survey_Date__c from Account where ID in: accts 
                           and Chemical_Management_NPS_Score__c = NULL];
        system.assertEquals(5, check.size());

    }
}