@isTest(SeeAllData=true)
private class testKBContentSubmission {
    
    static testMethod void test1() {
        //Setup
        
        SF_Knowledge_Content_Request__c newSubmission = new SF_Knowledge_Content_Request__c();
        newSubmission.Submission_Date__c = System.today();
        newSubmission.Submission_Type__c = 'SF Knowledge Article';
        newSubmission.Article_Type__c = 'Internal SOP';
        newSubmission.Article_Body__c = 'test';
        insert newSubmission;
        
        ApexPages.currentPage().getParameters().put('Id', newSubmission.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new SF_Knowledge_Content_Request__c());
        kbContentSubmissionController myController = new kbContentSubmissionController(testController); 
        newSubmission =[SELECT Id, OwnerID FROM SF_Knowledge_Content_Request__c WHERE Id = :newSubmission.Id];
        System.assertEquals('00580000006Ac0H',newSubmission.OwnerId);
    }
    
    static testMethod void test2() {
        ApexPages.StandardController testController = new ApexPages.StandardController(new SF_Knowledge_Content_Request__c());
        kbContentSubmissionController myController = new kbContentSubmissionController(testController);
        
        myController.newSubmission.Submission_Type__c = 'SF Knowledge Article';
        myController.step2();
    }
    
    static testMethod void test3() {
        ApexPages.StandardController testController = new ApexPages.StandardController(new SF_Knowledge_Content_Request__c());
        kbContentSubmissionController myController = new kbContentSubmissionController(testController); 
        
        myController.newSubmission.Submission_Type__c = 'SF Content Document';
        myController.step2();
    }
    
    static testMethod void test4() {
        ApexPages.StandardController testController = new ApexPages.StandardController(new SF_Knowledge_Content_Request__c());
        kbContentSubmissionController myController = new kbContentSubmissionController(testController); 
        
        myController.newSubmission.Submission_Date__c = System.today();
        myController.newSubmission.Submission_Type__c = 'SF Knowledge Article';
        myController.newSubmission.Article_Type__c = 'Internal SOP';
        myController.newSubmission.Article_Body__c = 'test';
        myController.saveNewSubmission();
    }
}