/*
* Test class: testSalesPerformanceSummaryTrigger
*/
public class salesPerformanceSummaryTriggerHandler {
    
    public static void beforeInsert(Sales_Performance_Summary__c[] oldSummaries, Sales_Performance_Summary__c[] newSummaries) {}
    
    public static void beforeUpdate(Sales_Performance_Summary__c[] oldSummaries, Sales_Performance_Summary__c[] newSummaries) {
    //SF-53877
        /*  Sales_Performance_Summary__c[] esAssociateEligibilityChange = new list<Sales_Performance_Summary__c>();
        
        for (integer i=0; i<newSummaries.size(); i++) {
            //Determine if ES associate is eligible for bookings credit 
            if(
                ((newSummaries[i].Total_Bookings__c >= newSummaries[i].Quota__c
                  && oldSummaries[i].Total_Bookings__c < oldSummaries[i].Quota__c)
                 ||
                 (newSummaries[i].Total_Bookings__c < newSummaries[i].Quota__c
                  && oldSummaries[i].Total_Bookings__c >= oldSummaries[i].Quota__c))
                && newSummaries[i].Total_Bookings__c != 0 
                && newSummaries[i].Quota__c != 0
                && newSummaries[i].Group__c == 'Enterprise Sales'
                && newSummaries[i].Role__c == 'Associate'
            ){
                esAssociateEligibilityChange.add(newSummaries[i]);
            }
        }
        
        if(esAssociateEligibilityChange.size() > 0){
            salesPerformanceSummaryUtility.updateEsAssociateCreditEligibility(esAssociateEligibilityChange);
        }*/
    }
    
    public static void afterInsert(Sales_Performance_Summary__c[] oldSummaries, Sales_Performance_Summary__c[] newSummaries) {}
    
    public static void afterUpdate(Sales_Performance_Summary__c[] oldSummaries, Sales_Performance_Summary__c[] newSummaries) {
        Sales_Performance_Summary__c[] ytdTotals = new list<Sales_Performance_Summary__c>();
        Sales_Performance_Summary__c[] associateITUpdates = new list<Sales_Performance_Summary__c>();
       // Sales_Performance_Summary__c[] createKicker = new list<Sales_Performance_Summary__c>();
       // Sales_Performance_Summary__c[] removeKicker = new list<Sales_Performance_Summary__c>();
        for (integer i=0; i<newSummaries.size(); i++) {
            if (newSummaries[i].Total_Bookings__c != oldSummaries[i].Total_Bookings__c
                || newSummaries[i].Quota__c != oldSummaries[i].Quota__c) {
                    ytdTotals.add(newSummaries[i]);
                }
            //if eligibility for associate credit changes 
            if (newSummaries[i].Eligible_for_Associate_Credit__c != oldSummaries[i].Eligible_for_Associate_Credit__c) {
                associateITUpdates.add(newSummaries[i]);
                //SF-53877
                /*if(newSummaries[i].Group__c == 'Enterprise Sales' && newSummaries[i].Role__c == 'Associate'){
                    if(newSummaries[i].Eligible_for_Associate_Credit__c){
                        createKicker.add(newSummaries[i]);
                    }else{
                        removeKicker.add(newSummaries[i]);
                    }  
                }*/
            }
        }
        if (ytdTotals.size() > 0) {
            salesPerformanceSummaryUtility.updateYTDTotals(ytdTotals);
        }
        if(associateITUpdates.size() > 0){
            incentiveTransactionUtility.updateAssociateCreditITs(associateITUpdates);
        }
        //SF-53877
        /*if(createKicker.size() > 0){
            incentiveTransactionUtility.createEsAssociateKicker(createKicker);
        }
        if(removeKicker.size() > 0){
            incentiveTransactionUtility.removeEsAssociateKicker(removeKicker);
        }*/
    }
    
}