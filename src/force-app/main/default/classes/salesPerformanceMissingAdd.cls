public class salesPerformanceMissingAdd {
    public String viewAs {get;set;}
    public String viewAsDefaultView {get;set;}
    public string userID {get;set;}
    public User u {get;set;}
    public User currentUser {get;set;}
    public String defaultView {get;set;}
    public Sales_Performance_Request__c newSPR {get;set;}
    public String errorMessage {get;set;}
    public String alertIcon {get;set;}
    public Boolean isSuccess {get;set;}
    public String pageName {get;set;}
    public boolean directorVP {get;set;}
    public boolean previousMonthClosed {get;set;}
    public list<String> approverNames {get;set;}
    public List<SelectOption> salesUsers {get;set;}
    
    public salesPerformanceMissingAdd() {
        //Get the Name of the Page
        pageName = ApexPages.currentPage().getUrl().substringAfter('apex/');
        //Get the Default View to display on the Homepage
        defaultView = Sales_Performance_Settings__c.getInstance().Default_View__c;
        //get viewAs id
        viewAs = ApexPages.currentPage().getParameters().get('va');
        if(!salesPerformanceUtility.canViewAs()){
            viewAs = userInfo.getUserId();
        }
        u = [SELECT id, Alias, FirstName, LastName, Name, ManagerID, Manager.Name, Role__c, Sales_Team__c, FullPhotoURL, UserRoleID, Sales_Director__c,
             Sales_Director__r.Name
             FROM User 
             WHERE id = :viewAs LIMIT 1];
        
        userID = u.id;
        //get defaultView to display when viewing as
        defaultView = Sales_Performance_Settings__c.getInstance(u.id).Default_View__c;
        newSPR = new Sales_Performance_Request__c();
        //Set directorVP Variable (used to determine if approval is required below)
        if(defaultView =='Director' || defaultView =='VP'){
            directorVP = TRUE;
        } else {
            directorVP = FALSE;
        }
        errorMessage = 'Please make sure all required fields are filled out';
        previousMonthClosed = salesPerformanceUtility.previousMonthClosed();
        //Get List of Sales Users to Display in Sales Rep Select List
        salesUsers = salesPerformanceUtility.getSalesUsers(defaultView, userID, u.Name);
    }
    
    //Get List of Incentive Months for Add Incentive
    public List<SelectOption> getIncentiveMonths() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Current Month', 'Current Month'));
        //If its the 6th business day or less in the current month, display Previous Mont
        if(!previousMonthClosed){
            options.add(new SelectOption('Previous Month', 'Previous Month'));
        }
        return options;
    }
    
    //Method Used on the Add Incentive Page
    public void addIncentive(){
        approverNames = new list<String>();
        //Set the Variables
        isSuccess = TRUE;
        alertIcon = 'success';
        errorMessage = '';
        
        //Create the SPR Record
        newSPR.Type__c = 'Add Incentive';
        newSPR.OwnerId = u.ID;
        if(newSPR.Incentive_Month__c == null){
            newSPR.Incentive_Month__c = 'Current Month';
        }
        if(defaultView =='Director' || defaultView =='VP'){
            newSPR.Status__c ='Approved';
            newSPR.Requires_Approval__c = FALSE;
        } 
        if(defaultView =='Rep'){
            newSPR.Approval_Required_By__c = salesPerformanceRequestUtility.approvalRequiredBy(string.valueOf(u.ID), TRUE, TRUE, FALSE);
            newSPR.Assigned_Approver_1__c = u.ManagerId;
            newSPR.Assigned_Approver_2__c = u.Sales_Director__c;
            newSPR.Status__c ='Pending Approval';
            newSPR.Requires_Approval__c = TRUE;
            approverNames.add(u.Manager.Name);
            approverNames.add(u.Sales_Director__r.Name);
            
        }
        if(defaultView =='Manager'){
            newSPR.Approval_Required_By__c = salesPerformanceRequestUtility.approvalRequiredBy(string.valueOf(u.ID), TRUE, FALSE, FALSE);
            newSPR.Assigned_Approver_1__c = u.ManagerId;
            newSPR.Status__c ='Pending Approval';
            newSPR.Requires_Approval__c = TRUE;
            approverNames.add(u.Manager.Name);
        }
        
        //Find the SPS to associate the SPR to
        newSPR.Sales_Performance_Summary__c = salesPerformanceRequestUtility.findSprSps(null, newSPR);
        
        try {
            insert newSPR;
        } catch(System.DMLexception e) {
            errorMessage = string.valueOf(e);
            isSuccess = false;
            alertIcon = 'error';
        }
    }
    
    //Method Used on the Missing Page
    public void createMissing(){
        //Set the Variables
        boolean okToSave = TRUE;
        isSuccess = TRUE;
        alertIcon = 'success';
        
        //Create the SPR Record
        newSPR.Type__c = 'Missing Bookings & Incentive';
        newSPR.Status__c ='Pending Approval';
        newSPR.Requires_Approval__c = TRUE;
        newSPR.OwnerId = u.Id;
        newSPR.Approval_Required_By__c = salesPerformanceRequestUtility.approvalRequiredBy(string.valueOf(u.ID), TRUE, FALSE, FALSE);
        newSPR.Assigned_Approver_1__c = u.ManagerID;
        
        if(newSpr.Missing__c == NULL || newSPR.Reason__c == NULL){
            okToSave = FALSE;
        }
        if((newSPR.Missing__c =='Bookings' || newSPR.Missing__c=='Both') && newSPR.Contract__c == NULL){
            okToSave = FALSE;
        }
        
        if(okToSave){
            try {
                insert newSPR;
            } catch(System.DMLexception e) {
                errorMessage = string.valueOf(e);
                isSuccess = false;
                alertIcon = 'error';
            }
        } else {
            isSuccess = false;
            alertIcon = 'error';
        }
    }

}