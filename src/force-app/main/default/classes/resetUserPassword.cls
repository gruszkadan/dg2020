global with sharing class resetUserPassword implements Process.Plugin {
    
    global Process.PluginResult invoke(Process.PluginRequest request) {
        string UserID = (String) request.inputParameters.get('userid');
        resetPassword(UserID);
        // return to Flow
        Map<String,Object> result = new Map<String,Object>();  
        return new Process.PluginResult(result);  
    }
    
    global Process.PluginDescribeResult describe() {
        Process.PluginDescribeResult result = new Process.PluginDescribeResult();
        result.Name = 'Reset User Password';
        result.Tag = 'User Management';
        result.inputParameters =new List<Process.PluginDescribeResult.InputParameter>();
        result.inputParameters.add(
            new Process.PluginDescribeResult.InputParameter('userid',Process.PluginDescribeResult.ParameterType.STRING, true));
        return result; 
        
    }
    
    public void resetPassword(String UserID)
    {
        System.setPassword(UserID, 'Safety1!');
    }
}