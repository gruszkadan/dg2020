trigger ameTrigger on Account_Management_Event__c (After Update) {
    if (trigger.isAfter) {
        // AFTER UPDATE
        if (trigger.isUpdate) {
            ameTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
    }
}