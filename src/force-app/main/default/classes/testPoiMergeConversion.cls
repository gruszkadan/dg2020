@isTest(seeAllData=true)
private class testPoiMergeConversion {
     // tests poi Contact Merges
    static testmethod void testLeadConversion() {
        Account newAccount = testAccount();
        insert newAccount;
        Contact master = testContact(newAccount.id);
        master.POI_Stage_AUTH__c = 'Neutral Interest';
        master.POI_Stage_ERGO__c = 'Neutral Interest';
        master.POI_Stage_MSDS__c = 'Neutral Interest';
        master.POI_Stage_EHS__c = 'Neutral Interest';
        master.POI_Stage_ODT__c = 'Neutral Interest';
        master.POI_Status_AUTH__c = 'Working - Connected';
        master.POI_Status_ERGO__c = 'Working - Connected';
        master.POI_Status_MSDS__c = 'Working - Connected';
        master.POI_Status_EHS__c = 'Working - Connected';
        master.POI_Status_ODT__c = 'Working - Connected';
        master.POI_Status_Change_Date_AUTH__c = date.today();
        master.POI_Status_Change_Date_ERGO__c = date.today();
        master.POI_Status_Change_Date_MSDS__c = date.today();
        master.POI_Status_Change_Date_EHS__c = date.today();
        master.POI_Status_Change_Date_ODT__c = date.today();
        insert master;
        
        //Create a queue record
        Lead newLead = testLead();
        newLead.POI_Stage_AUTH__c = 'Client';
        newLead.POI_Stage_ERGO__c = 'Client';
        newLead.POI_Stage_MSDS__c = 'Client';
        newLead.POI_Stage_EHS__c = 'Client';
        newLead.POI_Stage_ODT__c = 'Client';
        newLead.POI_Status_AUTH__c = 'Open';
        newLead.POI_Status_ERGO__c = 'Open';
        newLead.POI_Status_MSDS__c = 'Open';
        newLead.POI_Status_EHS__c = 'Open';
        newLead.POI_Status_ODT__c = 'Open';
        newLead.POI_Status_Change_Date_AUTH__c = date.today().addDays(-1);
        newLead.POI_Status_Change_Date_ERGO__c = date.today().addDays(-1);
        newLead.POI_Status_Change_Date_MSDS__c = date.today().addDays(-1);
        newLead.POI_Status_Change_Date_EHS__c = date.today().addDays(-1);
        newLead.POI_Status_Change_Date_ODT__c = date.today().addDays(-1);
        insert newLead;
        
        
        //Master Contact's POI
        Product_of_Interest__c newPoi = new Product_of_Interest__c();
        newPoi.Contact__c = master.id;
        insert newPoi;
        
        //MSDS Open Action
        Product_of_Interest_Action__c msdsOpenAction = new Product_of_Interest_Action__c();
        msdsOpenAction.Contact__c = master.id;
        msdsOpenAction.Product_of_Interest__c = newPoi.id;
        msdsOpenAction.Marketo_ID__c = '1234567';
        msdsOpenAction.Product_Suite__c = 'MSDS Management';
        msdsOpenAction.Product__c = 'HQ';
        msdsOpenAction.Action__c = 'Request Demo';
        
        insert msdsOpenAction;
        
        //MSDS  Closed Action
        Product_of_Interest_Action__c msdsClosedAction = new Product_of_Interest_Action__c();
        msdsClosedAction.Contact__c = master.id;
        msdsClosedAction.Product_of_Interest__c = newPoi.id;
        msdsClosedAction.Marketo_ID__c = '1234567';
        msdsClosedAction.Product_Suite__c = 'MSDS Management';
        msdsClosedAction.Product__c = 'HQ';
        msdsClosedAction.Action__c = 'Request Demo';
        msdsClosedAction.Action_Status__c = 'Open';
        insert msdsClosedAction;
        msdsClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update msdsClosedAction;
        
        //EHS Open Action
        Product_of_Interest_Action__c ehsOpenAction = new Product_of_Interest_Action__c();
        ehsOpenAction.Contact__c = master.id;
        ehsOpenAction.Product_of_Interest__c = newPoi.id;
        ehsOpenAction.Marketo_ID__c = '1234567';
        ehsOpenAction.Product_Suite__c = 'EHS Management';
        ehsOpenAction.Product__c = 'EHS Management';
        ehsOpenAction.Action__c = 'Request Demo';
        
        insert ehsOpenAction;
        
        //EHS  Closed Action
        Product_of_Interest_Action__c ehsClosedAction = new Product_of_Interest_Action__c();
        ehsClosedAction.Contact__c = master.id;
        ehsClosedAction.Product_of_Interest__c = newPoi.id;
        ehsClosedAction.Marketo_ID__c = '1234567';
        ehsClosedAction.Product_Suite__c = 'EHS Management';
        ehsClosedAction.Product__c = 'EHS Management';
        ehsClosedAction.Action__c = 'Request Demo';
        ehsClosedAction.Action_Status__c = 'Open';
        insert ehsClosedAction;
        ehsClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update ehsClosedAction;
        
        //AUTH Open Action
        Product_of_Interest_Action__c authOpenAction = new Product_of_Interest_Action__c();
        authOpenAction.Contact__c = master.id;
        authOpenAction.Product_of_Interest__c = newPoi.id;
        authOpenAction.Marketo_ID__c = '1234567';
        authOpenAction.Product_Suite__c = 'MSDS Authoring';
        authOpenAction.Product__c = 'MSDS Authoring';
        authOpenAction.Action__c = 'Request Demo';
        
        insert authOpenAction;
        
        //AUTH  Closed Action
        Product_of_Interest_Action__c authClosedAction = new Product_of_Interest_Action__c();
        authClosedAction.Contact__c = master.id;
        authClosedAction.Product_of_Interest__c = newPoi.id;
        authClosedAction.Marketo_ID__c = '1234567';
        authClosedAction.Product_Suite__c = 'MSDS Authoring';
        authClosedAction.Product__c = 'MSDS Authoring';
        authClosedAction.Action__c = 'Request Demo';
        authClosedAction.Action_Status__c = 'Open';
        insert authClosedAction;
        authClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update authClosedAction;
        
        //ERGO Open Action
        Product_of_Interest_Action__c ergoOpenAction = new Product_of_Interest_Action__c();
        ergoOpenAction.Contact__c = master.id;
        ergoOpenAction.Product_of_Interest__c = newPoi.id;
        ergoOpenAction.Marketo_ID__c = '1234567';
        ergoOpenAction.Product_Suite__c = 'Ergonomics';
        ergoOpenAction.Product__c = 'Ergonomics';
        ergoOpenAction.Action__c = 'Request Demo';
        
        insert ergoOpenAction;
        
        //ERGO  Closed Action
        Product_of_Interest_Action__c ergoClosedAction = new Product_of_Interest_Action__c();
        ergoClosedAction.Contact__c = master.id;
        ergoClosedAction.Product_of_Interest__c = newPoi.id;
        ergoClosedAction.Marketo_ID__c = '1234567';
        ergoClosedAction.Product_Suite__c = 'Ergonomics';
        ergoClosedAction.Product__c = 'Ergonomics';
        ergoClosedAction.Action__c = 'Request Demo';
        ergoClosedAction.Action_Status__c = 'Open';
        insert ergoClosedAction;
        ergoClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update ergoClosedAction;
        
          //ODT Open Action
        Product_of_Interest_Action__c odtOpenAction = new Product_of_Interest_Action__c();
        odtOpenAction.Contact__c = master.id;
        odtOpenAction.Product_of_Interest__c = newPoi.id;
        odtOpenAction.Marketo_ID__c = '1234567';
        odtOpenAction.Product_Suite__c = 'On-Demand Training';
        odtOpenAction.Product__c = 'On-Demand Training';
        odtOpenAction.Action__c = 'Request Demo';
        
        insert odtOpenAction;
        
        //ODT  Closed Action
        Product_of_Interest_Action__c odtClosedAction = new Product_of_Interest_Action__c();
        odtClosedAction.Contact__c = master.id;
        odtClosedAction.Product_of_Interest__c = newPoi.id;
        odtClosedAction.Marketo_ID__c = '1234567';
        odtClosedAction.Product_Suite__c = 'On-Demand Training';
        odtClosedAction.Product__c = 'On-Demand Training';
        odtClosedAction.Action__c = 'Request Demo';
        odtClosedAction.Action_Status__c = 'Open';
        insert odtClosedAction;
        odtClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update odtClosedAction;
        
        //Lead's POI
        Product_of_Interest__c newPoi2 = new Product_of_Interest__c();
        newPoi2.Lead__c = newLead.id;
        insert newPoi2;
        
        //ODT Open Action
        Product_of_Interest_Action__c odtOpenAction2 = new Product_of_Interest_Action__c();
        odtOpenAction2.Lead__c = newLead.id;
        odtOpenAction2.Product_of_Interest__c = newPoi2.id;
        odtOpenAction2.Marketo_ID__c = '1234567';
        odtOpenAction2.Product_Suite__c = 'On-Demand Training';
        odtOpenAction2.Product__c = 'On-Demand Training';
        odtOpenAction2.Action__c = 'Request Demo';
        
        insert odtOpenAction2;
        
        //ODT  Closed Action
        Product_of_Interest_Action__c odtClosedAction2 = new Product_of_Interest_Action__c();
        odtClosedAction2.Lead__c = newLead.id;
        odtClosedAction2.Product_of_Interest__c = newPoi2.id;
        odtClosedAction2.Marketo_ID__c = '1234567';
        odtClosedAction2.Product_Suite__c = 'On-Demand Training';
        odtClosedAction2.Product__c = 'On-Demand Training';
        odtClosedAction2.Action__c = 'Request Demo';
        odtClosedAction2.Action_Status__c = 'Open';
        insert odtClosedAction2;
        odtClosedAction2.Action_Status__c = 'Resolved - Unable to Connect';
        update odtClosedAction2;
        
        test.startTest();
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(newLead.id);
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setContactId(master.id);
        lc.setAccountId(newAccount.id);
        Database.convertLead(lc);
        test.stopTest();
    }
    
    static testmethod void testContactMerge() {
        Account newAccount = testAccount();
        insert newAccount;
        
        Contact master = testContact(newAccount.id);
        master.POI_Stage_AUTH__c = 'Neutral Interest';
        master.POI_Stage_ERGO__c = 'Neutral Interest';
        master.POI_Stage_MSDS__c = 'Neutral Interest';
        master.POI_Stage_EHS__c = 'Neutral Interest';
        master.POI_Stage_ODT__c = 'Neutral Interest';
        master.POI_Status_AUTH__c = 'Working - Connected';
        master.POI_Status_ERGO__c = 'Working - Connected';
        master.POI_Status_MSDS__c = 'Working - Connected';
        master.POI_Status_EHS__c = 'Working - Connected';
        master.POI_Status_ODT__c = 'Working - Connected';
        master.POI_Status_Change_Date_AUTH__c = date.today();
        master.POI_Status_Change_Date_ERGO__c = date.today();
        master.POI_Status_Change_Date_MSDS__c = date.today();
        master.POI_Status_Change_Date_EHS__c = date.today();
        master.POI_Status_Change_Date_ODT__c = date.today();
        insert master;
        
        Contact[] duplicates = new list<Contact>{testContact(newAccount.id),testContact(newAccount.id)};
            for(Contact d:duplicates){
                d.POI_Stage_AUTH__c = 'Sales Accepted';
                d.POI_Stage_ERGO__c = 'Sales Accepted';
                d.POI_Stage_MSDS__c = 'Sales Accepted';
                d.POI_Stage_EHS__c = 'Sales Accepted';
                d.POI_Stage_ODT__c = 'Sales Accepted';
                d.POI_Status_AUTH__c = 'Open';
                d.POI_Status_ERGO__c = 'Open';
                d.POI_Status_MSDS__c = 'Open';
                d.POI_Status_EHS__c = 'Open';
                d.POI_Status_ODT__c = 'Open';
                d.POI_Status_Change_Date_AUTH__c = date.today().addDays(-1);
                d.POI_Status_Change_Date_ERGO__c = date.today().addDays(-1);
                d.POI_Status_Change_Date_MSDS__c = date.today().addDays(-1);
                d.POI_Status_Change_Date_EHS__c = date.today().addDays(-1);
                d.POI_Status_Change_Date_ODT__c = date.today().addDays(-1);
            }
        insert duplicates;
        
        test.startTest();
        database.merge(master,duplicates);
        test.stopTest();
    }
    
    static testmethod void testLeadMerge() {
       
        
        Lead master = testLead();
        master.POI_Stage_AUTH__c = 'Neutral Interest';
        master.POI_Stage_ERGO__c = 'Neutral Interest';
        master.POI_Stage_MSDS__c = 'Neutral Interest';
        master.POI_Stage_EHS__c = 'Neutral Interest';
        master.POI_Stage_ODT__c = 'Neutral Interest';
        master.POI_Status_AUTH__c = 'Resolved - Not a Target';
        master.POI_Status_ERGO__c = 'Resolved - Not a Target';
        master.POI_Status_MSDS__c = 'Resolved - Not a Target';
        master.POI_Status_EHS__c = 'Resolved - Not a Target';
        master.POI_Status_ODT__c = 'Resolved - Not a Target';
        master.POI_Status_Change_Date_AUTH__c = date.today();
        master.POI_Status_Change_Date_ERGO__c = date.today();
        master.POI_Status_Change_Date_MSDS__c = date.today();
        master.POI_Status_Change_Date_EHS__c = date.today();
        master.POI_Status_Change_Date_ODT__c = date.today();
        insert master;
        
        Lead[] duplicates = new list<Lead>{testLead(),testLead()};
            for(Lead d:duplicates){
                d.POI_Stage_AUTH__c = 'Sales Accepted';
                d.POI_Stage_ERGO__c = 'Sales Accepted';
                d.POI_Stage_MSDS__c = 'Sales Accepted';
                d.POI_Stage_EHS__c = 'Sales Accepted';
                d.POI_Stage_ODT__c = 'Sales Accepted';
                d.POI_Status_AUTH__c = 'Resolved - Not a Target';
                d.POI_Status_ERGO__c = 'Resolved - Not a Target';
                d.POI_Status_MSDS__c = 'Resolved - Not a Target';
                d.POI_Status_EHS__c = 'Resolved - Not a Target';
                d.POI_Status_ODT__c = 'Resolved - Not a Target';
                d.POI_Status_Change_Date_AUTH__c = date.today().addDays(-1);
                d.POI_Status_Change_Date_ERGO__c = date.today().addDays(-1);
                d.POI_Status_Change_Date_MSDS__c = date.today().addDays(-1);
                d.POI_Status_Change_Date_EHS__c = date.today().addDays(-1);
                d.POI_Status_Change_Date_ODT__c = date.today().addDays(-1);
            }
        insert duplicates;
        
        //Lead's POI
        Product_of_Interest__c newPoi = new Product_of_Interest__c();
        newPoi.Lead__c = master.id;
        insert newPoi;
        
         //MSDS Open Action
        Product_of_Interest_Action__c msdsOpenAction = new Product_of_Interest_Action__c();
        msdsOpenAction.Lead__c = master.id;
        msdsOpenAction.Product_of_Interest__c = newPoi.id;
        msdsOpenAction.Marketo_ID__c = '1234567';
        msdsOpenAction.Product_Suite__c = 'MSDS Management';
        msdsOpenAction.Product__c = 'HQ';
        msdsOpenAction.Action__c = 'Request Demo';
        
        insert msdsOpenAction;
        
        //MSDS  Closed Action
        Product_of_Interest_Action__c msdsClosedAction = new Product_of_Interest_Action__c();
        msdsClosedAction.Lead__c = master.id;
        msdsClosedAction.Product_of_Interest__c = newPoi.id;
        msdsClosedAction.Marketo_ID__c = '1234567';
        msdsClosedAction.Product_Suite__c = 'MSDS Management';
        msdsClosedAction.Product__c = 'HQ';
        msdsClosedAction.Action__c = 'Request Demo';
        msdsClosedAction.Action_Status__c = 'Open';
        insert msdsClosedAction;
        msdsClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update msdsClosedAction;
        
        //EHS Open Action
        Product_of_Interest_Action__c ehsOpenAction = new Product_of_Interest_Action__c();
        ehsOpenAction.Lead__c = master.id;
        ehsOpenAction.Product_of_Interest__c = newPoi.id;
        ehsOpenAction.Marketo_ID__c = '1234567';
        ehsOpenAction.Product_Suite__c = 'EHS Management';
        ehsOpenAction.Product__c = 'EHS Management';
        ehsOpenAction.Action__c = 'Request Demo';
        
        insert ehsOpenAction;
        
        //EHS  Closed Action
        Product_of_Interest_Action__c ehsClosedAction = new Product_of_Interest_Action__c();
        ehsClosedAction.Lead__c = master.id;
        ehsClosedAction.Product_of_Interest__c = newPoi.id;
        ehsClosedAction.Marketo_ID__c = '1234567';
        ehsClosedAction.Product_Suite__c = 'EHS Management';
        ehsClosedAction.Product__c = 'EHS Management';
        ehsClosedAction.Action__c = 'Request Demo';
        ehsClosedAction.Action_Status__c = 'Open';
        insert ehsClosedAction;
        ehsClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update ehsClosedAction;
        
        //AUTH Open Action
        Product_of_Interest_Action__c authOpenAction = new Product_of_Interest_Action__c();
        authOpenAction.Lead__c = master.id;
        authOpenAction.Product_of_Interest__c = newPoi.id;
        authOpenAction.Marketo_ID__c = '1234567';
        authOpenAction.Product_Suite__c = 'MSDS Authoring';
        authOpenAction.Product__c = 'MSDS Authoring';
        authOpenAction.Action__c = 'Request Demo';
        
        insert authOpenAction;
        
        //AUTH  Closed Action
        Product_of_Interest_Action__c authClosedAction = new Product_of_Interest_Action__c();
        authClosedAction.Lead__c = master.id;
        authClosedAction.Product_of_Interest__c = newPoi.id;
        authClosedAction.Marketo_ID__c = '1234567';
        authClosedAction.Product_Suite__c = 'MSDS Authoring';
        authClosedAction.Product__c = 'MSDS Authoring';
        authClosedAction.Action__c = 'Request Demo';
        authClosedAction.Action_Status__c = 'Open';
        insert authClosedAction;
        authClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update authClosedAction;
        
        //ERGO Open Action
        Product_of_Interest_Action__c ergoOpenAction = new Product_of_Interest_Action__c();
        ergoOpenAction.Lead__c = master.id;
        ergoOpenAction.Product_of_Interest__c = newPoi.id;
        ergoOpenAction.Marketo_ID__c = '1234567';
        ergoOpenAction.Product_Suite__c = 'Ergonomics';
        ergoOpenAction.Product__c = 'Ergonomics';
        ergoOpenAction.Action__c = 'Request Demo';
        
        insert ergoOpenAction;
        
        //ERGO  Closed Action
        Product_of_Interest_Action__c ergoClosedAction = new Product_of_Interest_Action__c();
        ergoClosedAction.Lead__c = master.id;
        ergoClosedAction.Product_of_Interest__c = newPoi.id;
        ergoClosedAction.Marketo_ID__c = '1234567';
        ergoClosedAction.Product_Suite__c = 'Ergonomics';
        ergoClosedAction.Product__c = 'Ergonomics';
        ergoClosedAction.Action__c = 'Request Demo';
        ergoClosedAction.Action_Status__c = 'Open';
        insert ergoClosedAction;
        ergoClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update ergoClosedAction;
        
          //ODT Open Action
        Product_of_Interest_Action__c odtOpenAction = new Product_of_Interest_Action__c();
        odtOpenAction.Lead__c = master.id;
        odtOpenAction.Product_of_Interest__c = newPoi.id;
        odtOpenAction.Marketo_ID__c = '1234567';
        odtOpenAction.Product_Suite__c = 'On-Demand Training';
        odtOpenAction.Product__c = 'On-Demand Training';
        odtOpenAction.Action__c = 'Request Demo';
        
        insert odtOpenAction;
        
        //ODT  Closed Action
        Product_of_Interest_Action__c odtClosedAction = new Product_of_Interest_Action__c();
        odtClosedAction.Lead__c = master.id;
        odtClosedAction.Product_of_Interest__c = newPoi.id;
        odtClosedAction.Marketo_ID__c = '1234567';
        odtClosedAction.Product_Suite__c = 'On-Demand Training';
        odtClosedAction.Product__c = 'On-Demand Training';
        odtClosedAction.Action__c = 'Request Demo';
        odtClosedAction.Action_Status__c = 'Open';
        insert odtClosedAction;
        odtClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update odtClosedAction;
        
        //First Dupe's POI and Actions
        Product_of_Interest__c newPoi2 = new Product_of_Interest__c();
        newPoi2.Lead__c = duplicates[0].id;
        insert newPoi2;
        
        test.startTest();
        database.merge(master,duplicates);
        test.stopTest();
    }
    
    public static Account testAccount() {
        return new Account(Name='Test Account', OwnerId='00580000007F76q');
    }
    
    public static Contact testContact(id accountId) {
        return new Contact(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', AccountId=accountId);
    }
    
    public static Lead testLead() {
        return new Lead(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', Company = 'Test Account');
    }
    
    public static Product_of_Interest__c testPOI(id leadId, id contactId) {
        id personId;
        if(leadId != null){
            personId = leadId;
            return new Product_of_Interest__c(Lead__c = personId); 
        }else if(contactId != null){
            personId = contactId;
            return new Product_of_Interest__c(Contact__c = personId); 
        }
        return null;
    }
    
    public static Product_of_Interest_Action__c testPOIAction(id leadId, id contactId, Product_of_Interest__c poi) {
        id personId;
        if(leadId != null){
            personId = leadId;
            return new Product_of_Interest_Action__c(Lead__c = personId, Product_of_Interest__c = poi.Id); 
        }else if(contactId != null){
            personId = contactId;
            return new Product_of_Interest_Action__c(Contact__c = personId, Product_of_Interest__c = poi.Id); 
        }
        return null;
    }
}