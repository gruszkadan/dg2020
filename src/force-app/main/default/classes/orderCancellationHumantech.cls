public with sharing class orderCancellationHumantech {
    public orderItemWrapper[] oiList {get;set;}
    //public Order_Item__c[] cancelledOIs {get;set;}
    public Account Account{get; set;}
    public ID accountId {get;set;}
    public string errorMessage {get;set;}
    public string cancellationReason {get;set;}
    public string otherReason {get;set;}
    public string environment {get;set;}
    public id userId {get;set;}

    public orderCancellationHumantech() {
        //errorMessage = 'hello world';
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D300000001HEfEAM'){
            environment = 'Production';
        } else {
            environment ='Sandbox';
        }
        userId =  UserInfo.getUserId();
        accountId = ApexPages.currentPage().getParameters().get('id');   
        Account = [SELECT Name, Id, Ergonomics_Account_Owner__c, Ergonomics_Account_Owner__r.Email FROM Account WHERE Id =: accountId LIMIT 1];   
        Order_Item__c[] OrderList = [SELECT Admin_Tool_Product_Name__c, Order_Id__c, Order_Item_ID__c, Sales_Rep__c, Order_Date__c,Cancellation_Date__c,Site_Shutdown_Date__c, Cancellation_reason__c, Cancellation_Reason_Other__c,
                 Contract_Length__c, Invoice_Amount__c, Status__c,City__c, Term_Start_Date__c FROM Order_Item__c WHERE Account__c =: accountId AND Product_Suite__c = 'Ergonomics'AND Admin_Tool_Order_Status__c = 'A' AND Cancellation_Date__c = NULL ORDER BY Order_Id__c DESC];   
        oiList= new List<orderItemWrapper>();
        //cancelledOIs= new List<Order_Item__c>();
        for(Order_Item__c o: OrderList){
             oiList.add(new orderItemWrapper(o));
            /*if(o.Cancellation_reason__c == NULL){
                oiList.add(new orderItemWrapper(o));
            }else{
                cancelledOIs.add(o);
            }
			*/
        }
    }

     public List<SelectOption> getReasons(){
       List<SelectOption> options = new List<SelectOption>();
       Schema.DescribeFieldResult fieldResult = Order_Item__c.Cancellation_reason__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       options.add(new SelectOption('', 'None'));
       for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
       }     
       return options;
    }
    
    public void clearOtherReason(){
        otherReason = null;
    }

    public pagereference processCancellations(){
        errorMessage = null;
        //validation round one - get a cancellation reason
        if(cancellationReason == NULL){
            errorMessage= 'Please select a cancellation reason';
        }else if(cancellationReason == 'Other' && (otherReason == NULL || otherReason == '')){
            errorMessage= 'Please provide more details regarding the reason for cancellation';
        }     

        Order_Item__c[] oiToUpdate = new list<Order_Item__c>();
        for(orderItemWrapper w : oiList){
            if(w.isSelected){
                if(w.oi.Cancellation_Date__c != NULL && w.oi.Site_Shutdown_Date__c != NULL){
                w.oi.Cancellation_reason__c = cancellationReason;
                w.oi.Cancellation_Reason_Other__c = otherReason;
                //if(w.oi.Cancellation_Date__c < System.today()){
                   // w.oi.Admin_Tool_Order_Status__c = 'C';
                //}
                oiToUpdate.add(w.oi);
                }else{
                    if(errorMessage == Null){
                    	errorMessage= 'Please specify a cancellation date and a site shut down date for each order item being processed.';                    
                    }else{
                         errorMessage += ' and specify a cancellation date and a site shut down date for each order item being processed.';
                    }
                    break;
                }
            }
        }
        
        if(errorMessage != NULL){
            return null;
        }
        
        

        if(oiToUpdate.size() > 0){
            
            //build an email
            EmailTemplate eTemplate = [SELECT Id from EmailTemplate WHERE DeveloperName = 'Humantech_Cancellation_of_Product_Alert'];
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            //find who to send the email to
            string[] sendTo;  
            if(environment == 'Production'){
               sendTo = new string[] {'humantechhelp@ehs.com','humantechcs@ehs.com', 'klupo@ehs.com', 'wip@ehs.com', 'jmallon@ehs.com'};
            }else{
                sendTo = new string[] {'salesforceops@ehs.com'};
            }
            if(Account.Ergonomics_Account_Owner__r.Email != NULL){
                sendTo.add(Account.Ergonomics_Account_Owner__r.Email);      
            }
            //set the email fieldss
            message.toaddresses = sendTo;
            message.setWhatId(Account.Id);
            message.setTemplateId(eTemplate.Id);
            message.setTargetObjectId(userId);
            message.setTreatTargetObjectAsRecipient(false); 
            message.setSaveAsActivity(false);
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
			
            
            try{
                update oiToUpdate;   
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                PageReference pr = new PageReference('/'+Account.id);
                return pr.setRedirect(true);
            }catch(Exception e){
               errorMessage= string.valueOf(e); 
               return null;
            }
        }else{
            errorMessage= 'Please select at least one order item to cancel.'; 
            return null;
        }

    }

     public class orderItemWrapper {
        public Order_Item__c OI {get;set;}
        public boolean isSelected {get;set;}
        
        //Setting the two variables we defined in the above class   
        public orderItemWrapper(Order_Item__c xOI){
            isSelected = false;
            OI = xOI;
        }
     }

}