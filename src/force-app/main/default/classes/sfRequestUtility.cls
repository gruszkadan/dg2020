public class sfRequestUtility {
    
    
    public static void latestNote(Salesforce_Request_Note__c[] newSfNotes){
        
        //Create Set of Salesforce Update Records 
        Set<ID> sfrIds = new Set<ID>();
        
        for(Salesforce_Request_Note__c s : newSfNotes){   
            sfrIds.add(s.Salesforce_Update__c);
        }
        //Query List of IDs that match the set off Salesforce Update IDs
        List<Salesforce_Request__c> sfrs = [SELECT id, Latest_Note__c FROM Salesforce_Request__c WHERE ID IN :sfrIds];
        
        //If a Salesforce Request ID = Salesforce Update, set the Note comment equal to the Latest Note of Salesforce Request
        for(Salesforce_Request__c r : sfrs){
            for(Salesforce_Request_Note__c n : newSfNotes){
                if(r.ID == n.Salesforce_Update__c){
                    r.Latest_Note__c = n.Comments__c;
                }
            }
        }
        try {
            update sfrs;
        } catch(exception e) {
            salesforceLog.createLog('Salesforce Request', true, 'sfRequestUtility', 'latestNote', string.valueOf(e));
        }
        
    }
    
    
}