public class VPMTaskUtility {

    public static void VPMTaskCreation(VPM_Note__c[] notes){
               
        //account have multiple project? so no querying account
        Id[] relatedMilestone = new List<Id>();
        for(VPM_Note__c Note : notes){
            relatedMilestone.add(Note.VPM_Milestone__c);
        }   
        
        VPM_milestone__c[] relatedMilestones = new List<VPM_milestone__c>();
        relatedMilestones = [Select Id, Account__c from VPM_Milestone__c where Id IN :relatedMilestone];
        
        Map<VPM_Note__c, VPM_Milestone__c> getMilestones = new Map<VPM_Note__c, VPM_Milestone__c>();
        for(VPM_Note__c n : Notes){
            for(VPM_milestone__c mlstone : relatedMilestones){
                if(mlstone.id == n.VPM_Milestone__c){
                    getMilestones.put(n, mlstone);
                }
            }
        }
        
        VPM_Task__c[] newTasks = new List<VPM_Task__c>();
        
        for(VPM_Note__c n : notes){
            VPM_Task__c tsk = new VPM_Task__c();
            tsk.VPM_Milestone__c = n.VPM_Milestone__c;
            tsk.VPM_Project__c = n.VPM_Project__c;
            tsk.Subject__c = n.Type__c;
            tsk.Start_Date__c = date.valueOf(n.CreatedDate);
            //tsk.End_Date__c = date.valueOf(n.CreatedDate);
            tsk.OwnerId = n.CreatedById;       
            tsk.Contact__c = n.Contact__c;
            tsk.Account__c = getMilestones.get(n).Account__c;
            tsk.Comments__c = n.Comments__c;
            tsk.Status__c = 'Complete';
            tsk.Case_Note__c = n.Case_Note__c;  
            newTasks.add(tsk);
        }
        
        try{
            insert newTasks;
        }catch(exception e){
            salesforceLog.createLog('VPM_Note__c', true, 'projectNoteUtility', 'taskCreation', +string.valueOf(e));
        }
    }                
    
    //after update - find cases assocciated with each note and create a task
    public static void afterUpdateVPMTaskCreation(Case_Notes__c[] caseNotes){
 
        VPM_Note__c[] relatedNotes = new List<VPM_Note__c>();
        relatedNotes = [Select Type__c, Call_Duration__c, Comments__c, Case_Note__c, VPM_Milestone__c, VPM_Project__c, CreatedDate, CreatedById from VPM_Note__c where Case_Note__c IN :caseNotes];
        
        VPM_Task__c[] newTasks = new List<VPM_Task__c>();
        
        for(Case_Notes__c cN : caseNotes){
            for(VPM_Note__c note : relatedNotes){
                if(note.Case_Note__c == cN.Id){
                    VPM_Task__c tsk = new VPM_Task__c();
                    tsk.VPM_Milestone__c = note.VPM_Milestone__c;
                    tsk.VPM_Project__c = note.VPM_Project__c;
                    tsk.Subject__c = cN.Notes_Type__c;
                    tsk.Start_Date__c = date.valueOf(note.CreatedDate);
                    //Set end date since status is  complete
                    //tsk.End_Date__c = date.valueOf(note.CreatedDate);
                    tsk.OwnerId = note.CreatedById;       
                    tsk.Contact__c = cN.Case_Contact_Id__c;
                    tsk.Account__c = cN.Account_Id__c;
                    tsk.Comments__c = cN.Issue__c;
                    tsk.Status__c = 'Complete';
                    tsk.Case_Note__c = cN.Id;  
                    newTasks.add(tsk); 
                }
            }
        }      

        //after inserting task - trigger will associate note with task
        try{
            insert newTasks;
        }catch(exception e){
            salesforceLog.createLog('Case Note', true, 'caseNoteUtility', 'afterUpdateTaskCreation', +string.valueOf(e));
        }       
    }
    
    
    public static void afterUpdateVPMTaskRemoval(Case_Notes__c[] caseNotes){
   
        VPM_Task__c[] relatedTasks = new List<VPM_Task__c>();
        relatedTasks = [Select id from VPM_Task__c where Case_Note__c IN :caseNotes];
        
        try{
            delete relatedTasks;
        }catch(exception e){
            salesforceLog.createLog('Case Note', true, 'caseNoteUtility', 'afterUpdateTaskRemoval', +string.valueOf(e));
        }
    }
    
    
    //only applies to webinar - training to training or vice versa
    public static void afterUpdateVPMTaskUpdate(Case_Notes__c[] caseNotes){
        VPM_Task__c[] relatedTasks = new List<VPM_Task__c>();
        relatedTasks = [Select id, Case_Note__c, Subject__c, Comments__c from VPM_Task__c where Case_Note__c IN :caseNotes];
        
        VPM_Task__c[] tasksToUpdate = new List<VPM_Task__c>();
        for(Case_Notes__c cN : caseNotes){
            for(VPM_Task__c t : relatedTasks){
                if(t.Case_Note__c == cN.Id){
                    t.Subject__c = cN.Notes_Type__c;
                    t.Comments__c = cN.Issue__c;
                    tasksToUpdate.add(t);
                }
            }
        }
        
        try{
            update tasksToUpdate;
        }catch(exception e){
            salesforceLog.createLog('Case Note', true, 'caseNoteUtility', 'afterUpdateTaskUpdate', +string.valueOf(e));
        }
    }
    
    
    public static void updateVPMTaskEndDate(VPM_Task__c[] tasks){
        for(VPM_Task__c t: tasks){
            if(t.Status__c != 'Complete'){
                t.End_Date__c = NULL;
            }
            if(t.Status__c == 'Complete'){
                t.End_Date__c = System.today();
            }
        }
    }
    
    //Method to calculate upgrade email dates for VPM Task

    public static VPM_Task__c upgradeEmailVpmTask(VPM_Task__c vpmTask, VPM_Project__c vpmProject){
        
        if(vpmTask.Email_Sequence_Number__c == 1 && vpmProject.Scheduled_Upgrade_Date__c != null){
            vpmTask.Start_Date__c = vpmProject.Scheduled_Upgrade_Date__c.addDays(-30);
        }
        else if(vpmTask.Email_Sequence_Number__c == 2 && vpmProject.Scheduled_Upgrade_Date__c != null){
            vpmTask.Start_Date__c = vpmProject.Scheduled_Upgrade_Date__c.addDays(-7);
        }
        else if(vpmTask.Email_Sequence_Number__c == 3 && vpmProject.Actual_Upgrade_Date__c != null){
            vpmTask.Start_Date__c = vpmProject.Actual_Upgrade_Date__c.addDays(1);
        }
        else if(vpmTask.Email_Sequence_Number__c == 3 && vpmProject.Scheduled_Upgrade_Date__c != null && vpmProject.Actual_Upgrade_Date__c == null){
            vpmTask.Start_Date__c = vpmProject.Scheduled_Upgrade_Date__c.addDays(1);
        }
        else if(vpmTask.Email_Sequence_Number__c != null && vpmProject.Scheduled_Upgrade_Date__c == null){
            vpmTask.Start_Date__c = null;
        }
        return vpmTask;
    }
   
    public static void upgradeEmailVpmProjects(VPM_Project__c[] vProjects){
        
        VPM_Task__c[] vtasks = new list<VPM_Task__c>();
        //querying vpm projects and related tasks
        VPM_Project__c[] vpmProjects = [select id, Scheduled_Upgrade_Date__c, Actual_Upgrade_Date__c, (select Id, Email_Sequence_Number__c, End_Date__c from VPM_Tasks__r) from VPM_Project__c where Id IN :vProjects];
        for(VPM_Project__c v: vpmProjects){
            for(VPM_Task__c t: v.VPM_Tasks__r ){
                if(t.Email_Sequence_Number__c != null && t.End_Date__c == null){
                    t = upgradeEmailVpmTask(t, v);
                    vtasks.add(t);
                }
            }
        }
       update vtasks;
    }    
    
    //Method to set contact email sent dates on task end dates
    public static void updateTaskEndDate(Contact[] contacts){
        List<VPM_Task__c> Tasks = new List<VPM_Task__c>();
        VPM_Task__c[] T = [Select Id, Contact__c, Email_Sequence_Number__c, End_Date__c from VPM_Task__c where Email_Sequence_Number__c != null AND End_Date__c = null AND Status__c != 'Complete' AND Contact__c IN :contacts];
        for(Contact c :contacts){
            for(VPM_Task__c vTasks :T){
                if(c.Id == vTasks.Contact__c){
                    if(c.Upgrade_Email_1_Sent_Date__c != null && vTasks.Email_Sequence_Number__c == 1){
                        vTasks.End_Date__c = c.Upgrade_Email_1_Sent_Date__c;
                        vTasks.Status__c = 'Complete';
                    }
                    if(c.Upgrade_Email_2_Sent_Date__c != null && vTasks.Email_Sequence_Number__c == 2){
                        vTasks.End_Date__c = c.Upgrade_Email_2_Sent_Date__c;
                        vTasks.Status__c = 'Complete';
                    }
                    if(c.Upgrade_Email_3_Sent_Date__c != null && vTasks.Email_Sequence_Number__c == 3){
                        vTasks.End_Date__c = c.Upgrade_Email_3_Sent_Date__c;
                        vTasks.Status__c = 'Complete';
                    }
                }
                Tasks.add(vTasks);
            }
        }
        update Tasks;
    }
    
}