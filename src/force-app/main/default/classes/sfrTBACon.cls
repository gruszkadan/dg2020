public class sfrTBACon {
    public User u {get;set;}
    public request[] queue_reqs {get;set;}
    public request[] dan_reqs {get;set;}
    public request[] mark_reqs {get;set;}
    public request[] mac_reqs {get;set;} 
    public request[] doug_reqs {get;set;}
    public request[] tara_reqs {get;set;}    
    public request[] priyanka_reqs {get;set;}    
    public string queueId {get;set;}
    public string projectsId {get;set;}
    public string enhancementsId {get;set;}
    public string danId {get;set;}
    public string markId {get;set;}
    public string macId {get;set;}
    public string dougId {get;set;}
    public string taraId {get;set;}
    public string priyankaId {get;set;}
    public string danIcon {get;set;}
    public string markIcon {get;set;}
    public string macIcon {get;set;}
    public string dougIcon {get;set;}
    public string taraIcon {get;set;}
    public string priyankaIcon {get;set;}
    Set<string> ownerIds = new Set<string>();
    public string selId {get;set;}
    public Salesforce_Request__c selReq {get;set;}
    public string selReqType {get;set;}
    public string selProjSize {get;set;}
    // buttons
    public boolean viewRequestsTBA {get;set;}
    public boolean viewEnhancements {get;set;}
    public boolean viewProjects {get;set;}
    public boolean viewDepartmentManagement {get;set;}
    public boolean viewStatusWall {get;set;}
    public boolean viewAll {get;set;}
    
    public sfrTBACon() {
        u = [SELECT id, Name, FirstName, LastName, Email, Department__c, Role__c, ProfileId, Profile.Name 
             FROM User 
             WHERE id = :UserInfo.getUserId() LIMIT 1];
        viewButtons();
        queueId = [SELECT Queueid FROM QueueSObject WHERE Queue.Name = 'SF Request Queue' LIMIT 1].Queueid;
        projectsId = [SELECT Queueid FROM QueueSObject WHERE Queue.Name = 'Projects' LIMIT 1].Queueid;
        enhancementsId = [SELECT Queueid FROM QueueSObject WHERE Queue.Name = 'Enhancements' LIMIT 1].Queueid;
        danId = [SELECT id FROM User WHERE Name = 'Daniel Gruszka' LIMIT 1].id;
        markId = [SELECT id FROM User WHERE Name = 'Mark McCauley' LIMIT 1].id;
        macId = [SELECT id FROM User WHERE Name = 'Mac Nosek' LIMIT 1].id;
        dougId = [SELECT id FROM User WHERE Name = 'Doug Devine' LIMIT 1].id;
        taraId = [SELECT id FROM User WHERE Name = 'Tara Wills' LIMIT 1].id;
        priyankaId = [SELECT id FROM User WHERE Name = 'Priyanka Goriparthi' LIMIT 1].id;
        selReqType = 'Unassigned';
        selProjSize = 'Unassigned';
        findIcons();
        find();
    }
    
    public PageReference saveSelReq() {
        if (selReq != null) {
            selReq.Request_Type__c = selReqType;
            selReq.Estimated_Project_Size__c = selProjSize;
            if (selReqType == 'Project') {
                selReq.Ownerid = projectsId;
            }
            if (selReqType == 'Enhancement') {
                selReq.Ownerid = enhancementsId;
            }
            update selReq;
        }
        PageReference pr = new PageReference('/apex/sfrTBA');
        return pr.setRedirect(true);
    }
    
    public void viewButtons() {
        viewRequestsTBA = Salesforce_Request_Settings__c.getInstance().View_Requests_TBA__c;
        viewEnhancements = Salesforce_Request_Settings__c.getInstance().View_Enhancements__c;
        viewProjects = Salesforce_Request_Settings__c.getInstance().View_Projects__c;
        viewDepartmentManagement = Salesforce_Request_Settings__c.getInstance().View_Department_Management__c;
        viewStatusWall = Salesforce_Request_Settings__c.getInstance().View_Status_Wall__c;
        if (viewRequestsTBA && viewEnhancements && viewProjects && viewDepartmentManagement && viewStatusWall) {
            viewAll = true;
        }
    }
    
    public PageReference gotoHome() {
        PageReference pr = new PageReference('/apex/sf_request_Tab');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoEnhancements() {
        PageReference pr = new PageReference('/apex/sfrEnhancements');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoProjects() {
        PageReference pr = new PageReference('/apex/sfrProjects');
        return pr.setRedirect(true);
    }
    
    public PageReference gotoDepartmentManagement() {
        string url = '/apex/sfrDepartmentManagement';
        string url_var = ApexPages.currentPage().getParameters().get('myParam');
        if (url_var != 'User') {
            url += '?dept='+url_var;
        }
        PageReference pr = new PageReference(url);
        return pr.setRedirect(true);
    }
    
    public PageReference gotoStatusWall() {
        PageReference pr = new PageReference('/apex/sfrStatusWall');
        return pr.setRedirect(true);
    }
    
    
    public void find() {
        initLists();
        Salesforce_Request__c[] reqs_queue = [SELECT id, Name, Ownerid, Request_Type__c, Summary__c, Status__c, Requested_Date__c, Requested_By__c, Priority__c, Sort_Order__c, Requested_By__r.Name, Actual_Minutes__c
                                              FROM Salesforce_Request__c 
                                              WHERE OwnerId = :queueId
                                              ORDER BY Requested_Date__c DESC];
        Salesforce_Request__c[] reqs_dan = [SELECT id, Name, Ownerid, Request_Type__c, Summary__c, Status__c, Requested_Date__c, Requested_By__c, Priority__c, Sort_Order__c, Requested_By__r.Name, Actual_Minutes__c
                                            FROM Salesforce_Request__c 
                                            WHERE Ownerid = :danId
                                            AND Status__c != 'Completed' 
                                            AND Status__c != 'Duplicate Request'
                                            AND Status__c != 'Cannot Add'
                                            AND Status__c != 'Chosen Not to Add'
                                            AND Status__c != 'Unable to Complete'
                                            AND Request_Type__c != 'Merge'
                                            AND Request_Type__c != 'Territory Enrichment'
                                            ORDER BY Requested_Date__c DESC];
        Salesforce_Request__c[] reqs_mark = [SELECT id, Name, Ownerid, Request_Type__c, Summary__c, Status__c, Requested_Date__c, Requested_By__c, Priority__c, Sort_Order__c, Requested_By__r.Name, Actual_Minutes__c
                                             FROM Salesforce_Request__c 
                                             WHERE Ownerid = :markId
                                             AND Status__c != 'Completed' 
                                             AND Status__c != 'Duplicate Request'
                                             AND Status__c != 'Cannot Add'
                                             AND Status__c != 'Chosen Not to Add'
                                             AND Status__c != 'Unable to Complete'
                                             AND Request_Type__c != 'Merge'
                                             AND Request_Type__c != 'Territory Enrichment'
                                             ORDER BY Requested_Date__c DESC];
        Salesforce_Request__c[] reqs_mac = [SELECT id, Name, Ownerid, Request_Type__c, Summary__c, Status__c, Requested_Date__c, Requested_By__c, Priority__c, Sort_Order__c, Requested_By__r.Name, Actual_Minutes__c
                                            FROM Salesforce_Request__c 
                                            WHERE Ownerid = :macId
                                            AND Status__c != 'Completed' 
                                            AND Status__c != 'Duplicate Request'
                                            AND Status__c != 'Cannot Add'
                                            AND Status__c != 'Chosen Not to Add'
                                            AND Status__c != 'Unable to Complete'
                                            AND Request_Type__c != 'Merge'
                                            AND Request_Type__c != 'Territory Enrichment'
                                            ORDER BY Requested_Date__c DESC];
        Salesforce_Request__c[] reqs_doug = [SELECT id, Name, Ownerid, Request_Type__c, Summary__c, Status__c, Requested_Date__c, Requested_By__c, Priority__c, Sort_Order__c, Requested_By__r.Name, Actual_Minutes__c
                                             FROM Salesforce_Request__c 
                                             WHERE Ownerid = :dougId
                                             AND Status__c != 'Completed' 
                                             AND Status__c != 'Duplicate Request'
                                             AND Status__c != 'Cannot Add'
                                             AND Status__c != 'Chosen Not to Add'
                                             AND Status__c != 'Unable to Complete'
                                             AND Request_Type__c != 'Merge'
                                             AND Request_Type__c != 'Territory Enrichment'
                                             ORDER BY Requested_Date__c DESC];
        Salesforce_Request__c[] reqs_tara = [SELECT id, Name, Ownerid, Request_Type__c, Summary__c, Status__c, Requested_Date__c, Requested_By__c, Priority__c, Sort_Order__c, Requested_By__r.Name, Actual_Minutes__c
                                             FROM Salesforce_Request__c 
                                             WHERE Ownerid = :taraId
                                             AND Status__c != 'Completed' 
                                             AND Status__c != 'Duplicate Request'
                                             AND Status__c != 'Cannot Add'
                                             AND Status__c != 'Chosen Not to Add'
                                             AND Status__c != 'Unable to Complete'
                                             AND Request_Type__c != 'Merge'
                                             AND Request_Type__c != 'Territory Enrichment'
                                             ORDER BY Requested_Date__c DESC];
        Salesforce_Request__c[] reqs_priyanka = [SELECT id, Name, Ownerid, Request_Type__c, Summary__c, Status__c, Requested_Date__c, Requested_By__c, Priority__c, Sort_Order__c, Requested_By__r.Name, Actual_Minutes__c
                                                 FROM Salesforce_Request__c 
                                                 WHERE Ownerid = :priyankaId
                                                 AND Status__c != 'Completed' 
                                                 AND Status__c != 'Duplicate Request'
                                                 AND Status__c != 'Cannot Add'
                                                 AND Status__c != 'Chosen Not to Add'
                                                 AND Status__c != 'Unable to Complete'
                                                 AND Request_Type__c != 'Merge'
                                                 AND Request_Type__c != 'Territory Enrichment'
                                                 ORDER BY Requested_Date__c DESC];
        for (Salesforce_Request__c sfr : reqs_queue) {
            request req = new request(sfr);
            queue_reqs.add(req);
        }
        for (Salesforce_Request__c sfr : reqs_dan) {
            request req = new request(sfr);
            dan_reqs.add(req);
        }
        for (Salesforce_Request__c sfr : reqs_mark) {
            request req = new request(sfr);
            mark_reqs.add(req);
        }
        for (Salesforce_Request__c sfr : reqs_mac) {
            request req = new request(sfr);
            mac_reqs.add(req);
        }
        for (Salesforce_Request__c sfr : reqs_doug) {
            request req = new request(sfr);
            doug_reqs.add(req);
        }
        for (Salesforce_Request__c sfr : reqs_tara) {
            request req = new request(sfr);
            tara_reqs.add(req);
        }
        for (Salesforce_Request__c sfr : reqs_priyanka) {
            request req = new request(sfr);
            priyanka_reqs.add(req);
        }
    }
    
    
    public class request {
        public Salesforce_Request__c sfr {get;set;}
        public string truncSum {get;set;}
        public string truncStatus {get;set;}
        public string color {get;set;}
        
        public request(Salesforce_Request__c req) {
            sfr = req;
            if (sfr.Summary__c != null) {
                truncSum = sfr.Summary__c.abbreviate(35);
            } else {
                truncSum = '';
            }
            if (sfr.Status__c == null) {
                truncStatus = 'None';
            } else {
                if (sfr.Status__c == 'Waiting on More Information') {
                    truncStatus = 'Waiting...';
                } else {
                    truncStatus = sfr.Status__c;
                }
            }
            color = '#D8DDE6';
            if (sfr.Request_Type__c == 'Enhancement') {
                color = '#34BECD';
            } 
            if (sfr.Request_Type__c == 'Project') {
                color = '#7F8DE1';
            } 
            if (sfr.Request_Type__c == 'Admin') {
                color = '#EF7EAD';
            } 
            if (sfr.Request_Type__c == 'Bug') {
                color = '#F88962';
            } 
        }
    }
    
    public void querySelReq() {
        string param_reqId = ApexPages.currentPage().getParameters().get('param_reqId');
        selReq = [SELECT id, Name, Summary__c, Description__c, Request_Type__c, Estimated_Project_Size__c
                  FROM Salesforce_Request__c 
                  WHERE id = :param_reqId LIMIT 1];
    }
    
    public void selectRequestType() {
        selReqType = ApexPages.currentPage().getParameters().get('param_sel_type');
    }
    
    public void selectProjectSize() {
        selProjSize = ApexPages.currentPage().getParameters().get('param_sel_projsize');
        
    }
    
    public string[] getRequestTypeVals() { 
        string[] vals = new List<string>();
        Schema.DescribeFieldResult fieldResult = Salesforce_Request__c.Request_Type__c.getDescribe();
        Schema.PicklistEntry[] picklistEntries = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry entry : picklistEntries ) {
            vals.add(entry.getLabel()); 
        }
        return vals;
    }
    
    public string[] getEstimatedProjectSizeVals() { 
        string[] vals = new List<string>();
        Schema.DescribeFieldResult fieldResult = Salesforce_Request__c.Estimated_Project_Size__c.getDescribe();
        Schema.PicklistEntry[] picklistEntries = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry entry : picklistEntries ) {
            vals.add(entry.getLabel()); 
        }
        return vals;
    }
    
    public void initLists() {
        queue_reqs = new List<request>();
        dan_reqs = new List<request>();
        mark_reqs = new List<request>();
        mac_reqs = new List<request>();
        doug_reqs = new list<request>();
        tara_reqs = new list<request>();
        priyanka_reqs = new list<request>();
        ownerIds.add(queueId);
        ownerIds.add(danId);
        ownerIds.add(markId);
        ownerIds.add(macId);
        ownerIds.add(dougId);
        ownerIds.add(taraId);
        ownerIds.add(priyankaId);
    }
    
    public PageReference view() {
        PageReference pr = new PageReference('/'+selId);
        return pr.setRedirect(true);
    }
    
    public void findIcons() {
        User[] users = [SELECT id, LastName, SmallPhotoURL 
                        FROM User 
                        WHERE Name = 'Mac Nosek' 
                        OR Name = 'Daniel Gruszka' 
                        OR Name = 'Mark McCauley'
                        OR Name = 'Doug Devine'
                        OR Name = 'Tara Wills'
                        OR Name = 'Priyanka Goriparthi'];
        for (User u : users) {
            if (u.LastName == 'Gruszka') {
                danIcon = u.SmallPhotoURL;
            }
            if (u.LastName == 'McCauley') {
                markIcon = u.SmallPhotoURL;
            }
            if (u.LastName == 'Nosek') {
                macIcon = u.SmallPhotoURL;
            }
            if (u.LastName == 'Devine') {
                dougIcon = u.SmallPhotoURL;
            }
            if (u.LastName == 'Wills') {
                taraIcon = u.SmallPhotoURL;
            }
            if (u.LastName == 'Goriparthi') {
                priyankaIcon = u.SmallPhotoURL;
            }
        }
    }
    
}