trigger leadTrigger on Lead (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    if (trigger.isBefore) {
        
        // BEFORE INSERT
        if (trigger.isInsert) {
            leadTriggerHandler.beforeInsert(trigger.new);
        }
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            leadTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
        
        // BEFORE DELETE
        if (trigger.isDelete) {
            leadTriggerHandler.beforeDelete(trigger.old, trigger.new);
        }
        
    }
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            leadTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            leadTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
        // AFTER DELETE
        if (trigger.isDelete) {
            leadTriggerHandler.afterDelete(trigger.old, trigger.new);
        }
        
        // AFTER UNDELETE
        if (trigger.isUndelete) {
            leadTriggerHandler.afterUndelete(trigger.old, trigger.new);
        }
        
    }
    
    dlrs.RollupService.triggerHandler();
    
}