@isTest(SeeAllData=true)
private class testPOIActionsToResolve {
    static testmethod void test1() {
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Customer_Status__c ='Active';
        insert newAccount;
        
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        insert newContact;
        
        Product_Of_Interest__c poi = new Product_Of_Interest__c();
        poi.Contact__c = newContact.id;
        poi.POI_Status_MSDS__c ='Open';
        poi.POI_Status_ERGO__c ='Open';
        poi.POI_Status_AUTH__c ='Open';
        poi.POI_Status_EHS__c ='Open';
        poi.POI_Status_ODT__c ='Open';
        poi.Product_Actions_To_Resolve_MSDS__c='Request Demo';
        insert poi;
        
        Product_Of_Interest_Action__c poia = new Product_Of_Interest_Action__c();
        poia.Product_Of_Interest__c = poi.id;
        poia.Action_ID__c = 'poia1';
        poia.Product__c = 'HQ';
        poia.Action__c = 'Request Demo';
        poia.Product_Suite__c = 'MSDS Management';
        poia.Session_Open__c = TRUE;
        poia.Session_Open_Date__c = date.today();
        poia.Session_Unique_ID__c = '12345672698';
        poia.Needs_Resolution__c = TRUE;
        insert poia;
        
        Product_Of_Interest_Action__c poia1 = new Product_Of_Interest_Action__c();
        poia1.Product_Of_Interest__c = poi.id;
        poia1.Action_ID__c = 'poia2';
        poia1.Product__c = 'Ergonomics';
        poia1.Action__c = 'Request Demo';
        poia1.Product_Suite__c = 'Ergonomics';
        poia1.Session_Open__c = TRUE;
        poia1.Session_Open_Date__c = date.today();
        poia1.Session_Unique_ID__c = '1234567269548';
        poia1.Needs_Resolution__c = TRUE;
        insert poia1;
        
        Product_Of_Interest_Action__c poia2 = new Product_Of_Interest_Action__c();
        poia2.Product_Of_Interest__c = poi.id;
        poia2.Action_ID__c = 'poia3';
        poia2.Product__c = 'MSDS Authoring';
        poia2.Action__c = 'Request Demo';
        poia2.Product_Suite__c = 'MSDS Authoring';
        poia2.Session_Open__c = TRUE;
        poia2.Session_Open_Date__c = date.today();
        poia2.Session_Unique_ID__c = '123456726967412';
        poia2.Needs_Resolution__c = TRUE;
        insert poia2;
        
        Product_Of_Interest_Action__c poia3 = new Product_Of_Interest_Action__c();
        poia3.Product_Of_Interest__c = poi.id;
        poia3.Action_ID__c = 'poia4';
        poia3.Product__c = 'EHS Management';
        poia3.Action__c = 'Request Demo';
        poia3.Product_Suite__c = 'EHS Management';
        poia3.Session_Open__c = TRUE;
        poia3.Session_Open_Date__c = date.today();
        poia3.Session_Unique_ID__c = '12345672696742';
        poia3.Needs_Resolution__c = TRUE;
        insert poia3;
        
        Product_Of_Interest_Action__c poia4 = new Product_Of_Interest_Action__c();
        poia4.Product_Of_Interest__c = poi.id;
        poia4.Action_ID__c = 'poia5';
        poia4.Product__c = 'On-Demand Training';
        poia4.Action__c = 'Request Demo';
        poia4.Product_Suite__c = 'On-Demand Training';
        poia4.Session_Open__c = TRUE;
        poia4.Session_Open_Date__c = date.today();
        poia4.Session_Unique_ID__c = '123456726962';
        poia4.Needs_Resolution__c = TRUE;
        insert poia4;
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
        poiActionsToResolve myController = new poiActionsToResolve();
        myController.sessions[0].getStatusVals();
        myController.sessions[0].getCanEdit();
        myController.sessions[0].newStatus='Resolved - Good Title, But Not the DM';
        myController.sessions[0].saveStatus();
        myController.sessions[1].getStatusVals();
        myController.sessions[1].getCanEdit();
        myController.sessions[1].newStatus='Resolved - Good Title, But Not the DM';
        myController.sessions[1].saveStatus();
        myController.sessions[2].getStatusVals();
        myController.sessions[2].getCanEdit();
        myController.sessions[2].newStatus='Resolved - Good Title, But Not the DM';
        myController.sessions[2].saveStatus();
        myController.sessions[3].getStatusVals();
        myController.sessions[3].getCanEdit();
        myController.sessions[3].newStatus='Resolved - Good Title, But Not the DM';
        myController.sessions[3].saveStatus();
        myController.sessions[4].getStatusVals();
        myController.sessions[4].getCanEdit();
        myController.sessions[4].newStatus='Resolved - Good Title, But Not the DM';
        myController.sessions[4].saveStatus();
        myController.viewInactive();
        Test.stopTest();        
    }
    static testmethod void test2() {
        User u =[Select ID from User where LastName='McCauley' and isActive = TRUE limit 1];
        system.runAs(u){
            Account newAccount = new Account();
            newAccount.Name = 'Test Account';
            newAccount.Customer_Status__c ='Active';
            insert newAccount;
            
            Contact newContact = new Contact();
            newContact.FirstName = 'Bob';
            newContact.LastName = 'Bob';
            newContact.AccountID = newAccount.Id;
            newContact.POI_Status_MSDS__c ='Working - Connected';
            newContact.POI_Status_ERGO__c ='Working - Pending Response';
            insert newContact;
            
            Product_Of_Interest__c poi = new Product_Of_Interest__c();
            poi.Contact__c = newContact.id;
            poi.POI_Status_MSDS__c ='Working - Connected';
            poi.POI_Status_ERGO__c ='Working - Pending Response';
            insert poi;
            
            Product_Of_Interest_Action__c poia = new Product_Of_Interest_Action__c();
            poia.Product_Of_Interest__c = poi.id;
            poia.Action_ID__c = 'poia1';
            poia.Product__c = 'HQ';
            poia.Action_Status__c ='Working - Connected';
            poia.Action__c = 'Request Demo';
            poia.Product_Suite__c = 'MSDS Management';
            poia.Session_Open__c = TRUE;
            poia.Session_Open_Date__c = date.today();
            poia.Session_Unique_ID__c = '12345672698';
            poia.Needs_Resolution__c = TRUE;
            insert poia;
            
            Product_Of_Interest_Action__c poia1 = new Product_Of_Interest_Action__c();
            poia1.Product_Of_Interest__c = poi.id;
            poia1.Action_ID__c = 'poia2';
            poia1.Product__c = 'Ergonomics';
            poia1.Action_Status__c ='Working - Pending Response';
            poia1.Action__c = 'Request Demo';
            poia1.Product_Suite__c = 'Ergonomics';
            poia1.Session_Open__c = TRUE;
            poia1.Session_Open_Date__c = date.today();
            poia1.Session_Unique_ID__c = '1234567269548';
            poia1.Needs_Resolution__c = TRUE;
            insert poia1;
            
            Test.startTest();
            ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
            poiArchivedActionsToResolveController myController1 = new poiArchivedActionsToResolveController();
            myController1.goBack();
            Test.stopTest();
        }
    }
    static testmethod void test3() {
        User u =[Select ID from User where LastName='McCauley' and isActive = TRUE limit 1];
        system.runAs(u){
            Account newAccount = new Account();
            newAccount.Name = 'Test Account';
            newAccount.Customer_Status__c ='Active';
            insert newAccount;
            
            Contact newContact = new Contact();
            newContact.FirstName = 'Bob';
            newContact.LastName = 'Bob';
            newContact.AccountID = newAccount.Id;
            newContact.POI_Status_MSDS__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_ERGO__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_AUTH__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_EHS__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_ODT__c ='Resolved - Good Title, But Not the DM';
            insert newContact;
            
            Product_Of_Interest__c poi = new Product_Of_Interest__c();
            poi.Contact__c = newContact.id;
            poi.POI_Status_MSDS__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_ERGO__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_AUTH__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_EHS__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_ODT__c ='Resolved - Good Title, But Not the DM';
            insert poi;
            
            
            Product_Of_Interest_Action__c poia2 = new Product_Of_Interest_Action__c();
            poia2.Product_Of_Interest__c = poi.id;
            poia2.Action_ID__c = 'poia3';
            poia2.Product__c = 'MSDS Authoring';
            poia2.Action__c = 'Request Demo';
            poia2.Action_Status__c ='Resolved - Good Title, But Not the DM';
            poia2.Product_Suite__c = 'MSDS Authoring';
            poia2.Session_Open__c = TRUE;
            poia2.Session_Open_Date__c = date.today();
            poia2.Session_Unique_ID__c = '123456726967412';
            poia2.Needs_Resolution__c = TRUE;
            poia2.Contact__c = newContact.id;
            insert poia2;
            
            Product_Of_Interest_Action__c poia3 = new Product_Of_Interest_Action__c();
            poia3.Product_Of_Interest__c = poi.id;
            poia3.Action_ID__c = 'poia4';
            poia3.Product__c = 'EHS Management';
            poia3.Action__c = 'Request Demo';
            poia3.Product_Suite__c = 'EHS Management';
            poia3.Action_Status__c ='Resolved - Good Title, But Not the DM';
            poia3.Session_Open__c = TRUE;
            poia3.Session_Open_Date__c = date.today();
            poia3.Session_Unique_ID__c = '12345672696742';
            poia3.Needs_Resolution__c = TRUE;
            poia3.Contact__c = newContact.id;
            insert poia3;
            
            Test.startTest();
            ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
            poiArchivedActionsToResolveController myController1 = new poiArchivedActionsToResolveController();
            Test.stopTest();
        }
    }
    static testmethod void test4() {
        User u =[Select ID from User where LastName='McCauley' and isActive = TRUE limit 1];
        system.runAs(u){
            Account newAccount = new Account();
            newAccount.Name = 'Test Account';
            newAccount.Customer_Status__c ='Active';
            insert newAccount;
            
            Contact newContact = new Contact();
            newContact.FirstName = 'Bob';
            newContact.LastName = 'Bob';
            newContact.AccountID = newAccount.Id;
            newContact.POI_Status_MSDS__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_ERGO__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_AUTH__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_EHS__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_ODT__c ='Resolved - Good Title, But Not the DM';
            insert newContact;
            
            Product_Of_Interest__c poi = new Product_Of_Interest__c();
            poi.Contact__c = newContact.id;
            poi.POI_Status_MSDS__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_ERGO__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_AUTH__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_EHS__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_ODT__c ='Resolved - Good Title, But Not the DM';
            insert poi;
            
            
            Product_Of_Interest_Action__c poia2 = new Product_Of_Interest_Action__c();
            poia2.Product_Of_Interest__c = poi.id;
            poia2.Action_ID__c = 'poia3';
            poia2.Product__c = 'MSDS Authoring';
            poia2.Action__c = 'Request Demo';
            poia2.Action_Status__c ='Resolved - Good Title, But Not the DM';
            poia2.Product_Suite__c = 'MSDS Authoring';
            poia2.Session_Open__c = TRUE;
            poia2.Session_Open_Date__c = date.today();
            poia2.Session_Unique_ID__c = '123456726967412';
            poia2.Needs_Resolution__c = TRUE;
            poia2.Contact__c = newContact.id;
            insert poia2;
            
            Test.startTest();
            ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
            poiArchivedActionsToResolveController myController1 = new poiArchivedActionsToResolveController();
            Test.stopTest();
        }
    }
    
    static testmethod void test5() {
        User u =[Select ID from User where LastName='McCauley' and isActive = TRUE limit 1];
        system.runAs(u){
            Account newAccount = new Account();
            newAccount.Name = 'Test Account';
            newAccount.Customer_Status__c ='Active';
            insert newAccount;
            
            Contact newContact = new Contact();
            newContact.FirstName = 'Bob';
            newContact.LastName = 'Bob';
            newContact.AccountID = newAccount.Id;
            newContact.POI_Status_MSDS__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_ERGO__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_AUTH__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_EHS__c ='Resolved - Good Title, But Not the DM';
            newContact.POI_Status_ODT__c ='Resolved - Good Title, But Not the DM';
            insert newContact;
            
            Product_Of_Interest__c poi = new Product_Of_Interest__c();
            poi.Contact__c = newContact.id;
            poi.POI_Status_MSDS__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_ERGO__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_AUTH__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_EHS__c ='Resolved - Good Title, But Not the DM';
            poi.POI_Status_ODT__c ='Resolved - Good Title, But Not the DM';
            insert poi;
            
            
            Product_Of_Interest_Action__c poia4 = new Product_Of_Interest_Action__c();
            poia4.Product_Of_Interest__c = poi.id;
            poia4.Action_ID__c = 'poia5';
            poia4.Product__c = 'On-Demand Training';
            poia4.Action__c = 'Request Demo';
            poia4.Product_Suite__c = 'On-Demand Training';
            poia4.Session_Open__c = TRUE;
            poia4.Session_Open_Date__c = date.today();
            poia4.Session_Unique_ID__c = '123456726962';
            poia4.Needs_Resolution__c = TRUE;
            poia4.Action_Status__c ='Resolved - Good Title, But Not the DM';
            poia4.Contact__c = newContact.id;
            insert poia4;
            
            Test.startTest();
            ApexPages.currentPage().getParameters().put('Id', newAccount.Id);
            poiArchivedActionsToResolveController myController1 = new poiArchivedActionsToResolveController();
            Test.stopTest();
        }
    }
}