@isTest(seeAllData=true)
private class testLeadCustomSettings {
    
    static testmethod void test1() {
        //init class
        leadCustomSettings lcs = new leadCustomSettings();
        
        //assert the boolean methods - all should be true for admins
        System.assert(lcs.canChangeAuthoringOwner() == true);
        System.assert(lcs.canChangeChemicalMgtOwner() == true);
        System.assert(lcs.canChangeEHSOwner() == true);
        System.assert(lcs.canChangeOnlineTrainingOwner() == true);
        System.assert(lcs.canChangeErgonomicsOwner() == true);
        System.assert(lcs.canChangeSecondaryLeadOwner() == true);
        
        //grab the default owners from the utility
        leadCustomSettings.defaultOwners def = lcs.findDefaultOwners();
        
        //assert default owners were found
        System.assert(def.defChemMgtOwner != null);
        System.assert(def.defAuthOwner != null);
        System.assert(def.defErgoOwner != null);
        System.assert(def.defOTOwner != null);
        System.assert(def.defEHSOwner != null);
        System.assert(def.entEHSOwner != null);
        System.assert(def.specialEHSOwner != null);
        System.assert(def.defESOwner != null);
        System.assert(def.allIds.split(';').size() == 9);
    }
    
}