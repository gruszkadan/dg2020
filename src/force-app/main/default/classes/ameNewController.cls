public class ameNewController {
    //Variables used in the controller 
    public Account_Management_Event__c ame {get;set;}
    public User User {get;set;}
    public User u {get;set;}
    public id rtRenew {get;set;}
    public id rtAtRisk {get;set;}
    public id rtRAM {get;set;}
    public id rtCancel {get;set;}
    public id rtVip {get;set;}
    public id rtCanAME {get;set;}
    public id ameId {get;set;}
    public list<Order_Item__c> oiList {get;set;}
    public Order_Item__c[] accountOrderItems {get;set;}
    public id accountId {get;set;}
    public boolean createQuoteOpportunity {get;set;}
    public orderItemWrapper[] orderItemWrappers {get;set;}
    Public list<Contact> ContactList {get;set;}    
    
    public ameNewController(ApexPages.StandardController stdController) {
        //Setting a variable for creation of the a new AME
        ame = new Account_Management_Event__c();
        //Setting up vairables to keep teck of the record type Id's
        rtRenew = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();  
        rtAtRisk = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('At Risk').getRecordTypeId();  
        rtRAM = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Reactive Account Management').getRecordTypeId();  
        rtCancel = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Cancellation').getRecordTypeId();  
        rtVip = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('VIP Account Management').getRecordTypeId(); 
        //Setting values to variables on page load
        ame.RecordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
        ame.Related_AME__c = ApexPages.currentPage().getParameters().get('Id');
        
        //The owner field will be populated with the person Id who creates the ame
        ame.ownerId = UserInfo.getUserId();


        
        //Holds account Id from the account the user is creating AME
        //Being used to redirect user when they press cancel button 
        accountId = ApexPages.currentPage().getParameters().get('parentId');
        //Taking the parent id parameter and setting the account field 
        ame.Account__c = accountId;
        
        //Checking to see if the account field has a value and the related ame field dosen't have a value 
        //Calling the default primary account  method 
        //Then we select the contact which is the primary admin on the account to populate on chnage
        if(ame.Account__c != null && ame.Related_AME__c  == null){
            InitializeData();
        }
        
        //If the related ame field has a value 
        //Query the related ame and pull back account, contact and the contact name
        //The contactSelected vairable will hold the related ame contact name 
        //the account field will be populated with the account we queried 
        else if(ame.Related_AME__c !=  null){
            
            Account_Management_Event__c RelatedAME =[Select contact__c, account__c
                                                     from Account_Management_Event__c
                                                     where id = :ame.Related_AME__c];
            
            ame.Account__c = RelatedAME.Account__c;
            ame.Contact__c = RelatedAME.Contact__c;
        } 
        //set the following fields at page load
        ame.Price_Difference__c = 0;
        ame.Status__c = 'Active';
        
        
        //Query for current user info 
        u = [Select id, Name, Group__c from User where id =: UserInfo.getUserId()];
        
        //set fields on page load
        if(u.Group__c  != 'Retention'){
            //Query for Retention Manager Id
            User = [SELECT Id from User where UserRole.Name = 'Manager of Retention'];
            ame.RecordTypeId = rtCancel;
            ame.Status__c = 'Active';
            ame.OwnerId = User.Id;
        }
        

        
        //Loading the wrappers on the order items and defaulting primary admin when account changes 
        InitializeData();
    }
    //Select list option for status on record types
    //Creating list with picklist options available
    //If statment to  display the closed options on the record types besided the renewal record type 
    public list<SelectOption> getStatus(){
        list<SelectOption> options = new list<SelectOption>();
        options.add(new SelectOption('Active','Active'));
        if(ame.RecordTypeId != rtRenew){
            options.add(new SelectOption('Closed','Closed'));
        }
        return options;
    }
    
    public List<SelectOption> getContactSelectList() {
        queryContacts();     
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- Select Contact --',false));        
        for (Contact contact : contactList) {
            options.add(new SelectOption(contact.ID, contact.Name));
        }
        return options;
    }
    //In this method we are defining a variable which will hold the result of our query 
    //We are querying fields on the contact object that will have matching account id and the ame account id 
    public void queryContacts(){
        
        contactList = [Select id, name, primary_admin__c from contact
                       where Accountid =: ame.account__c
                       order by name ASC];
    } 
    
    //Save method for cancellation recordtype on new ame
    public pageReference cancelSave(){
        if(ame.RecordTypeId == rtCancel){
            Order_Item__c[] selOrderItems = new list<Order_Item__c>();
            for (orderItemWrapper i : orderItemWrappers){
                if(i.isSelected == true){
                    selOrderItems.add(i.orderItem);
                }
            }
            if(ameUtility.canSave(ame) && selOrderItems.size() > 0){            
                insert ame;
                ameUtility.canAME(ame.Id, selOrderItems);
                return new PageReference('/'+ame.id);
            }else{
                apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR,'Please Fill In All Required Fields & Select at least one Product to Cancel'));
                return null;
            }
        }else{
            return null;
        }
    }
    
    //This method is used to save the ame_new page
    public PageReference Save(){
        
        //User can save if they have selected a record type 
        if(ameUtility.canSave(ame)){
            
            //Checking if the create quote and opportunity field is checked 
            if (createQuoteOpportunity == true){
                //Creating a list of order items that will be part of the wrappers   
                Order_Item__c[] selectedOrderItems = new list<Order_Item__c>();
                //Looping through our list if the user selects an item in the table 
                //Associate the work item with the current AME 
                for (orderItemWrapper w : orderItemWrappers){
                    if(w.isSelected == true){
                        //   w.orderItem.Account_Management_Event__c = ame.id;
                        selectedOrderItems.add(w.orderItem);
                    }
                }
                //If our list has anything in it we will update the work items that we are using in our wrappers 
                if(selectedOrderItems.size() > 0){
                    insert ame;
                    
                    for(Order_Item__c x : selectedOrderItems){
                        x.Account_Management_Event__c = ame.id;
                    }
                    update selectedOrderItems;
                    
                    //Auto create the quote and opp
                    quoteUtility.createQuoteFromAME(new list <Account_Management_Event__c>{ame});
                    return new PageReference('/'+ame.id);
                }else{
                    apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR,'Please Select an Order Item'));
                    return null;
                }
                
                
            }else {
                insert ame;
                return new PageReference('/'+ame.id);
            }
            
            //Error message for the save method         
        } else{
            apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR,'Please Fill In All Required Fields'));
            return null;
        }
    } 
    
    //We are creating a new list that will hold our wrappers but it will only display if theere was anything added to our list
    //Also, using this method to default primary admin when account changes 
    //We combined both methods to be able to only have 1 action support in the VF page 
    
    public void InitializeData(){
        //Method to default primary admin when account chnages 
        //Query contacts and if contact had primary admin checkbox checked we are populating the contact field with that value 
        queryContacts();
        for(Contact con : contactList){
            if(con.Primary_Admin__c == true){
                ame.Contact__c = con.id; 
            }
        }
        
        
        //Initialize new list that will store the order items 
        orderItemWrappers = new List<orderItemWrapper>();
        
        // Query account order items that will be displayed on the table in the Renewal Information section 
        accountOrderItems = [Select Id, Admin_Tool_Product_Name__c, Renewal_Amount__c, Term_Start_Date__c, Term_End_Date__c, Do_Not_Renew__c, Account__r.Channel__c, Cancelled_On_AME__c, Account_Management_Event__c
                             from Order_Item__c 
                             where Admin_Tool_Order_Status__c = 'A' and Term_Start_Date__c <= TODAY and Term_End_Date__c > TODAY and Renewal_Amount__c != NULL and Renewal_Amount__c > 0 and Product_Platform__c != null and Account__c =:ame.Account__c and  Product_Suite__c != 'EHS Management']; 
        
        
        //When the AccountOrderItems query is greater than 0 we are adding each product to the orderitem wrapper table 
        if(accountOrderItems.size() > 0){
            for(Order_Item__c oi:accountOrderItems){
                orderItemWrappers.add(new orderItemWrapper(oi));
            }
        }
        createQuoteOpportunity = false;
        
        if(ame.RecordTypeId == rtRenew){
            createQuoteOpportunity = true;
            ame.Count_in_Stats__c = true;
        }
        
        
    }
    
    //Vairables for the wrapper component that will be used to display the table on the visual force page 
    //Wrappers are used when grouping is needed. EX: we need to group order items and make sure is selected is checked    
    public class orderItemWrapper {
        public Order_Item__c orderItem {get;set;}
        public boolean isSelected {get;set;}
        
        //Setting the two variables we defined in the above class   
        public orderItemWrapper(Order_Item__c xOrderItem){
            orderItem = xOrderItem;
            isSelected = false;
        }
    }
    
    public void SetRenewalDate(){
        string OrderItem = system.currentpagereference().getparameters().get('OrderItem'); 
        for(Order_Item__c o : accountOrderItems){
            if(o.id == OrderItem){
                ame.Renewal_Date__c = o.Term_End_Date__c;
            }
        }
    }
}