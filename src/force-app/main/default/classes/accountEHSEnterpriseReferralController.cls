public class accountEHSEnterpriseReferralController {
    
    public Account_Referral__c ref {get;set;}
    public Account a {get;set;}
    public User u {get;set;}
    public ID refNot {get;set;}
    public User refName {get;set;}
    public id refId {get;set;}
    public id aId {get;set;}
    public boolean canSave {get;set;}
    
    public accountEHSEnterpriseReferralController() {
        refId = ApexPages.currentPage().getParameters().get('id');
        aId = ApexPages.currentPage().getParameters().get('aid');
        u = [SELECT id, Name, FirstName, Email, ManagerID, Manager.Email, Sales_Director__c, Department__c, Group__c, Role__c, ProfileID, Profile.Name FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
        //retrieve ID that has custom setting checked
        refNot = [SELECT SetupOwnerId FROM MSDS_Custom_Settings__c WHERE Include_in_EHS_ES_Referral_Notification__c = true LIMIT 1].SetupOwnerId; 
        //retrieve ID and Name (Displayed in Visualforce Page) of Sean Oswald
        refName = [Select ID, Name FROM User WHERE ID = :refNot]; 
        
        
        if (refId != null) {
            ref = [SELECT Id, Budget_Authority_Etc__c, Current_Management_Method_Notes__c, Lead_Type__c, Reason_for_Referral__c, Primary_Driver__c, Mid_Market_Determine__c
                   FROM Account_Referral__c 
                   WHERE id = :refId LIMIT 1];
        } else {
            ref = new Account_Referral__c();
        }
        if (aId != null) {
            a = [SELECT Id, Name, Territory__c FROM Account WHERE Id = :aId LIMIT 1];
        }
        
    }
    
    public SelectOption[] getLeadVals() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('Inbound Lead', 'Inbound Lead'));   
        ops.add(new SelectOption('Outbound Lead', 'Outbound Lead'));       
        return ops;
    }
    
    public SelectOption[] getContactSelectList() {
        SelectOption[] ops = new List<SelectOption>();
        ops.add(new SelectOption('', '-- Select Contact --'));        
        Contact[] contacts = [SELECT id, Name FROM Contact WHERE AccountId = :a.id ORDER BY Name ASC];
        for (Contact contact : contacts) {
            ops.add(new SelectOption(contact.id, contact.Name));
        }
        return ops;
    }
    
    public PageReference cancel() {
        PageReference pr = new PageReference('/' + a.id);
        return pr.setRedirect(true);
    }
    
    public pageReference saveRef() {
        saveCheck();
        PageReference pr = new PageReference('/apex/account_detail?id='+aId+'&ref=true');
        if (canSave == false) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill out all information before submitting'));
            return null;
        } else {
            if (refId == null) {
                ref.Account__c = a.Id;
                ref.Date__c = date.today();
                ref.Referrer__c = u.Id;
                ref.Type__c = 'EHS Enterprise';
                ref.Team_Member__c = refName.ID; //Make Sean Oswald/box checked
                try{
                    insert ref;
                    sendEmail();
                    return pr; 
                } catch (DMLException e){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'There was a problem inserting the referral'));
                    return null;
                }   
            } 
            else {
                try{
                    update ref;
                    return pr;
                } catch (DMLException e){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'There was a problem updating the referral'));
                    return null;
                }    
            }
        }
    }
    
    public void saveCheck() {
        canSave = true;
        if (ref.Lead_Type__c == null) {canSave = false;}
        if (ref.Reason_for_Referral__c == null) {canSave = false;}
        if (ref.Mid_Market_Determine__c == null) {canSave = false;}
        if (ref.Primary_Driver__c == null) {canSave = false;}
        if (ref.Budget_Authority_Etc__c == null) {canSave = false;}
        
        
    }
    
    public void sendEmail() {
        EmailTemplate refTemp = [SELECT id FROM EmailTemplate WHERE Name = 'Account Referral' LIMIT 1];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   		//Id of the contact/user e-mail being sent to. Sean Oswald
        mail.setTargetObjectId(refNot); 
        
        
        mail.setWhatId(ref.Id);
        mail.setTemplateId(refTemp.Id);
        mail.setSaveAsActivity(false);
        mail.setUseSignature(false);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }
    
    
}