global class knowledgeArticleSetAssignedTo {

    
    
    @InvocableMethod (label='FAQApprovalProcess')
    global static void setAssignedTo(List<List<String>> oneParameter) {
        // variable set in the flow: Knowledge_Flow_for_All_Types 
        //articleId = oneParameter[0][0];
        //currentApprover/new Assigned To' = oneParameter[0][1];   
       	
        KbManagement.PublishingService.assignDraftArticleTask(oneParameter[0][0], oneParameter[0][1], null, null, false);
       	
        
    }
}