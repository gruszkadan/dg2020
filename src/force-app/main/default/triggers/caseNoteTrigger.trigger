trigger caseNoteTrigger on Case_Notes__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    if (trigger.isBefore) {
        
        // BEFORE INSERT
        if (trigger.isInsert) {
            caseNoteTriggerHandler.beforeInsert(trigger.new);
        }
        
        // BEFORE UPDATE
        if (trigger.isUpdate) {
            caseNoteTriggerHandler.beforeUpdate(trigger.old, trigger.new);
        }
        
        // BEFORE DELETE
        if (trigger.isDelete) {
            caseNoteTriggerHandler.beforeDelete(trigger.old, trigger.new);
        }
        
    }
    
    if (trigger.isAfter) {
        
        // AFTER INSERT
        if (trigger.isInsert) {
            caseNoteTriggerHandler.afterInsert(trigger.old, trigger.new);
        }
        
        // AFTER UPDATE
        if (trigger.isUpdate) {
            caseNoteTriggerHandler.afterUpdate(trigger.old, trigger.new);
        }
        
        // AFTER DELETE
        if (trigger.isDelete) {
            caseNoteTriggerHandler.afterDelete(trigger.old);
        }
        
    }
    
    // if(!test.isRunningTest()){
    // dlrs.RollupService.triggerHandler(); 
    //}
    
    
}