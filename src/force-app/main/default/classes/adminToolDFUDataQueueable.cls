public class adminToolDFUDataQueueable implements Queueable, Database.AllowsCallouts {
/**
* This is called from the adminToolDFUDataAPISend Scheduled Class
* This calls the Admin Tool Web Services to send DFU Data to the Admin Tool
**/
    public void execute(QueueableContext context) {
        String salesforceEnvironment;
        String adminToolEnvironment;
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D0v000000DvZCEAM'){
            salesforceEnvironment = 'Production';
            adminToolEnvironment = 'Production';
        }else{
            salesforceEnvironment ='Sandbox';
            adminToolEnvironment = 'Staging';
        }
        adminToolUtility atUtility = new adminToolUtility();
        Case[] dfuExport = atUtility.getdfuExport();      
        adminToolWebServices atWS = new adminToolWebServices();
        atWS.sendAdminToolData('DFU Export', dfuExport, adminToolEnvironment);
    }
}