@isTest
private class testDemoScheduledOpportunityCloseDate {
    
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test1() {
        
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.AccountID = newAccount.Id;
        newOpportunity.Name = 'Test';
        newOpportunity.Opportunity_Type__c = 'New';
        newOpportunity.CloseDate = date.today(); 
        newOpportunity.StageName = 'System Demo';
        insert newOpportunity;
        
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new demoScheduledOpportunityCloseDate());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
    
}