@isTest(seeAllData=true)
private class testquoteUtility {
    
    static testmethod void test1() {

        Account a = new Account(Name='Test Account', Active_MSDS_Management_Products__c='GM');
        insert a;
        
        Contact c = new Contact(FirstName='Firstname', LastName='Lastname', AccountId=a.id);
        insert c;
        
        Quote_Promotion__c p = new Quote_Promotion__c(Name='Promo', Start_Date__c=date.today(), End_Date__c=date.today().addDays(90), Code__c='Promo');
        insert p;
        
        Quote_Promotion_Product__c qpp = new Quote_Promotion_Product__c(Quote_Promotion__c=p.id, Name__c='HQ Account', Product__c='01t80000002NON4', Discount__c=1, Discount_Method__c='Amount Off', 
                                                                        Year_1__c=true, Year_2__c=true, Year_3__c=true, Year_4__c=true, Year_5__c=true);
        insert qpp;
        
        Quote__c q = new Quote__c(Account__c=a.id, Contact__c=c.id, Promotion__c=p.id, Contract_Length__c='5 Years', Price_Book__c='01s300000001EAF');
        insert q;
        
        product2 prod = [SELECT id,Product_Types__c,Product_Types_Requiring_Approval__c FROM Product2 WHERE Name = 'Data Import'];
        prod.Product_Types__c = 'Incidents;Corrective Actions;Custom';
        prod.Product_Types_Requiring_Approval__c = 'Custom';
        update prod;
        
        Quote_Product__c[] qps = new list<Quote_Product__c>();
        for (integer i=0; i<11; i++) {
            Quote_Product__c qp = new Quote_Product__c();
            qp.Quote__c = q.id;
            qp.Quantity__c = 1;
            qp.Y1_Quote_Price__c = 1000; qp.Y1_List_Price__c = 1500; qp.Year_1__c = true; 
            qp.Y2_Quote_Price__c = 1000; qp.Y2_List_Price__c = 1500; qp.Year_2__c = true;
            qp.Y3_Quote_Price__c = 1000; qp.Y3_List_Price__c = 1500; qp.Year_3__c = true;
            qp.Y4_Quote_Price__c = 1000; qp.Y4_List_Price__c = 1500; qp.Year_4__c = true;
            qp.Y5_Quote_Price__c = 1000; qp.Y5_List_Price__c = 1500; qp.Year_5__c = true; 
            qp.Renewal_Amount__c = 500; qp.New_Revenue_Price__c = 250; qp.PI__c = 250;
            if (i==0) {	
                qp.Name__c = 'HQ Account'; qp.Product__c = '01t80000002NON4'; qp.PricebookEntryId__c = '01u80000006eap9';
                qp.Promo_Discount_Percent__c = 1; qp.Promo_Y1__c = true; qp.Promo_Y2__c = true; qp.Promo_Y3__c = true; qp.Promo_Y4__c = true; qp.Promo_Y5__c = true;
                qp.Y1_Quote_Price__c = 10;
                qp.Type__c = 'Renewal';
                qp.Renewal_Amount__c = 1;
            }
            if (i==1) {
                qp.Name__c = 'eBinder Valet'; qp.Product__c = '01t80000002NOOR'; qp.PricebookEntryId__c = '01u80000006eaqb';
                qp.Year_1__c = false;
                qp.Y2_Quote_Price__c = 10000.00;
                qp.Type__c = 'New';
            }
            if (i==2) {
                qp.Name__c = 'On-Demand Training - Instructor Role - Stand Alone'; qp.Product__c = '01t80000003kZ0m'; qp.PricebookEntryId__c = '01u80000009y06A';
                qp.Year_1__c = false; 
                qp.Year_2__c = false;
            }
            if (i==3) {
                qp.Name__c = 'Update Services - Ongoing Indexing'; qp.Product__c = '01t34000003vc2J'; qp.PricebookEntryId__c = '01u3400000DI4WK';
                qp.Year_1__c = false; 
                qp.Year_2__c = false;
                qp.Year_3__c = false;
            }
            if (i==4) {
                qp.Name__c = 'Custom Services Project'; qp.Product__c = '01t80000003RcTu'; qp.PricebookEntryId__c = '01u80000008yoj2';
            }
            if (i==5) {
                qp.Name__c = 'Addl Licensing Fees'; qp.Product__c = '01t80000002NONE'; qp.PricebookEntryId__c = '01u80000006eapJ'; qp.Type__c = 'New';
            }
            if (i==6) {
                qp.Name__c = 'Custom Label'; qp.Product__c = '01t80000002NPrl'; qp.PricebookEntryId__c = '01u80000006ekuO';
                qp.Type__c = 'Renewal';
                qp.Renewal_Amount__c = 2000.00;
            }
            if (i==7) {
                qp.Name__c = 'Webpliance'; qp.Product__c = '01t80000002NONi'; qp.PricebookID__c = '01u80000006eapn';
            }
            if (i==8) {
                qp.Name__c = 'PPI'; qp.Product__c = '01t80000003PMBN'; qp.PricebookID__c = '01u80000007Tlhq';
            }
            if (i==9) {
                qp.Name__c = 'Spill Center'; qp.Product__c = '01t80000002NOPF'; qp.PricebookID__c = '01u80000006earP';
            }
            if (i==10) {
                qp.Name__c = 'Data Import'; qp.Product__c = '01t340000041hq6'; qp.Product_Types__c = 'Incidents;Corrective Actions' ; qp.PricebookID__c = '01u80000006earP';
            }
            qps.add(qp);
        }
             
        Quote_Product__c qp2 = new Quote_Product__c();
        
        qp2.Name__c = 'Data Import'; qp2.Product__c = '01t340000041hq6'; qp2.Product_Types__c = 'Incidents;Corrective Actions' ; qp2.PricebookID__c = '01u80000006earP';
                    qp2.Renewal_Amount__c = 500; qp2.New_Revenue_Price__c = 250; qp2.PI__c = 250;

        qp2.Y1_Quote_Price__c = null; qp2.Y1_List_Price__c = null; qp2.Year_1__c = true; 
        qp2.Y2_Quote_Price__c = null; qp2.Y2_List_Price__c = null; qp2.Year_2__c = true;
        qp2.Y3_Quote_Price__c = null; qp2.Y3_List_Price__c = null; qp2.Year_3__c = true;
        qp2.Y4_Quote_Price__c = null; qp2.Y4_List_Price__c = null; qp2.Year_4__c = true;
        qp2.Y5_Quote_Price__c = null; qp2.Y5_List_Price__c = null; qp2.Year_5__c = true;
        qp2.Quote__c = q.id;
        qp2.Type__c = 'Renewal';
        

        insert qps;
        insert qp2;
        
        string addFields = 'CreatedBy.Department__c, CreatedBy.Group__c, CreatedBy.Role__c, CreatedBy.Product_Group__c, Product__r.Sales_Executive_Approval__c, Product__r.Online_Training_Approval__c, Product__r.Associate_Approval__c, '+
            'Product__r.Authoring_Sales_Executive_Approval__c, Product__r.Enterprise_Sales_Rep_Approval__c, Product__r.Senior_Sales_Executive_Approval__c, Product__r.Team_Leader_Approval__c, Product__r.Director_Approval__c, '+
            'Product__r.VP_Approval__c, Product__r.Sales_Product_Grouping__c, Product__r.EHS_Operations_Director_Approval__c, Quote__r.Account__r.Active_MSDS_Management_Products__c, Quote__r.Department__c, Product__r.ODT__c, Quote__r.Price_Book__c, Product__r.Family, Product__r.Product_Types__c, Product__r.Product_Types_Requiring_Approval__c ';
        qps = (Quote_Product__c[])new dynamicQuery().queryList('Quote_Product__c', addFields, 'Quote__c = \''+q.id+'\'', null, null);
        
        quoteUtility util = new quoteUtility(q,qps);
        
        for (Quote_Product__c qp : qps) {
            util.discountCheck(qp);
        }
        
        User retentionUser = [SELECT id, Group__c, Product_Group__c FROM User WHERE isActive = true AND Group__c = 'Retention' LIMIT 1];
        User salesUser = [SELECT id, Group__c, Product_Group__c FROM User WHERE isActive = true AND Group__c = 'Mid-Market' LIMIT 1];
        
        
        
        q.OwnerId = salesUser.id;
        update q;
        q = (Quote__c)new dynamicQuery().query('Quote__c', null, 'id = \''+q.id+'\'');
        util = new quoteUtility(q,qps);
        util.approvalCheck(qps, FALSE);
        for (Quote_Product__c qp : qps) {
            util.findApprovers(qp);
            util.findQuotedPrice(qp);
        }
        
        
        q.OwnerId = retentionUser.id;
        update q;
        q = (Quote__c)new dynamicQuery().query('Quote__c', null, 'id = \''+q.id+'\''); 
        util = new quoteUtility(q,qps);
        util.approvalCheck(qps, FALSE);
        for (Quote_Product__c qp : qps) {
            util.findApprovers(qp);util.findQuotedPrice(qp);
        }
        
        qps = util.processQuoteProductWrappers(qps);
    }

    @isTest(SeeAllData=true) static void testAutoQuoteOppCreation() {
        //Create Account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        //Create Contact
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Primary_Admin__c = true;
        insert newContact;
        
        //Renewal AME
        Account_Management_event__c ame = new Account_Management_event__c();
        ame.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        ame.Account__c = newAccount.id;
        ame.Contact__c = newContact.id;
        ame.Status__c = 'Not Yet Created';
        ame.Batch_Month__c	= 'Mar';   
        ame.Batch_Year__c = '2014';
        insert ame;

        //Create Order Item
        testDataUtility Util = new testDataUtility(); 
        order_item__c newOrderItem =  Util.createOrderItem('House Sale', 'T654321');
        newOrderItem.Product_Platform__c = 'MSDSonline';
        newOrderItem.Renewal_Amount__c = 100;
        newOrderItem.Account__c = newAccount.Id;
        newOrderItem.Account_Management_Event__c = ame.id;
        insert newOrderItem;
        
        ame.Status__c = 'Active';
        update ame;

        //Assert that the opp & quote was auto created
        Quote__c[] ameQuotes = [SELECT id FROM Quote__c WHERE Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameQuotes.size());

        Quote_Product__c[] ameQuoteProds = [SELECT id FROM Quote_Product__c WHERE Quote__r.Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameQuoteProds.size());
        
        Opportunity[] ameOpps = [SELECT id FROM Opportunity WHERE Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameOpps.size());

        OpportunityLineItem[] ameOLIs = [SELECT id FROM OpportunityLineItem WHERE Opportunity.Account_Management_Event__c =:ame.id];
        system.assertEquals(1, ameOLIs.size());
    }
    
    
    @isTest(SeeAllData=true) static void testRenewalOrderItems() {
        //Create Account
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        //Create Contact
        Contact newContact = new Contact();
        newContact.FirstName = 'Bob';
        newContact.LastName = 'Bob';
        newContact.AccountID = newAccount.Id;
        newContact.Primary_Admin__c = true;
        insert newContact;
        
        //Renewal AME
        Account_Management_event__c ame = new Account_Management_event__c();
        ame.RecordTypeId = Schema.SObjectType.Account_Management_Event__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        ame.Account__c = newAccount.id;
        ame.Contact__c = newContact.id;
        ame.Status__c = 'Not Yet Created';
        ame.Batch_Month__c	= 'Mar';   
        ame.Batch_Year__c = '2014';
        insert ame;
        
        //Create Order Item
        testDataUtility Util = new testDataUtility(); 
        order_item__c associatedOrderItem =  Util.createOrderItem('House Sale', 'T654321');
        associatedOrderItem.Product_Platform__c = 'MSDSonline';
        associatedOrderItem.Renewal_Amount__c = 100;
        associatedOrderItem.Account__c = newAccount.Id;
        associatedOrderItem.Cancelled_on_AME__c = ame.id;
        insert associatedOrderItem;

        //Create Order Item
        order_item__c newOrderItem =  Util.createOrderItem('House Sale', 'T654322');
        newOrderItem.Product_Platform__c = 'MSDSonline';
        newOrderItem.Renewal_Amount__c = 100;
        newOrderItem.Account__c = newAccount.Id;
        newOrderItem.Account_Management_Event__c = ame.id;
        insert newOrderItem;
        
        ame.Status__c = 'Active';
        update ame;
        
        quoteUtility qUtil = new quoteUtility(null,null);
		Order_item__c[] check = quoteUtility.nonRenewingOrderItems(ame.id);
        System.debug(check.size());
        
    }
    
    
}