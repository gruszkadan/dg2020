@isTest
private class testSfRequestTestScenario {
    
    private static testmethod void test1() {
        
        Salesforce_Request__c sfr = new Salesforce_Request__c();
        insert sfr;
        
        Salesforce_Request_Test_Scenario__c scen = new   Salesforce_Request_Test_Scenario__c(Salesforce_Request__c = sfr.id);
        insert scen;
        
        //Pass the extension controller/params
        ApexPages.currentPage().getParameters().put('scenid', scen.Id);
        ApexPages.currentPage().getParameters().put('Id', sfr.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Salesforce_Request_Test_Scenario__c());
        sfRequestTestScenario myController = new sfRequestTestScenario(testController);
        
        System.assert(myController.scenarioList.size() == 1, 'Expected 1 test scenario, found '+myController.scenarioList.size());
        
        //back button
        ApexPages.currentPage().getParameters().put('Id', sfr.Id);
        myController.backButton();
        
        //new button
        ApexPages.currentPage().getParameters().put('Id', sfr.Id);
        myController.newScenario();
        
        System.assert(myController.newScenario.Step__c == 2, 'Expected Step__c to be 2, found '+myController.newScenario.Step__c);
        
        //save button on the add_edit page
        myController.saveAddEdit();
        myController.newScenario = null;
        myController.saveAddEdit();
        
        //save button on the detail page
        myController.saveDetail();
        
        //save button on the reorder page
        myController.saveReorder();
        
        // finds the scenario details to pull back on test scenario detail page  
        ApexPages.currentPage().getParameters().put('scenarioId', scen.Id);
        myController.findScenDetail();
        
        System.assert(myController.scenarioDetail.id == scen.id, 'Expected id to be'+scen.id+', found '+myController.scenarioDetail.id);
    }
}