@isTest
public class testExcelSPSReporter {
    public static testmethod void test1() {
    User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
    System.runAs(u) {
        //Create test data
        testDataUtility util = new testDataUtility();
        
        User vp = util.createUser('TestVP', 'vp', null, null, null);
        insert vp;
        
        User dir = util.createUser('TestDIR', 'director', vp, vp, vp);
        insert dir;
        
        User mgr = util.createUser('TestMGR', 'manager', vp, dir, dir);
        insert mgr;
        
        User rep = util.createUser('TestREP', 'rep', vp, dir, mgr);
        insert rep;
        
        DateTime d = datetime.now();
        String monthName= d.format('MMMMM');
        System.debug(monthName);
        
        Sales_Performance_Summary__c vpSPS = new Sales_Performance_Summary__c();
        vpSPS.OwnerId = rep.Sales_VP__c;
        vpSPS.Month__c = monthName;
        vpSPS.Year__c = string.valueof(date.today().year());
        vpSPS.Role__c = 'VP';
        vpSPS.Sales_Team_Manager__c = rep.Sales_VP__c;
        vpSPS.Sales_Rep__c = rep.Sales_VP__c;
        insert vpSPS;
        
        Sales_Performance_Summary__c dirSPS = new Sales_Performance_Summary__c();
        dirSPS.OwnerId = rep.Sales_Director__c;
        dirSPS.Month__c = monthName;
        dirSPS.Year__c = string.valueof(date.today().year());
        dirSPS.Manager__c = rep.Sales_VP__c;
        dirSPS.Manager_Sales_Performance_Summary__c = vpSPS.id;
        dirSPS.Role__c = 'Director';
        dirSPS.Sales_Team_Manager__c = rep.Sales_Director__c;
        dirSPS.Sales_Rep__c = rep.Sales_Director__c;
        insert dirSPS;
        
        Sales_Performance_Summary__c mgrSPS = new Sales_Performance_Summary__c();
        mgrSPS.OwnerId = rep.Sales_Performance_Manager_ID__c;
        mgrSPS.Month__c = monthName;
        mgrSPS.Year__c = string.valueof(date.today().year());
        mgrSPS.Manager__c = rep.Sales_Director__c;
        mgrSPS.Manager_Sales_Performance_Summary__c = dirSPS.id;
        mgrSPS.Role__c = 'Sales Manager';
        mgrSPS.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
        mgrSPS.Sales_Rep__c = rep.Sales_Performance_Manager_ID__c;
        insert mgrSPS;
        
        Sales_Performance_Summary__c s = new Sales_Performance_Summary__c();
        s.OwnerId = rep.id;
        s.Month__c = monthName;
        s.Year__c = string.valueof(date.today().year());
        s.Manager__c = rep.Sales_Performance_Manager_ID__c;
        s.Manager_Sales_Performance_Summary__c = mgrSPS.id;
        s.Sales_Director__c = dir.id;
        s.Role__c = rep.Role__c;
        s.Sales_Team_Manager__c = rep.Sales_Performance_Manager_ID__c;
        s.Sales_Rep__c = rep.id; 
        insert s;
        
        
        
        ApexPages.currentPage().getParameters().put('month', string.valueof(date.today().month()));
        ApexPages.currentPage().getParameters().put('year', string.valueof(date.today().year()));
        
        
        excelSPSReporter con = new excelSPSReporter();
       
        System.debug(con.selectedMonth);
        //con.selectedYear = string.valueOf(Date.today().year());
        System.debug(con.selectedYear);
        
        System.debug(con.spsResults.size());
        System.debug(con.masterListdirector.size());
        System.assert(con.spsResults.size() > 0);
        System.assert(con.masterListdirector.size() > 0);
        
        
    }  
        
        
    }
    
}