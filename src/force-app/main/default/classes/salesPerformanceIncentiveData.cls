//** Test Class : testSalesPerformanceTable ** 
public class salesPerformanceIncentiveData {
    public string searchName {get;set;}
    public string groupRoleRepName {get;set;}
    public string sortFieldSave;
    string saveSearchName;
    boolean managerOnly;
    string query;
    string viewType;
    string groupRoleRepId;
    public string month {get;set;}
    string year; 
    public Incentive_Transaction__c[] incentives {get;set;}
    public incentiveWrapper[] incentiveWrappers {get;set;}
    public boolean showToolTipRow {get;set;}
    
    public salesPerformanceIncentiveData(string xviewType, string xgroupRoleRepId, string xgroupRoleRepName, string xmanagerOnly, string xmonth, string xyear) {
        
        incentiveWrappers = new list<incentiveWrapper>();
        viewType = xviewType; 
        groupRoleRepId = xgroupRoleRepId;
        groupRoleRepName = xgroupRoleRepName;
        if (xmanagerOnly == 'true') {
            managerOnly = true;
        } else {
            managerOnly = false;
        }
        month = xmonth;
        year = xyear;
        buildQuery();
        incentives = Database.query(query);
        //If incentives are found then create incentive wrappers
        if(incentives.size() > 0){
            set<id> orderIds = new set<id>();
            for(Incentive_Transaction__c it: incentives){
                if(it.Sales_Performance_Order__c != null){
                    orderIds.add(it.Sales_Performance_Order__c);
                }else{
                    String type = it.Incentive_Type__c;
                    if(type == 'Referral'){
                        type = type + ' - ' + it.Referral_Type__c;
                    }
                    incentiveWrappers.add(new incentiveWrapper(it.Account__r.Name, it.Account__c, null, null, it.Owner.Name, type, it.Incentive_Date__c, null, false, new Incentive_Transaction__c[]{it}));
                }
            }
            if(orderIds.size() > 0){
                for(id orderId: orderIds){
                    Incentive_Transaction__c[] orderIncentives = new list<Incentive_Transaction__c>();
                    String owner;
                    String type;
                    Date orderDate;
                    String acctName;
                    Decimal bookings;
                    String acctId;
                    String spoId;
                    String atOrderId;
                    Boolean dontCount;
                    for(Incentive_Transaction__c it: incentives){
                        if(it.Sales_Performance_Order__c == orderId){
                            orderIncentives.add(it);
                            owner = it.Sales_Performance_Order__r.Owner.Name;
                            if(type != 'Booking'){
                                type = it.Incentive_Type__c;
                            }
                            orderDate = it.Incentive_Date__c;
                            acctName = it.Sales_Performance_Order__r.Account__r.Name;
                            acctId = it.Sales_Performance_Order__r.Account__c;
                            spoId = it.Sales_Performance_Order__c;
                            bookings = it.Sales_Performance_Order__r.Booking_Amount__c;
                            atOrderId = it.Sales_Performance_Order__r.Order_Id__c;
                            dontCount = it.Dont_Count__c;
                        }
                    }
                    if(orderIncentives.size() > 0){
                        incentiveWrappers.add(new incentiveWrapper(acctName, acctId, atOrderId, spoId, owner, type, orderDate, bookings, dontCount, orderIncentives));
                    }
                }
            }
        }
    }
    
    public void buildQuery() {
        string groupRoleRepIdType = '';
        if (groupRoleRepId.left(3) == '005') {
            groupRoleRepIdType = 'Rep';
        } else {
            if (viewType == 'Group') {
                groupRoleRepIdType = 'Group';
            } else if (viewType == 'Role') { 
                groupRoleRepIdType = 'Role';
            }
        }
        query = 'SELECT id, OwnerId, Owner.Name, Sales_Performance_Order__r.Owner.Name, Sales_Performance_Order__r.Account__c, Sales_Performance_Order__r.Account__r.Name, ' +
            'Sales_Performance_Order__r.Order_ID__c, Sales_Performance_Order__r.Order_Date__c, Sales_Performance_Order__r.Booking_Amount__c, ' +
            'Sales_Performance_Order__r.Type__c, Incentive_Type__c , Incentive_Amount__c, Incentive_Date__c, Account__r.Name, Booking_Amount__c, Account__c, ' +
            'Referral_Type__c, Sales_Performance_Order__c, Dont_Count__c, CreatedBy.Name, CreatedDate, Adjustment_Reason__c ' +
            'FROM Incentive_Transaction__c ' +
            'WHERE Year__c = \'' + year + '\' ';
        if(month != null){
            query += 'AND Month_Name__c = \'' + month + '\' ';
        }
        if (groupRoleRepIdType == 'Rep') {
            if (!managerOnly) {
                if (viewType != 'My Team') {
                    query += 'AND OwnerId = \'' + groupRoleRepId + '\' ';
                } else {
                    query += 'AND (OwnerId = \'' + groupRoleRepId + '\' OR Sales_Manager__c = \'' + groupRoleRepId + '\' OR Sales_Director__c = \'' + groupRoleRepId + '\' OR Sales_VP__c = \'' + groupRoleRepId + '\') ';
                }
            } else {
                query += 'AND OwnerId = \'' + groupRoleRepId + '\' ';
            }
        } else if (groupRoleRepIdType == 'Group') {
            if (groupRoleRepId != 'Group') {
                query += 'AND Group__c = \'' + groupRoleRepId + '\' ';
            } else {
                query += 'AND Group__c != null ';
            }
        } else if (groupRoleRepIdType == 'Role') {
            if (groupRoleRepId != 'Role') {
                query += 'AND Role__c = \'' + groupRoleRepId + '\' ';
            } else {
                query += 'AND Role__c != null ';
            }
        }
        query += 'ORDER BY Incentive_Date__c asc ';
    }
    
    //Create incentive wrappers that will be displayed in the incentives tab
    public class incentiveWrapper{
        Public boolean dontCount {get;set;}
        Public String orderId {get;set;}
        Public String spoId {get;set;}
        Public String accountName {get;set;}
        Public String accountId {get;set;}
        Public String repName {get;set;}
        Public String incentiveType {get;set;}
        Public Date incentiveDate {get;set;}
        Public Decimal bookingAmount {get;set;}
        Public Decimal incentiveAmount {get;set;}
        Public Decimal itBookings {get;set;}
        Public List<Incentive_Transaction__c> adjustmentReasons {get;set;}
        public incentiveWrapper(String xAccountName, String xAccountId, String xOrderId, String xSpoId, String xRepName, String xIncentiveType, Date xIncentiveDate, Decimal xBookingAmount, boolean xDontCount, Incentive_Transaction__c[] incentives){
            orderId = xOrderId;  
            spoId = xSpoId;
            accountName = xAccountName;
            accountId = xAccountId;
            repName = xRepName;
            incentiveType = xIncentiveType;
            incentiveDate = xIncentiveDate;
            itBookings = 0;
            incentiveAmount = 0;
            adjustmentReasons = new List<Incentive_Transaction__c>();
            
            //calculate the total incentive amount
            for(Incentive_transaction__c it:incentives){
                incentiveAmount = incentiveAmount+it.Incentive_Amount__c;
                if(it.Adjustment_Reason__c != null){
                    adjustmentReasons.add(it); 
                }
            }
            
            //if booking or adjustment calculate the booking amount based on the incentive transactions
            if (incentiveType == 'Booking' || incentiveType == 'Adjustment'){
                for(Incentive_transaction__c it:incentives){
                    itBookings = itBookings+it.Booking_Amount__c;
                }
                bookingAmount = itBookings;
            }
            //else use the booking amount passed in
            else{
                bookingAmount = xBookingAmount;
            }
            dontCount = xDontCount;
        }
    }
    
}