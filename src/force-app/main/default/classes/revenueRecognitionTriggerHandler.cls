public class revenueRecognitionTriggerHandler {
    
    // AFTER INSERT
    public static void afterInsert(Revenue_Recognition__c[] oldRevRecs, Revenue_Recognition__c[] newRevRecs) {
        
    }
    
    // AFTER UPDATE
    public static void afterUpdate(Revenue_Recognition__c[] oldRevRecs, Revenue_Recognition__c[] newRevRecs) {
        Revenue_Recognition__c[] phaseAmountChanged = new List<Revenue_Recognition__c>();
        Revenue_Recognition__c[] projectStatusChanged = new List<Revenue_Recognition__c>();
        Revenue_Recognition__c[] projectStatusCancelled = new List<Revenue_Recognition__c>();
        
        for (integer i=0; i<newRevRecs.size(); i++) {
            if (newRevRecs[i].Phase_Amount__c != null && newRevRecs[i].Phase_Amount__c != oldRevRecs[i].Phase_Amount__c) {
                phaseAmountChanged.add(newRevRecs[i]);
            }
            if (newRevRecs[i].Project_Status__c != oldRevRecs[i].Project_Status__c && newRevRecs[i].Project_Status__c != NULL 
               && (oldRevRecs[i].Project_Status__c != NULL && newRevRecs[i].Project_Status__c != newRevRecs[i].Current_Case_Project_Status__c))
            {
                projectStatusChanged.add(newRevRecs[i]);
                if(newRevRecs[i].Project_Status__c.contains('Cancelled')){
                    projectStatusCancelled.add(newRevRecs[i]);
                }
            }
            
        }
        
        
        //If Project Status changes set recognized date
        if(projectStatusChanged.size() > 0){
            revenueRecognitionUtility.checkForSkippedStatus(projectStatusChanged);
            revenueRecognitionUtility.recognizeTransaction(projectStatusChanged);
        }
        
        //If Phase_Amount__c changes re-forecast unrecognized transactions
        if(phaseAmountChanged.size() > 0){
            revenueRecognitionUtility.updateForecast(phaseAmountChanged);
        }
        
        //If Project Status is cancelled re-forecast unrecognized transactions
        if(projectStatusCancelled.size() > 0){
            revenueRecognitionUtility.updateForecast(projectStatusCancelled);
        }
    }
    
}