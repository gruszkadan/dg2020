public class contractTriggerHandler {
    public static void afterUpdate(Contract__c[] oldContract, Contract__c[] newContract){
        
        set<id> ContactIds = new set<Id>();
        
        for (integer i=0; i<newContract.size(); i++) {
            
            if((oldContract[i].Num_of_EHS_Management_Products__c == null || oldContract[i].Num_of_EHS_Management_Products__c == 0) && newContract[i].Num_of_EHS_Management_Products__c >= 1 ){
                ContactIds.add(newContract[i].Contact__c );
            }
        }
        if(ContactIds.size() > 0){
            contactUtility.EHSContactCheck(ContactIds);
        }     
    }
}