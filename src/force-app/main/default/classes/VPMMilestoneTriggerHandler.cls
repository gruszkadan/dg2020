public class VPMMilestoneTriggerHandler {

    // BEFORE INSERT
    public static void beforeInsert(VPM_Milestone__c[] milestones){
       VPM_Milestone__c[] closedMilestones = new list<VPM_Milestone__c>();
        
        for (integer i=0; i<Milestones.size(); i++) {
            if(Milestones[i].Status__c == 'Closed'){
                closedMilestones.add(Milestones[i]);
            }
        }
        if(closedMilestones.size() > 0){
            VPMMilestoneUtility.updateMilestoneEndDate(closedMilestones);
        }    
    }
    
    // BEFORE UPDATE
    public static void beforeUpdate(VPM_Milestone__c[] oldMilestones, VPM_Milestone__c[] newMilestones) {
         //milestoneUtility.updateMilestoneStatus(newMilestones); 
        
        VPM_milestone__c[] changedStatus = new list<VPM_milestone__c>();
        
        for (integer i=0; i<newMilestones.size(); i++){
            if (newMilestones[i].Status__c != oldMilestones[i].Status__c){
                changedStatus.add(newMilestones[i]);
            }
        }
        if(changedStatus.size() > 0){
            VPMMilestoneUtility.updateMilestoneEndDate(changedStatus); 
        }
    }
    
    // BEFORE DELETE
    public static void beforeDelete(VPM_Milestone__c[] oldMilestones, VPM_Milestone__c[] newMilestones) {}
    
    // AFTER INSERT
    public static void afterInsert(VPM_Milestone__c[] oldMilestones, VPM_Milestone__c[] newMilestones){
              VPMProjectUtility.projectStatusEvaluation(newMilestones);      
    }

    
    // AFTER UPDATE
    public static void afterUpdate(VPM_Milestone__c[] oldMilestones, VPM_Milestone__c[] newMilestones) {

        System.debug('reached milestone trigger after update');
        
        VPM_Milestone__c[] milestonesStatusChanged = new list<VPM_Milestone__c>();
        
        for (integer i=0; i<newMilestones.size(); i++) {
            if(newMilestones[i].Status__c != oldMilestones[i].Status__c){
                milestonesStatusChanged.add(newMilestones[i]);
            }
        }
        
        if(milestonesStatusChanged.size() > 0){
            VPMProjectUtility.projectStatusEvaluation(milestonesStatusChanged);
        }    
   
    }

    // AFTER DELETE
    public static void afterDelete(VPM_Milestone__c[] milestones) {}
  
    
}