/*
 * Test class: testSalesPerformanceSummaryScheduler
 */
public class salesPerformanceSummaryScheduler implements Schedulable {    
    public void execute(SchedulableContext sc) {
        
        // Run step1 - this will create summaries based of ForecastingQuotas or update the quotas on summaries
        salesPerformanceSummaryBatchableStep1 step1 = new salesPerformanceSummaryBatchableStep1();
        Database.executeBatch(step1, 1);
       
    }
}