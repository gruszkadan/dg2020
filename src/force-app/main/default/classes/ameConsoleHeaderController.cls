public class ameConsoleHeaderController {
    public string viewAsName {get;set;}
    Public list<User> Users {get;set;}
    public string viewAs {get;set;}
    public string currentPage {get;set;}
    public string errorMsg {get;set;}
    public User u {get;set;}
    public string userID {get;set;}
    public string activeTab {get;set;}
    
    
    
    public ameConsoleHeaderController(){
        //  check to see if the va parameter does not eqaul nothing and is not null
        //  query to get user information where id is equal too the Va Id
        //  View as name will be used to displyed in the banner when viewing as another person
        if(ApexPages.currentPage().getParameters().get('va') != '' && ApexPages.currentPage().getParameters().get('va') != null &&  ApexPages.currentPage().getParameters().get('va') != userInfo.getUserId()){
            viewAsName = [Select id,name,email from User where id =:ApexPages.currentPage().getParameters().get('va') Limit 1].Name; 
        }   
        // query to pull back picture to be diplayed in header 
        u = [SELECT id, EmployeeNumber, FullPhotoURL FROM User WHERE id = :userInfo.getUserId() LIMIT 1];
    }
    
    // Method to find "View As" options 
    // Create list of Retention reps 
    // check if user has permission to view AME console 
    // Query active user that have a Retention role we will have two lsit one with Id's and the other with names 
    // This will populate the view as dropdown list
    public String[] getRetentionReps(){
        List<String> reps = new List<String>();
        users = new list <user>();
        
        
        for (User usr : [Select Id, Name From User 
                         where isActive = TRUE
                         and UserRole.Name LIKE '%Retention%'
                         ORDER BY Name ASC]) {
                             reps.add(usr.Name);
                             Users.add(usr);
                         }
       
        return reps;
    }
    //Method to select "View As" Rep
    //If users name are equal to the view as anem variable 
    //Create a new page refrence where we put the va= and the view as Id in the url
    //Page reference to redirect the user 
    //Display error message if user is not found in query 
    public PageReference viewAsRedirect(){
        for(User r:users){
            if(r.Name == viewAsName){
                viewAs = r.id;
                PageReference pr = new PageReference('/apex/'+currentPage);
                pr.getParameters().put('va', viewAs);
                
                 return pr.setRedirect(true);   
                
                
                
                
            }
        }
        errorMsg = 'Error: User Not Found';
        return null;
    }
    // page refrence to redirect user to current page when viewing as
    public PageReference resetView(){
        PageReference pr = new PageReference('/apex/'+currentPage);
        return pr.setRedirect(true);
    }
}