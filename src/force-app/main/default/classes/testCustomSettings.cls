@isTest(SeeAllData=true)
private class testCustomSettings {
    
    public static testmethod void RegularTests() {        
        
        Test.startTest();
        testMSDSCustomSettings1();
        testcontractcustomsettings1();
        Test.stopTest();
    }
    
    public static void testMSDSCustomSettings1() {
        
        msdsCustomSettings myController = new msdsCustomSettings();
        
        myController.editActiveCustomer();
        myController.showQuotes();
        myController.showContracts();
        myController.showSalesInsight();
        myController.editAccountOwnership();
        myController.defaultOpportunityAmount();
        myController.defaultLeadSource();
        myController.ViewLDR();
        myController.ViewSurveys();
        myController.stypes();
        myController.changeOfContact();
        myController.notificationGroups();
        myController.editSynopsis();
        myController.viewSynopsis();
        myController.editSynopsisMSDS();
        myController.viewSynopsisMSDS();
        myController.viewCustomerBillingRates();
        myController.editCustomerBillingRates();        
        myController.viewMonthlyReview();
        myController.cloneCases();
        myController.viewSales();
        myController.viewAccountLocator();
        myController.viewActivateProducts();
        myController.viewDataDotCom();
        myController.editCustomerSuccess();
        myController.editSAM();
        
    }
    
    public static void testContractCustomSettings1() {
        
        contractCustomSettings myController = new contractCustomSettings();
        
        myController.viewApprovalPage();
        myController.viewManualBuild();
        myController.viewOrdersButtons();
        
    }
}