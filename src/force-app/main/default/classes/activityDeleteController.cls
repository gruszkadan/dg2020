public class activityDeleteController {
    id activityId;
    boolean hasPermission;
    public string returnUrl {get;set;}
    public string activityType {get;set;}
    sObject activityToDelete;
    
    public activityDeleteController(ApexPages.StandardController stdController) {
        
        activityId = ApexPages.currentPage().getParameters().get('delID'); 
       	returnUrl = ApexPages.currentPage().getParameters().get('retURL');
        
        if (String.valueOf(activityId).startsWith('00T')){
            hasPermission = MSDS_Custom_Settings__c.getInstance().Can_Delete_Tasks__c;
            activityType = 'task';
            activityToDelete = [Select Id FROM Task WHERE id =:activityId LIMIT 1];
        }
        if (String.valueOf(activityId).startsWith('00U')){
            hasPermission = MSDS_Custom_Settings__c.getInstance().Can_Delete_Events__c;
            activityType = 'event';
            activityToDelete = [Select Id FROM Event WHERE id =:activityId LIMIT 1];
        }
    }
    
    public PageReference deleteRedirect() {
        if(hasPermission){
            delete activityToDelete;
            string goHere;
            if (returnUrl != null){
                goHere = returnUrl;   
            }
            else{
                goHere = '/home/home.jsp';
            }
            PageReference previousPage = new PageReference(goHere);
            previousPage.setRedirect(true);
            return previousPage;       
        }
        
        else{
            return null;
        }  
    }
    
    
    public PageReference backButton() {
        String goHere;
        if (returnUrl != null){
                goHere = returnUrl;   
            }
            else{
                goHere = '/home/home.jsp';
            }
        PageReference previousPage = new PageReference(goHere);
        previousPage.setRedirect(true);
        return previousPage;
    }
    
    
}