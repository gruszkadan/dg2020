public with sharing class renewal_new_opportunity {
    public Opportunity oppty;
    public Account currentAccount {get;set;}
    public Contact currentContact {get;set;}
    public Renewal__c currentRenewal {get;set;}
    public User u {get;set;}
    public string opptyName {get;set;}
    public Opportunity o {get;set;}
    public Contract__c contract {get;set;}
    public OpportunityContactRole opptyContact {get;set;}
    public CampaignMember[] allCampaigns {get;set;}
    public CampaignMember primaryCampaign {get;set;}
    public CampaignMember[] respondedCampaign {get;set;}
    public CampaignMember firstRespondedCampaign {get;set;}
    public Campaign unknownCampaign {get;set;}  
    Public CampaignMember[] allCampaigns_SelectOptions {get;set;}
    
    public renewal_new_opportunity(ApexPages.StandardController stdController) {
        Id opptyId = ApexPages.currentPage().getParameters().get('id');
        Id renewalID= System.currentPageReference().getParameters().get('renewal');
        Id accountID= System.currentPageReference().getParameters().get('accid');
        Id contactID= System.currentPageReference().getParameters().get('contactID');
        String leadSource= System.currentPageReference().getParameters().get('opp6');
        Id ameID = System.currentPageReference().getParameters().get('ameID');

        u = [Select id, Name, Department__c, Group__c, Suite_Of_Interest__c from User where id =: UserInfo.getUserId() LIMIT 1];  
        opptyContact = new OpportunityContactRole(); 
        if ( opptyId != null ) {
            this.oppty = [SELECT Id, LeadSource, Count_in_Stats__c, Renewal__c, Amount__c, Y1_Amount__c
                          FROM Opportunity WHERE Id = :opptyId ];    
        }
        if(accountID !=null){
            currentAccount = [Select ID, Name from Account where ID =:accountID];
        }
        if(contactID !=null){
            currentContact = [Select ID, AccountID, Account.Name, CreatedDate, Lead_Source_Category__c, Lead_Source_Detail__c,
                              Lead_Source_Type__c, mkto_si__Last_Interesting_Moment_Date__c, Lead_Created_Date__c
                              from Contact where ID=:contactID];
            unknownCampaign = [Select ID, Name from Campaign where Name='Unknown' limit 1];
            allCampaigns = [Select CampaignId, Campaign.Name, HasResponded, CreatedDate From CampaignMember
                            where ContactID=:currentContact.ID];
            if(allCampaigns.size()>0){
                respondedCampaign = [Select CampaignId, Campaign.Name, CreatedDate From CampaignMember
                                     where ContactID=:currentContact.ID and HasResponded = TRUE];
                if(respondedCampaign.size()>0){
                    primaryCampaign = [Select CampaignId, Campaign.Name, CreatedDate From CampaignMember where ContactID=:currentContact.ID and HasResponded = TRUE ORDER By First_Responded_Date__c DESC Limit 1];
                } else{
                    primaryCampaign = [Select CampaignId, Campaign.Name, CreatedDate From CampaignMember where ContactID=:currentContact.ID ORDER By CreatedDate DESC Limit 1];
                }
            }
        }
        if(renewalID !=null){
            currentRenewal = [Select ID, Count_in_Stats__c, Name from Renewal__c where ID =:renewalID];
        }
        if(opptyID == NULL && (u.Department__c !='Customer Care' || u.Group__c != 'Retention')){		//if(opptyID == NULL && u.Department__c !='Customer Care'){
            Date cCD = currentContact.CreatedDate.date();
            this.o = new Opportunity();
            this.o.AccountID = currentContact.AccountID;
            this.o.Name = currentContact.Account.Name;
            this.o.CloseDate = date.today().addMonths(18);
            this.o.Amount = 0;
            this.o.StageName = 'Discovery';
            if(allCampaigns.size()>0 && primaryCampaign.CampaignId != NULL){
                this.o.CampaignId = primaryCampaign.CampaignId;
            }
            if(allCampaigns.size() ==0){
                this.o.CampaignId = unknownCampaign.Id;
            }
            this.o.ContactID__c = currentContact.id;
            this.o.Suite_Of_Interest__c = u.Suite_Of_Interest__c;
            this.o.OwnerID = u.ID;
            this.o.Opportunity_Type__c ='New';
            this.o.LeadSource = leadSource;
            this.o.Lead_Source_Category__c = currentContact.Lead_Source_Category__c;
            this.o.Lead_Source_Detail__c = currentContact.Lead_Source_Detail__c;
            this.o.Lead_Source_Type__c = currentContact.Lead_Source_Type__c;
            this.o.Last_Interesting_Moment_Date__c = currentContact.mkto_si__Last_Interesting_Moment_Date__c;
            
            if(currentContact.Lead_Created_Date__c != NULL){
                Date cLCD = currentContact.Lead_Created_Date__c.date();
                if(currentContact.Lead_Created_Date__c < currentContact.CreatedDate){
                    this.o.Lead_CreatedDate__c = cLCD;
                } else {
                    this.o.Lead_CreatedDate__c = cCD;
                }
            } else {
                this.o.Lead_CreatedDate__c = cCD;
            }
        }
        if(opptyID == NULL && (u.Department__c =='Customer Care' && u.Group__c == 'Retention')){		//if(opptyID == NULL && u.Department__c =='Customer Care'){
            this.o = new Opportunity();
            if(u.Department__c == 'Customer Care' && u.Group__c == 'Retention'){		//if(u.Department__c == 'Customer Care'){
                this.o.AccountID = currentAccount.ID;
                if(renewalID !=NULL){
                    this.o.Renewal__c = currentRenewal.id;
                    this.o.Count_in_Stats__c = currentRenewal.Count_in_Stats__c;
                }
                this.o.OwnerID = u.ID;
                this.o.Account_Management_Event__c = ameID; 
                //contact was not mapped previously -- now set and create oppContact (see save method)
                this.o.ContactID__c = currentContact.id;
                this.o.StageName = 'New';
                this.o.Opportunity_Type__c ='Renewal';    
               
            }
        }
    }
    
    // runs when user presses "save"
    public PageReference saveOppty() {
        // if the user isn't in customer care
        //if (u.Department__c != 'Customer Care' || u.Group__c != 'Retention') {
            o.Num_of_Primary_Contacts__c = 1;
            insert this.o;	
            opptyContact.OpportunityId = this.o.id;
            opptyContact.ContactId = currentContact.id;
            opptyContact.isPrimary = true;        
            insert opptyContact;
        /*
        } else {
            insert this.o;	
        }
        */
        // return to opportunity detail page
        return new PageReference('/apex/opportunity_detail?id='+this.o.id).setRedirect(true);
    }
    
    public List<SelectOption> getCampaigns() {
        allCampaigns_SelectOptions = new list<CampaignMember>();
        
        if(currentAccount != null){
            allCampaigns_SelectOptions = [Select Campaign.id, Campaign.Name From CampaignMember where contact.Accountid =:currentAccount.id order by FirstRespondedDate Desc Limit 100];
            
        }
        
        list<SelectOption> options = new list<SelectOption>();
        set<SelectOption> Optset = new set<SelectOption>();
        
        
        Optset.add(new SelectOption('', '-- Select Campaign --',false));
        Optset.add(new SelectOption(unknownCampaign.id, unknownCampaign.name,false));
        
        
        for(CampaignMember x : allCampaigns_SelectOptions){
            Optset.add(new SelectOption(x.Campaign.id, x.campaign.name));    
        }
        
        options.addall(optset);

        Return options;
    }




    
}