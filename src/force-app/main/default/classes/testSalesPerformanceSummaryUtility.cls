@isTest
private class testSalesPerformanceSummaryUtility {
    static testmethod void test_updateYTDTotals() {
        User u = [SELECT id FROM User WHERE id = : UserInfo.getUserId()];
        System.runAs(u) {
            testDataUtility util = new testDataUtility();
            
            User vp = util.createUser('TestVP', 'vp', null, null, null);
            insert vp;
            
            Sales_Performance_Summary__c[] SPSs = new list<Sales_Performance_Summary__c>();
            for (integer i=1; i<=12; i++) {
                Sales_Performance_Summary__c sps = util.createSPS(vp, null);
                sps.Month__c = testDataUtility.findMonth(i);
                sps.Total_Bookings__c = 100;
                sps.Quota__c = 100;
                SPSs.add(sps);
            }
            insert SPSs;
            
            set<id> spsIds = new set<id>();
            for (Sales_Performance_Summary__c sps : SPSs) {
                spsIds.add(sps.id);
            }
            
            test.startTest();
            SPSs[5].Total_Bookings__c = 0;
            update SPSs;
            
            SPSs = [SELECT id, YTD_Total_Bookings__c, YTD_Quota__c, Month_Number__c
                    FROM Sales_Performance_Summary__c
                    WHERE id IN : spsIds
                    ORDER BY Month_Number__c ASC];
            test.stopTest();
            
            System.Assert(SPSs[11].YTD_Total_Bookings__c == 1100);
        }
    } 
}