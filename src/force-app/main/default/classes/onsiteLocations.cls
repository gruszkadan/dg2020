//TEST CLASS - testOnsiteLocations

public class onsiteLocations {
    
    public List<Location> locations {get;set;}
    public class Location{
        public integer rowNumber {get;set;}
        public string physicalAddress {get;set;}
        public string nameOfContact {get;set;}
        public string contactNumberEmail {get;set;}
        public integer numOfProducts {get;set;}
        public integer sqFootage {get;set;}
        public string closestAirport {get;set;}
        public string addlComments {get;set;}
        public boolean escortAvailable {get;set;}
        location(integer rowNum){
            rowNumber = rowNum;
        }
        
    }
    
    
    public void addLocation(integer rowNum){
        if(locations == null){
            locations = new list<Location>();
        }
        locations.add(new Location(rowNum));
    }
    
    public void removeLocation(integer rowNum){
        locations.remove(rowNum-1);
        if(locations.size() > 0){
            integer newRowNum = 1;
            for(Location l:locations){
                l.rowNumber = newRowNum;
                newRowNum++;
            }
        }
    }
    
    public void clearLocations(){
        locations.clear();
    }
    
    public boolean saveCheck() {
        boolean sCheck = true;
        if(locations[0].nameOfContact == null || locations[0].nameOfContact == ''){
            sCheck = false;
        }
        if(locations[0].contactNumberEmail == null || locations[0].contactNumberEmail == ''){
            sCheck = false;
        }
        if(locations[0].numOfProducts == null || locations[0].numOfProducts == 0){
            sCheck = false;
        }
        if(locations[0].sqFootage == null || locations[0].sqFootage == 0){
            sCheck = false;
        }
        if(locations[0].closestAirport == null || locations[0].closestAirport == ''){
            sCheck = false;
        }
        if(locations[0].addlComments == null || locations[0].addlComments == ''){
            sCheck = false;
        }
        for(Location l:locations){
            if(l.physiCalAddress == null || l.physicalAddress == ''){
                sCheck = false;
            }
        }
        return sCheck;
    }
    
    public void createOnsiteAttachment(Quote_Product__c qp){
        Attachment[] existingAttachments = [Select id,parentId,name FROM Attachment WHERE parentId =:qp.Quote__c AND Name LIKE 'OnSiteInventoryQuestionnaire%'];
        integer version;
        if(existingAttachments != null && existingAttachments.size() > 0){
            version = existingAttachments.size()+1;
        }else{
            version = 1;
        }
        blob pdfBlob;
        if (!test.isRunningTest()) {
            pdfBlob = new PageReference('/apex/onsiteInventoryQuestionnaire?id='+qp.id+'&pdf=true').setRedirect(false).getContentAsPDF();
        } else {
            pdfBlob = Blob.valueOf('Unit Test Attachment Body');
        }
        Attachment toQuote = new Attachment(parentId = qp.Quote__c, name='OnSiteInventoryQuestionnaire' +'-v'+ version + '.pdf', body = pdfBlob);
        insert toQuote; 
        Attachment toQP = toQuote.clone();
        toQP.ParentId = qp.id;
        insert toQP;
    }
    
    
    public integer onsiteTotalSQFeet(){
        integer total;
        if(locations != null && locations.size() > 0){
            total = 0;
            for(Location l:locations){
                if(l.sqFootage != null){
                    total = total+l.sqFootage;
                }
            }
        }
        return total;
    }
    
    public integer onsiteTotalProducts(){
        integer total;
        if(locations != null && locations.size() > 0){
            total = 0;
            for(Location l:locations){
                if(l.numOfProducts != null){
                    total = total+l.numOfProducts;
                }
            }
        }
        return total;
    }
    
}