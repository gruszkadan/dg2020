@isTest(SeeAllData=true)
private class testSalesLocationMap {
    
    static testmethod void test1() {
        
        Account a = new Account();
        a.Name = 'Test Account';
        a.Customer_Status__c = 'Active';
        a.NAICS_Code__c = '11 - Agriculture, Forestry, Fishing & Hunting';
        a.Num_Of_Employees__c = 50;
        a.Active_Products__c = 'HQ';
        a.Current_Chemical_Management_System__c = 'Paper';
        a.Rating = 'High';
        a.Num_Of_Beds__c = 10;
        a.BillingStreet = '350 N Orleans St';
        a.BillingCity = 'Chicago';
        a.BillingCountry = 'United States';
        a.BillingPostalCode = '60654';
        a.Key_Competitors__c = 'ABCCorp';
        a.enterprise_sales__c = true;
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'Bob';
        c.LastName = 'Bob';
        c.AccountID = a.Id;
        insert c;     
        
        Opportunity o = new Opportunity();
        o.AccountID = a.Id;
        o.Name = 'Test';
        o.CloseDate = Date.today(); 
        o.StageName = 'Closed/Won';
        insert o;
        
        SF_Usage__c sfuse = new SF_Usage__c();
        sfuse.AccountLocator_Last_Use__c = date.TODAY();
        sfuse.AccountLocator_Searches__c = 0;
        sfuse.AccountLOcator_Views__c = 9;
        sfuse.Name = 'Test1';
        insert sfuse;
        
        ApexPages.currentPage().getParameters().put('id', a.Id);
        ApexPages.currentPage().getParameters().put('map', 'true');
        ApexPages.currentPage().getParameters().put('aListId', a.Id);
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        salesLocationMapController myController = new salesLocationMapController(testController);
        
        myController.aListId = a.Id;
        myController.selCustomerStatus = 'All';
        myController.selNAICSCode = 'All';
        //    myController.selTerritory = 'All';
        myController.selCounty = 'All';
        myController.selBeds = 'All';
        myController.selRating = 'All';
        myController.selNumEmployee = 'All';
        myController.showMap = true;
        myController.sortBy = NULL;
        //   myController.selTerritory = 'All';
        myController.selRegion = 'United States/Canada';
        myController.getCustomerStatuses();
        myController.getNAICSCodes();
        //   myController.getTerritories();
        myController.queryCounties();
        myController.getNumEmployees();
        myController.getNumBeds();
        myController.getCloseDateMo();
        myController.getRatings();
        myController.hideSDSMethods();
        myController.showOnMap();
        myController.addUse();
        myController.getRegions();
        myController.find();
        system.assert(myController.selRegion == 'United States/Canada');
        
        
        myController.a_wrappers[0].aWrap.Key_EHS_Competitors__c = 'Test Account';
        myController.a_wrappers[0].aWrap.Key_Competitors__c = 'Test Account';
        myController.a_wrappers[0].aWrap.Key_Authoring_Competitors__c = 'Test Account';
        myController.a_wrappers[0].aWrap.Key_Training_Competitors__c = 'Test Account';
        myController.a_wrappers[0].key_ehs = true;
        myController.a_wrappers[0].key_chemMgmt = true;
        myController.a_wrappers[0].key_auth = true;
        myController.a_wrappers[0].key_train = true;
        myController.a_wrappers[0].key_ergo = true;
        
        ApexPages.currentPage().getParameters().put('aListId', myController.a_wrappers[0].aWrap.id);
        myController.grabKeyCompId();
        myController.saveKeyComp();
        
        myController.selRegion = 'International';
        myController.find();
        system.assert(myController.selRegion != 'United States/Canada');
        
        //Set account type and call method
        //Assert to check value
        myController.AccountType = 'Enterprise';
        mycontroller.find();
        
        system.assertEquals('Enterprise', mycontroller.AccountType);
        system.assertEquals(True, a.enterprise_sales__c);        
        
        
    }
    
    static testmethod void test2() {
        //Create Account for test class
        Account a = new Account();
        a.Name = 'Macs Account';
        a.BillingCountry = 'United States';
        a.BillingState = 'IL';
        a.enterprise_sales__c = false;
        
        insert a;
        
        
        // pass in account id for page 
        ApexPages.currentPage().getParameters().put('id', a.Id);
        // pass in controller used in page 
        ApexPages.StandardController testController = new ApexPages.StandardController(new Account());
        salesLocationMapController myController = new salesLocationMapController(testController);
        
        mycontroller.getstates();
        
        // set selected state 
        mycontroller.SelectedState = 'IL';
        // call find method and see if it adds the selected state to query
        mycontroller.find();
        
        system.assertEquals('IL', mycontroller.selectedstate);
        
        // Set search type to my accounts 
        mycontroller.searchtype = 'My Accounts';
        // call method and see if searchtype is set to my accounts  
        mycontroller.find();
        
        system.assertEquals('My Accounts', mycontroller.SearchType);
        
        // set searchtype too Acts in my territories 
        mycontroller.searchtype = 'Accounts in My Territories'; 
        // call method and see if search type was set 
        mycontroller.find();
        
        system.assertEquals('Accounts in My Territories', mycontroller.SearchType);
        //Set account type and call method
        //Assert to check value 
        myController.AccountType = 'Mid-Market';
        mycontroller.find();
        
        
        system.assertEquals('Mid-Market', mycontroller.AccountType);
        system.assertEquals(False, a.enterprise_sales__c);   
        
        
    }
    
    
    
}