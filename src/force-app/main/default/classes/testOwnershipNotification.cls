@isTest(seeAllData=true)
public class testOwnershipNotification {
    
    public static string CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void test1() {
        Account[] childList = new List<Account>();
        
        Account parent = new Account(Name = 'Parent', Enterprise_Sales__c = true, OwnerId = [SELECT id from User where LastName = 'Gruszka' Limit 1].id);
        insert parent;
        
        Account child1 = new Account(Name = 'Child 1', OwnerId = [SELECT id from User where LastName = 'McCauley' Limit 1].id);
        childList.add(child1);
        //insert child1;
        
        Account child2 = new Account(Name = 'Child 2', OwnerId = [SELECT id from User where LastName = 'McCauley' Limit 1].id);
        childList.add(child2);
        
        Account child3 = new Account(Name = 'Child 3', OwnerId = [SELECT id from User where LastName = 'McCauley' Limit 1].id);
        childList.add(child3);
        
        Account child4 = new Account(Name = 'Child 4', OwnerId = [SELECT id from User where LastName = 'McCauley' Limit 1].id);
        childList.add(child4);
        
        Account child5 = new Account(Name = 'Child 5', OwnerId = [SELECT id from User where LastName = 'McCauley' Limit 1].id);
        childList.add(child5);
        
        Account child6 = new Account(Name = 'Child 6', OwnerId = [SELECT id from User where LastName = 'McCauley' Limit 1].id);
        childList.add(child6);
        
        insert childList;
              
        test.startTest();
        account[] childupdates = new List<account>();
        child1.parentId = parent.id;
        childupdates.add(child1);
        //pdate child1;
        child2.parentId = child1.id;
        childupdates.add(child2);
        child3.parentId = child2.id;
        childupdates.add(child3);
        child4.parentId = child3.id;
        childupdates.add(child4);
        child5.parentId = child4.id;
        childupdates.add(child5);
        child6.parentId = child5.id;
        childupdates.add(child6);
        update childupdates;
        
        
        child6.ownerId = [SELECT id from User where LastName = 'McCauley' Limit 1].id;
        child6.parentId = null;
        update child6;
        child6.parentId = child5.id;
        update child6;
        
        child1 = [select id, Enterprise_Sales__c, OwnerId from account where id =: child1.id];
        System.assertEquals(parent.OwnerId, child1.OwnerId);
        System.assertEquals(parent.Enterprise_Sales__c, child1.Enterprise_Sales__c);
        
        child2 = [select id, Enterprise_Sales__c, OwnerId from account where id =: child2.id];
        System.assertEquals(parent.OwnerId, child2.OwnerId);
        System.assertEquals(parent.Enterprise_Sales__c, child2.Enterprise_Sales__c);
        
        child3 = [select id, Enterprise_Sales__c, OwnerId from account where id =: child3.id];
        System.assertEquals(parent.OwnerId, child3.OwnerId);
        System.assertEquals(parent.Enterprise_Sales__c, child3.Enterprise_Sales__c);
        
        child4 = [select id, Enterprise_Sales__c, OwnerId from account where id =: child4.id];
        System.assertEquals(parent.OwnerId, child4.OwnerId);
        System.assertEquals(parent.Enterprise_Sales__c, child4.Enterprise_Sales__c);
        
        child5 = [select id, Enterprise_Sales__c, OwnerId from account where id =: child5.id];
        System.assertEquals(parent.OwnerId, child5.OwnerId);
        System.assertEquals(parent.Enterprise_Sales__c, child5.Enterprise_Sales__c);
        
        child6 = [select id, Enterprise_Sales__c, OwnerId from account where id =: child6.id];
        System.assertEquals(parent.OwnerId, child6.OwnerId);
        System.assertEquals(parent.Enterprise_Sales__c, child6.Enterprise_Sales__c);
        
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new ownershipNotificationScheduled());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        test.stopTest();
        
    }
    
    static testmethod void test2(){
        
        id OwnerId = [SELECT id from User where LastName = 'Gruszka' Limit 1].id;
        
       
        Lead[] leads = new list<Lead>();
        
        //LEAD 1 - enterprise, territory
        Lead l1 = new Lead();
        l1.LastName = 'TestTerritory';
        l1.Company = 'Test Company';
        l1.Enterprise_Sales_Lead__c = true;
        //l1.Territory__c = t.id;
        l1.OwnerId = OwnerId;
        leads.add(l1);
        
        //LEAD 2 - enterprise, territory, ent owner
        Lead l2 = new Lead();
        l2.LastName = 'TestNoTerritory';
        l2.Company = 'Test Company';
        l2.Enterprise_Sales_Lead__c = true;
        l2.OwnerId = '005300000012mUR';
        leads.add(l2);
        
		insert leads;

        ownershipChangeNotifications.createLeadOCN(leads);
        List<Ownership_Change_Notification__c> checkRecords = [Select id, Name from Ownership_Change_Notification__c];
        boolean check;
        for(Ownership_Change_Notification__c ocn: checkRecords){
            if(ocn.Name == l1.id){
                check = true;
            }
        }
        System.assert(check==true);

        
    }
    
}