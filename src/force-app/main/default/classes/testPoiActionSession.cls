@isTest(seeAllData = true)
private class testPoiActionSession {
    
    static testmethod void test1(){
        Account newAccount = testAccount();
        insert newAccount;
        Contact master = testContact(newAccount.id);
        master.POI_Stage_AUTH__c = 'Neutral Interest';
        master.POI_Stage_ERGO__c = 'Neutral Interest';
        master.POI_Stage_MSDS__c = 'Neutral Interest';
        master.POI_Stage_EHS__c = 'Neutral Interest';
        master.POI_Stage_ODT__c = 'Neutral Interest';
        master.POI_Status_AUTH__c = 'Working - Connected';
        master.POI_Status_ERGO__c = 'Working - Connected';
        master.POI_Status_MSDS__c = 'Working - Connected';
        master.POI_Status_EHS__c = 'Working - Connected';
        master.POI_Status_ODT__c = 'Working - Connected';
        master.POI_Status_Change_Date_AUTH__c = date.today();
        master.POI_Status_Change_Date_ERGO__c = date.today();
        master.POI_Status_Change_Date_MSDS__c = date.today();
        master.POI_Status_Change_Date_EHS__c = date.today();
        master.POI_Status_Change_Date_ODT__c = date.today();
        insert master;
        
        //Master Contact's POI
        Product_of_Interest__c newPoi = new Product_of_Interest__c();
        newPoi.Contact__c = master.id;
        insert newPoi;
        
        Product_of_Interest_Action__c[] actions = new list<Product_of_Interest_Action__c>();
        
        //MSDS Open Action
        Product_of_Interest_Action__c msdsOpenAction = new Product_of_Interest_Action__c();
        msdsOpenAction.Contact__c = master.id;
        msdsOpenAction.Product_of_Interest__c = newPoi.id;
        msdsOpenAction.Marketo_ID__c = '1234567';
        msdsOpenAction.Product_Suite__c = 'MSDS Management';
        msdsOpenAction.Product__c = 'HQ';
        msdsOpenAction.Action__c = 'Request Demo';
        actions.add(msdsOpenAction);
        
        //MSDS  Closed Action
        Product_of_Interest_Action__c msdsClosedAction = new Product_of_Interest_Action__c();
        msdsClosedAction.Contact__c = master.id;
        msdsClosedAction.Product_of_Interest__c = newPoi.id;
        msdsClosedAction.Marketo_ID__c = '1234567';
        msdsClosedAction.Product_Suite__c = 'MSDS Management';
        msdsClosedAction.Product__c = 'HQ';
        msdsClosedAction.Action__c = 'Request Demo';
        msdsClosedAction.Action_Status__c = 'Open';
        insert msdsClosedAction;
        msdsClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update msdsClosedAction;
        
        //EHS Open Action
        Product_of_Interest_Action__c ehsOpenAction = new Product_of_Interest_Action__c();
        ehsOpenAction.Contact__c = master.id;
        ehsOpenAction.Product_of_Interest__c = newPoi.id;
        ehsOpenAction.Marketo_ID__c = '1234567';
        ehsOpenAction.Product_Suite__c = 'EHS Management';
        ehsOpenAction.Product__c = 'EHS Management';
        ehsOpenAction.Action__c = 'Request Demo';
        actions.add(ehsOpenAction);
        
        //EHS  Closed Action
        Product_of_Interest_Action__c ehsClosedAction = new Product_of_Interest_Action__c();
        ehsClosedAction.Contact__c = master.id;
        ehsClosedAction.Product_of_Interest__c = newPoi.id;
        ehsClosedAction.Marketo_ID__c = '1234567';
        ehsClosedAction.Product_Suite__c = 'EHS Management';
        ehsClosedAction.Product__c = 'EHS Management';
        ehsClosedAction.Action__c = 'Request Demo';
        ehsClosedAction.Action_Status__c = 'Open';
        insert ehsClosedAction;
        ehsClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update ehsClosedAction;
        
        //AUTH Open Action
        Product_of_Interest_Action__c authOpenAction = new Product_of_Interest_Action__c();
        authOpenAction.Contact__c = master.id;
        authOpenAction.Product_of_Interest__c = newPoi.id;
        authOpenAction.Marketo_ID__c = '1234567';
        authOpenAction.Product_Suite__c = 'MSDS Authoring';
        authOpenAction.Product__c = 'MSDS Authoring';
        authOpenAction.Action__c = 'Request Demo';
        actions.add(authOpenAction);
        
        //AUTH  Closed Action
        Product_of_Interest_Action__c authClosedAction = new Product_of_Interest_Action__c();
        authClosedAction.Contact__c = master.id;
        authClosedAction.Product_of_Interest__c = newPoi.id;
        authClosedAction.Marketo_ID__c = '1234567';
        authClosedAction.Product_Suite__c = 'MSDS Authoring';
        authClosedAction.Product__c = 'MSDS Authoring';
        authClosedAction.Action__c = 'Request Demo';
        authClosedAction.Action_Status__c = 'Open';
        insert authClosedAction;
        authClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update authClosedAction;
        
        //ERGO Open Action
        Product_of_Interest_Action__c ergoOpenAction = new Product_of_Interest_Action__c();
        ergoOpenAction.Contact__c = master.id;
        ergoOpenAction.Product_of_Interest__c = newPoi.id;
        ergoOpenAction.Marketo_ID__c = '1234567';
        ergoOpenAction.Product_Suite__c = 'Ergonomics';
        ergoOpenAction.Product__c = 'Ergonomics';
        ergoOpenAction.Action__c = 'Request Demo';
        actions.add(ergoOpenAction);
        
        //ERGO  Closed Action
        Product_of_Interest_Action__c ergoClosedAction = new Product_of_Interest_Action__c();
        ergoClosedAction.Contact__c = master.id;
        ergoClosedAction.Product_of_Interest__c = newPoi.id;
        ergoClosedAction.Marketo_ID__c = '1234567';
        ergoClosedAction.Product_Suite__c = 'Ergonomics';
        ergoClosedAction.Product__c = 'Ergonomics';
        ergoClosedAction.Action__c = 'Request Demo';
        ergoClosedAction.Action_Status__c = 'Open';
        insert ergoClosedAction;
        ergoClosedAction.Action_Status__c = 'Resolved - Unable to Connect';
        update ergoClosedAction;
        
        //ODT Open Action
        Product_of_Interest_Action__c odtOpenAction = new Product_of_Interest_Action__c();
        odtOpenAction.Contact__c = master.id;
        odtOpenAction.Product_of_Interest__c = newPoi.id;
        odtOpenAction.Marketo_ID__c = '1234567';
        odtOpenAction.Product_Suite__c = 'On-Demand Training';
        odtOpenAction.Product__c = 'On-Demand Training';
        odtOpenAction.Action__c = 'Request Demo';
        actions.add(odtOpenAction);
        
        //ODT  Closed Action
        Product_of_Interest_Action__c odtClosedAction = new Product_of_Interest_Action__c();
        odtClosedAction.Contact__c = master.id;
        odtClosedAction.Product_of_Interest__c = newPoi.id;
        odtClosedAction.Marketo_ID__c = '1234567';
        odtClosedAction.Product_Suite__c = 'On-Demand Training';
        odtClosedAction.Product__c = 'On-Demand Training';
        odtClosedAction.Action__c = 'Request Demo';
        odtClosedAction.Action_Status__c = 'Open';
        insert odtClosedAction;
        
        
        poiActionSession.insertSession(actions);
    } 
    
      static testmethod void test2(){
        
        Lead master = testLead();
        insert master;
        
        //Master Contact's POI
        Product_of_Interest__c newPoi = new Product_of_Interest__c();
        newPoi.Lead__c = master.id;
        insert newPoi;
        
        Product_of_Interest_Action__c[] actions = new list<Product_of_Interest_Action__c>();
        
        //MSDS Open Action
        Product_of_Interest_Action__c msdsOpenAction = new Product_of_Interest_Action__c();
        msdsOpenAction.Lead__c = master.id;
        msdsOpenAction.Product_of_Interest__c = newPoi.id;
        msdsOpenAction.Marketo_ID__c = '1234567';
        msdsOpenAction.Product_Suite__c = 'MSDS Management';
        msdsOpenAction.Product__c = 'HQ';
        msdsOpenAction.Action__c = 'Request Demo';
        actions.add(msdsOpenAction);
        
        //EHS Open Action
        Product_of_Interest_Action__c ehsOpenAction = new Product_of_Interest_Action__c();
        ehsOpenAction.Lead__c = master.id;
        ehsOpenAction.Product_of_Interest__c = newPoi.id;
        ehsOpenAction.Marketo_ID__c = '1234567';
        ehsOpenAction.Product_Suite__c = 'EHS Management';
        ehsOpenAction.Product__c = 'EHS Management';
        ehsOpenAction.Action__c = 'Request Demo';
        actions.add(ehsOpenAction);
        
        //AUTH Open Action
        Product_of_Interest_Action__c authOpenAction = new Product_of_Interest_Action__c();
        authOpenAction.Lead__c = master.id;
        authOpenAction.Product_of_Interest__c = newPoi.id;
        authOpenAction.Marketo_ID__c = '1234567';
        authOpenAction.Product_Suite__c = 'MSDS Authoring';
        authOpenAction.Product__c = 'MSDS Authoring';
        authOpenAction.Action__c = 'Request Demo';
        actions.add(authOpenAction);
        
        //ERGO Open Action
        Product_of_Interest_Action__c ergoOpenAction = new Product_of_Interest_Action__c();
        ergoOpenAction.Lead__c = master.id;
        ergoOpenAction.Product_of_Interest__c = newPoi.id;
        ergoOpenAction.Marketo_ID__c = '1234567';
        ergoOpenAction.Product_Suite__c = 'Ergonomics';
        ergoOpenAction.Product__c = 'Ergonomics';
        ergoOpenAction.Action__c = 'Request Demo';
        actions.add(ergoOpenAction);
        
        //ODT Open Action
        Product_of_Interest_Action__c odtOpenAction = new Product_of_Interest_Action__c();
        odtOpenAction.Lead__c = master.id;
        odtOpenAction.Product_of_Interest__c = newPoi.id;
        odtOpenAction.Marketo_ID__c = '1234567';
        odtOpenAction.Product_Suite__c = 'On-Demand Training';
        odtOpenAction.Product__c = 'On-Demand Training';
        odtOpenAction.Action__c = 'Request Demo';
        actions.add(odtOpenAction);
        
        poiActionSession.insertSession(actions);
    } 

    public static Account testAccount() {
        return new Account(Name='Test Account', OwnerId='00580000007F76q');
    }
    
    public static Contact testContact(id accountId) {
        return new Contact(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', AccountId=accountId);
    }
     public static Lead testLead() {
        return new Lead(FirstName='Test', LastName='Test', Communication_Channel__c='Inbound Call', Company = 'Test Account');
    }
}