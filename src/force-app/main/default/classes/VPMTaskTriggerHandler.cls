public class VPMTaskTriggerHandler {

   
    // BEFORE INSERT
    public static void beforeInsert(VPM_Task__c[] tasks) {
        

 		//alll training tasks are set as closed and the end date is set- 
        VPM_task__c[] closedTasks = new list<VPM_task__c>();
        
        for (integer i=0; i<tasks.size(); i++) {
            if(tasks[i].Status__c == 'Complete' && tasks[i].Email_Sequence_Number__c == null){
                closedTasks.add(tasks[i]);
            }
        }
        if(closedTasks.size() > 0){
           VPMTaskUtility.updateVPMTaskEndDate(closedTasks);
        }  

        
    }
    
    // BEFORE UPDATE
    public static void beforeUpdate(VPM_Task__c[] oldTasks, VPM_Task__c[] newTasks) {
        VPM_task__c[] changedStatus = new list<VPM_task__c>();
        
        for (integer i=0; i<newTasks.size(); i++){
            if (newTasks[i].Status__c != oldTasks[i].Status__c && newTasks[i].Email_Sequence_Number__c == null){
                changedStatus.add(newTasks[i]);
            }
        }
        if(changedStatus.size() > 0){
            VPMTaskUtility.updateVPMTaskEndDate(changedStatus);
        }
    }
    
    // BEFORE DELETE
    public static void beforeDelete(VPM_Task__c[] oldTasks, VPM_Task__c[] newTasks) {}
    
    // AFTER INSERT
    public static void afterInsert(VPM_Task__c[] oldTasks, VPM_Task__c[] newTasks) {

        VPM_Task__c[] trainingTasks = new list<VPM_Task__c>();
        
        //check that it was created from taskNote? &&  newTasks[i].Case_Note__c != NULL 
        for (integer i=0; i<newTasks.size(); i++) {
            if (newTasks[i].Subject__c == 'Training' || newTasks[i].Subject__c == 'Webinar - Training'){
                trainingTasks.add(newTasks[i]);
            }
        }
        
        if(trainingTasks.size() > 0){
            VPMNoteUtility.VPMTaskNoteAssociation(trainingTasks);
        }

       	VPMMilestoneUtility.MilestoneStatusEvaluation(newTasks);
    }
    
    
    // AFTER UPDATE
    public static void afterUpdate(VPM_Task__c[] oldTasks, VPM_Task__c[] newTasks) {
        VPM_Task__c[] tasksStatusChanged = new list<VPM_Task__c>();
        VPM_Task__c[] changeContactDatesToNull = new list<VPM_Task__c>();
        VPM_Task__c[] updateContactDates = new list<VPM_Task__c>();
        for (integer i=0; i<newTasks.size(); i++) {
            if(newTasks[i].Status__c != oldTasks[i].Status__c){
                tasksStatusChanged.add(newTasks[i]);
            }
            if(newTasks[i].Contact__c != oldTasks[i].Contact__c && newTasks[i].Status__c != 'Completed' && newTasks[i].Email_Sequence_number__c != null){
               changeContactDatesToNull.add(oldTasks[i]);
            }
            if((newTasks[i].Contact__c != oldTasks[i].Contact__c || newTasks[i].Start_Date__c != oldTasks[i].Start_Date__c) && newTasks[i].Status__c != 'Completed' && newTasks[i].Email_Sequence_number__c != null){
               updateContactDates.add(newTasks[i]);
            }
        }
        
        if(tasksStatusChanged.size() > 0){
            VPMMilestoneUtility.MilestoneStatusEvaluation(tasksStatusChanged);
        }
        if(changeContactDatesToNull.size() > 0){
            contactUtility.clearContactDatesOldContact(changeContactDatesToNull);
        }
        if(updateContactDates.size() > 0){
            contactUtility.updateNewContactDates(updateContactDates);
        }
    }

    
    // AFTER DELETE
    public static void afterDelete(VPM_Task__c[] tasks) {}

}