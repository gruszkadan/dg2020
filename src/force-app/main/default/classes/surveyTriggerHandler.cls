public class surveyTriggerHandler {
    
    public static void beforeInsert(Survey_Feedback__c[] newSurveys) {}
    
    public static void beforeUpdate(Survey_Feedback__c[] oldSurveys, Survey_Feedback__c[] newSurveys) {
        Survey_Feedback__c[] upsellSurveys = new list<Survey_Feedback__c>();
        for (integer i=0; i<newSurveys.size(); i++) {
            if (oldSurveys[i].Survey_Completed__c == false && newSurveys[i].Survey_Completed__c == true && 
                newSurveys[i].Additional_Product_Interest__c != null && newSurveys[i].Additional_Product_Interest__c != 'None of the above') {
                    upsellSurveys.add(newSurveys[i]);
                }
        }
        if (upsellSurveys.size() > 0) {
            surveyUtility.surveyUpsell(upsellSurveys);
        }
    }
    
    public static void afterInsert(Survey_Feedback__c[] oldSurveys, Survey_Feedback__c[] newSurveys) {
        Survey_Feedback__c[] etsSurveys = new list<Survey_Feedback__c>();
        for (integer i=0; i<newSurveys.size(); i++) {
            if (newSurveys[i].Name_of_Survey__c == 'ETS - Onboarding' || newSurveys[i].Name_of_Survey__c == 'ETS - Services' || newSurveys[i].Name_of_Survey__c == 'ETS - Support') {
                etsSurveys.add(newSurveys[i]);
            }
        }
        if (etsSurveys.size() > 0) {
            surveyUtility.ets_addToSurveyQueue(etsSurveys);
        }
    }
    
    public static void afterUpdate(Survey_Feedback__c[] oldSurveys, Survey_Feedback__c[] newSurveys) {
        Survey_Feedback__c[] asSurveys = new list<Survey_Feedback__c>();
        Survey_Feedback__c[] etsReminders = new list<Survey_Feedback__c>();
        Survey_Feedback__c[] completedWithNPS = new list<Survey_Feedback__c>();
        
        for (integer i=0; i<newSurveys.size(); i++) {
            if (oldSurveys[i].Name != newSurveys[i].Name && newSurveys[i].Name_of_Survey__c == 'Annual Satisfaction') {
                asSurveys.add(newSurveys[i]);
            }
            if (oldSurveys[i].ETS_Send_Reminder__c == false && newSurveys[i].ETS_Send_Reminder__c == true) {
                etsReminders.add(newSurveys[i]);  
            } 
            if (oldSurveys[i].Survey_Completed__c == false && newSurveys[i].Survey_Completed__c == true && newSurveys[i].NPS_Score_Num__c != null) {
                completedWithNPS.add(newSurveys[i]);
            }
        }
        if (asSurveys.size() > 0) {
            surveyUtility.as_contactSurveyUpdate(asSurveys);
        }
        if (etsReminders.size() > 0) {
            surveyUtility.ets_sendReminder(etsReminders);
        }
        if (completedWithNPS.size() > 0) {
            surveyUtility.contactNPSScoreUpdate(completedWithNPS);
        }
    }
}