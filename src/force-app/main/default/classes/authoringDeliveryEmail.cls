public with sharing class authoringDeliveryEmail {
    public EmailMessage emailMsg {get; private set;}
    public Contact ourContact {get; set;}
    public attachment ourAttachment {get;set;}
    public attachment ourAttachment2 {get;set;}
    public attachment ourAttachment3 {get;set;}
    public attachment ourAttachment4 {get;set;}
    public attachment ourAttachment5 {get;set;}
    public attachment[] attachmentList {get;set;}
    public String userName {get;set;}
    String numDocs;
    Public String addlTo {get;set;}
    String cc;
    String bcc;
    String Subject;
    String Body;
    String caseId;
    string contactId;
    User u;
    
    public authoringDeliveryEmail(){
        ourAttachment = new attachment();
        ourAttachment2 = new attachment();
        ourAttachment3 = new attachment();
        ourAttachment4 = new attachment();
        ourAttachment5 = new attachment();
        userName = UserInfo.getName();
        caseId = ApexPages.currentPage().getParameters().get('caseid');
        contactId = ApexPages.currentPage().getParameters().get('contactid');
        ourContact = [SELECT id, email, name FROM Contact WHERE id =: contactid LIMIT 1];
        emailMsg = new EmailMessage(); 
        emailMsg.ParentId = caseId;
        emailMsg.ToAddress = ourContact.Email;
        emailMsg.FromAddress = UserInfo.getUserEmail();
        u = [SELECT id, Name, Title, Phone, email, EmailPreferencesAutoBcc FROM User where id =: UserInfo.getUserId()];
        if(u.EmailPreferencesAutoBcc == true){
            emailMsg.BccAddress = UserInfo.getUserEmail();
        }
        numDocs = ApexPages.currentPage().getParameters().get('numDocs');
        if(numDocs != null){
            emailMsg.Num_of_Docs__c = decimal.valueOf(numDocs);
        }
        addlTo = ApexPages.currentPage().getParameters().get('addlTo');
        cc = ApexPages.currentPage().getParameters().get('cc');
        if(cc != null){
            emailMsg.CcAddress = cc;
        }
        bcc = ApexPages.currentPage().getParameters().get('bcc');
        if(bcc != null){
            emailMsg.BccAddress = bcc;
        }
        Subject = ApexPages.currentPage().getParameters().get('Subject');
        if(Subject != null){
            emailMsg.Subject = Subject;
        }
        Body = ApexPages.currentPage().getParameters().get('Body');
        if(Body != null){
            emailMsg.Body__c = Body;
        }
        if(Body == null){
            string title;
            if(u.Title != null){
                title = '<br />'+u.Title;
            }else{
                title = '';
            }
            string phone;
            if(u.Phone != null){
                phone = '<br /> Direct: 1.312.881.'+u.Phone.right(4)+'<br /> Fax: 1.866.369.2785';
            }else{
                phone = '';
            }
            string signature = '<br/><br/><p><strong>'+u.Name+'</strong>'+title+'<strong><br /></strong><em>VelocityEHS</em><br /> 222 Merchandise Mart Plaza, Suite 1750<br />Chicago, IL 60654<strong></strong>'+phone+'<br /> Email: <a href="mailto:'+u.email+'">'+u.email+'</a><br /><br />Visit us at:&nbsp;<a href="https://www.ehs.com/">https://www.EHS.com/</a><br /><strong><img src="https://www.ehs.com/wp-content/themes/velocity/lib/images/ehs-logo.png" alt="VelocityEHS logo" /></strong>'+
                +'<strong><br /> </strong><strong>Notice:</strong>&nbsp;Copyright&copy; '+System.Today().year()+', <em>VelocityEHS</em>. All rights reserved. This email, including any attachments, may contain confidential information and may not be redistributed without permission. '+
                +'If you are not the intended recipient, please immediately notify the sender by phone or reply-email, and delete it and all its contents from your system.<br/><br/>To learn more about our award-winning chemical and safety data sheet management solution, '+
                +'visit <a href="http://www.msdsonline.com/">MSDSonline.com</a>.';
            emailMsg.Body__c = signature;
        }
        attachmentList = [SELECT id,Body,Name,ParentId,createdbyid,lastmodifieddate,bodylength FROM Attachment WHERE ParentId =: caseId AND CreatedDate > :Datetime.now().addMinutes(-30) AND CreatedById =: Userinfo.getUserId()];
    }
    
    public PageReference addAttachment(){
        Attachment[] ourAttachments = new list<Attachment>();
        if(ourAttachment.Name != null){
            ourAttachment.ParentId = caseid;
            ourAttachments.add(ourAttachment);
        }
        if(ourAttachment2.Name != null){
            ourAttachment2.ParentId = caseid;
            ourAttachments.add(ourAttachment2);
        }
        if(ourAttachment3.Name != null){
            ourAttachment3.ParentId = caseid;
            ourAttachments.add(ourAttachment3);
        }
        if(ourAttachment4.Name != null){
            ourAttachment4.ParentId = caseid;
            ourAttachments.add(ourAttachment4);
        }
        if(ourAttachment5.Name != null){
            ourAttachment5.ParentId = caseid;
            ourAttachments.add(ourAttachment5);
        }
        try {
            insert ourAttachments;
        } catch(exception e) {
            salesforceLog.createLog('Case', true, 'authoringDeliveryEmail', 'addAttachment', +string.valueOf(e));
        }
        
        PageReference authoring_delivery_email = Page.authoring_delivery_email;
        authoring_delivery_email.setRedirect(true);
        authoring_delivery_email.getParameters().put('caseid',caseId);
        authoring_delivery_email.getParameters().put('contactid',contactId);
        authoring_delivery_email.getParameters().put('numDocs',string.valueof(emailMsg.Num_of_Docs__c));
        authoring_delivery_email.getParameters().put('addlTo',addlTo);
        authoring_delivery_email.getParameters().put('cc',emailMsg.CcAddress);
        authoring_delivery_email.getParameters().put('bcc',emailMsg.BccAddress);
        authoring_delivery_email.getParameters().put('Subject',emailMsg.Subject);
        authoring_delivery_email.getParameters().put('Body',emailMsg.Body__c);
        return authoring_delivery_email;
    }
    
    public PageReference removeAttachment(){
        id attchid = ApexPages.currentPage().getParameters().get('aid');
        for(attachment a:attachmentList){
            if(a.id == attchid){
                try {
                    delete a;
                } catch(exception e) {
                    salesforceLog.createLog('Case', true, 'authoringDeliveryEmail', 'removeAttachment', +string.valueOf(e));
                }
            }
        }
        PageReference authoring_delivery_email = Page.authoring_delivery_email;
        authoring_delivery_email.setRedirect(true);
        authoring_delivery_email.getParameters().put('caseid',caseId);
        authoring_delivery_email.getParameters().put('contactid',contactId);
        authoring_delivery_email.getParameters().put('numDocs',string.valueof(emailMsg.Num_of_Docs__c));
        authoring_delivery_email.getParameters().put('addlTo',addlTo);
        authoring_delivery_email.getParameters().put('cc',emailMsg.CcAddress);
        authoring_delivery_email.getParameters().put('bcc',emailMsg.BccAddress);
        authoring_delivery_email.getParameters().put('Subject',emailMsg.Subject);
        authoring_delivery_email.getParameters().put('Body',emailMsg.Body__c);
        return authoring_delivery_email;
    }
    
    public PageReference sendEmail(){
        if(emailMsg.Num_of_Docs__c != null && emailMsg.Subject != null && emailMsg.Subject != '' && (ourAttachment.body == null && ourAttachment2.body == null && ourAttachment3.body == null && ourAttachment4.body == null && ourAttachment5.body == null)){
            Messaging.SingleEmailMessage singleEmailMsg = new Messaging.SingleEmailMessage();
            String[] toAddresses = new list<String>();
            if(addlTo != null && addlTo != ''){
                toAddresses = addlto.split(',');
            }
            toAddresses.add(emailMsg.ToAddress);
            if(toAddresses.size() > 1){
                emailMsg.ToAddress = string.join(toAddresses,';');
            }
            singleEmailMsg.setToAddresses(toAddresses);
            if(emailMsg.BccAddress != null && emailMsg.BccAddress != ''){
                singleEmailMsg.setBccAddresses(emailMsg.BccAddress.split(','));
            }
            if (emailMsg.CcAddress != null && emailMsg.CcAddress != '') {
                singleEmailMsg.setCcAddresses(emailMsg.CcAddress.split(','));
            }
            singleEmailMsg.setSubject(emailMsg.Subject);
            if(emailMsg.Body__c != null && emailMsg.Body__c != ''){
                singleEmailMsg.setHtmlBody(emailMsg.Body__c);
            }
            if(attachmentList.size() > 0){
                Messaging.EmailFileAttachment[] emailAttachments = new list<Messaging.EmailFileAttachment>();
                for(Attachment a:attachmentList){
                    Messaging.EmailFileAttachment ea = new Messaging.EmailFileAttachment();
                    ea.setBody(a.Body);
                    ea.setFileName(a.Name);
                    emailAttachments.add(ea);
                }
                singleEmailMsg.setFileAttachments(emailAttachments);
            }
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {singleEmailMsg});
            
            emailMsg.HtmlBody = emailMsg.Body__c;
            emailMsg.TextBody = emailMsg.Body__c.stripHTMLTags();
            if(attachmentList != null && attachmentList.size() > 0){
                String[] aCountList = new list<String>();
                for(attachment a:attachmentList){
                    if(a.Name.contains('.doc') || a.Name.contains('.docx') || a.Name.contains('.pdf') ||
                       a.Name.contains('.DOC') || a.Name.contains('.DOCX') || a.Name.contains('.PDF')){
                           aCountList.add(a.Name);
                       }
                }
                emailMsg.Num_of_Attachments__c = aCountList.size();
            }
            insert emailMsg;
            if(attachmentList != null && attachmentList.size() > 0){
                attachment[] emailAttachmentList = new list<attachment>();
                for(attachment a:attachmentList){
                    attachment na = new attachment();
                    na.ParentId = emailMsg.id;
                    na.Body = a.Body;
                    na.Name = a.Name;
                    emailAttachmentList.add(na);
                }
                try {
                    insert emailAttachmentList;
                    delete attachmentList;
                } catch(exception e) {
                    salesforceLog.createLog('Case', true, 'authoringDeliveryEmail', 'sendEmail', +string.valueOf(e));
                }
            }
            PageReference casedetail = Page.casedetail;
            casedetail.setRedirect(true);
            casedetail.getParameters().put('id',caseId);
            return casedetail;
        }
        if(emailMsg.Num_of_Docs__c == null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'# of Docs: Must inlcude # of docs.');
            ApexPages.addMessage(myMsg);
        }
        if(emailMsg.Subject == null || emailMsg.Subject == ''){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Subject: Must inlcude a subject.');
            ApexPages.addMessage(myMsg);
        }
        if(ourAttachment.body != null || ourAttachment2.body != null || ourAttachment3.body != null || ourAttachment4.body != null || ourAttachment5.body != null){
            ourAttachment = new attachment();
            ourAttachment2 = new attachment();
            ourAttachment3 = new attachment();
            ourAttachment4 = new attachment();
            ourAttachment5 = new attachment();
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Attachment(s) not added: Must click "Attach" after selecting file(s).');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
    public Pagereference cancel(){
        delete attachmentList;
        PageReference casedetail = Page.casedetail;
        casedetail.setRedirect(true);
        casedetail.getParameters().put('id',caseId);
        return casedetail;
    }
    
    
    
}