public class adminToolAPIManualRun implements Queueable, Database.AllowsCallouts {
    /**
* This is a method that is used to manually call the Admin Tool API
* Available Reports: 
*   CustomerData = Returns Customers (Accounts) 9.0
*   CustomerData85 = Returns Customers (Accounts) 8.5
*   UserData = Returns Users (Contacts)
*   BookingsReport = Returns Order Items
*
* Environment information:
*   If running this in the sandbox, enviroment should be "Staging"
*   If running this in Production, environment should be "Production"
*
* This can only be run as Automated Process
* Execute Anonymous Code:
*   adminToolAPIManualRun atManual = new adminToolAPIManualRun('REPORT NAME','DATE','ADMIN TOOL ENVIRONMENT');
*   ID jobID = System.enqueueJob(atManual);
*
**/
    private string report;
    private string dataDate;
    private string environment;
    
    public adminToolAPIManualRun(string reportName, string startDate, string atEnvironment) {
        this.report = reportName;
        this.dataDate = startDate;
        this.environment = atEnvironment;
    }
    
    public void execute(QueueableContext context) {        
        string adminToolData = adminToolWebServices.fetchAdminToolData(report, dataDate, environment);
        adminToolUtility atUtility = new adminToolUtility();
        if(adminToolData != NULL && (report =='CustomerData' || report =='CustomerData85')){
            atUtility.upsertCustomerData(adminToolData);
        }
        if(adminToolData != NULL && report =='UserData'){
            atUtility.upsertUserData(adminToolData);
        }
        if(adminToolData != NULL && report =='BookingsReport'){
            atUtility.upsertOrderData(adminToolData);
        }
    }
}