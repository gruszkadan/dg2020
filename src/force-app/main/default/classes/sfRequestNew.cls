public with sharing class sfRequestNew {
    public User u {get;set;}
    public QueueSobject q {get;set;}
    public QueueSobject qm {get;set;}
    public Attachment myfile;
    public Attachment getmyfile()
    {
        myfile = new Attachment();
        return myfile;
    }
    public String attachmentMessage {get;set;}  
    public string pagetype {get;set;}
    public List<sfMergesWrapper> wrappers {get; set;}
    public static Integer toDelIdent {get; set;}
    public static Integer addCount {get; set;}
    private Integer nextIdent=0;
    public Salesforce_Request__c theRequest {get;set;}
    public Salesforce_Request__c newReq {get;set;}
    public Salesforce_Request_Merge__c[] theMerges {get;set;}
    
    private Apexpages.StandardController controller; 
    
    public sfRequestNew(ApexPages.StandardController stdController) {
         pagetype = Apexpages.currentPage().getParameters().get('ptype');
        this.request = (Salesforce_Request__c)stdController.getRecord();
        this.controller = stdController;
        this.newReq = new Salesforce_Request__c();
        u = [Select id, Name, LastName, Email, ManagerID, Department__c from User where id =: UserInfo.getUserId() LIMIT 1];
        q = [Select QueueId From QueueSobject where SobjectType='Salesforce_Request__c' AND Queue.Name='SF Request Queue'];
        qm = [Select QueueId From QueueSobject where SobjectType='Salesforce_Request__c' AND Queue.Name='SF Merge Queue'];
        Salesforce_Request__c sfr = (Salesforce_Request__c)controller.getRecord();
        if(u.LastName=='McCauley'  || u.LastName=='Berkowitz' || u.LastName=='Werner'){
            sfr.OwnerId = q.QueueId;
        } else {
            sfr.Requested_By__c = u.Id;
            sfr.OwnerId = q.QueueId;
        }
        wrappers=new List<sfMergesWrapper>();
        for (Integer idx=0; idx<3; idx++)
        {
            wrappers.add(new sfMergesWrapper(nextIdent++));
        }
    }
    
    Salesforce_Request__c request;
    
    public Salesforce_Request__c getRequest()
    {
        if(request == null) request = new Salesforce_Request__c();
        return request;
    }
    
    public void addFile()
    {
        try{   
            Attachment a = new Attachment(parentId = request.Id, name=myfile.name, body = myfile.body);
            
            /* insert the attachment */
            insert a;
            this.attachmentMessage = 'Your File Has Been Added!'; 
            
        }
        
        catch(Exception e)
        {
            this.attachmentMessage = e.getMessage();
        }
    }
    
    public PageReference onSave()
    {
        if(u.LastName=='McCauley'  || u.LastName=='Berkowitz'){
            request.OwnerId = q.QueueId; 
        } else {
            request.Requested_By__c = u.id;
            request.OwnerId = q.QueueId; 
        }        
        insert request;
        addFile();
        PageReference requestPage = new ApexPages.StandardController(request).view();
        requestPage.setRedirect(true);
        return requestPage;
    }   
    public void delWrapper()
    {
        Integer toDelPos=-1;
        for (Integer idx=0; idx<wrappers.size(); idx++)
        {
            if (wrappers[idx].ident==toDelIdent)
            {
                toDelPos=idx;
            }
        }
        
        if (-1!=toDelPos)
        {
            wrappers.remove(toDelPos);
        }
    }
    
    public void addRows()
    {
        for (Integer idx=0; idx<addCount; idx++)
        {
            wrappers.add(new sfMergesWrapper(nextIdent++));
            
        }
    }
    
    public PageReference save()
    {
        
        this.newReq.Request_Type__c = 'Merge';
        this.newReq.Summary__c = 'Salesforce Merge';
        this.newReq.OwnerId = qm.QueueId;
        this.newReq.requested_by__c = u.id;
        insert this.newReq;
        
        List<Salesforce_Request_Merge__c> sftcs=new List<Salesforce_Request_Merge__c>();
        for (sfMergesWrapper wrap : wrappers)
        {
            
            wrap.sftc.Salesforce_Request__c = newReq.Id;
            if(wrap.sftc.secondary_account_url__c != null){
                sftcs.add(wrap.sftc);
            }
        }
        
        
        insert sftcs;
        
        
        
        
        
        return new PageReference('/' + newReq.ID + '?ptype=merge');
    }
    
    
    
    
    public class sfMergesWrapper
    {
        public Salesforce_Request_Merge__c sftc {get; private set;}
        public Integer ident {get; private set;}
        
        
        public sfMergesWrapper(Integer inIdent)
        {
            
            ident=inIdent;
            sftc=new Salesforce_Request_Merge__c();
        }
    }
    
    public PageReference saveEdits(){
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(theMerges.size()>0)
                upsert(theMerges);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
        return new PageReference('/' + theRequest.ID);
    }
    
    
    
    
}