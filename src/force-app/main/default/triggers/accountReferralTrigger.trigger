trigger accountReferralTrigger on Account_Referral__c 
(after insert) {

    if (trigger.isAfter) {
        if (trigger.isInsert) {
            accountReferralTriggerHandler.afterInsert(trigger.new);
        }
    }
    
}