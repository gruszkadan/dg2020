@isTest
private class testOrderItemTrigger {

    private static testmethod void test1() {
        test.startTest();
        testDataUtility util = new testDataUtility();
        Order_Item__c oi = util.createOrderItem('TestVP', 'test123');
        insert oi;
        oi.Invoice_Amount__c = 100;
        oi.Admin_Tool_Order_Status__c = 'C';
        update oi;
        test.stopTest();
    }    
}