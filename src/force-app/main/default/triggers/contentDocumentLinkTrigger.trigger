trigger contentDocumentLinkTrigger on ContentDocumentLink (after insert) {
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            contentDocumentLinkTriggerHandler.afterInsert(trigger.old,trigger.new);
        }
    }
}