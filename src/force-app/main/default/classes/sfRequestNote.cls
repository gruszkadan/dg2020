public with sharing class sfRequestNote {
    private Apexpages.StandardController controller;
    public Salesforce_Request__c currentRequest {get;set;} 
    public Salesforce_Request_Note__c note;
    public User u {get;set;}
    public QueueSobject[] q {get;set;}
    public EmailTemplate e {get;set;}
    public string environment {get;set;}
    
    public sfRequestNote(ApexPages.StandardController stdController) {
        this.note = (Salesforce_Request_Note__c)stdController.getRecord();
        Id requestID= System.currentPageReference().getParameters().get('CF00N800000056H7C_lkid'); 
        currentRequest = [Select ID, Name, OwnerID, Owner.Email, Requested_By__c, Requested_By__r.Email, Owner.Type from Salesforce_Request__c where ID =:requestID];
        this.note.Salesforce_Update__c = currentRequest.id;
        u = [Select id, Name, FirstName, Email, Username from User where id =: UserInfo.getUserId() LIMIT 1];
        q = [Select QueueId From QueueSobject where SobjectType = 'Salesforce_Request__c'];
        e = [Select Id From EmailTemplate where Name='Salesforce Request Note Notification'];
        
        if(u.ID == currentRequest.Requested_By__c){
            this.note.CC_Recipients__c = u.Email; 
            if(currentRequest.Owner.Type !='Queue'){
                this.note.To_Recipient__c = currentRequest.Owner.Email;
            } else {
                this.note.To_Recipient__c = 'mmccauley@ehs.com'; 
            }
        }
        if(u.ID == currentRequest.OwnerID){
            this.note.CC_Recipients__c = u.Email;
            this.note.To_Recipient__c = currentRequest.Requested_By__r.Email;
        }
        if(u.ID != currentRequest.OwnerID && u.ID != currentRequest.Requested_By__c){
            this.note.CC_Recipients__c = u.Email+','+currentRequest.Requested_By__r.Email;
            if(currentRequest.Owner.Type !='Queue'){
                this.note.To_Recipient__c = currentRequest.Owner.Email;
            } else {
                this.note.To_Recipient__c = 'mmccauley@ehs.com'; 
            }
        }
        ID orgID = UserInfo.getOrganizationId();
        if(orgID =='00D300000001HEfEAM'){
            environment = 'Production';
        } else {
            environment ='Sandbox';
        }
    }
    
    public void SendEmail(){
        Salesforce_Request_Note__c sfNote = [select ID, CC_Recipients__c from Salesforce_Request_Note__c where ID = :note.Id ];
        if(environment =='Sandbox'){
            this.note.To_Recipient__c = [SELECT ID, Email FROM USER WHERE FirstName = 'Mark' AND LastName = 'McCauley' AND isActive= TRUE Limit 1].Email;
        }
        string ehsEmail = this.note.To_Recipient__c;
        string msdsonlineEmail = this.note.To_Recipient__c.replace('@ehs.com', '@msdsonline.com');
        string kmiEmail = this.note.To_Recipient__c.replace('@ehs.com', '@kminnovations.com');
        User sendToUser;
        if(environment =='Production'){
           sendToUser = [SELECT ID 
                               FROM User 
                               WHERE UserName = :this.note.To_Recipient__c
                               AND (Email = :ehsEmail OR Email = :msdsonlineEmail OR Email = :kmiEmail)
                               LIMIT 1];
        } else {
            sendToUser = [SELECT ID FROM User WHERE ID='00580000003UChI' LIMIT 1];
        }
        
        String[] ccAddresses = new String[] {'salesforcerequestnote@e-19mnzwu5pe5r12kph6zprduwgnim4u0ud59q46vkzekyn3t3gs.3-1hefeam.na29.apex.salesforce.com'}; 
            if(sfNote.CC_Recipients__c != null){
                String[] ccAddressesSplit = this.note.CC_Recipients__c.split(',');
                ccAddresses.addAll(ccAddressesSplit);
            }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if(environment == 'Sandbox' ){
            mail.setTargetObjectId('00580000003UChI');
        }
        if(environment =='Production'){
            mail.setTargetObjectId(sendToUser.Id);
        }
        mail.setCcAddresses(ccAddresses);
        mail.setWhatId(this.note.Id);
        mail.setTemplateId(e.Id);
        mail.setSaveAsActivity(FALSE);
        mail.setUseSignature(FALSE);
        mail.setSenderDisplayName(u.Name);
        mail.setReplyTo(u.Email);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public PageReference onSave()
    {
        insert note;
        SendEmail();
        PageReference requestPage = new ApexPages.StandardController(currentRequest).view();
        requestPage.setRedirect(true);
        return requestPage;
    }
    
    
    
}