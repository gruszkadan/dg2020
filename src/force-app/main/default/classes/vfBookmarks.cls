public with sharing class vfBookmarks 
{
    public User u {get; set;}
    public msdsCustomSettings cs = new msdsCustomSettings();
    public ApexPages.StandardController controller;
    public notificationHomePageController notifCon = new notificationHomePageController(controller);
    public Boolean showMonthlyReview {get; set;}
    public integer numPopups {get; set;}
    public boolean isAudience {get; set;}
    public integer popupHeight {get; set;}
    
    public vfBookmarks()
    {
        u = [Select id, Name, FirstName, Role__c, Email, ManagerID, Department__c, Group__c, ProfileID from User where id =: UserInfo.getUserId() LIMIT 1];
        numPopups = [SELECT id FROM Notification_Audience__c WHERE User__c = :UserInfo.getUserId() AND Notification_Read__c = false AND Notification__r.Status__c != 'Draft' AND Notification__r.Status__c != 'Scheduled' AND Notification__r.Publish_Date__c < :datetime.now()].size();
        showMonthlyReview = cs.viewMonthlyReview();
        if (numPopups == 1) {popupHeight = 400;}
		if (numPopups == 2) {popupHeight = 422;}
		if (numPopups == 3) {popupHeight = 451;}
        if (numPopups == 4) {popupHeight = 480;}
        if (numPopups == 5 || numPopups > 5) {popupHeight = 510;}
    }
}