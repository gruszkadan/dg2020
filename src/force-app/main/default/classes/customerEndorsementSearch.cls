public with sharing class customerEndorsementSearch {
    private ApexPages.StandardController stdCtrl;
    public Customer_Story_Settings__c settings {get;set;}
    public Customer_Story_Settings__c[] settingslist {get;set;}
    public String naicsInput{get;set;}
    public String midMarketEsOption{get;set;}
    public String storyTypeInput{get;set;}
    public String stateInput{get;set;}
    public String countyInput{get;set;}
    public String zipInput{get;set;}    
    public String focus1Input{get;set;}
    public String testimonialTypeInput{get;set;} 
    public string statusInput{get;set;}
    public String soql{get;set;}
    public integer totalStories {get;set;}
    public integer OffsetSize {get;set;}
    public integer LimitSize {get;set;}
    public integer totalSize {get;set;}
    public Customer_Endorsement__c newstory {get;set;}
    public Customer_Endorsement__c numtimes {get;set;}
    public User u{get;set;}
    public List <County_Zip_Code__c> zips{get;set;}
    public List <Customer_Endorsement__c> stories{get;set;}
    public List <Customer_Endorsement__c> stories2{get;set;}
    public list <Account> soslAccounts {get;set;}
    public String sId{get;set;}
    public Customer_Endorsement__c selectedStory{get;set;}   
    public Customer_Endorsement__c allstory{get;set;}
    public string selectedCounty{get;set;}
    public Customer_Endorsement__c popupStory{get;set;}
    public String story{get;set;}
    public string CustomerStoriesPicklist{get;set;}
    public boolean noresults{get;set;}
    public boolean uptodate{get;set;}
    public string emailinput {get;set;}
    public boolean input_hq {get;set;}
    public boolean input_hqregxr {get;set;}
    public boolean input_authoring {get;set;}
    public boolean input_incidentmanagement {get;set;}
    public boolean input_plan1 {get;set;}
    public boolean input_complianceeducation {get;set;}
    public boolean input_workplacetraining {get;set;}
    public string f1 {get;set;}
    public string f2 {get;set;}
    public string f3 {get;set;}
    public string f4 {get;set;}
    public string f5 {get;set;}
    public string f6 {get;set;}
    public string f7 {get;set;}
    public Customer_Endorsement__c s {get;set;}
    public boolean selectall {get;set;}
    public boolean showatrisk {get;set;}
    public boolean showinactive {get;set;}
    public date sixmonths {get;set;}
    public string caseURL {get;set;}
    public string caseURL2 {get;set;}
    public string acctNameSearch {get;set;}
    public string formerSDSSolutionSearch {get;set;}
    private msdsCustomSettings helper = new msdsCustomSettings();
    public formerSDSSolution[] formerSDSSolutions {get;set;}
    public string exceptionE {get;set;}
    public string searchQuery {get;set;}
    public storyWrapper[] storyWrappers {get;set;}
    
    public customerEndorsementSearch(ApexPages.StandardController controller) {
        storyWrappers = new list<storyWrapper>();
        stdCtrl=Controller;
        sixmonths = Date.Today().adddays(-182);
        u = [Select id, Name, FirstName, Email, ManagerID, Department__c, Group__c, ProfileID, Role__c, UserRole.Name from User where id =: UserInfo.getUserId() LIMIT 1];
        Id storyId = ApexPages.currentPage().getParameters().get('id');
        noresults = true;
        CustomerStoriesPicklist = helper.stypes();
        selectall = true;
        if(storyid != null){
            this.s = [Select Id, Account__c, Account__r.Current_Chemical_Management_System__c, Account__r.Active_Products__c, Account__r.Current_Competitor_System__c, Contact__c, Story_Name__c, focus_hq__c, focus_authoring__c, focus_hq_reg_xr__c, focus_incident_management__c, focus_compliance_education__c, focus_plan1__c, focus_workplace_training__c, Case_Study_Link__c, Account__r.NAICS_Code__c, Account__r.At_Risk__c, Story_Type__c, Account__r.BillingState, Story__c, Testimonial_Type__c, Account__r.Territory__c, Reference_Times_Used__c, Reference_Date_Used__c from Customer_Endorsement__c WHERE id =: storyid LIMIT 1];
        } else {
            this.newstory = new Customer_Endorsement__c();
        }  
        totalStories = 0;
        OffsetSize = 0;
        LimitSize = 100;
        totalSize=0;
        formerSDSSolutions = new list<formerSDSSolution>();
        findFormerSDSSolutions();
    }
    
    public void findFormerSDSSolutions() {
        Schema.DescribeFieldResult dfr = Account.Current_Competitor_System__c.getDescribe();
        Schema.PicklistEntry[] vals = dfr.getPicklistValues();
        for (Schema.PicklistEntry val : vals) {
            formerSDSSolutions.add(new formerSDSSolution(val.getValue())); 
        }
    }
    
    public void soslSearch(){
        string aNames = acctNameSearch;
        if(aNames != null){
            string[] acctNames = aNames.split(',');
            string acNames;
            
            for (integer i=0; i<acctNames.size(); i++) {
                string thisAcctName = acctNames[i].trim();
                //if has a apostophe
                if(thisAcctName.contains('\'')){
                    String x = thisAcctName.replace('\'', '');
                    string y = String.escapeSingleQuotes(thisAcctName);
                    thisAcctName = x+' OR ' + y;
                } else {
                    //if no apostrophes
                    String fullName = thisAcctName;
                    string lastLet = thisAcctName.right(1);
                    string lastLetterRemoved = thisAcctName.removeEnd(lastLet);
                    string splitName;
                    String formattedName = '';
                    for(String s:thisAcctName.split(' ')){
                        string lastLetter = s.right(1);
                        if(lastLetter == 's'){
                            if(formattedName == ''){
                                if(thisAcctName.split(' ').size() > 1){
                                    splitName = s;
                                }
                                formattedName = s.removeEnd(lastLetter);
                            }else{
                                splitName += ' OR '+s;
                                formattedName += ' '+s.removeEnd(lastLetter);
                            }
                        }
                    }
                    thisAcctName = fullName;
                    if(splitName != null){
                        thisAcctName +=' OR ' + splitName;
                        thisAcctName +=' OR ' + lastLetterRemoved;
                    }
                    if(formattedName != ''){
                        thisAcctName +=' OR ' + formattedName;
                    }
                }
                if (i == 0) {
                    acNames = thisAcctName ;
                } else {
                    acNames += ' OR ' + thisAcctName;
                } 
            }
            searchQuery = 'FIND {'+ acNames + '} IN ALL FIELDS RETURNING Account(id,name)'; 
            List<List<SObject>>searchList=search.query(searchquery);
            soslAccounts = (Account[])searchList[0];
        }
    }
    
    public void find() {
        storyWrappers.clear();
        if(acctNameSearch != NULL && acctNameSearch !=''){
            soslSearch();
        }
        
        try {
            system.debug('soslquery '+soslAccounts);
            if(input_hq == true){
                f1 = 'Focus_HQ__c = true';   
            }else{
                f1 = 'Story_Type__c = \'Reference\'';
            }
            if(input_hqregxr  == true){
                f2 = 'Focus_HQ_Reg_XR__c = true';   
            }else{
                f2 = 'Story_Type__c = \'Reference\'';
            }
            if(input_authoring == true){
                f3 = 'Focus_Authoring__c = true';   
            }else{
                f3 = 'Story_Type__c = \'Reference\'';
            }
            if(input_incidentmanagement == true){
                f4 = 'Focus_Incident_Management__c = true';   
            }else{
                f4 = 'Story_Type__c = \'Reference\'';
            }
            if(input_complianceeducation == true){
                f5 = 'Focus_Compliance_Education__c = true';   
            }else{
                f5 = 'Story_Type__c = \'Reference\'';
            }
            if(input_workplacetraining == true){
                f6 = 'Focus_Workplace_Training__c = true';   
            }else{
                f6 = 'Story_Type__c = \'Reference\'';
            }
            if(input_plan1 == true){
                f7 = 'Focus_Plan1__c = true';   
            }else{
                f7 = 'Story_Type__c = \'Reference\'';
            }      
            soql = 'SELECT Id, Survey_Source__c, Account__r.BillingPostalCode, Status__c, Account__c, Account__r.Billing_County__c, '+
                'Account__r.Current_Chemical_Management_System__c, Reference_Times_Used_6_Months__c, Account__r.Active_Products__c, '+
                'Account__r.Current_Competitor_System__c, Contact__c, Story_Name__c,Case_Study_Link__c, Account__r.NAICS_Code__c, '+
                'Account__r.At_Risk__c, Story_Type__c, Account__r.BillingState, Account__r.Enterprise_Sales__c, Account__r.VIP__c, Story__c, Focus_HQ__c, Focus_Authoring__c, Focus_HQ_Reg_XR__c, '+
                'Focus_Incident_Management__c, Focus_Compliance_Education__c, Focus_Plan1__c, Focus_Workplace_Training__c, Testimonial_Type__c, '+
                'Account__r.Territory__c, Reference_Times_Used__c, Reference_Date_Used__c, Account__r.Num_Of_Employees__c, Account__r.Licensing_Base_Amount__c, Account__r.Num_Of_Locations__c '+
                'FROM Customer_Endorsement__c where Story_Type__c = \'' + storyTypeInput + '\' AND Status__c = \'Active\'';
            if(acctNameSearch != NULL && acctNameSearch !=''){
                soql += 'AND Account__c in: soslAccounts';  
            }
            if (formerSDSSolutions.size() > 0) {
                string[] selVals = new list<string>();
                for (formerSDSSolution sol : formerSDSSolutions) {
                    if (sol.selected) {
                        selVals.add(sol.val);
                    }
                }
                if (selVals.size() > 0) {
                    for (integer i=0; i<selVals.size(); i++) {
                        if (i == 0) {
                            soql += ' AND (Account__r.Current_Competitor_System__c = \'' + selVals[i] + '\''; 
                        } else {
                            soql += ' OR Account__r.Current_Competitor_System__c = \'' + selVals[i] + '\''; 
                        } 
                    }
                    soql += ')';
                }
            }
            if(showatrisk != true)
                soql += ' AND Account__r.At_Risk__c = FALSE';
            if(showinactive != true)
                soql += ' AND Account__r.Customer_Status__c = \'Active\'';
            if(midMarketEsOption == 'Mid-Market')
                soql += ' AND Account__r.Enterprise_Sales__c = false ';
             if(midMarketEsOption == 'Enterprise Sales')
                soql += ' AND Account__r.Enterprise_Sales__c = true ';
            if (naicsInput != 'Select')
                soql += ' AND Account__r.NAICS_Code__c = \''+String.escapeSingleQuotes(naicsInput)+'\''; 
            if (stateInput != 'Select' && zipinput != '')
                soql += ' AND Account__r.BillingPostalCode = \''+String.escapeSingleQuotes(zipInput)+'\''; 
            if (stateInput == 'Select' && zipinput != '')
                soql += ' AND Account__r.BillingPostalCode = \''+String.escapeSingleQuotes(zipInput)+'\'';       
            if (stateInput != 'Select' && zipinput == '')
                soql += ' AND Account__r.BillingState = \''+String.escapeSingleQuotes(stateInput)+'\'';       
            if (stateInput != 'Select' && countyinput != '' && zipinput =='')
                soql += ' AND Account__r.BillingState = \''+String.escapeSingleQuotes(stateInput)+'\' AND Account__r.Billing_County__c = \''+String.escapeSingleQuotes(countyInput)+'\''; 
            if(storytypeinput == 'Select')
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Please select an endorsement type'));
            if (storyTypeInput !='Select')
                soql += ' AND Story_Type__c = \''+String.escapeSingleQuotes(storyTypeInput)+'\'';  
            if (storyTypeInput == 'Reference')
                soql += ' AND Story_Type__c = \''+String.escapeSingleQuotes(storytypeinput)+'\'';  
            if (storyTypeInput == 'Testimonial' &&  testimonialtypeinput == 'Select')
                soql += '';  
            if (storyTypeInput == 'Testimonial' &&  testimonialtypeinput != 'Select')
                soql += ' AND Testimonial_Type__c = \''+String.escapeSingleQuotes(testimonialTypeInput)+'\'';
            if (storyTypeInput == 'Case Study' )
                soql += ' AND ('+f1+' OR '+f2+' OR '+f3+' OR '+f4+' OR '+f5+' OR '+f6+' OR '+f7+')'; 
            if (storyTypeInput == 'Testimonial' &&  testimonialtypeinput == 'Select')
                soql += ' AND Story_Type__c = \''+String.escapeSingleQuotes(storyTypeInput)+'\'';  
            stories = Database.query(soql);
            totalStories = stories.size();
            soql += ' LIMIT ' + limitSize + ' OFFSET ' + offsetSize;
            system.debug('query is' +soql);
            stories = Database.query(soql);
            system.debug(soql);
            totalSize = limitSize + OffsetSize;
            if(stories.size() > 0){
                noresults = false;
                createStoryWrapper();
            }
            if(stories.size() < 1){
                noresults = true;
            }  
        } catch (exception e) {
            exceptionE = e.getMessage().capitalize();
        }
    }
    
    public void createStoryWrapper(){
        set <id> accountIds = new set<id>();
        Order_Item__c[] oi = new list <Order_Item__c>();
        for(Customer_Endorsement__c st: stories){
            accountIds.add(st.Account__c);
        }
        
        oi = [Select Invoice_Amount__c, Account__c from Order_Item__c where Admin_Tool_Product_Name__c LIKE 'HQ%' and Account__c in: accountIds];
        for(Customer_Endorsement__c s : stories){
            storyWrapper wrapper;
            decimal hqAmount = null;
            Order_Item__c[] hqOrders = new list<Order_Item__c>();
            for(Order_Item__c o:oi){
                if(o.Account__c == s.Account__c){
                    hqOrders.add(o);
                }
            }
            if(hqOrders.size() > 0){
                wrapper = new storyWrapper(s,hqOrders[0].Invoice_Amount__c);
            }else{
                wrapper = new storyWrapper(s,0.00);
            }
            storyWrappers.add(wrapper);
        }
    }
    
    public list<string> getfocuslist() {
        list<string> foc = new List<string>();
        if(this.s.focus_hq__c == TRUE){
            foc.add('HQ');
        }
        if(this.s.focus_hq_reg_xr__c == TRUE){
            foc.add('HQ Reg XR');
        }
        if(this.s.focus_authoring__c == TRUE){
            foc.add('Authoring');
        }
        if(this.s.focus_incident_management__c == TRUE){
            foc.add('Incident Management');
        }
        if(this.s.focus_Compliance_Education__c == TRUE){
            foc.add('Compliance Education');
        }
        if(this.s.focus_plan1__c == TRUE){
            foc.add('Plan1');
        }
        if(this.s.focus_Workplace_training__c == TRUE){
            foc.add('Workplace Training');
        }
        return foc;        
    }
    
    public class storyWrapper{
        public Customer_Endorsement__c s {get;set;}
        public decimal hqDollars {get;set;}
        
        storyWrapper(Customer_Endorsement__c st, decimal hq){
            s = st;
            hqDollars = hq;
        }
    }
    
    
    public void queryUptoDate() {
        if(noresults = true){
            story = ApexPages.currentPage().getParameters().get('sid');
            selectedstory = [SELECT Id, Name, Reference_Date_Used__c, Story_Name__c,  Testimonial_Type__c FROM Customer_Endorsement__c WHERE Id =:story LIMIT 1];
            if(selectedstory.Reference_Date_Used__c != Date.TODAY()){
                uptodate = false;
            }
            if(selectedstory.Reference_Date_Used__c == Date.TODAY()){
                uptodate= TRUE;
            } 
        }
    }
    
    public PageReference updateReferenceUsed() {
        story = ApexPages.currentPage().getParameters().get('sid');
        selectedstory = [SELECT Id,Reference_Times_Used_6_Months__c, Name, Contact__r.firstname,Contact__r.lastname, Account__r.name, Contact__r.email, Account__r.BillingStreet,Account__r.BillingCity,Account__r.BillingState,Account__r.BillingPostalCode, Reference_Date_Used__c,Contact__r.phone, Story_Name__c,  Reference_Times_Used__c, Testimonial_Type__c FROM Customer_Endorsement__c WHERE Id =:story LIMIT 1];  
        return null;
    }
    
    public PageReference updateReferenceUsed2() {
        getEmailInput();
        settings = new Customer_Story_Settings__c();
        settings.Name = 'REF-'+DateTime.now();
        settings.Date_Used__c = Date.Today();
        settings.Reference_Name__c = selectedstory.Story_Name__c;
        settings.Reference_Id__c = selectedstory.Id;
        insert settings;
        settingslist = [SELECT Id, Name, Reference_Id__c, Date_Used__c FROM Customer_Story_Settings__c WHERE Date_Used__c >= :sixmonths AND Reference_Id__c =:selectedstory.Id];
        selectedstory.Reference_Times_Used_6_Months__c = settingslist.size() ;
        selectedstory.Reference_Date_Used__c = Date.TODAY(); 
        if (selectedstory.reference_times_used__c == 0){
            selectedstory.reference_times_used__c = 1;
        }
        update selectedstory;  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        string body =  '<html><body><table><tr><td style="width:70px"><b>Account:</b></td><td> '+selectedstory.Account__r.name+'</td></tr><tr><td> <b>Contact:</b> </td><td>'+selectedstory.Contact__r.firstname+' '+selectedstory.Contact__r.lastname+'</td></tr><tr><td><b>Email:</b></td><td> '+selectedstory.Contact__r.email+'</td></tr><tr><td><b>Phone:</b></td><td> '+selectedstory.Contact__r.phone+
            '</td></tr><tr><td><b>Billing:</b></td><td> '+selectedstory.Account__r.BillingStreet+'</td></tr><tr><td></td><td>'+selectedstory.Account__r.BillingCity+', '+selectedstory.Account__r.BillingState+' '+selectedstory.Account__r.BillingPostalCode+'</td></tr></table></body></html>';
        String[] toAddresses = new String[] {emailinput};   
            mail.setToAddresses(toAddresses);  
        mail.setSubject('Reference Information');    
        mail.setSaveAsActivity(false);    
        for(OrgWideEmailAddress owa : [select id, Address, DisplayName from OrgWideEmailAddress]) {  
            if(owa.Address.contains('testuser'))   
                mail.setOrgWideEmailAddressId(owa.id);   
        }  
        mail.setHtmlBody(body);    
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
        return null;
    }
    
    public void FirstPage() {
        OffsetSize = 0;
        find();
    }
    
    public void previous() {
        OffsetSize = OffsetSize - LimitSize;
        find();
    }
    
    public void next() {
        OffsetSize = OffsetSize + LimitSize;
        find();
    }
    
    public void LastPage() {
        OffsetSize = totalStories - math.mod(totalStories,LimitSize);
        find();
    }
    
    public boolean getprev() {
        if(OffsetSize == 0)
            return true;
        else
            return false;
    }
    
    public boolean getnxt() {
        if((OffsetSize + LimitSize) >= totalStories)
            return true;
        else
            return false;
    }
    
    public pagereference gotoPopup() {
        story = ApexPages.currentPage().getParameters().get('sid');
        popupStory = [SELECT Id, Name, Reference_Date_Used__c, Story__c, Story_Type__c, Story_Name__c, Testimonial_Type__c FROM Customer_Endorsement__c WHERE Id =:story LIMIT 1];
        return null;
    }
    
    public string getEmailInput() {
        return emailinput;
    }
    
    public void alls() {
        Id storyid2 = ApexPages.currentPage().getParameters().get('id');
        allstory = [Select Id, Account__c, Account__r.Current_Chemical_Management_System__c, Account__r.Active_Products__c, Account__r.Current_Competitor_System__c, Contact__c, Story_Name__c, focus_hq__c, focus_authoring__c, focus_hq_reg_xr__c, focus_incident_management__c, focus_compliance_education__c, focus_plan1__c, focus_workplace_training__c, Case_Study_Link__c, Account__r.NAICS_Code__c, Account__r.At_Risk__c, Story_Type__c, Account__r.BillingState, Story__c, Testimonial_Type__c, Account__r.Territory__c, Reference_Times_Used__c, Reference_Date_Used__c from Customer_Endorsement__c WHERE id =: storyid2 LIMIT 1];
        if(selectall == true){
            allstory.focus_hq__c = true;
            allstory.focus_hq_reg_xr__c = true;
            allstory.focus_authoring__c = true;
            allstory.focus_incident_management__c = true;
            allstory.focus_compliance_education__c = true;
            allstory.focus_plan1__c = true;
            allstory.focus_workplace_training__c = true;
            stdCtrl.save();
        }else{
            allstory.focus_hq__c = false;
            allstory.focus_hq_reg_xr__c = false;
            allstory.focus_authoring__c = false;
            allstory.focus_incident_management__c = false;
            allstory.focus_compliance_education__c = false;
            allstory.focus_plan1__c = false;
            allstory.focus_workplace_training__c = false;
            stdCtrl.save();
        }
    }   
    
    public list<selectoption> getStatusList() {
        List<SelectOption> options = new List<SelectOption>();   
        options.add(new SelectOption('Select', '- All Statuses -')); 
        options.add(new SelectOption('Pending', 'Pending')); 
        options.add(new SelectOption('Active', 'Active')); 
        options.add(new SelectOption('Archived', 'Archived')); 
        options.add(new SelectOption('Problematic', 'Problematic')); 
        return options;
    }
    
    public list<selectoption> getMidMarketEnterpriseSales() {
        List<SelectOption> options = new List<SelectOption>();   
        options.add(new SelectOption('- All -', '- All -')); 
        options.add(new SelectOption('Mid-Market', 'Mid-Market')); 
        options.add(new SelectOption('Enterprise Sales', 'Enterprise Sales')); 
        return options;
    }
    

    public List<SelectOption> getStateOptions() { 
        List<SelectOption> options = new List<SelectOption>();      
        options.add(new SelectOption('Select', '- All -')); 
        Schema.DescribeFieldResult fieldResult = County_Zip_Code__c.State__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry entry : picklistEntries){
            options.add(new SelectOption(entry.getValue(), entry.getLabel())); 
        }
        return options;
    }
    
    public List<SelectOption> getNAICSOptions() { 
        List<SelectOption> options = new List<SelectOption>();      
        options.add(new SelectOption('Select', '- All -')); 
        Schema.DescribeFieldResult fieldResult = Account.NAICS_Code__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry entry : picklistEntries){
            options.add(new SelectOption(entry.getValue(), entry.getLabel())); 
        }
        return options;
    }
    
    public List<SelectOption> getStoryTypeOptions() { 
        List<SelectOption> options = new List<SelectOption>();      
        options.add(new SelectOption('Select', '- Select -')); 
        List<string> parts = CustomerStoriesPicklist.split(',',0);
        for(string part : parts){
            options.add(new selectoption(part, part));
        }
        return options;
    }
    
    public List<SelectOption> getTestimonialTypeOptions() { 
        List<SelectOption> options = new List<SelectOption>();      
        options.add(new SelectOption('Select', '- All -')); 
        Schema.DescribeFieldResult fieldResult = Customer_Endorsement__c.Testimonial_Type__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry entry : picklistEntries){
            options.add(new SelectOption(entry.getValue(), entry.getLabel())); 
        }
        return options;
    }
    
    public List<SelectOption> getProductTypes() { 
        List<SelectOption> options = new List<SelectOption>();      
        options.add(new SelectOption('Select', '- All -')); 
        Schema.DescribeFieldResult fieldResult = Customer_Endorsement__c.Testimonial_Type__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry entry : picklistEntries){
            options.add(new SelectOption(entry.getValue(), entry.getLabel())); 
        }
        return options;
    }
    
    public List<SelectOption> getFocusOptions() { 
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Select', '- All -')); 
        options.add(new SelectOption('HQ;', 'HQ')); 
        options.add(new SelectOption('Authoring;', 'Authoring')); 
        options.add(new SelectOption('Select', '- All -')); 
        options.add(new SelectOption('Select', '- All -')); 
        return options;
    }
    
    public class formerSDSSolution {
        public string val {get;set;}
        public boolean selected {get;set;}
        
        public formerSDSSolution(string xval) {
            val = xval;
            selected = false;
        }
    }
    
}