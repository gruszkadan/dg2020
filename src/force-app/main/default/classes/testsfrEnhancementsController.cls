@isTest
private class testsfrEnhancementsController {
    static testmethod void test1() {
        id queue1 = [SELECT Queueid FROM QueueSObject WHERE SObjectType = 'Salesforce_Request__c' and Queue.Name ='Enhancements' LIMIT 1].Queueid;
        id queue2 = [SELECT Queueid FROM QueueSObject WHERE SObjectType = 'Salesforce_Request__c' and Queue.Name ='Enhancements Dev Work Queue' LIMIT 1].Queueid;
        
        Salesforce_Request__c sfr1 = new Salesforce_Request__c();
        sfr1.Ownerid = queue1;
        sfr1.Summary__c = 'test';
        sfr1.Request_Type__c = 'Enhancement';
        insert sfr1;
        
        Salesforce_Request__c sfr2 = new Salesforce_Request__c();
        sfr2.Ownerid = queue2;
        sfr2.Summary__c = 'test';
        sfr2.Request_Type__c = 'Enhancement';
        insert sfr2;
        
        sfrEnhancementsController con = new sfrEnhancementsController();
        
        con.find();
        con.view();
    }
    
    static testmethod void test2() {
        id queue1 = [SELECT Queueid FROM QueueSObject WHERE SObjectType = 'Salesforce_Request__c' and Queue.Name ='Projects' LIMIT 1].Queueid;
        id queue2 = [SELECT Queueid FROM QueueSObject WHERE SObjectType = 'Salesforce_Request__c' and Queue.Name ='Projects Dev Work Queue' LIMIT 1].Queueid;
        
        Salesforce_Request__c sfr1 = new Salesforce_Request__c();
        sfr1.Ownerid = queue1;
        sfr1.Summary__c = 'test';
        sfr1.Request_Type__c = 'Project';
        insert sfr1;
        
        Salesforce_Request__c sfr2 = new Salesforce_Request__c();
        sfr2.Ownerid = queue2;
        sfr2.Summary__c = 'test';
        sfr2.Request_Type__c = 'Project';
        insert sfr2;
        
        sfrProjectsController con = new sfrProjectsController();
        
        con.find();
        con.view();
    }
    
    static testmethod void test3() {
        id queue = [SELECT Queueid FROM QueueSObject WHERE SObjectType = 'Salesforce_Request__c' and Queue.Name ='Projects' LIMIT 1].Queueid;
        id joe = [SELECT id FROM User WHERE Name = 'Joseph Berkowitz' LIMIT 1].id;
        id mark = [SELECT id FROM User WHERE Name = 'Mark McCauley' LIMIT 1].id;
        id ryan = [SELECT id FROM User WHERE Name = 'Ryan Werner' LIMIT 1].id;
        
        Salesforce_Request__c sfr1 = new Salesforce_Request__c();
        sfr1.Ownerid = queue;
        sfr1.Summary__c = 'test';
        sfr1.Request_Type__c = 'Project';
        insert sfr1;
        
        Salesforce_Request__c sfr2 = new Salesforce_Request__c();
        sfr2.Ownerid = joe;
        sfr2.Summary__c = 'test';
        sfr2.Request_Type__c = 'Enhancement';
        insert sfr2;
        
        Salesforce_Request__c sfr3 = new Salesforce_Request__c();
        sfr3.Ownerid = mark;
        sfr3.Summary__c = 'test';
        sfr3.Request_Type__c = 'Bug';
        insert sfr3;
        
        Salesforce_Request__c sfr4 = new Salesforce_Request__c();
        sfr4.Ownerid = ryan;
        sfr4.Summary__c = 'test';
        sfr4.Request_Type__c = 'Project';
        insert sfr4;
        
        sfrRequestsTBAController con = new sfrRequestsTBAController();
        
        con.find();
        con.findIcons();
        con.view();
    }
}