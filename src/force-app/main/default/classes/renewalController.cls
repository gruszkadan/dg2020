public class renewalController {
  private ApexPages.StandardController controller;
  public Contract__c theContract {get;set;}
  public Renewal__c theRenewal {get;set;}
  public Integer contractCount {get;set;}
  public Attachment[] theDocs {get;set;}
  public Renewal_Note__c[] renewalNotes {get;set;}
  public string environment {get;set;}
  
  public renewalController(ApexPages.StandardController stdController) {
         Id renewalId = ApexPages.currentPage().getParameters().get('id');
         this.controller = stdController;
        if (renewalId != null ) {
        theRenewal = [Select ID, Name, Num_of_Renewal_Notes__c from Renewal__c where ID=:renewalID];
        contractCount = [select count() from Contract__c where Renewal__c=: renewalId];
        }
        if(contractCount >0){
          theContract = [SELECT Id, Name, Renewal__c, Status__c FROM Contract__c WHERE Renewal__c = :renewalId ORDER BY CreatedDate desc limit 1];
        }
        if(contractCount >0){
          theDocs();
          }
        if(theRenewal.Num_of_Renewal_Notes__c>0){
     	renewalNotes=[SELECT Id, Name, Note_Type__c, Renewal__c, Comments__c, Created_Date__c, Created_By__c  FROM Renewal_Note__c
           WHERE Renewal__c = :renewalID ORDER BY CreatedDate desc];
     }
     ID orgID = UserInfo.getOrganizationId();
     if(orgID =='00D300000001HEfEAM'){
     	environment = 'Production';
     } else {
     	environment ='Sandbox';
     }   
   }
   
   public void theDocs () {
            theDocs = [Select ParentId, Name, LastModifiedDate, Id, CreatedById From Attachment where ParentID=:theContract.id];
     
   }
   
   public PageReference newRenewalNote(){
    	PageReference renewalNote= new PageReference('/a14/e?');
		renewalNote.setRedirect(true);
		if(environment =='Sandbox'){
		renewalNote.getParameters().put('CF00NS0000001EqOW_lkid',theRenewal.Id);
		renewalNote.getParameters().put('CF00NS0000001EqOW',theRenewal.Name);
		} else {
		renewalNote.getParameters().put('CF00N800000057HR8_lkid',theRenewal.Id);
		renewalNote.getParameters().put('CF00N800000057HR8',theRenewal.Name);	
		}
		return renewalNote;
    }
}